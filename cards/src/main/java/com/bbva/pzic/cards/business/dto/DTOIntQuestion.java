package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
public class DTOIntQuestion {

    @NotNull(groups = ValidationGroup.CreateOffersGenerateCards.class)
    private String id;
    @NotNull(groups = ValidationGroup.CreateOffersGenerateCards.class)
    private String answerValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAnswerValue() {
        return answerValue;
    }

    public void setAnswerValue(String answerValue) {
        this.answerValue = answerValue;
    }
}

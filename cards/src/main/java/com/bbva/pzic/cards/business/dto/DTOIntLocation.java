package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import java.util.List;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
public class DTOIntLocation {

    private String id;
    private String formattedAddress;
    @Valid
    private List<DTOIntAddressComponents> addressComponents;
    private List<String> locationTypes;
    @Valid
    private DTOIntGeolocation geolocation;
    private String additionalInformation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public List<DTOIntAddressComponents> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<DTOIntAddressComponents> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public List<String> getLocationTypes() {
        return locationTypes;
    }

    public void setLocationTypes(List<String> locationTypes) {
        this.locationTypes = locationTypes;
    }

    public DTOIntGeolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(DTOIntGeolocation geolocation) {
        this.geolocation = geolocation;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }
}

package com.bbva.pzic.cards.util;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.bbva.pzic.cards.util.Errors.WRONG_DATE;
import static org.apache.commons.lang.StringUtils.isEmpty;

/**
 * Created on 15/04/2020.
 *
 * @author Entelgy
 */
public final class Converter {

    private static final String TRUE = "S";
    private static final String FALSE = "N";
    private static final String TRUE_1 = "1";
    private static final String FALSE_0 = "0";
    private static final String TRUE_VALUE = "true";
    private static final String FALSE_VALUE = "false";

    public static Boolean convertFrom(String source) {
        return stringToBoolean(source, TRUE, FALSE);
    }

    public static Boolean convertFromString(String source) {
        return stringToBoolean(source, TRUE_1, FALSE_0);
    }

    public static Boolean stringToBoolean(String source, String trueValue, String falseValue) {
        return trueValue.equalsIgnoreCase(source) ? Boolean.TRUE : (falseValue.equalsIgnoreCase(source) ? Boolean.FALSE : null);
    }

    public static Integer booleanToInteger(final Boolean source) {
        return source == null ? null : (source ? 1 : 0);
    }

    public static String booleanToString(final Boolean source) {
        if (source == null) {
            return null;
        }
        return source ? "1" : "0";
    }

    public static Calendar dateToDateTime(Date date) {
        return dateToDateTime(date, "00:00:00");
    }

    public static Calendar dateToDateTime(Date date, String time) {
        if (date == null || StringUtils.isEmpty(time)) {
            return null;
        }
        try {
            return DateUtils.toDateTime(date, time);
        } catch (ParseException e) {
            throw new BusinessServiceException(WRONG_DATE, e);
        }
    }

    public static boolean isAllEmpty(Object... values) {
        for (Object value : values) {
            if (value instanceof String) {
                if (!isEmpty((String) value)) return false;
            } else if (value instanceof List) {
                if (!CollectionUtils.isEmpty((List) value)) return false;
            } else if (value != null) {
                return false;
            }
        }
        return true;
    }

    public static String booleanStringToString(String value) {
        return StringUtils.isEmpty(value) ? null : TRUE_VALUE.equalsIgnoreCase(value) ? TRUE : FALSE_VALUE.equalsIgnoreCase(value) ? FALSE : null;
    }

    public static String stringToBooleanToString(String value) {
        return StringUtils.isEmpty(value) ? null : TRUE.equalsIgnoreCase(value) ? TRUE_VALUE : FALSE.equalsIgnoreCase(value) ? FALSE_VALUE : null;
    }
}
package com.bbva.pzic.cards.business.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created on 13/02/2016.
 *
 * @author Entelgy
 */
public class DTOIntCardStatementDetail {

    public static final String PEN = "PEN";
    public static final String USD = "USD";

    private String formatsPan;

    private String cardId;
    private String productName;
    private String branchName;

    private String participantNameFirst;
    private String participantNameSecond;

    private String participantType;
    private String accountNumberPEN;
    private String accountNumberUSD;

    private String currency;

    private DTOIntCardStatementProgram program;

    private Date endDate;

    private String payType;

    private DTOIntAddress address;

    private BigDecimal creditLimit;

    private DTOIntCardStatementLimits limits;

    private Date paymentDate;

    private DTOIntCardStatementInterest interest;

    private DTOMoney disposedBalanceLocalCurrency;
    private DTOMoney disposedBalance;

    private Integer totalTransactions;

    private DTOIntExtraPayments extraPayments;

    private DTOMoney totalDebtLocalCurrency;
    private DTOMoney totalDebt;

    private Integer totalMonthsAmortizationMinimum;
    private Integer totalMonthsAmortizationMinimumFull;

    public String getFormatsPan() {
        return formatsPan;
    }

    public void setFormatsPan(String formatsPan) {
        this.formatsPan = formatsPan;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getParticipantNameFirst() {
        return participantNameFirst;
    }

    public void setParticipantNameFirst(String participantNameFirst) {
        this.participantNameFirst = participantNameFirst;
    }

    public String getParticipantNameSecond() {
        return participantNameSecond;
    }

    public void setParticipantNameSecond(String participantNameSecond) {
        this.participantNameSecond = participantNameSecond;
    }

    public String getParticipantType() {
        return participantType;
    }

    public void setParticipantType(String participantType) {
        this.participantType = participantType;
    }

    public String getAccountNumberPEN() {
        return accountNumberPEN;
    }

    public void setAccountNumberPEN(String accountNumberPEN) {
        this.accountNumberPEN = accountNumberPEN;
    }

    public String getAccountNumberUSD() {
        return accountNumberUSD;
    }

    public void setAccountNumberUSD(String accountNumberUSD) {
        this.accountNumberUSD = accountNumberUSD;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public DTOIntCardStatementProgram getProgram() {
        return program;
    }

    public void setProgram(DTOIntCardStatementProgram program) {
        this.program = program;
    }

    public Date getEndDate() {
        if (endDate == null) {
            return null;
        }
        return new Date(endDate.getTime());
    }

    public void setEndDate(Date endDate) {
        if (endDate != null) {
            this.endDate = new Date(endDate.getTime());
        }
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public DTOIntAddress getAddress() {
        return address;
    }

    public void setAddress(DTOIntAddress address) {
        this.address = address;
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public DTOIntCardStatementLimits getLimits() {
        return limits;
    }

    public void setLimits(DTOIntCardStatementLimits limits) {
        this.limits = limits;
    }

    public Date getPaymentDate() {
        if (paymentDate == null) {
            return null;
        }
        return new Date(paymentDate.getTime());
    }

    public void setPaymentDate(Date paymentDate) {
        if (paymentDate != null) {
            this.paymentDate = new Date(paymentDate.getTime());
        }
    }

    public DTOIntCardStatementInterest getInterest() {
        return interest;
    }

    public void setInterest(DTOIntCardStatementInterest interest) {
        this.interest = interest;
    }

    public DTOMoney getDisposedBalanceLocalCurrency() {
        return disposedBalanceLocalCurrency;
    }

    public void setDisposedBalanceLocalCurrency(DTOMoney disposedBalanceLocalCurrency) {
        this.disposedBalanceLocalCurrency = disposedBalanceLocalCurrency;
        this.disposedBalanceLocalCurrency.setCurrency(PEN);
    }

    public DTOMoney getDisposedBalance() {
        return disposedBalance;
    }

    public void setDisposedBalance(DTOMoney disposedBalance) {
        this.disposedBalance = disposedBalance;
        this.disposedBalance.setCurrency(USD);
    }

    public Integer getTotalTransactions() {
        return totalTransactions;
    }

    public void setTotalTransactions(Integer totalTransactions) {
        this.totalTransactions = totalTransactions;
    }

    public DTOIntExtraPayments getExtraPayments() {
        return extraPayments;
    }

    public void setExtraPayments(DTOIntExtraPayments extraPayments) {
        this.extraPayments = extraPayments;
    }

    public DTOMoney getTotalDebtLocalCurrency() {
        return totalDebtLocalCurrency;
    }

    public void setTotalDebtLocalCurrency(DTOMoney totalDebtLocalCurrency) {
        this.totalDebtLocalCurrency = totalDebtLocalCurrency;
        this.totalDebtLocalCurrency.setCurrency(PEN);
    }

    public DTOMoney getTotalDebt() {
        return totalDebt;
    }

    public void setTotalDebt(DTOMoney totalDebt) {
        this.totalDebt = totalDebt;
        this.totalDebt.setCurrency(USD);
    }

    public Integer getTotalMonthsAmortizationMinimum() {
        return totalMonthsAmortizationMinimum;
    }

    public void setTotalMonthsAmortizationMinimum(Integer totalMonthsAmortizationMinimum) {
        this.totalMonthsAmortizationMinimum = totalMonthsAmortizationMinimum;
    }

    public Integer getTotalMonthsAmortizationMinimumFull() {
        return totalMonthsAmortizationMinimumFull;
    }

    public void setTotalMonthsAmortizationMinimumFull(Integer totalMonthsAmortizationMinimumFull) {
        this.totalMonthsAmortizationMinimumFull = totalMonthsAmortizationMinimumFull;
    }
}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputCreateCardsMaskedToken;
import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMEN2F;
import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMS12F;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardsMaskedTokenMapper;
import com.bbva.pzic.cards.facade.v0.dto.MaskedToken;
import com.bbva.pzic.cards.util.mappers.Mapper;

/**
 * Created on 23/07/2019.
 *
 * @author Entelgy
 */
@Mapper
public class TxCreateCardsMaskedTokenMapper implements ITxCreateCardsMaskedTokenMapper {

    @Override
    public FormatoMPMEN2F mapIn(final InputCreateCardsMaskedToken dtoIn) {
        FormatoMPMEN2F formatoMPMEN2F = new FormatoMPMEN2F();
        formatoMPMEN2F.setIdetarj(dtoIn.getCardId());
        formatoMPMEN2F.setFeccad(dtoIn.getExpirationDate());
        return formatoMPMEN2F;
    }

    @Override
    public MaskedToken mapOut(final FormatoMPMS12F formatOutput) {
        MaskedToken maskedToken = new MaskedToken();
        maskedToken.setId(formatOutput.getIdtoken());
        maskedToken.setToken(formatOutput.getNrotarj());
        return maskedToken;
    }
}

package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "RelatedContract", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "RelatedContract", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class RelatedContract implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier associated to the relationship between the card and the
     * contract.
     */
    @DatoAuditable(omitir = true)
    private String relatedContractId;
    /**
     * Identifier associated to the contract.
     */
    @DatoAuditable(omitir = true)
    private String contractId;
    /**
     * Contract number.
     */
    @DatoAuditable(omitir = true)
    private String number;
    /**
     * Contract number type based on the financial product type.
     */
    private NumberType numberType;
    /**
     * Financial product associated to the contract.
     */
    private Product product;
    /**
     * Type of relation between the related contract and the card.
     */
    private RelationType relationType;

    public String getRelatedContractId() {
        return relatedContractId;
    }

    public void setRelatedContractId(String relatedContractId) {
        this.relatedContractId = relatedContractId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public NumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(NumberType numberType) {
        this.numberType = numberType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public RelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(RelationType relationType) {
        this.relationType = relationType;
    }
}
package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "delivery", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "delivery", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Delivery implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Destination type of card delivery.
     */
    private Destination destination;
    /**
     * Custom address where the card will be deliver. This can be an address
     * that the customer enters manually or and address previously selected by
     * the customer.
     */
    private Address address;

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
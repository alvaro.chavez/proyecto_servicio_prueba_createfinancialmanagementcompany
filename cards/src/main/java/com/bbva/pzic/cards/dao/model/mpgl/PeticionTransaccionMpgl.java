package com.bbva.pzic.cards.dao.model.mpgl;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>MPGL</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpgl</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpgl</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: QGDTCCT.MPGL.txt
 * MPGLLIMITES DE TARJETAS                MP        MP2CMPGL     01 MPMENGL             MPGL  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2020-04-27XP96618 2020-05-0711.47.50XP96618 2020-04-27-10.30.56.417603XP96618 0001-01-010001-01-01
 * 
 * FICHERO: QGDTFDF.MPMENGL.txt
 * MPMENGL �LIMITES DE TARJETAS           �F�01�00019�01�00001�IDETARJ�NUMERO DE TARJETA   �A�019�0�R�        �
 * 
 * FICHERO: QGDTFDF.MPMS1GL.txt
 * MPMS1GL �SALIDA LIMITES DE TARJETAS    �X�07�00055�01�00001�IDLIMIT�IDENT. DE LIMITE    �A�001�0�S�        �
 * MPMS1GL �SALIDA LIMITES DE TARJETAS    �X�07�00055�02�00002�MONACTT�MONTO LIMITE ACTUAL �S�015�2�S�        �
 * MPMS1GL �SALIDA LIMITES DE TARJETAS    �X�07�00055�03�00017�DIVACTT�MONEDA LIMITE ACTUAL�A�003�0�S�        �
 * MPMS1GL �SALIDA LIMITES DE TARJETAS    �X�07�00055�04�00020�MONMINT�MONTO LIMITE MINIMO �S�015�2�S�        �
 * MPMS1GL �SALIDA LIMITES DE TARJETAS    �X�07�00055�05�00035�DIVMINT�MONEDA LIMITE MINIMO�A�003�0�S�        �
 * MPMS1GL �SALIDA LIMITES DE TARJETAS    �X�07�00055�06�00038�MONMAXT�MONTO LIMITE MAXIMO �S�015�2�S�        �
 * MPMS1GL �SALIDA LIMITES DE TARJETAS    �X�07�00055�07�00053�DIVMAXT�MONEDA LIMITE MAXIMO�A�003�0�S�        �
 * 
 * FICHERO: QGDTFDX.MPGL.txt
 * MPGLMPMS1GL MPNCS1GLMP2CMPGL1S                             XP96618 2020-04-27-11.33.41.066510XP96618 2020-04-27-11.33.41.066543
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionMpgl
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPGL",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpgl.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENGL.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionMpgl implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}
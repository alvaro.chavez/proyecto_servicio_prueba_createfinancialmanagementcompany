package com.bbva.pzic.cards.dao.model.mpdc;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MCRMDC0</code> de la transacci&oacute;n <code>MPDC</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MCRMDC0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMCRMDC0 {

	/**
	 * <p>Campo <code>IDOPER</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDOPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
	private String idoper;

	/**
	 * <p>Campo <code>DESOPER</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "DESOPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String desoper;

	/**
	 * <p>Campo <code>TIPREEM</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "TIPREEM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String tipreem;

	/**
	 * <p>Campo <code>ISALACR</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 4, nombre = "ISALACR", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal isalacr;

	/**
	 * <p>Campo <code>DIVSACR</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "DIVSACR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divsacr;

	/**
	 * <p>Campo <code>FECOPER</code>, &iacute;ndice: <code>6</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 6, nombre = "FECOPER", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecoper;

	/**
	 * <p>Campo <code>HORAOPE</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "HORAOPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String horaope;

	/**
	 * <p>Campo <code>FECFACT</code>, &iacute;ndice: <code>8</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 8, nombre = "FECFACT", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecfact;

	/**
	 * <p>Campo <code>HORAFAC</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "HORAFAC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String horafac;

	/**
	 * <p>Campo <code>NCUENTA</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "NCUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String ncuenta;

	/**
	 * <p>Campo <code>ISALCTA</code>, &iacute;ndice: <code>11</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 11, nombre = "ISALCTA", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal isalcta;

	/**
	 * <p>Campo <code>DIVSACT</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "DIVSACT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divsact;

	/**
	 * <p>Campo <code>TIPCAM</code>, &iacute;ndice: <code>13</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 13, nombre = "TIPCAM", tipo = TipoCampo.DECIMAL, longitudMinima = 7, longitudMaxima = 7, decimales = 4)
	private BigDecimal tipcam;

	/**
	 * <p>Campo <code>TIPPREC</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "TIPPREC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipprec;

	/**
	 * <p>Campo <code>TOTIMP</code>, &iacute;ndice: <code>15</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 15, nombre = "TOTIMP", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal totimp;

	/**
	 * <p>Campo <code>DIVTOTI</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "DIVTOTI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divtoti;

}
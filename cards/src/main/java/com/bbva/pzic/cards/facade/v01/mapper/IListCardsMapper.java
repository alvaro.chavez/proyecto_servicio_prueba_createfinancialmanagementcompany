package com.bbva.pzic.cards.facade.v01.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.canonic.CardsList;

import java.util.List;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
public interface IListCardsMapper {
    /**
     * Input Mapping to DTO
     *
     * @param customerId        Customer ID
     * @param cardTypeId        Type card ID
     * @param physicalSupportId Physical Support ID
     * @param statusId          Status ID
     * @param paginationKey     Pagination ID
     * @param pageSize          Size of pagination
     * @return DTOIntListCards
     */
    DTOIntListCards mapIn(String customerId, List<String> cardTypeId, String physicalSupportId,
                          List<String> statusId, String paginationKey, Integer pageSize);

    /**
     * Output Mapping to CardsList
     *
     * @param listCards departure Information
     * @param userAgent an user agent
     * @return CardsList
     */
    CardsList mapOut(DTOOutListCards listCards, String userAgent);
}

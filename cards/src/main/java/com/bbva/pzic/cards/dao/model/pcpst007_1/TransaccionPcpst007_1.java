package com.bbva.pzic.cards.dao.model.pcpst007_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PCPST007</code>
 * 
 * @see PeticionTransaccionPcpst007_1
 * @see RespuestaTransaccionPcpst007_1
 */
@Component
public class TransaccionPcpst007_1 implements InvocadorTransaccion<PeticionTransaccionPcpst007_1,RespuestaTransaccionPcpst007_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPcpst007_1 invocar(PeticionTransaccionPcpst007_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPcpst007_1.class, RespuestaTransaccionPcpst007_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPcpst007_1 invocarCache(PeticionTransaccionPcpst007_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPcpst007_1.class, RespuestaTransaccionPcpst007_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
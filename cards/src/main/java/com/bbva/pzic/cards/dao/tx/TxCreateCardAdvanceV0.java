package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.dao.model.mprt.FormatoMPRMRT0;
import com.bbva.pzic.cards.dao.model.mprt.FormatoMPRMRT1;
import com.bbva.pzic.cards.dao.model.mprt.PeticionTransaccionMprt;
import com.bbva.pzic.cards.dao.model.mprt.RespuestaTransaccionMprt;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardAdvanceMapperV0;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 04/10/2018.
 *
 * @author Entelgy
 */
@Component("txCreateCardAdvanceV0")
public class TxCreateCardAdvanceV0
        extends SingleOutputFormat<InputCreateCard, FormatoMPRMRT0, Card, FormatoMPRMRT1> {

    @Resource(name = "txCreateCardAdvanceMapperV0")
    private ITxCreateCardAdvanceMapperV0 mapper;

    @Autowired
    public TxCreateCardAdvanceV0(@Qualifier("transaccionMprt") InvocadorTransaccion<PeticionTransaccionMprt, RespuestaTransaccionMprt> transaction) {
        super(transaction, PeticionTransaccionMprt::new, Card::new, FormatoMPRMRT1.class);
    }

    @Override
    protected FormatoMPRMRT0 mapInput(InputCreateCard inputCreateCard) {
        return mapper.mapIn(inputCreateCard);
    }

    @Override
    protected Card mapFirstOutputFormat(FormatoMPRMRT1 formatoMPRMRT1, InputCreateCard inputCreateCard, Card card) {
        return null;
    }
}

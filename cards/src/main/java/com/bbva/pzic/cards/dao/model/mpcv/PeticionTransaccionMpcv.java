package com.bbva.pzic.cards.dao.model.mpcv;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPCV</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpcv</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpcv</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPCV.D1171120.txt
 * MPCVLISTADO DE CODIGOS DE SEGURIDAD    MP        MP2CMPCVPBDMPPO MPMENCV             MPCV  NN3000CNNNNN    SSTN     E  NNNSNNNN  NN                2017-05-04XP85517 2017-11-1613.52.07XP85517 2017-05-04-11.05.57.826818XP85517 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENCV.D1171120.txt
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�01�00001�NUMTARJ�NUMERO DE TARJETA   �A�016�0�R�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�02�00017�KEYPB01�LLAVE PUB. MOVIL 01 �A�075�0�R�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�03�00092�KEYPB02�LLAVE PUB. MOVIL 02 �A�075�0�R�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�04�00167�KEYPB03�LLAVE PUB. MOVIL 03 �A�075�0�R�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�05�00242�KEYPB04�LLAVE PUB. MOVIL 04 �A�075�0�R�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�06�00317�KEYPB05�LLAVE PUB. MOVIL 05 �A�075�0�O�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�07�00392�KEYPB06�LLAVE PUB. MOVIL 06 �A�075�0�O�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�08�00467�KEYPB07�LLAVE PUB. MOVIL 07 �A�075�0�O�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�09�00542�KEYPB08�LLAVE PUB. MOVIL 08 �A�075�0�O�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�10�00617�KEYPB09�LLAVE PUB. MOVIL 09 �A�075�0�O�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�11�00692�KEYPB10�LLAVE PUB. MOVIL 10 �A�075�0�O�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�12�00767�KEYPB11�LLAVE PUB. MOVIL 11 �A�075�0�O�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�13�00842�KEYPB12�LLAVE PUB. MOVIL 12 �A�075�0�O�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�14�00917�KEYPB13�LLAVE PUB. MOVIL 13 �A�075�0�O�        �
 * MPMENCV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�15�00992�KEYPB14�LLAVE PUB. MOVIL 14 �A�075�0�O�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1CV.D1171120.txt
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�01�00001�IDCODSG�ID CODIGO SEGURIDAD �A�004�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�02�00005�DSCODSG�DES.CODIGO SEGURIDAD�A�030�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�03�00035�CODSE01�COD.CVV2 CIFRADO 01 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�04�00110�CODSE02�COD.CVV2 CIFRADO 02 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�05�00185�CODSE03�COD.CVV2 CIFRADO 03 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�06�00260�CODSE04�COD.CVV2 CIFRADO 04 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�07�00335�CODSE05�COD.CVV2 CIFRADO 05 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�08�00410�CODSE06�COD.CVV2 CIFRADO 06 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�09�00485�CODSE07�COD.CVV2 CIFRADO 07 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�10�00560�CODSE08�COD.CVV2 CIFRADO 08 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�11�00635�CODSE09�COD.CVV2 CIFRADO 09 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�12�00710�CODSE10�COD.CVV2 CIFRADO 10 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�13�00785�CODSE11�COD.CVV2 CIFRADO 11 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�14�00860�CODSE12�COD.CVV2 CIFRADO 12 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�15�00935�CODSE13�COD.CVV2 CIFRADO 13 �A�075�0�S�        �
 * MPMS1CV �LISTADO DE CODIGOS DE SEGURIDA�X�16�01084�16�01010�CODSE14�COD.CVV2 CIFRADO 14 �A�075�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPCV.D1171120.txt
 * MPCVMPMS1CV MPNCS1CVMP2CMPCV1S                             XP85517 2017-05-04-14.34.20.124775XP85517 2017-05-04-14.34.20.125916
</pre></code>
 *
 * @see RespuestaTransaccionMpcv
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPCV",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpcv.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENCV.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpcv implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
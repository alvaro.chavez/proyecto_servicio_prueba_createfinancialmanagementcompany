package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPME0W1;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPMS1W1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardMapperV0;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.util.Constants.DEFAULT_TIME;

/**
 * Created on 30/01/2018.
 *
 * @author Entelgy
 */
@Mapper("txCreateCardMapperV0")
public class TxCreateCardMapperV0 extends ConfigurableMapper implements ITxCreateCardMapperV0 {

    private static final String CARDS_DELIVERIES_MANAGEMENT_ID = "cards.deliveriesManagement.id";

    @Autowired
    private AbstractCypherTool cypherTool;

    @Autowired
    private Translator translator;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(InputCreateCard.class, FormatoMPME0W1.class)
                .field("dtoIntCard.participantId", "numclie")
                .field("dtoIntCard.cardTypeId", "tiprodf")
                .field("dtoIntCard.physicalSupportId", "sopfisi")
                .field("dtoIntCard.relatedContractIdLinkedWith", "ctacarg")
                .field("dtoIntCard.relatedContractIdExpeditionConstant", "indtjad")
                .field("dtoIntCard.relatedContractIdExpedition", "nucorel")
                .field("dtoIntCard.titleId", "mctarje")
                .field("dtoIntCard.cutOffDay", "fcierre")
                .field("dtoIntCard.paymentMethodEndDay", "fcargo")
                .field("dtoIntCard.statementType", "indeecc")
                .field("dtoIntCard.grantedCreditsAmount", "lincred")
                .field("dtoIntCard.grantedCreditsCurrency", "divisa")
                .field("dtoIntCard.destinationIdHome", "numdom2")
                .field("dtoIntCard.destinationIdBranch", "codofen")
                .field("dtoIntCard.deliveryAdressConstant", "codser2")
                .field("dtoIntCard.membershipsNumber", "codpafr")
                .field("dtoIntCard.supportContractType", "tcontra")
                .field("dtoIntCard.deliveriesManagementServiceTypeIdStatement", "codseri")
                .field("dtoIntCard.deliveriesManagementContactDetailIdStatement", "corafil")
                .field("dtoIntCard.deliveriesManagementAddressIdStatement", "numdomi")
                .field("dtoIntCard.deliveriesManagementServiceTypeIdSticker", "codser3")
                .field("dtoIntCard.deliveriesManagementAddressIdSticker", "numdom3")
                .field("dtoIntCard.deliveriesManagementContactDetailIdSmsCode", "idtesms")
                .field("dtoIntCard.deliveryManagementConstant", "indcsms")
                .field("dtoIntCard.contractingBussinesAgentId", "regisco")
                .field("dtoIntCard.contractingBussinesAgentOriginId", "indorig")
                .field("dtoIntCard.marketBusinessAgentId", "regisve")
                .field("dtoIntCard.imageId", "distarj")
                .field("dtoIntCard.offerId", "idoffer")
                .field("dtoIntCard.managementBranchId", "codgest")
                .field("dtoIntCard.contractingBranchId", "codofig")
                .register();

        factory.classMap(Card.class, FormatoMPMS1W1.class)
                .field("number", "numtarj")
                .field("numberType.name", "dstiptj")
                .field("cardType.name", "dsprodf")
                .field("brandAssociation.name", "nomatar")
                .field("physicalSupport.name", "dsopfis")
                .field("expirationDate", "fecvenc")
                .field("holderName", "nombcli")
                .field("status.name", "destarj")
                .field("delivery.address.id", "codofen")
                .field("delivery.address.addressName", "desofen")
                .field("deliveriesManagement[0].serviceType.id", "codseri")
                .field("deliveriesManagement[0].serviceType.name", "desseri")
                .field("deliveriesManagement[0].address.id", "numdomi")
                .field("deliveriesManagement[0].address.addressName", "desdomi")
                .field("deliveriesManagement[1].serviceType.id", "codser2")
                .field("deliveriesManagement[1].serviceType.name", "desser2")
                .field("deliveriesManagement[1].address.id", "numdom2")
                .field("deliveriesManagement[1].address.addressName", "desdom2")
                .field("deliveriesManagement[2].serviceType.id", "codser3")
                .field("deliveriesManagement[2].serviceType.name", "desser3")
                .field("deliveriesManagement[2].address.id", "numdom3")
                .field("deliveriesManagement[2].address.addressName", "desdom3")
                .register();

        factory.classMap(GrantedCredit.class, FormatoMPMS1W1.class)
                .field("amount", "ilimcre")
                .field("currency", "dlimcre")
                .register();

        factory.classMap(RelatedContract.class, FormatoMPMS1W1.class)
                .field("relatedContractId", "idcorel")
                .field("number", "nucorel")
                .field("numberType.name", "detcore")
                .register();
    }

    @Override
    public FormatoMPME0W1 mapIn(InputCreateCard input) {
        return map(input, FormatoMPME0W1.class);
    }

    @Override
    public Card mapOut(FormatoMPMS1W1 formatOutput) {
        if (formatOutput == null) {
            return null;
        }
        Card card = map(formatOutput, Card.class);
        card.setOpeningDate(FunctionUtils.buildDatetime(formatOutput.getFecalta(), DEFAULT_TIME));
        card.setCardId(cypherTool.encrypt(formatOutput.getNumtarj(), AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V0));

        updateNumberType(formatOutput, card);
        updateCardType(formatOutput, card);
        updateBrandAssociation(formatOutput, card);
        updatePhysicalSupport(formatOutput, card);
        updateCurrencies(formatOutput, card);
        updateGrantedCredits(formatOutput, card);
        updateStatus(formatOutput, card);
        updateRelatedContracts(formatOutput, card);
        updateDeliveriesManagement(formatOutput, card);

        return card;
    }

    private void updateDeliveriesManagement(FormatoMPMS1W1 formatOutput, Card card) {
        if (formatOutput.getCodseri() != null)
            card.getDeliveriesManagement().get(0).getServiceType().setId(translator.translateBackendEnumValueStrictly(CARDS_DELIVERIES_MANAGEMENT_ID, formatOutput.getCodseri()));
        if (formatOutput.getCodser2() != null)
            card.getDeliveriesManagement().get(1).getServiceType().setId(translator.translateBackendEnumValueStrictly(CARDS_DELIVERIES_MANAGEMENT_ID, formatOutput.getCodser2()));
        if (formatOutput.getCodser3() != null)
            card.getDeliveriesManagement().get(2).getServiceType().setId(translator.translateBackendEnumValueStrictly(CARDS_DELIVERIES_MANAGEMENT_ID, formatOutput.getCodser3()));
    }

    private void updateNumberType(FormatoMPMS1W1 formatOutput, Card card) {
        NumberType numberType = card.getNumberType();
        if (numberType == null) {
            numberType = new NumberType();
        }
        numberType.setId(translator.translateBackendEnumValueStrictly("cards.numberType.id", formatOutput.getIdtiptj()));
        card.setNumberType(numberType);
    }

    private void updateCardType(FormatoMPMS1W1 formatOutput, Card card) {
        CardType cardType = card.getCardType();
        if (cardType == null) {
            cardType = new CardType();
        }
        cardType.setId(translator.translateBackendEnumValueStrictly("cards.cardType.id", formatOutput.getTiprodf()));
        card.setCardType(cardType);
    }

    private void updateBrandAssociation(FormatoMPMS1W1 formatOutput, Card card) {
        BrandAssociation brandAssociation = card.getBrandAssociation();
        if (brandAssociation == null) {
            brandAssociation = new BrandAssociation();
        }
        brandAssociation.setId(translator.translateBackendEnumValueStrictly("cards.brandAssociation.id", formatOutput.getIdmatar()));
        card.setBrandAssociation(brandAssociation);
    }

    private void updatePhysicalSupport(FormatoMPMS1W1 formatOutput, Card card) {
        PhysicalSupport physicalSupport = card.getPhysicalSupport();
        if (physicalSupport == null) {
            physicalSupport = new PhysicalSupport();
        }
        physicalSupport.setId(translator.translateBackendEnumValueStrictly("cards.physicalSupport.id", formatOutput.getSopfisi()));
        card.setPhysicalSupport(physicalSupport);
    }

    private void updateCurrencies(FormatoMPMS1W1 formatOutput, Card card) {
        List<Currency> currencies = new ArrayList<>();
        currencies.add(convertCurrency(formatOutput.getMonpri(), true));
        currencies.add(convertCurrency(formatOutput.getMonsec(), false));
        card.setCurrencies(currencies);
    }

    private void updateGrantedCredits(FormatoMPMS1W1 formatOutput, Card card) {
        GrantedCredit grantedCredit = map(formatOutput, GrantedCredit.class);
        if (grantedCredit.getAmount() == null && grantedCredit.getCurrency() == null) {
            return;
        }
        List<GrantedCredit> grantedCredits = new ArrayList<>();
        grantedCredits.add(grantedCredit);
        card.setGrantedCredits(grantedCredits);
    }

    private void updateStatus(FormatoMPMS1W1 formatOutput, Card card) {
        Status status = card.getStatus();
        if (status == null) {
            status = new Status();
        }
        status.setId(translator.translateBackendEnumValueStrictly("cards.status.id", formatOutput.getEstarjs()));
        card.setStatus(status);
    }

    private void updateRelatedContracts(FormatoMPMS1W1 formatOutput, Card card) {
        RelatedContract relatedContract = map(formatOutput, RelatedContract.class);
        if (formatOutput.getNucorel() != null) {
            relatedContract.setContractId(cypherTool.encrypt(formatOutput.getNucorel(), AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V0));
        }

        NumberType numberType = relatedContract.getNumberType();
        if (numberType == null) {
            numberType = new NumberType();
        }
        numberType.setId(translator.translateBackendEnumValueStrictly("cards.relatedContracts.relatedContract.numberType.id", formatOutput.getIdtcore()));
        relatedContract.setNumberType(numberType);

        List<RelatedContract> relatedContracts = new ArrayList<>();
        relatedContracts.add(relatedContract);
        card.setRelatedContracts(relatedContracts);
    }

    private Currency convertCurrency(String value, boolean isMajor) {
        Currency currency = new Currency();
        currency.setCurrency(value);
        currency.setIsMajor(isMajor);
        return currency;
    }
}

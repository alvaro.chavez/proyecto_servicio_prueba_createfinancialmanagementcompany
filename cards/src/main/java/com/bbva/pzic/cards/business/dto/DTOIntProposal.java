package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntProposal {

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private DTOIntCardType cardType;

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private DTOIntTitle title;

    @Valid
    private DTOIntPaymentMethod paymentMethod;

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private List<DTOIntImport> grantedCredits;

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private DTOIntDelivery delivery;

    @Valid
    private DTOIntBankType bank;

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private DTOIntFee fees;

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private List<DTOIntRate> rates;

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private List<DTOIntAdditionalProduct> additionalProducts;

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private DTOIntMembership membership;

    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    @Size(max = 15, groups = ValidationGroup.CreateCardsProposal.class)
    private String contactDetailsId0;

    @Size(max = 15, groups = ValidationGroup.CreateCardsProposal.class)
    private String contactDetailsId1;

    @Size(max = 20, groups = ValidationGroup.CreateCardsProposal.class)
    private String offerId;

    private String id;
    private Date operationDate;
    private String operationNumber;
    @Valid
    private List<DTOIntDelivery> deliveries;
    @Valid
    private DTOIntContactAbility contactability;
    @Valid
    private DTOIntProduct product;
    @Valid
    private DTOIntRate rate;

    @Pattern(regexp = "(?i)true|false", groups = ValidationGroup.ModifyCardProposal.class)
    private String notificationsByOperation;

    public DTOIntCardType getCardType() {
        return cardType;
    }

    public void setCardType(DTOIntCardType cardType) {
        this.cardType = cardType;
    }

    public DTOIntTitle getTitle() {
        return title;
    }

    public void setTitle(DTOIntTitle title) {
        this.title = title;
    }

    public DTOIntPaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(DTOIntPaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<DTOIntImport> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<DTOIntImport> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public DTOIntDelivery getDelivery() {
        return delivery;
    }

    public void setDelivery(DTOIntDelivery delivery) {
        this.delivery = delivery;
    }

    public DTOIntBankType getBank() {
        return bank;
    }

    public void setBank(DTOIntBankType bank) {
        this.bank = bank;
    }

    public DTOIntFee getFees() {
        return fees;
    }

    public void setFees(DTOIntFee fees) {
        this.fees = fees;
    }

    public List<DTOIntRate> getRates() {
        return rates;
    }

    public void setRates(List<DTOIntRate> rates) {
        this.rates = rates;
    }

    public List<DTOIntAdditionalProduct> getAdditionalProducts() {
        return additionalProducts;
    }

    public void setAdditionalProducts(
            List<DTOIntAdditionalProduct> additionalProducts) {
        this.additionalProducts = additionalProducts;
    }

    public DTOIntMembership getMembership() {
        return membership;
    }

    public void setMembership(DTOIntMembership membership) {
        this.membership = membership;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getOperationDate() {
        if (operationDate == null) {
            return null;
        }
        return new Date(operationDate.getTime());
    }

    public void setOperationDate(Date operationDate) {
        if (operationDate == null) {
            this.operationDate = null;
        } else {
            this.operationDate = new Date(operationDate.getTime());
        }
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public String getContactDetailsId0() {
        return contactDetailsId0;
    }

    public void setContactDetailsId0(String contactDetailsId0) {
        this.contactDetailsId0 = contactDetailsId0;
    }

    public String getContactDetailsId1() {
        return contactDetailsId1;
    }

    public void setContactDetailsId1(String contactDetailsId1) {
        this.contactDetailsId1 = contactDetailsId1;
    }

    public List<DTOIntDelivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<DTOIntDelivery> deliveries) {
        this.deliveries = deliveries;
    }

    public DTOIntContactAbility getContactability() {
        return contactability;
    }

    public void setContactability(DTOIntContactAbility contactability) {
        this.contactability = contactability;
    }

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }

    public DTOIntRate getRate() {
        return rate;
    }

    public void setRate(DTOIntRate rate) {
        this.rate = rate;
    }

    public String getNotificationsByOperation() {
        return notificationsByOperation;
    }

    public void setNotificationsByOperation(String notificationsByOperation) {
        this.notificationsByOperation = notificationsByOperation;
    }
}
package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanSimulation;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
public interface ISimulateCardInstallmentsPlanMapper {

    /**
     * Method that creates a DTO with the input data.
     *
     * @param cardId                     unique card identifier
     * @param installmentsPlanSimulation Object with the data of the card
     * @return Object with the data of input
     */
    DTOIntCardInstallmentsPlan mapIn(String cardId, InstallmentsPlanSimulation installmentsPlanSimulation);
}

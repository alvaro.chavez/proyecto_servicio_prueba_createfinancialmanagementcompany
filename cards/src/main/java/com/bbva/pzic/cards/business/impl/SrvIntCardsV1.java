package com.bbva.pzic.cards.business.impl;

import com.bbva.pzic.cards.business.ISrvIntCardsV1;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.ICardsDAOV1;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Entelgy
 */
@Component
public class SrvIntCardsV1 implements ISrvIntCardsV1 {

    private static final Log LOG = LogFactory.getLog(SrvIntCardsV1.class);

    private Validator validator;
    private ICardsDAOV1 cardsDAO;

    @Autowired
    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    @Autowired
    public void setCardsDAO(ICardsDAOV1 cardsDAO) {
        this.cardsDAO = cardsDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Proposal> listCardProposals(final InputListCardProposals input) {
        LOG.info("... Invoking method SrvIntCardsV1.listCardProposals ...");
        return cardsDAO.listCardProposals(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProposalCard createCardProposal(final InputCreateCardProposal input) {
        LOG.info("... Invoking method SrvIntCardsV1.createCardProposal ...");
        LOG.info("... Validating createCardProposal input parameter ...");
        validator.validate(input, ValidationGroup.CreateCardProposal.class);
        return cardsDAO.createCardProposal(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Proposal modifyCardProposal(final InputModifyCardProposal input) {
        LOG.info("... Invoking method SrvIntCardsV1.modifyCardProposal ...");
        LOG.info("... Validating modifyCardProposal input parameter ...");
        validator.validate(input, ValidationGroup.ModifyCardProposal.class);
        return cardsDAO.modifyCardProposal(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CardPost createCard(final InputCreateCard input) {
        LOG.info("... Invoking method SrvIntCardsV1.createCard ...");
        LOG.info("... Validating createCard input parameter ...");
        validator.validate(input, ValidationGroup.CreateCardV1.class);
        return cardsDAO.createCard(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Delivery createCardDelivery(final DTOIntDelivery dtoInt) {
        LOG.info("... Invoking method SrvIntCardsV1.createCardDelivery ...");
        LOG.info("... Validating createCardDelivery input parameter ...");
        validator.validate(dtoInt, ValidationGroup.CreateCardDelivery.class);
        return cardsDAO.createCardDelivery(dtoInt);
    }

    @Override
    public List<SecurityData> getCardSecurityData(final InputGetCardSecurityData input) {
        LOG.info("... Invoking method SrvIntCardsV1.getCardSecurityData ...");
        LOG.info("... Validating getCardSecurityData input parameter ...");
        validator.validate(input, ValidationGroup.GetCardSecurityDataV1.class);
        return cardsDAO.getCardSecurityData(input);
    }

    @Override
    public CancellationVerification getCardCancellationVerification(final InputGetCardCancellationVerification input) {
        LOG.info("... Invoking method SrvIntCardsV1.getCardCancellationVerification ...");
        LOG.info("... Validating getCardCancellationVerification input parameter ...");
        validator.validate(input, ValidationGroup.GetCardCancellationVerificationV1.class);
        return cardsDAO.getCardCancellationVerification(input);
    }

    @Override
    public SimulateTransactionRefund simulateCardTransactionTransactionRefund(final InputSimulateCardTransactionTransactionRefund input) {
        LOG.info("... Invoking method SrvIntCardsV1.simulateCardTransactionTransactionRefund ...");
        LOG.info("... Validating simulateCardTransactionTransactionRefund input parameter ...");
        validator.validate(input, ValidationGroup.SimulateCardTransactionTransactionRefund.class);
        return cardsDAO.simulateCardTransactionTransactionRefund(input);
    }

    @Override
    public ReimburseCardTransactionTransactionRefund reimburseCardTransactionTransactionRefund(InputReimburseCardTransactionTransactionRefund input) {
        LOG.info("... Invoking method SrvIntCardsV1.ReimburseCardTransactionTransactionRefund ...");
        LOG.info("... Validating ReimburseCardTransactionTransactionRefund input parameter ...");
        validator.validate(input, ValidationGroup.ReimburseCardTransactionTransactionRefund.class);
        return cardsDAO.reimburseCardTransactionTransactionRefund(input);
    }

    @Override
    public FinancialStatement getCardFinancialStatement(final InputGetCardFinancialStatement input) {
        LOG.info("... Invoking method SrvIntCardsV1.getCardFinancialStatement ...");
        LOG.info("... Validating getCardFinancialStatement input parameter ...");
        validator.validate(input, ValidationGroup.GetCardFinancialStatementV1.class);
        return cardsDAO.getCardFinancialStatement(input);
    }

    @Override
    public RequestCancel createCardCancelRequest(final InputCreateCardCancelRequest input) {
        LOG.info("... Invoking method SrvIntCardsV1.createCardCancelRequest ...");
        LOG.info("... Validating createCardCancelRequest input parameter ...");
        validator.validate(input, ValidationGroup.CreateCardCancelRequestV1.class);
        return cardsDAO.createCardCancelRequest(input);
    }

    @Override
    public Cancellation createCardCancellations(final InputCreateCardCancellations input) {
        LOG.info("... Invoking method SrvIntCardsV1.createCardCancellations ...");
        LOG.info("... Validating createCardCancellations input parameter ...");
        validator.validate(input, ValidationGroup.CreateCardCancellationsV1.class);
        return cardsDAO.createCardCancellations(input);
    }
}

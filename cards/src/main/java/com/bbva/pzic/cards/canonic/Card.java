package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Card", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Card", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Card implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Card identifier.
     */
    @DatoAuditable(omitir = true)
    private String cardId;
    /**
     * Number of the card. DISCLAIMER: This field is used, in the payload
     * request for the creation in case the card would be an innominate debit
     * card and the person who makes the request(creation, replacement or
     * reposition) of the card has the full number of plastic card(PAN).
     */
    @DatoAuditable(omitir = true)
    private String number;
    /**
     * Card number type
     */
    private NumberType numberType;
    /**
     * Financial product type.
     */
    private CardType cardType;
    /**
     * Financial product title.
     */
    private Title title;
    /**
     * Card alias. This attribute allows customers to assign a custom name to
     * their cards.
     */
    private String alias;
    /**
     * It indicates if the card belongs to a business or retail customer.
     */
    private Boolean isBusiness;
    /**
     * Commercial brand associated to the card.
     */
    private BrandAssociation brandAssociation;
    /**
     * Physical support type associated to the card.
     */
    private PhysicalSupport physicalSupport;
    /**
     * Identifies the package to which the card belongs. A package brings
     * together different products that customers contract together. For
     * example: "Libreton Full", that groups credit card, savings account and
     * checking account
     */
    private CommercialPackage commercialPackage;
    /**
     * String based on ISO-8601 for specifying the date when the card will
     * expire. For normal plastic cards, the embossed date is usually this
     * expiration date without the day (YYYY-MM).
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    @DatoAuditable(omitir = true)
    private Date expirationDate;
    /**
     * Card holder. This name matches the printed name on the card.
     */
    @DatoAuditable(omitir = true)
    private String holderName;
    /**
     * Currencies related to the card. Monetary amounts related to the card may
     * be provided in one or many of these currencies.
     */
    private List<Currency> currencies;
    /**
     * Granted credit. This amount may be provided in several currencies
     * (depending on the country). This attribute is mandatory for credit cards.
     */
    private List<GrantedCredit> grantedCredits;
    /**
     * This balance means:<br>
     * <ul>
     * <li>Credit cards: Difference between granted credit and spent amount.</li>
     * <li>Debit cards: Available amount in the related contract.</li>
     * <li>Prepaid cards: Remaining loaded amount.</li>
     * </ul>
     */
    private AvailableBalance availableBalance;
    /**
     * This balance is the total spent amount from granted credit. This
     * attribute is mandatory for credit cards.
     */
    private DisposedBalance disposedBalance;
    /**
     * Card current status.
     */
    private Status status;
    /**
     * List of images printed on the card.
     */
    private List<Image> images;
    /**
     * Entity associated to card.
     */
    private BankType bank;
    /**
     * String based on ISO-8601 date format for specifying the date when the
     * current credit card period ends and the next one starts. All the
     * transactions performed until this date will be added to the next payment
     * term. This date is calculated based on the cutOffDay which is selected by
     * the customer at the moment of contract the credit card.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date cutOffDate;
    /**
     * String based on ISO-8601 timestamp format for specifying the date when
     * the card was created.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar openingDate;
    /**
     * String based on ISO-8601 date format for specifying the date when the
     * card was activated. Fore those cards whose delivered PIN is temporary, if
     * the card has not been activated but the PIN has already been delivered,
     * this date means the date until delivered PIN is valid.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date activationDate;
    /**
     * String based on ISO-8601 date format for specifying the date when the
     * card was issued for first time.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar issueDate;
    /**
     * List of contracts associated to the card. Note - If the card type is
     * CREDIT_CARD or DEBIT_CARD the field is required.
     */
    private List<RelatedContract> relatedContracts;
    /**
     * Image selected for the customer to be printed on the card.
     */
    private Image image;
    /**
     * Card delivery details for a physical card.
     */
    private Delivery delivery;
    /**
     * Required for business accounts. On a business card the participantId will
     * be the authorized person that will hold the card. It is required to
     * specify AUTHORIZED as participant type also.
     */
    private List<Participant> participants;
    /**
     * Required for business accounts. Legal business name, DBA name or
     * customized name to use on card. Note - The company name will be truncated
     * after 20 characters. Use LEGAL if uncertain.
     */
    private CompanyEmbossingNameType companyEmbossingNameType;
    /**
     * Required for business accounts. Company name that will be embossed on the
     * card. Only applies to CUSTOMIZED types.
     */
    private String customCompanyEmbossingName;
    /**
     * Payment method related to a specific credit card.
     */
    private PaymentMethod paymentMethod;
    /**
     * Number of the operation, when the credit card was generated.
     */
    private String operationNumber;
    /**
     * String based on the ISO-8601 time stamp format to specify the operation
     * date.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar operationDate;
    /**
     * Unique offer associated with the card.
     */
    private String offerId;
    /**
     * The day of the month when the current credit card period ends and the
     * next one starts. All the transactions performed until this day will be
     * added to the next payment term. DISCLAIMER: This field is only used when
     * in the contracting process the customer can negotiate and provide
     * alternative cut off days.
     */
    private Integer cutOffDay;
    /**
     * Indicates the type of card statement that will be send to the customer.
     */
    private String statementType;
    /**
     * Indicates the type of contract support where customer acceptance is
     * received.
     */
    private String supportContractType;
    /**
     * Memberships associated to the card.
     */
    private List<Membership> memberships;
    /**
     * Specifies whether the commission should be charged.
     */
    private Boolean hasFees;
    /**
     * Card contract identifier. It´s the identifier of the card contract
     * containing the card. A card contract can contain multiple cards, with
     * different PAN numbers.
     */
    private String contractId;
    /**
     * Numeric value that indicates the sequential position of the card in the
     * card contract.
     */
    private String dependencyValue;
    /**
     * Identifier associated to the card.
     */
    private String id;
    /**
     * Financial product associated to the contract. In this case only CARDS is
     * allowed
     */
    private Product product;
    /**
     * Membership associated to the card.
     */
    private Membership membership;

    private List<Block> blocks;

    private List<Activation> activations;

    private List<Condition> conditions;

    private List<PaymentMethod> paymentMethods;
    /**
     * Details of delivery of products or documents associated to the registration of a card.
     */
    private List<DeliveriesManagement> deliveriesManagement;
    /**
     * Information that corresponds to the person registering the card.
     */
    private ContractingBusinessAgent contractingBusinessAgent;
    /**
     * Information that corresponds to a seller of card.
     */
    private MarketBusinessAgent marketBusinessAgent;
    /**
     * Managing office of the contract. To identify the operating office where the contract is managed
     */
    private ManagementBranch managementBranch;
    /**
     * Center with which the card contract will be registered.
     */
    private ContractingBranch contractingBranch;
    /**
     * Rate charge applied to the type of offer associated.
     */
    private List<Rate> rates;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public NumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(NumberType numberType) {
        this.numberType = numberType;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public BrandAssociation getBrandAssociation() {
        return brandAssociation;
    }

    public void setBrandAssociation(BrandAssociation brandAssociation) {
        this.brandAssociation = brandAssociation;
    }

    public Boolean getIsBusiness() {
        return isBusiness;
    }

    public void setIsBusiness(Boolean isBusiness) {
        this.isBusiness = isBusiness;
    }

    public PhysicalSupport getPhysicalSupport() {
        return physicalSupport;
    }

    public void setPhysicalSupport(PhysicalSupport physicalSupport) {
        this.physicalSupport = physicalSupport;
    }

    public CommercialPackage getCommercialPackage() {
        return commercialPackage;
    }

    public void setCommercialPackage(CommercialPackage commercialPackage) {
        this.commercialPackage = commercialPackage;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public List<GrantedCredit> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<GrantedCredit> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public AvailableBalance getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(AvailableBalance availableBalance) {
        this.availableBalance = availableBalance;
    }

    public DisposedBalance getDisposedBalance() {
        return disposedBalance;
    }

    public void setDisposedBalance(DisposedBalance disposedBalance) {
        this.disposedBalance = disposedBalance;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public BankType getBank() {
        return bank;
    }

    public void setBank(BankType bank) {
        this.bank = bank;
    }

    public Date getCutOffDate() {
        return cutOffDate;
    }

    public void setCutOffDate(Date cutOffDate) {
        this.cutOffDate = cutOffDate;
    }

    public Calendar getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Calendar openingDate) {
        this.openingDate = openingDate;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Calendar getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Calendar issueDate) {
        this.issueDate = issueDate;
    }

    public List<RelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<RelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public CompanyEmbossingNameType getCompanyEmbossingNameType() {
        return companyEmbossingNameType;
    }

    public void setCompanyEmbossingNameType(
            CompanyEmbossingNameType companyEmbossingNameType) {
        this.companyEmbossingNameType = companyEmbossingNameType;
    }

    public String getCustomCompanyEmbossingName() {
        return customCompanyEmbossingName;
    }

    public void setCustomCompanyEmbossingName(String customCompanyEmbossingName) {
        this.customCompanyEmbossingName = customCompanyEmbossingName;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public Calendar getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Calendar operationDate) {
        this.operationDate = operationDate;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public Integer getCutOffDay() {
        return cutOffDay;
    }

    public void setCutOffDay(Integer cutOffDay) {
        this.cutOffDay = cutOffDay;
    }

    public String getStatementType() {
        return statementType;
    }

    public void setStatementType(String statementType) {
        this.statementType = statementType;
    }

    public String getSupportContractType() {
        return supportContractType;
    }

    public void setSupportContractType(String supportContractType) {
        this.supportContractType = supportContractType;
    }

    public List<Membership> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<Membership> memberships) {
        this.memberships = memberships;
    }

    public Boolean getHasFees() {
        return hasFees;
    }

    public void setHasFees(Boolean hasFees) {
        this.hasFees = hasFees;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getDependencyValue() {
        return dependencyValue;
    }

    public void setDependencyValue(String dependencyValue) {
        this.dependencyValue = dependencyValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<Block> blocks) {
        this.blocks = blocks;
    }

    public List<Activation> getActivations() {
        return activations;
    }

    public void setActivations(List<Activation> activations) {
        this.activations = activations;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public List<DeliveriesManagement> getDeliveriesManagement() {
        return deliveriesManagement;
    }

    public void setDeliveriesManagement(List<DeliveriesManagement> deliveriesManagement) {
        this.deliveriesManagement = deliveriesManagement;
    }

    public ContractingBusinessAgent getContractingBusinessAgent() {
        return contractingBusinessAgent;
    }

    public void setContractingBusinessAgent(ContractingBusinessAgent contractingBusinessAgent) {
        this.contractingBusinessAgent = contractingBusinessAgent;
    }

    public MarketBusinessAgent getMarketBusinessAgent() {
        return marketBusinessAgent;
    }

    public void setMarketBusinessAgent(MarketBusinessAgent marketBusinessAgent) {
        this.marketBusinessAgent = marketBusinessAgent;
    }

    public ManagementBranch getManagementBranch() {
        return managementBranch;
    }

    public void setManagementBranch(ManagementBranch managementBranch) {
        this.managementBranch = managementBranch;
    }

    public ContractingBranch getContractingBranch() {
        return contractingBranch;
    }

    public void setContractingBranch(ContractingBranch contractingBranch) {
        this.contractingBranch = contractingBranch;
    }

    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }
}
package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTECKB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTS1KB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTS2KB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTSCKB89;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardsProposalMapper;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.Converter;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang.StringUtils.isEmpty;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
@Component
public class TxCreateCardsProposalMapper implements ITxCreateCardsProposalMapper {

    private static final Log LOG = LogFactory.getLog(TxCreateCardsProposalMapper.class);

    private Translator translator;

    private AbstractCypherTool cypherTool;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Autowired
    public void setCypherTool(AbstractCypherTool cypherTool) {
        this.cypherTool = cypherTool;
    }

    @Override
    public FormatoKTECKB89 mapIn(final DTOIntProposal dtoIn) {
        LOG.info("... called method TxCreateCardsProposalMapper.mapIn ...");
        FormatoKTECKB89 formatoKTECKB89 = new FormatoKTECKB89();
        formatoKTECKB89.setIdtipta(dtoIn.getCardType() == null ? null : dtoIn.getCardType().getId());
        mapInTitle(formatoKTECKB89, dtoIn.getTitle());
        mapInPaymentMethod(formatoKTECKB89, dtoIn.getPaymentMethod());
        mapInGrantedCredit(formatoKTECKB89, dtoIn.getGrantedCredits());
        mapInDelivery(formatoKTECKB89, dtoIn.getDelivery());
        mapInBank(formatoKTECKB89, dtoIn.getBank());
        mapInFee(formatoKTECKB89, dtoIn.getFees());
        mapInRates(formatoKTECKB89, dtoIn.getRates());
        mapInAdditionalProducts(formatoKTECKB89, dtoIn.getAdditionalProducts());
        mapInMembership(formatoKTECKB89, dtoIn.getMembership());
        formatoKTECKB89.setIdcelul(dtoIn.getContactDetailsId0());
        formatoKTECKB89.setEcenvio(dtoIn.getContactDetailsId1());
        formatoKTECKB89.setIdofert(dtoIn.getOfferId());
        formatoKTECKB89.setIndnotf(Converter.booleanStringToString(dtoIn.getNotificationsByOperation()));
        return formatoKTECKB89;
    }

    private void mapInTitle(final FormatoKTECKB89 formatoKTECKB89, final DTOIntTitle title) {
        if (title == null) {
            return;
        }
        formatoKTECKB89.setIdprodu(title.getId());
        formatoKTECKB89.setIdsprod(title.getSubproduct() == null ? null : title.getSubproduct().getId());
    }

    private void mapInPaymentMethod(final FormatoKTECKB89 formatoKTECKB89, final DTOIntPaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            return;
        }
        formatoKTECKB89.setIdtpago(paymentMethod.getId());
        formatoKTECKB89.setIdperio(paymentMethod.getPeriod() == null ? null : paymentMethod.getPeriod().getId());
        formatoKTECKB89.setDiapago(paymentMethod.getEndDay());
    }

    private void mapInGrantedCredit(final FormatoKTECKB89 formatoKTECKB89, final List<DTOIntImport> grantedCredits) {
        if (CollectionUtils.isEmpty(grantedCredits)
                || grantedCredits.get(0) == null) {
            return;
        }
        formatoKTECKB89.setMonline(grantedCredits.get(0).getAmount());
        formatoKTECKB89.setDivisal(grantedCredits.get(0).getCurrency());
    }

    private void mapInDelivery(final FormatoKTECKB89 formatoKTECKB89, final DTOIntDelivery delivery) {
        if (delivery == null || delivery.getDestination() == null) {
            return;
        }
        formatoKTECKB89.setIddesti(delivery.getDestination().getId());
    }

    private void mapInBank(final FormatoKTECKB89 formatoKTECKB89, final DTOIntBankType bank) {
        if (bank == null) {
            return;
        }
        formatoKTECKB89.setIdbanco(bank.getId());
        formatoKTECKB89.setIdofici(bank.getBranch() == null ? null : bank.getBranch().getId());
    }

    private void mapInFee(final FormatoKTECKB89 formatoKTECKB89, final DTOIntFee fee) {
        if (fee == null
                || CollectionUtils.isEmpty(fee.getItemizeFees())) {
            return;
        }
        if (fee.getItemizeFees().get(0) != null) {
            formatoKTECKB89.setIdcoms1(fee.getItemizeFees().get(0).getFeeType());
            formatoKTECKB89.setNomcom1(fee.getItemizeFees().get(0).getName());
            formatoKTECKB89.setTipcomi(fee.getItemizeFees().get(0).getMode() == null ? null
                    : fee.getItemizeFees().get(0).getMode().getId());
            if (fee.getItemizeFees().get(0).getItemizeFeeUnit() != null) {
                formatoKTECKB89.setImpcom1(fee.getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
                formatoKTECKB89.setDivisal(fee.getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
            }
        }
        if (fee.getItemizeFees().size() > 1 && fee.getItemizeFees().get(1) != null) {
            formatoKTECKB89.setIdcoms2(fee.getItemizeFees().get(1).getFeeType());
            formatoKTECKB89.setNomcom2(fee.getItemizeFees().get(1).getName());
            formatoKTECKB89.setTipcomi(fee.getItemizeFees().get(1).getMode() == null ? null
                    : fee.getItemizeFees().get(1).getMode().getId());
            if (fee.getItemizeFees().get(1).getItemizeFeeUnit() != null) {
                formatoKTECKB89.setImpcom2(fee.getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
                formatoKTECKB89.setDivisal(fee.getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
            }
        }
    }

    private void mapInRates(final FormatoKTECKB89 formatoKTECKB89, final List<DTOIntRate> rates) {
        if (CollectionUtils.isEmpty(rates)) {
            return;
        }
        if (rates.get(0) != null) {
            formatoKTECKB89.setIdtstea(rates.get(0).getRateType() == null ? null : rates.get(0).getRateType().getId());
            formatoKTECKB89.setFectasa(rates.get(0).getCalculationDate());
            formatoKTECKB89.setTiptasa(rates.get(0).getModeid());
            formatoKTECKB89.setTasatea(rates.get(0).getUnit() == null ? null : rates.get(0).getUnit().getPercentage());
        }
        if (rates.size() > 1 && rates.get(1) != null) {
            formatoKTECKB89.setIdttcea(rates.get(1).getRateType() == null ? null : rates.get(1).getRateType().getId());
            formatoKTECKB89.setFectasa(rates.get(1).getCalculationDate());
            formatoKTECKB89.setTiptasa(rates.get(1).getModeid());
            formatoKTECKB89.setTastcea(rates.get(1).getUnit() == null ? null : rates.get(1).getUnit().getPercentage());
        }
    }

    private void mapInAdditionalProducts(final FormatoKTECKB89 formatoKTECKB89, final List<DTOIntAdditionalProduct> additionalProducts) {
        if (CollectionUtils.isEmpty(additionalProducts)
                || additionalProducts.get(0) == null) {
            return;
        }
        formatoKTECKB89.setProduct(additionalProducts.get(0).getProductType());
        formatoKTECKB89.setImpprod(additionalProducts.get(0).getAmount());
        formatoKTECKB89.setDivisal(additionalProducts.get(0).getCurrency());
    }

    private void mapInMembership(final FormatoKTECKB89 formatoKTECKB89, final DTOIntMembership membership) {
        if (membership == null) {
            return;
        }
        formatoKTECKB89.setIdpmemb(membership.getId());
        formatoKTECKB89.setNumpmem(membership.getNumber());
    }

    @Override
    public Proposal mapOut(final FormatoKTSCKB89 formatOutput) {
        LOG.info("... called method TxCreateCardsProposalMapper.mapOut ...");
        Proposal proposal = new Proposal();
        proposal.setId(formatOutput.getIdsolic());
        proposal.setDelivery(mapOutDelivery(formatOutput));
        proposal.setPaymentMethod(mapOutPaymentMethod(formatOutput));
        proposal.setGrantedCredits(mapOutGrantedCredits(formatOutput));
        proposal.setAdditionalProducts(mapOutAdditionalProducts(formatOutput));
        proposal.setMembership(mapOutMembership(formatOutput));
        proposal.setContactDetails(mapOutContactDetails(formatOutput));
        proposal.setOperationNumber(formatOutput.getIdsolic());
        proposal.setNotificationsByOperation(Converter.stringToBooleanToString(formatOutput.getIndnotf()));

        try {
            if (formatOutput.getFecope() != null && !StringUtils.isEmpty(formatOutput.getHorope())) {
                proposal.setOperationDate(DateUtils.toDateTime(formatOutput.getFecope(), formatOutput.getHorope()));
            }
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }

        return proposal;
    }

    private Delivery mapOutDelivery(final FormatoKTSCKB89 formatOutput) {
        if (isEmpty(formatOutput.getIddesti()) && isEmpty(formatOutput.getNomdest())) {
            return null;
        }
        Delivery delivery = new Delivery();
        Destination destination = new Destination();
        destination.setId(translator.translateBackendEnumValueStrictly(
                "cards.delivery.destionation.id", formatOutput.getIddesti()));
        destination.setName(formatOutput.getNomdest());
        delivery.setDestination(destination);
        return delivery;
    }

    private PaymentMethod mapOutPaymentMethod(final FormatoKTSCKB89 formatOutput) {
        if (isEmpty(formatOutput.getIdtpago())
                && isEmpty(formatOutput.getNompago())
                && isEmpty(formatOutput.getIdperio())
                && isEmpty(formatOutput.getNomperi())
                && formatOutput.getDiapago() == null) {
            return null;
        }
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(translator
                .translateBackendEnumValueStrictly("cards.paymentMethod.id", formatOutput.getIdtpago()));
        paymentMethod.setName(formatOutput.getNompago());
        paymentMethod.setPeriod(mapOutPeriod(formatOutput));
        paymentMethod.setEndDay(formatOutput.getDiapago());
        return paymentMethod;
    }

    private Period mapOutPeriod(final FormatoKTSCKB89 formatOutput) {
        if (isEmpty(formatOutput.getIdperio()) && isEmpty(formatOutput.getNomperi())) {
            return null;
        }
        Period period = new Period();
        period.setId(translator.translateBackendEnumValueStrictly(
                "cards.period.id", formatOutput.getIdperio()));
        period.setName(formatOutput.getNomperi());
        return period;
    }

    private List<Import> mapOutGrantedCredits(final FormatoKTSCKB89 formatOutput) {
        if (formatOutput.getMonline() == null && isEmpty(formatOutput.getDivisal())) {
            return null;
        }
        List<Import> grantedCredits = new ArrayList<>();
        Import grantedCredit = new Import();
        grantedCredit.setAmount(formatOutput.getMonline());
        grantedCredit.setCurrency(formatOutput.getDivisal());
        grantedCredits.add(grantedCredit);
        return grantedCredits;
    }

    private List<AdditionalProduct> mapOutAdditionalProducts(final FormatoKTSCKB89 formatOutput) {
        if (isEmpty(formatOutput.getProduct())
                && formatOutput.getImpprod() == null
                && isEmpty(formatOutput.getDivisal())) {
            return null;
        }
        List<AdditionalProduct> additionalProducts = new ArrayList<>();
        AdditionalProduct additionalProduct = new AdditionalProduct();
        additionalProduct.setProductType(translator.translateBackendEnumValueStrictly(
                "cards.offers.additionalProducts.productType", formatOutput.getProduct()));
        additionalProduct.setAmount(formatOutput.getImpprod());
        additionalProduct.setCurrency(formatOutput.getDivisal());
        additionalProducts.add(additionalProduct);
        return additionalProducts;
    }

    private Membership mapOutMembership(final FormatoKTSCKB89 formatOutput) {
        if (isEmpty(formatOutput.getIdpmemb())
                && isEmpty(formatOutput.getNompmeb())
                && isEmpty(formatOutput.getNumpmem())) {
            return null;
        }
        Membership membership = new Membership();
        membership.setId(formatOutput.getIdpmemb());
        membership.setDescription(formatOutput.getNompmeb());
        membership.setNumber(formatOutput.getNumpmem());
        return membership;
    }

    private List<ContactDetail> mapOutContactDetails(final FormatoKTSCKB89 formatOutput) {
        if (isEmpty(formatOutput.getIdcelul()) && isEmpty(formatOutput.getEcenvio())) {
            return null;
        }
        List<ContactDetail> contactDetails = new ArrayList<>();
        ContactDetail contactDetail = new ContactDetail();
        contactDetail.setId(cypherTool.encrypt(formatOutput.getIdcelul(), AbstractCypherTool.CONTACT_DETAIL_ID, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL));
        contactDetails.add(contactDetail);
        contactDetail.setId(cypherTool.encrypt(formatOutput.getEcenvio(), AbstractCypherTool.CONTACT_DETAIL_ID, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL));
        contactDetails.add(contactDetail);
        return contactDetails;
    }

    @Override
    public Proposal mapOut2(final FormatoKTS1KB89 formatOutput, Proposal dtoOut) {
        LOG.info("... called method TxCreateCardsProposalMapper.mapOut2 ...");
        if (dtoOut == null) {
            dtoOut = new Proposal();
        }
        Rate rate = new Rate();
        if (StringUtils.isNotEmpty(formatOutput.getIdttasa())
                || StringUtils.isNotEmpty(formatOutput.getNomtasa())) {
            rate.setRateType(new RateType());
            rate.getRateType().setId(formatOutput.getIdttasa());
            rate.getRateType().setName(formatOutput.getNomtasa());
        }
        rate.setCalculationDate(formatOutput.getFectasa());

        if (StringUtils.isNotEmpty(formatOutput.getTiptasa())
                || StringUtils.isNotEmpty(formatOutput.getNomtipt())) {
            rate.setMode(new Mode());
            rate.getMode().setId(translator
                    .translateBackendEnumValueStrictly("cards.rates.mode", formatOutput.getTiptasa()));
            rate.getMode().setName(formatOutput.getNomtipt());
        }

        if (formatOutput.getPortasa() != null
                && formatOutput.getPortasa().compareTo(BigDecimal.ZERO) > 0) {
            rate.setUnit(new Percentage());
            rate.getUnit().setPercentage(formatOutput.getPortasa());
        }

        if (CollectionUtils.isEmpty(dtoOut.getRates())) {
            dtoOut.setRates(new ArrayList<>());
        }
        dtoOut.getRates().add(rate);
        return dtoOut;
    }

    @Override
    public Proposal mapOut3(final FormatoKTS2KB89 formatOutput, Proposal dtoOut) {
        LOG.info("... called method TxCreateCardsProposalMapper.mapOut3 ...");
        if (dtoOut == null) {
            dtoOut = new Proposal();
        }

        if (dtoOut.getFees() == null) {
            dtoOut.setFees(new Fee());
        }

        ItemizeFee itemizeFee = new ItemizeFee();
        itemizeFee.setFeeType(translator
                .translateBackendEnumValueStrictly("cards.itemizeFees.feeType", formatOutput.getIdcomim()));
        itemizeFee.setName(formatOutput.getNomcomi());

        if (StringUtils.isNotEmpty(formatOutput.getTipcomi())
                || StringUtils.isNotEmpty(formatOutput.getNomtipc())) {
            itemizeFee.setMode(new Mode());
            itemizeFee.getMode().setId(translator
                    .translateBackendEnumValueStrictly("cards.itemizeFees.mode", formatOutput.getTipcomi()));
            itemizeFee.getMode().setName(formatOutput.getNomtipc());
        }

        if (formatOutput.getImpcomi() != null
                && formatOutput.getImpcomi().compareTo(BigDecimal.ZERO) > 0) {
            itemizeFee.setItemizeFeeUnit(new ItemizeFeeUnit());
            itemizeFee.getItemizeFeeUnit().setAmount(formatOutput.getImpcomi());
            if (CollectionUtils.isNotEmpty(dtoOut.getGrantedCredits())
                    && dtoOut.getGrantedCredits().get(0) != null) {
                itemizeFee.getItemizeFeeUnit().setCurrency(dtoOut.getGrantedCredits().get(0).getCurrency());
            }

        }

        if (CollectionUtils.isEmpty(dtoOut.getFees().getItemizeFees())) {
            dtoOut.getFees().setItemizeFees(new ArrayList<>());
        }
        dtoOut.getFees().getItemizeFees().add(itemizeFee);

        return dtoOut;
    }


}
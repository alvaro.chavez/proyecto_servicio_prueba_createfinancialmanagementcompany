package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.business.dto.DTOIntDetailSimulation;
import com.bbva.pzic.cards.business.dto.DTOIntProduct;
import com.bbva.pzic.cards.business.dto.DTOIntSubProduct;
import com.bbva.pzic.cards.canonic.HolderSimulation;
import com.bbva.pzic.cards.canonic.Product;
import com.bbva.pzic.cards.canonic.SubProduct;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardsOfferSimulateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext.ASTA_MX_CLIENT_ID;

@Component
public class CreateCardsOfferSimulateMapper implements ICreateCardsOfferSimulateMapper {

    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    public void setServiceInvocationContext(ServiceInvocationContext serviceInvocationContext) {
        this.serviceInvocationContext = serviceInvocationContext;
    }

    @Override
    public DTOIntDetailSimulation mapIn(final String offerId, final HolderSimulation holderSimulation) {
        if (holderSimulation.getDetails() == null) {
            return null;
        }

        DTOIntDetailSimulation dtoInt = new DTOIntDetailSimulation();
        dtoInt.setOfferId(offerId);
        dtoInt.setClientId(serviceInvocationContext.getProperty(ASTA_MX_CLIENT_ID));
        dtoInt.setProduct(mapInProduct(holderSimulation.getDetails().getProduct()));
        return dtoInt;
    }

    private DTOIntProduct mapInProduct(final Product product) {
        if (product == null) {
            return null;
        }
        DTOIntProduct dtoIntProduct = new DTOIntProduct();
        dtoIntProduct.setId(product.getId());
        dtoIntProduct.setSubproduct(mapInSubProduct(product.getSubproduct()));
        return dtoIntProduct;
    }

    private DTOIntSubProduct mapInSubProduct(final SubProduct subProduct) {
        if (subProduct == null) {
            return null;
        }
        DTOIntSubProduct dtoIntSubProduct = new DTOIntSubProduct();
        dtoIntSubProduct.setId(subProduct.getId());
        return dtoIntSubProduct;
    }

    @Override
    public ServiceResponseOK<HolderSimulation> mapOut(final HolderSimulation holderSimulation) {
        if (holderSimulation == null) {
            return null;
        }
        return ServiceResponseOK.data(holderSimulation).build();
    }
}

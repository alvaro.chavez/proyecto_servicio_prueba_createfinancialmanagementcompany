package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "destination", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "destination", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Destination implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier of the destination type of the card delivery.
     */
    private String id;
    /**
     * Name of destination type of card delivery.
     */
    private String name;
    /**
     * Branch where the customer collects the card. It should be informed when
     * the destination is BRANCH.
     */
    private Branch branch;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}

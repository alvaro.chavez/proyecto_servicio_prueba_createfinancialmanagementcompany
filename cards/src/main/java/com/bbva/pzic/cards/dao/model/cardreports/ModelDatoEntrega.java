package com.bbva.pzic.cards.dao.model.cardreports;

public class ModelDatoEntrega {

    private String id;

    private ModelTipoDatoEntrega tipo;

    private ModelDestino destino;

    private ModelOficina oficina;

    private ModelContacto contacto;

    private ModelDireccion direccion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModelTipoDatoEntrega getTipo() {
        return tipo;
    }

    public void setTipo(ModelTipoDatoEntrega tipo) {
        this.tipo = tipo;
    }

    public ModelDestino getDestino() {
        return destino;
    }

    public void setDestino(ModelDestino destino) {
        this.destino = destino;
    }

    public ModelOficina getOficina() {
        return oficina;
    }

    public void setOficina(ModelOficina oficina) {
        this.oficina = oficina;
    }

    public ModelContacto getContacto() {
        return contacto;
    }

    public void setContacto(ModelContacto contacto) {
        this.contacto = contacto;
    }

    public ModelDireccion getDireccion() {
        return direccion;
    }

    public void setDireccion(ModelDireccion direccion) {
        this.direccion = direccion;
    }
}

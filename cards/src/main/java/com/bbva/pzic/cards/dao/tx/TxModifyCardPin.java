package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntPin;
import com.bbva.pzic.cards.dao.model.mpwp.FormatoMPMENWP;
import com.bbva.pzic.cards.dao.model.mpwp.PeticionTransaccionMpwp;
import com.bbva.pzic.cards.dao.model.mpwp.RespuestaTransaccionMpwp;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardPinMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.NoneOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created on 3/10/2017.
 *
 * @author Entelgy
 */
@Component("txModifyCardPin")
public class TxModifyCardPin
        extends NoneOutputFormat<DTOIntPin, FormatoMPMENWP> {

    @Autowired
    private ITxModifyCardPinMapper txModifyCardPinMapper;

    @Autowired
    public TxModifyCardPin(@Qualifier("transaccionMpwp") InvocadorTransaccion<PeticionTransaccionMpwp, RespuestaTransaccionMpwp> transaction) {
        super(transaction, PeticionTransaccionMpwp::new);
    }

    @Override
    protected FormatoMPMENWP mapInput(final DTOIntPin dtoIntPin) {
        return txModifyCardPinMapper.mapInput(dtoIntPin);
    }
}

package com.bbva.pzic.cards.dao.model.mpg1;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>MPMS1G1</code> de la transacci&oacute;n <code>MPG1</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS1G1")
@RooJavaBean
@RooSerializable
public class FormatoMPMS1G1 {
	
	/**
	 * <p>Campo <code>IDETARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDETARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	private String idetarj;
	
	/**
	 * <p>Campo <code>NUMTARJ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NUMTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	private String numtarj;
	
	/**
	 * <p>Campo <code>IDTNUCO</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "IDTNUCO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtnuco;
	
	/**
	 * <p>Campo <code>DETNUCO</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "DETNUCO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String detnuco;
	
	/**
	 * <p>Campo <code>TIPPROD</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "TIPPROD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String tipprod;
	
	/**
	 * <p>Campo <code>DESCPRO</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "DESCPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String descpro;
	
	/**
	 * <p>Campo <code>DDISPUE</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "DDISPUE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String ddispue;
	
	/**
	 * <p>Campo <code>DDISPUB</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "DDISPUB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String ddispub;
	
	/**
	 * <p>Campo <code>IDIMGT</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "IDIMGT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idimgt;
	
	/**
	 * <p>Campo <code>NIMGTA</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "NIMGTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String nimgta;
	
	/**
	 * <p>Campo <code>IMGTAR1</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "IMGTAR1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 143, longitudMaxima = 143)
	private String imgtar1;
	
	/**
	 * <p>Campo <code>ESTARJO</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "ESTARJO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String estarjo;
	
	/**
	 * <p>Campo <code>DESTARJ</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "DESTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String destarj;
	
	/**
	 * <p>Campo <code>LIMCRE</code>, &iacute;ndice: <code>14</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 14, nombre = "LIMCRE", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal limcre;
	
	/**
	 * <p>Campo <code>MONTAR</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "MONTAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String montar;
	
	/**
	 * <p>Campo <code>SALDISP</code>, &iacute;ndice: <code>16</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 16, nombre = "SALDISP", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal saldisp;
	
	/**
	 * <p>Campo <code>SALUSAD</code>, &iacute;ndice: <code>17</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 17, nombre = "SALUSAD", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal salusad;
	
	/**
	 * <p>Campo <code>MONUSAD</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "MONUSAD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String monusad;
	
	/**
	 * <p>Campo <code>TIPTARJ</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "TIPTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tiptarj;
	
	/**
	 * <p>Campo <code>DTITARJ</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "DTITARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String dtitarj;
	
	/**
	 * <p>Campo <code>FECVENC</code>, &iacute;ndice: <code>21</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 21, nombre = "FECVENC", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecvenc;
	
	/**
	 * <p>Campo <code>FECENT</code>, &iacute;ndice: <code>22</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 22, nombre = "FECENT", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecent;
	
	/**
	 * <p>Campo <code>NOMTIT</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 23, nombre = "NOMTIT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String nomtit;
	
	/**
	 * <p>Campo <code>FECORTE</code>, &iacute;ndice: <code>24</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 24, nombre = "FECORTE", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecorte;
	
	/**
	 * <p>Campo <code>NUOFGES</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 25, nombre = "NUOFGES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String nuofges;
	
	/**
	 * <p>Campo <code>DEOFGES</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "DEOFGES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String deofges;
	
	/**
	 * <p>Campo <code>CODOFBA</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 27, nombre = "CODOFBA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String codofba;
	
	/**
	 * <p>Campo <code>DEOFBAN</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 28, nombre = "DEOFBAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 50, longitudMaxima = 50)
	private String deofban;
	
	/**
	 * <p>Campo <code>FECSITT</code>, &iacute;ndice: <code>29</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 29, nombre = "FECSITT", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecsitt;
	
	/**
	 * <p>Campo <code>IDBENEF</code>, &iacute;ndice: <code>30</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 30, nombre = "IDBENEF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String idbenef;
	
	/**
	 * <p>Campo <code>DESBENF</code>, &iacute;ndice: <code>31</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 31, nombre = "DESBENF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String desbenf;
	
	/**
	 * <p>Campo <code>IDIMGT2</code>, &iacute;ndice: <code>32</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 32, nombre = "IDIMGT2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idimgt2;
	
	/**
	 * <p>Campo <code>NIMGTA2</code>, &iacute;ndice: <code>33</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 33, nombre = "NIMGTA2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String nimgta2;
	
	/**
	 * <p>Campo <code>IMGTAR2</code>, &iacute;ndice: <code>34</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 34, nombre = "IMGTAR2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 143, longitudMaxima = 143)
	private String imgtar2;
	
	/**
	 * <p>Campo <code>IDMATAR</code>, &iacute;ndice: <code>35</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 35, nombre = "IDMATAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idmatar;
	
	/**
	 * <p>Campo <code>NOMATAR</code>, &iacute;ndice: <code>36</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 36, nombre = "NOMATAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nomatar;
	
	/**
	 * <p>Campo <code>INDEMPR</code>, &iacute;ndice: <code>37</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 37, nombre = "INDEMPR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indempr;
	
}
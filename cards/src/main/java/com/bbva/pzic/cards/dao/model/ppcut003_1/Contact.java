package com.bbva.pzic.cards.dao.model.ppcut003_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>contact</code>, utilizado por la clase <code>Specificcontact</code></p>
 * 
 * @see Specificcontact
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Contact {
	
	/**
	 * <p>Campo <code>contactDetailType</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "contactDetailType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String contactdetailtype;
	
	/**
	 * <p>Campo <code>address</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "address", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 80, signo = true, obligatorio = true)
	private String address;
	
}
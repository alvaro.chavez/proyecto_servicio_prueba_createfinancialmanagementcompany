package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "cardProduct", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "cardProduct", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Financial product identifier.
     */
    private String id;
    /**
     * Financial product name.
     */
    private String name;
    /**
     * Contains the subproduct related to a product.
     */
    private Subproduct subproduct;
    /**
     * Card type.
     */
    private CardType cardType;
    /**
     * List of images printed on the card.
     */
    private List<Image> images;
    /**
     * Granted credit. This amount may be provided in several currencies (depending on the country, major currency and equivalent in another currency).
     * This attribute is mandatory for credit cards.
     */
    private List<GrantedCredit> grantedCredits;
    /**
     *Rates charge applied to the type of offer associated.
     */
    private List<RateOffer> rates;
    /**
     * Defines the priority that each card has over the cards listed and determines the order in which the cards are presented to the client.
     */
    private String priorityLevel;
    /**
     * The bank identification number (BIN) associated to the product.
     */
    private String bankIdentificationNumber;
    /**
     * Fees charge applied to the type of card associated.
     */
    private FeesOffer fees;
    /**
     * Benefits related to the card selected as cash disposition among others.
     */
    private List<Benefits> benefits;
    /**
     * Granted minimum credit. This amount may be provided in several currencies (depending on the country).
     */
    private List<GrantedMinimumCredit> grantedMinimumCredits;
    /**
     * Loyalty program related to a card.
     */
    private LoyaltyProgram loyaltyProgram;
    /**
     * Commercial brand associated to the card.
     */
    private BrandAssociation brandAssociation;
    /**
     * Offer identifier.
     */
    private String offerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Subproduct getSubproduct() {
        return subproduct;
    }

    public void setSubproduct(Subproduct subproduct) {
        this.subproduct = subproduct;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<GrantedCredit> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<GrantedCredit> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public List<RateOffer> getRates() {
        return rates;
    }

    public void setRates(List<RateOffer> rates) {
        this.rates = rates;
    }

    public String getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(String priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public String getBankIdentificationNumber() {
        return bankIdentificationNumber;
    }

    public void setBankIdentificationNumber(String bankIdentificationNumber) {
        this.bankIdentificationNumber = bankIdentificationNumber;
    }

    public FeesOffer getFees() {
        return fees;
    }

    public void setFees(FeesOffer fees) {
        this.fees = fees;
    }

    public List<Benefits> getBenefits() {
        return benefits;
    }

    public void setBenefits(List<Benefits> benefits) {
        this.benefits = benefits;
    }

    public List<GrantedMinimumCredit> getGrantedMinimumCredits() {
        return grantedMinimumCredits;
    }

    public void setGrantedMinimumCredits(List<GrantedMinimumCredit> grantedMinimumCredits) {
        this.grantedMinimumCredits = grantedMinimumCredits;
    }

    public LoyaltyProgram getLoyaltyProgram() {
        return loyaltyProgram;
    }

    public void setLoyaltyProgram(LoyaltyProgram loyaltyProgram) {
        this.loyaltyProgram = loyaltyProgram;
    }

    public BrandAssociation getBrandAssociation() {
        return brandAssociation;
    }

    public void setBrandAssociation(BrandAssociation brandAssociation) {
        this.brandAssociation = brandAssociation;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}

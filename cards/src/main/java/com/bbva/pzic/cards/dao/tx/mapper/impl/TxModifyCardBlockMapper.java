package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.canonic.Block;
import com.bbva.pzic.cards.canonic.BlockData;
import com.bbva.pzic.cards.canonic.Reason;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2S;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardBlockMapper;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;

/**
 * @author Entelgy
 */
@Component
public class TxModifyCardBlockMapper implements ITxModifyCardBlockMapper {

    private static final Log LOG = LogFactory.getLog(TxModifyCardBlockMapper.class);

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }


    /**
     * @see ITxModifyCardBlockMapper#mapInput(DTOIntBlock)
     */
    @Override
    public FormatoMPM0B2 mapInput(final DTOIntBlock dtoIntBlock) {
        LOG.info("... called method TxModifyCardBlockMapper.mapInput ...");
        final FormatoMPM0B2 formatoMPM0B2 = new FormatoMPM0B2();
        formatoMPM0B2.setIdetarj(dtoIntBlock.getCard().getCardId());
        formatoMPM0B2.setIdebloq(dtoIntBlock.getBlockId());
        formatoMPM0B2.setIderazo(dtoIntBlock.getReasonId());
        formatoMPM0B2.setIdactbl(dtoIntBlock.getIsActive());
        formatoMPM0B2.setIdrepos(dtoIntBlock.getIsReissued());
        formatoMPM0B2.setDescrip(dtoIntBlock.getAdditionalInformation());
        return formatoMPM0B2;
    }

    /**
     * @see ITxModifyCardBlockMapper#mapOutput(FormatoMPM0B2S, DTOIntBlock)
     */
    @Override
    public BlockData mapOutput(final FormatoMPM0B2S formatoMPM0B2S, final DTOIntBlock dtoIntBlock) {
        LOG.info("... called method TxModifyCardBlockMapper.mapOutput ...");
        if (formatoMPM0B2S == null) {
            return new BlockData();
        }

        final Block block = new Block();
        block.setBlockId(translator.translateBackendEnumValueStrictly("cards.block.blockId", formatoMPM0B2S.getIdebloq()));
        block.setName(formatoMPM0B2S.getDesbloq());
        if (formatoMPM0B2S.getIderazo() != null || formatoMPM0B2S.getDesrazo() != null) {
            Reason reason = new Reason();
            reason.setId(formatoMPM0B2S.getIderazo());
            reason.setName(formatoMPM0B2S.getDesrazo());
            block.setReason(reason);
        }
        if (formatoMPM0B2S.getMcnbloq() != null) {
            block.setReference(formatoMPM0B2S.getMcnbloq().toString());
        }
        if (formatoMPM0B2S.getFecbloq() != null && formatoMPM0B2S.getHorablq() != null) {
            try {
                block.setBlockDate(DateUtils.toDateTime(formatoMPM0B2S.getFecbloq(), formatoMPM0B2S.getHorablq()));
            } catch (ParseException e) {
                throw new BusinessServiceException(Errors.WRONG_DATE, e);
            }
        }

        final BlockData blockData = new BlockData();
        blockData.setData(block);
        return blockData;
    }

}
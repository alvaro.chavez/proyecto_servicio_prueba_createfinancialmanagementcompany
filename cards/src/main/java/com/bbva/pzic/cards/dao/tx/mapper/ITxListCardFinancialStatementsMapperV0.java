package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntStatementList;
import com.bbva.pzic.cards.business.dto.InputListCardFinancialStatements;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPME1E2;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPMS1E2;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPMS2E2;

/**
 * Created on 17/07/2018.
 *
 * @author Entelgy
 */
public interface ITxListCardFinancialStatementsMapperV0 {

    FormatoMPME1E2 mapIn(InputListCardFinancialStatements dtoIn);

    DTOIntStatementList mapOut(FormatoMPMS1E2 formatOutput, DTOIntStatementList dtoOut);

    DTOIntStatementList mapOut2(FormatoMPMS2E2 formatOutput, DTOIntStatementList dtoOut);
}
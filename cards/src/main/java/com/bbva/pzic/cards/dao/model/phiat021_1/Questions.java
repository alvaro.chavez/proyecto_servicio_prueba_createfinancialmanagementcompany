package com.bbva.pzic.cards.dao.model.phiat021_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>questions</code>, utilizado por la clase <code>PeticionTransaccionPhiat021_1</code></p>
 * 
 * @see PeticionTransaccionPhiat021_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Questions {
	
	/**
	 * <p>Campo <code>question</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "question", tipo = TipoCampo.DTO)
	private Question question;
	
}
package com.bbva.pzic.cards.dao.model.ppcut003_1;

import java.util.List;
import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Bean de respuesta para la transacci&oacute;n <code>PPCUT003</code>
 * 
 * @see PeticionTransaccionPpcut003_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooSerializable
public class RespuestaTransaccionPpcut003_1 {
	
	/**
	 * <p>Cabecera <code>COD-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_AVISO)
	private String codigoAviso;
	
	/**
	 * <p>Cabecera <code>DES-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.DESCRIPCION_AVISO)
	private String descripcionAviso;
	
	/**
	 * <p>Cabecera <code>COD-UUAA-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.APLICACION_AVISO)
	private String aplicacionAviso;
	
	/**
	 * <p>Cabecera <code>COD-RETORNO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_RETORNO)
	private String codigoRetorno;
	
	/**
	 * <p>Campo <code>campo_1_deliveries</code>, &iacute;ndice: <code>1</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 1, nombre = "deliveries", tipo = TipoCampo.LIST)
	private List<Campo_1_deliveries> campo_1_deliveries;
	
	/**
	 * <p>Campo <code>campo_2_paymentMethod</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "paymentMethod", tipo = TipoCampo.DTO)
	private Campo_2_paymentmethod campo_2_paymentmethod;
	
	/**
	 * <p>Campo <code>campo_3_contactability</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "contactability", tipo = TipoCampo.DTO)
	private Campo_3_contactability campo_3_contactability;
	
	/**
	 * <p>Campo <code>campo_4_grantedCredits</code>, &iacute;ndice: <code>4</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 4, nombre = "grantedCredits", tipo = TipoCampo.LIST)
	private List<Campo_4_grantedcredits> campo_4_grantedcredits;
	
	/**
	 * <p>Campo <code>campo_5_rates</code>, &iacute;ndice: <code>5</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 5, nombre = "rates", tipo = TipoCampo.DTO)
	private Campo_5_rates campo_5_rates;
	
	/**
	 * <p>Campo <code>campo_6_fees</code>, &iacute;ndice: <code>6</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 6, nombre = "fees", tipo = TipoCampo.DTO)
	private Campo_6_fees campo_6_fees;
	
	/**
	 * <p>Campo <code>campo_7_offerId</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "offerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String campo_7_offerid;
	
	/**
	 * <p>Campo <code>campo_8_product</code>, &iacute;ndice: <code>8</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 8, nombre = "product", tipo = TipoCampo.DTO)
	private Campo_8_product campo_8_product;
	
	/**
	 * <p>Campo <code>campo_9_notificationsByOperation</code>, &iacute;ndice: <code>9</code>, tipo: <code>BOOLEAN</code>
	 */
	@Campo(indice = 9, nombre = "notificationsByOperation", tipo = TipoCampo.BOOLEAN, signo = true)
	private Boolean campo_9_notificationsbyoperation;

}
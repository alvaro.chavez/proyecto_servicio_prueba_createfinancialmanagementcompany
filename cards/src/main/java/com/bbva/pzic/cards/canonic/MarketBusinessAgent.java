package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "MarketBusinessAgent", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "MarketBusinessAgent", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class MarketBusinessAgent implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Unique identifier of the seller, which can be employee or external
     */
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Transaction", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Transaction", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Unique identifier of the transaction.
     */
    private String id;
    /**
     * Amount that is associated to the transaction on the local currency. This
     * amount may not include any charges that the bank can carry with the
     * operation.
     */
    private LocalAmount localAmount;
    /**
     * Origin amount that is associated to the transaction. This attribute can
     * only be informed when the transactions has been performed on a currency
     * that is different from the contract\`s original currency. For example,
     * when a customer performs a purchase in USD using his card that is
     * associated to an account opened in Euros, originAmount attribute will be
     * fulfilled with the original price of the item purchased in UK. This
     * amount may not include any charges that the bank can carry with the
     * operation.
     */
    private OriginAmount originAmount;
    /**
     * Indicates whether the operation is an addition or a substraction from the
     * total balance that customer has in the cotract that is being querying.
     */
    private MoneyFlow moneyFlow;
    /**
     * Brief description of the operative that create the transaction. Backend
     * must provide this information depending on the transaction\`s type.
     */
    private String concept;
    /**
     * Transaction type. It talks about which kind of operative has created this
     * transaction. DISCLAIMER: Nowsdays some geographies have no possibility of
     * identifying all the types of transaction that are detailed below. This
     * should not become an impediment that in the near future this
     * inconvenience be fixed by the corresponding backends and allow the
     * customers to know and filter those transactions according to their
     * criteria.
     */
    private TransactionType transactionType;
    /**
     * String based on ISO-8601 date format for providing the last date when the
     * operation was performed by the client.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name="dateTime")
    private Calendar operationDate;
    /**
     * String based on ISO-8601 date format for providing the last date when the
     * transaction was accounted.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name="dateTime")
    private Calendar accountedDate;
    /**
     * It represents the information about the possibility that certain
     * operations to be financed within the conditions of the product to which
     * they belong. For example, we can perform a purchase in an store that can
     * be financed if the amount of the purchase is greater than 50,00 euros.
     * The product and the transaction type can define which business rules are
     * applied to know whether the operation can be financed or not.
     */
    private FinancingType financingType;
    /**
     * Transaction status.
     */
    private Status status;
    /**
     * Customer\`s contract that is being queried. When customer is performing
     * an account\`s transactions query in this attribute must be setted the
     * information about that account. It occurs the same for other products
     * which transaction can be queried (i.e. cards, loans, etc...)
     */
    private Contract contract;
    /**
     * List of tags that can label a transaction. This tags are customizable by
     * the customers and can be used to help them when the try to locate a
     * transaction.
     */
    private List<String> tags;
    /**
     * Represents the category that a transaction can fit.
     */
    private Category category;
    /**
     * Additional information related to this transaction.
     */
    private AdditionalInformation additionalInformation;

    /**
     * Detailed information according to the transaction type.
     */
    private TransactionTypeAgreement detail;

    private ExchangeRate exchangeRate;

    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name="dateTime")
    private Calendar valuationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalAmount getLocalAmount() {
        return localAmount;
    }

    public void setLocalAmount(LocalAmount localAmount) {
        this.localAmount = localAmount;
    }

    public OriginAmount getOriginAmount() {
        return originAmount;
    }

    public void setOriginAmount(OriginAmount originAmount) {
        this.originAmount = originAmount;
    }

    public MoneyFlow getMoneyFlow() {
        return moneyFlow;
    }

    public void setMoneyFlow(MoneyFlow moneyFlow) {
        this.moneyFlow = moneyFlow;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Calendar getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Calendar operationDate) {
        this.operationDate = operationDate;
    }

    public Calendar getAccountedDate() {
        return accountedDate;
    }

    public void setAccountedDate(Calendar accountedDate) {
        this.accountedDate = accountedDate;
    }

    public FinancingType getFinancingType() {
        return financingType;
    }

    public void setFinancingType(FinancingType financingType) {
        this.financingType = financingType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public AdditionalInformation getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(
            AdditionalInformation additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public TransactionTypeAgreement getDetail() {
        return detail;
    }

    public void setDetail(TransactionTypeAgreement detail) {
        this.detail = detail;
    }

    public ExchangeRate getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(ExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Calendar getValuationDate() {
        return valuationDate;
    }

    public void setValuationDate(Calendar valuationDate) {
        this.valuationDate = valuationDate;
    }
}
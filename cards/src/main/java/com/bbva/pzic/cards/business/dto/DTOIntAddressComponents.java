package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
public class DTOIntAddressComponents {

    private List<String> componentTypes;
    private String code;
    private String name;

    private String formattedAddress;

    public List<String> getComponentTypes() {
        return componentTypes;
    }

    public void setComponentTypes(List<String> componentTypes) {
        this.componentTypes = componentTypes;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }
}

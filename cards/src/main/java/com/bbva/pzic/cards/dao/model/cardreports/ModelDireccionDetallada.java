package com.bbva.pzic.cards.dao.model.cardreports;

public class ModelDireccionDetallada {

    private ModelTipoDireccionDetallada tipo;

    private String codigo;

    private String nombre;

    public ModelTipoDireccionDetallada getTipo() {
        return tipo;
    }

    public void setTipo(ModelTipoDireccionDetallada tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}

package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
public class DTOIntListCards {
    @Size(max = 8, groups = {ValidationGroup.ListCards.class, ValidationGroup.ListCardsV0.class})
    private String customerId;
    private List<String> cardTypeId;
    private String physicalSupportId;
    private List<String> statusId;
    private List<String> participantsParticipantTypeId;
    @Size(max = 16, groups = {ValidationGroup.ListCards.class, ValidationGroup.ListCardsV0.class})
    private String paginationKey;
    @Digits(integer = 3, fraction = 0, groups = {ValidationGroup.ListCards.class, ValidationGroup.ListCardsV0.class})
    private Integer pageSize;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<String> getCardTypeId() {
        return cardTypeId;
    }

    public void setCardTypeId(List<String> cardTypeId) {
        this.cardTypeId = cardTypeId;
    }

    public String getPhysicalSupportId() {
        return physicalSupportId;
    }

    public void setPhysicalSupportId(String physicalSupportId) {
        this.physicalSupportId = physicalSupportId;
    }

    public List<String> getStatusId() {
        return statusId;
    }

    public void setStatusId(List<String> statusId) {
        this.statusId = statusId;
    }

    public String getPaginationKey() {
        return paginationKey;
    }

    public void setPaginationKey(String paginationKey) {
        this.paginationKey = paginationKey;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<String> getParticipantsParticipantTypeId() {
        return participantsParticipantTypeId;
    }

    public void setParticipantsParticipantTypeId(List<String> participantsParticipantTypeId) {
        this.participantsParticipantTypeId = participantsParticipantTypeId;
    }
}

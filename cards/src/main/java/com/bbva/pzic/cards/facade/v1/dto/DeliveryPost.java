package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "deliveryPost", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "deliveryPost", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeliveryPost implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Delivery service type of a product or document. These can be physical or
     * digital, depending on the case the delivery will be at an address or a
     * contact.
     */
    private ServiceTypePost serviceType;
    /**
     * Detail of delivery contact.
     */
    private Contact contact;
    /**
     * Destination type of card delivery.
     */
    private Destination destination;
    /**
     * Custom address where the card will be deliver. This can be an address
     * that the customer enters manually or and address previously selected by
     * the customer.
     */
    private Address address;
    /**
     * Delivery manager identifier.
     */
    private String id;

    public ServiceTypePost getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceTypePost serviceType) {
        this.serviceType = serviceType;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
public class DTOIntItemizeRate {

    @NotNull(groups = {ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    private String rateType;
    private Date calculationDate;
    @NotNull(groups = {ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    @Valid
    private DTOIntItemizeRatesUnit itemizeRatesUnit;

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public Date getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(Date calculationDate) {
        this.calculationDate = calculationDate;
    }

    public DTOIntItemizeRatesUnit getItemizeRatesUnit() {
        return itemizeRatesUnit;
    }

    public void setItemizeRatesUnit(DTOIntItemizeRatesUnit itemizeRatesUnit) {
        this.itemizeRatesUnit = itemizeRatesUnit;
    }
}

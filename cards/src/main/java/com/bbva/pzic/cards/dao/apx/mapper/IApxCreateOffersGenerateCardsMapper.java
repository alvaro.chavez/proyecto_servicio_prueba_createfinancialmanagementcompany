package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateOffersGenerateCards;
import com.bbva.pzic.cards.dao.model.phiat021_1.PeticionTransaccionPhiat021_1;
import com.bbva.pzic.cards.dao.model.phiat021_1.RespuestaTransaccionPhiat021_1;
import com.bbva.pzic.cards.facade.v0.dto.OfferGenerate;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
public interface IApxCreateOffersGenerateCardsMapper {

    PeticionTransaccionPhiat021_1 mapIn(InputCreateOffersGenerateCards input);

    OfferGenerate mapOut(RespuestaTransaccionPhiat021_1 response);
}

package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanSimulationData;
import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0DET;
import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0WSC;
import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0WSE;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
public interface ITxSimulateCardInstallmentsPlanMapperV0 {

    FormatoMPM0WSE mapIn(DTOIntCardInstallmentsPlan dtoIn);

    InstallmentsPlanSimulationData mapOut1(FormatoMPM0WSC formatoMPM0TSC, DTOIntCardInstallmentsPlan dtoIn);

    InstallmentsPlanSimulationData mapOut2(FormatoMPM0DET formatoMPM0DET, InstallmentsPlanSimulationData installmentsPlanSimulationData);
}

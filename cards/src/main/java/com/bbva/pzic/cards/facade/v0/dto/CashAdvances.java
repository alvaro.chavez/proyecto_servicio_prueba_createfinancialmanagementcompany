package com.bbva.pzic.cards.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "CashAdvances", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "CashAdvances", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CashAdvances implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String description;
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar applyDate;
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name="dateTime")
    private Calendar accountingDate;
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date maturityDate;
    private Import requestedAmount;
    private ImportBreakDown installmentAmount;
    private CurrentInstallment currentInstallment;
    private Import total;
    private PaidAmount paidAmount;
    private PendingAmount pendingAmount;
    private List<Rate> rates;
    private Terms terms;
    private String offerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Calendar applyDate) {
        this.applyDate = applyDate;
    }

    public Calendar getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(Calendar accountingDate) {
        this.accountingDate = accountingDate;
    }

    public Date getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }

    public Import getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(Import requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public ImportBreakDown getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(ImportBreakDown installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public CurrentInstallment getCurrentInstallment() {
        return currentInstallment;
    }

    public void setCurrentInstallment(CurrentInstallment currentInstallment) {
        this.currentInstallment = currentInstallment;
    }

    public Import getTotal() {
        return total;
    }

    public void setTotal(Import total) {
        this.total = total;
    }

    public PaidAmount getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(PaidAmount paidAmount) {
        this.paidAmount = paidAmount;
    }

    public PendingAmount getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(PendingAmount pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }

    public Terms getTerms() {
        return terms;
    }

    public void setTerms(Terms terms) {
        this.terms = terms;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}

package com.bbva.pzic.cards.dao.model.mp6i;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MP6I</code>
 * 
 * @see PeticionTransaccionMp6i
 * @see RespuestaTransaccionMp6i
 */
@Component
public class TransaccionMp6i implements InvocadorTransaccion<PeticionTransaccionMp6i,RespuestaTransaccionMp6i> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMp6i invocar(PeticionTransaccionMp6i transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMp6i.class, RespuestaTransaccionMp6i.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMp6i invocarCache(PeticionTransaccionMp6i transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMp6i.class, RespuestaTransaccionMp6i.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
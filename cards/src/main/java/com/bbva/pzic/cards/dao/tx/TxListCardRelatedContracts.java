package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntRelatedContractsSearchCriteria;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMENL5;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMS1L5;
import com.bbva.pzic.cards.dao.model.mpl5.PeticionTransaccionMpl5;
import com.bbva.pzic.cards.dao.model.mpl5.RespuestaTransaccionMpl5;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardRelatedContractsMapper;
import com.bbva.pzic.cards.facade.v0.dto.RelatedContracts;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("txListCardRelatedContracts")
public class TxListCardRelatedContracts
        extends SingleOutputFormat<DTOIntRelatedContractsSearchCriteria, FormatoMPMENL5, List<RelatedContracts>, FormatoMPMS1L5> {

    @Autowired
    private ITxListCardRelatedContractsMapper txListCardRelatedContractsMapper;

    public TxListCardRelatedContracts(@Qualifier("transaccionMpl5") InvocadorTransaccion<PeticionTransaccionMpl5, RespuestaTransaccionMpl5> transaction) {
        super(transaction, PeticionTransaccionMpl5::new, ArrayList::new, FormatoMPMS1L5.class);
    }

    @Override
    protected FormatoMPMENL5 mapInput(DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria) {
        return txListCardRelatedContractsMapper.mapInput(dtoIntRelatedContractsSearchCriteria);
    }

    @Override
    protected List<RelatedContracts> mapFirstOutputFormat(FormatoMPMS1L5 formatoMPMS1L5, DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria, List<RelatedContracts> dtoOut) {
        return txListCardRelatedContractsMapper.mapOutput(formatoMPMS1L5, dtoOut);
    }
}

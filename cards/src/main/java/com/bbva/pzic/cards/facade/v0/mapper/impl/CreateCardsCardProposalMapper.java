package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.CardProposal;
import com.bbva.pzic.cards.canonic.CardProposalData;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardsCardProposalMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.ws.rs.core.Response;
import java.util.List;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
@Mapper
public class CreateCardsCardProposalMapper extends ConfigurableMapper
        implements ICreateCardsCardProposalMapper {

    private static final Log LOG = LogFactory.getLog(CreateCardsCardProposalMapper.class);

    @Autowired
    private EnumMapper enumMapper;

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(CardProposal.class, InputCreateCardsCardProposal.class)
                .field("title.id", "cardProposal.title.id")
                .field("cardType.id", "cardProposal.cardType.id")
                .field("participant.identityDocument.documentType.id", "cardProposal.participant.identityDocument.documentType.id")
                .field("participant.identityDocument.number", "cardProposal.participant.identityDocument.number")
                .field("participant.firstName", "cardProposal.participant.firstName")
                .field("participant.middleName", "cardProposal.participant.middleName")
                .field("participant.lastName", "cardProposal.participant.lastName")
                .field("participant.secondLastName", "cardProposal.participant.secondLastName")
                .field("participant.profession.id", "cardProposal.participant.profession.id")
                .field("participant.maritalStatus.id", "cardProposal.participant.maritalStatus.id")
                .field("participant.relationType", "cardProposal.participant.relationType")
                .field("participant.contactDetails[0].contactValue", "cardProposal.participant.contactDetails[0].contactValue")
                .field("participant.contactDetails[0].contactType.id", "cardProposal.participant.contactDetails[0].contactType.id")
                .field("participant.contactDetails[1].contactValue", "cardProposal.participant.contactDetails[1].contactValue")
                .field("participant.contactDetails[1].contactType.id", "cardProposal.participant.contactDetails[1].contactType.id")
                .field("grantedCredits[0].amount", "cardProposal.grantedCredits[0].amount")
                .field("grantedCredits[0].currency", "cardProposal.grantedCredits[0].currency")
                .field("delivery.destination.id", "cardProposal.delivery.destination.id")
                .field("bank.id", "cardProposal.bank.id")
                .field("bank.branch.id", "cardProposal.bank.branch.id")
                .field("offerId", "cardProposal.offerId")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputCreateCardsCardProposal mapIn(final String cardId, final CardProposal cardProposal) {
        LOG.info("... called method CreateCardsCardProposalMapper.mapIn ...");
        InputCreateCardsCardProposal input = map(cardProposal, InputCreateCardsCardProposal.class);

        input.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.CARDID, SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL));

        final DTOIntCardProposal dtoIntCardProposal = input.getCardProposal();
        if (dtoIntCardProposal == null) {
            return input;
        }

        dtoIntCardProposal.setCardType(enumMapCardType(dtoIntCardProposal.getCardType()));
        dtoIntCardProposal.setParticipant(enumMapParticipantIdentityDocumentType(dtoIntCardProposal.getParticipant()));
        dtoIntCardProposal.setDelivery(enumMapDelivery(dtoIntCardProposal.getDelivery()));

        return input;
    }

    private DTOIntDelivery enumMapDelivery(final DTOIntDelivery delivery) {
        if (delivery == null || delivery.getDestination() == null) {
            return delivery;
        }
        delivery.getDestination().setId(enumMapper.getBackendValue("cards.delivery.destination.id", delivery.getDestination().getId()));
        return delivery;
    }

    private DTOIntCardType enumMapCardType(final DTOIntCardType cardType) {
        if (cardType == null) {
            return null;
        }
        cardType.setId(enumMapper.getBackendValue("cards.cardType.id", cardType.getId()));
        return cardType;
    }


    private DTOIntAuthorizedParticipant enumMapParticipantIdentityDocumentType(final DTOIntAuthorizedParticipant participant) {
        if (participant == null) {
            return null;
        }
        participant.setIdentityDocument(enumMapIdentityDocumentType(participant.getIdentityDocument()));
        participant.setContactDetails(enumMapContactDetails(participant.getContactDetails()));
        return participant;
    }

    private List<DTOIntParticipantContactDetail> enumMapContactDetails(final List<DTOIntParticipantContactDetail> contactDetails) {
        if (CollectionUtils.isEmpty(contactDetails)) {
            return contactDetails;
        }

        DTOIntParticipantContactDetail dtoIntParticipantContactDetail = contactDetails.get(0);
        dtoIntParticipantContactDetail.setContactType(enumMapContactType(dtoIntParticipantContactDetail.getContactType()));

        if (contactDetails.size() > 1) {
            dtoIntParticipantContactDetail = contactDetails.get(1);
            dtoIntParticipantContactDetail.setContactType(enumMapContactType(dtoIntParticipantContactDetail.getContactType()));
        }

        return contactDetails;
    }

    private DTOIntContactType enumMapContactType(final DTOIntContactType contactType) {
        if (contactType == null) {
            return null;
        }
        contactType.setId(enumMapper.getBackendValue("cards.contactDetails.contactType.id", contactType.getId()));
        return contactType;
    }

    private DTOIntIdentityDocument enumMapIdentityDocumentType(final DTOIntIdentityDocument identityDocument) {

        if (identityDocument == null || identityDocument.getDocumentType() == null) {
            return identityDocument;
        }

        final DTOIntDocumentType documentType = identityDocument.getDocumentType();
        documentType.setId(enumMapper.getBackendValue("cards.identityDocument.documentType.id", documentType.getId()));
        return identityDocument;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response mapOut(final CardProposalData data) {
        LOG.info("... called method CreateCardsCardProposalMapper.mapOut ...");

        return Response.status(Response.Status.CREATED).entity(data).build();
    }

}

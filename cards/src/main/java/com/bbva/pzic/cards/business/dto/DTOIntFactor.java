package com.bbva.pzic.cards.business.dto;

import java.math.BigDecimal;

public class DTOIntFactor {

    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }


}

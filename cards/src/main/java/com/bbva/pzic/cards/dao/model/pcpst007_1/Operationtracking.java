package com.bbva.pzic.cards.dao.model.pcpst007_1;

import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>OperationTracking</code>, utilizado por la clase <code>RespuestaTransaccionPcpst007_1</code></p>
 * 
 * @see RespuestaTransaccionPcpst007_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Operationtracking {
	
	/**
	 * <p>Campo <code>operationNumber</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "operationNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String operationnumber;
	
	/**
	 * <p>Campo <code>operationDate</code>, &iacute;ndice: <code>2</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 2, nombre = "operationDate", tipo = TipoCampo.FECHA, signo = true, formato = "yyyy-MM-dd", obligatorio = true)
	private Date operationdate;
	
}
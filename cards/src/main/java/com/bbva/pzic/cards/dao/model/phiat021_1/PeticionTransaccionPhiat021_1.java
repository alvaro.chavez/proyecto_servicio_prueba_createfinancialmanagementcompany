package com.bbva.pzic.cards.dao.model.phiat021_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PHIAT021</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPhiat021_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPhiat021_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PHIAT021-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;&lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PHIAT021&quot; application=&quot;PHIA&quot; version=&quot;01&quot; country=&quot;PE&quot; language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;list order=&quot;1&quot; mandatory=&quot;0&quot; name=&quot;questions&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;question&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.QuestionDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;50&quot;/&gt;
 * &lt;dto order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;answer&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.AnswerDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;value&quot; type=&quot;String&quot; size=&quot;50&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;parameter order=&quot;2&quot; mandatory=&quot;0&quot; name=&quot;BCS-Device-Screen-Size&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;dto order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;dictum&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.DictumDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot;/&gt;
 * &lt;list order=&quot;2&quot; mandatory=&quot;0&quot; name=&quot;reasons&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;reason&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.ReasonDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;50&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; mandatory=&quot;0&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;100&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;list order=&quot;2&quot; mandatory=&quot;0&quot; name=&quot;cardProducts&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;cardProduct&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.CardProductDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;40&quot;/&gt;
 * &lt;dto order=&quot;3&quot; mandatory=&quot;0&quot; name=&quot;subproduct&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.SubproductDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto order=&quot;4&quot; mandatory=&quot;1&quot; name=&quot;cardType&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.CardTypeDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;11&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;list order=&quot;5&quot; mandatory=&quot;1&quot; name=&quot;images&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;image&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.ImageDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;40&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; mandatory=&quot;0&quot; name=&quot;url&quot; type=&quot;String&quot; size=&quot;200&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list order=&quot;6&quot; mandatory=&quot;0&quot; name=&quot;grantedCredits&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;grantedCredit&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.GrantedCreditDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list order=&quot;7&quot; mandatory=&quot;0&quot; name=&quot;rates&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;rate&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.RateDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;dto order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;rateType&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.RateTypeDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;unit&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.UnitDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;10&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; mandatory=&quot;1&quot; name=&quot;percentage&quot; type=&quot;String&quot; size=&quot;10&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;parameter order=&quot;8&quot; mandatory=&quot;0&quot; name=&quot;priorityLevel&quot; type=&quot;Long&quot; size=&quot;4&quot;/&gt;
 * &lt;parameter order=&quot;9&quot; mandatory=&quot;0&quot; name=&quot;bankIdentificationNumber&quot; type=&quot;String&quot; size=&quot;6&quot;/&gt;
 * &lt;dto order=&quot;10&quot; mandatory=&quot;0&quot; name=&quot;fees&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.FeesDTO&quot; artifactId=&quot;PHIAC041&quot; &gt;
 * &lt;list order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;itemizeFees&quot;&gt;
 * &lt;dto name=&quot;itemizeFee&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.ItemizeFeeDTO&quot; artifactId=&quot;PHIAC041&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;feeType&quot; type=&quot;String&quot; size=&quot;27&quot;/&gt;
 * &lt;dto order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;itemizeFeeUnit&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.ItemizeFeeUnitDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;unitType&quot; type=&quot;String&quot; size=&quot;6&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; mandatory=&quot;1&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;list order=&quot;11&quot; mandatory=&quot;0&quot; name=&quot;benefits&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;benefit&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.BenefitDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;4&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; mandatory=&quot;1&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;254&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;list order=&quot;12&quot; mandatory=&quot;0&quot; name=&quot;grantedMinimumCredits&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;grantedMinimumCredit&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.GrantedCreditDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; mandatory=&quot;1&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto order=&quot;13&quot; mandatory=&quot;0&quot; name=&quot;loyaltyProgram&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.LoyaltyProgramDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto order=&quot;14&quot; mandatory=&quot;0&quot; name=&quot;brandAssociation&quot; package=&quot;com.bbva.phia.dto.generatecardoffers.BrandAssociationDTO&quot; artifactId=&quot;PHIAC041&quot;&gt;
 * &lt;parameter order=&quot;1&quot; mandatory=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;15&quot; mandatory=&quot;0&quot; name=&quot;offerId&quot; type=&quot;String&quot; size=&quot;4&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Evaluación y generación de ofertas de TC&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPhiat021_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PHIAT021",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPhiat021_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPhiat021_1 {
		
		/**
	 * <p>Campo <code>questions</code>, &iacute;ndice: <code>1</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 1, nombre = "questions", tipo = TipoCampo.LIST)
	private List<Questions> questions;
	
	/**
	 * <p>Campo <code>BCS-Device-Screen-Size</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "BCS-Device-Screen-Size", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
	private String bcsDeviceScreenSize;
	
}
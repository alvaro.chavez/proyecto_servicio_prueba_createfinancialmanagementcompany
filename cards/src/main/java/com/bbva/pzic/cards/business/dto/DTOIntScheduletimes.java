package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import java.util.Calendar;

/**
 * Created on 20/04/2020.
 *
 * @author Entelgy.
 */
public class DTOIntScheduletimes {

    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    private Calendar starttime;
    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    private Calendar endtime;

    public Calendar getStarttime() {
        return starttime;
    }

    public void setStarttime(Calendar starttime) {
        this.starttime = starttime;
    }

    public Calendar getEndtime() {
        return endtime;
    }

    public void setEndtime(Calendar endtime) {
        this.endtime = endtime;
    }
}

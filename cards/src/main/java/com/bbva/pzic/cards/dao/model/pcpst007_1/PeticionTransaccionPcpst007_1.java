package com.bbva.pzic.cards.dao.model.pcpst007_1;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PCPST007</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPcpst007_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPcpst007_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PCPST007-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PCPST007&quot; application=&quot;PCPS&quot; version=&quot;01&quot; country=&quot;PE&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;cardId&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;19&quot;/&gt;
 * &lt;dto order=&quot;2&quot; name=&quot;reason&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.pcps.dto.eventenrichcr.card.ReasonDTO&quot;
 * artifactId=&quot;PCPSC004&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;2&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;comments&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;30&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto order=&quot;3&quot; name=&quot;card&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.pcps.dto.eventenrichcr.card.CardDTO&quot;
 * artifactId=&quot;PCPSC004&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;product&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.pcps.dto.eventenrichcr.card.ProductDTO&quot;
 * artifactId=&quot;PCPSC004&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;dto order=&quot;2&quot; name=&quot;subProduct&quot; mandatory=&quot;0&quot;
 * package=&quot;com.bbva.pcps.dto.eventenrichcr.card.SubProductDTO&quot; artifactId=&quot;PCPSC004&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;customerId&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;40&quot;/&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;OperationTracking&quot; mandatory=&quot;1&quot;
 * package=&quot;com.bbva.pcps.dto.eventenrichcr.card.OperationTrackingDTO&quot; artifactId=&quot;PCPSC004&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;operationNumber&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;10&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;operationDate&quot; mandatory=&quot;1&quot; type=&quot;Date (YYYY-MM-DD)&quot; size=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto order=&quot;2&quot; name=&quot;card&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.pcps.dto.eventenrichcr.card.CardDTO&quot;
 * artifactId=&quot;PCPSC004&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;number&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;19&quot;/&gt;
 * &lt;dto order=&quot;3&quot; name=&quot;product&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.pcps.dto.eventenrichcr.card.ProductDTO&quot;
 * artifactId=&quot;PCPSC004&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;30&quot;/&gt;
 * &lt;dto order=&quot;3&quot; name=&quot;subProduct&quot; mandatory=&quot;0&quot;
 * package=&quot;com.bbva.pcps.dto.eventenrichcr.card.SubProductDTO&quot; artifactId=&quot;PCPSC004&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto order=&quot;4&quot; name=&quot;numberType&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.pcps.dto.eventenrichcr.card.NumberTypeDTO&quot;
 * artifactId=&quot;PCPSC004&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;30&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;transaccion para cancelacion de tarjetas&lt;/description&gt;
 * &lt;/transaction&gt;
 * </pre></code>
 *
 * @author Arquitectura Spring BBVA
 * @see RespuestaTransaccionPcpst007_1
 */
@Transaccion(
        nombre = "PCPST007",
        tipo = 1,
        subtipo = 1,
        version = 1,
        configuracion = "default_apx",
        respuesta = RespuestaTransaccionPcpst007_1.class,
        atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPcpst007_1 {

    /**
     * <p>Campo <code>cardId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 1, nombre = "cardId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true, obligatorio = true)
    private String cardid;

    /**
     * <p>Campo <code>reason</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 2, nombre = "reason", tipo = TipoCampo.DTO)
    private Reason reason;

    /**
     * <p>Campo <code>card</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 3, nombre = "card", tipo = TipoCampo.DTO)
    private Card card;

    /**
     * <p>Campo <code>customerId</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "customerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
    private String customerid;

}
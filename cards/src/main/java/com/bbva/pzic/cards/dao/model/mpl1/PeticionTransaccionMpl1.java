package com.bbva.pzic.cards.dao.model.mpl1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>MPL1</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpl1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpl1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPL1.D1200603.txt
 * MPL1LISTADO DE TARJETAS                MP        MP2CMPL7     01 MPMENL1             MPL1  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2017-01-25XP91612 2020-06-0312.22.33XP86064 2017-01-25-15.56.33.330443XP91612 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENL1.D1200603.txt
 * MPMENL1 �LISTADO DE TARJETAS           �F�07�00051�01�00001�NUMCLIE�CODIGO DE CLIENTE   �A�008�0�O�        �
 * MPMENL1 �LISTADO DE TARJETAS           �F�07�00051�02�00009�TIPTARC�TIPO DE TARJETA 1   �A�010�0�O�        �
 * MPMENL1 �LISTADO DE TARJETAS           �F�07�00051�03�00019�SOPFISL�SOPORTE FISICO 1    �A�001�0�O�        �
 * MPMENL1 �LISTADO DE TARJETAS           �F�07�00051�04�00020�ESTARJO�ESTADO DE LA TARJETA�A�010�0�O�        �
 * MPMENL1 �LISTADO DE TARJETAS           �F�07�00051�05�00030�TIPPART�TIPO PARTICIPANTE   �A�003�0�O�        �
 * MPMENL1 �LISTADO DE TARJETAS           �F�07�00051�06�00033�IDPAGIN�SIG. IDENTIF DEVOLV.�A�016�0�O�        �
 * MPMENL1 �LISTADO DE TARJETAS           �F�07�00051�07�00049�TAMPAGI�SIGUIENTE REGISTRO. �N�003�0�O�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1L1.D1200603.txt
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�01�00001�NUMTARJ�NUMERO DE TARJETA   �A�016�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�02�00017�FECVENC�FECHA DE VENCIMIENTO�A�010�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�03�00027�NOMBCLI�NOMBRE DEL CLIENTE  �A�030�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�04�00057�SDISPON�SALDO DISPONIBLE    �S�015�2�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�05�00072�MDISPON�MONEDA SALDO DISPON �A�003�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�06�00075�SDISPUE�SALDO DISPUE CONTRAT�S�015�2�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�07�00090�MDISPUE�MONEDA SALDO DISPUE �A�003�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�08�00093�SDISPUB�SALDO DISPUESTO BIMO�S�015�2�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�09�00108�MDISPUB�MONEDA SALDO DIS BIM�A�003�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�10�00111�IPROTAR�IDENTIF PROD TARJETA�A�004�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�11�00115�DPROTAR�DESCRIP PROD TARJETA�A�030�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�12�00145�ILIMCRE�LIMITE DE CREDITO   �N�015�2�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�13�00160�DLIMCRE�MONEDA LIMITE CREDIT�A�003�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�14�00163�TIPTARJ�TIPO DE TARJETA     �A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�15�00164�DTITARJ�DESCRIP TIPO TARJETA�A�010�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�16�00174�SOPFISS�SOPORTE FISICO      �A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�17�00175�DSOPFIS�DESCRIP SOPORTE FISI�A�020�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�18�00195�IDTNUCO�ID TIPO NRO CONTRATO�A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�19�00196�DETNUCO�DESCRIP TIPO NRO CON�A�030�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�20�00226�ESTARJS�ID ESTADO DE TARJETA�A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�21�00227�DESTARJ�DESCRIP ESTADO TARJE�A�020�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�22�00247�IDMATAR�ID MARCA ASOC TARJET�A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�23�00248�NOMATAR�NOMBRE MARCA ASOC   �A�020�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�24�00268�IDCOREL�ID CONTRATO RELACION�A�060�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�25�00328�NUCOREL�NRO CONTRATO RELACIO�A�020�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�26�00348�IDTCORE�ID TIPO NRO CONT REL�A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�27�00349�DETCORE�DESCRIP TIPO NRO CON�A�030�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�28�00379�IDIMGT1�ID DE LA IMAGEN 1   �A�020�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�29�00399�NIMGTA1�NOMBRE DE IMAGEN 1  �A�016�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�30�00415�IMGTAR1�IMAGEN 1 DE LA TARJE�A�143�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�31�00558�IDIMGT2�ID DE LA IMAGEN 1   �A�020�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�32�00578�NIMGTA2�NOMBRE DE IMAGEN 2  �A�016�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�33�00594�IMGTAR2�IMAGEN 2 DE LA TARJE�A�143�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�34�00737�IDEBLOQ�IDENTIF.DEL BLOQUEO �A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�35�00738�DESBLOQ�DESC.ID.DE BLOQUEO  �A�020�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�36�00758�IDERAZO�ID.RAZON DE BLOQUEO �A�002�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�37�00760�DESRAZO�DESC RAZON DE BLOQ  �A�030�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�38�00790�FECBLOQ�FECHA DE BLOQUEO    �A�010�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�39�00800�IDACTBL�IND BLOQ/DESBLOQ    �A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�40�00801�CODACTV�CODIGO DE ACTIVACION�A�002�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�41�00803�INDACTV�INDICADOR ACTIVACION�A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�42�00804�IDPART �ID PARTICIPANTE     �A�060�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�43�00864�NOMPART�NOMBRES PARTIC.     �A�040�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�44�00904�APEPART�APELLIDOS PARTIC.   �A�040�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�45�00944�TIPPART�TIPO PARTICIPANTE   �A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�46�00945�DESPART�DESC PARTICIPANTE   �A�010�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�47�00955�CODRETI�CODIGO RETIRO       �A�002�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�48�00957�INDRETI�INDICADOR RETIRO    �A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�49�00958�CODINTE�CODIGO INTERNET     �A�002�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�50�00960�INDINTE�INDICADOR INTERNET  �A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�51�00961�CODCOEX�CODIGO COMPRAS EXTER�A�002�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�52�00963�INDCOEX�IND COMPRAS EXTERIOR�A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�53�00964�FECSITT�FECHA SIT. TARJETA  �A�010�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�54�00974�IDCORTV�TARJETA-CONTRATO    �A�060�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�55�01034�NUCORTV�TARJETA FISICA      �A�020�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�56�01054�IDTCOTV�ID.TIPO             �A�001�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�57�01055�DETCOTV�DESCRIPCION TIPO    �A�030�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�58�01085�CODSOBR�CODIGO SOBREGIRO    �A�002�0�S�        �
 * MPMS1L1 �LISTADO DE TARJETAS           �X�59�01087�59�01087�INDSOBR�IND SOBREGIRO       �A�001�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS2L1.D1200603.txt
 * MPMS2L1 �FORMATO DE PAGINACION         �X�02�00019�01�00001�IDPAGIN�IDENTIFICADOR DE PAG�A�016�0�S�        �
 * MPMS2L1 �FORMATO DE PAGINACION         �X�02�00019�02�00017�TAMPAGI�NRO REG A DEVOLVER  �N�003�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPL1.D1200603.txt
 * MPL1MPMS1L1 MPNCS1L1MP2CMPL11S                             XP86289 2019-08-27-12.00.50.974374XP86289 2019-08-27-12.00.50.974392
 * MPL1MPMS2L1 MPNCS2L1MP2CMPL11S                             XP91612 2017-01-26-13.50.31.081115XP93683 2018-10-16-16.52.46.592754
 *
</pre></code>
 *
 * @see RespuestaTransaccionMpl1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPL1",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpl1.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENL1.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionMpl1 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}

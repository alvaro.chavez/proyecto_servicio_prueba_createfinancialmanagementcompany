package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class DTOIntItemizeFeeUnit {

    @NotNull(groups = {ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    @Digits(integer = 10, fraction = 2, groups = ValidationGroup.CreateCardsProposal.class)
    private BigDecimal amount;

    @NotNull(groups = {ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    @Size(max = 3, groups = ValidationGroup.CreateCardsProposal.class)
    private String currency;

    @NotNull(groups = {ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    private String unitType;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }
}

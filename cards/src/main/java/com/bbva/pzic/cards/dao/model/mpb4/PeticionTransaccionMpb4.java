package com.bbva.pzic.cards.dao.model.mpb4;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPB4</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpb4</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpb4</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPB4.D1171023
 * MPB4DESBLOQUEO PF DE TARJETAS          MP        MP2CMPB4PBDMPPO MPM0B4E             MPB4  NS3000CNNNNN    SSTN    C   NNNNSNNN  NN                2017-08-24XP92460 2017-08-2415.52.36XP92460 2017-08-24-12.18.21.967891XP92460 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0B4E.D1171023
 * MPM0B4E �DESBLOQUEO PF DE TARJETAS     �F�04�00020�01�00001�IDETARJ�NUMERO TARJETA      �A�016�0�R�        �
 * MPM0B4E �DESBLOQUEO PF DE TARJETAS     �F�04�00020�02�00017�IDEBLOQ�INDENTIF.DEL BLOQUEO�A�001�0�R�        �
 * MPM0B4E �DESBLOQUEO PF DE TARJETAS     �F�04�00020�03�00018�IDERAZO�ID.RAZON DE BLOQUEO �A�002�0�R�        �
 * MPM0B4E �DESBLOQUEO PF DE TARJETAS     �F�04�00020�04�00020�IDACTBL�INDICADOR DESBLOQUEO�A�001�0�O�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0B4S.D1171023.txt
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPB4.D1171023.txt
</pre></code>
 *
 * @see RespuestaTransaccionMpb4
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPB4",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpb4.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPM0B4E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpb4 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntAdditionalProduct {

    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    @Size(max = 1, groups = ValidationGroup.CreateCardsProposal.class)
    private String productType;

    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    @Digits(integer = 10, fraction = 2, groups = ValidationGroup.CreateCardsProposal.class)
    private BigDecimal amount;

    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    @Size(max = 3, groups = ValidationGroup.CreateCardsProposal.class)
    private String currency;

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
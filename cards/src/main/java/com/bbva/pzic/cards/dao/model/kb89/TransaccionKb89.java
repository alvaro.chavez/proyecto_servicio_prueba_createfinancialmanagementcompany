package com.bbva.pzic.cards.dao.model.kb89;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>KB89</code>
 * 
 * @see PeticionTransaccionKb89
 * @see RespuestaTransaccionKb89
 */
@Component
public class TransaccionKb89 implements InvocadorTransaccion<PeticionTransaccionKb89,RespuestaTransaccionKb89> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionKb89 invocar(PeticionTransaccionKb89 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionKb89.class, RespuestaTransaccionKb89.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionKb89 invocarCache(PeticionTransaccionKb89 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionKb89.class, RespuestaTransaccionKb89.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
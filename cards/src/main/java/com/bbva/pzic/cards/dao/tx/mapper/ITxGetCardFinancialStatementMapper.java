package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCardStatement;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPME1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS2GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS3GH;

/**
 * Created on 26/07/2018.
 *
 * @author Entelgy
 */
public interface ITxGetCardFinancialStatementMapper {
    FormatoMPME1GH mapIn(InputGetCardFinancialStatement dtoIn);

    DTOIntCardStatement mapOut(FormatoMPMS1GH formatOutput);

    DTOIntCardStatement mapOut2(FormatoMPMS2GH formatOutput, DTOIntCardStatement dtoOut);

    DTOIntCardStatement mapOut3(FormatoMPMS3GH formatOutput, DTOIntCardStatement dtoOut);
}

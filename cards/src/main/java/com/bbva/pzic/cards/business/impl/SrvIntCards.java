package com.bbva.pzic.cards.business.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.ISrvIntCards;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.ICardsDAO;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Entelgy
 */
@Component
public class SrvIntCards implements ISrvIntCards {

    private static final Log LOG = LogFactory.getLog(SrvIntCards.class);

    @Autowired
    private Validator validator;

    @Autowired
    private ICardsDAO cardsDAO;

    @Override
    public CardData createCard(final DTOIntCard input) {
        LOG.info("... called method SrvIntCards.createCard ...");
        LOG.info("... validating createCard input parameter ...");
        validator.validate(input, ValidationGroup.CreateCard.class);
        return cardsDAO.createCard(input);
    }

    @Override
    public BlockData modifyCardBlock(final DTOIntBlock block) {
        LOG.info("... called method SrvIntCards.modifyCardBlock ...");
        LOG.info("... validating modifyCardBlock input parameter ...");
        validator.validate(block, ValidationGroup.ModifyCardBlock.class);
        return cardsDAO.modifyCardBlock(block);
    }

    @Override
    public void modifyCardActivation(final DTOIntActivation activation) {
        LOG.info("... called method SrvIntCards.modifyCardActivation ...");
        LOG.info("... validating modifyCardActivation input parameter ...");
        validator.validate(activation, ValidationGroup.ModifyCardActivation.class);
        cardsDAO.modifyCardActivation(activation);
    }

    @Override
    public DTOOutListCards listCards(final DTOIntListCards dtoIn) {
        LOG.info("... called method SrvIntCards.listCards ...");
        LOG.info("... validating listCards input parameter ...");
        validator.validate(dtoIn, ValidationGroup.ListCards.class);
        return cardsDAO.listCards(dtoIn);
    }

    @Override
    public TransactionsData listCardTransactions(final DTOInputListCardTransactions queryFilter) {
        LOG.info("... called method SrvIntCards.listCardTransactions ...");
        LOG.info("... validating listCardTransactions input parameter ...");
        validator.validate(queryFilter);
        return cardsDAO.listCardTransactions(queryFilter);
    }

    @Override
    public DTOOutCreateCardRelatedContract createCardRelatedContract(final DTOInputCreateCardRelatedContract dtoIn) {
        LOG.info("... called method SrvIntCards.createRelatedContract ...");
        LOG.info("... validating listTransactions input parameter ...");
        validator.validate(dtoIn, ValidationGroup.CreateRelatedContract.class);
        return cardsDAO.createCardRelatedContract(dtoIn);
    }

    @Override
    public SecurityData getCardSecurityData(final InputGetCardSecurityData input) {
        LOG.info("... invoke method SrvIntCards.getCardSecurityData ...");
        LOG.info("... validating getCardSecurityData input parameter ...");
        validator.validate(input, ValidationGroup.GetCardSecurityData.class);
        if (input.getPublicKey().length() <= 225) {
            throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING);
        }
        return cardsDAO.getCardSecurityData(input);
    }

    @Override
    public InstallmentsPlanData createCardInstallmentsPlan(final DTOIntCardInstallmentsPlan dtoIn) {
        LOG.info("... invoke method SrvIntCards.createCardInstallmentsPlan ...");
        LOG.info("... validating createCardInstallmentsPlan input parameter ...");
        validator.validate(dtoIn, ValidationGroup.CreateInstallmentsPlan.class);
        return cardsDAO.createInstallmentsPlan(dtoIn);
    }

    @Override
    public InstallmentsPlanSimulationData simulateCardInstallmentsPlanV00(final DTOIntCardInstallmentsPlan dtoIn) {
        LOG.info("... invoke method SrvIntCards.simulateCardInstallmentsPlanV00 ...");
        LOG.info("... validating simulateCardInstallmentsPlanV00 input parameter ...");
        validator.validate(dtoIn, ValidationGroup.SimulateInstallmentsPlanV00.class);
        return cardsDAO.simulateInstallmentsPlan(dtoIn);
    }

    @Override
    public void modifyCard(final DTOIntCard dtoInt) {
        LOG.info("... invoke method SrvIntCards.modifyCard ...");
        LOG.info("... validating modifyCard input parameter ...");
        validator.validate(dtoInt, ValidationGroup.ModifyCard.class);
        cardsDAO.modifyCard(dtoInt);
    }

    @Override
    public void modifyPartialCardBlock(final DTOIntBlock dtoInt) {
        LOG.info("... invoke method SrvIntCards.modifyPartialCardBlock ...");
        LOG.info("... validating modifyPartialCardBlock input parameter ...");
        validator.validate(dtoInt, ValidationGroup.ModifyPartialCardBlock.class);
        cardsDAO.modifyPartialCardBlock(dtoInt);
    }

    @Override
    public List<Activation> listCardActivations(final DTOIntCard dtoInt) {
        LOG.info("... invoke method SrvIntCards.listCardActivations ...");
        LOG.info("... validating listCardActivations input parameter ...");
        validator.validate(dtoInt, ValidationGroup.ListCardActivations.class);
        return cardsDAO.listCardActivations(dtoInt);
    }

    @Override
    public List<Activation> modifyCardActivations(final InputModifyCardActivations dtoInt) {
        LOG.info("... invoke method SrvIntCards.modifyCardActivations ...");
        LOG.info("... validating modifyCardActivations input parameter ...");
        validator.validate(dtoInt, ValidationGroup.ModifyCardActivations.class);
        return cardsDAO.modifyCardActivations(dtoInt);
    }

    @Override
    public void modifyCardPin(final DTOIntPin dtoInt) {
        LOG.info("... invoke method SrvIntCards.modifyCardPin ...");
        LOG.info("... validating modifyCardPin input parameter ...");
        validator.validate(dtoInt, ValidationGroup.ModifyCardPin.class);
        cardsDAO.modifyCardPin(dtoInt);
    }

    @Override
    public CardProposal createCardsCardProposal(final InputCreateCardsCardProposal input) {
        LOG.info("... Invoking method SrvIntCards.createCardsCardProposal ...");
        LOG.info("... Validating createCardsCardProposal input parameter ...");
        validator.validate(input, ValidationGroup.CreateCardsCardProposal.class);

        final List<DTOIntParticipantContactDetail> contactDetails = input.getCardProposal().getParticipant().getContactDetails();
        validator.validate(contactDetails.get(0), ValidationGroup.CreateCardsCardProposal.FirstContactDetailsContactValue.class);
        validator.validate(contactDetails.get(1), ValidationGroup.CreateCardsCardProposal.SecondContactDetailsContactValue.class);

        return cardsDAO.createCardsCardProposal(input);
    }
}

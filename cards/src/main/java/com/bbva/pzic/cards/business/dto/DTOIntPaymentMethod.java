package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntPaymentMethod {
    @NotNull(groups = {ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.ModifyCardProposal.class,
            ValidationGroup.CreateCardReportsV0.class})
    @Size(max = 3, groups = ValidationGroup.CreateCardsProposal.class)
    private String id;

    @Valid
    private DTOIntPeriod period;

    @Digits(integer = 2, fraction = 0, groups = ValidationGroup.CreateCardsProposal.class)
    private Integer endDay;

    private String name;

    @Valid
    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    private DTOIntFrequency frequency;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private DTOIntFrequency frecuency;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntPeriod getPeriod() {
        return period;
    }

    public void setPeriod(DTOIntPeriod period) {
        this.period = period;
    }

    public Integer getEndDay() {
        return endDay;
    }

    public void setEndDay(Integer endDay) {
        this.endDay = endDay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(DTOIntFrequency frequency) {
        this.frequency = frequency;
    }

    public DTOIntFrequency getFrecuency() {
        return frecuency;
    }

    public void setFrecuency(DTOIntFrequency frecuency) {
        this.frecuency = frecuency;
    }
}
package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>daysOfMonth</code>, utilizado por la clase <code>Frecuency</code></p>
 * 
 * @see Frecuency
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Daysofmonth {
	
	/**
	 * <p>Campo <code>day</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "day", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 2, signo = true, obligatorio = true)
	private String day;
	
	/**
	 * <p>Campo <code>cutOffDay</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "cutOffDay", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 2, signo = true)
	private String cutoffday;
	
}
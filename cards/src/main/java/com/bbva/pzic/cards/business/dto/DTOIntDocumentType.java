package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntDocumentType {

    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(max = 1, groups = ValidationGroup.CreateCardsCardProposal.class)
    private String id;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
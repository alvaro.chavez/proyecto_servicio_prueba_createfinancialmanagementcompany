package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "accumulatedAmount", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "accumulatedAmount", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccumulatedAmount implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Total amount of the all consumption made in the period of time.
     */
    private BigDecimal amount;
    /**
     * String based on ISO-4217 for specifying the currency of cumulative amount
     * of the consumption made in the period of time.
     */
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
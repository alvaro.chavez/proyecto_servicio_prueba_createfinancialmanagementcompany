package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 03/10/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "DeliveriesManagement", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "DeliveriesManagement", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeliveriesManagement implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Delivery manager identifier.
     */
    private String id;
    /**
     * Delivery service type of a product or document.
     * These can be physical or digital, depending on the case the delivery will be at an address or a contact.
     */
    private ServiceType serviceType;
    /**
     * Address where the product or document will be delivered.
     * This can be an address that the customer enters manually or and address previously selected by the customer.
     */
    private Address address;

    private ContactDetail contactDetail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ContactDetail getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(ContactDetail contactDetail) {
        this.contactDetail = contactDetail;
    }
}

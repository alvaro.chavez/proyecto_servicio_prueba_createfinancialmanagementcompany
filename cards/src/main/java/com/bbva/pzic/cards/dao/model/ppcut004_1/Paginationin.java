package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>paginationIn</code>, utilizado por la clase <code>PeticionTransaccionPpcut004_1</code></p>
 * 
 * @see PeticionTransaccionPpcut004_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Paginationin {
	
	/**
	 * <p>Campo <code>pageSize</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "pageSize", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true)
	private Integer pagesize;
	
	/**
	 * <p>Campo <code>paginationKey</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "paginationKey", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
	private String paginationkey;
	
}
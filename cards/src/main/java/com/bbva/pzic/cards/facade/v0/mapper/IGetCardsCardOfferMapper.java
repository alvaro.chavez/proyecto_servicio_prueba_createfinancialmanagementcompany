package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardsCardOffer;
import com.bbva.pzic.cards.canonic.Offer;

public interface IGetCardsCardOfferMapper {

    InputGetCardsCardOffer mapIn(String cardId, String offerId);

    ServiceResponse<Offer> mapOut(Offer offer);

}

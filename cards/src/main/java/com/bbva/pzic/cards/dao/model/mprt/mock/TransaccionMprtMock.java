package com.bbva.pzic.cards.dao.model.mprt.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.dao.model.mprt.PeticionTransaccionMprt;
import com.bbva.pzic.cards.dao.model.mprt.RespuestaTransaccionMprt;
import org.springframework.stereotype.Component;

/**
 * Created on 04/10/2018.
 *
 * @author Entelgy
 */
@Component("transaccionMprt")
public class TransaccionMprtMock implements InvocadorTransaccion<PeticionTransaccionMprt, RespuestaTransaccionMprt> {

    @Override
    public RespuestaTransaccionMprt invocar(PeticionTransaccionMprt peticionTransaccionMprt) throws ExcepcionTransaccion {
        RespuestaTransaccionMprt response = new RespuestaTransaccionMprt();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        return response;
    }

    @Override
    public RespuestaTransaccionMprt invocarCache(PeticionTransaccionMprt peticionTransaccionMprt) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputCreateCardsMaskedToken;
import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMEN2F;
import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMS12F;
import com.bbva.pzic.cards.dao.model.mp2f.PeticionTransaccionMp2f;
import com.bbva.pzic.cards.dao.model.mp2f.RespuestaTransaccionMp2f;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardsMaskedTokenMapper;
import com.bbva.pzic.cards.facade.v0.dto.MaskedToken;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 23/07/2019.
 *
 * @author Entelgy
 */
@Component("txCreateCardsMaskedToken")
public class TxCreateCardsMaskedToken
        extends SingleOutputFormat<InputCreateCardsMaskedToken, FormatoMPMEN2F, MaskedToken, FormatoMPMS12F> {

    @Resource(name = "txCreateCardsMaskedTokenMapper")
    private ITxCreateCardsMaskedTokenMapper mapper;

    @Autowired
    public TxCreateCardsMaskedToken(@Qualifier("transaccionMp2f") InvocadorTransaccion<PeticionTransaccionMp2f, RespuestaTransaccionMp2f> transaction) {
        super(transaction, PeticionTransaccionMp2f::new, MaskedToken::new, FormatoMPMS12F.class);
    }

    @Override
    protected FormatoMPMEN2F mapInput(InputCreateCardsMaskedToken inputCreateCardsMaskedToken) {
        return mapper.mapIn(inputCreateCardsMaskedToken);
    }

    @Override
    protected MaskedToken mapFirstOutputFormat(FormatoMPMS12F formatoMPMS12F, InputCreateCardsMaskedToken inputCreateCardsMaskedToken, MaskedToken maskedToken) {
        return mapper.mapOut(formatoMPMS12F);
    }
}

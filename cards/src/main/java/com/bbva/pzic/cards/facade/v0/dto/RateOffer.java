package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "rateOffer", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "rateOffer", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RateOffer implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     *Types of rates on credit.
     */
    private RateTypeOffer rateType;
    /**
     * Detail mode in which the collection of the rate is applied.
     */
    private Unit unit;

    public RateTypeOffer getRateType() {
        return rateType;
    }

    public void setRateType(RateTypeOffer rateType) {
        this.rateType = rateType;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}

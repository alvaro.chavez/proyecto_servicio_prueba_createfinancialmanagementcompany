// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.pcpst007_1;

import com.bbva.pzic.cards.dao.model.pcpst007_1.Card;
import com.bbva.pzic.cards.dao.model.pcpst007_1.Product;

privileged aspect Card_Roo_JavaBean {
    
    /**
     * Gets product value
     * 
     * @return Product
     */
    public Product Card.getProduct() {
        return this.product;
    }
    
    /**
     * Sets product value
     * 
     * @param product
     * @return Card
     */
    public Card Card.setProduct(Product product) {
        this.product = product;
        return this;
    }
    
}

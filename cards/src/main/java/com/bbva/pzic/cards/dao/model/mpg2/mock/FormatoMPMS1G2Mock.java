package com.bbva.pzic.cards.dao.model.mpg2.mock;

import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMS1G2;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 6/10/2017.
 *
 * @author Entelgy
 */
public class FormatoMPMS1G2Mock {

    private final ObjectMapperHelper mapper;

    public FormatoMPMS1G2Mock() {
        mapper = ObjectMapperHelper.getInstance();
    }

    public List<FormatoMPMS1G2> getFormatoMPMS1G2s() throws IOException {
        return mapper.readValue(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(
                        "com/bbva/pzic/cards/dao/model/mpg2/mock/formatoMPMS1G2.json"),
                new TypeReference<List<FormatoMPMS1G2>>() {
                });
    }


}

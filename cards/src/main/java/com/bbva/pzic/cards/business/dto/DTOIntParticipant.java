package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class DTOIntParticipant {

    @NotNull(groups = ValidationGroup.CreateCardProposal.class)
    private String id;
    @NotNull(groups = ValidationGroup.CreateCardProposal.class)
    private String personType;
    @NotNull(groups = ValidationGroup.CreateCardProposal.class)
    private String participantTypeId;
    private String legalPersonTypeId;
    private Boolean isCustomer;

    @Valid
    private DTOIntIdentityDocument identityDocument;

    @Valid
    private DTOIntCard card;

    private String offerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public String getParticipantTypeId() {
        return participantTypeId;
    }

    public void setParticipantTypeId(String participantTypeId) {
        this.participantTypeId = participantTypeId;
    }

    public String getLegalPersonTypeId() {
        return legalPersonTypeId;
    }

    public void setLegalPersonTypeId(String legalPersonTypeId) {
        this.legalPersonTypeId = legalPersonTypeId;
    }

    public DTOIntIdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(DTOIntIdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public DTOIntCard getCard() {
        return card;
    }

    public void setCard(DTOIntCard card) {
        this.card = card;
    }

    public Boolean getIsCustomer() {
        return isCustomer;
    }

    public void setIsCustomer(Boolean customer) {
        isCustomer = customer;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}

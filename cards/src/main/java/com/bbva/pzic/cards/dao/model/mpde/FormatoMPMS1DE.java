package com.bbva.pzic.cards.dao.model.mpde;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS1DE</code> de la transacci&oacute;n <code>MPDE</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS1DE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS1DE {

	/**
	 * <p>Campo <code>IDENDEP</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDENDEP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String idendep;

	/**
	 * <p>Campo <code>DESCDEP</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "DESCDEP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String descdep;

	/**
	 * <p>Campo <code>MONEDEP</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "MONEDEP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String monedep;

	/**
	 * <p>Campo <code>DEUDMES</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 4, nombre = "DEUDMES", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal deudmes;

	/**
	 * <p>Campo <code>CAPCUOM</code>, &iacute;ndice: <code>5</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 5, nombre = "CAPCUOM", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal capcuom;

	/**
	 * <p>Campo <code>INTCUOM</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 6, nombre = "INTCUOM", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal intcuom;

	/**
	 * <p>Campo <code>FECVENC</code>, &iacute;ndice: <code>7</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 7, nombre = "FECVENC", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecvenc;

	/**
	 * <p>Campo <code>MONTOTF</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 8, nombre = "MONTOTF", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal montotf;

	/**
	 * <p>Campo <code>CAPABON</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 9, nombre = "CAPABON", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal capabon;

	/**
	 * <p>Campo <code>INTABON</code>, &iacute;ndice: <code>10</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 10, nombre = "INTABON", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal intabon;

	/**
	 * <p>Campo <code>TOTCANC</code>, &iacute;ndice: <code>11</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 11, nombre = "TOTCANC", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal totcanc;

	/**
	 * <p>Campo <code>TCUOPEN</code>, &iacute;ndice: <code>12</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 12, nombre = "TCUOPEN", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal tcuopen;

	/**
	 * <p>Campo <code>INTERES</code>, &iacute;ndice: <code>13</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 13, nombre = "INTERES", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal interes;

	/**
	 * <p>Campo <code>TOTDEUD</code>, &iacute;ndice: <code>14</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 14, nombre = "TOTDEUD", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal totdeud;

	/**
	 * <p>Campo <code>MTOSOLI</code>, &iacute;ndice: <code>15</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 15, nombre = "MTOSOLI", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal mtosoli;

	/**
	 * <p>Campo <code>FECINIC</code>, &iacute;ndice: <code>16</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 16, nombre = "FECINIC", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecinic;

	/**
	 * <p>Campo <code>HORINIC</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "HORINIC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String horinic;

	/**
	 * <p>Campo <code>FECCONT</code>, &iacute;ndice: <code>18</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 18, nombre = "FECCONT", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date feccont;

	/**
	 * <p>Campo <code>HORCONT</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "HORCONT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String horcont;

	/**
	 * <p>Campo <code>FECHFIN</code>, &iacute;ndice: <code>20</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 20, nombre = "FECHFIN", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fechfin;

	/**
	 * <p>Campo <code>NUMCUOT</code>, &iacute;ndice: <code>21</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 21, nombre = "NUMCUOT", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer numcuot;

	/**
	 * <p>Campo <code>CUOPAGA</code>, &iacute;ndice: <code>22</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 22, nombre = "CUOPAGA", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer cuopaga;

	/**
	 * <p>Campo <code>CUOPDTE</code>, &iacute;ndice: <code>23</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 23, nombre = "CUOPDTE", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer cuopdte;

	/**
	 * <p>Campo <code>CUOVNPA</code>, &iacute;ndice: <code>24</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 24, nombre = "CUOVNPA", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer cuovnpa;

	/**
	 * <p>Campo <code>FRECCUO</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 25, nombre = "FRECCUO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String freccuo;

	/**
	 * <p>Campo <code>IDEOFEA</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "IDEOFEA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 21, longitudMaxima = 21)
	private String ideofea;

}
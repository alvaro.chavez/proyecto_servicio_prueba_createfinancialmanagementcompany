package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntCardType {
    @NotNull(groups = {
            ValidationGroup.CreateCardsCardProposal.class,
            ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.CreateCardProposal.class
    })
    @Size(max = 1, groups = {
            ValidationGroup.CreateCardsCardProposal.class,
            ValidationGroup.CreateCardsProposal.class
    })
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/12/2018.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "holderSimulation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "holderSimulation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class HolderSimulation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Characteristics of the associated offer.
     */
    private OfferType offerType;
    /**
     * Detailed information according to the offer type.
     */
    private CardHolderSimulation details;

    public OfferType getOfferType() {
        return offerType;
    }

    public void setOfferType(OfferType offerType) {
        this.offerType = offerType;
    }

    public CardHolderSimulation getDetails() {
        return details;
    }

    public void setDetails(CardHolderSimulation details) {
        this.details = details;
    }
}

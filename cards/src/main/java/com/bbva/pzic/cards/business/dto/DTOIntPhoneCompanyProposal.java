package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 27/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntPhoneCompanyProposal {

    @NotNull(groups = {ValidationGroup.CreateCardProposal.class})
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

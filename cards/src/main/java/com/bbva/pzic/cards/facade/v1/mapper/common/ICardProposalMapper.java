package com.bbva.pzic.cards.facade.v1.mapper.common;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v1.dto.*;

import java.util.List;

/**
 * Created on 26/06/2020.
 *
 * @author Entelgy.
 */
public interface ICardProposalMapper {

    DTOIntCardType mapInCardType(CardType cardType);

    DTOIntProduct mapInProduct(Product product);

    DTOIntPhysicalSupport mapInPhysicalSupport(PhysicalSupport physicalSupport);

    List<DTOIntDelivery> mapInDeliveries(List<Delivery> deliveries);

    DTOIntPaymentMethod mapInPaymentMethod(PaymentMethod paymentMethod);

    List<DTOIntImport> mapInGrantedCredits(List<Import> grantedCredits);

    DTOIntSpecificProposalContact mapInContact(SpecificProposalContact contact);

    DTOIntRate mapInRates(ProposalRate rates);

    DTOIntFee mapInFees(TransferFees fees);

    List<DTOIntAdditionalProduct> mapInAdditionalProducts(List<AdditionalProduct> additionalProducts);

    DTOIntMembership mapInMembership(Membership membership);

    DTOIntImage mapInImage(Image image);

    DTOIntContactAbility mapInContactAbility(ContactAbility contactability);

    List<DTOIntParticipant> mapInParticipants(List<KnowParticipant> participantList);

}

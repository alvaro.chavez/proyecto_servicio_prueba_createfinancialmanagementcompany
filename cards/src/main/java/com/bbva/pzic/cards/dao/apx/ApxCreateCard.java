package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateCardMapper;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Entityin;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.PeticionTransaccionPpcutc01_1;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.RespuestaTransaccionPpcutc01_1;
import com.bbva.pzic.cards.facade.v1.dto.CardPost;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
@Component
public class ApxCreateCard {

    @Resource(name = "apxCreateCardMapper")
    private IApxCreateCardMapper mapper;

    @Resource(name = "transaccionPpcutc01_1")
    private InvocadorTransaccion<PeticionTransaccionPpcutc01_1, RespuestaTransaccionPpcutc01_1> transaccion;

    public CardPost perform(final DTOIntCard dtoInt) {
        Entityin entityIn = mapper.mapIn(dtoInt);

        PeticionTransaccionPpcutc01_1 request = new PeticionTransaccionPpcutc01_1();
        request.setEntityin(entityIn);

        RespuestaTransaccionPpcutc01_1 response = transaccion.invocar(request);

        return mapper.mapOut(response.getEntityout());
    }
}

package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "itemizeRateUnit", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "itemizeRateUnit", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemizeRateUnit implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Monetary amount.
     */
    private BigDecimal amount;
    /**
     * String based on ISO-4217 for specifying the currency related to the amount.
     */
    private String currency;
    /**
     * Unit Rate Type.
     */
    private String unitRateType;
    /**
     * Percentage to be charged.
     */
    private BigDecimal percentage;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getUnitRateType() {
        return unitRateType;
    }

    public void setUnitRateType(String unitRateType) {
        this.unitRateType = unitRateType;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
}

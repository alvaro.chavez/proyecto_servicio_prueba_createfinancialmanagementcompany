package com.bbva.pzic.cards.dao.model.mpws.mock;

import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0DET;
import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0WSC;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
public class FormatMpwsMock {

    private ObjectMapperHelper objectMapper;

    public FormatMpwsMock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public FormatoMPM0WSC getFormatoMPM0WSC() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpws/mock/formatoMPM0WSC.json"), FormatoMPM0WSC.class);
    }

    public List<FormatoMPM0DET> getFormatoMPM0DET() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpws/mock/formatoMPM0DET.json"), new TypeReference<List<FormatoMPM0DET>>() {
        });
    }

    public List<FormatoMPM0DET> getFormatoMPM0DETEmpty() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpws/mock/formatoMPM0DETEmpty.json"), new TypeReference<List<FormatoMPM0DET>>() {
        });
    }

    public FormatoMPM0WSC getFormatoMPM0WSCEmpty() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpws/mock/formatoMPM0WSCEmpty.json"), FormatoMPM0WSC.class);
    }

}

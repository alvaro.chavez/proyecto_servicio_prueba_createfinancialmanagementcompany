package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOInputCreateCardRelatedContract;
import com.bbva.pzic.cards.business.dto.DTOOutCreateCardRelatedContract;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1E;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1S;

/**
 * Created on 14/02/2017.
 *
 * @author Entelgy
 */
public interface ITxCreateCardRelatedContractMapper {
    /**
     * Method that create an input FormatoMPM0V1E with the entered data
     *
     * @param dtoIn DTO with the datas of input validate
     * @return Format of input
     */
    FormatoMPM0V1E mapIn(DTOInputCreateCardRelatedContract dtoIn);

    /**
     * Method that creates an output DTO from an output format
     *
     * @param formatOutput Format of output
     * @param dtoIn
     * @return DTO of output
     */
    DTOOutCreateCardRelatedContract mapOut(FormatoMPM0V1S formatOutput, DTOInputCreateCardRelatedContract dtoIn);
}

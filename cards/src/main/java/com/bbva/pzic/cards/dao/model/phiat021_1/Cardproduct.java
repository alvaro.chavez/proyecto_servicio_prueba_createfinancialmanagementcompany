package com.bbva.pzic.cards.dao.model.phiat021_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>cardProduct</code>, utilizado por la clase <code>Cardproducts</code></p>
 * 
 * @see Cardproducts
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Cardproduct {
	
	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>name</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "name", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String name;
	
	/**
	 * <p>Campo <code>subproduct</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "subproduct", tipo = TipoCampo.DTO)
	private Subproduct subproduct;
	
	/**
	 * <p>Campo <code>cardType</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "cardType", tipo = TipoCampo.DTO)
	private Cardtype cardtype;
	
	/**
	 * <p>Campo <code>images</code>, &iacute;ndice: <code>5</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 5, nombre = "images", tipo = TipoCampo.LIST)
	private List<Images> images;
	
	/**
	 * <p>Campo <code>grantedCredits</code>, &iacute;ndice: <code>6</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 6, nombre = "grantedCredits", tipo = TipoCampo.LIST)
	private List<Grantedcredits> grantedcredits;
	
	/**
	 * <p>Campo <code>rates</code>, &iacute;ndice: <code>7</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 7, nombre = "rates", tipo = TipoCampo.LIST)
	private List<Rates> rates;
	
	/**
	 * <p>Campo <code>priorityLevel</code>, &iacute;ndice: <code>8</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 8, nombre = "priorityLevel", tipo = TipoCampo.ENTERO, longitudMaxima = 4, signo = true)
	private Integer prioritylevel;
	
	/**
	 * <p>Campo <code>bankIdentificationNumber</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "bankIdentificationNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true)
	private String bankidentificationnumber;
	
	/**
	 * <p>Campo <code>fees</code>, &iacute;ndice: <code>10</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 10, nombre = "fees", tipo = TipoCampo.DTO)
	private Fees fees;
	
	/**
	 * <p>Campo <code>benefits</code>, &iacute;ndice: <code>11</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 11, nombre = "benefits", tipo = TipoCampo.LIST)
	private List<Benefits> benefits;
	
	/**
	 * <p>Campo <code>grantedMinimumCredits</code>, &iacute;ndice: <code>12</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 12, nombre = "grantedMinimumCredits", tipo = TipoCampo.LIST)
	private List<Grantedminimumcredits> grantedminimumcredits;
	
	/**
	 * <p>Campo <code>loyaltyProgram</code>, &iacute;ndice: <code>13</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 13, nombre = "loyaltyProgram", tipo = TipoCampo.DTO)
	private Loyaltyprogram loyaltyprogram;
	
	/**
	 * <p>Campo <code>brandAssociation</code>, &iacute;ndice: <code>14</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 14, nombre = "brandAssociation", tipo = TipoCampo.DTO)
	private Brandassociation brandassociation;
	
	/**
	 * <p>Campo <code>offerId</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "offerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String offerid;
	
}
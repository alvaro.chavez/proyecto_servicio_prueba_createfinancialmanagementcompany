package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class InputCreateCardReports {

    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String cardId;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String user;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String password;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private DTOIntCardType cardType;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String number;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private DTOIntNumberType numberType;
    @Valid
    private DTOIntBrandAssociation brandAssociation;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private DTOIntProduct product;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private List<DTOIntCurrency> currencies;

    private Boolean hasPrintedHolderName;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private List<DTOIntParticipantReport> participants;
    @Valid
    private DTOIntPaymentMethod paymentMethod;
    @Valid
    private List<DTOIntAmount> grantedCredits;
    @Valid
    private List<DTOIntRelatedContract> relatedContracts;
    @Valid
    private List<DTOIntDelivery> deliveries;
    @Valid
    private DTOIntContractingBranch contractingBranch;
    @Valid
    private DTOIntLoyaltyProgram loyaltyProgram;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String proposalStatus;

    private String offerId;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String channel;

    private Boolean notificationsByOperation;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DTOIntCardType getCardType() {
        return cardType;
    }

    public void setCardType(DTOIntCardType cardType) {
        this.cardType = cardType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DTOIntNumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(DTOIntNumberType numberType) {
        this.numberType = numberType;
    }

    public DTOIntBrandAssociation getBrandAssociation() {
        return brandAssociation;
    }

    public void setBrandAssociation(DTOIntBrandAssociation brandAssociation) {
        this.brandAssociation = brandAssociation;
    }

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }

    public List<DTOIntCurrency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<DTOIntCurrency> currencies) {
        this.currencies = currencies;
    }

    public Boolean getHasPrintedHolderName() {
        return hasPrintedHolderName;
    }

    public void setHasPrintedHolderName(Boolean hasPrintedHolderName) {
        this.hasPrintedHolderName = hasPrintedHolderName;
    }

    public List<DTOIntParticipantReport> getParticipants() {
        return participants;
    }

    public void setParticipants(List<DTOIntParticipantReport> participants) {
        this.participants = participants;
    }

    public DTOIntPaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(DTOIntPaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<DTOIntAmount> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<DTOIntAmount> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public List<DTOIntRelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<DTOIntRelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public List<DTOIntDelivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<DTOIntDelivery> deliveries) {
        this.deliveries = deliveries;
    }

    public DTOIntContractingBranch getContractingBranch() {
        return contractingBranch;
    }

    public void setContractingBranch(DTOIntContractingBranch contractingBranch) {
        this.contractingBranch = contractingBranch;
    }

    public DTOIntLoyaltyProgram getLoyaltyProgram() {
        return loyaltyProgram;
    }

    public void setLoyaltyProgram(DTOIntLoyaltyProgram loyaltyProgram) {
        this.loyaltyProgram = loyaltyProgram;
    }

    public String getProposalStatus() {
        return proposalStatus;
    }

    public void setProposalStatus(String proposalStatus) {
        this.proposalStatus = proposalStatus;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Boolean getNotificationsByOperation() {
        return notificationsByOperation;
    }

    public void setNotificationsByOperation(Boolean notificationsByOperation) {
        this.notificationsByOperation = notificationsByOperation;
    }
}

package com.bbva.pzic.cards.dao.model.mp2f;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MP2F</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMp2f</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMp2f</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MP2F.D1190705.txt
 * MP2FALTA DE TARJETAS - TOKEN           MP        MP1CMP2FPBDMPPO MPMEN2F             MP2F  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2018-09-14XP92348 2018-09-1719.11.30P014658 2018-09-14-09.47.09.477862XP92348 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMEN2F.D1190705.txt
 * MPMEN2F �ALTA DE TARJETAS - TOKEN      �F�02�00022�01�00001�IDETARJ�NUMERO DE TARJETA   �A�016�0�R�        �
 * MPMEN2F �ALTA DE TARJETAS - TOKEN      �F�02�00022�02�00017�FECCAD �FECHA CADUC (MMAAAA)�A�006�0�R�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS12F.D1190705.txt
 * MPMS12F �ALTA DE TARJETA - TOKEN       �X�02�00018�01�00001�IDTOKEN�ID TOKEN            �A�002�0�S�        �
 * MPMS12F �ALTA DE TARJETA - TOKEN       �X�02�00018�02�00003�NROTARJ�NUMERO TARJETA      �A�016�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MP2F.D1190705.txt
 * MP2FMPMS12F MPECS12FMP1CMP2F1S                             XP92348 2018-09-14-09.53.03.098143P014658 2018-09-17-18.30.07.395310
 *
</pre></code>
 *
 * @see RespuestaTransaccionMp2f
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MP2F",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMp2f.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMEN2F.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMp2f implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
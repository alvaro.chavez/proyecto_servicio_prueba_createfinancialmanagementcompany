package com.bbva.pzic.cards.dao.model.mp6j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MP6J</code>
 * 
 * @see PeticionTransaccionMp6j
 * @see RespuestaTransaccionMp6j
 */
@Component
public class TransaccionMp6j implements InvocadorTransaccion<PeticionTransaccionMp6j,RespuestaTransaccionMp6j> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMp6j invocar(PeticionTransaccionMp6j transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMp6j.class, RespuestaTransaccionMp6j.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMp6j invocarCache(PeticionTransaccionMp6j transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMp6j.class, RespuestaTransaccionMp6j.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
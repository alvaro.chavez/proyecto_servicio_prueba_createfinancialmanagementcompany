package com.bbva.pzic.cards.dao.model.mpw1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPME0W1;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPMS1W1;
import com.bbva.pzic.cards.dao.model.mpw1.PeticionTransaccionMpw1;
import com.bbva.pzic.cards.dao.model.mpw1.RespuestaTransaccionMpw1;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created on 12/06/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpw1")
public class TransaccionMpw1Mock implements InvocadorTransaccion<PeticionTransaccionMpw1, RespuestaTransaccionMpw1> {

    public static final String TEST_NULL = "R";
    public static final String TEST_EMPTY = "C";

    private FormatsMpw1Mock mock;

    @PostConstruct
    private void init() {
        mock = new FormatsMpw1Mock();
    }

    @Override
    public RespuestaTransaccionMpw1 invocar(PeticionTransaccionMpw1 peticionTransaccionMpw1) {
        final RespuestaTransaccionMpw1 response = new RespuestaTransaccionMpw1();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        FormatoMPME0W1 formatoMPME0W1 = peticionTransaccionMpw1.getCuerpo().getParte(FormatoMPME0W1.class);
        if (TEST_NULL.equalsIgnoreCase(formatoMPME0W1.getTiprodf())) {
            return response;
        }
        if (TEST_EMPTY.equalsIgnoreCase(formatoMPME0W1.getTiprodf())) {
            response.getCuerpo().getPartes().add(createFormatoSalidaEmpty());
            return response;
        }
        response.getCuerpo().getPartes().add(createFormatoSalidaDefault());
        return response;
    }

    @Override
    public RespuestaTransaccionMpw1 invocarCache(PeticionTransaccionMpw1 peticionTransaccionMpw1) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida createFormatoSalidaDefault() {
        FormatoMPMS1W1 outFormat = mock.getFormatoMPMS1W1();
        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(outFormat);
        return copySalida;
    }

    private CopySalida createFormatoSalidaEmpty() {
        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(new FormatoMPMS1W1());
        return copySalida;
    }
}

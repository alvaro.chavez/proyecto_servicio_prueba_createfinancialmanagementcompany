package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Size;

/**
 * Created on 22/05/2018.
 *
 * @author Entelgy
 */
public class InputGetCardConditions {

    @Size(max = 19, groups = ValidationGroup.GetConditions.class)
    private String cardId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
}
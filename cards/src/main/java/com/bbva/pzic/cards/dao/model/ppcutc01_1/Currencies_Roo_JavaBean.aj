// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.ppcutc01_1;

import com.bbva.pzic.cards.dao.model.ppcutc01_1.Currencies;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Currency;

privileged aspect Currencies_Roo_JavaBean {
    
    /**
     * Gets currency value
     * 
     * @return Currency
     */
    public Currency Currencies.getCurrency() {
        return this.currency;
    }
    
    /**
     * Sets currency value
     * 
     * @param currency
     * @return Currencies
     */
    public Currencies Currencies.setCurrency(Currency currency) {
        this.currency = currency;
        return this;
    }
    
}

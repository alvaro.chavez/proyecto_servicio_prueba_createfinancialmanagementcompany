package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.mp6h.FormatoMPMQ6GS;
import com.bbva.pzic.cards.dao.model.mp6h.FormatoMPMQ6HE;
import com.bbva.pzic.cards.dao.model.mp6h.FormatoMPMQ6HS;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardFinancialStatementV1Mapper;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.Enums;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class TxGetCardFinancialStatementV1Mapper implements ITxGetCardFinancialStatementV1Mapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public FormatoMPMQ6HE mapIn(final InputGetCardFinancialStatement input) {
        FormatoMPMQ6HE format = new FormatoMPMQ6HE();
        format.setIdetarj(input.getCardId());
        format.setIddocta(input.getFinancialStatementId());
        return format;
    }

    @Override
    public FinancialStatement mapOutFormatoMPMQ6HS(final FormatoMPMQ6HS format, final FinancialStatement financialStatement) {
        financialStatement.setId(format.getIddocta());
        financialStatement.setFinancialStatementNumber(format.getNextra());
        financialStatement.setTransactionsNumber(format.getMcnmovi());
        financialStatement.setBusinessDayCutoffDate(buildDate(format.getMcfextr()));
        financialStatement.setCutOffDate(buildDate(format.getMcfexta()));
        financialStatement.setNextPaymentDate(buildDate(format.getMcfrpag()));
        financialStatement.setBillingPeriodDaysNumber(format.getMcndiae());
        financialStatement.setRelatedContracts(mapOutRelatedContracts(format.getNcontcc()));
        financialStatement.setGrantedCredits(mapOutAmounts(format.getMcilmcr(), format.getMonlinc()));
        financialStatement.setDisposedBalance(mapOutDisposedBalance(format.getMcitoex(), format.getNcmotit()));
        financialStatement.setAvailableBalances(mapOutAmounts(format.getMcidbto(), format.getNcmotit()));
        financialStatement.setCurrencies(mapOutCurrencies(format.getNcmotit()));
        mapOutConditions(financialStatement, format);
        mapOutSpecificsAmount(financialStatement, format);
        mapOutPercentageRates(financialStatement, format);
        return financialStatement;
    }

    @Override
    public FinancialStatement mapOutFormatoMPMQ6GS(final FormatoMPMQ6GS format, final FinancialStatement financialStatement) {
        mapOutSecondConditions(financialStatement, format);
        mapOutAmountRates(financialStatement, format);
        mapOutSecondSpecificsAmount(financialStatement, format);
        return financialStatement;
    }

    private DisposedBalanceStatement mapOutDisposedBalance(final BigDecimal amount, final String currency) {
        if (amount == null && currency == null) {
            return null;
        }

        DisposedBalanceStatement result = new DisposedBalanceStatement();
        result.setPendingBalances(mapOutPendingBalances(amount, currency));
        return result;
    }

    private List<AmountFinancialStatement> mapOutPendingBalances(final BigDecimal amount, final String currency) {
        AmountFinancialStatement result = new AmountFinancialStatement();
        result.setAmount(amount);
        result.setCurrency(currency);
        return Collections.singletonList(result);
    }

    private void mapOutConditions(final FinancialStatement financialStatement, final FormatoMPMQ6HS format) {
        if (CollectionUtils.isEmpty(financialStatement.getConditions())) {
            financialStatement.setConditions(initializeConditions());
        }

        financialStatement.getConditions().set(0, mapOutCondition(financialStatement.getConditions().get(0), null, null, null, format.getMoncon3()));
    }

    private List<Condition> initializeConditions() {
        return Stream.generate(Condition::new).limit(2).collect(Collectors.toList());
    }

    private void mapOutSpecificsAmount(final FinancialStatement financialStatement, final FormatoMPMQ6HS format) {
        if (CollectionUtils.isEmpty(financialStatement.getSpecificAmounts())) {
            financialStatement.setSpecificAmounts(initializeSpecificAmounts());
        }

        financialStatement.getSpecificAmounts().set(0, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(0), format.getIdliqp1(), format.getIliqpen(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(1, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(1), format.getIdliqp2(), format.getIliqpbm(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(2, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(2), format.getIdsalm1(), format.getIsalmes(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(3, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(3), format.getIdsalm2(), format.getIsalmbm(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(4, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(4), format.getIdmcit1(), format.getMcitoco(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(5, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(5), format.getIdmcit2(), format.getMcitocb(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(6, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(6), format.getIdmciv1(), format.getMcitoav(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(7, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(7), format.getIdmciv2(), format.getMcitoab(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(8, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(8), format.getIdmcia1(), format.getMcitopa(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(9, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(9), format.getIdmcia2(), format.getMcitopb(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(10, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(10), format.getIdmciu1(), format.getMcidsto(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(11, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(11), format.getIdmciu2(), format.getMcidstb(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(12, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(12), format.getIdmcil1(), format.getMcilibr(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(13, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(13), format.getIdmcil2(), format.getMcilbrb(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(14, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(14), format.getIdmcic1(), format.getMcidtcu(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(15, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(15), format.getIdmcic2(), format.getMcidtcb(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(16, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(16), format.getIdmcpa1(), format.getMcpagmi(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(17, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(17), format.getIdmcpa2(), format.getMcpagmb(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(18, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(18), format.getIdmcpt1(), format.getMcicarg(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(19, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(19), format.getIdmcpt2(), format.getMcicarb(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(20, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(20), format.getIdmcis1(), format.getMcisdoa(), format.getMoncon1()));
        financialStatement.getSpecificAmounts().set(21, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(21), format.getIdmcis2(), format.getMcisdob(), format.getMoncon2()));
        financialStatement.getSpecificAmounts().set(22, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(22), null, null, format.getMoncon3()));
        financialStatement.getSpecificAmounts().set(24, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(24), null, null, format.getMoncon3()));
        financialStatement.getSpecificAmounts().set(26, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(26), null, null, format.getMoncon3()));
        financialStatement.getSpecificAmounts().set(28, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(28), null, null, format.getMoncon3()));
        financialStatement.getSpecificAmounts().set(30, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(30), null, null, format.getMoncon3()));
    }

    private List<SpecificAmount> initializeSpecificAmounts() {
        return Stream.generate(SpecificAmount::new).limit(32).collect(Collectors.toList());
    }

    private SpecificAmount mapOutSpecificAmount(final SpecificAmount specificAmount, final String id, final BigDecimal amount, final String currency) {
        if (id != null) {
            specificAmount.setId(translator.translateBackendEnumValueStrictly(Enums.FINANCIAL_STATEMENT_SPECIFIC_AMOUNTS_ID, id));
        }
        specificAmount.setAmount(mapOutAmount(specificAmount.getAmount(), amount, currency));
        return specificAmount;
    }

    private void mapOutPercentageRates(final FinancialStatement financialStatement, final FormatoMPMQ6HS format) {
        if (financialStatement.getRates() == null) {
            financialStatement.setRates(new Rate());
        }

        if (CollectionUtils.isEmpty(financialStatement.getRates().getItemizeRates())) {
            financialStatement.getRates().setItemizeRates(initializeItemizeRates());
        }

        financialStatement.getRates().getItemizeRates().set(0, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(0), format.getIdavanm(), format.getPortype(), format.getPoravan(), null, null));
        financialStatement.getRates().getItemizeRates().set(1, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(1), format.getIdcompm(), format.getPortype(), format.getPorcomp(), null, null));
        financialStatement.getRates().getItemizeRates().set(2, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(2), format.getIdavabm(), format.getPortype(), format.getPavanbm(), null, null));
        financialStatement.getRates().getItemizeRates().set(3, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(3), format.getIdcombm(), format.getPortype(), format.getPcompbm(), null, null));
        financialStatement.getRates().getItemizeRates().set(4, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(4), null, null, null, null, format.getMoncon3()));
        financialStatement.getRates().getItemizeRates().set(5, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(5), null, null, null, null, format.getMoncon3()));
        financialStatement.getRates().getItemizeRates().set(6, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(6), null, null, null, null, format.getMoncon3()));
        financialStatement.getRates().getItemizeRates().set(7, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(7), null, null, null, null, format.getMoncon3()));
        financialStatement.getRates().getItemizeRates().set(8, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(8), null, null, null, null, format.getMoncon3()));
    }

    private ItemizeRate mapOutItemizeRate(final ItemizeRate itemizeRate, final String rateType, final String unitRateType, final BigDecimal percentage, final BigDecimal amount, final String currency) {
        if (rateType != null) {
            itemizeRate.setRateType(rateType);
        }
        itemizeRate.setItemizeRatesUnit(mapOutItemizeRatesUnit(itemizeRate.getItemizeRatesUnit(), unitRateType, percentage, amount, currency));
        return itemizeRate;
    }

    private ItemizeRateUnit mapOutItemizeRatesUnit(final ItemizeRateUnit itemizeRatesUnit, final String unitRateType, final BigDecimal percentage, final BigDecimal amount, final String currency) {
        if (unitRateType == null && percentage == null && amount == null && currency == null) {
            return null;
        }

        ItemizeRateUnit result = itemizeRatesUnit;
        if (result == null) {
            result = new ItemizeRateUnit();
        }

        if (result.getUnitRateType() == null) {
            result.setUnitRateType(translator.translateBackendEnumValueStrictly(Enums.FINANCIAL_STATEMENT_RATE_UNIT_RATE_TYPE, unitRateType));
        }

        if (result.getUnitRateType() == null) {
            if (percentage == null && amount == null && currency != null) {
                result.setCurrency(currency);
            } else {
                return null;
            }
        } else if ("PERCENTAGE".equalsIgnoreCase(result.getUnitRateType())) {
            result.setPercentage(percentage);
        } else if ("AMOUNT".equalsIgnoreCase(result.getUnitRateType())) {
            if (amount != null) {
                result.setAmount(amount);
            }
            if (currency != null) {
                result.setCurrency(currency);
            }
        } else {
            return null;
        }

        return result;
    }

    private void mapOutSecondConditions(final FinancialStatement financialStatement, final FormatoMPMQ6GS format) {
        if (CollectionUtils.isEmpty(financialStatement.getConditions())) {
            financialStatement.setConditions(initializeConditions());
        }

        financialStatement.getConditions().set(0, mapOutCondition(financialStatement.getConditions().get(0), format.getIditocu(), format.getAmocty3(), format.getMcitocu(), null));
        financialStatement.getConditions().set(1, mapOutCondition(financialStatement.getConditions().get(1), format.getIditcmb(), format.getAmoctyp(), format.getMcitcmb(), format.getMoncon4()));
    }

    private Condition mapOutCondition(final Condition condition, final String id, final String formatType, final BigDecimal amount, final String currency) {
        if (id != null) {
            condition.setId(id);
        }

        condition.setFormat(mapOutFormat(condition.getFormat(), formatType, amount, currency));
        return condition;
    }

    private ConditionFormat mapOutFormat(final ConditionFormat conditionFormat, final String formatType, final BigDecimal amount, final String currency) {
        if (formatType == null && amount == null && currency == null) {
            return null;
        }

        ConditionFormat result = conditionFormat;
        if (result == null) {
            result = new ConditionFormat();
        }

        if (result.getFormatType() == null) {
            result.setFormatType(translator.translateBackendEnumValueStrictly(Enums.FINANCIAL_STATEMENT_CONDITION_FORMAT_TYPE, formatType));
        }

        if (result.getFormatType() == null) {
            if (amount == null && currency != null) {
                result.setAmount(mapOutAmount(result.getAmount(), null, currency));
                return result;
            }
        } else {
            if ("AMOUNT".equalsIgnoreCase(result.getFormatType())) {
                result.setAmount(mapOutAmount(result.getAmount(), amount, currency));
                return result;
            }
        }

        return null;
    }

    private Amount mapOutAmount(final Amount resultAmount, final BigDecimal amount, final String currency) {
        if (amount == null && currency == null) {
            return null;
        }

        Amount result = resultAmount;
        if (result == null) {
            result = new Amount();
        }

        if (amount != null) {
            result.setAmount(amount);
        }
        if (currency != null) {
            result.setCurrency(currency);
        }
        return result;
    }

    private List<Currency> mapOutCurrencies(final String currency) {
        if (currency == null) {
            return null;
        }

        Currency result = new Currency();
        result.setCurrency(currency);
        return Collections.singletonList(result);
    }

    private List<Amount> mapOutAmounts(final BigDecimal amount, String currency) {
        if (amount == null && currency == null) {
            return null;
        }

        Amount result = new Amount();
        result.setAmount(amount);
        result.setCurrency(currency);
        return Collections.singletonList(result);
    }

    private List<RelatedContract> mapOutRelatedContracts(final String id) {
        if (id == null) {
            return null;
        }

        RelatedContract result = new RelatedContract();
        result.setId(id);
        result.setContractId(id);
        return Collections.singletonList(result);
    }

    private Date buildDate(final String date) {
        if (date == null) {
            return null;
        }

        try {
            return DateUtils.toDate(date, "yyyy-MM-dd");
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE);
        }
    }

    private void mapOutSecondSpecificsAmount(final FinancialStatement financialStatement, final FormatoMPMQ6GS format) {
        if (CollectionUtils.isEmpty(financialStatement.getSpecificAmounts())) {
            financialStatement.setSpecificAmounts(initializeSpecificAmounts());
        }

        financialStatement.getSpecificAmounts().set(22, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(22), format.getIdmcig1(), format.getMcipagv(), null));
        financialStatement.getSpecificAmounts().set(23, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(23), format.getIdmcig2(), format.getMcipagb(), format.getMoncon4()));
        financialStatement.getSpecificAmounts().set(24, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(24), format.getIdmcie1(), format.getMciexcl(), null));
        financialStatement.getSpecificAmounts().set(25, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(25), format.getIdmcie2(), format.getMciexcb(), format.getMoncon4()));
        financialStatement.getSpecificAmounts().set(26, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(26), format.getIddcim1(), format.getMcitois(), null));
        financialStatement.getSpecificAmounts().set(27, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(27), format.getIddcim2(), format.getMcitccb(), format.getMoncon4()));
        financialStatement.getSpecificAmounts().set(28, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(28), format.getIdideu1(), format.getIdeumes(), null));
        financialStatement.getSpecificAmounts().set(29, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(29), format.getIdideu2(), format.getIdeumbm(), format.getMoncon4()));
        financialStatement.getSpecificAmounts().set(30, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(30), format.getIdmcir1(), format.getMcicarp(), null));
        financialStatement.getSpecificAmounts().set(31, mapOutSpecificAmount(financialStatement.getSpecificAmounts().get(31), format.getIdmcir2(), format.getMcicplb(), format.getMoncon4()));
    }

    private void mapOutAmountRates(final FinancialStatement financialStatement, final FormatoMPMQ6GS format) {
        if (financialStatement.getRates() == null) {
            financialStatement.setRates(new Rate());
        }

        if (CollectionUtils.isEmpty(financialStatement.getRates().getItemizeRates())) {
            financialStatement.getRates().setItemizeRates(initializeItemizeRates());
        }

        financialStatement.getRates().getItemizeRates().set(4, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(4), format.getIdiinne(), format.getAmoity3(), null, format.getMciinne(), null));
        financialStatement.getRates().getItemizeRates().set(5, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(5), format.getIdiinac(), format.getAmoity3(), null, format.getMciinac(), null));
        financialStatement.getRates().getItemizeRates().set(6, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(6), format.getIdiinam(), format.getAmoity3(), null, format.getMciinam(), null));
        financialStatement.getRates().getItemizeRates().set(7, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(7), format.getIdiinsd(), format.getAmoity3(), null, format.getMciinsd(), null));
        financialStatement.getRates().getItemizeRates().set(8, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(8), format.getIdiinsm(), format.getAmoity3(), null, format.getMciinsm(), null));
        financialStatement.getRates().getItemizeRates().set(9, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(9), format.getIdiicub(), format.getAmoityp(), null, format.getMciicub(), format.getMoncon4()));
        financialStatement.getRates().getItemizeRates().set(10, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(10), format.getIdiinab(), format.getAmoityp(), null, format.getMciinab(), format.getMoncon4()));
        financialStatement.getRates().getItemizeRates().set(11, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(11), format.getIdiiamb(), format.getAmoityp(), null, format.getMciiamb(), format.getMoncon4()));
        financialStatement.getRates().getItemizeRates().set(12, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(12), format.getIdiincb(), format.getAmoityp(), null, format.getMciincb(), format.getMoncon4()));
        financialStatement.getRates().getItemizeRates().set(13, mapOutItemizeRate(financialStatement.getRates().getItemizeRates().get(13), format.getIdiinmb(), format.getAmoityp(), null, format.getMciinmb(), format.getMoncon4()));
    }

    private List<ItemizeRate> initializeItemizeRates() {
        return Stream.generate(ItemizeRate::new).limit(14).collect(Collectors.toList());
    }
}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.bbva.pzic.cards.business.dto.DTOIntMembership;
import com.bbva.pzic.cards.business.dto.DTOIntMembershipServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntSearchCriteria;
import com.bbva.pzic.cards.business.dto.DTOIntStatus;
import com.bbva.pzic.cards.dao.model.mppf.FormatoMPM0PF;
import com.bbva.pzic.cards.dao.model.mppf.FormatoMPM0PS;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardMembershipMapper;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;

/**
 * Created on 16/10/2017.
 *
 * @author Entelgy
 */
@Mapper("txGetCardMembershipMapper")
public class TxGetCardMembershipMapper implements ITxGetCardMembershipMapper {

    @Autowired
    private EnumMapper enumMapper;

   
    


	@Override
	public FormatoMPM0PF mapInput(DTOIntSearchCriteria dtoIntSearchCriteria) {
		FormatoMPM0PF formatoMPM0PF = new FormatoMPM0PF();
		formatoMPM0PF.setNtarjlf(dtoIntSearchCriteria.getCardId());
		formatoMPM0PF.setIdpremi(dtoIntSearchCriteria.getMembershipId());
		
		return formatoMPM0PF;
	}


	@Override
	public DTOIntMembershipServiceResponse mapOutput(FormatoMPM0PS formatOutput, DTOIntSearchCriteria dtoIntSearchCriteria) {
		
		if (formatOutput == null) {
			return null;
		}
		
		DTOIntMembership dtoIntMembership = new DTOIntMembership();
		dtoIntMembership.setId(formatOutput.getIdpremi());
		dtoIntMembership.setDescription(formatOutput.getDescpre());
		dtoIntMembership.setNumber(formatOutput.getNpasfre());
		dtoIntMembership.setEnrollmentDate(formatOutput.getFecalta());
		dtoIntMembership.setCancellationDate(formatOutput.getFecbaja());
		dtoIntMembership.setExpirationDate(formatOutput.getFecven());
		
		if (formatOutput.getIndlife()!= null) {
			DTOIntStatus status = new DTOIntStatus(); 
		
			status.setId(enumMapper.getEnumValue("cards.membership.status.id", formatOutput.getIndlife()));
			dtoIntMembership.setStatus(status);
		}
		if(formatOutput.getIndmigr()!= null ) {
			dtoIntMembership.setIssuedDueToMigration("S".equalsIgnoreCase(formatOutput.getIndmigr())? Boolean.TRUE: Boolean.FALSE);
		}
		DTOIntMembershipServiceResponse dtoIntMembershipServiceResponse = new DTOIntMembershipServiceResponse();
		dtoIntMembershipServiceResponse.setData(dtoIntMembership);
		return dtoIntMembershipServiceResponse;
	}

   

}

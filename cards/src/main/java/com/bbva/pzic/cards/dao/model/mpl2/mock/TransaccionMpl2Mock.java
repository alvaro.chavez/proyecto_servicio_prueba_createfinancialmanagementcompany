package com.bbva.pzic.cards.dao.model.mpl2.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMENL2;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMS1L2;
import com.bbva.pzic.cards.dao.model.mpl2.PeticionTransaccionMpl2;
import com.bbva.pzic.cards.dao.model.mpl2.RespuestaTransaccionMpl2;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Invocador de la transacci&oacute;n <code>MPL2</code>
 *
 * @see com.bbva.pzic.cards.dao.model.mpl2.PeticionTransaccionMpl2
 * @see com.bbva.pzic.cards.dao.model.mpl2.RespuestaTransaccionMpl2
 */
@Component("transaccionMpl2")
public class TransaccionMpl2Mock implements InvocadorTransaccion<PeticionTransaccionMpl2, RespuestaTransaccionMpl2> {

    public static final String CARD_ID_FOR_PARTIAL_LIST = "1111111111111111";
    public static final String CARD_ID_FOR_TRANSACTION_WITH_WRONG_OPERATION_DATETIME = "2222222222222222";
    public static final String CARD_ID_FOR_EMPTY_LIST = "3333333333333333";
    public static final String CARD_ID_FOR_COMPLETE_LIST = "4444444444444444";

    private FormatsMpl2Mock formatsMpl2Mock = FormatsMpl2Mock.getInstance();

    @Override
    public RespuestaTransaccionMpl2 invocar(PeticionTransaccionMpl2 transaccion) {
        final RespuestaTransaccionMpl2 respuestaTransaccionMpl2 = new RespuestaTransaccionMpl2();
        respuestaTransaccionMpl2.setCodigoControl("OK");
        respuestaTransaccionMpl2.setCodigoRetorno("OK_COMMIT");

        final FormatoMPMENL2 formatoMPMENL2 = transaccion.getCuerpo().getParte(FormatoMPMENL2.class);

        String cardNumber = formatoMPMENL2.getNumtarj();
        if (CARD_ID_FOR_EMPTY_LIST.equals(cardNumber)) {
            return respuestaTransaccionMpl2;
        }

        try {
            if (CARD_ID_FOR_PARTIAL_LIST.equals(cardNumber)) {
                respuestaTransaccionMpl2.getCuerpo().getPartes().addAll(buildFormatoMPMS1L2Copy());
                respuestaTransaccionMpl2.getCuerpo().getPartes().add(buildFormatoMPMS2L2Copy());

            } else if (CARD_ID_FOR_TRANSACTION_WITH_WRONG_OPERATION_DATETIME.equals(cardNumber)) {
                respuestaTransaccionMpl2.getCuerpo().getPartes().add(buildWrongFormatoMPMS1L2());

            } else {
                respuestaTransaccionMpl2.getCuerpo().getPartes().addAll(buildFormatoMPMS1L2Copy());
            }

            return respuestaTransaccionMpl2;
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionMpl2 invocarCache(PeticionTransaccionMpl2 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private List<CopySalida> buildFormatoMPMS1L2Copy() throws IOException {
        List<FormatoMPMS1L2> formatosMPMS1L2 = formatsMpl2Mock.buildFormatosMPMS1L2();

        List<CopySalida> copiesSalida = new ArrayList<>();
        for (FormatoMPMS1L2 formatoMPMS1L2 : formatosMPMS1L2) {
            final CopySalida copySalida = new CopySalida();
            copySalida.setCopy(formatoMPMS1L2);
            copiesSalida.add(copySalida);
        }

        return copiesSalida;
    }

    private CopySalida buildWrongFormatoMPMS1L2() {
        FormatoMPMS1L2 formatoMPMS1L2 = new FormatoMPMS1L2();
        formatoMPMS1L2.setFechope(new Date());
        formatoMPMS1L2.setHoraope("12459");

        final CopySalida copySalida = new CopySalida();
        copySalida.setCopy(formatoMPMS1L2);
        return copySalida;
    }

    private CopySalida buildFormatoMPMS2L2Copy() throws IOException {
        final CopySalida copySalida = new CopySalida();
        copySalida.setCopy(formatsMpl2Mock.buildFormatoMPMS2L2());
        return copySalida;
    }
}

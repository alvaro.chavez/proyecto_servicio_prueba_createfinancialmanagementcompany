package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "product", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "product", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Financial product identifier.
     */
    private String id;
    /**
     * Financial product name.
     */
    private String name;
    /**
     * Financial product associated to the contract.
     */
    private ProductType productType;
    /**
     * Contains the subproduct related to a product.
     */
    private Subproduct subproduct;

    private Subproduct subProduct;

    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Subproduct getSubproduct() {
        return subproduct;
    }

    public void setSubproduct(Subproduct subproduct) {
        this.subproduct = subproduct;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Subproduct getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(Subproduct subProduct) {
        this.subProduct = subProduct;
    }
}

package com.bbva.pzic.cards.dao.rest;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.ErrorSeverity;
import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.dao.model.filenet.Documents;
import com.bbva.pzic.cards.dao.rest.mapper.IRestDigitizeDocumentFileMapper;
import com.bbva.pzic.cards.util.connection.rest.RestPostConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 27/03/2018.
 *
 * @author Entelgy
 */
@Component
public class RestDigitizeDocumentFile extends RestPostConnection<DocumentRequest, Documents> {

    private static final String DIGITIZE_DOCUMENT_FILE_URL = "servicing.url.documents.digitizeDocumentFile";
    private static final String DIGITIZE_DOCUMENT_FILE_USE_PROXY_DOCUMENTS = "servicing.proxy.documents.digitizeDocumentFile";

    @Autowired
    private IRestDigitizeDocumentFileMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(DIGITIZE_DOCUMENT_FILE_USE_PROXY_DOCUMENTS, Boolean.FALSE);
    }

    public Document perform(final DocumentRequest input) {
        return mapper.mapOut(connect(DIGITIZE_DOCUMENT_FILE_URL, input));
    }

    @Override
    protected void evaluateResponse(Documents response, int statusCode) {
        evaluateMessagesResponse(generateErrorMessages(statusCode), "SMCPE1810268", statusCode);
    }

    private List<Message> generateErrorMessages(int statusCode) {
        List<Message> messages = new ArrayList<>();
        Message message = new Message();
        message.setCode(String.valueOf(statusCode));
        message.setType(ErrorSeverity.ERROR);
        messages.add(message);

        return messages;
    }
}

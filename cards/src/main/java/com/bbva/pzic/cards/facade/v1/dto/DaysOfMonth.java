package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "daysOfMonth", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "daysOfMonth", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DaysOfMonth implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Represents the days of month as numbers. (1)First day and the last day depends on the month (28, 29, 30 or 31).
     */
    private String day;
    /**
     * The day of the month that the credit card billing ends. Represents the days of month as numbers. (1)First day and the last day depends on the month (28, 29, 30 or 31).
     */
    private String cutOffDay;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCutOffDay() {
        return cutOffDay;
    }

    public void setCutOffDay(String cutOffDay) {
        this.cutOffDay = cutOffDay;
    }
}

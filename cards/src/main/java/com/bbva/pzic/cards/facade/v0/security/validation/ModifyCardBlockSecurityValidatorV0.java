package com.bbva.pzic.cards.facade.v0.security.validation;

import com.bbva.jee.arq.spring.core.servicing.security.validations.ApplicationSecurityValidator;
import com.bbva.pzic.cards.canonic.Block;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK;

/**
 * Created on 26/02/2018.
 *
 * @author Entelgy
 */
@Component(value = "modifyCardBlockSecurityValidatorV0")
public class ModifyCardBlockSecurityValidatorV0 implements ApplicationSecurityValidator {

    private static final Log LOG = LogFactory.getLog(ModifyCardBlockSecurityValidatorV0.class);

    private static final int DECLARATION_INDEX_OF_OBJECT_BLOCK_PARAMETER = 2;

    private static final String OPERATION_CARD_LOCK_NO_REPLACEMENT_SIGNATURE_SECURITY_LEVEL = "OP001";
    private static final String OPERATION_CARD_LOCK_WITH_RESET_SIGNATURE_SECURITY_LEVEL = "OP002";

    @Override
    public String getOperation(List<Object> declaredParameters) {
        LOG.info("----- Call security validator ModifyCardBlockSecurityValidatorV0 -----");
        Object rawSimulated = declaredParameters.get(DECLARATION_INDEX_OF_OBJECT_BLOCK_PARAMETER);
        Block block = (Block) rawSimulated;
        String operation = SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK;
        if (block != null && block.getIsReissued() != null && block.getIsReissued()) {
            operation = operation.concat(OPERATION_CARD_LOCK_WITH_RESET_SIGNATURE_SECURITY_LEVEL);
        } else {
            operation = operation.concat(OPERATION_CARD_LOCK_NO_REPLACEMENT_SIGNATURE_SECURITY_LEVEL);
        }
        LOG.info(String.format("----- Operación a ejecutar %s -----", operation));
        return operation;
    }
}

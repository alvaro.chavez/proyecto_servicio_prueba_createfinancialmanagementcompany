package com.bbva.pzic.cards.dao.model.mpdc.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpdc.*;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

import static com.bbva.pzic.cards.dao.model.mpdc.mock.TransaccionMpdcMockResponses.getFormatoMCRMDC0;
import static com.bbva.pzic.cards.dao.model.mpdc.mock.TransaccionMpdcMockResponses.getFormatoMCRMDC1;

@Component("transaccionMpdc")
public class TransaccionMpdcMock implements InvocadorTransaccion<PeticionTransaccionMpdc, RespuestaTransaccionMpdc> {
    public static final String CARD_ID_WITH_DATA = "999";
    public static final String CARD_ID_WITH_NONEXISTENT_REFUND_TYPE="888";
    public static final String CARD_ID_WITH_NONEXISTENT_PRICE_TYPE="777";
    public static final String CARD_ID_WITH_NONEXISTENT_TAX_TYPE="666";


    @Override
    public RespuestaTransaccionMpdc invocar(PeticionTransaccionMpdc transaccionMpdc) throws ExcepcionTransaccion {
        RespuestaTransaccionMpdc respuestaTransaccionMpdc = new RespuestaTransaccionMpdc();
        respuestaTransaccionMpdc.setCodigoRetorno("OK_COMMIT");
        respuestaTransaccionMpdc.setCodigoControl("OK");

        FormatoMCEMDC0 formatoMCEMDC0 = transaccionMpdc.getCuerpo().getParte(FormatoMCEMDC0.class);
        try {
            if (CARD_ID_WITH_DATA.equals(formatoMCEMDC0.getNrotarj())) {
                respuestaTransaccionMpdc.getCuerpo().getPartes().add(getFormatoMCRMDC0());
                respuestaTransaccionMpdc.getCuerpo().getPartes().addAll(getFormatoMCRMDC1());
            }else if(CARD_ID_WITH_NONEXISTENT_REFUND_TYPE.equals(formatoMCEMDC0.getNrotarj())) {
                CopySalida copySalida = getFormatoMCRMDC0();
                copySalida.getCopy(FormatoMCRMDC0. class).setTipreem("NONEXISTENT");
                respuestaTransaccionMpdc.getCuerpo().getPartes().add(copySalida);
                respuestaTransaccionMpdc.getCuerpo().getPartes().addAll(getFormatoMCRMDC1());
            }else if(CARD_ID_WITH_NONEXISTENT_PRICE_TYPE.equals(formatoMCEMDC0.getNrotarj())){
                CopySalida copySalida = getFormatoMCRMDC0();
                copySalida.getCopy(FormatoMCRMDC0. class).setTipprec("NONEXISTENT");
                respuestaTransaccionMpdc.getCuerpo().getPartes().add(copySalida);
                respuestaTransaccionMpdc.getCuerpo().getPartes().addAll(getFormatoMCRMDC1());
            }else if(CARD_ID_WITH_NONEXISTENT_TAX_TYPE.equals(formatoMCEMDC0.getNrotarj())){
                respuestaTransaccionMpdc.getCuerpo().getPartes().add(getFormatoMCRMDC0());
                List<CopySalida> copiesSalida = getFormatoMCRMDC1();
                copiesSalida.get(0).getCopy(FormatoMCRMDC1.class).setTipoimp("NONEXISTENT");
                respuestaTransaccionMpdc.getCuerpo().getPartes().addAll(copiesSalida);


            }

        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
        return respuestaTransaccionMpdc;
    }

    @Override
    public RespuestaTransaccionMpdc invocarCache(PeticionTransaccionMpdc peticionTransaccionMpdc) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

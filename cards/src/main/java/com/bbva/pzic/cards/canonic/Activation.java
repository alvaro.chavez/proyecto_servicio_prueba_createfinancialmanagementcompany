package com.bbva.pzic.cards.canonic;

import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Activation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Activation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Activation implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Operational activation identifier.
     */
    private String activationId;
    /**
     * Operational activation description.
     */
    private String name;
    /**
     * Indicates whether the activation is enabled.
     */
    private Boolean isActive;
    /**
     * String based on ISO-8601 timestamp format for specifying when the
     * activation became temporarily disabled.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar startDate;
    /**
     * String based on ISO-8601 timestamp format for specifying when the
     * activation becomes enabled after temporarily disabled.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar endDate;
    /**
     * List of limits associated to the operational activation.
     */
    private List<Limit> limits;

    public String getActivationId() {
        return activationId;
    }

    public void setActivationId(String activationId) {
        this.activationId = activationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public List<Limit> getLimits() {
        return limits;
    }

    public void setLimits(List<Limit> limits) {
        this.limits = limits;
    }
}
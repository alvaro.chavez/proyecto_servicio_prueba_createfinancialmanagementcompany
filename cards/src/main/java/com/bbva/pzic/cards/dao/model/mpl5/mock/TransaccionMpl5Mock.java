package com.bbva.pzic.cards.dao.model.mpl5.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMENL5;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMS1L5;
import com.bbva.pzic.cards.dao.model.mpl5.PeticionTransaccionMpl5;
import com.bbva.pzic.cards.dao.model.mpl5.RespuestaTransaccionMpl5;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

import static com.bbva.pzic.cards.dao.model.mpl5.mock.TransaccionMpl5MockResponses.getFormatoMPMS1L5;

/**
 * Invocador de la transacci&oacute;n <code>MPL5</code>
 *
 * @see PeticionTransaccionMpl5
 * @see RespuestaTransaccionMpl5
 */
@Component("transaccionMpl5")
public class TransaccionMpl5Mock implements InvocadorTransaccion<PeticionTransaccionMpl5, RespuestaTransaccionMpl5> {

    public static final String CARD_ID_WITH_DATA = "999";
    public static final String CARD_ID_WITH_NONEXISTENT_NUMBERTYPE = "666";
    public static final String CARD_ID_WITH_NONEXISTENT_PRODUCT = "888";

    @Override
    public RespuestaTransaccionMpl5 invocar(PeticionTransaccionMpl5 transaccion) {
        final RespuestaTransaccionMpl5 response = new RespuestaTransaccionMpl5();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENL5 formatoMPMENL5 = transaccion.getCuerpo().getParte(FormatoMPMENL5.class);
        try {
            if (CARD_ID_WITH_DATA.equals(formatoMPMENL5.getIdetarj())) {
                response.getCuerpo().getPartes().addAll(getFormatoMPMS1L5());
            } else if (CARD_ID_WITH_NONEXISTENT_NUMBERTYPE.equals(formatoMPMENL5.getIdetarj())) {
                List<CopySalida> copiesSalida = getFormatoMPMS1L5();
                copiesSalida.get(0).getCopy(FormatoMPMS1L5.class).setTnumid("NOEXISTENTNUMBERTYPE");
                response.getCuerpo().getPartes().addAll(copiesSalida);
            } else if (CARD_ID_WITH_NONEXISTENT_PRODUCT.equals(formatoMPMENL5.getIdetarj())) {
                List<CopySalida> copiesSalida = getFormatoMPMS1L5();
                copiesSalida.get(0).getCopy(FormatoMPMS1L5.class).setProdid("NOEXISTENTPRODUCT");
                response.getCuerpo().getPartes().addAll(copiesSalida);
            }
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
        return response;
    }

    @Override
    public RespuestaTransaccionMpl5 invocarCache(PeticionTransaccionMpl5 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

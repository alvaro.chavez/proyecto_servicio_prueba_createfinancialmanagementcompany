package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "BrandAssociation", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "BrandAssociation", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BrandAssociation implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

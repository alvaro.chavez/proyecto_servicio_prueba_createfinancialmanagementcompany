package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;

/**
 * Created on 27/11/2019.
 *
 * @author Entelgy
 */
public class InputModifyCardProposal {

    private String proposalId;
    @Valid
    private DTOIntProposal proposal;

    public String getProposalId() {
        return proposalId;
    }

    public void setProposalId(String proposalId) {
        this.proposalId = proposalId;
    }

    public DTOIntProposal getProposal() {
        return proposal;
    }

    public void setProposal(DTOIntProposal proposal) {
        this.proposal = proposal;
    }
}

package com.bbva.pzic.cards.business;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v1.dto.*;

import java.util.List;

/**
 * @author Entelgy
 */
public interface ISrvIntCardsV1 {

    /**
     * Allows to consult the card proposals.
     *
     * @param input dto with input fields to validate
     * @return {@link List<Proposal>}
     */
    List<Proposal> listCardProposals(InputListCardProposals input);

    /**
     * * It stores a customer generated pre-application of the formalization
     * * of a credit card, which is finalized when the customer goes to the branch
     * * of his choice to pick up the card. This operation could be derived from an offer.
     *
     * @param mapIn dto with input fields to validate
     * @return {@link ProposalCard}
     */
    ProposalCard createCardProposal(InputCreateCardProposal mapIn);

    /**
     * It modifies a card proposal.
     *
     * @param input dto with input fields to validate
     * @return {@link Proposal}
     */
    Proposal modifyCardProposal(InputModifyCardProposal input);

    /**
     * @param input dto with input fields to validate
     * @return {@link CardPost}
     */
    CardPost createCard(InputCreateCard input);

    /**
     * @param dtoInt dto with input fields to validate
     * @return {@link Delivery}
     */
    Delivery createCardDelivery(DTOIntDelivery dtoInt);

    /**
     * @param input dto with input fields to validate
     * @return {@link List<SecurityData>}
     */
    List<SecurityData> getCardSecurityData(InputGetCardSecurityData input);

    /**
     * @param input
     * @return {@link CancellationVerification}
     */
    CancellationVerification getCardCancellationVerification(InputGetCardCancellationVerification input);

    SimulateTransactionRefund simulateCardTransactionTransactionRefund(InputSimulateCardTransactionTransactionRefund mapIn);

    ReimburseCardTransactionTransactionRefund reimburseCardTransactionTransactionRefund(InputReimburseCardTransactionTransactionRefund input);

    /**
     * @param input
     * @return {@link FinancialStatement}
     */
    FinancialStatement getCardFinancialStatement(InputGetCardFinancialStatement input);

    /**
     * @param input
     * @return {@link RequestCancel}
     */
    RequestCancel createCardCancelRequest(InputCreateCardCancelRequest input);

    /**
     *
     * @param input
     * @return {@link Cancellation}
     */
    Cancellation createCardCancellations(InputCreateCardCancellations input);

}

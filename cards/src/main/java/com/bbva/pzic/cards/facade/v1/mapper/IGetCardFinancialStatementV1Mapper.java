package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.facade.v1.dto.FinancialStatement;

public interface IGetCardFinancialStatementV1Mapper {

    InputGetCardFinancialStatement mapIn(String cardId, String financialStatementId);

    ServiceResponse<FinancialStatement> mapOut(FinancialStatement cardFinancialStatement);
}

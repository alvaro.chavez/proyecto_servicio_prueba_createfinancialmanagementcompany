package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Size;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntPeriod {

    @Size(max = 1, groups = ValidationGroup.CreateCardsProposal.class)
    private String id;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
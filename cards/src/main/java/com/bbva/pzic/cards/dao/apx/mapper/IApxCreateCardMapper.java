package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Entityin;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Entityout;
import com.bbva.pzic.cards.facade.v1.dto.CardPost;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
public interface IApxCreateCardMapper {

    Entityin mapIn(DTOIntCard dtoInt);

    CardPost mapOut(Entityout entityOut);
}

package com.bbva.pzic.cards.facade.v01.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;

/**
 * Created on 29/12/2016.
 *
 * @author Entelgy
 */
public interface ICreateCardMapper {

    DTOIntCard mapIn(Card card);
}

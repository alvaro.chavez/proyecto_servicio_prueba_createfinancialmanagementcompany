package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Block;
import com.bbva.pzic.cards.facade.v0.mapper.IModifyCardBlockMapper;
import com.bbva.pzic.cards.util.mappers.MapperUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Entelgy
 */
@Component
public class ModifyCardBlockMapper implements IModifyCardBlockMapper {

    private static final Log LOG = LogFactory.getLog(ModifyCardBlockMapper.class);

    @Autowired
    private MapperUtils mapperUtils;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    /**
     * @see IModifyCardBlockMapper#mapInput(String, String, Block)
     */
    @Override
    public DTOIntBlock mapInput(final String cardId, final String blockId, final Block block) {
        LOG.info("... called method ModifyCardBlockMapper.mapInput ...");
        final DTOIntBlock dtoIntBlock = new DTOIntBlock();
        final DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardId(cardId);
        dtoIntBlock.setCard(dtoIntCard);
        dtoIntBlock.setBlockId(translator.translateFrontendEnumValueStrictly("cards.block.blockId", blockId));
        dtoIntBlock.setIsActive(mapperUtils.convertBooleanToString(block.getIsActive()));
        dtoIntBlock.setIsReissued(mapperUtils.convertBooleanToString(block.getIsReissued()));
        dtoIntBlock.setAdditionalInformation(block.getAdditionalInformation());
        if (block.getReason() != null) {
            dtoIntBlock.setReasonId(block.getReason().getId());
        }

        return dtoIntBlock;
    }

}
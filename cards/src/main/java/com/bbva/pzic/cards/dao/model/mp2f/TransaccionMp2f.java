package com.bbva.pzic.cards.dao.model.mp2f;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MP2F</code>
 *
 * @see PeticionTransaccionMp2f
 * @see RespuestaTransaccionMp2f
 */
@Component("transaccionMp2f")
public class TransaccionMp2f implements InvocadorTransaccion<PeticionTransaccionMp2f, RespuestaTransaccionMp2f> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionMp2f invocar(PeticionTransaccionMp2f transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMp2f.class, RespuestaTransaccionMp2f.class, transaccion);
    }

    @Override
    public RespuestaTransaccionMp2f invocarCache(PeticionTransaccionMp2f transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMp2f.class, RespuestaTransaccionMp2f.class, transaccion);
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

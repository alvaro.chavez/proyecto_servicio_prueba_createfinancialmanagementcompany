package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardsProposalMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
@Component
public class CreateCardsProposalMapper implements ICreateCardsProposalMapper {

    private static final Log LOG = LogFactory.getLog(CreateCardsProposalMapper.class);

    private Translator translator;

    @Autowired
    private AbstractCypherTool cypherTool;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntProposal mapIn(final Proposal proposal) {
        LOG.info("... called method CreateCardsProposalMapper.mapIn ...");
        DTOIntProposal dtoIntProposal = new DTOIntProposal();
        dtoIntProposal.setCardType(mapInCardType(proposal.getCardType()));
        dtoIntProposal.setTitle(mapInTitle(proposal.getTitle()));
        dtoIntProposal.setPaymentMethod(mapInPaymentMethod(proposal.getPaymentMethod()));
        dtoIntProposal.setGrantedCredits(mapInGrantedCredits(proposal.getGrantedCredits()));
        dtoIntProposal.setDelivery(mapInDelivery(proposal.getDelivery()));
        dtoIntProposal.setBank(mapInBank(proposal.getBank()));
        dtoIntProposal.setFees(mapInFees(proposal.getFees()));
        dtoIntProposal.setRates(mapInRates(proposal.getRates()));
        dtoIntProposal.setAdditionalProducts(mapInAdditionalProducts(proposal.getAdditionalProducts()));
        dtoIntProposal.setMembership(mapInMembership(proposal.getMembership()));
        dtoIntProposal.setContactDetailsId0(mapInContactDetailsId0(proposal.getContactDetails()));
        dtoIntProposal.setContactDetailsId1(mapInContactDetailsId1(proposal.getContactDetails()));
        dtoIntProposal.setOfferId(proposal.getOfferId());
        dtoIntProposal.setNotificationsByOperation(proposal.getNotificationsByOperation());
        return dtoIntProposal;
    }

    private DTOIntCardType mapInCardType(final CardType cardType) {
        if (cardType == null) {
            return null;
        }
        DTOIntCardType dtoIntCardType = new DTOIntCardType();
        dtoIntCardType.setId(translator.translateFrontendEnumValueStrictly("cards.cardType.id", cardType.getId()));
        return dtoIntCardType;
    }

    private DTOIntTitle mapInTitle(final Title title) {
        if (title == null) {
            return null;
        }
        DTOIntTitle dtoIntTitle = new DTOIntTitle();
        dtoIntTitle.setId(title.getId());
        dtoIntTitle.setSubproduct(mapInTitleSubproduct(title.getSubproduct()));
        return dtoIntTitle;
    }

    private DTOIntPaymentMethod mapInPaymentMethod(final PaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            return null;
        }
        DTOIntPaymentMethod dtoIntPaymentMethod = new DTOIntPaymentMethod();
        dtoIntPaymentMethod.setId(translator
                .translateFrontendEnumValueStrictly("cards.paymentMethod.id", paymentMethod.getId()));
        dtoIntPaymentMethod.setEndDay(paymentMethod.getEndDay());
        dtoIntPaymentMethod.setPeriod(mapInPaymentMethodPeriod(paymentMethod.getPeriod()));
        return dtoIntPaymentMethod;
    }

    private DTOIntPeriod mapInPaymentMethodPeriod(final Period period) {
        if (period == null) {
            return null;
        }
        DTOIntPeriod dtoIntPeriod = new DTOIntPeriod();
        dtoIntPeriod.setId(translator
                .translateFrontendEnumValueStrictly("cards.period.id", period.getId()));
        return dtoIntPeriod;
    }

    private DTOIntSubProduct mapInTitleSubproduct(final SubProduct subproduct) {
        if (subproduct == null) {
            return null;
        }
        DTOIntSubProduct dtoIntSubProduct = new DTOIntSubProduct();
        dtoIntSubProduct.setId(subproduct.getId());
        return dtoIntSubProduct;
    }

    private List<DTOIntImport> mapInGrantedCredits(final List<Import> grantedCreditList) {
        if (CollectionUtils.isEmpty(grantedCreditList)
                || grantedCreditList.get(0) == null) {
            return null;
        }
        DTOIntImport dtoIntImport = new DTOIntImport();
        dtoIntImport.setAmount(grantedCreditList.get(0).getAmount());
        dtoIntImport.setCurrency(grantedCreditList.get(0).getCurrency());
        return Collections.singletonList(dtoIntImport);
    }

    private DTOIntDelivery mapInDelivery(final Delivery delivery) {
        if (delivery == null
                || delivery.getDestination() == null) {
            return null;
        }
        DTOIntDestination dtoIntDestination = new DTOIntDestination();
        dtoIntDestination.setId(translator
                .translateFrontendEnumValueStrictly("cards.delivery.destionation.id", delivery.getDestination().getId()));
        DTOIntDelivery dtoIntDelivery = new DTOIntDelivery();
        dtoIntDelivery.setDestination(dtoIntDestination);
        return dtoIntDelivery;
    }

    private DTOIntBankType mapInBank(final BankType bank) {
        if (bank == null) {
            return null;
        }
        DTOIntBankType dtoIntBankType = new DTOIntBankType();
        dtoIntBankType.setId(bank.getId());
        dtoIntBankType.setBranch(mapInBankBranch(bank.getBranch()));
        return dtoIntBankType;
    }

    private DTOIntBranch mapInBankBranch(final Branch branch) {
        if (branch == null) {
            return null;
        }
        DTOIntBranch dtoIntBranch = new DTOIntBranch();
        dtoIntBranch.setId(branch.getId());
        return dtoIntBranch;
    }

    private DTOIntFee mapInFees(final Fee fee) {
        if (fee == null) {
            return null;
        }
        DTOIntFee dtoIntFee = new DTOIntFee();
        dtoIntFee.setItemizeFees(mapInItemizeFees(fee.getItemizeFees()));
        return dtoIntFee;
    }

    private List<DTOIntItemizeFee> mapInItemizeFees(final List<ItemizeFee> itemizeFees) {
        if (CollectionUtils.isEmpty(itemizeFees)) {
            return null;
        }
        List<DTOIntItemizeFee> dtoIntItemizeFeeList = new ArrayList<>();
        dtoIntItemizeFeeList.add(mapInItemizeFee(itemizeFees.get(0)));
        if (itemizeFees.size() > 1) {
            dtoIntItemizeFeeList.add(mapInItemizeFee(itemizeFees.get(1)));
        }
        return dtoIntItemizeFeeList;
    }

    private DTOIntItemizeFee mapInItemizeFee(final ItemizeFee itemizeFee) {
        if (itemizeFee == null) {
            return null;
        }
        DTOIntItemizeFee dtoIntItemizeFee = new DTOIntItemizeFee();
        dtoIntItemizeFee.setFeeType(translator
                .translateFrontendEnumValueStrictly("cards.itemizeFees.feeType", itemizeFee.getFeeType()));
        dtoIntItemizeFee.setName(itemizeFee.getName());
        dtoIntItemizeFee.setMode(mapInItemizeFeeMode(itemizeFee.getMode()));
        dtoIntItemizeFee.setItemizeFeeUnit(mapInItemizeFeeUnit(itemizeFee.getItemizeFeeUnit()));
        return dtoIntItemizeFee;
    }

    private DTOIntMode mapInItemizeFeeMode(final Mode mode) {
        if (mode == null) {
            return null;
        }
        DTOIntMode dtoIntMode = new DTOIntMode();
        dtoIntMode.setId(translator
                .translateFrontendEnumValueStrictly("cards.itemizeFees.mode", mode.getId()));
        return dtoIntMode;
    }

    private DTOIntItemizeFeeUnit mapInItemizeFeeUnit(final ItemizeFeeUnit itemizeFeeUnit) {
        if (itemizeFeeUnit == null) {
            return null;
        }
        DTOIntItemizeFeeUnit dtoIntItemizeFeeUnit = new DTOIntItemizeFeeUnit();
        dtoIntItemizeFeeUnit.setAmount(itemizeFeeUnit.getAmount());
        dtoIntItemizeFeeUnit.setCurrency(itemizeFeeUnit.getCurrency());
        return dtoIntItemizeFeeUnit;
    }

    private List<DTOIntRate> mapInRates(final List<Rate> rateList) {
        if (CollectionUtils.isEmpty(rateList)) {
            return null;
        }
        List<DTOIntRate> dtoIntRateList = new ArrayList<>();
        dtoIntRateList.add(mapInRate(rateList.get(0)));
        if (rateList.size() > 1) {
            dtoIntRateList.add(mapInRate(rateList.get(1)));
        }
        return dtoIntRateList;
    }

    private DTOIntRate mapInRate(final Rate rate) {
        if (rate == null) {
            return null;
        }
        DTOIntRate dtoIntRate = new DTOIntRate();
        dtoIntRate.setRateType(mapInRateType(rate.getRateType()));
        dtoIntRate.setCalculationDate(rate.getCalculationDate());
        dtoIntRate.setModeid(rate.getMode() == null ? null : translator
                .translateFrontendEnumValueStrictly("cards.rates.mode", rate.getMode().getId()));
        dtoIntRate.setUnit(mapInRateUnit(rate.getUnit()));
        return dtoIntRate;
    }

    private DTOIntRateType mapInRateType(final RateType rateType) {
        if (rateType == null) {
            return null;
        }
        DTOIntRateType dtoIntRateType = new DTOIntRateType();
        dtoIntRateType.setId(rateType.getId());
        return dtoIntRateType;
    }

    private DTOIntUnit mapInRateUnit(final Percentage unit) {
        if (unit == null) {
            return null;
        }
        DTOIntUnit dtoIntUnit = new DTOIntUnit();
        dtoIntUnit.setPercentage(unit.getPercentage());
        return dtoIntUnit;
    }

    private List<DTOIntAdditionalProduct> mapInAdditionalProducts(final List<AdditionalProduct> additionalProducts) {
        if (CollectionUtils.isEmpty(additionalProducts)
                || additionalProducts.get(0) == null) {
            return null;
        }
        DTOIntAdditionalProduct dtoIntAdditionalProduct = new DTOIntAdditionalProduct();
        dtoIntAdditionalProduct.setProductType(translator.translateFrontendEnumValueStrictly(
                "cards.offers.additionalProducts.productType", additionalProducts.get(0).getProductType()));
        dtoIntAdditionalProduct.setAmount(additionalProducts.get(0).getAmount());
        dtoIntAdditionalProduct.setCurrency(additionalProducts.get(0).getCurrency());
        return Collections.singletonList(dtoIntAdditionalProduct);
    }

    private DTOIntMembership mapInMembership(final Membership membership) {
        if (membership == null) {
            return null;
        }
        DTOIntMembership dtoIntMembership = new DTOIntMembership();
        dtoIntMembership.setId(membership.getId());
        dtoIntMembership.setNumber(membership.getNumber());
        return dtoIntMembership;
    }

    private String mapInContactDetailsId0(final List<ContactDetail> contactDetails) {
        if (CollectionUtils.isEmpty(contactDetails)
                || contactDetails.get(0) == null) {
            return null;
        }
        return cypherTool.decrypt(contactDetails.get(0).getId(),
                AbstractCypherTool.CONTACT_DETAIL_ID, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL);
    }

    private String mapInContactDetailsId1(final List<ContactDetail> contactDetails) {
        if (CollectionUtils.isEmpty(contactDetails)
                || contactDetails.size() < 2
                || contactDetails.get(1) == null) {
            return null;
        }
        return cypherTool.decrypt(contactDetails.get(1).getId(),
                AbstractCypherTool.CONTACT_DETAIL_ID, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProposalData mapOut(final Proposal proposal) {
        LOG.info("... called method CreateCardsProposalMapper.mapOut ...");
        if (proposal == null) {
            return null;
        }

        ProposalData data = new ProposalData();
        data.setData(proposal);
        return data;
    }
}

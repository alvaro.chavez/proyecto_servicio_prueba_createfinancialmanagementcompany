package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "deliveryProposal", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "deliveryProposal", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeliveryProposal implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Delivery manager identifier.
     */
    private String id;
    /**
     * Delivery service type of a product or document. These can be physical or
     * digital, depending on the case the delivery will be at an address or a
     * contact.
     */
    private ServiceType serviceType;
    /**
     * Detail of delivery contact.
     */
    private SpecificProposalContact contact;
    /**
     * Detail of address delivery.
     */
    private SpecificProposalAddress address;
    /**
     * Destination type of card delivery.
     */
    private Destination destination;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public SpecificProposalContact getContact() {
        return contact;
    }

    public void setContact(SpecificProposalContact contact) {
        this.contact = contact;
    }

    public SpecificProposalAddress getAddress() {
        return address;
    }

    public void setAddress(SpecificProposalAddress address) {
        this.address = address;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }
}

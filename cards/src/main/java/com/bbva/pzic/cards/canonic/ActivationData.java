package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "ActivationData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "ActivationData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivationData implements Serializable {

    private static final long serialVersionUID = 1L;

    private Activation data;

    public Activation getData() {
        return data;
    }

    public void setData(Activation data) {
        this.data = data;
    }
}
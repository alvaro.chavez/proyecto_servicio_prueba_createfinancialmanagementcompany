package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMENDV;
import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMS1DV;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardSecurityDataV1Mapper;
import com.bbva.pzic.cards.facade.v1.dto.SecurityData;
import com.bbva.pzic.cards.facade.v1.dto.ValidityPeriod;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper("txGetCardSecurityDataV1Mapper")
public class TxGetCardSecurityDataV1Mapper implements ITxGetCardSecurityDataV1Mapper {

    @Autowired
    private Translator translator;

    @Override
    public FormatoMPMENDV mapIn(final InputGetCardSecurityData input) {
        FormatoMPMENDV formatoMPMENDV = new FormatoMPMENDV();
        formatoMPMENDV.setNumtarj(input.getCardId());
        String publicKey = input.getPublicKey();
        formatoMPMENDV.setKeypb01(StringUtils.trimToNull(StringUtils.substring(publicKey, 0, 75)));
        formatoMPMENDV.setKeypb02(StringUtils.trimToNull(StringUtils.substring(publicKey, 75, 150)));
        formatoMPMENDV.setKeypb03(StringUtils.trimToNull(StringUtils.substring(publicKey, 150, 225)));
        formatoMPMENDV.setKeypb04(StringUtils.trimToNull(StringUtils.substring(publicKey, 225, 300)));
        formatoMPMENDV.setKeypb05(StringUtils.trimToNull(StringUtils.substring(publicKey, 300, 375)));
        formatoMPMENDV.setKeypb06(StringUtils.trimToNull(StringUtils.substring(publicKey, 375, 450)));
        formatoMPMENDV.setKeypb07(StringUtils.trimToNull(StringUtils.substring(publicKey, 450, 525)));
        formatoMPMENDV.setKeypb08(StringUtils.trimToNull(StringUtils.substring(publicKey, 525, 600)));
        formatoMPMENDV.setKeypb09(StringUtils.trimToNull(StringUtils.substring(publicKey, 600, 675)));
        formatoMPMENDV.setKeypb10(StringUtils.trimToNull(StringUtils.substring(publicKey, 675, 750)));
        formatoMPMENDV.setKeypb11(StringUtils.trimToNull(StringUtils.substring(publicKey, 750, 825)));
        formatoMPMENDV.setKeypb12(StringUtils.trimToNull(StringUtils.substring(publicKey, 825, 900)));
        formatoMPMENDV.setKeypb13(StringUtils.trimToNull(StringUtils.substring(publicKey, 900, 975)));
        formatoMPMENDV.setKeypb14(StringUtils.trimToNull(StringUtils.substring(publicKey, 975, 1050)));

        return formatoMPMENDV;
    }

    @Override
    public List<SecurityData> mapOut(final FormatoMPMS1DV formatoMPMS1DV, final List<SecurityData> securityDataList) {
        if (formatoMPMS1DV == null) {
            return securityDataList;
        }
        SecurityData securityData = new SecurityData();
        securityData.setId(translator.translateBackendEnumValueStrictly("securityData.id", formatoMPMS1DV.getIdcodsg()));
        securityData.setDescription(formatoMPMS1DV.getDscodsg());
        StringBuilder codeTotal = new StringBuilder();
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse01()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse02()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse03()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse04()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse05()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse06()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse07()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse08()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse09()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse10()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse11()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse12()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse13()));
        codeTotal.append(StringUtils.trimToEmpty(formatoMPMS1DV.getCodse14()));
        securityData.setCode("".equals(codeTotal.toString()) ? null : new String(codeTotal));
        securityData.setValidityPeriod(mapOutValidityPeriod(formatoMPMS1DV));
        securityDataList.add(securityData);
        return securityDataList;
    }

    private ValidityPeriod mapOutValidityPeriod(final FormatoMPMS1DV formatoMPMS1DV) {
        if (formatoMPMS1DV.getTimerun() == null && formatoMPMS1DV.getTimerns() == null) {
            return null;
        }
        ValidityPeriod validityPeriod = new ValidityPeriod();
        validityPeriod.setTimeUnit(translator.translateBackendEnumValueStrictly("validity.timeUnit.id", formatoMPMS1DV.getTimerun()));
        validityPeriod.setValue(formatoMPMS1DV.getTimerns());
        return validityPeriod;
    }
}

package com.bbva.pzic.cards.dao.model.mpg9.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpg9.FormatoMPMENG9;
import com.bbva.pzic.cards.dao.model.mpg9.PeticionTransaccionMpg9;
import com.bbva.pzic.cards.dao.model.mpg9.RespuestaTransaccionMpg9;
import org.springframework.stereotype.Component;

/**
 * Created on 9/02/2018.
 *
 * @author Entelgy
 */
@Component("transaccionMpg9")
public class TransaccionMpg9Mock implements InvocadorTransaccion<PeticionTransaccionMpg9, RespuestaTransaccionMpg9> {

    public static final String TEST_EMPTY = "6666";
    public static final String TEST_NO_RESPONSE = "9999";

    @Override
    public RespuestaTransaccionMpg9 invocar(PeticionTransaccionMpg9 peticion) throws ExcepcionTransaccion {
        RespuestaTransaccionMpg9 response = new RespuestaTransaccionMpg9();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENG9 format = peticion.getCuerpo().getParte(FormatoMPMENG9.class);
        final String numberCard = format.getNumtarj();
        switch (numberCard) {
            case TEST_NO_RESPONSE:
                return response;
            case TEST_EMPTY:
                response.getCuerpo().getPartes().add(this.buildData(FormatoMpg9Mock.getInstance().getFormatoMPMS1G9Empty()));
                return response;
            default:
                response.getCuerpo().getPartes().add(this.buildData(FormatoMpg9Mock.getInstance().getFormatoMPMS1G9()));
                return response;
        }
    }

    @Override
    public RespuestaTransaccionMpg9 invocarCache(PeticionTransaccionMpg9 peticion) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida buildData(Object object) {
        CopySalida copy = new CopySalida();
        copy.setCopy(object);
        return copy;
    }
}

package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "specificProposalContact", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "specificProposalContact", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SpecificProposalContact implements Serializable {

    private static final long serialVersionUID = 1L;

    private String contactType;
    /**
     * This object represents the specific information of the contact depending
     * on its type.
     */
    private MobileProposal contact;
    /**
     * Contact information identifier.
     */
    private String id;

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public MobileProposal getContact() {
        return contact;
    }

    public void setContact(MobileProposal contact) {
        this.contact = contact;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

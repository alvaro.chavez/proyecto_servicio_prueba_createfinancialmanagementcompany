package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "TransactionType", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "TransactionType", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Transaction type identifier. DISCLAIMER: UNCATEGORIZED type will be
     * accompanied by information that must be fulfilled on the attribute
     * internalCode that belogs to transactionType.
     */
    private String id;
    /**
     * Name associated to the transaction type.
     */
    private String name;
    /**
     * Associated to the transaction type that can\`t be categorized and must be
     * considered. DISCLAIMER: In case of using UNCATEGORIZED type, this
     * attribute must be forcible informed with the informations that talks
     * about which kind of transaction is being shown.
     */
    private InternalCode internalCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InternalCode getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(InternalCode internalCode) {
        this.internalCode = internalCode;
    }
}
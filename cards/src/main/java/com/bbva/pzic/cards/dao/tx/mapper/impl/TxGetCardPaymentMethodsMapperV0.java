package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.PaymentAmount;
import com.bbva.pzic.cards.canonic.PaymentMethod;
import com.bbva.pzic.cards.canonic.Value;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMENG5;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMS1G5;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMS2G5;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardPaymentMethodsMapperV0;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 25/10/2017.
 *
 * @author Entelgy
 */
@Component("txGetCardPaymentMethodsMapperV0")
public class TxGetCardPaymentMethodsMapperV0 implements ITxGetCardPaymentMethodsMapperV0 {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public FormatoMPMENG5 mapIn(final DTOIntCard dtoIn) {
        FormatoMPMENG5 formatoMPMENG5 = new FormatoMPMENG5();
        formatoMPMENG5.setIdetarj(dtoIn.getCardId());
        return formatoMPMENG5;
    }

    @Override
    public PaymentMethod mapOut1(final FormatoMPMS1G5 formatOutput, final PaymentMethod dtoOut) {
        dtoOut.setId(translator.translateBackendEnumValueStrictly("paymentMethod.paymentType.id", formatOutput.getIdforpa()));
        dtoOut.setName(formatOutput.getDsforpa());
        dtoOut.setEndDate(formatOutput.getFecpago());
        return dtoOut;
    }

    @Override
    public PaymentMethod mapOut2(final FormatoMPMS2G5 formatOutput, final PaymentMethod dtoOut) {
        if (dtoOut.getPaymentAmounts() == null) {
            dtoOut.setPaymentAmounts(new ArrayList<>());
        }

        PaymentAmount paymentAmount = new PaymentAmount();
        paymentAmount.setId(translator.translateBackendEnumValueStrictly("paymentMethod.paymentAmountsType.id", formatOutput.getIdforpa()));
        paymentAmount.setName(formatOutput.getDsforpa());
        paymentAmount.setValues(mapOutListValue(formatOutput.getImpamis(), formatOutput.getMopamis()));

        dtoOut.setPaymentAmounts(updatePaymentAmounts(dtoOut.getPaymentAmounts(), paymentAmount));
        return dtoOut;
    }

    private List<Value> mapOutListValue(BigDecimal ammount, String currency) {
        if (ammount == null && currency == null) {
            return null;
        }
        Value value = new Value();
        value.setAmount(ammount);
        value.setCurrency(currency);
        List<Value> valueList = new ArrayList<>();
        valueList.add(value);
        return valueList;
    }

    private List<PaymentAmount> updatePaymentAmounts(List<PaymentAmount> paymentAmounts, PaymentAmount paymentAmount) {
        boolean found = true;
        for (PaymentAmount amount : paymentAmounts) {
            if (amount.getId().equals(paymentAmount.getId())) {
                amount.getValues().addAll(paymentAmount.getValues());
                found = false;
            }
        }
        if (found) {
            paymentAmounts.add(paymentAmount);
        }
        return paymentAmounts;
    }
}

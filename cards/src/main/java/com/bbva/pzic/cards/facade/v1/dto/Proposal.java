package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.bbva.pzic.routine.commons.utils.BooleanAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "proposal", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "proposal", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Proposal implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier of the request that is pending to be formalized, associated
     * with a card. For example when the customer requested a credit card and go
     * to the branch to pick it up.
     */
    private String id;
    /**
     * Financial product type.
     */
    private CardType cardType;
    /**
     * Financial product title.
     */
    private Product product;
    /**
     * Physical support type associated to the card.
     */
    private PhysicalSupport physicalSupport;
    /**
     * Details of delivery of products or documents associated to the
     * registration of a card. Note - If the physical support is different from
     * VIRTUAL, the field becomes mandatory.
     */
    private List<Delivery> deliveries;
    /**
     * Payment method related to a specific credit card.
     */
    private PaymentMethod paymentMethod;
    /**
     * Granted credit. This amount may be provided in several currencies
     * (depending on the country). This attribute is mandatory for credit cards.
     */
    private List<Import> grantedCredits;
    /**
     * Contact information value. This value will be masked when basic
     * authentication state. If the authentication state is advanced the value
     * will be clear.
     */
    private SpecificProposalContact contact;
    /**
     * Rate charge applied to the type of offer associated.
     */
    private ProposalRate rates;
    /**
     * Commissions associated with the contracting of a Credit Card. They may or may not come more than one fee depending on the offer related.
     */
    private TransferFees fees;
    /**
     * Additional products that BBVA considers should be offered in conjunction with the main product of the offer.
     */
    private List<AdditionalProduct> additionalProducts;
    /**
     * Membership associated to the card.
     */
    private Membership membership;
    /**
     * Identifier associated to the proposal modification status.
     */
    private String status;
    /**
     * String based on the ISO-8601 time stamp format to specify the operation
     * date.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar operationDate;
    /**
     * Image selected for the customer to be printed on the card.
     */
    private Image image;
    /**
     * Detail of the schedule in which the customer can be contacted if necessary.
     */
    private ContactAbility contactability;
    /**
     * Unique offer associated with the card.
     */
    private String offerId;
    /**
     * On a business card the participantId will be the authorized person that will hold the card.
     * It is required to specify AUTHORIZED as participant type also.
     */
    private List<KnowParticipant> participants;
    /**
     * Indicator of the activation of notifications to the customer, through the contacts that he has indicated,
     * when operations are carried out on the card.
     */
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    private String notificationsByOperation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public PhysicalSupport getPhysicalSupport() {
        return physicalSupport;
    }

    public void setPhysicalSupport(PhysicalSupport physicalSupport) {
        this.physicalSupport = physicalSupport;
    }

    public List<Delivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<Delivery> deliveries) {
        this.deliveries = deliveries;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<Import> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<Import> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public SpecificProposalContact getContact() {
        return contact;
    }

    public void setContact(SpecificProposalContact contact) {
        this.contact = contact;
    }

    public ProposalRate getRates() {
        return rates;
    }

    public void setRates(ProposalRate rates) {
        this.rates = rates;
    }

    public TransferFees getFees() {
        return fees;
    }

    public void setFees(TransferFees fees) {
        this.fees = fees;
    }

    public List<AdditionalProduct> getAdditionalProducts() {
        return additionalProducts;
    }

    public void setAdditionalProducts(List<AdditionalProduct> additionalProducts) {
        this.additionalProducts = additionalProducts;
    }

    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Calendar getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Calendar operationDate) {
        this.operationDate = operationDate;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public ContactAbility getContactability() {
        return contactability;
    }

    public void setContactability(ContactAbility contactability) {
        this.contactability = contactability;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public List<KnowParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<KnowParticipant> participants) {
        this.participants = participants;
    }

    public String getNotificationsByOperation() {
        return notificationsByOperation;
    }

    public void setNotificationsByOperation(String notificationsByOperation) {
        this.notificationsByOperation = notificationsByOperation;
    }
}

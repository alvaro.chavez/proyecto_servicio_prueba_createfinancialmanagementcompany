package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.facade.v0.mapper.IModifyCardV0Mapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD;

/**
 * Created on 23/03/2018.
 *
 * @author Entelgy
 */
@Mapper
public class ModifyCardV0Mapper implements IModifyCardV0Mapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public DTOIntCard mapIn(String cardId, Card card) {
        DTOIntCard dtoInt = new DTOIntCard();
        dtoInt.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_MODIFY_CARD));
        if (card.getStatus() != null) {
            dtoInt.setStatusId(translator.translateFrontendEnumValueStrictly("cards.status.id", card.getStatus().getId()));

            if (card.getStatus().getReason() != null)
                dtoInt.setStatusReasonId(card.getStatus().getReason().getId());
        }

        return dtoInt;
    }
}

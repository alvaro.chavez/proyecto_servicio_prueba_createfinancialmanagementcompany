package com.bbva.pzic.cards.dao.model.mpg2.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMENG2;
import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMS1G2;
import com.bbva.pzic.cards.dao.model.mpg2.PeticionTransaccionMpg2;
import com.bbva.pzic.cards.dao.model.mpg2.RespuestaTransaccionMpg2;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 6/10/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpg2")
public class TransaccionMpg2Mock implements InvocadorTransaccion<PeticionTransaccionMpg2, RespuestaTransaccionMpg2> {

    public static final String EMPTY_TEST = "9999";
    private FormatoMPMS1G2Mock formatoMPMS1G2Mock;

    @PostConstruct
    private void setUp() {
        formatoMPMS1G2Mock = new FormatoMPMS1G2Mock();
    }

    @Override
    public RespuestaTransaccionMpg2 invocar(PeticionTransaccionMpg2 peticion) {
        final RespuestaTransaccionMpg2 response = new RespuestaTransaccionMpg2();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENG2 formatIn = peticion.getCuerpo().getParte(FormatoMPMENG2.class);
        String idTarj = formatIn.getIdetarj();

        if (EMPTY_TEST.equals(idTarj)) {
            return response;
        } else {
            try {
                response.getCuerpo().getPartes().addAll(buildDataCopies());
            } catch (IOException e) {
                throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
            }
        }

        return response;
    }

    @Override
    public RespuestaTransaccionMpg2 invocarCache(PeticionTransaccionMpg2 peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private List<CopySalida> buildDataCopies() throws IOException {
        List<FormatoMPMS1G2> formatoMPMS1G2s = formatoMPMS1G2Mock.getFormatoMPMS1G2s();

        List<CopySalida> copies = new ArrayList<>();
        for (FormatoMPMS1G2 format : formatoMPMS1G2s) {
            CopySalida copy = new CopySalida();
            copy.setCopy(format);
            copies.add(copy);
        }
        return copies;
    }
}

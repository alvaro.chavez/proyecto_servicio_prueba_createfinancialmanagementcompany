package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

/**
 * Created on 08/02/2017.
 *
 * @author Entelgy
 */
public class DTOInputListCardTransactions {

    @Size(max = 16)
    private String cardId;
    @Size(max = 10)
    private String fromOperationDate;
    @Size(max = 10)
    private String toOperationDate;
    @Size(max = 20)
    private String paginationKey;
    @Digits(integer = 3, fraction = 0)
    private Long pageSize;
    private boolean haveFinancingType;
    private String registryId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getPaginationKey() {
        return paginationKey;
    }

    public void setPaginationKey(String paginationKey) {
        this.paginationKey = paginationKey;
    }

    public String getFromOperationDate() {
        return fromOperationDate;
    }

    public void setFromOperationDate(String fromOperationDate) {
        this.fromOperationDate = fromOperationDate;
    }

    public String getToOperationDate() {
        return toOperationDate;
    }

    public void setToOperationDate(String toOperationDate) {
        this.toOperationDate = toOperationDate;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isHaveFinancingType() {
        return haveFinancingType;
    }

    public void setHaveFinancingType(boolean haveFinancingType) {
        this.haveFinancingType = haveFinancingType;
    }

    public String getRegistryId() {
        return registryId;
    }

    public void setRegistryId(String registryId) {
        this.registryId = registryId;
    }
}

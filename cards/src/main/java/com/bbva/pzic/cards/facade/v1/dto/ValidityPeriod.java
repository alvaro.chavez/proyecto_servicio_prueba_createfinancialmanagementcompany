package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "validityPeriod", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "validityPeriod", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidityPeriod implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Temporary unit identifier of the period of validity.
     */
    private String timeUnit;
    /**
     * Numeric value that represents the validity of code.
     */
    private Integer value;

    public String getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(String timeUnit) {
        this.timeUnit = timeUnit;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}

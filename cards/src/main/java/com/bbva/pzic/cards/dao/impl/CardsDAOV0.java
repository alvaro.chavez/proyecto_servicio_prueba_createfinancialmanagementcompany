package com.bbva.pzic.cards.dao.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.ICardsDAOV0;
import com.bbva.pzic.cards.dao.apx.*;
import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.dao.rest.*;
import com.bbva.pzic.cards.dao.tx.*;
import com.bbva.pzic.cards.facade.v0.dto.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created on 24/10/2017.
 *
 * @author Entelgy
 */
@Repository
public class CardsDAOV0 implements ICardsDAOV0 {

    private static final Log LOG = LogFactory.getLog(CardsDAOV0.class);

    @Autowired
    private TxGetCardPaymentMethodsV0 txGetCardPaymentMethodsV0;

    @Autowired
    private TxListCardsV0 txListCardsV0;

    @Autowired
    private TxListCardLimits txListCardLimits;

    @Autowired
    private TxGetCard txGetCard;

    @Autowired
    private RestListImagesCovers restListImagesCovers;

    @Autowired
    private TxModifyCardLimitV0 txModifyCardLimitV0;

    @Autowired
    private TxGetCardTransactionV0 txGetCardTransactionV0;

    @Resource(name = "txCreateCardV0")
    private TxCreateCardV0 txCreateCardV0;

    @Resource(name = "txCreateCardAdvanceV0")
    private TxCreateCardAdvanceV0 txCreateCardAdvanceV0;

    @Autowired
    private TxListInstallmentPlansV0 txListInstallmentPlans;

    @Autowired
    private TxCreateCardInstallmentsPlan txCreateCardInstallmentsPlan;

    @Autowired
    private TxModifyCard txModifyCard;

    @Autowired
    private TxSimulateCardInstallmentsPlanV0 txSimulateCardInstallmentsPlanV0;

    @Autowired
    private TxGetCardConditions txGetCardConditions;

    @Autowired
    private TxListCardFinancialStatementsV0 txListCardFinancialStatementsV0;

    @Autowired
    private TxGetCardFinancialStatement txGetCardFinancialStatement;

    @Autowired
    private RestDigitizeDocumentFile restGetDocumentFile;

    @Autowired
    private TxCreateCardsMaskedToken txCreateCardsMaskedToken;

    @Autowired
    private TxGetCardMembership txGetCardMembership;

    @Autowired
    private TxGetCardsCardOffer txGetCardsCardOffer;

    @Autowired
    private RestCreateCardsOfferSimulate restCreateCardsOfferSimulate;

    @Autowired
    private RestListImagesCoversHolderSimulation restCreateCardsOfferSimulateAWS;

    @Autowired
    private TxCreateCardsProposal txCreateCardsProposal;

    @Autowired
    private TxListCardRelatedContracts txListCardRelatedContracts;

    @Autowired
    private TxListCardCashAdvances txListCardCashAdvances;

    @Autowired
    private TxCreateCardCashRefund txCreateCardCashRefund;

    @Autowired
    private RestCreateCardReportsV0 restCreateCardReportsV0;

    @Autowired
    private ApxCreateOffersGenerateCards apxCreateOffersGenerateCards;

    @Override
    public com.bbva.pzic.cards.canonic.PaymentMethod getCardPaymentMethods(final DTOIntCard dtoIn) {
        LOG.info("... called method CardsDAOV0.getCardPaymentMethods ...");
        return txGetCardPaymentMethodsV0.perform(dtoIn);
    }

    @Override
    public DTOOutListCards listCardsV0(final DTOIntListCards dtoIn) {
        LOG.info("... called method CardsDAOV0.listCards ...");
        return txListCardsV0.perform(dtoIn);
    }

    @Override
    public List<Limits> listCardLimits(final DTOIntCard dtoIn) {
        LOG.info("... Invoking method CardsDAOV0.listCardLimits ...");
        return txListCardLimits.perform(dtoIn);
    }

    @Override
    public Card getCard(final DTOIntCard dtoIn) {
        LOG.info("... called method CardsDAO.getCard ...");
        return txGetCard.perform(dtoIn);
    }

    @Override
    public void listImagesCovers(final DTOOutListCards dtoOut) {
        LOG.info("... Invoking method CardsDAOV0.listImagesCovers ...");
        restListImagesCovers.perform(dtoOut);
    }

    @Override
    public Limit modifyCardLimit(final InputModifyCardLimit modifyCardLimit) {
        LOG.info("... Invoking method CardsDAOV0.modifyCardLimit ...");
        return txModifyCardLimitV0.perform(modifyCardLimit);
    }

    @Override
    public TransactionData getCardTransaction(final InputGetCardTransaction input) {
        LOG.info("... called method CardsDAOV0.getCardTransaction ...");
        return txGetCardTransactionV0.perform(input);
    }

    @Override
    public Card createCard(final InputCreateCard input) {
        LOG.info("... called method CardsDAOV0.createCard ...");
        return txCreateCardV0.perform(input);
    }

    @Override
    public Card createCardAdvance(final InputCreateCard inputCreateCard) {
        LOG.info("... called method CardsDAOV0.createCardAdvance ...");
        return txCreateCardAdvanceV0.perform(inputCreateCard);
    }

    @Override
    public DTOInstallmentsPlanList listInstallmentPlans(final InputListInstallmentPlans input) {
        LOG.info("... Invoking method CardsDAOV0.listInstallmentPlans ...");
        return txListInstallmentPlans.perform(input);
    }

    @Override
    public InstallmentsPlanData createInstallmentsPlan(final DTOIntCardInstallmentsPlan input) {
        LOG.info("... called method CardsDAOV0.createInstallmentsPlan ...");
        return txCreateCardInstallmentsPlan.perform(input);
    }

    @Override
    public void modifyCard(final DTOIntCard dtoIn) {
        LOG.info("... called method CardsDAOV0.modifyCard ...");
        txModifyCard.perform(dtoIn);
    }

    @Override
    public InstallmentsPlanSimulationData simulateInstallmentsPlan(DTOIntCardInstallmentsPlan input) {
        LOG.info("... called method CardsDAOV0.simulateInstallmentsPlan ...");
        return txSimulateCardInstallmentsPlanV0.perform(input);
    }

    @Override
    public List<Condition> getCardConditions(final InputGetCardConditions input) {
        LOG.info("... Invoking method CardsDAOV0.getCardConditions ...");
        return txGetCardConditions.perform(input);
    }

    @Override
    public DTOIntStatementList listCardFinancialStatements(final InputListCardFinancialStatements input) {
        LOG.info("... Invoking method CardsDAOV0.listCardFinancialStatements ...");
        return txListCardFinancialStatementsV0.perform(input);
    }

    @Override
    public DTOIntCardStatement getCardFinancialStatement(final InputGetCardFinancialStatement input) {
        LOG.info("... Invoking method CardsDAOV0.getCardFinancialStatement ...");
        return txGetCardFinancialStatement.perform(input);
    }

    @Override
    public Document getCardFinancialStatementDocument(final DocumentRequest input) {
        LOG.info("... Invoking method CardsDAOV0.getCardFinancialStatementDocument ...");
        return restGetDocumentFile.perform(input);
    }

    @Override
    public MaskedToken createCardsMaskedToken(final InputCreateCardsMaskedToken input) {
        LOG.info("... Invoking method CardsDAOV0.createCardsMaskedToken ...");
        return txCreateCardsMaskedToken.perform(input);
    }

    @Override
    public DTOIntMembershipServiceResponse getCardMembership(final DTOIntSearchCriteria searchCriteria) {
        LOG.info("----- Dentro de CardsDAOV0.getCardMembership() -----");
        return txGetCardMembership.perform(searchCriteria);
    }

    @Override
    public Offer getCardsCardOffer(final InputGetCardsCardOffer input) {
        LOG.info("... Invoking method CardsDAOV0.getCardsCardOffer ...");
        return txGetCardsCardOffer.perform(input);
    }

    @Override
    public HolderSimulation createCardsOfferSimulate(final DTOIntDetailSimulation input) {
        LOG.info("... Invoking method CardsDAOV0.createCardsOfferSimulate ...");
        return restCreateCardsOfferSimulate.perform(input);
    }

    @Override
    public void createImageCover(final HolderSimulation input) {
        LOG.info("... Invoking method CardsDAOV0.createImageCover ...");
        restCreateCardsOfferSimulateAWS.perform(input);
    }

    @Override
    public Proposal createCardsProposal(final DTOIntProposal dtoIntProposal) {
        LOG.info("... Invoking method CardsDAOV0.createCardsProposal ...");
        return txCreateCardsProposal.perform(dtoIntProposal);
    }

    @Override
    public List<RelatedContracts> listCardRelatedContracts(final DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria) {
        LOG.info("... Invoking method CardsDAOV0.listCardRelatedContracts ...");
        return txListCardRelatedContracts.perform(dtoIntRelatedContractsSearchCriteria);
    }

    @Override
    public DTOIntListCashAdvances listCardCashAdvances(final DTOIntCashAdvancesSearchCriteria dtoIntCashAdvancesSearchCriteria) {
        LOG.info("... Invoking method CardsDAOV0.listCardCashAdvances ...");
        return txListCardCashAdvances.perform(dtoIntCashAdvancesSearchCriteria);
    }

    @Override
    public DTOIntCashRefund createCardCashRefund(final DTOInputCreateCashRefund dtoInputCreateCashRefund) {
        LOG.info("... Invoking method CardsDAOV0.createCardCashRefund ...");
        return txCreateCardCashRefund.perform(dtoInputCreateCashRefund);
    }

    @Override
    public void createCardReports(final InputCreateCardReports input) {
        LOG.info("... Invoking method CardsDAOV0.createCardReports ...");
        restCreateCardReportsV0.invoke(input);
    }

    @Override
    public OfferGenerate createOffersGenerateCards(final InputCreateOffersGenerateCards input) {
        LOG.info("... Invoking method CardsDAOV0.createOffersGenerateCards ...");
        return apxCreateOffersGenerateCards.perform(input);
    }
}

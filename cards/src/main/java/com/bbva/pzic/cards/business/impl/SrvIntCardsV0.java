package com.bbva.pzic.cards.business.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.ISrvIntCardsV0;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.ICardsDAOV0;
import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.facade.v0.dto.*;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * Created on 24/10/2017.
 *
 * @author Entelgy
 */
@Component
public class SrvIntCardsV0 implements ISrvIntCardsV0 {

    private static final Log LOG = LogFactory.getLog(SrvIntCardsV0.class);
    private static final String ENUM_VALUE_CANCELED = "C";

    @Autowired
    private ICardsDAOV0 daoV0;
    @Autowired
    private Validator validator;

    @Override
    public com.bbva.pzic.cards.canonic.PaymentMethod getCardPaymentMethods(final DTOIntCard dtoIntCard) {
        LOG.info("... invoke method SrvIntCards.getCardPaymentMethods ...");
        LOG.info("... validating getCardPaymentMethods input parameter ...");
        validator.validate(dtoIntCard, ValidationGroup.GetCardPaymentMethodsV0.class);
        return daoV0.getCardPaymentMethods(dtoIntCard);
    }

    @Override
    public DTOOutListCards listCardsV0(final DTOIntListCards dtoIn) {
        LOG.info("... called method SrvIntCardsV0.listCardsV0 ...");
        LOG.info("... validating listCardsV0 input parameter ...");
        validator.validate(dtoIn, ValidationGroup.ListCardsV0.class);

        DTOOutListCards dtoOut = daoV0.listCardsV0(dtoIn);

        daoV0.listImagesCovers(dtoOut);

        return dtoOut;
    }

    @Override
    public List<Limits> listCardLimits(DTOIntCard dtoIn) {
        LOG.info("... invoke method SrvIntCardsV0.listCardLimits ...");
        LOG.info("... validating listCardLimits input parameter ...");
        validator.validate(dtoIn, ValidationGroup.ListCardLimits.class);
        return daoV0.listCardLimits(dtoIn);
    }

    @Override
    public Card getCard(final DTOIntCard dtoIntCard) {
        LOG.info("... invoke method SrvIntCards.getCard ...");
        LOG.info("... validating getCard input parameter ...");
        validator.validate(dtoIntCard, ValidationGroup.GetCard.class);

        Card card = daoV0.getCard(dtoIntCard);

        if (card == null) {
            return null;
        }

        DTOOutListCards dtoOut = new DTOOutListCards();
        dtoOut.setData(Collections.singletonList(card));
        daoV0.listImagesCovers(dtoOut);

        return dtoOut.getData().get(0);
    }

    @Override
    public Limit modifyCardLimit(InputModifyCardLimit modifyCardLimit) {
        LOG.info("... called method SrvIntCardsV0.modifyCardLimit ...");
        LOG.info("... validating modifyCardLimit input parameter ...");
        validator.validate(modifyCardLimit, ValidationGroup.ModifyCardLimitV0.class);
        return daoV0.modifyCardLimit(modifyCardLimit);
    }

    @Override
    public TransactionData getCardTransaction(InputGetCardTransaction input) {
        LOG.info("... called method SrvIntCardsV0.getCardTransaction ...");
        LOG.info("... validating getCardTransaction input parameter ...");
        validator.validate(input, ValidationGroup.GetCardTransactionV0.class);
        return daoV0.getCardTransaction(input);
    }

    @Override
    public Card createCard(InputCreateCard input) {
        LOG.info("... called method SrvIntCardsV0.createCard ...");
        LOG.info("... validating createCard input parameter ...");

        if (input.getDtoIntCard().getNumber() == null) {
            validator.validate(input, ValidationGroup.CreateCardV0.class);
            return daoV0.createCard(input);
        } else {
            validator.validate(input, ValidationGroup.CreateCardAdvanceV0.class);
            return daoV0.createCardAdvance(input);
        }
    }

    @Override
    public DTOInstallmentsPlanList listInstallmentPlans(InputListInstallmentPlans installmentPlans) {
        LOG.info("... invoke method SrvIntCardsV0.listInstallmentPlans ...");
        LOG.info("... validating listInstallmentPlans input parameter ...");
        validator.validate(installmentPlans, ValidationGroup.ListInstallmentPlansV0.class);
        return daoV0.listInstallmentPlans(installmentPlans);
    }

    @Override
    public InstallmentsPlanData createCardInstallmentsPlan(DTOIntCardInstallmentsPlan dtoIn) {
        LOG.info("... invoke method SrvIntCardsV0.createCardInstallmentsPlan ...");
        LOG.info("... validating createCardInstallmentsPlan input parameter ...");
        validator.validate(dtoIn, ValidationGroup.CreateInstallmentsPlanV0.class);
        return daoV0.createInstallmentsPlan(dtoIn);
    }

    @Override
    public void modifyCard(DTOIntCard dtoInt) {
        LOG.info("... invoke method SrvIntCardsV0.modifyCard ...");
        LOG.info("... validating modifyCard input parameter ...");
        validator.validate(dtoInt, ValidationGroup.ModifyCardV0.class);
        if (dtoInt.getStatusId().equals(ENUM_VALUE_CANCELED)) {
            if (dtoInt.getStatusReasonId() != null) {
                daoV0.modifyCard(dtoInt);
            } else {
                throw new BusinessServiceException(Errors.MANDATORY_PARAMETERS_MISSING);
            }
        } else {
            daoV0.modifyCard(dtoInt);
        }
    }

    @Override
    public InstallmentsPlanSimulationData simulateCardInstallmentsPlan(DTOIntCardInstallmentsPlan dtoIn) {
        LOG.info("... invoke method SrvIntCardsV0.simulateCardInstallmentsPlan ...");
        LOG.info("... validating simulateCardInstallmentsPlan input parameter ...");
        validator.validate(dtoIn, ValidationGroup.SimulateInstallmentsPlanV0.class);
        return daoV0.simulateInstallmentsPlan(dtoIn);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Condition> getCardConditions(final InputGetCardConditions input) {
        LOG.info("... Invoking method SrvIntCardsV0.getCardConditions ...");
        LOG.info("... validating getCardConditions input parameter ...");
        validator.validate(input, ValidationGroup.GetConditions.class);
        return daoV0.getCardConditions(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntStatementList listCardFinancialStatements(final InputListCardFinancialStatements input) {
        LOG.info("... Invoking method SrvIntCardsV0.listCardFinancialStatements ...");
        LOG.info("... validating listCardFinancialStatements input parameter ...");
        validator.validate(input, ValidationGroup.ListCardFinancialStatementsV0.class);
        return daoV0.listCardFinancialStatements(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntCardStatement getCardFinancialStatement(InputGetCardFinancialStatement input) {
        LOG.info("... Invoking method SrvIntCardsV0.getCardFinancialStatement ...");
        LOG.info("... validating getCardFinancialStatement input parameter ...");
        validator.validate(input, ValidationGroup.GetCardFinancialStatement.class);
        return daoV0.getCardFinancialStatement(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Document getCardFinancialStatementDocument(final DocumentRequest input) {
        LOG.info("... Invoking method SrvIntCardsV0.getCardFinancialStatementDocument ...");
        LOG.info("... validating getCardFinancialStatementDocument input parameter ...");
        validator.validate(input, ValidationGroup.GetCardFinancialStatementDocument.class);
        return daoV0.getCardFinancialStatementDocument(input);
    }

    @Override
    public MaskedToken createCardsMaskedToken(final InputCreateCardsMaskedToken input) {
        LOG.info("... Invoking method SrvIntCardsV0.createCardsMaskedToken ...");
        LOG.info("... validating createCardsMaskedToken input parameter ...");
        validator.validate(input, ValidationGroup.CreateCardsMaskedToken.class);
        return daoV0.createCardsMaskedToken(input);
    }

    @Override
    public DTOIntMembershipServiceResponse getCardMembership(DTOIntSearchCriteria searchCriteria) {
        LOG.info("----- Dentro de SrvIntCardsV0.getCrdMembership() -----");
        return daoV0.getCardMembership(searchCriteria);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Offer getCardsCardOffer(InputGetCardsCardOffer input) {
        LOG.info("... Invoking method SrvIntCards.getCardsCardOffer ...");
        validator.validate(input, ValidationGroup.GetCardsCardOffer.class);
        return daoV0.getCardsCardOffer(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HolderSimulation createCardsOfferSimulate(final DTOIntDetailSimulation input) {
        LOG.info("... Invoking method SrvIntCards.createCardsOfferSimulate ...");
        validator.validate(input, ValidationGroup.CreateCardsOfferSimulate.class);

        HolderSimulation holderSimulation = daoV0.createCardsOfferSimulate(input);
        if (holderSimulation == null)
            return null;

        daoV0.createImageCover(holderSimulation);
        return holderSimulation;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Proposal createCardsProposal(DTOIntProposal dtoIntProposal) {
        LOG.info("... Invoking method SrvIntCards.createCardsProposal ...");
        validator.validate(dtoIntProposal, ValidationGroup.CreateCardsProposal.class);
        return daoV0.createCardsProposal(dtoIntProposal);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<RelatedContracts> listCardRelatedContracts(final DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria) {
        LOG.info("... Invoking method SrvIntCards.listCardRelatedContracts ...");
        return daoV0.listCardRelatedContracts(dtoIntRelatedContractsSearchCriteria);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntListCashAdvances listCardCashAdvances(DTOIntCashAdvancesSearchCriteria dtoIntCashAdvancesSearchCriteria) {
        LOG.info("... Invoking method SrvIntCards.listCardCashAdvances ...");
        return daoV0.listCardCashAdvances(dtoIntCashAdvancesSearchCriteria);
    }

    @Override
    public DTOIntCashRefund createCardCashRefund(DTOInputCreateCashRefund dtoInputCreateCashRefund) {
        LOG.info("... Invoking method SrvIntCards.createCardCashRefund ...");
        LOG.info("... validating createCardCashRefund input parameter ...");
        validator.validate(dtoInputCreateCashRefund, ValidationGroup.CreateCardCashRefund.class);
        return daoV0.createCardCashRefund(dtoInputCreateCashRefund);
    }

    @Override
    public void createCardReports(final InputCreateCardReports input) {
        LOG.info("... Invoking method SrvIntCards.createCardReports ...");
        LOG.info("... Validating createCardReports input ...");
        validator.validate(input, ValidationGroup.CreateCardReportsV0.class);
        daoV0.createCardReports(input);
    }

    @Override
    public OfferGenerate createOffersGenerateCards(final InputCreateOffersGenerateCards input) {
        LOG.info("... Invoking method SrvIntCards.createOffersGenerateCards ...");
        LOG.info("... Validating createOffersGenerateCards input ...");
        validator.validate(input, ValidationGroup.CreateOffersGenerateCards.class);
        return daoV0.createOffersGenerateCards(input);
    }
}

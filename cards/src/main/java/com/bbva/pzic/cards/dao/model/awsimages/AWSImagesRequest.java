package com.bbva.pzic.cards.dao.model.awsimages;

import java.util.List;

/**
 * Created on 30/07/2019.
 *
 * @author Entelgy
 */
public class AWSImagesRequest {

    private ModelCountry country;
    private Boolean isDefaultImage;
    private String width;
    private String height;
    private List<ModelCard> cards;

    public AWSImagesRequest() {
        this.isDefaultImage = true;
    }

    public ModelCountry getCountry() {
        return country;
    }

    public void setCountry(ModelCountry country) {
        this.country = country;
    }

    public Boolean getIsDefaultImage() {
        return isDefaultImage;
    }

    public void setIsDefaultImage(Boolean isDefaultImage) {
        this.isDefaultImage = isDefaultImage;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public List<ModelCard> getCards() {
        return cards;
    }

    public void setCards(List<ModelCard> cards) {
        this.cards = cards;
    }
}

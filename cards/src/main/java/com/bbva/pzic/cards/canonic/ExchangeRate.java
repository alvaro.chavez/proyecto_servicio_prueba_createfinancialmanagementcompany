package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "ExchangeRate", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "ExchangeRate", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExchangeRate implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Date exchange rate. Date when transfer has been made. String based on
     * ISO-8601 date format.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name="dateTime")
    private Calendar date;
    /**
     * Detailed info for the exchange rate.
     */
    private Values values;

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public Values getValues() {
        return values;
    }

    public void setValues(Values values) {
        this.values = values;
    }
}

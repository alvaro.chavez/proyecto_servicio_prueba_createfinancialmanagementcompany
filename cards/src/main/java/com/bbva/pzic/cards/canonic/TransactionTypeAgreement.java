package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 26/12/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "TransactionTypeAgreement", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "TransactionTypeAgreement", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionTypeAgreement implements Serializable {

    private static final long serialVersionUID = 1L;
}
package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.utils.Money;
import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0DET;
import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0WSC;
import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0WSE;
import com.bbva.pzic.cards.dao.tx.mapper.ITxSimulateCardInstallmentsPlanMapperV0;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
@Mapper
public class TxSimulateCardInstallmentsPlanMapperV0 extends ConfigurableMapper implements ITxSimulateCardInstallmentsPlanMapperV0 {

    private static final String CONSTANT_RATE_TYPE_ID_TEA = "TEA";
    private static final String CONSTANT_RATE_TYPE_NAME_TEA = "TASA EFECTIVA ANUAL";
    private static final String CONSTANT_RATE_TYPE_ID_TCEA = "TCEA";
    private static final String CONSTANT_RATE_TYPE_NAME_TCEA = "TASA DE COSTO EFECTIVA ANUAL";
    private static final String CONSTANT_RATE_MODE_PERCENTAGE = "PERCENTAGE";

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(DTOIntCardInstallmentsPlan.class, FormatoMPM0WSE.class)
                .field("cardId", "numtarj")
                .field("amount", "impoper")
                .field("currency", "divoper")
                .field("termsNumber", "ncuotas")
                .field("transactionId", "numoper")
                .register();

        factory.classMap(FormatoMPM0WSC.class, InstallmentsPlanSimulation.class)
                .field("totcapi", "capital.amount")
                .field("divtcap", "capital.currency")
                .field("totinte", "interest.amount")
                .field("divtint", "interest.currency")
                .field("totcuot", "total.amount")
                .field("divtcuo", "total.currency")
                .field("impcuo1", "firstInstallment.amount")
                .field("divtcuo", "firstInstallment.currency")
                .field("feccuo1", "firstInstallmentDate")
                .field("fecsimu", "simulationDate")
                .register();
    }

    @Override
    public FormatoMPM0WSE mapIn(DTOIntCardInstallmentsPlan dtoIn) {
        return map(dtoIn, FormatoMPM0WSE.class);
    }

    @Override
    public InstallmentsPlanSimulationData mapOut1(FormatoMPM0WSC formatoMPM0WSC, DTOIntCardInstallmentsPlan dtoIn) {
        InstallmentsPlanSimulationData data = new InstallmentsPlanSimulationData();
        InstallmentsPlanSimulation installmentsPlanSimulation = map(formatoMPM0WSC, InstallmentsPlanSimulation.class);
        installmentsPlanSimulation.setRates(mapRatesList(formatoMPM0WSC));

        data.setData(installmentsPlanSimulation);
        return data;
    }

    @Override
    public InstallmentsPlanSimulationData mapOut2(FormatoMPM0DET formatoMPM0DET, InstallmentsPlanSimulationData installmentsPlanSimulationData) {
        InstallmentsPlanSimulation data = installmentsPlanSimulationData.getData();
        if (data.getScheduledPayments() == null) {
            data.setScheduledPayments(new ArrayList<ScheduleInstallment>());
        }
        data.getScheduledPayments().add(mapScheduledPayments(formatoMPM0DET));

        return installmentsPlanSimulationData;
    }

    private ScheduleInstallment mapScheduledPayments(FormatoMPM0DET formatoMPM0DET) {
        ScheduleInstallment scheduleInstallment = new ScheduleInstallment();
        scheduleInstallment.setMaturityDate(formatoMPM0DET.getFecpago());
        scheduleInstallment.setTotal(mapMoney(formatoMPM0DET.getImpcuot(), formatoMPM0DET.getDivcuot()));
        scheduleInstallment.setCapital(mapMoney(formatoMPM0DET.getCapital(), formatoMPM0DET.getDivcapc()));
        scheduleInstallment.setInterest(mapMoney(formatoMPM0DET.getInteres(), formatoMPM0DET.getDivinte()));
        scheduleInstallment.setFee(mapMoney(formatoMPM0DET.getComisio(), formatoMPM0DET.getDivcomi()));

        return scheduleInstallment;
    }

    private Money mapMoney(BigDecimal amount, String currency) {
        if (amount == null && currency == null) {
            return null;
        }
        Money money = new Money();
        money.setAmount(amount);
        money.setCurrency(currency);

        return money;
    }

    private List<Rate> mapRatesList(FormatoMPM0WSC formatoMPM0TSC) {
        List<Rate> rateList = new ArrayList<>();
        rateList.add(mapRates(CONSTANT_RATE_TYPE_ID_TEA, CONSTANT_RATE_TYPE_NAME_TEA, formatoMPM0TSC.getTasaefa()));
        rateList.add(mapRates(CONSTANT_RATE_TYPE_ID_TCEA, CONSTANT_RATE_TYPE_NAME_TCEA, formatoMPM0TSC.getPortcea()));

        return rateList;
    }

    private Rate mapRates(final String rateTypeId, final String rateTypeName, BigDecimal tasaefa) {
        Rate rate = new Rate();
        rate.setRateType(mapRateType(rateTypeId, rateTypeName));
        rate.setMode(new Mode());
        rate.getMode().setName(CONSTANT_RATE_MODE_PERCENTAGE);
        if (tasaefa != null) {
            rate.setUnit(new Percentage());
            rate.getUnit().setPercentage(tasaefa);
        }

        return rate;
    }

    private RateType mapRateType(final String rateTypeId, final String rateTypeName) {
        RateType rateType = new RateType();
        rateType.setId(enumMapper.getEnumValue("installmentPlan.rates.rateType.id", rateTypeId));
        rateType.setName(rateTypeName);

        return rateType;
    }
}

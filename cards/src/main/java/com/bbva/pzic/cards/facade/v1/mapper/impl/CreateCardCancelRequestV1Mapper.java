package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v1.dto.CardRequestCancel;
import com.bbva.pzic.cards.facade.v1.dto.Reason;
import com.bbva.pzic.cards.facade.v1.dto.RequestCancel;
import com.bbva.pzic.cards.facade.v1.dto.Subproduct;
import com.bbva.pzic.cards.facade.v1.mapper.ICreateCardCancelRequestV1Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateCardCancelRequestV1Mapper implements ICreateCardCancelRequestV1Mapper {

    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    public void setServiceInvocationContext(ServiceInvocationContext serviceInvocationContext) {
        this.serviceInvocationContext = serviceInvocationContext;
    }

    @Override
    public InputCreateCardCancelRequest mapIn(final String cardId, final RequestCancel requestCancel) {
        InputCreateCardCancelRequest dtoInt = new InputCreateCardCancelRequest();
        dtoInt.setCustomerId(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID));
        dtoInt.setCardId(cardId);
        dtoInt.setReason(mapInReason(requestCancel.getReason()));
        dtoInt.setCard(mapInCard(requestCancel.getCard()));
        return dtoInt;
    }

    private DTOIntReason mapInReason(final Reason reason) {
        if (reason == null) {
            return null;
        }

        DTOIntReason dtoIntReason = new DTOIntReason();
        dtoIntReason.setId(reason.getId());
        dtoIntReason.setComments(reason.getComments());
        return dtoIntReason;
    }

    private DTOIntCardRequestCancel mapInCard(final CardRequestCancel card) {
        if (card == null || card.getProduct() == null) {
            return null;
        }

        DTOIntCardRequestCancel dtoIntCardRequestCancel = new DTOIntCardRequestCancel();
        DTOIntProduct dtoIntProduct = new DTOIntProduct();
        dtoIntProduct.setId(card.getProduct().getId());
        dtoIntProduct.setSubproduct(mapInSubproduct(card.getProduct().getSubProduct()));
        dtoIntCardRequestCancel.setProduct(dtoIntProduct);
        return dtoIntCardRequestCancel;
    }

    private DTOIntSubProduct mapInSubproduct(final Subproduct subProduct) {
        if (subProduct == null) {
            return null;
        }

        DTOIntSubProduct dtoIntSubProduct = new DTOIntSubProduct();
        dtoIntSubProduct.setId(subProduct.getId());
        return dtoIntSubProduct;
    }

    @Override
    public ServiceResponse<RequestCancel> mapOut(final RequestCancel cardCancelRequest) {
        if (cardCancelRequest == null) {
            return null;
        }

        return ServiceResponse.data(cardCancelRequest).build();
    }
}

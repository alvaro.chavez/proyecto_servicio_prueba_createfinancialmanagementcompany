package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntQuestion;
import com.bbva.pzic.cards.business.dto.InputCreateOffersGenerateCards;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateOffersGenerateCardsMapper;
import com.bbva.pzic.cards.dao.model.phiat021_1.Answer;
import com.bbva.pzic.cards.dao.model.phiat021_1.Dictum;
import com.bbva.pzic.cards.dao.model.phiat021_1.Question;
import com.bbva.pzic.cards.dao.model.phiat021_1.*;
import com.bbva.pzic.cards.facade.v0.dto.Benefits;
import com.bbva.pzic.cards.facade.v0.dto.Subproduct;
import com.bbva.pzic.cards.facade.v0.dto.Unit;
import com.bbva.pzic.cards.facade.v0.dto.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
@Component
public class ApxCreateOffersGenerateCardsMapper implements IApxCreateOffersGenerateCardsMapper {

    @Override
    public PeticionTransaccionPhiat021_1 mapIn(final InputCreateOffersGenerateCards input) {
        PeticionTransaccionPhiat021_1 peticion = new PeticionTransaccionPhiat021_1();
        peticion.setBcsDeviceScreenSize(input.getBcsDeviceScreenSize());
        peticion.setQuestions(mapInQuestions(input.getQuestions()));
        return peticion;
    }

    private List<Questions> mapInQuestions(final List<DTOIntQuestion> questions) {
        if (CollectionUtils.isEmpty(questions)) {
            return null;
        }

        return questions.stream().filter(Objects::nonNull).map(this::mapInQuestion).collect(Collectors.toList());
    }

    private Questions mapInQuestion(final DTOIntQuestion dtoIntQuestion) {
        Questions result = new Questions();
        result.setQuestion(new Question());
        result.getQuestion().setId(dtoIntQuestion.getId());
        result.getQuestion().setAnswer(mapInAnswer(dtoIntQuestion.getAnswerValue()));
        return result;
    }

    private Answer mapInAnswer(final String answerValue) {
        if (StringUtils.isEmpty(answerValue)) {
            return null;
        }

        Answer result = new Answer();
        result.setValue(answerValue);
        return result;
    }

    @Override
    public OfferGenerate mapOut(final RespuestaTransaccionPhiat021_1 response) {
        OfferGenerate result = new OfferGenerate();
        result.setDictum(mapOutDictum(response.getDictum()));
        result.setCardProducts(mapOutCardProducts(response.getCardproducts()));
        return result;
    }

    private List<CardProduct> mapOutCardProducts(List<Cardproducts> cardproducts) {
        if (CollectionUtils.isEmpty(cardproducts)) {
            return null;
        }

        return cardproducts.stream().filter(cp -> cp != null && cp.getCardproduct() != null).map(this::mapOutCardProduct).collect(Collectors.toList());
    }

    private CardProduct mapOutCardProduct(final Cardproducts cardproducts) {
        CardProduct result = new CardProduct();
        result.setId(cardproducts.getCardproduct().getId());
        result.setName(cardproducts.getCardproduct().getName());
        result.setSubproduct(mapOutSubProduct(cardproducts.getCardproduct().getSubproduct()));
        result.setCardType(mapOutCardType(cardproducts.getCardproduct().getCardtype()));
        result.setImages(mapOutImages(cardproducts.getCardproduct().getImages()));
        result.setGrantedCredits(mapOutGrantedCredits(cardproducts.getCardproduct().getGrantedcredits()));
        result.setRates(mapOutRates(cardproducts.getCardproduct().getRates()));
        result.setPriorityLevel(cardproducts.getCardproduct().getPrioritylevel() == null ? null : cardproducts.getCardproduct().getPrioritylevel().toString());
        result.setBankIdentificationNumber(cardproducts.getCardproduct().getBankidentificationnumber());
        result.setFees(mapOutFees(cardproducts.getCardproduct().getFees()));
        result.setBenefits(mapOutBenefits(cardproducts.getCardproduct().getBenefits()));
        result.setGrantedMinimumCredits(mapOutGrantedMinimumCredits(cardproducts.getCardproduct().getGrantedminimumcredits()));
        result.setLoyaltyProgram(mapOutLoyaltyProgram(cardproducts.getCardproduct().getLoyaltyprogram()));
        result.setBrandAssociation(mapOutBrandAssociation(cardproducts.getCardproduct().getBrandassociation()));
        result.setOfferId(cardproducts.getCardproduct().getOfferid());
        return result;
    }

    private Subproduct mapOutSubProduct(com.bbva.pzic.cards.dao.model.phiat021_1.Subproduct modelSubproduct) {
        if (modelSubproduct == null) {
            return null;
        }
        Subproduct result = new Subproduct();
        result.setId(modelSubproduct.getId());
        return result;
    }

    private BrandAssociation mapOutBrandAssociation(final Brandassociation brandassociation) {
        if (brandassociation == null) {
            return null;
        }

        BrandAssociation result = new BrandAssociation();
        result.setId(brandassociation.getId());
        return result;
    }

    private LoyaltyProgram mapOutLoyaltyProgram(final Loyaltyprogram loyaltyprogram) {
        if (loyaltyprogram == null) {
            return null;
        }

        LoyaltyProgram result = new LoyaltyProgram();
        result.setId(loyaltyprogram.getId());
        return result;
    }

    private List<GrantedMinimumCredit> mapOutGrantedMinimumCredits(final List<Grantedminimumcredits> grantedminimumcredits) {
        if (CollectionUtils.isEmpty(grantedminimumcredits)) {
            return null;
        }

        return grantedminimumcredits.stream().filter(gmc -> gmc != null && gmc.getGrantedminimumcredit() != null).map(this::mapOutGrantedMinimumCredit).collect(Collectors.toList());
    }

    private GrantedMinimumCredit mapOutGrantedMinimumCredit(final Grantedminimumcredits grantedminimumcredits) {
        GrantedMinimumCredit result = new GrantedMinimumCredit();
        result.setAmount(StringUtils.isEmpty(grantedminimumcredits.getGrantedminimumcredit().getAmount()) ? null : new BigDecimal(grantedminimumcredits.getGrantedminimumcredit().getAmount()));
        result.setCurrency(grantedminimumcredits.getGrantedminimumcredit().getCurrency());
        return result;
    }

    private List<Benefits> mapOutBenefits(final List<com.bbva.pzic.cards.dao.model.phiat021_1.Benefits> benefits) {
        if (CollectionUtils.isEmpty(benefits)) {
            return null;
        }

        return benefits.stream().filter(bft -> bft != null && bft.getBenefit() != null).map(this::mapOutBenefit).collect(Collectors.toList());
    }

    private Benefits mapOutBenefit(final com.bbva.pzic.cards.dao.model.phiat021_1.Benefits benefits) {
        Benefits result = new Benefits();
        result.setId(benefits.getBenefit().getId());
        result.setName(benefits.getBenefit().getName());
        result.setDescription(benefits.getBenefit().getDescription());
        return result;
    }

    private FeesOffer mapOutFees(final Fees fees) {
        if (fees == null) {
            return null;
        }

        FeesOffer result = new FeesOffer();
        result.setItemizeFees(mapOutItemizeFees(fees.getItemizefees()));
        return result;
    }

    private List<ItemizeFee> mapOutItemizeFees(final List<Itemizefees> itemizefees) {
        if (CollectionUtils.isEmpty(itemizefees)) {
            return null;
        }

        return itemizefees.stream().filter(ifs -> ifs != null && ifs.getItemizefee() != null).map(this::mapOutItemizeFee).collect(Collectors.toList());
    }

    private ItemizeFee mapOutItemizeFee(final Itemizefees itemizefees) {
        ItemizeFee result = new ItemizeFee();
        result.setFeeType(itemizefees.getItemizefee().getFeetype());
        result.setItemizeFeeUnit(mapOutItemizeFeeUnit(itemizefees.getItemizefee().getItemizefeeunit()));
        return result;
    }

    private Unit mapOutItemizeFeeUnit(final Itemizefeeunit itemizefeeunit) {
        if (itemizefeeunit == null) {
            return null;
        }

        Unit result = new Unit();
        result.setAmount(StringUtils.isEmpty(itemizefeeunit.getAmount()) ? null : new BigDecimal(itemizefeeunit.getAmount()));
        result.setCurrency(itemizefeeunit.getCurrency());
        result.setUnitType(itemizefeeunit.getUnittype());
        return result;
    }

    private List<RateOffer> mapOutRates(final List<Rates> rates) {
        if (CollectionUtils.isEmpty(rates)) {
            return null;
        }

        return rates.stream().filter(rt -> rt != null && rt.getRate() != null).map(this::mapOutRate).collect(Collectors.toList());
    }

    private RateOffer mapOutRate(final Rates rates) {
        RateOffer result = new RateOffer();
        result.setRateType(mapOutRateTye(rates.getRate().getRatetype()));
        result.setUnit(mapOutUnit(rates.getRate().getUnit()));
        return result;
    }

    private Unit mapOutUnit(final com.bbva.pzic.cards.dao.model.phiat021_1.Unit unit) {
        if (unit == null) {
            return null;
        }

        Unit result = new Unit();
        result.setId(unit.getId());
        result.setName(unit.getName());
        result.setPercentage(StringUtils.isEmpty(unit.getPercentage()) ? null : new BigDecimal(unit.getPercentage()));
        return result;
    }

    private RateTypeOffer mapOutRateTye(final Ratetype ratetype) {
        if (ratetype == null) {
            return null;
        }

        RateTypeOffer result = new RateTypeOffer();
        result.setId(ratetype.getId());
        return result;
    }

    private List<GrantedCredit> mapOutGrantedCredits(final List<Grantedcredits> grantedcredits) {
        if (CollectionUtils.isEmpty(grantedcredits)) {
            return null;
        }

        return grantedcredits.stream().filter(gc -> gc != null && gc.getGrantedcredit() != null).map(this::mapOutGrantedCredit).collect(Collectors.toList());
    }

    private GrantedCredit mapOutGrantedCredit(final Grantedcredits grantedcredits) {
        GrantedCredit result = new GrantedCredit();
        result.setAmount(StringUtils.isEmpty(grantedcredits.getGrantedcredit().getAmount()) ? null : new BigDecimal(grantedcredits.getGrantedcredit().getAmount()));
        result.setCurrency(grantedcredits.getGrantedcredit().getCurrency());
        return result;
    }

    private List<com.bbva.pzic.cards.facade.v0.dto.Image> mapOutImages(final List<Images> images) {
        if (CollectionUtils.isEmpty(images)) {
            return null;
        }

        return images.stream().filter(ig -> ig != null && ig.getImage() != null).map(this::mapOutImage).collect(Collectors.toList());
    }

    private com.bbva.pzic.cards.facade.v0.dto.Image mapOutImage(final Images images) {
        com.bbva.pzic.cards.facade.v0.dto.Image result = new com.bbva.pzic.cards.facade.v0.dto.Image();
        result.setId(images.getImage().getId());
        result.setName(images.getImage().getName());
        result.setUrl(images.getImage().getUrl());
        return result;
    }

    private CardType mapOutCardType(final Cardtype cardtype) {
        if (cardtype == null) {
            return null;
        }

        CardType result = new CardType();
        result.setId(cardtype.getId());
        result.setName(cardtype.getName());
        return result;
    }

    private com.bbva.pzic.cards.facade.v0.dto.Dictum mapOutDictum(final Dictum dictum) {
        if (dictum == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v0.dto.Dictum result = new com.bbva.pzic.cards.facade.v0.dto.Dictum();
        result.setId(dictum.getId());
        result.setReasons(mapOutReasons(dictum.getReasons()));
        return result;
    }

    private List<com.bbva.pzic.cards.facade.v0.dto.Reason> mapOutReasons(final List<Reasons> reasons) {
        if (CollectionUtils.isEmpty(reasons)) {
            return null;
        }

        return reasons.stream().filter(rs -> rs != null && rs.getReason() != null).map(this::mapOutReason).collect(Collectors.toList());
    }

    private com.bbva.pzic.cards.facade.v0.dto.Reason mapOutReason(final Reasons reasons) {
        com.bbva.pzic.cards.facade.v0.dto.Reason result = new com.bbva.pzic.cards.facade.v0.dto.Reason();
        result.setId(reasons.getReason().getId());
        result.setDescription(reasons.getReason().getDescription());
        return result;
    }
}

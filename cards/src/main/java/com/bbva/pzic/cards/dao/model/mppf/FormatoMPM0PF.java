package com.bbva.pzic.cards.dao.model.mppf;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPM0PF</code> de la transacci&oacute;n <code>MPPF</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPM0PF")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPM0PF {

	/**
	 * <p>Campo <code>NTARJLF</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "NTARJLF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String ntarjlf;

	/**
	 * <p>Campo <code>IDPREMI</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "IDPREMI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idpremi;

}
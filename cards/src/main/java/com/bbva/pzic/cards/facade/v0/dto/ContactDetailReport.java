package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "ContactDetailReport", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "ContactDetailReport", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactDetailReport implements Serializable {

    private static final long serialVersionUID = 1L;

    private SpecificContactDetail contact;

    public SpecificContactDetail getContact() {
        return contact;
    }

    public void setContact(SpecificContactDetail contact) {
        this.contact = contact;
    }
}
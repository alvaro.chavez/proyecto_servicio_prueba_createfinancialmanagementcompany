package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "SecurityData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "SecurityData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class SecurityData implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Security code card identifier.
     */
    private String id;
    /**
     * Security code card description.
     */
    private String name;
    /**
     * Security code value. This value will be masked when basic authentication
     * state. If the authentication state is advanced the value will be clear.
     */
    @DatoAuditable(omitir = true)
    private String code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
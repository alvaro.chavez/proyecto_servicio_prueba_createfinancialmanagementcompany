package com.bbva.pzic.cards.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

/**
 * Created on 23/07/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "maskedToken", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "maskedToken", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class MaskedToken implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Token unique identifier.
     */
    private String id;
    /**
     * String based on ISO-8601 for specifying the date when the card will expire.
     * For normal plastic cards, the embossed date is usually this expiration date
     * without the day (YYYY-MM). It is used as part of the validation process.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date expirationDate;
    /**
     * Masked token generated for MasterCard cards, so they can operate like a Visa Card.
     */
    private String token;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

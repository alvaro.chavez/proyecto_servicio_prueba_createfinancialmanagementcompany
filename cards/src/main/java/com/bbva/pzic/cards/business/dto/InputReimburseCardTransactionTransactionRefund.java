package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class InputReimburseCardTransactionTransactionRefund {

    @NotNull(groups = ValidationGroup.ReimburseCardTransactionTransactionRefund.class)
    @Size(max = 19, groups = ValidationGroup.ReimburseCardTransactionTransactionRefund.class)
    private String cardId;

    @NotNull(groups = ValidationGroup.ReimburseCardTransactionTransactionRefund.class)
    @Size(max = 20, groups = ValidationGroup.ReimburseCardTransactionTransactionRefund.class)
    private String transactionId;

    @Valid
    private InputRefundType refundType;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public InputRefundType getRefundType() {
        return refundType;
    }

    public void setRefundType(InputRefundType refundType) {
        this.refundType = refundType;
    }
}

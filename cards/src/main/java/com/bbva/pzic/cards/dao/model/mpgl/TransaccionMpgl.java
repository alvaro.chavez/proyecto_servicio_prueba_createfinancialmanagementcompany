package com.bbva.pzic.cards.dao.model.mpgl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPGL</code>
 * 
 * @see PeticionTransaccionMpgl
 * @see RespuestaTransaccionMpgl
 */
@Component
public class TransaccionMpgl implements InvocadorTransaccion<PeticionTransaccionMpgl,RespuestaTransaccionMpgl> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMpgl invocar(PeticionTransaccionMpgl transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpgl.class, RespuestaTransaccionMpgl.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMpgl invocarCache(PeticionTransaccionMpgl transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpgl.class, RespuestaTransaccionMpgl.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Size;

/**
 * Created on 22/12/2017.
 *
 * @author Entelgy
 */
public class InputGetCardTransaction {
    @Size(max = 20, groups = ValidationGroup.GetCardTransactionV0.class)
    private String cardId;
    @Size(max = 20, groups = ValidationGroup.GetCardTransactionV0.class)
    private String transactionId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
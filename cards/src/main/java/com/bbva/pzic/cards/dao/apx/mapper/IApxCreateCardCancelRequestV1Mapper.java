package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCardCancelRequest;
import com.bbva.pzic.cards.dao.model.pcpst007_1.PeticionTransaccionPcpst007_1;
import com.bbva.pzic.cards.dao.model.pcpst007_1.RespuestaTransaccionPcpst007_1;
import com.bbva.pzic.cards.facade.v1.dto.RequestCancel;

public interface IApxCreateCardCancelRequestV1Mapper {

    PeticionTransaccionPcpst007_1 mapIn(InputCreateCardCancelRequest input);

    RequestCancel mapOut(RespuestaTransaccionPcpst007_1 response);

}

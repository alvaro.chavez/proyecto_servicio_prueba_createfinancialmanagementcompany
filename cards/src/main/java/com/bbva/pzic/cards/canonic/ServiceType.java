package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 03/10/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "ServiceType", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "ServiceType", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Delivery service type identifier.
     */
    private String id;
    /**
     * Delivery service type description.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

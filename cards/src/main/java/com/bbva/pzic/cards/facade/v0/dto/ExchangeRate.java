package com.bbva.pzic.cards.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@XmlRootElement(name = "ExchangeRate", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "ExchangeRate", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExchangeRate implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name="dateTime")
    private Calendar date;
    private Values values;

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public Values getValues() {
        return values;
    }

    public void setValues(Values values) {
        this.values = values;
    }
}

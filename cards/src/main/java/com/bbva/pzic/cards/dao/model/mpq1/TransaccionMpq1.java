package com.bbva.pzic.cards.dao.model.mpq1;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MPQ1</code>
 *
 * @see PeticionTransaccionMpq1
 * @see RespuestaTransaccionMpq1
 */
@Component
public class TransaccionMpq1 implements InvocadorTransaccion<PeticionTransaccionMpq1, RespuestaTransaccionMpq1> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionMpq1 invocar(PeticionTransaccionMpq1 transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMpq1.class, RespuestaTransaccionMpq1.class, transaccion);
    }

    @Override
    public RespuestaTransaccionMpq1 invocarCache(PeticionTransaccionMpq1 transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionMpq1.class, RespuestaTransaccionMpq1.class, transaccion);
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Balances", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Balances", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Balances implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Available balance associated to all the credit and prepaid cards of the
     * user. This attribute provides the card aggregated balance.
     */
    private AggregatedAvailableBalance aggregatedAvailableBalance;
    /**
     * This balance is the spent amount from granted credit. This attribute is
     * mandatory for credit cards.
     */
    private AggregatedDisposedBalance aggregatedDisposedBalance;

    public AggregatedAvailableBalance getAggregatedAvailableBalance() {
        return aggregatedAvailableBalance;
    }

    public void setAggregatedAvailableBalance(
            AggregatedAvailableBalance aggregatedAvailableBalance) {
        this.aggregatedAvailableBalance = aggregatedAvailableBalance;
    }

    public AggregatedDisposedBalance getAggregatedDisposedBalance() {
        return aggregatedDisposedBalance;
    }

    public void setAggregatedDisposedBalance(
            AggregatedDisposedBalance aggregatedDisposedBalance) {
        this.aggregatedDisposedBalance = aggregatedDisposedBalance;
    }
}
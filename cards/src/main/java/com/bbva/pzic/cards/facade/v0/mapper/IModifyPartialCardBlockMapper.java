package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.canonic.Block;

/**
 * Created on 29/08/2017.
 *
 * @author Entelgy
 */
public interface IModifyPartialCardBlockMapper {

    /**
     * Method that creates a DTO with the input data
     *
     * @param cardId  unique card identifier
     * @param blockId unique block identifier
     * @param block   payload
     * @return Object with the data of input
     */
    DTOIntBlock mapIn(String cardId, String blockId, Block block);
}

package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOInputCreateCashRefund;
import com.bbva.pzic.cards.business.dto.DTOIntCashRefund;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCEMDC0;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCRMDC0;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCRMDC1;

public interface ITxCreateCardCashRefundMapper {

    FormatoMCEMDC0 mapInput(DTOInputCreateCashRefund dtoInt);

    DTOIntCashRefund mapOutput(FormatoMCRMDC0 formatoMCRMDC0, DTOIntCashRefund dtoOut);

    DTOIntCashRefund mapOutput(FormatoMCRMDC1 formatoMCRMDC1, DTOIntCashRefund dtoOut);

}

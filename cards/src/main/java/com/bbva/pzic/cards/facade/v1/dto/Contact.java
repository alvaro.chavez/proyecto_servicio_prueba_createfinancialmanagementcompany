package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "contact", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "contact", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact information identifier.
     */
    private String id;
    /**
     * Contact type.
     */
    private String contactType;
    /**
     * This object represents the specific information of the contact depending on its type.
     */
    private SpecificContact contact;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public SpecificContact getContact() {
        return contact;
    }

    public void setContact(SpecificContact contact) {
        this.contact = contact;
    }
}

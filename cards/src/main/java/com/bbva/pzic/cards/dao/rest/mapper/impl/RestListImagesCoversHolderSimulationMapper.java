package com.bbva.pzic.cards.dao.rest.mapper.impl;

import com.bbva.pzic.cards.canonic.HolderSimulation;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesRequest;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesResponse;
import com.bbva.pzic.cards.dao.model.awsimages.ModelCard;
import com.bbva.pzic.cards.dao.model.awsimages.ModelImage;
import com.bbva.pzic.cards.dao.rest.ListImagesCoversUtil;
import com.bbva.pzic.cards.dao.rest.mapper.IRestListImagesCoversHolderSimulationMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 20/02/2020.
 *
 * @author Entelgy
 */
@Mapper
public class RestListImagesCoversHolderSimulationMapper implements IRestListImagesCoversHolderSimulationMapper {

    private static final Log LOG = LogFactory.getLog(RestListImagesCoversHolderSimulationMapper.class);

    @Override
    public AWSImagesRequest mapIn(HolderSimulation holderSimulation) {
        LOG.info("... called method RestListImagesCoversHolderSimulationMapper.mapIn ...");
        if (holderSimulation == null || holderSimulation.getDetails() == null ||
                holderSimulation.getDetails().getImage() == null || holderSimulation.getDetails().getImage().getUrl() == null) {
            return null;
        }

        AWSImagesRequest input = new AWSImagesRequest();
        String url = holderSimulation.getDetails().getImage().getUrl();
        ModelCard modelCard = ListImagesCoversUtil.getValorDTOUrl(url);
        ListImagesCoversUtil.getValorDTOUrlData0(input, url);

        List<ModelCard> modelCards = new ArrayList<>();
        input.setCards(modelCards);
        modelCards.add(modelCard);

        return input;
    }

    @Override
    public void mapOut(AWSImagesResponse response, HolderSimulation holderSimulation) {
        LOG.info("... called method RestListImagesCoversHolderSimulationMapper.mapOut ...");

        if (response != null && holderSimulation != null && holderSimulation.getDetails() != null &&
                holderSimulation.getDetails().getImage() != null && holderSimulation.getDetails().getImage().getUrl() != null) {

            List<ModelImage> modelImages = response.getData().get(0);
            for (ModelImage modelImage : modelImages) {
                if ("url".equals(modelImage.getKey())) {
                    String imagenValue = modelImage.getValue();
                    LOG.debug(String.format("Updating with value %s", imagenValue));
                    holderSimulation.getDetails().getImage().setUrl(imagenValue);
                    break;
                }
            }
        }
    }
}

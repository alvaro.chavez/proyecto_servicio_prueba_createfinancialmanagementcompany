package com.bbva.pzic.cards.dao.model.productofferdetail;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;

import java.util.List;

public class CardHolderSimulationResponse {

    private CardHolderSimulation data;
    private List<Message> messages;

    public CardHolderSimulation getData() {
        return data;
    }

    public void setData(CardHolderSimulation data) {
        this.data = data;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}

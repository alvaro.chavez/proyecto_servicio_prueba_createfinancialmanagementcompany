package com.bbva.pzic.cards.dao.model.ppcut003_1;

import java.util.Calendar;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>scheduleTimes</code>, utilizado por la clase <code>Contactability</code></p>
 * 
 * @see Contactability
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Scheduletimes {
	
	/**
	 * <p>Campo <code>startTime</code>, &iacute;ndice: <code>1</code>, tipo: <code>TIMESTAMP</code>
	 */
	@Campo(indice = 1, nombre = "startTime", tipo = TipoCampo.TIMESTAMP, signo = true, formato = "yyyy-MM-dd'T'HH:mm:ss.SSSX", obligatorio = true)
	private Calendar starttime;
	
	/**
	 * <p>Campo <code>endTime</code>, &iacute;ndice: <code>2</code>, tipo: <code>TIMESTAMP</code>
	 */
	@Campo(indice = 2, nombre = "endTime", tipo = TipoCampo.TIMESTAMP, signo = true, formato = "yyyy-MM-dd'T'HH:mm:ss.SSSX", obligatorio = true)
	private Calendar endtime;
	
}
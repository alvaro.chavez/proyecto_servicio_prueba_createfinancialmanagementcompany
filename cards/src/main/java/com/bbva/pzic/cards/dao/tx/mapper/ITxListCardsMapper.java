package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMENL1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS1L1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS2L1;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
public interface ITxListCardsMapper {
    /**
     * @param dtoIn
     * @return
     */
    FormatoMPMENL1 mapIn(DTOIntListCards dtoIn);

    /**
     * Formato correspondiente a los datos de tarjeta
     *
     * @param formatOutput
     * @param dtoIn
     * @param dtoOut
     * @return
     */
    DTOOutListCards mapOut(FormatoMPMS1L1 formatOutput, DTOIntListCards dtoIn, DTOOutListCards dtoOut);

    /**
     * Formato correspondiente a la paginacion
     *
     * @param formatOutput
     * @param dtoIn
     * @param dtoOut
     * @return
     */
    DTOOutListCards mapOut2(FormatoMPMS2L1 formatOutput, DTOIntListCards dtoIn, DTOOutListCards dtoOut);
}

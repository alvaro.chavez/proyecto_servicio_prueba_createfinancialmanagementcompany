package com.bbva.pzic.cards.business.dto;

import java.util.List;

public class DTOIntTaxes {

    private DTOIntTotalTaxes totalTaxes;
    private List<DTOIntItemizeTaxes> itemizeTaxes;

    public DTOIntTotalTaxes getTotalTaxes() {
        return totalTaxes;
    }

    public void setTotalTaxes(DTOIntTotalTaxes totalTaxes) {
        this.totalTaxes = totalTaxes;
    }

    public List<DTOIntItemizeTaxes> getItemizeTaxes() {
        return itemizeTaxes;
    }

    public void setItemizeTaxes(List<DTOIntItemizeTaxes> itemizeTaxes) {
        this.itemizeTaxes = itemizeTaxes;
    }
}

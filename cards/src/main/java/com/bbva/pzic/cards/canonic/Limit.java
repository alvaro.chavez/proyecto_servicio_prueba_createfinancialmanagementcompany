package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Limit", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Limit", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Limit implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Limit identifier.
     */
    private String id;
    /**
     * Limit description.
     */
    private String name;
    /**
     * Monetary restriction related to the operation.
     */
    private AmountLimit amountLimit;
    /**
     * Limit identifier.
     */
    private String limitId;
    /**
     * Monetary restriction related to the current limit. This amount may be
     * provided in several currencies (depending on the country).
     */
    private List<AmountLimit> amountLimits;
    /**
     * String based on ISO-8601 date format for specifying the operation date.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name="dateTime")
    private Calendar operationDate;
    /**
     * There is no additional information.
     */
    private String offerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AmountLimit getAmountLimit() {
        return amountLimit;
    }

    public void setAmountLimit(AmountLimit amountLimit) {
        this.amountLimit = amountLimit;
    }

    public String getLimitId() {
        return limitId;
    }

    public void setLimitId(String limitId) {
        this.limitId = limitId;
    }

    public List<AmountLimit> getAmountLimits() {
        return amountLimits;
    }

    public void setAmountLimits(List<AmountLimit> amountLimits) {
        this.amountLimits = amountLimits;
    }

    public Calendar getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Calendar operationDate) {
        this.operationDate = operationDate;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
package com.bbva.pzic.cards.dao.apx.mapper;

import com.bbva.pzic.cards.business.dto.InputListCardProposals;
import com.bbva.pzic.cards.dao.model.ppcut004_1.PeticionTransaccionPpcut004_1;
import com.bbva.pzic.cards.dao.model.ppcut004_1.RespuestaTransaccionPpcut004_1;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;

import java.util.List;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
public interface IApxListCardProposalsMapper {

    PeticionTransaccionPpcut004_1 mapIn(InputListCardProposals input);

    List<Proposal> mapOut(RespuestaTransaccionPpcut004_1 response);
}

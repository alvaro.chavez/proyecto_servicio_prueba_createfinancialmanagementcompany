package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardTransaction;
import com.bbva.pzic.cards.canonic.Transaction;
import com.bbva.pzic.cards.canonic.TransactionData;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardTransactionMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.bbva.pzic.cards.facade.RegistryIds;

/**
 * Created on 22/12/2017.
 *
 * @author Entelgy
 */
@Mapper("getCardTransactionMapper")
public class GetCardTransactionMapper implements IGetCardTransactionMapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public InputGetCardTransaction mapIn(final String cardId, final String transactionId) {
        InputGetCardTransaction input = new InputGetCardTransaction();
        input.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_TRANSACTION));
        input.setTransactionId(transactionId);
        return input;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<Transaction> mapOut(final TransactionData transactionData) {
        if (transactionData == null || transactionData.getData() == null) {
            return null;
        }

        return ServiceResponse
                .data(transactionData.getData())
                .messages(null)
                .build();
    }
}

package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateCardMapper;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Address;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Branch;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Contact;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Currency;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Delivery;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Destination;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Frecuency;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Image;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Membership;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Participant;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Product;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Status;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.*;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.BusinessServiceUtil;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.bbva.pzic.cards.util.Constants.*;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ApxCreateCardMapper implements IApxCreateCardMapper {

    private static final Log LOG = LogFactory.getLog(ApxCreateCardMapper.class);

    @Override
    public Entityin mapIn(final DTOIntCard dtoInt) {
        LOG.info("... called method ApxCreateCardMapper.mapIn ...");
        Entityin entityIn = new Entityin();
        entityIn.setCardtype(dtoIntCardTypeIdToCardType(dtoInt.getCardTypeId()));
        entityIn.setProduct(dtoIntProductIdToProduct(dtoInt.getProductId()));
        entityIn.setPhysicalsupport(dtoIntPhysicalSupportIdToPhysicalSupport(dtoInt.getPhysicalSupportId()));
        entityIn.setHoldername(dtoInt.getHolderName());
        entityIn.setCurrencies(dtoIntCurrenciesToCurrencies(dtoInt.getCurrenciesCurrency(), dtoInt.getCurrenciesIsMajor()));
        entityIn.setGrantedcredits(dtoIntGrantedCreditsToGrantedCredits(dtoInt.getGrantedCreditsAmount(), dtoInt.getGrantedCreditsCurrency()));
        entityIn.setCutoffday(dtoInt.getCutOffDay() == null ? null : dtoInt.getCutOffDay().toString());
        entityIn.setRelatedcontracts(dtoIntRelatedContractListToRelatedContractList(dtoInt.getRelatedContracts()));
        entityIn.setImages(dtoIntImagesToImages(dtoInt.getImages()));
        entityIn.setDeliveries(dtoIntDeliveryListToDeliveryList(dtoInt.getDeliveries()));
        entityIn.setParticipants(dtoIntParticipantsToParticipants(dtoInt.getParticipants()));
        entityIn.setPaymentmethod(dtoIntPaymentMethodToPaymentMethod(dtoInt.getPaymentMethod()));
        entityIn.setSupportcontracttype(dtoInt.getSupportContractType());
        entityIn.setMemberships(dtoIntMembershipsToMemberships(dtoInt.getMembershipsNumber()));
        entityIn.setCardagreement(dtoInt.getCardAgreement());
        entityIn.setContractingbranch(dtoIntContractingBranchIdToContractingBranch(dtoInt.getContractingBranchId()));
        entityIn.setManagementbranch(dtoIntManagementBranchIdToManagementBranch(dtoInt.getManagementBranchId()));
        entityIn.setContractingbusinessagent(dtoIntContractingBusinessAgentIdToContractingBusinessAgent(dtoInt.getContractingBusinessAgentId()));
        entityIn.setMarketbusinessagent(dtoIntMarketBusinessAgentIdToMarketBusinessAgent(dtoInt.getMarketBusinessAgentId()));
        entityIn.setBankidentificationnumber(dtoInt.getBankIdentificationNumber());

        return entityIn;
    }

    private Cardtype dtoIntCardTypeIdToCardType(String cardTypeId) {
        if (cardTypeId == null) {
            return null;
        }

        Cardtype cardtype = new Cardtype();
        cardtype.setId(cardTypeId);
        return cardtype;
    }

    private Product dtoIntProductIdToProduct(String productId) {
        if (productId == null) {
            return null;
        }

        Product product = new Product();
        product.setId(productId);
        return product;
    }

    private Physicalsupport dtoIntPhysicalSupportIdToPhysicalSupport(String physicalSupportId) {
        if (physicalSupportId == null) {
            return null;
        }

        Physicalsupport physicalSupport = new Physicalsupport();
        physicalSupport.setId(physicalSupportId);
        return physicalSupport;
    }

    private List<Currencies> dtoIntCurrenciesToCurrencies(String currency, Boolean isMajor) {
        if (currency == null && isMajor == null) {
            return null;
        }
        Currency c = new Currency();
        c.setCurrency(currency);
        c.setIsmajor(isMajor);

        Currencies currencies = new Currencies();
        currencies.setCurrency(c);

        return Collections.singletonList(currencies);
    }

    private List<Grantedcredits> dtoIntGrantedCreditsToGrantedCredits(BigDecimal amount, String currency) {
        if (amount == null && currency == null) {
            return null;
        }
        Grantedcredit gc = new Grantedcredit();
        gc.setAmount(amount);
        gc.setCurrency(currency);

        Grantedcredits grantedCredits = new Grantedcredits();
        grantedCredits.setGrantedcredit(gc);

        return Collections.singletonList(grantedCredits);
    }

    private Relationtype dtoIntRelationTypeToRelationType(DTOIntRelationType dtoIntRelationType) {
        if (dtoIntRelationType == null) {
            return null;
        }

        Relationtype relationType = new Relationtype();

        relationType.setId(dtoIntRelationType.getId());

        return relationType;
    }

    private Producttype dtoIntProductTypeToProductType(DTOIntProductType dtoIntProductType) {
        if (dtoIntProductType == null) {
            return null;
        }

        Producttype productType = new Producttype();

        productType.setId(dtoIntProductType.getId());

        return productType;
    }

    private Product dtoIntProductToProduct(DTOIntProduct dtoIntProduct) {
        if (dtoIntProduct == null) {
            return null;
        }

        Product product = new Product();

        product.setId(dtoIntProduct.getId());
        product.setProducttype(dtoIntProductTypeToProductType(dtoIntProduct.getProductType()));

        return product;
    }

    private Relatedcontracts dtoIntRelatedContractToRelatedContract(DTOIntRelatedContract dtoIntRelatedContract) {
        if (dtoIntRelatedContract == null) {
            return null;
        }

        Relatedcontract relatedContract = new Relatedcontract();

        relatedContract.setContractid(dtoIntRelatedContract.getContractId());
        relatedContract.setRelationtype(dtoIntRelationTypeToRelationType(dtoIntRelatedContract.getRelationType()));
        relatedContract.setProduct(dtoIntProductToProduct(dtoIntRelatedContract.getProduct()));

        Relatedcontracts relatedcontracts = new Relatedcontracts();
        relatedcontracts.setRelatedcontract(relatedContract);
        return relatedcontracts;
    }

    private List<Relatedcontracts> dtoIntRelatedContractListToRelatedContractList(List<DTOIntRelatedContract> list) {
        if (list == null) {
            return null;
        }

        List<Relatedcontracts> list1 = new ArrayList<>(list.size());
        for (DTOIntRelatedContract dtoIntRelatedContract : list) {
            list1.add(dtoIntRelatedContractToRelatedContract(dtoIntRelatedContract));
        }

        return list1;
    }

    private List<Images> dtoIntImagesToImages(final List<DTOIntImage> dtoInt) {
        if (CollectionUtils.isEmpty(dtoInt)) {
            return null;
        }
        return dtoInt.stream().map(this::dtoIntImageToImages).collect(Collectors.toList());
    }

    private Images dtoIntImageToImages(final DTOIntImage dtoInt) {
        if (dtoInt == null) {
            return null;
        }

        Image image = new Image();
        image.setId(dtoInt.getId());

        Images images = new Images();
        images.setImage(image);
        return images;
    }

    private Servicetype dtoIntServiceTypeIdToServiceType(String serviceTypeId) {
        if (serviceTypeId == null) {
            return null;
        }

        Servicetype serviceType = new Servicetype();
        serviceType.setId(serviceTypeId);
        return serviceType;
    }

    private Contact dtoIntContactToContact(DTOIntContact dtoIntContact) {
        if (dtoIntContact == null) {
            return null;
        }

        Contact contact = new Contact();
        contact.setContacttype(dtoIntContact.getContactType());
        contact.setId(dtoIntContact.getId());
        return contact;
    }

    private Destination dtoIntDestinationToDestination(DTOIntDestination dtoIntDestination) {
        if (dtoIntDestination == null) {
            return null;
        }

        Destination destination = new Destination();
        destination.setId(dtoIntDestination.getId());

        Branch branch;
        if (dtoIntDestination.getBranch() == null) {
            branch = null;
        } else {
            branch = new Branch();
            branch.setId(dtoIntDestination.getBranch().getId());
        }
        destination.setBranch(branch);
        return destination;
    }

    private Address dtoIntAddressToAddress(DTOIntAddress dtoIntAddress) {
        if (dtoIntAddress == null) {
            return null;
        }

        Address address = new Address();
        address.setAddresstype(dtoIntAddress.getAddressType());
        address.setId(dtoIntAddress.getId());
        return address;
    }

    private Deliveries dtoIntDeliveryToDelivery(DTOIntDelivery dtoIntDelivery) {
        if (dtoIntDelivery == null) {
            return null;
        }

        Delivery delivery = new Delivery();
        delivery.setServicetype(dtoIntServiceTypeIdToServiceType(dtoIntDelivery.getServiceTypeId()));
        delivery.setContact(dtoIntContactToContact(dtoIntDelivery.getContact()));
        delivery.setDestination(dtoIntDestinationToDestination(dtoIntDelivery.getDestination()));
        delivery.setAddress(dtoIntAddressToAddress(dtoIntDelivery.getAddress()));

        Deliveries deliveries = new Deliveries();
        deliveries.setDelivery(delivery);
        return deliveries;
    }

    private List<Deliveries> dtoIntDeliveryListToDeliveryList(List<DTOIntDelivery> list) {
        if (list == null) {
            return null;
        }

        List<Deliveries> list1 = new ArrayList<>(list.size());
        for (DTOIntDelivery dtoIntDelivery : list) {
            list1.add(dtoIntDeliveryToDelivery(dtoIntDelivery));
        }

        return list1;
    }

    private Participanttype dtoIntParticipantTypeIdToParticipantType(String participantTypeId) {
        if (participantTypeId == null) {
            return null;
        }

        Participanttype participantType = new Participanttype();
        participantType.setId(participantTypeId);
        return participantType;
    }

    private Legalpersontype dtoIntLegalPersonTypeIdToLegalPersonType(String legalPersonTypeId) {
        if (legalPersonTypeId == null) {
            return null;
        }

        Legalpersontype legalPersonType = new Legalpersontype();
        legalPersonType.setId(legalPersonTypeId);
        return legalPersonType;
    }

    private Participants dtoIntParticipantToParticipant(DTOIntParticipant dtoIntParticipant) {
        if (dtoIntParticipant == null) {
            return null;
        }

        Participant participant = new Participant();
        participant.setPersontype(dtoIntParticipant.getPersonType());
        participant.setId(dtoIntParticipant.getId());
        participant.setParticipanttype(dtoIntParticipantTypeIdToParticipantType(dtoIntParticipant.getParticipantTypeId()));
        participant.setLegalpersontype(dtoIntLegalPersonTypeIdToLegalPersonType(dtoIntParticipant.getLegalPersonTypeId()));

        Participants participants = new Participants();
        participants.setParticipant(participant);
        return participants;
    }

    private List<Participants> dtoIntParticipantsToParticipants(List<DTOIntParticipant> dtoIntParticipants) {
        if (CollectionUtils.isEmpty(dtoIntParticipants)) {
            return null;
        }

        List<Participants> list1 = new ArrayList<>(dtoIntParticipants.size());
        for (DTOIntParticipant dtoIntParticipant : dtoIntParticipants) {
            list1.add(dtoIntParticipantToParticipant(dtoIntParticipant));
        }

        return list1;
    }

    private Frecuency dtoIntFrecuencyToFrecuency(DTOIntFrequency dtoIntFrequency) {
        if (dtoIntFrequency == null) {
            return null;
        }

        Daysofmonth dayOfMonth;
        if (dtoIntFrequency.getDaysOfMonth() == null) {
            dayOfMonth = null;
        } else {
            dayOfMonth = new Daysofmonth();
            dayOfMonth.setDay(dtoIntFrequency.getDaysOfMonth().getDay());
        }

        Frecuency frecuency = new Frecuency();
        frecuency.setId(dtoIntFrequency.getId());
        frecuency.setDaysofmonth(dayOfMonth);
        return frecuency;
    }

    private Paymentmethod dtoIntPaymentMethodToPaymentMethod(DTOIntPaymentMethod dtoIntPaymentMethod) {
        if (dtoIntPaymentMethod == null) {
            return null;
        }

        Paymentmethod paymentMethod = new Paymentmethod();
        paymentMethod.setId(dtoIntPaymentMethod.getId());
        paymentMethod.setFrecuency(dtoIntFrecuencyToFrecuency(dtoIntPaymentMethod.getFrequency()));

        return paymentMethod;
    }

    private List<Memberships> dtoIntMembershipsToMemberships(String membershipsNumber) {
        if (membershipsNumber == null) {
            return null;
        }

        Membership m = new Membership();
        m.setNumber(membershipsNumber);

        Memberships memberships = new Memberships();
        memberships.setMembership(m);

        return Collections.singletonList(memberships);
    }

    private Contractingbranch dtoIntContractingBranchIdToContractingBranch(String contractingBranchId) {
        if (contractingBranchId == null) {
            return null;
        }

        Contractingbranch cb = new Contractingbranch();
        cb.setId(contractingBranchId);
        return cb;
    }

    private Managementbranch dtoIntManagementBranchIdToManagementBranch(String managementBranchId) {
        if (managementBranchId == null) {
            return null;
        }

        Managementbranch mb = new Managementbranch();
        mb.setId(managementBranchId);
        return mb;
    }

    private Contractingbusinessagent dtoIntContractingBusinessAgentIdToContractingBusinessAgent(String contractingBusinessAgentId) {
        if (contractingBusinessAgentId == null) {
            return null;
        }

        Contractingbusinessagent cba = new Contractingbusinessagent();
        cba.setId(contractingBusinessAgentId);
        return cba;
    }

    private Marketbusinessagent dtoIntMarketBusinessAgentIdToMarketBusinessAgent(String marketBusinessAgentId) {
        if (marketBusinessAgentId == null) {
            return null;
        }

        Marketbusinessagent mba = new Marketbusinessagent();
        mba.setId(marketBusinessAgentId);
        return mba;
    }

    @Override
    public CardPost mapOut(final Entityout entityOut) {
        LOG.info("... called method ApxCreateCardMapper.mapOut ...");
        if (entityOut == null) {
            return null;
        }

        CardPost card = new CardPost();
        card.setId(entityOut.getId());
        card.setNumber(entityOut.getNumber());
        card.setNumberType(modelNumberTypeToNumberType(entityOut.getNumbertype()));
        card.setCardType(modelCardTypeToCardType(entityOut.getCardtype()));
        card.setProduct(modelProductToProduct(entityOut.getProduct()));
        card.setBrandAssociation(modelBrandAssociationToBrandAssociation(entityOut.getBrandassociation()));
        card.setPhysicalSupport(modelPhysicalSupportToPhysicalSupport(entityOut.getPhysicalsupport()));
        card.setExpirationDate(FunctionUtils.buildDate(entityOut.getExpirationdate()));
        card.setHolderName(entityOut.getHoldername());
        card.setCurrencies(modelCurrenciesToCurrencies(entityOut.getCurrencies()));
        card.setGrantedCredits(modelGrantedCreditsToGrantedCredits(entityOut.getGrantedcredits()));
        card.setStatus(modelStatusToStatus(entityOut.getStatus()));
        card.setCutOffDay(entityOut.getCutoffday() == null ? null : Integer.parseInt(entityOut.getCutoffday()));
        card.setOpeningDate(FunctionUtils.buildDatetime(entityOut.getOpeningdate()));
        card.setRelatedContracts(modelRelatedContractsToRelatedContracts(entityOut.getRelatedcontracts()));
        card.setDeliveries(modelDeliveriesToDeliveries(entityOut.getDeliveries(), entityOut.getCardagreement()));
        card.setSupportContractType(entityOut.getSupportcontracttype());
        card.setMemberships(modelMembershipsToMemberships(entityOut.getMemberships()));
        card.setCardAgreement(entityOut.getCardagreement());
        card.setContractingBranch(modelContractingBranchToContractingBranch(entityOut.getContractingbranch()));
        card.setManagementBranch(modelManagementBranchToManagementBranch(entityOut.getManagementbranch()));
        card.setContractingBusinessAgent(modelContractingBusinessAgentToContractingBusinessAgent(entityOut.getContractingbusinessagent()));
        card.setMarketBusinessAgent(modelMarketBusinessAgentToMarketBusinessAgent(entityOut.getMarketbusinessagent()));
        card.setBankIdentificationNumber(entityOut.getBankidentificationnumber());
        return card;
    }

    private NumberType modelNumberTypeToNumberType(Numbertype model) {
        if (model == null) {
            return null;
        }
        NumberType numberType = new NumberType();
        numberType.setId(model.getId());
        numberType.setDescription(model.getDescription());
        return numberType;
    }

    private CardTypePost modelCardTypeToCardType(Cardtype model) {
        if (model == null) {
            return null;
        }
        CardTypePost cardType = new CardTypePost();
        cardType.setId(model.getId());
        cardType.setDescription(model.getDescription());
        return cardType;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Product modelProductToProduct(Product model) {
        if (model == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Product product = new com.bbva.pzic.cards.facade.v1.dto.Product();
        product.setId(model.getId());
        product.setName(model.getName());
        product.setProductType(modelProductTypeToProductType(model.getProducttype()));
        return product;
    }

    private ProductType modelProductTypeToProductType(Producttype model) {
        if (model == null) {
            return null;
        }
        ProductType productType = new ProductType();
        productType.setId(model.getId());
        return productType;
    }

    private BrandAssociation modelBrandAssociationToBrandAssociation(Brandassociation model) {
        if (model == null) {
            return null;
        }
        BrandAssociation brandAssociation = new BrandAssociation();
        brandAssociation.setId(model.getId());
        brandAssociation.setDescription(model.getDescription());
        return brandAssociation;
    }

    private PhysicalSupportPost modelPhysicalSupportToPhysicalSupport(Physicalsupport model) {
        if (model == null) {
            return null;
        }
        PhysicalSupportPost physicalSupport = new PhysicalSupportPost();
        physicalSupport.setId(model.getId());
        physicalSupport.setDescription(model.getDescription());
        return physicalSupport;
    }

    private List<com.bbva.pzic.cards.facade.v1.dto.Currency> modelCurrenciesToCurrencies(List<Currencies> model) {
        if (CollectionUtils.isEmpty(model)) {
            return null;
        }
        return model.stream()
                .map(c -> modelCurrencyToCurrency(c.getCurrency()))
                .collect(Collectors.toList());
    }

    private com.bbva.pzic.cards.facade.v1.dto.Currency modelCurrencyToCurrency(final Currency model) {
        if (model == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Currency currency = new com.bbva.pzic.cards.facade.v1.dto.Currency();
        currency.setCurrency(model.getCurrency());
        currency.setIsMajor(model.getIsmajor());
        return currency;
    }

    private List<Amount> modelGrantedCreditsToGrantedCredits(List<Grantedcredits> model) {
        if (CollectionUtils.isEmpty(model)) {
            return null;
        }
        return model.stream()
                .map(gc -> modelGrantedCreditToGrantedCredit(gc.getGrantedcredit()))
                .collect(Collectors.toList());
    }

    private Amount modelGrantedCreditToGrantedCredit(Grantedcredit model) {
        if (model == null) {
            return null;
        }
        Amount amount = new Amount();
        amount.setAmount(model.getAmount());
        amount.setCurrency(model.getCurrency());
        return amount;
    }

    private StatusPost modelStatusToStatus(Status model) {
        if (model == null) {
            return null;
        }
        StatusPost status = new StatusPost();
        status.setId(model.getId());
        status.setDescription(model.getDescription());
        return status;
    }

    private List<RelatedContractPost> modelRelatedContractsToRelatedContracts(List<Relatedcontracts> model) {
        if (CollectionUtils.isEmpty(model)) {
            return null;
        }
        return model.stream()
                .map(rc -> modelRelatedContractToRelatedContract(rc.getRelatedcontract()))
                .collect(Collectors.toList());
    }

    private RelatedContractPost modelRelatedContractToRelatedContract(Relatedcontract model) {
        if (model == null) {
            return null;
        }
        RelatedContractPost relatedContract = new RelatedContractPost();
        relatedContract.setId(model.getId());
        relatedContract.setContractId(model.getContractid());
        relatedContract.setProduct(modelProductToProduct(model.getProduct()));
        relatedContract.setRelationType(modelRelationTypeToRelationType(model.getRelationtype()));
        return relatedContract;
    }

    private RelationType modelRelationTypeToRelationType(Relationtype model) {
        if (model == null) {
            return null;
        }
        RelationType relationType = new RelationType();
        relationType.setId(model.getId());
        return relationType;
    }

    private List<DeliveryPost> modelDeliveriesToDeliveries(List<Deliveries> model, String cardAgreement) {
        if (CollectionUtils.isEmpty(model)) {
            return null;
        }
        return model.stream()
                .map(d -> modelDeliveryToDelivery(d.getDelivery(), cardAgreement))
                .collect(Collectors.toList());
    }

    private DeliveryPost modelDeliveryToDelivery(Delivery model, String cardAgreement) {
        if (model == null) {
            return null;
        }
        DeliveryPost delivery = new DeliveryPost();
        delivery.setServiceType(modelServiceTypeToServiceType(model.getServicetype()));

        delivery.setContact(modelContactToContact(model.getContact()));
        if (delivery.getContact() != null) {
            delivery.setId(BusinessServiceUtil.buildDeliveryId(cardAgreement, delivery.getContact().getId()));
        }

        delivery.setDestination(modelDestinationToDestination(model.getDestination()));

        if (model.getDestination() != null) {
            if (HOME.equals(model.getDestination().getId()) ||
                    CUSTOM.equals(model.getDestination().getId())) {
                delivery.setAddress(modelAddressToAddress(model.getAddress()));

                if (STORED.equals(model.getAddress().getAddresstype())) {
                    delivery.setId(BusinessServiceUtil.buildDeliveryId(cardAgreement, model.getAddress().getId()));
                }
            } else if (BRANCH.equals(model.getDestination().getId()) && delivery.getDestination().getBranch() != null) {
                delivery.setId(BusinessServiceUtil.buildDeliveryId(cardAgreement, delivery.getDestination().getBranch().getId()));
            }
        }

        return delivery;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Contact modelContactToContact(Contact model) {
        if (model == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Contact contact = new com.bbva.pzic.cards.facade.v1.dto.Contact();
        contact.setContactType(model.getContacttype());
        if (STORED.equals(model.getContacttype())) {
            contact.setId(model.getId());
        }
        return contact;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Destination modelDestinationToDestination(Destination model) {
        if (model == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Destination destination = new com.bbva.pzic.cards.facade.v1.dto.Destination();
        destination.setId(model.getId());
        destination.setBranch(modeBranchToBranch(model.getBranch()));
        return destination;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Address modelAddressToAddress(Address model) {
        if (model == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Address address = new com.bbva.pzic.cards.facade.v1.dto.Address();
        address.setAddressType(model.getAddresstype());
        if (STORED.equals(model.getAddresstype())) {
            address.setId(model.getId());
        }
        return address;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Branch modeBranchToBranch(Branch model) {
        if (model == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Branch branch = new com.bbva.pzic.cards.facade.v1.dto.Branch();
        branch.setId(model.getId());
        return branch;
    }

    private ServiceTypePost modelServiceTypeToServiceType(Servicetype model) {
        if (model == null) {
            return null;
        }
        ServiceTypePost serviceType = new ServiceTypePost();
        serviceType.setId(model.getId());
        return serviceType;
    }

    private List<com.bbva.pzic.cards.facade.v1.dto.Membership> modelMembershipsToMemberships(List<Memberships> model) {
        if (CollectionUtils.isEmpty(model)) {
            return null;
        }
        return model.stream()
                .map(m -> modelMembershipToMembership(m.getMembership()))
                .collect(Collectors.toList());
    }

    private com.bbva.pzic.cards.facade.v1.dto.Membership modelMembershipToMembership(Membership model) {
        if (model == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Membership membership = new com.bbva.pzic.cards.facade.v1.dto.Membership();
        membership.setId(model.getId());
        membership.setNumber(model.getNumber());
        return membership;
    }

    private AttentionBranch modelContractingBranchToContractingBranch(Contractingbranch model) {
        if (model == null) {
            return null;
        }
        AttentionBranch attentionBranch = new AttentionBranch();
        attentionBranch.setId(model.getId());
        return attentionBranch;
    }

    private AttentionBranch modelManagementBranchToManagementBranch(Managementbranch model) {
        if (model == null) {
            return null;
        }
        AttentionBranch attentionBranch = new AttentionBranch();
        attentionBranch.setId(model.getId());
        return attentionBranch;
    }

    private ContractingBusinessAgent modelContractingBusinessAgentToContractingBusinessAgent(Contractingbusinessagent model) {
        if (model == null) {
            return null;
        }
        ContractingBusinessAgent contractingBusinessAgent = new ContractingBusinessAgent();
        contractingBusinessAgent.setId(model.getId());
        return contractingBusinessAgent;
    }

    private MarketBusinessAgent modelMarketBusinessAgentToMarketBusinessAgent(Marketbusinessagent model) {
        if (model == null) {
            return null;
        }
        MarketBusinessAgent marketBusinessAgent = new MarketBusinessAgent();
        marketBusinessAgent.setId(model.getId());
        return marketBusinessAgent;
    }
}

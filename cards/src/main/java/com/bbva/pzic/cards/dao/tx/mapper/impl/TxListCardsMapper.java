package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOIntPagination;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMENL1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS1L1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS2L1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardsMapper;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.converter.builtin.BooleanToStringConverter;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
@Mapper
public class TxListCardsMapper implements ITxListCardsMapper {

    private static final String PIPE_SEPARATOR = "|";

    @Autowired
    private EnumMapper enumMapper;

    /**
     * @see com.bbva.pzic.cards.dao.tx.mapper.ITxListCardsMapper#mapIn(com.bbva.pzic.cards.business.dto.DTOIntListCards)
     */
    @Override
    public FormatoMPMENL1 mapIn(final DTOIntListCards dtoIn) {
        FormatoMPMENL1 formatoMPMENL1 = new FormatoMPMENL1();
        formatoMPMENL1.setNumclie(dtoIn.getCustomerId());

        formatoMPMENL1.setTiptarc(mapInEnumPipeConcatenate("cards.cardType.id", dtoIn.getCardTypeId()));
        formatoMPMENL1.setSopfisl(enumMapper.getBackendValue("cards.physicalSupport.id", dtoIn.getPhysicalSupportId()));
        formatoMPMENL1.setEstarjo(mapInEnumPipeConcatenate("cards.status.id", dtoIn.getStatusId()));

        formatoMPMENL1.setIdpagin(dtoIn.getPaginationKey());
        formatoMPMENL1.setTampagi(dtoIn.getPageSize());
        return formatoMPMENL1;
    }

    private String mapInEnumPipeConcatenate(String enumPropertyValue, List<String> backendIds) {
        if (backendIds == null) {
            return null;
        }

        StringBuilder hostEnumIds = new StringBuilder();
        for (String id : backendIds) {
            hostEnumIds.append(enumMapper.getBackendValue(enumPropertyValue, id));
            hostEnumIds.append(PIPE_SEPARATOR);
        }
        return hostEnumIds.substring(0, hostEnumIds.length() - 1);
    }

    /**
     * @see com.bbva.pzic.cards.dao.tx.mapper.ITxListCardsMapper#mapOut(com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS1L1, com.bbva.pzic.cards.business.dto.DTOIntListCards, com.bbva.pzic.cards.business.dto.DTOOutListCards)
     */
    @Override
    public DTOOutListCards mapOut(FormatoMPMS1L1 formatOutput, DTOIntListCards dtoIn, DTOOutListCards dtoOut) {
        DTOOutListCards cards = dtoOut;
        if (cards == null) {
            cards = new DTOOutListCards();
        }
        if (cards.getData() == null) {
            cards.setData(new ArrayList<Card>());
        }

        Card card = new Card();
        card.setNumber(formatOutput.getNumtarj());

        card.setExpirationDate(formatOutput.getFecvenc());
        card.setHolderName(formatOutput.getNombcli());

        card.setNumberType(getNumberType("cards.numberType.id", formatOutput.getIdtnuco(), formatOutput.getDetnuco()));

        card.setCurrencies(getCurrencies(formatOutput.getMdispue(), formatOutput.getMdispub()));

        card.setCardType(getCardType(formatOutput.getTiptarj(), formatOutput.getDtitarj()));

        card.setPhysicalSupport(getPhysicalSupport(formatOutput.getSopfiss(), formatOutput.getDsopfis()));

        card.setStatus(getStatus(formatOutput.getEstarjs(), formatOutput.getDestarj()));

        card.setAvailableBalance(getAvailableBalance(formatOutput.getSdispon(), formatOutput.getMdispon()));

        card.setDisposedBalance(getDisposedBalance(formatOutput.getSdispue(), formatOutput.getMdispue(), formatOutput.getSdispub(), formatOutput.getMdispub()));

        card.setTitle(getTitle(formatOutput.getIprotar(), formatOutput.getDprotar()));

        card.setBrandAssociation(getBrandAssociation(formatOutput.getIdmatar(), formatOutput.getNomatar()));

        card.setGrantedCredits(mapGrantedCredits(formatOutput.getIlimcre(), formatOutput.getDlimcre()));

        card.setRelatedContracts(mapRelatedContracts(formatOutput.getIdcorel(), formatOutput.getNucorel(), formatOutput.getIdtcore(), formatOutput.getDetcore()));

        card.setBlocks(mapBlocks(formatOutput.getIdebloq(), formatOutput.getIderazo(), formatOutput.getDesrazo(), formatOutput.getFecbloq(), formatOutput.getIdactbl()));

        card.setImages(mapListImages(formatOutput));

        cards.getData().add(card);
        return cards;
    }

    private List<Block> mapBlocks(String blockId, String reasonId, String reasonName, Date blockDate, String isActive) {
        if (blockId == null && reasonId == null && reasonName == null && blockDate == null && isActive == null) {
            return null;
        }
        List<Block> blocks = new ArrayList<>();
        Block block = new Block();
        block.setBlockId(blockId);
        block.setBlockDate(FunctionUtils.buildDatetime(blockDate, Constants.DEFAULT_TIME));
        block.setIsActive(new BooleanToStringConverter().convertFrom(isActive, null));

        Reason reason = new Reason();
        reason.setId(reasonId);
        reason.setName(reasonName);
        block.setReason(reason);

        blocks.add(block);
        return blocks;
    }

    /**
     * @see com.bbva.pzic.cards.dao.tx.mapper.ITxListCardsMapper#mapOut2(com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS2L1, com.bbva.pzic.cards.business.dto.DTOIntListCards, com.bbva.pzic.cards.business.dto.DTOOutListCards)
     */
    @Override
    public DTOOutListCards mapOut2(FormatoMPMS2L1 formatOutput, DTOIntListCards dtoIn, DTOOutListCards dtoOut) {
        DTOOutListCards cards = dtoOut;
        if (formatOutput.getTampagi() == null && formatOutput.getIdpagin() == null) {
            return cards;
        }
        if (cards == null) {
            cards = new DTOOutListCards();
        }
        DTOIntPagination pagination = new DTOIntPagination();
        pagination.setPaginationKey(formatOutput.getIdpagin());
        pagination.setPageSize(formatOutput.getTampagi() == null ? null : formatOutput.getTampagi().longValue());
        cards.setPagination(pagination);
        return cards;
    }

    private List<RelatedContract> mapRelatedContracts(String relatedContractId, String number, String id, String name) {
        if (relatedContractId == null && number == null && id == null && name == null) {
            return null;
        }
        RelatedContract relatedContract = new RelatedContract();
        relatedContract.setRelatedContractId(relatedContractId);
        relatedContract.setNumber(number);
        relatedContract.setNumberType(getNumberType("relatedContracts.numberType.id", id, name));

        List<RelatedContract> relatedContracts = new ArrayList<>();
        relatedContracts.add(relatedContract);
        return relatedContracts;
    }

    private List<GrantedCredit> mapGrantedCredits(BigDecimal amount, String currency) {
        if (amount == null && currency == null) {
            return null;
        }
        GrantedCredit grantedCredit = new GrantedCredit();
        grantedCredit.setAmount(amount);
        grantedCredit.setCurrency(currency);
        List<GrantedCredit> grantedCredits = new ArrayList<>();
        grantedCredits.add(grantedCredit);
        return grantedCredits;
    }

    private BrandAssociation getBrandAssociation(String id, String name) {
        if (id == null && name == null) {
            return null;
        }

        BrandAssociation brandAssociation = new BrandAssociation();
        brandAssociation.setId(enumMapper.getEnumValue("cards.brandAssociation.id", id));
        brandAssociation.setName(name);
        return brandAssociation;
    }

    private Title getTitle(String id, String name) {
        if (id == null && name == null) {
            return null;
        }
        Title title = new Title();
        title.setId(id);
        title.setName(name);
        return title;
    }

    private DisposedBalance getDisposedBalance(BigDecimal amount, String currency, BigDecimal amount2, String currency2) {
        if (amount == null && currency == null && amount2 == null && currency2 == null) {
            return null;
        }
        List<CurrentBalance> currentBalances = new ArrayList<>();
        CurrentBalance currentBalance;
        if (amount != null || currency != null) {
            currentBalance = new CurrentBalance();
            currentBalance.setAmount(amount);
            currentBalance.setCurrency(currency);
            currentBalances.add(currentBalance);
        }
        if (amount2 != null || currency2 != null) {
            currentBalance = new CurrentBalance();
            currentBalance.setAmount(amount2);
            currentBalance.setCurrency(currency2);
            currentBalances.add(currentBalance);
        }
        DisposedBalance disposedBalance = new DisposedBalance();
        disposedBalance.setCurrentBalances(currentBalances);
        return disposedBalance;
    }

    private AvailableBalance getAvailableBalance(BigDecimal amount, String currency) {
        if (amount == null && currency == null) {
            return null;
        }
        List<CurrentBalance> currentBalances = new ArrayList<>();
        CurrentBalance currentBalance = new CurrentBalance();
        currentBalance.setCurrency(currency);
        currentBalance.setAmount(amount);
        currentBalances.add(currentBalance);
        AvailableBalance availableBalance = new AvailableBalance();
        availableBalance.setCurrentBalances(currentBalances);
        return availableBalance;
    }

    private PhysicalSupport getPhysicalSupport(String id, String name) {
        if (id == null && name == null) {
            return null;
        }
        PhysicalSupport physicalSupport = new PhysicalSupport();
        physicalSupport.setId(enumMapper.getEnumValue("cards.physicalSupport.id", id));
        physicalSupport.setName(name);
        return physicalSupport;
    }

    private Status getStatus(String id, String name) {
        if (id == null && name == null) {
            return null;
        }
        Status status = new Status();
        status.setId(enumMapper.getEnumValue("cards.status.id", id));
        status.setName(name);
        return status;
    }

    private CardType getCardType(String id, String name) {
        if (id == null && name == null) {
            return null;
        }
        CardType cardType = new CardType();
        cardType.setId(enumMapper.getEnumValue("cards.cardType.id", id));
        cardType.setName(name);
        return cardType;
    }

    private NumberType getNumberType(String field, String id, String name) {
        if (id == null && name == null) {
            return null;
        }
        NumberType numberType = new NumberType();
        numberType.setId(enumMapper.getEnumValue(field, id));
        numberType.setName(name);
        return numberType;
    }

    private List<Currency> getCurrencies(String mdispue, String mdispub) {
        if (mdispue == null && mdispub == null) {
            return null;
        }
        List<Currency> currencies = new ArrayList<>();
        Currency currency;
        if (mdispue != null) {
            currency = new Currency();
            currency.setCurrency(mdispue);
            currency.setIsMajor(Boolean.TRUE);
            currencies.add(currency);
        }
        if (mdispub != null) {
            currency = new Currency();
            currency.setCurrency(mdispub);
            currency.setIsMajor(Boolean.FALSE);
            currencies.add(currency);
        }
        return currencies;
    }

    private List<Image> mapListImages(FormatoMPMS1L1 formatOutput) {
        List<Image> images = null;
        Image image = mapImage(formatOutput.getIdimgt1(), formatOutput.getNimgta1(), formatOutput.getImgtar1());
        if (image != null) {
            images = new ArrayList<>();
            images.add(image);
        }
        image = mapImage(formatOutput.getIdimgt2(), formatOutput.getNimgta2(), formatOutput.getImgtar2());
        if (image != null) {
            if (images == null) {
                images = new ArrayList<>();
            }
            images.add(image);
        }
        return images;
    }

    private Image mapImage(String idimgt, String nimgta, String imgtar) {
        if (idimgt == null && nimgta == null && imgtar == null) {
            return null;
        }
        Image image = new Image();
        image.setId(idimgt);
        image.setName(nimgta);
        image.setUrl(imgtar);
        return image;
    }
}

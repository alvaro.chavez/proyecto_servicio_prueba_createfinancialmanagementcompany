package com.bbva.pzic.cards.dao.model.ppcutc01_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>EntityOut</code>, utilizado por la clase <code>RespuestaTransaccionPpcutc01_1</code></p>
 *
 * @author Arquitectura Spring BBVA
 * @see RespuestaTransaccionPpcutc01_1
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityout {

    /**
     * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 22, signo = true, obligatorio = true)
    private String id;

    /**
     * <p>Campo <code>number</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 2, nombre = "number", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 16, signo = true, obligatorio = true)
    private String number;

    /**
     * <p>Campo <code>expirationDate</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "expirationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
    private String expirationdate;

    /**
     * <p>Campo <code>holderName</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "holderName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true)
    private String holdername;

    /**
     * <p>Campo <code>openingDate</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "openingDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
    private String openingdate;

    /**
     * <p>Campo <code>currencies</code>, &iacute;ndice: <code>6</code>, tipo: <code>LIST</code>
     */
    @Campo(indice = 6, nombre = "currencies", tipo = TipoCampo.LIST)
    private List<Currencies> currencies;

    /**
     * <p>Campo <code>grantedCredits</code>, &iacute;ndice: <code>7</code>, tipo: <code>LIST</code>
     */
    @Campo(indice = 7, nombre = "grantedCredits", tipo = TipoCampo.LIST)
    private List<Grantedcredits> grantedcredits;

    /**
     * <p>Campo <code>relatedContracts</code>, &iacute;ndice: <code>8</code>, tipo: <code>LIST</code>
     */
    @Campo(indice = 8, nombre = "relatedContracts", tipo = TipoCampo.LIST)
    private List<Relatedcontracts> relatedcontracts;

    /**
     * <p>Campo <code>deliveries</code>, &iacute;ndice: <code>9</code>, tipo: <code>LIST</code>
     */
    @Campo(indice = 9, nombre = "deliveries", tipo = TipoCampo.LIST)
    private List<Deliveries> deliveries;

    /**
     * <p>Campo <code>product</code>, &iacute;ndice: <code>10</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 10, nombre = "product", tipo = TipoCampo.DTO)
    private Product product;

    /**
     * <p>Campo <code>brandAssociation</code>, &iacute;ndice: <code>11</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 11, nombre = "brandAssociation", tipo = TipoCampo.DTO)
    private Brandassociation brandassociation;

    /**
     * <p>Campo <code>physicalSupport</code>, &iacute;ndice: <code>12</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 12, nombre = "physicalSupport", tipo = TipoCampo.DTO)
    private Physicalsupport physicalsupport;

    /**
     * <p>Campo <code>status</code>, &iacute;ndice: <code>13</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 13, nombre = "status", tipo = TipoCampo.DTO)
    private Status status;

    /**
     * <p>Campo <code>supportContractType</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "supportContractType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true)
    private String supportcontracttype;

    /**
     * <p>Campo <code>memberships</code>, &iacute;ndice: <code>15</code>, tipo: <code>LIST</code>
     */
    @Campo(indice = 15, nombre = "memberships", tipo = TipoCampo.LIST)
    private List<Memberships> memberships;

    /**
     * <p>Campo <code>cardAgreement</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 16, nombre = "cardAgreement", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
    private String cardagreement;

    /**
     * <p>Campo <code>contractingBranch</code>, &iacute;ndice: <code>17</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 17, nombre = "contractingBranch", tipo = TipoCampo.DTO)
    private Contractingbranch contractingbranch;

    /**
     * <p>Campo <code>managementBranch</code>, &iacute;ndice: <code>18</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 18, nombre = "managementBranch", tipo = TipoCampo.DTO)
    private Managementbranch managementbranch;

    /**
     * <p>Campo <code>contractingBusinessAgent</code>, &iacute;ndice: <code>19</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 19, nombre = "contractingBusinessAgent", tipo = TipoCampo.DTO)
    private Contractingbusinessagent contractingbusinessagent;

    /**
     * <p>Campo <code>marketBusinessAgent</code>, &iacute;ndice: <code>20</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 20, nombre = "marketBusinessAgent", tipo = TipoCampo.DTO)
    private Marketbusinessagent marketbusinessagent;

    /**
     * <p>Campo <code>numberType</code>, &iacute;ndice: <code>21</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 21, nombre = "numberType", tipo = TipoCampo.DTO)
    private Numbertype numbertype;

    /**
     * <p>Campo <code>cardType</code>, &iacute;ndice: <code>22</code>, tipo: <code>DTO</code>
     */
    @Campo(indice = 22, nombre = "cardType", tipo = TipoCampo.DTO)
    private Cardtype cardtype;

    /**
     * <p>Campo <code>cutOffDay</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 23, nombre = "cutOffDay", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
    private String cutoffday;

    /**
     * <p>Campo <code>bankIdentificationNumber</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 24, nombre = "bankIdentificationNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
    private String bankidentificationnumber;

}
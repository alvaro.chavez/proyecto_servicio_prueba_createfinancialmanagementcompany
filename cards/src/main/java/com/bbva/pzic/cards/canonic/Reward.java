package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Reward", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Reward", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reward implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Reward identifier.
     */
    private String rewardId;
    /**
     * Reward description.
     */
    private String name;
    /**
     * Non-monetary reward value or amount. This will not be used when the
     * reward is monetary.
     */
    private BigDecimal nonMonetaryValue;
    /**
     * Monetary reward value or amount. This will not be used when the reward is
     * non-monetary.
     */
    private MonetaryValue monetaryValue;
    /**
     * Reward unit information.
     */
    private UnitType unitType;

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getNonMonetaryValue() {
        return nonMonetaryValue;
    }

    public void setNonMonetaryValue(BigDecimal nonMonetaryValue) {
        this.nonMonetaryValue = nonMonetaryValue;
    }

    public MonetaryValue getMonetaryValue() {
        return monetaryValue;
    }

    public void setMonetaryValue(MonetaryValue monetaryValue) {
        this.monetaryValue = monetaryValue;
    }

    public UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(UnitType unitType) {
        this.unitType = unitType;
    }
}
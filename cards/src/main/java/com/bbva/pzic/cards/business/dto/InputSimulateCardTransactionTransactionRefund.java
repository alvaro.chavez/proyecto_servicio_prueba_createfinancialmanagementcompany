package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 17/09/2020.
 *
 * @author Entelgy.
 */
public class InputSimulateCardTransactionTransactionRefund {

    @NotNull(groups = ValidationGroup.SimulateCardTransactionTransactionRefund.class)
    @Size(max = 19, groups = ValidationGroup.SimulateCardTransactionTransactionRefund.class)
    private String cardId;
    @NotNull(groups = ValidationGroup.SimulateCardTransactionTransactionRefund.class)
    @Size(max = 20, groups = ValidationGroup.SimulateCardTransactionTransactionRefund.class)
    private String transactionId;
    @NotNull(groups = ValidationGroup.SimulateCardTransactionTransactionRefund.class)
    @Size(max = 2, groups = ValidationGroup.SimulateCardTransactionTransactionRefund.class)
    private String refundTypeId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getRefundTypeId() {
        return refundTypeId;
    }

    public void setRefundTypeId(String refundTypeId) {
        this.refundTypeId = refundTypeId;
    }
}

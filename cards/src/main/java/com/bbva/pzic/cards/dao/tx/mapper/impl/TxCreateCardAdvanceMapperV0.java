package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.dao.model.mprt.FormatoMPRMRT0;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardAdvanceMapperV0;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;

@Mapper("txCreateCardAdvanceMapperV0")
public class TxCreateCardAdvanceMapperV0 extends ConfigurableMapper implements ITxCreateCardAdvanceMapperV0 {

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(InputCreateCard.class, FormatoMPRMRT0.class)
                .field("dtoIntCard.relatedContractIdLinkedWith", "ctacarg")
                .field("dtoIntCard.relatedContractIdRenewalDueTo", "nucorel")
                .field("dtoIntCard.number", "tarinom")
                .field("dtoIntCard.participantId", "numclie")
                .field("dtoIntCard.deliveriesManagementContactDetailIdSmsCode", "idtesms")
                .field("dtoIntCard.deliveryManagementConstant", "indcsms")
                .register();
    }

    @Override
    public FormatoMPRMRT0 mapIn(InputCreateCard input) {
        return map(input, FormatoMPRMRT0.class);
    }
}

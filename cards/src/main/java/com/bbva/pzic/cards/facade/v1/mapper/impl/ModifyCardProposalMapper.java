package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntProposal;
import com.bbva.pzic.cards.business.dto.InputModifyCardProposal;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;
import com.bbva.pzic.cards.facade.v1.mapper.IModifyCardProposalMapper;
import com.bbva.pzic.cards.facade.v1.mapper.common.AbstractCardProposalMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 27/11/2019.
 *
 * @author Entelgy
 */
@Component
public class ModifyCardProposalMapper implements IModifyCardProposalMapper {

    private static final Log LOG = LogFactory.getLog(ModifyCardProposalMapper.class);

    private AbstractCardProposalMapper cardProposalMapper;

    @Resource(name = "cardProposalTimeMapper")
    public void setAddressMapper(AbstractCardProposalMapper addressMapper) {
        this.cardProposalMapper = addressMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputModifyCardProposal mapIn(final String proposalId, final Proposal proposal) {
        LOG.info("... called method ModifyCardProposalMapper.mapIn ...");
        InputModifyCardProposal input = new InputModifyCardProposal();
        input.setProposalId(proposalId);
        input.setProposal(mapInProposal(proposal));
        return input;
    }

    private DTOIntProposal mapInProposal(final Proposal proposal) {
        DTOIntProposal dtoIntProposal = new DTOIntProposal();
        dtoIntProposal.setProduct(cardProposalMapper.mapInProduct(proposal.getProduct()));
        dtoIntProposal.setDeliveries(cardProposalMapper.mapInDeliveries(proposal.getDeliveries()));
        dtoIntProposal.setPaymentMethod(cardProposalMapper.mapInPaymentMethod(proposal.getPaymentMethod()));
        dtoIntProposal.setContactability(cardProposalMapper.mapInContactAbility(proposal.getContactability()));
        dtoIntProposal.setRate(cardProposalMapper.mapInRates(proposal.getRates()));
        dtoIntProposal.setFees(cardProposalMapper.mapInFees(proposal.getFees()));
        dtoIntProposal.setGrantedCredits(cardProposalMapper.mapInGrantedCredits(proposal.getGrantedCredits()));
        dtoIntProposal.setOfferId(proposal.getOfferId());
        dtoIntProposal.setNotificationsByOperation(proposal.getNotificationsByOperation());
        return dtoIntProposal;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServiceResponse mapOut(final Proposal proposal) {
        LOG.info("... called method ModifyCardProposalMapper.mapOut ...");
        if (proposal == null) {
            return null;
        }
        return ServiceResponse.data(proposal).build();
    }
}

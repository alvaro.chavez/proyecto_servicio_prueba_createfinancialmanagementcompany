package com.bbva.pzic.cards.facade.v00.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v00.mapper.ICreateCardInstallmentsPlanMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 03/07/2017.
 *
 * @author Entelgy
 */
@Mapper
public class CreateCardInstallmentsPlanMapper implements ICreateCardInstallmentsPlanMapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntCardInstallmentsPlan mapIn(String cardId, InstallmentsPlan installmentsPlan) {
        DTOIntCardInstallmentsPlan dtoInt = new DTOIntCardInstallmentsPlan();
        dtoInt.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN_V00));
        dtoInt.setTransactionId(installmentsPlan.getTransactionId());
        if (installmentsPlan.getTerms() != null) {
            dtoInt.setTermsNumber(installmentsPlan.getTerms().getNumber());
        }
        return dtoInt;
    }
}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOInputCreateCardRelatedContract;
import com.bbva.pzic.cards.business.dto.DTOOutCreateCardRelatedContract;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1E;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1S;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardRelatedContractMapper;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 14/02/2017.
 *
 * @author Entelgy
 */
@Mapper("txCreateRelatedContractMapper")
public class TxCreateCardRelatedContractMapper implements ITxCreateCardRelatedContractMapper {

    @Autowired
    private EnumMapper enumMapper;

    @Override
    public FormatoMPM0V1E mapIn(final DTOInputCreateCardRelatedContract dtoIn) {
        FormatoMPM0V1E formatoMPM0V1E = new FormatoMPM0V1E();
        formatoMPM0V1E.setNumtarj(dtoIn.getCardId());
        formatoMPM0V1E.setNucorel(dtoIn.getContractId());
        return formatoMPM0V1E;
    }

    @Override
    public DTOOutCreateCardRelatedContract mapOut(FormatoMPM0V1S formatOutput, DTOInputCreateCardRelatedContract dtoIn) {
        if (formatOutput == null) {
            return null;
        }
        if (formatOutput.getDetcore() == null && formatOutput.getIdcorel() == null &&
                formatOutput.getIdtcore() == null && formatOutput.getNucorel() == null) {
            return null;
        }
        DTOOutCreateCardRelatedContract contract = new DTOOutCreateCardRelatedContract();
        contract.setRelatedContractId(formatOutput.getIdcorel());
        contract.setContractId(formatOutput.getNucorel());
        contract.setNumberTypeId(enumMapper.getEnumValue("relatedContracts.numberType.id", formatOutput.getIdtcore()));
        contract.setNumberTypeName(formatOutput.getDetcore());
        return contract;
    }
}

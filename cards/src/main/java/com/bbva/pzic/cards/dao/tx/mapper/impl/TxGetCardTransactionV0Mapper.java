package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputGetCardTransaction;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.mpgm.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardTransactionV0Mapper;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.converter.builtin.DateToStringConverter;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import static com.bbva.pzic.cards.util.Constants.DEFAULT_TIME;

/**
 * Created on 22/12/2017.
 *
 * @author Entelgy
 */
@Mapper("txGetCardTransactionV0Mapper")
public class TxGetCardTransactionV0Mapper extends ConfigurableMapper implements ITxGetCardTransactionV0Mapper {

    public static final String FEE_PAYMENT = "FEE_PAYMENT";
    public static final String CARDS_TRANSACTION_PERIOD = "cards.transaction.period";
    private static final String IDENTIF = "identif";
    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.getConverterFactory().registerConverter(new DateToStringConverter("yyyy-MM-dd"));

        factory.classMap(InputGetCardTransaction.class, FormatoMPMENGM.class)
                .field("cardId", "numtarj")
                .field("transactionId", IDENTIF)
                .register();

        factory.classMap(FormatoMPMS1GM.class, TransactionData.class)
                .field(IDENTIF, "data.id")
                .field("importe", "data.localAmount.amount")
                .field("divisa", "data.localAmount.currency")
                .field("tipmov", "data.transactionType.id")
                .field("nommov", "data.transactionType.name")
                .field("intcod", "data.transactionType.internalCode.id")
                .field("nomcod", "data.transactionType.internalCode.name")
                .field("concept", "data.concept")
                .field("indoper", "data.moneyFlow.id")
                .field("nomiope", "data.moneyFlow.name")
                .field("fechcon", "data.accountedDate")
                .field("fechcon", "data.valuationDate")
                .field("idfinan", "data.financingType.id")
                .field("tipfina", "data.financingType.name")
                .field("idestad", "data.status.id")
                .field("tipesta", "data.status.name")
                .field("tipcamb", "data.exchangeRate.values.factor.ratio")
                .field("modcamb", "data.exchangeRate.values.priceType")
                .field("idcont", "data.contract.id")
                .field("numtarj", "data.contract.number")
                .field("tipnum", "data.contract.numberType.id")
                .field("descnum", "data.contract.numberType.name")
                .field(IDENTIF, "data.additionalInformation.reference")
                .register();

        factory.classMap(FormatoMPMS2GM.class, Purchase.class)
                .field("idtiend", "store.id")
                .field("nomtien", "store.name")
                .field("idcateg", "store.category.id")
                .field("nomcat", "store.category.name")
                .field("tippag", "paymentChannel.id")
                .field("despag", "paymentChannel.name")
                .register();

        factory.classMap(FormatoMPMS3GM.class, CashWithdrawal.class)
                .field("idatm", "atm.id")
                .field("idred", "atm.net.id")
                .field("tipatm", "atm.net.name")
                .field("origret", "originType")
                .field("idofic", "branch.id")
                .field("desofic", "branch.name")
                .register();

        factory.classMap(FormatoMPMS4GM.class, FeePayment.class)
                .field("impcons", "accumulatedDisposedBalance.amount")
                .field("divcons", "accumulatedDisposedBalance.currency")
                .field("porcons", "accumulatedDisposedBalance.percentage")
                .field("periodo", "accumulatedDisposedBalance.period")
                .field("impmeta", "feeExonerationAmount.amount")
                .field("divmeta", "feeExonerationAmount.currency")
                .register();
    }

    @Override
    public FormatoMPMENGM mapIn(InputGetCardTransaction dtoIn) {
        return map(dtoIn, FormatoMPMENGM.class);
    }

    @Override
    public TransactionData mapOut(FormatoMPMS1GM formatOutput, TransactionData dtoOut) {
        TransactionData transactionData = map(formatOutput, TransactionData.class);

        if (formatOutput.getFechope() != null) {
            if (transactionData.getData().getExchangeRate() == null) {
                transactionData.getData().setExchangeRate(new ExchangeRate());
            }
            transactionData.getData().getExchangeRate().setDate(FunctionUtils.buildDatetime(formatOutput.getFechope(), DEFAULT_TIME));
        }

        if (formatOutput.getTipmov() != null) {
            transactionData.getData().getTransactionType().setId(enumMapper.getEnumValue("cards.transaction.transactionType.id", formatOutput.getTipmov()));
        }
        if (formatOutput.getIndoper() != null) {
            transactionData.getData().getMoneyFlow().setId(enumMapper.getEnumValue("transactions.moneyFlow.id", formatOutput.getIndoper()));
        }
        if (formatOutput.getIdfinan() != null) {
            transactionData.getData().getFinancingType().setId(enumMapper.getEnumValue("transactions.financingType.id", formatOutput.getIdfinan()));
        }
        if (formatOutput.getIdestad() != null) {
            transactionData.getData().getStatus().setId(enumMapper.getEnumValue("transactions.status.id", formatOutput.getIdestad()));
        }
        if (formatOutput.getFechope() != null && !StringUtils.isEmpty(formatOutput.getHorope())) {
            transactionData.getData().setOperationDate(FunctionUtils.buildDatetime(formatOutput.getFechope(), formatOutput.getHorope()));
        }
        return transactionData;
    }

    @Override
    public TransactionData mapOut2(FormatoMPMS2GM formatOutput, TransactionData dtoOut) {
        if (dtoOut.getData() == null) {
            dtoOut.setData(new Transaction());
        }

        if (dtoOut.getData().getTransactionType() == null || !"PURCHASE".equalsIgnoreCase(dtoOut.getData().getTransactionType().getId())) {
            return dtoOut;
        }

        Purchase purchase = map(formatOutput, Purchase.class);
        dtoOut.getData().setDetail(purchase);
        return dtoOut;
    }

    @Override
    public TransactionData mapOut3(final FormatoMPMS3GM formatOutput, final TransactionData dtoOut) {
        if (dtoOut.getData() == null) {
            dtoOut.setData(new Transaction());
        }

        if (dtoOut.getData().getTransactionType() == null || !"CASH_WITHDRAWAL".equalsIgnoreCase(dtoOut.getData().getTransactionType().getId())) {
            return dtoOut;
        }

        CashWithdrawal cashWithdrawal = map(formatOutput, CashWithdrawal.class);
        if (formatOutput.getOrigret() != null) {
            cashWithdrawal.setOriginType(enumMapper.getEnumValue("cards.transaction.originType", formatOutput.getOrigret()));
        }
        dtoOut.getData().setDetail(cashWithdrawal);
        return dtoOut;
    }

    @Override
    public TransactionData mapOut4(FormatoMPMS4GM formatOutput, TransactionData dtoOut) {
        if (dtoOut.getData() == null) {
            dtoOut.setData(new Transaction());
        }

        if (dtoOut.getData().getTransactionType() == null || !FEE_PAYMENT.equalsIgnoreCase(dtoOut.getData().getTransactionType().getId())) {
            return dtoOut;
        }

        if (formatOutput.getPeriodo() != null) {
            String periodo = enumMapper.getEnumValue(CARDS_TRANSACTION_PERIOD, formatOutput.getPeriodo());
            formatOutput.setPeriodo(periodo);
        }

        FeePayment feePayment = map(formatOutput, FeePayment.class);
        dtoOut.getData().setDetail(feePayment);

        return dtoOut;
    }
}

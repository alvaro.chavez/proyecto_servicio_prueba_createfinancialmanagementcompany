package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "addressComponent", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "addressComponent", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class AddressComponent implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * An abbreviated textual name for the address component. For example,an
     * address component for Spain, may have a `longName` of "Spain", anda
     * `shortName` of `ES`, using the ISO-3166-1 2-digit code.
     */
    private String code;
    /**
     * The full text description or name of the address component.
     */
    private String name;

    private List<String> componentTypes;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getComponentTypes() {
        return componentTypes;
    }

    public void setComponentTypes(List<String> componentTypes) {
        this.componentTypes = componentTypes;
    }
}

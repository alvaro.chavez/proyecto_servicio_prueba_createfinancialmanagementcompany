package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Title", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Title", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Title implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Financial product title identifier.
     */
    private String id;
    /**
     * Financial product title name.
     */
    private String name;

    private SubProduct subproduct;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SubProduct getSubproduct() {
        return subproduct;
    }

    public void setSubproduct(SubProduct subproduct) {
        this.subproduct = subproduct;
    }

}
package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "frecuencyPost", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "frecuencyPost", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class FrecuencyPost implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Period identifier.
     */
    private String id;
    /**
     * Specifies the days of the month in which the fortnight or monthly
     * frequency applies. For example, fortnight frequency which restarts the 2º
     * day of month and the 18º day of month.
     */
    private DaysOfMonth daysOfMonth;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DaysOfMonth getDaysOfMonth() {
        return daysOfMonth;
    }

    public void setDaysOfMonth(DaysOfMonth daysOfMonth) {
        this.daysOfMonth = daysOfMonth;
    }
}

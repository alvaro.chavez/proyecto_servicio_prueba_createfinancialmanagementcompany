package com.bbva.pzic.cards.dao.model.ppcut001_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PPCUT001</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPpcut001_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPpcut001_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: createCardProposal - PPCUT001-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PPCUT001&quot; application=&quot;PPCU&quot; version=&quot;01&quot; country=&quot;PE&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;dto name=&quot;EntityIn&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.CardProposalDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;dto name=&quot;cardType&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;product&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ProductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;physicalSupport&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;14&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;list name=&quot;grantedCredits&quot; order=&quot;4&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;grantedCredit&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.AmountDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;contact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;specificContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDetailDTO&quot;
 * artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactDetailType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;phoneCompany&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;rates&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.RateDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;6&quot;&gt;
 * &lt;list name=&quot;itemizeRates&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeRate&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateDTO&quot;
 * artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;rateType&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;itemizeRatesUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitRateType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;percentage&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;fees&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FeeDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;7&quot;&gt;
 * &lt;list name=&quot;itemizeFees&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeFee&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeDTO&quot;
 * artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;feeType&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;itemizeFeeUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitType&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;offerId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;list name=&quot;participants&quot; order=&quot;9&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;participant&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ParticipantDTO&quot;
 * artifactId=&quot;PPCUC001&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;16&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;isCustomer&quot; mandatory=&quot;1&quot; type=&quot;Boolean&quot; size=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;personType&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;6&quot;/&gt;
 * &lt;dto order=&quot;4&quot; name=&quot;participantType&quot; mandatory=&quot;1&quot;
 * package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot; artifactId=&quot;PPCUC001&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;dto name=&quot;EntityOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.CardProposalDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;36&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;cardType&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;product&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ProductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;physicalSupport&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;14&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;list name=&quot;grantedCredits&quot; order=&quot;5&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;grantedCredit&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.AmountDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;contact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;6&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;specificContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDetailDTO&quot;
 * artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactDetailType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;phoneCompany&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;rates&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.RateDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;7&quot;&gt;
 * &lt;list name=&quot;itemizeRates&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeRate&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;rateType&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;itemizeRatesUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitRateType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;percentage&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;fees&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FeeDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;8&quot;&gt;
 * &lt;list name=&quot;itemizeFees&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeFee&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;feeType&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;itemizeFeeUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitType&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;operationNumber&quot; type=&quot;String&quot; size=&quot;36&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;operationDate&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;11&quot; name=&quot;status&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;12&quot; name=&quot;offerId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;list name=&quot;participants&quot; order=&quot;13&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;participant&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ParticipantDTO&quot;
 * artifactId=&quot;PPCUC001&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;16&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;isCustomer&quot; mandatory=&quot;1&quot; type=&quot;Boolean&quot; size=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;personType&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;6&quot;/&gt;
 * &lt;dto order=&quot;4&quot; name=&quot;participantType&quot; mandatory=&quot;1&quot;
 * package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot; artifactId=&quot;PPCUC001&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;firstName&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;lastName&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;30&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion que ejecuta el registro de una solicitud
 * &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPpcut001_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PPCUT001",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPpcut001_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPpcut001_1 {
		
		/**
	 * <p>Campo <code>EntityIn</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "EntityIn", tipo = TipoCampo.DTO)
	private Entityin entityin;
	
}
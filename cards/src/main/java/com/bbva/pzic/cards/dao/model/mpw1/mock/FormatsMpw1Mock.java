package com.bbva.pzic.cards.dao.model.mpw1.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPMS1W1;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 12/06/2017.
 *
 * @author Entelgy
 */
public class FormatsMpw1Mock {

    private ObjectMapperHelper objectMapper;

    public FormatsMpw1Mock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public FormatoMPMS1W1 getFormatoMPMS1W1() {
        try {
            return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                            .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpw1/mock/FormatoMPMS1W1.json"),
                    FormatoMPMS1W1.class);
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

}

package com.bbva.pzic.cards.dao.model.awsimages;

/**
 * Created on 01/08/2019.
 *
 * @author Entelgy
 */
public class ModelType {

    private String id;

    ModelType() {
        this.id = "N";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;

/**
 * Created on 23/03/2018.
 *
 * @author Entelgy
 */
public interface IModifyCardV0Mapper {

    /**
     * Method that creates a DTO with the input data.
     *
     * @param cardId unique card identifier
     * @param card   Object with the data of the card
     * @return Object with the data of input
     */
    DTOIntCard mapIn(String cardId, Card card);

}

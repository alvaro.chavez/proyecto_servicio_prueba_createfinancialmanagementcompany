package com.bbva.pzic.cards.facade.v01;

import com.bbva.pzic.cards.canonic.*;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author Entelgy
 */
public interface ISrvCardsV01 {

    /**
     * Method for creating a new card related to a specific user.
     *
     * @param card payload
     * @return the id of card
     */
    CardData createCard(Card card);

    /**
     * Method for updating an operational activation related to a card.
     *
     * @param cardId       card identifier
     * @param activationId activation identifier
     * @param activation   card operational activation
     * @return the operational activation updated
     */
    Response modifyCardActivation(String cardId, String activationId, Activation activation);

    /**
     * Method for retrieving the list of cards related to a specific user.
     *
     * @param customerId        Customer ID
     * @param cardTypeId        Type card ID
     * @param physicalSupportId Physical Support ID
     * @param statusId          Status ID
     * @param expand            Value to expand
     * @param paginationKey     Pagination ID
     * @param pageSize          Size of pagination
     * @return DTOIntListCards
     */
    CardsList listCards(String customerId, List<String> cardTypeId, String physicalSupportId,
                        List<String> statusId, String expand, String paginationKey, Integer pageSize);

    /**
     * Method for retrieving all transactions that match with searching criteria.
     * These criteria parameters may be concept, financing type, contract, operation date, transaction type and
     * the currency that was used for the different operations.
     *
     * @param cardId            unique card identifier
     * @param fromOperationDate Filters the transactions which operation date is later than this one (ISO-8601 date format).
     * @param toOperationDate   Filters the transactions which operation date is earlier than this one (ISO-8601 date format)
     * @param paginationKey     Key to obtain a single page
     * @param pageSize          Number of elements per page
     * @return the list of card transactions
     */
    TransactionsData listCardTransactions1(String cardId, String fromOperationDate, String toOperationDate, String paginationKey, Long pageSize);

    TransactionsData listCardTransactions2(String cardId, String fromOperationDate, String toOperationDate, String paginationKey, Long pageSize);

    /**
     * Method for adding a new related contract to a card.
     *
     * @param cardId          unique card identifier
     * @param relatedContract Object with the data that relates the card and the contract.
     * @return object RelatedContractData
     */
    RelatedContractData createCardRelatedContract(String cardId, RelatedContract relatedContract);

}

package com.bbva.pzic.cards.dao.model.cardreports;

public class ModelTipoContacto {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

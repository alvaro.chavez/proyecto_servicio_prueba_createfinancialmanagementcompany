package com.bbva.pzic.cards.dao.model.ppcut003_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>specificContact</code>, utilizado por la clase <code>Delivery</code></p>
 * 
 * @see Delivery
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Specificcontact {

	/**
	 * <p>Campo <code>contactType</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "contactType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String contacttype;
	
	/**
	 * <p>Campo <code>contact</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "contact", tipo = TipoCampo.DTO)
	private Contact contact;

	@Campo(indice = 3, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 80, signo = true, obligatorio = true)
	private String id;

}
package com.bbva.pzic.cards.dao.model.mpw2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPW2</code>
 * 
 * @see PeticionTransaccionMpw2
 * @see RespuestaTransaccionMpw2
 */
@Component
public class TransaccionMpw2 implements InvocadorTransaccion<PeticionTransaccionMpw2,RespuestaTransaccionMpw2> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMpw2 invocar(PeticionTransaccionMpw2 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpw2.class, RespuestaTransaccionMpw2.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMpw2 invocarCache(PeticionTransaccionMpw2 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpw2.class, RespuestaTransaccionMpw2.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
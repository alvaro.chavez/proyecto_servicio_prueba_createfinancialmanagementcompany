package com.bbva.pzic.cards.util.mappers;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;

public final class GabiPaginationMapper {

    private GabiPaginationMapper() {
    }

    public static Pagination perform(Object source) {
        if (source == null) {
            return null;
        }

        if (source instanceof com.bbva.jee.arq.spring.core.servicing.utils.Pagination) {
            com.bbva.jee.arq.spring.core.servicing.utils.Pagination asoPagination = (com.bbva.jee.arq.spring.core.servicing.utils.Pagination) source;
            Pagination pagination = new Pagination();
            pagination.setPage(asoPagination.getPage());
            pagination.setPageSize(asoPagination.getPageSize());
            pagination.setTotalElements(asoPagination.getTotal());
            pagination.setTotalPages(asoPagination.getNumPages());

            Pagination.Links links = new Pagination.Links();
            links.setFirst(asoPagination.getFirstPage());
            links.setLast(asoPagination.getLastPage());
            links.setPrevious(asoPagination.getPreviousPage());
            links.setNext(asoPagination.getNextPage());
            pagination.setLinks(links);

            return pagination;
        } else if (source instanceof Pagination) {
            return (Pagination) source;
        } else {
            return null;
        }
    }

}
package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 21/12/2016.
 *
 * @author Entelgy
 */
public class DTOIntActivation {

    @NotNull(groups = {ValidationGroup.ModifyCardActivations.class})
    @Size(max = 2, groups = {ValidationGroup.ModifyCardActivations.class})
    private String activationId;

    @Valid
    private DTOIntCard card;

    @NotNull(groups = {ValidationGroup.ModifyCardActivation.class,
            ValidationGroup.ModifyCardActivations.class})
    private Boolean isActive;


    public String getActivationId() {
        return activationId;
    }

    public void setActivationId(String activationId) {
        this.activationId = activationId;
    }

    public DTOIntCard getCard() {
        return card;
    }

    public void setCard(DTOIntCard card) {
        this.card = card;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}

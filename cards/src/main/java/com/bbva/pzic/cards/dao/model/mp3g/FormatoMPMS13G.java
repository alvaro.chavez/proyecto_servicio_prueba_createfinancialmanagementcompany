package com.bbva.pzic.cards.dao.model.mp3g;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>MPMS13G</code> de la transacci&oacute;n <code>MP3G</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS13G")
@RooJavaBean
@RooSerializable
public class FormatoMPMS13G {
	
	/**
	 * <p>Campo <code>NOMCLIE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "NOMCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String nomclie;
	
	/**
	 * <p>Campo <code>CONTRAT</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "CONTRAT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String contrat;
	
	/**
	 * <p>Campo <code>FULTLIQ</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "FULTLIQ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String fultliq;
	
	/**
	 * <p>Campo <code>LINCRED</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 4, nombre = "LINCRED", tipo = TipoCampo.DECIMAL, longitudMinima = 13, longitudMaxima = 13, signo = true, decimales = 2)
	private BigDecimal lincred;
	
	/**
	 * <p>Campo <code>MONEDL</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "MONEDL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String monedl;
	
}
package com.bbva.pzic.cards.dao.model.mp2f.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMEN2F;
import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMS12F;
import com.bbva.pzic.cards.dao.model.mp2f.PeticionTransaccionMp2f;
import com.bbva.pzic.cards.dao.model.mp2f.RespuestaTransaccionMp2f;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MP2F</code>
 *
 * @see PeticionTransaccionMp2f
 * @see RespuestaTransaccionMp2f
 */
@Component("transaccionMp2f")
public class TransaccionMp2fMock implements InvocadorTransaccion<PeticionTransaccionMp2f, RespuestaTransaccionMp2f> {

    public static final String TEST_EMPTY = "CSAJDPOSKDOASKDP";
    public static final String TEST_NO_RESPONSE = "CSAJDPOSKDOASKDQ";

    private FormatsMp2fStubs formatsMp2fStubs = FormatsMp2fStubs.getInstance();

    @Override
    public RespuestaTransaccionMp2f invocar(PeticionTransaccionMp2f transaccion) throws ExcepcionTransaccion {
        final RespuestaTransaccionMp2f response = new RespuestaTransaccionMp2f();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMEN2F format = transaccion.getCuerpo().getParte(FormatoMPMEN2F.class);

        if (TEST_NO_RESPONSE.equalsIgnoreCase(format.getIdetarj())) {
            return response;
        }

        CopySalida copySalida = new CopySalida();
        if (TEST_EMPTY.equals(format.getIdetarj())) {
            copySalida.setCopy(new FormatoMPMS12F());
        } else {
            copySalida.setCopy(formatsMp2fStubs.getFormatoMPMS12F());
        }
        response.getCuerpo().getPartes().add(copySalida);

        return response;
    }

    @Override
    public RespuestaTransaccionMp2f invocarCache(PeticionTransaccionMp2f transaccion) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

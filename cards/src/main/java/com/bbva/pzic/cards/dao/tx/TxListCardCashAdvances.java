package com.bbva.pzic.cards.dao.tx;


import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCashAdvancesSearchCriteria;
import com.bbva.pzic.cards.business.dto.DTOIntListCashAdvances;
import com.bbva.pzic.cards.dao.model.mpde.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardCashAdvancesMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component("txListCardCashAdvances")
public class TxListCardCashAdvances
        extends DoubleOutputFormat<DTOIntCashAdvancesSearchCriteria, FormatoMPMENDE, DTOIntListCashAdvances, FormatoMPMS1DE, FormatoMPMS2DE> {

    @Resource(name = "txListCardCashAdvancesMapper")
    private ITxListCardCashAdvancesMapper mapper;

    @Autowired
    public TxListCardCashAdvances(@Qualifier("transaccionMpde") InvocadorTransaccion<PeticionTransaccionMpde, RespuestaTransaccionMpde> transaction) {
        super(transaction, PeticionTransaccionMpde::new, DTOIntListCashAdvances::new, FormatoMPMS1DE.class, FormatoMPMS2DE.class);
    }

    @Override
    protected FormatoMPMENDE mapInput(DTOIntCashAdvancesSearchCriteria dtoIntCashAdvancesSearchCriteria) {
        return mapper.mapInput(dtoIntCashAdvancesSearchCriteria);
    }

    @Override
    protected DTOIntListCashAdvances mapFirstOutputFormat(FormatoMPMS1DE formatoMPMS1DE, DTOIntCashAdvancesSearchCriteria dtoIntCashAdvancesSearchCriteria, DTOIntListCashAdvances dtoOut) {
        return mapper.mapOutput(formatoMPMS1DE, dtoOut);
    }

    @Override
    protected DTOIntListCashAdvances mapSecondOutputFormat(FormatoMPMS2DE formatoMPMS2DE, DTOIntCashAdvancesSearchCriteria dtoIntCashAdvancesSearchCriteria, DTOIntListCashAdvances dtoOut) {
        return mapper.mapOutput2(formatoMPMS2DE, dtoOut);
    }
}

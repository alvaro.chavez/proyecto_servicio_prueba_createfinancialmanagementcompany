package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.canonic.SecurityData;
import com.bbva.pzic.cards.dao.model.mpcv.FormatoMPMENCV;
import com.bbva.pzic.cards.dao.model.mpcv.FormatoMPMS1CV;
import com.bbva.pzic.cards.dao.model.mpcv.PeticionTransaccionMpcv;
import com.bbva.pzic.cards.dao.model.mpcv.RespuestaTransaccionMpcv;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardSecurityDataMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@Component("txGetCardSecurityData")
public class TxGetCardSecurityData
        extends SingleOutputFormat<InputGetCardSecurityData, FormatoMPMENCV, SecurityData, FormatoMPMS1CV> {

    @Resource(name = "txGetCardSecurityDataMapper")
    private ITxGetCardSecurityDataMapper mapper;

    @Autowired
    public TxGetCardSecurityData(@Qualifier("transaccionMpcv") InvocadorTransaccion<PeticionTransaccionMpcv, RespuestaTransaccionMpcv> transaction) {
        super(transaction, PeticionTransaccionMpcv::new, SecurityData::new, FormatoMPMS1CV.class);
    }

    @Override
    protected FormatoMPMENCV mapInput(InputGetCardSecurityData inputGetCardSecurityData) {
        return mapper.mapIn(inputGetCardSecurityData);
    }

    @Override
    protected SecurityData mapFirstOutputFormat(FormatoMPMS1CV formatoMPMS1CV, InputGetCardSecurityData inputGetCardSecurityData, SecurityData securityData) {
        return mapper.mapOut(formatoMPMS1CV);
    }
}

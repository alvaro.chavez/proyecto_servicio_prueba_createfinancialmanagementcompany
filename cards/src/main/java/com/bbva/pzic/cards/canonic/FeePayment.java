package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "FeePayment", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "FeePayment", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class FeePayment extends TransactionTypeAgreement implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * In Perú the attribute is not implemented.
     */
    private PaymentChannel paymentChannel;
    /**
     * Is the spent amount from the granted credit in a fixed period. There are some fees that evaluates this
     * amount against the fee exoneration amount.
     */
    private AccumulatedDisposedBalance accumulatedDisposedBalance;
    /**
     * Fee exoneration amount. It represents the amount that allows to the customer to avoid the
     * payment of the current fee.
     */
    private FeeExonerationAmount feeExonerationAmount;

    public PaymentChannel getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(PaymentChannel paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public AccumulatedDisposedBalance getAccumulatedDisposedBalance() {
        return accumulatedDisposedBalance;
    }

    public void setAccumulatedDisposedBalance(AccumulatedDisposedBalance accumulatedDisposedBalance) {
        this.accumulatedDisposedBalance = accumulatedDisposedBalance;
    }

    public FeeExonerationAmount getFeeExonerationAmount() {
        return feeExonerationAmount;
    }

    public void setFeeExonerationAmount(FeeExonerationAmount feeExonerationAmount) {
        this.feeExonerationAmount = feeExonerationAmount;
    }
}
package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.Valid;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
public class DTOIntRelatedContract {

    @NotNull(groups = ValidationGroup.CreateCardV1.class)
    private String contractId;
    @Valid
    private DTOIntRelationType relationType;
    @Valid
    private DTOIntProduct product;
    private String number;
    @Valid
    private DTOIntNumberType numberType;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public DTOIntRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(DTOIntRelationType relationType) {
        this.relationType = relationType;
    }

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DTOIntNumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(DTOIntNumberType numberType) {
        this.numberType = numberType;
    }
}

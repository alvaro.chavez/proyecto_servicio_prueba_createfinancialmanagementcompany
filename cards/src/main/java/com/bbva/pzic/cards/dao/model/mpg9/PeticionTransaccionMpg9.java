package com.bbva.pzic.cards.dao.model.mpg9;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPG9</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpg9</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpg9</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPG9.D1181107.txt
 * MPG9OFERTA DE INCREMENTO DE LINEA      MP        MP2CMPG9PBDMPPO MPMENG9             MPG9  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2018-04-06XP87452 2018-11-0514.50.53XP92348 2018-04-06-15.50.32.061303XP87452 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENG9.D1181107.txt
 * MPMENG9 �OFERTA INCREMENTO DE LINEA    �F�02�00030�01�00001�NUMTARJ�NUMERO DE TARJETA   �A�020�0�R�        �
 * MPMENG9 �OFERTA INCREMENTO DE LINEA    �F�02�00030�02�00021�NUMOFER�NUMERO DE LA OFERTA �A�010�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1G9.D1181107.txt
 * MPMS1G9 �DETALLE INCREMENTO DE LINEA   �X�10�00143�01�00001�NUMOFER�NUMERO DE LA OFERTA �A�010�0�S�        �
 * MPMS1G9 �DETALLE INCREMENTO DE LINEA   �X�10�00143�02�00011�IDTOFER�IDENT. TIPO OFERTA  �A�002�0�S�        �
 * MPMS1G9 �DETALLE INCREMENTO DE LINEA   �X�10�00143�03�00013�DESOFER�DESCRIPCION OFERTA  �A�040�0�S�        �
 * MPMS1G9 �DETALLE INCREMENTO DE LINEA   �X�10�00143�04�00053�IDORIGE�ORIGEN DE LA OFERTA �A�002�0�S�        �
 * MPMS1G9 �DETALLE INCREMENTO DE LINEA   �X�10�00143�05�00055�DESCORI�DESC. ORIGEN OFERTA �A�040�0�S�        �
 * MPMS1G9 �DETALLE INCREMENTO DE LINEA   �X�10�00143�06�00095�IDEDITA�IND. MONTO EDITABLE �A�001�0�S�        �
 * MPMS1G9 �DETALLE INCREMENTO DE LINEA   �X�10�00143�07�00096�LIMMINI�LIMITE MINIMO       �N�015�2�S�        �
 * MPMS1G9 �DETALLE INCREMENTO DE LINEA   �X�10�00143�08�00111�LIMMAXI�LIMITE MAXIMO       �N�015�2�S�        �
 * MPMS1G9 �DETALLE INCREMENTO DE LINEA   �X�10�00143�09�00126�LIMOFER�LIMITE PRED. OFERTA �N�015�2�S�        �
 * MPMS1G9 �DETALLE INCREMENTO DE LINEA   �X�10�00143�10�00141�DIVISA �DIVISA OFERTA       �A�003�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPG9.D1181107.txt
 * MPG9MPMS1G9 MPNCS1G9MP2CMPG91S                             XP92348 2018-11-05-14.28.55.377198XP92348 2018-11-05-14.28.55.377228
</pre></code>
 *
 * @see RespuestaTransaccionMpg9
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPG9",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpg9.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENG9.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpg9 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
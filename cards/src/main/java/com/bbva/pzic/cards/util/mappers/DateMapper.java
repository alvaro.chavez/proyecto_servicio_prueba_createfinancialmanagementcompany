package com.bbva.pzic.cards.util.mappers;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.util.Errors;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;

/**
 * Created on 25/06/2020.
 *
 * @author Entelgy.
 */
public final class DateMapper {
    private static final Log LOG = LogFactory.getLog(DateMapper.class);

    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String TIME_PATTERN = "HH:mm:ss";

    private DateMapper() {
    }

    public static String mapDateToStringHour(final Calendar date) {
        if (date == null) {
            return null;
        }

        return new SimpleDateFormat(TIME_PATTERN).format(date.getTime());
    }

    public static String mapDateToStringDateTime(final Calendar date) {
        if (date == null) {
            return null;
        }

        return new SimpleDateFormat(DATE_TIME_PATTERN).format(date.getTime());
    }

    public static Calendar buildCalendar(final Date date) {
        if (date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static Calendar buildCalendar(final String time) {
        if (StringUtils.isEmpty(time)) {
            return null;
        }

        try {
            LocalTime localTime = LocalTime.parse(time);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, localTime.getHour());
            calendar.set(Calendar.MINUTE, localTime.getMinute());
            calendar.set(Calendar.SECOND, localTime.getSecond());
            LOG.info(String.format("[DateMapper] Conversion de tiempo: '%s' a fecha: %s", time, calendar.getTime()));
            return calendar;
        } catch (Exception e) {
            LOG.error(String.format("[DateMapper] Error al parsear el tiempo: '%s'", time), e);
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

}

package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 14/05/2020.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "AllowedInterval", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "AllowedInterval", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AllowedInterval implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Minimum amount
     */
    private Import minimumAmount;
    /**
     * Maximun amount
     */
    private Import maximumAmount;

    public Import getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(Import minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public Import getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(Import maximumAmount) {
        this.maximumAmount = maximumAmount;
    }
}

package com.bbva.pzic.cards.business;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.canonic.PaymentMethod;
import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.dao.model.productofferdetail.CardHolderSimulation;
import com.bbva.pzic.cards.facade.v0.dto.*;

import java.util.List;

/**
 * @author Entelgy
 */
public interface ISrvIntCardsV0 {

    /**
     * @param dtoIntCard DTO with the input fields to validate
     * @return Payment method
     */
    PaymentMethod getCardPaymentMethods(DTOIntCard dtoIntCard);

    /**
     * Method that validates input data
     *
     * @param dtoIn DTO with the input fields to validate
     * @return List cards
     */
    DTOOutListCards listCardsV0(DTOIntListCards dtoIn);

    /**
     * Method for retrieving the list of limits related to the current card. The limits are only amount.
     *
     * @param dtoIn DTO with the input fields to validate
     * @return {@link List<Limits>}
     */
    List<Limits> listCardLimits(DTOIntCard dtoIn);

    /**
     * Method that validates mandatory and size of the input data.
     *
     * @param dtoIntCard Object that contains the input data
     * @return Objeto que contiene los datos de salida
     */
    Card getCard(DTOIntCard dtoIntCard);

    /**
     * @param modifyCardLimit DTO with the input fields to validate
     * @return Limit
     */
    Limit modifyCardLimit(InputModifyCardLimit modifyCardLimit);

    /**
     * @param input
     * @return
     */
    TransactionData getCardTransaction(InputGetCardTransaction input);

    /**
     * @param input
     * @return
     */
    Card createCard(InputCreateCard input);

    /**
     * @param installmentPlans
     * @return
     */
    DTOInstallmentsPlanList listInstallmentPlans(InputListInstallmentPlans installmentPlans);

    InstallmentsPlanData createCardInstallmentsPlan(DTOIntCardInstallmentsPlan dtoIn);

    void modifyCard(DTOIntCard dtoInt);

    InstallmentsPlanSimulationData simulateCardInstallmentsPlan(DTOIntCardInstallmentsPlan dtoIn);

    /**
     * Service for retrieving the conditions of the given card.
     *
     * @param input Object that contains the input data
     * @return list of {@link Condition}
     */
    List<Condition> getCardConditions(InputGetCardConditions input);

    /**
     * Service for retrieving the list of periods related to financial
     * statements documents for determined product type.
     *
     * @param input dto with input fields to validate
     * @return {@link Statement}
     */
    DTOIntStatementList listCardFinancialStatements(InputListCardFinancialStatements input);

    DTOIntCardStatement getCardFinancialStatement(InputGetCardFinancialStatement input);

    Document getCardFinancialStatementDocument(DocumentRequest input);

    MaskedToken createCardsMaskedToken(InputCreateCardsMaskedToken input);

    DTOIntMembershipServiceResponse getCardMembership(DTOIntSearchCriteria searchCriteria);

    /**
     * Service for listing the detailed query of the offer sent to the customer.
     *
     * @param input dto with input fields to validate
     * @return {@link Offer}
     */
    Offer getCardsCardOffer(InputGetCardsCardOffer input);

    /**
     * Service for simulating the hiring of a credit card.
     *
     * @param input dto with input fields to validate.
     * @return {@link CardHolderSimulation}
     */
    HolderSimulation createCardsOfferSimulate(DTOIntDetailSimulation input);

    Proposal createCardsProposal(DTOIntProposal dtoIntProposal);

    List<RelatedContracts> listCardRelatedContracts(DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria);

    DTOIntListCashAdvances listCardCashAdvances(DTOIntCashAdvancesSearchCriteria dtoIntCashAdvancesSearchCriteria);

    DTOIntCashRefund createCardCashRefund(DTOInputCreateCashRefund dtoInputCreateCashRefund);

    void createCardReports(InputCreateCardReports input);

    OfferGenerate createOffersGenerateCards(InputCreateOffersGenerateCards mapIn);
}

package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputGetCardConditions;
import com.bbva.pzic.cards.canonic.Condition;
import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPFMCE1;
import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPNCCS1;
import com.bbva.pzic.cards.dao.model.mpq1.PeticionTransaccionMpq1;
import com.bbva.pzic.cards.dao.model.mpq1.RespuestaTransaccionMpq1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardConditionsMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 22/05/2018.
 *
 * @author Entelgy
 */
@Component
public class TxGetCardConditions
        extends SingleOutputFormat<InputGetCardConditions, FormatoMPFMCE1, List<Condition>, FormatoMPNCCS1> {

    @Resource(name = "txGetCardConditionsMapper")
    private ITxGetCardConditionsMapper mapper;

    @Autowired
    public TxGetCardConditions(@Qualifier("transaccionMpq1") InvocadorTransaccion<PeticionTransaccionMpq1, RespuestaTransaccionMpq1> transaction) {
        super(transaction, PeticionTransaccionMpq1::new, ArrayList::new, FormatoMPNCCS1.class);
    }

    @Override
    protected FormatoMPFMCE1 mapInput(InputGetCardConditions inputGetCardConditions) {
        return mapper.mapIn(inputGetCardConditions);
    }

    @Override
    protected List<Condition> mapFirstOutputFormat(FormatoMPNCCS1 formatoMPNCCS1, InputGetCardConditions inputGetCardConditions, List<Condition> conditionList) {
        return mapper.mapOut(formatoMPNCCS1, conditionList);
    }
}
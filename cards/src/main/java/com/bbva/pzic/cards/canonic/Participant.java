package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Participant", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Participant", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Participant implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Participant identifier.
     */
    @DatoAuditable(omitir = true)
    private String participantId;
    /**
     * Participant first name.
     */
    @DatoAuditable(omitir = true)
    private String firstName;
    /**
     * Participant last name.
     */
    @DatoAuditable(omitir = true)
    private String lastName;
    /**
     * Participation role.
     */
    private ParticipantType participantType;

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ParticipantType getParticipantType() {
        return participantType;
    }

    public void setParticipantType(ParticipantType participantType) {
        this.participantType = participantType;
    }
}
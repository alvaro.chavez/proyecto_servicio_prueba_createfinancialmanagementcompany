package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 18/07/2018.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "StatementList", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "StatementList", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatementList implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Statement> data;

    private Pagination pagination;

    public List<Statement> getData() {
        return data;
    }

    public void setData(List<Statement> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}

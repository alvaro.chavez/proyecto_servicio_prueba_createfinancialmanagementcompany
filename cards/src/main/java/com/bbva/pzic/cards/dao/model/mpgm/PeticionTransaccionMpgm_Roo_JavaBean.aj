// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mpgm;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.pzic.cards.dao.model.mpgm.PeticionTransaccionMpgm;

privileged aspect PeticionTransaccionMpgm_Roo_JavaBean {
    
    /**
     * Sets cuerpo value
     * 
     * @param cuerpo
     * @return PeticionTransaccionMpgm
     */
    public PeticionTransaccionMpgm PeticionTransaccionMpgm.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String PeticionTransaccionMpgm.toString() {
        return "PeticionTransaccionMpgm {" + 
        "}" + super.toString();
    }
    
}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.business.dto.DTOIntListLimits;
import com.bbva.pzic.cards.canonic.AllowedInterval;
import com.bbva.pzic.cards.canonic.AmountLimit;
import com.bbva.pzic.cards.canonic.Import;
import com.bbva.pzic.cards.canonic.Limits;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMENGL;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMS1GL;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardLimitsMapperV0;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy
 */
@Mapper
public class TxListCardLimitsMapperV0 implements ITxListCardLimitsMapperV0 {

    private static final Log LOG = LogFactory.getLog(TxListCardLimitsMapperV0.class);

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public FormatoMPMENGL mapIn(DTOIntCard dtoIn) {
        LOG.info("... called method TxListCardLimitsMapperV0.mapIn ...");
        if (dtoIn == null)
            return null;

        FormatoMPMENGL format = new FormatoMPMENGL();
        format.setIdetarj(dtoIn.getCardId());
        return format;
    }

    @Override
    public List<Limits> mapOut(FormatoMPMS1GL formatOutput, List<Limits> dtoOut) {
        LOG.info("... called method TxListCardLimitsMapperV0.mapOut ...");
        if (dtoOut == null)
            dtoOut = new ArrayList<>();

        Limits limits = new Limits();
        limits.setId(translator.translateBackendEnumValueStrictly("cards.limits.id", formatOutput.getIdlimit()));
        limits.setAmountLimits(getAmountLimits(formatOutput));
        limits.setAllowedInterval(getAmounts(formatOutput));

        dtoOut.add(limits);
        return dtoOut;
    }

    private List<AmountLimit> getAmountLimits(FormatoMPMS1GL formatOutput) {
        if (formatOutput.getMonactt() == null && formatOutput.getDivactt() == null)
            return null;

        List<AmountLimit> amountLimits = new ArrayList<>();
        AmountLimit amountLimit = new AmountLimit();
        amountLimit.setAmount(formatOutput.getMonactt());
        amountLimit.setCurrency(formatOutput.getDivactt());
        amountLimits.add(amountLimit);
        return amountLimits;
    }

    private AllowedInterval getAmounts(FormatoMPMS1GL formatOutput) {
        if (formatOutput.getMonmint() == null && formatOutput.getDivmint() == null &&
                formatOutput.getMonmaxt() == null && formatOutput.getDivmaxt() == null)
            return null;

        AllowedInterval allowedInterval = new AllowedInterval();
        allowedInterval.setMinimumAmount(getMinimumAmount(formatOutput));
        allowedInterval.setMaximumAmount(getMaximumAmount(formatOutput));
        return allowedInterval;
    }

    private Import getMinimumAmount(FormatoMPMS1GL formatOutput) {
        if (formatOutput.getMonmint() == null && formatOutput.getDivmint() == null)
            return null;

        Import minimumAmount = new Import();
        minimumAmount.setAmount(formatOutput.getMonmint());
        minimumAmount.setCurrency(formatOutput.getDivmint());
        return minimumAmount;
    }

    private Import getMaximumAmount(FormatoMPMS1GL formatOutput) {
        if (formatOutput.getMonmaxt() == null && formatOutput.getDivmaxt() == null)
            return null;

        Import maximumAmount = new Import();
        maximumAmount.setAmount(formatOutput.getMonmaxt());
        maximumAmount.setCurrency(formatOutput.getDivmaxt());
        return maximumAmount;
    }
}

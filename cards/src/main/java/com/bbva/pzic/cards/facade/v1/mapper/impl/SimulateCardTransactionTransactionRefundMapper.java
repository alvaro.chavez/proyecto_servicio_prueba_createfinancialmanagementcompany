package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputSimulateCardTransactionTransactionRefund;
import com.bbva.pzic.cards.facade.v1.dto.SimulateTransactionRefund;
import com.bbva.pzic.cards.facade.v1.mapper.ISimulateCardTransactionTransactionRefundMapper;
import com.bbva.pzic.cards.util.Enums;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 17/09/2020.
 *
 * @author Entelgy.
 */
@Component
public class SimulateCardTransactionTransactionRefundMapper implements ISimulateCardTransactionTransactionRefundMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public InputSimulateCardTransactionTransactionRefund mapIn(final String cardId, final String transactionId, final SimulateTransactionRefund simulateTransactionRefund) {
        InputSimulateCardTransactionTransactionRefund input = new InputSimulateCardTransactionTransactionRefund();
        input.setCardId(cardId);
        input.setTransactionId(transactionId);
        input.setRefundTypeId(simulateTransactionRefund.getRefundType() == null ? null :
                translator.translateFrontendEnumValueStrictly(Enums.TRANSACTION_REFUND_REFUND_TYPE_ID_KEY, simulateTransactionRefund.getRefundType().getId()));
        return input;
    }

    @Override
    public ServiceResponse<SimulateTransactionRefund> mapOut(final SimulateTransactionRefund simulateCardTransactionTransactionRefund) {
        if (simulateCardTransactionTransactionRefund == null) {
            return null;
        }

        return ServiceResponse.data(simulateCardTransactionTransactionRefund).build();
    }
}

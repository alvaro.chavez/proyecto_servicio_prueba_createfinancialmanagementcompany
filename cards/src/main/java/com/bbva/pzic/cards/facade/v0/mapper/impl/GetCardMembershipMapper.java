package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntMembership;
import com.bbva.pzic.cards.business.dto.DTOIntMembershipServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntSearchCriteria;
import com.bbva.pzic.cards.facade.v0.dto.Membership;
import com.bbva.pzic.cards.facade.v0.dto.Status;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardMembershipMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper
public class GetCardMembershipMapper implements IGetCardMembershipMapper {

    private static final Log LOG = LogFactory.getLog(GetCardMembershipMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntSearchCriteria mapInput(String cardId, String membershipId) {
        LOG.info("----- Dentro de GetCardMembershipMapper.mapInput() -----");
        DTOIntSearchCriteria searchCriteria = new DTOIntSearchCriteria();
        searchCriteria.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.CARDID));
        searchCriteria.setMembershipId(membershipId);
        return searchCriteria;
    }

    @Override
    public ServiceResponse<Membership> mapOutput(DTOIntMembershipServiceResponse dtoIntMembershipServiceResponse) {
        LOG.info("----- Dentro de GetCardMembershipMapper.mapOutput() -----");
        if (dtoIntMembershipServiceResponse == null ||
                dtoIntMembershipServiceResponse.getData() == null) {
            return null;
        }

        DTOIntMembership dtoIntMembership = dtoIntMembershipServiceResponse.getData();
        Membership membership = new Membership();


        membership.setId(dtoIntMembership.getId());
        membership.setDescription(dtoIntMembership.getDescription());
        membership.setNumber(dtoIntMembership.getNumber());
        membership.setEnrollmentDate(dtoIntMembership.getEnrollmentDate());
        membership.setCancellationDate(dtoIntMembership.getCancellationDate());

        if (dtoIntMembership.getStatus() != null) {
            Status status = new Status();
            status.setId(dtoIntMembership.getStatus().getId());
            membership.setStatus(status);
        }

        membership.setIssuedDueToMigration(dtoIntMembership.getIssuedDueToMigration());

        membership.setExpirationDate(dtoIntMembership.getExpirationDate());

        return ServiceResponse.data(membership).build();

    }

}

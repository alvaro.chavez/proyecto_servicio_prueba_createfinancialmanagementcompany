package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.DTOIntCashAdvancesSearchCriteria;
import com.bbva.pzic.cards.business.dto.DTOIntListCashAdvances;
import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMENDE;
import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMS1DE;
import com.bbva.pzic.cards.dao.model.mpde.FormatoMPMS2DE;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardCashAdvancesMapper;
import com.bbva.pzic.cards.facade.v0.dto.*;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Mapper("txListCardCashAdvancesMapper")
public class TxListCardCashAdvancesMapper extends ConfigurableMapper implements ITxListCardCashAdvancesMapper {

    private static final Log LOG = LogFactory.getLog(TxListCardCashAdvancesMapper.class);

    @Autowired
    private EnumMapper enumMapper;

    @Override
    public FormatoMPMENDE mapInput(DTOIntCashAdvancesSearchCriteria dtoIn) {
        return map(dtoIn, FormatoMPMENDE.class);
    }

    @Override
    public DTOIntListCashAdvances mapOutput(FormatoMPMS1DE formatOutput, DTOIntListCashAdvances dtoOut) {

        if (dtoOut.getData() == null) {
            dtoOut.setData(new ArrayList<>());
        }

        CashAdvances cashAdvance = map(formatOutput, CashAdvances.class);
        try {
            if (formatOutput.getFecinic() != null && !StringUtils.isEmpty(formatOutput.getHorinic())) {
                cashAdvance.setApplyDate(DateUtils.toDateTime(formatOutput.getFecinic(), formatOutput.getHorinic()));
            }
            if (formatOutput.getFeccont() != null && !StringUtils.isEmpty(formatOutput.getHorcont())) {
                cashAdvance.setAccountingDate(DateUtils.toDateTime(formatOutput.getFeccont(), formatOutput.getHorcont()));
            }
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }

        this.mapOutEnums(formatOutput, cashAdvance);
        dtoOut.getData().add(cashAdvance);
        return dtoOut;

    }

    @Override
    public DTOIntListCashAdvances mapOutput2(FormatoMPMS2DE formatOutput, DTOIntListCashAdvances dtoOut) {
        CashAdvances cashAdvances = getLastCashAdvance(dtoOut.getData());

        if(cashAdvances.getRates()== null){
            cashAdvances.setRates(new ArrayList<Rate>());
        }

        Rate rate = map(formatOutput,Rate.class);

        if (formatOutput.getIdmodta() != null){
            if (rate.getMode() == null) {
                rate.setMode(new Mode());
            }
            rate.getMode().setId(enumMapper.getEnumValue("cards.rates.mode",formatOutput.getIdmodta()));
            Unit unit;
            if("PERCENTAGE".equals(rate.getMode().getId())) {
                unit = new Unit();
                unit.setPercentage(formatOutput.getValtasa());
            } else if("AMOUNT".equals(rate.getMode().getId())) {
                unit = new Unit();
                unit.setAmount(formatOutput.getIntdeut());
                unit.setCurrency(formatOutput.getMontasa());
            } else {
                LOG.warn(String.format("No se tiene definido un mapeo para la unidad %s", rate.getMode().getId()));
                unit = null;
            }

            rate.setUnit(unit);
        }

       cashAdvances.getRates().add(rate);

       return dtoOut;
    }

    private void mapOutEnums(final FormatoMPMS1DE formatOutput, final CashAdvances cashAdvance1) {
        if(formatOutput.getFreccuo()!= null){
            if (cashAdvance1.getTerms() == null) {
                cashAdvance1.setTerms(new Terms());
            }
            cashAdvance1.getTerms().setFrequency(enumMapper.getEnumValue("cards.terms.frequency",formatOutput.getFreccuo()));
       }
    }

    private CashAdvances getLastCashAdvance(List<CashAdvances> cashAdvances) {
        return cashAdvances.get(cashAdvances.size() - 1);
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(DTOIntCashAdvancesSearchCriteria.class, FormatoMPMENDE.class)
                .field("cardId","nrotarj")
                .register();

        factory.classMap(FormatoMPMS1DE.class,CashAdvances.class)
                .field("idendep","id")
                .field("descdep","description")
                .field("fechfin","maturityDate")
                .field("mtosoli","requestedAmount.amount")
                .field("monedep","requestedAmount.currency")
                .field("deudmes","installmentAmount.amount")
                .field("monedep","installmentAmount.currency")
                .field("capcuom","installmentAmount.breakDown.principal.amount")
                .field("monedep","installmentAmount.breakDown.principal.currency")
                .field("intcuom","installmentAmount.breakDown.rates.amount")
                .field("monedep","installmentAmount.breakDown.rates.currency")
                .field("fecvenc","currentInstallment.paymentDate")
                .field("totdeud","total.amount")
                .field("monedep","total.currency")
                .field("montotf","paidAmount.total.amount")
                .field("monedep","paidAmount.total.currency")
                .field("capabon","paidAmount.principal.amount")
                .field("monedep","paidAmount.principal.currency")
                .field("intabon","paidAmount.rates.amount")
                .field("monedep","paidAmount.rates.currency")
                .field("totcanc","pendingAmount.total.amount")
                .field("monedep","pendingAmount.total.currency")
                .field("tcuopen","pendingAmount.principal.amount")
                .field("monedep","pendingAmount.principal.currency")
                .field("interes","pendingAmount.rates.amount")
                .field("monedep","pendingAmount.rates.currency")
                .field("numcuot","terms.number")
                .field("cuopdte","terms.pending")
                .field("cuopaga","terms.paid")
                .field("cuovnpa","terms.unpaid")
                .field("freccuo","terms.frequency")
                .field("ideofea","offerId")
                .register();

        factory.classMap(FormatoMPMS2DE.class,Rate.class)
                .field("idettas","rateType.id")
                .field("desttas","rateType.name")
                .field("fecctas","calculationDate")
                .field("idmodta","mode.id")
                .field("nommod","mode.name")
               .register();
    }
}

package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardTransaction;
import com.bbva.pzic.cards.canonic.Transaction;
import com.bbva.pzic.cards.canonic.TransactionData;

/**
 * Created on 22/12/2017.
 *
 * @author Entelgy
 */
public interface IGetCardTransactionMapper {
    InputGetCardTransaction mapIn(String cardId, String transactionId);

    ServiceResponse<Transaction> mapOut(TransactionData transactionData);
}

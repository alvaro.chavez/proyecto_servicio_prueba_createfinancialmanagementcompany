package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.pzic.cards.business.dto.DTOIntDelivery;
import com.bbva.pzic.cards.facade.v1.dto.Delivery;

/**
 * Created on 17/12/2019.
 *
 * @author Entelgy
 */
public interface ICreateCardDeliveryMapper {

    DTOIntDelivery mapIn(String cardId, Delivery delivery);

    ServiceResponseCreated<Delivery> mapOut(Delivery delivery);
}

package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "offerGenerate", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "offerGenerate", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class OfferGenerate implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Questions used to assess whether the client or non-client qualify for any product offer from the bank.
     * These  questions could change according to the business needs in order to maximize profits and provide more attractive rates to customers or non customers.
     */
    private List<Question> questions;
    /**
     * Dictum of the generation of card offers.
     */
    private Dictum dictum;
    /**
     * List of cards that the customer or non-customer qualifies after their evaluation.
     */
    private List<CardProduct> cardProducts;

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Dictum getDictum() {
        return dictum;
    }

    public void setDictum(Dictum dictum) {
        this.dictum = dictum;
    }

    public List<CardProduct> getCardProducts() {
        return cardProducts;
    }

    public void setCardProducts(List<CardProduct> cardProducts) {
        this.cardProducts = cardProducts;
    }
}

package com.bbva.pzic.cards.business.dto;

import com.bbva.pzic.cards.canonic.HolderSimulation;
import java.util.List;

/**
 * Created on 19/02/2020.
 *
 * @author Entelgy
 */
public class DTOOutListHolderSimulation {

    private List<HolderSimulation> data;
    private DTOIntPagination pagination;

    public List<HolderSimulation> getData() {
        return data;
    }

    public void setData(List<HolderSimulation> data) {
        this.data = data;
    }

    public DTOIntPagination getPagination() {
        return pagination;
    }

    public void setPagination(DTOIntPagination pagination) {
        this.pagination = pagination;
    }
}

package com.bbva.pzic.cards.dao.model.mpg2;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPG2</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpg2</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpg2</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPG2.D1171005.txt
 * MPG2ACTIVACIONES RELACIONADAS A UN TJ  MP        MP2CMPG2PBDMPPO MPMENG2             MPG2  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2017-10-04XP85978 2017-10-0515.18.51P014658 2017-10-04-16.15.54.290166XP85978 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENG2.D1171005.txt
 * MPMENG2 �ACT. OPERA. RELACIONADO A TJ  �F�01�00019�01�00001�IDETARJ�NRO.TARJETA CLIENTE �A�019�0�R�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1G2.D1171005.txt
 * MPMS1G2 �ACT. OPERA. RELACIONADO A TJ  �X�03�00028�01�00001�CODACTV�IND. BLOQUEO OPE    �A�002�0�S�        �
 * MPMS1G2 �ACT. OPERA. RELACIONADO A TJ  �X�03�00028�02�00003�DESACTV�DESC. IND. BLOQUEO  �A�025�0�S�        �
 * MPMS1G2 �ACT. OPERA. RELACIONADO A TJ  �X�03�00028�03�00028�INVACTV�IND. PARA HABILITAR �A�001�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPG2.D1171005.txt
 * MPG2MPMS1G2 MPNCS1G2MP2CMPG21S                             P014658 2017-10-05-12.37.19.891602P014658 2017-10-05-12.37.19.891662
 *
</pre></code>
 *
 * @see RespuestaTransaccionMpg2
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPG2",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpg2.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENG2.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpg2 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
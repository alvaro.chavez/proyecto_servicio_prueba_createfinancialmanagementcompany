package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "feesOffer", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "feesOffer", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class FeesOffer implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Detail of each fee.
     */
    private List<ItemizeFee> itemizeFees;

    public List<ItemizeFee> getItemizeFees() {
        return itemizeFees;
    }

    public void setItemizeFees(List<ItemizeFee> itemizeFees) {
        this.itemizeFees = itemizeFees;
    }
}

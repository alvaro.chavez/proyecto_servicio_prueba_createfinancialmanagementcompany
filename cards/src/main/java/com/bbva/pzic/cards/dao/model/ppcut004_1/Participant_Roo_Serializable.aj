// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.pzic.cards.dao.model.ppcut004_1.Participant;
import java.io.Serializable;

privileged aspect Participant_Roo_Serializable {
    
    declare parents: Participant implements Serializable;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private static final long Participant.serialVersionUID = 1L;
    
}

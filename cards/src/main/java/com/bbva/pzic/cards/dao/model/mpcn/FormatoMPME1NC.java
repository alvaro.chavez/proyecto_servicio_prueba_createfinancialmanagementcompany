package com.bbva.pzic.cards.dao.model.mpcn;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPME1NC</code> de la transacci&oacute;n <code>MPCN</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPME1NC")
@RooJavaBean
@RooSerializable
public class FormatoMPME1NC {

	/**
	 * <p>Campo <code>CODTAR</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 1, nombre = "CODTAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	private String codtar;
	
	/**
	 * <p>Campo <code>CODRAZ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "CODRAZ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String codraz;
	
}
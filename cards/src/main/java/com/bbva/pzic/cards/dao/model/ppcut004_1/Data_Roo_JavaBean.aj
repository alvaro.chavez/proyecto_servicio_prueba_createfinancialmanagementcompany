// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.pzic.cards.dao.model.ppcut004_1.Additionalproducts;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Cardtype;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Contactability;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Data;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Deliveries;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Fees;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Grantedcredits;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Image;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Membership;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Participants;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Paymentmethod;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Physicalsupport;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Product;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Rates;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Specificcontact;
import java.util.Calendar;
import java.util.List;

privileged aspect Data_Roo_JavaBean {
    
    /**
     * Gets id value
     * 
     * @return String
     */
    public String Data.getId() {
        return this.id;
    }
    
    /**
     * Sets id value
     * 
     * @param id
     * @return Data
     */
    public Data Data.setId(String id) {
        this.id = id;
        return this;
    }
    
    /**
     * Gets cardtype value
     * 
     * @return Cardtype
     */
    public Cardtype Data.getCardtype() {
        return this.cardtype;
    }
    
    /**
     * Sets cardtype value
     * 
     * @param cardtype
     * @return Data
     */
    public Data Data.setCardtype(Cardtype cardtype) {
        this.cardtype = cardtype;
        return this;
    }
    
    /**
     * Gets product value
     * 
     * @return Product
     */
    public Product Data.getProduct() {
        return this.product;
    }
    
    /**
     * Sets product value
     * 
     * @param product
     * @return Data
     */
    public Data Data.setProduct(Product product) {
        this.product = product;
        return this;
    }
    
    /**
     * Gets physicalsupport value
     * 
     * @return Physicalsupport
     */
    public Physicalsupport Data.getPhysicalsupport() {
        return this.physicalsupport;
    }
    
    /**
     * Sets physicalsupport value
     * 
     * @param physicalsupport
     * @return Data
     */
    public Data Data.setPhysicalsupport(Physicalsupport physicalsupport) {
        this.physicalsupport = physicalsupport;
        return this;
    }
    
    /**
     * Gets deliveries value
     * 
     * @return List
     */
    public List<Deliveries> Data.getDeliveries() {
        return this.deliveries;
    }
    
    /**
     * Sets deliveries value
     * 
     * @param deliveries
     * @return Data
     */
    public Data Data.setDeliveries(List<Deliveries> deliveries) {
        this.deliveries = deliveries;
        return this;
    }
    
    /**
     * Gets paymentmethod value
     * 
     * @return Paymentmethod
     */
    public Paymentmethod Data.getPaymentmethod() {
        return this.paymentmethod;
    }
    
    /**
     * Sets paymentmethod value
     * 
     * @param paymentmethod
     * @return Data
     */
    public Data Data.setPaymentmethod(Paymentmethod paymentmethod) {
        this.paymentmethod = paymentmethod;
        return this;
    }
    
    /**
     * Gets grantedcredits value
     * 
     * @return List
     */
    public List<Grantedcredits> Data.getGrantedcredits() {
        return this.grantedcredits;
    }
    
    /**
     * Sets grantedcredits value
     * 
     * @param grantedcredits
     * @return Data
     */
    public Data Data.setGrantedcredits(List<Grantedcredits> grantedcredits) {
        this.grantedcredits = grantedcredits;
        return this;
    }
    
    /**
     * Gets specificcontact value
     * 
     * @return Specificcontact
     */
    public Specificcontact Data.getSpecificcontact() {
        return this.specificcontact;
    }
    
    /**
     * Sets specificcontact value
     * 
     * @param specificcontact
     * @return Data
     */
    public Data Data.setSpecificcontact(Specificcontact specificcontact) {
        this.specificcontact = specificcontact;
        return this;
    }
    
    /**
     * Gets rates value
     * 
     * @return Rates
     */
    public Rates Data.getRates() {
        return this.rates;
    }
    
    /**
     * Sets rates value
     * 
     * @param rates
     * @return Data
     */
    public Data Data.setRates(Rates rates) {
        this.rates = rates;
        return this;
    }
    
    /**
     * Gets fees value
     * 
     * @return Fees
     */
    public Fees Data.getFees() {
        return this.fees;
    }
    
    /**
     * Sets fees value
     * 
     * @param fees
     * @return Data
     */
    public Data Data.setFees(Fees fees) {
        this.fees = fees;
        return this;
    }
    
    /**
     * Gets additionalproducts value
     * 
     * @return List
     */
    public List<Additionalproducts> Data.getAdditionalproducts() {
        return this.additionalproducts;
    }
    
    /**
     * Sets additionalproducts value
     * 
     * @param additionalproducts
     * @return Data
     */
    public Data Data.setAdditionalproducts(List<Additionalproducts> additionalproducts) {
        this.additionalproducts = additionalproducts;
        return this;
    }
    
    /**
     * Gets membership value
     * 
     * @return Membership
     */
    public Membership Data.getMembership() {
        return this.membership;
    }
    
    /**
     * Sets membership value
     * 
     * @param membership
     * @return Data
     */
    public Data Data.setMembership(Membership membership) {
        this.membership = membership;
        return this;
    }
    
    /**
     * Gets operationdate value
     * 
     * @return Calendar
     */
    public Calendar Data.getOperationdate() {
        return this.operationdate;
    }
    
    /**
     * Sets operationdate value
     * 
     * @param operationdate
     * @return Data
     */
    public Data Data.setOperationdate(Calendar operationdate) {
        this.operationdate = operationdate;
        return this;
    }
    
    /**
     * Gets status value
     * 
     * @return String
     */
    public String Data.getStatus() {
        return this.status;
    }
    
    /**
     * Sets status value
     * 
     * @param status
     * @return Data
     */
    public Data Data.setStatus(String status) {
        this.status = status;
        return this;
    }
    
    /**
     * Gets image value
     * 
     * @return Image
     */
    public Image Data.getImage() {
        return this.image;
    }
    
    /**
     * Sets image value
     * 
     * @param image
     * @return Data
     */
    public Data Data.setImage(Image image) {
        this.image = image;
        return this;
    }
    
    /**
     * Gets offerid value
     * 
     * @return String
     */
    public String Data.getOfferid() {
        return this.offerid;
    }
    
    /**
     * Sets offerid value
     * 
     * @param offerid
     * @return Data
     */
    public Data Data.setOfferid(String offerid) {
        this.offerid = offerid;
        return this;
    }
    
    /**
     * Gets contactability value
     * 
     * @return Contactability
     */
    public Contactability Data.getContactability() {
        return this.contactability;
    }
    
    /**
     * Sets contactability value
     * 
     * @param contactability
     * @return Data
     */
    public Data Data.setContactability(Contactability contactability) {
        this.contactability = contactability;
        return this;
    }
    
    /**
     * Gets participants value
     * 
     * @return List
     */
    public List<Participants> Data.getParticipants() {
        return this.participants;
    }
    
    /**
     * Sets participants value
     * 
     * @param participants
     * @return Data
     */
    public Data Data.setParticipants(List<Participants> participants) {
        this.participants = participants;
        return this;
    }

    /**
     * Gets notificationsbyoperation value
     *
     * @return Boolean
     */
    public Boolean Data.getNotificationsbyoperation() {
        return this.notificationsbyoperation;
    }

    /**
     * Sets notificationsbyoperation value
     *
     * @param notificationsbyoperation
     * @return Data
     */
    public Data Data.setNotificationsbyoperation(Boolean notificationsbyoperation) {
        this.notificationsbyoperation = notificationsbyoperation;
        return this;
    }

}

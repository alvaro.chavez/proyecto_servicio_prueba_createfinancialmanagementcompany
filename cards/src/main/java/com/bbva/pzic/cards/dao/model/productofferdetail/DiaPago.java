package com.bbva.pzic.cards.dao.model.productofferdetail;

public class DiaPago {

    private Integer valor;

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
}

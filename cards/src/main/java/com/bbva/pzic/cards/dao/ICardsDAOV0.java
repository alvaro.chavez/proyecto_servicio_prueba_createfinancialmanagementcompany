package com.bbva.pzic.cards.dao;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.facade.v0.dto.*;

import java.util.List;

/**
 * @author Entelgy
 */
public interface ICardsDAOV0 {

    /**
     * Method that communicates with HOST
     *
     * @param dtoIn Object with the data of input
     * @return with the data of output
     */
    com.bbva.pzic.cards.canonic.PaymentMethod getCardPaymentMethods(DTOIntCard dtoIn);

    DTOOutListCards listCardsV0(DTOIntListCards dtoIn);

    List<Limits> listCardLimits(DTOIntCard dtoIn);

    Card getCard(DTOIntCard dtoIn);

    void listImagesCovers(DTOOutListCards dtoOut);

    Limit modifyCardLimit(InputModifyCardLimit modifyCardLimit);

    TransactionData getCardTransaction(InputGetCardTransaction input);

    Card createCard(InputCreateCard input);

    Card createCardAdvance(InputCreateCard inputCreateCard);

    DTOInstallmentsPlanList listInstallmentPlans(InputListInstallmentPlans input);

    InstallmentsPlanData createInstallmentsPlan(DTOIntCardInstallmentsPlan dtoIn);

    void modifyCard(DTOIntCard dtoIn);

    InstallmentsPlanSimulationData simulateInstallmentsPlan(DTOIntCardInstallmentsPlan dtoIn);

    List<Condition> getCardConditions(InputGetCardConditions input);

    DTOIntStatementList listCardFinancialStatements(InputListCardFinancialStatements input);

    DTOIntCardStatement getCardFinancialStatement(InputGetCardFinancialStatement input);

    Document getCardFinancialStatementDocument(DocumentRequest documentRequest);

    MaskedToken createCardsMaskedToken(InputCreateCardsMaskedToken input);

    DTOIntMembershipServiceResponse getCardMembership(DTOIntSearchCriteria searchCriteria);

    Offer getCardsCardOffer(InputGetCardsCardOffer input);

    HolderSimulation createCardsOfferSimulate(DTOIntDetailSimulation input);

    void createImageCover(HolderSimulation holderSimulation);

    Proposal createCardsProposal(DTOIntProposal dtoIntProposal);

    List<RelatedContracts> listCardRelatedContracts(DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria);

    DTOIntListCashAdvances listCardCashAdvances(DTOIntCashAdvancesSearchCriteria dtoIntCashAdvancesSearchCriteria);

    DTOIntCashRefund createCardCashRefund(DTOInputCreateCashRefund dtoInputCreateCashRefund);

    void createCardReports(InputCreateCardReports input);

    OfferGenerate createOffersGenerateCards(InputCreateOffersGenerateCards input);

}

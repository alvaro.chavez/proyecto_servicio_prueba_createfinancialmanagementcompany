package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.dao.model.mpb4.FormatoMPM0B4E;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyPartialCardBlockMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

/**
 * Created on 29/08/2017.
 *
 * @author Entelgy
 */
@Component
public class TxModifyPartialCardBlockMapper implements ITxModifyPartialCardBlockMapper {

    private static final Log LOG = LogFactory.getLog(TxModifyPartialCardBlockMapper.class);

    @Override
    public FormatoMPM0B4E mapIn(final DTOIntBlock dtoIntBlock) {
        LOG.info("... called method TxModifyPartialCardBlockMapper.mapIn ...");
        final FormatoMPM0B4E formatoMPM0B4E = new FormatoMPM0B4E();
        formatoMPM0B4E.setIdetarj(dtoIntBlock.getCard().getCardId());
        formatoMPM0B4E.setIdebloq(dtoIntBlock.getBlockId());
        formatoMPM0B4E.setIderazo(dtoIntBlock.getReasonId());
        formatoMPM0B4E.setIdactbl(dtoIntBlock.getIsActive());

        return formatoMPM0B4E;
    }
}

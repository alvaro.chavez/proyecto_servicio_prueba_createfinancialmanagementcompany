package com.bbva.pzic.cards.dao.model.cardreports;

import java.util.List;

public class ModelCardReport {

    private String id;

    private String canal;

    private ModelTipoTarjeta tipoTarjeta;

    private ModelTipoNumeroTarjeta tipoNumeroTarjeta;

    private String numeroProducto;

    private ModelMarca marca;

    private ModelProducto producto;

    private ModelSubproducto subproducto;

    private ModelImporte importe;

    private Integer flagMonedaPrincipal;

    private Integer flagInnominada;

    private ModelCliente cliente;

    private ModelDatosPago datosPago;

    private String cuentaCargo;

    private ModelTipoCuentaCargo tipoCuentaCargo;

    private List<ModelDatoEntrega> datosEntrega;

    private ModelOficina oficina;

    private ModelProgramaBeneficios programaBeneficios;

    private Integer estado;

    private ModelOferta oferta;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public ModelTipoTarjeta getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(ModelTipoTarjeta tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public ModelTipoNumeroTarjeta getTipoNumeroTarjeta() {
        return tipoNumeroTarjeta;
    }

    public void setTipoNumeroTarjeta(ModelTipoNumeroTarjeta tipoNumeroTarjeta) {
        this.tipoNumeroTarjeta = tipoNumeroTarjeta;
    }

    public String getNumeroProducto() {
        return numeroProducto;
    }

    public void setNumeroProducto(String numeroProducto) {
        this.numeroProducto = numeroProducto;
    }

    public ModelMarca getMarca() {
        return marca;
    }

    public void setMarca(ModelMarca marca) {
        this.marca = marca;
    }

    public ModelProducto getProducto() {
        return producto;
    }

    public void setProducto(ModelProducto producto) {
        this.producto = producto;
    }

    public ModelSubproducto getSubproducto() {
        return subproducto;
    }

    public void setSubproducto(ModelSubproducto subproducto) {
        this.subproducto = subproducto;
    }

    public ModelImporte getImporte() {
        return importe;
    }

    public void setImporte(ModelImporte importe) {
        this.importe = importe;
    }

    public Integer getFlagMonedaPrincipal() {
        return flagMonedaPrincipal;
    }

    public void setFlagMonedaPrincipal(Integer flagMonedaPrincipal) {
        this.flagMonedaPrincipal = flagMonedaPrincipal;
    }

    public Integer getFlagInnominada() {
        return flagInnominada;
    }

    public void setFlagInnominada(Integer flagInnominada) {
        this.flagInnominada = flagInnominada;
    }

    public ModelCliente getCliente() {
        return cliente;
    }

    public void setCliente(ModelCliente cliente) {
        this.cliente = cliente;
    }

    public ModelDatosPago getDatosPago() {
        return datosPago;
    }

    public void setDatosPago(ModelDatosPago datosPago) {
        this.datosPago = datosPago;
    }

    public String getCuentaCargo() {
        return cuentaCargo;
    }

    public void setCuentaCargo(String cuentaCargo) {
        this.cuentaCargo = cuentaCargo;
    }

    public ModelTipoCuentaCargo getTipoCuentaCargo() {
        return tipoCuentaCargo;
    }

    public void setTipoCuentaCargo(ModelTipoCuentaCargo tipoCuentaCargo) {
        this.tipoCuentaCargo = tipoCuentaCargo;
    }

    public List<ModelDatoEntrega> getDatosEntrega() {
        return datosEntrega;
    }

    public void setDatosEntrega(List<ModelDatoEntrega> datosEntrega) {
        this.datosEntrega = datosEntrega;
    }

    public ModelOficina getOficina() {
        return oficina;
    }

    public void setOficina(ModelOficina oficina) {
        this.oficina = oficina;
    }

    public ModelProgramaBeneficios getProgramaBeneficios() {
        return programaBeneficios;
    }

    public void setProgramaBeneficios(ModelProgramaBeneficios programaBeneficios) {
        this.programaBeneficios = programaBeneficios;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public ModelOferta getOferta() {
        return oferta;
    }

    public void setOferta(ModelOferta oferta) {
        this.oferta = oferta;
    }
}

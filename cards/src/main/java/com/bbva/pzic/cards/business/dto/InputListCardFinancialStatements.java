package com.bbva.pzic.cards.business.dto;


import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

/**
 * Created on 17/07/2018.
 *
 * @author Entelgy
 */
public class InputListCardFinancialStatements {

    @Size(max = 16, groups = ValidationGroup.ListCardFinancialStatementsV0.class)
    private String cardId;
    private Boolean isActive;
    @Size(max = 6, groups = ValidationGroup.ListCardFinancialStatementsV0.class)
    private String paginationKey;
    @Digits(integer = 3, fraction = 0, groups = ValidationGroup.ListCardFinancialStatementsV0.class)
    private Integer pageSize;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getPaginationKey() {
        return paginationKey;
    }

    public void setPaginationKey(String paginationKey) {
        this.paginationKey = paginationKey;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
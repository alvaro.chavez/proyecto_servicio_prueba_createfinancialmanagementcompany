package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancelRequest;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateCardCancelRequestV1Mapper;
import com.bbva.pzic.cards.dao.model.pcpst007_1.PeticionTransaccionPcpst007_1;
import com.bbva.pzic.cards.dao.model.pcpst007_1.RespuestaTransaccionPcpst007_1;
import com.bbva.pzic.cards.facade.v1.dto.RequestCancel;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class ApxCreateCardCancelRequestV1 {

    @Resource(name = "apxCreateCardCancelRequestV1Mapper")
    private IApxCreateCardCancelRequestV1Mapper mapper;

    @Resource(name = "transaccionPcpst007_1")
    private transient InvocadorTransaccion<PeticionTransaccionPcpst007_1, RespuestaTransaccionPcpst007_1> transaccion;

    public RequestCancel perform(final InputCreateCardCancelRequest input) {
        PeticionTransaccionPcpst007_1 request = mapper.mapIn(input);
        RespuestaTransaccionPcpst007_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}

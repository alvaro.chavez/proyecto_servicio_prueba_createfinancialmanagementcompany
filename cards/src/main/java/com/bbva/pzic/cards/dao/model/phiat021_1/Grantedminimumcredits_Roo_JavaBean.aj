// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.phiat021_1;

import com.bbva.pzic.cards.dao.model.phiat021_1.Grantedminimumcredit;
import com.bbva.pzic.cards.dao.model.phiat021_1.Grantedminimumcredits;

privileged aspect Grantedminimumcredits_Roo_JavaBean {
    
    /**
     * Gets grantedminimumcredit value
     * 
     * @return Grantedminimumcredit
     */
    public Grantedminimumcredit Grantedminimumcredits.getGrantedminimumcredit() {
        return this.grantedminimumcredit;
    }
    
    /**
     * Sets grantedminimumcredit value
     * 
     * @param grantedminimumcredit
     * @return Grantedminimumcredits
     */
    public Grantedminimumcredits Grantedminimumcredits.setGrantedminimumcredit(Grantedminimumcredit grantedminimumcredit) {
        this.grantedminimumcredit = grantedminimumcredit;
        return this;
    }
    
}

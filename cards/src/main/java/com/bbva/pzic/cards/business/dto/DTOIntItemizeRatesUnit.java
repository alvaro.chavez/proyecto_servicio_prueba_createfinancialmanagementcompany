package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
public class DTOIntItemizeRatesUnit {

    @NotNull(groups = {ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    private String unitRateType;
    @NotNull(groups = {ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    private BigDecimal percentage;

    public String getUnitRateType() {
        return unitRateType;
    }

    public void setUnitRateType(String unitRateType) {
        this.unitRateType = unitRateType;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
}

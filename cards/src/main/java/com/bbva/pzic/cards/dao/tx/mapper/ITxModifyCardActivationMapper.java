package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.dao.model.mpb1.FormatoMPM0BX;

/**
 * @author Entelgy
 */
public interface ITxModifyCardActivationMapper {

    /**
     * Crea una nueva instancia de {@link FormatoMPM0BX} y la inicializa con el parámetro enviado.
     *
     * @param dtoIntActivation
     * @return the created object.
     */
    FormatoMPM0BX mapInput(DTOIntActivation dtoIntActivation);

}

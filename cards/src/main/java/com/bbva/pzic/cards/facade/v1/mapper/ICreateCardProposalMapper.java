package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.pzic.cards.business.dto.InputCreateCardProposal;
import com.bbva.pzic.cards.facade.v1.dto.ProposalCard;

/**
 * Created on 19/11/2019.
 *
 * @author Entelgy
 */
public interface ICreateCardProposalMapper {

    InputCreateCardProposal mapIn(ProposalCard proposal);

    ServiceResponseCreated<ProposalCard> mapOut(ProposalCard cardProposal);
}

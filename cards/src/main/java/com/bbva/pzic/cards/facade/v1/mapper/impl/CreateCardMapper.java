package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.jee.arq.spring.core.managers.InputHeaderManager;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.facade.v1.mapper.ICreateCardMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.bbva.pzic.cards.util.Constants.*;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
@Mapper("createCardMapperV1")
public class CreateCardMapper implements ICreateCardMapper {

    private static final Log LOG = LogFactory.getLog(CreateCardMapper.class);

    private final InputHeaderManager inputHeaderManager;
    private final Translator translator;

    public CreateCardMapper(InputHeaderManager inputHeaderManager,
                            Translator translator) {
        this.inputHeaderManager = inputHeaderManager;
        this.translator = translator;
    }

    @Override
    public InputCreateCard mapIn(final CardPost cardPost) {
        LOG.info("... called method CreateCardMapper.mapIn ...");

        DTOIntCard dtoInt = new DTOIntCard();
        dtoInt.setCardTypeId(cardTypeToDTOIntCardTypeId(cardPost.getCardType()));
        dtoInt.setProductId(productToDTOIntProductId(cardPost.getProduct()));
        dtoInt.setPhysicalSupportId(physicalSupportIdToDTOIntPhysicalSupportId(cardPost.getPhysicalSupport()));
        dtoInt.setHolderName(cardPost.getHolderName());

        currenciesToDTOIntCurrencies(dtoInt, cardPost.getCurrencies());
        grantedCreditsToDTOIntGrantedCredits(dtoInt, cardPost.getGrantedCredits());

        dtoInt.setCutOffDay(cardPost.getCutOffDay());
        dtoInt.setRelatedContracts(relatedContractPostListToDTOIntRelatedContractList(cardPost.getRelatedContracts()));
        dtoInt.setImages(imagesToDTOIntImages(cardPost.getImages()));
        dtoInt.setDeliveries(deliveryPostListToDTOIntDeliveryList(cardPost.getDeliveries()));
        dtoInt.setParticipants(participantsToDTOIntParticipants(cardPost.getParticipants()));
        dtoInt.setPaymentMethod(paymentMethodPostToDTOIntPaymentMethod(cardPost.getPaymentMethod()));
        dtoInt.setSupportContractType(translateFrontendEnumIfNecessary(
                "cards.supportContractType.id", cardPost.getSupportContractType()));
        dtoInt.setMembershipsNumber(membershipsToDTOIntMemberships(cardPost.getMemberships()));
        dtoInt.setCardAgreement(cardPost.getCardAgreement());
        dtoInt.setContractingBranchId(contractingBranchIdToDTOIntContractingBranch(cardPost.getContractingBranch()));
        dtoInt.setManagementBranchId(managementBranchIdToDTOIntManagementBranch(cardPost.getManagementBranch()));
        dtoInt.setContractingBusinessAgentId(contractingBusinessAgentIdToDTOIntContractingBusinessAgent(cardPost.getContractingBusinessAgent()));
        dtoInt.setMarketBusinessAgentId(marketBusinessAgentIdToDTOIntMarketBusinessAgent(cardPost.getMarketBusinessAgent()));
        dtoInt.setBankIdentificationNumber(cardPost.getBankIdentificationNumber());

        InputCreateCard input = new InputCreateCard();
        input.setHeaderBCSOperationTracer(inputHeaderManager.getHeader(HEADER_BCS_OPERATION_TRACER));
        input.setCard(dtoInt);
        return input;
    }

    private String translateFrontendEnumIfNecessary(String key, String value) {
        String headerBCSOperationTracer = inputHeaderManager.getHeader(HEADER_BCS_OPERATION_TRACER);
        if (headerBCSOperationTracer == null) {
            return value;
        } else {
            return translator.translateFrontendEnumValueStrictly(key, value);
        }
    }

    private String cardTypeToDTOIntCardTypeId(final CardTypePost cardType) {
        if (cardType == null) {
            return null;
        }
        return translateFrontendEnumIfNecessary(
                "cards.cardType.id", cardType.getId());
    }

    private String productToDTOIntProductId(final Product product) {
        if (product == null) {
            return null;
        }
        return product.getId();
    }

    private String physicalSupportIdToDTOIntPhysicalSupportId(final PhysicalSupportPost physicalSupport) {
        if (physicalSupport == null) {
            return null;
        }
        return translateFrontendEnumIfNecessary(
                "cards.physicalSupport.id", physicalSupport.getId());
    }

    private void currenciesToDTOIntCurrencies(DTOIntCard dtoInt, List<Currency> currencies) {
        if (CollectionUtils.isNotEmpty(currencies)) {
            dtoInt.setCurrenciesCurrency(currencies.get(0).getCurrency());
            dtoInt.setCurrenciesIsMajor(currencies.get(0).getIsMajor());
        }
    }

    private void grantedCreditsToDTOIntGrantedCredits(DTOIntCard dtoInt, List<Amount> grantedCredits) {
        if (CollectionUtils.isNotEmpty(grantedCredits)) {
            dtoInt.setGrantedCreditsAmount(grantedCredits.get(0).getAmount());
            dtoInt.setGrantedCreditsCurrency(grantedCredits.get(0).getCurrency());
        }
    }

    private DTOIntRelationType relationTypeToDTOIntRelationType(RelationType relationType) {
        if (relationType == null) {
            return null;
        }

        DTOIntRelationType dtoIntRelationType = new DTOIntRelationType();

        dtoIntRelationType.setId(translateFrontendEnumIfNecessary(
                "cards.relatedContracts.relationType", relationType.getId()));

        return dtoIntRelationType;
    }

    private DTOIntProductType productTypeToDTOIntProductType(ProductType productType) {
        if (productType == null) {
            return null;
        }

        DTOIntProductType dtoIntProductType = new DTOIntProductType();

        dtoIntProductType.setId(translateFrontendEnumIfNecessary(
                "cards.relatedContracts.productType", productType.getId()));

        return dtoIntProductType;
    }

    private DTOIntSubProduct subproductToDTOIntSubProduct(Subproduct subproduct) {
        if (subproduct == null) {
            return null;
        }

        DTOIntSubProduct dtoIntSubProduct = new DTOIntSubProduct();

        dtoIntSubProduct.setId(subproduct.getId());

        return dtoIntSubProduct;
    }

    private DTOIntProduct productToDTOIntProduct(Product product) {
        if (product == null) {
            return null;
        }

        DTOIntProduct dtoIntProduct = new DTOIntProduct();

        dtoIntProduct.setId(product.getId());
        dtoIntProduct.setName(product.getName());
        dtoIntProduct.setProductType(productTypeToDTOIntProductType(product.getProductType()));
        dtoIntProduct.setSubproduct(subproductToDTOIntSubProduct(product.getSubproduct()));

        return dtoIntProduct;
    }

    private DTOIntRelatedContract relatedContractPostToDTOIntRelatedContract(RelatedContractPost relatedContractPost) {
        if (relatedContractPost == null) {
            return null;
        }

        DTOIntRelatedContract dtoIntRelatedContract = new DTOIntRelatedContract();

        dtoIntRelatedContract.setContractId(relatedContractPost.getContractId());
        dtoIntRelatedContract.setRelationType(relationTypeToDTOIntRelationType(relatedContractPost.getRelationType()));
        dtoIntRelatedContract.setProduct(productToDTOIntProduct(relatedContractPost.getProduct()));

        return dtoIntRelatedContract;
    }

    private List<DTOIntRelatedContract> relatedContractPostListToDTOIntRelatedContractList(List<RelatedContractPost> list) {
        if (list == null) {
            return null;
        }

        List<DTOIntRelatedContract> list1 = new ArrayList<>(list.size());
        for (RelatedContractPost relatedContractPost : list) {
            list1.add(relatedContractPostToDTOIntRelatedContract(relatedContractPost));
        }

        return list1;
    }

    private List<DTOIntImage> imagesToDTOIntImages(final List<Image> images) {
        if (CollectionUtils.isEmpty(images)) {
            return null;
        }
        return images.stream().map(this::imageToDTOIntImage).collect(Collectors.toList());
    }

    private DTOIntImage imageToDTOIntImage(final Image image) {
        if (image == null) {
            return null;
        }
        DTOIntImage dtoInt = new DTOIntImage();
        dtoInt.setId(image.getId());
        return dtoInt;
    }

    private DTOIntParticipant participantToDTOIntParticipant(final Participant participant) {
        if (participant == null) {
            return null;
        }

        DTOIntParticipant dtoIntParticipant = new DTOIntParticipant();
        dtoIntParticipant.setPersonType(participant.getPersonType());

        if ("KNOW".equals(participant.getPersonType())) {
            dtoIntParticipant.setId(participant.getId());

            if (participant.getParticipantType() != null) {
                dtoIntParticipant.setParticipantTypeId(translateFrontendEnumIfNecessary(
                        "cards.participants.participantType", participant.getParticipantType().getId()));
            }
            if (participant.getLegalPersonType() != null) {
                dtoIntParticipant.setLegalPersonTypeId(translateFrontendEnumIfNecessary(
                        "cards.participants.legalPersonType", participant.getLegalPersonType().getId()));
            }
        }

        return dtoIntParticipant;
    }

    private List<DTOIntParticipant> participantsToDTOIntParticipants(final List<Participant> participants) {
        if (CollectionUtils.isEmpty(participants)) {
            return null;
        }

        List<DTOIntParticipant> dtoIntParticipants = new ArrayList<>(participants.size());
        for (Participant participant : participants) {
            dtoIntParticipants.add(participantToDTOIntParticipant(participant));
        }

        return dtoIntParticipants;
    }

    private DTOIntPaymentMethod paymentMethodPostToDTOIntPaymentMethod(final PaymentMethodPost paymentMethodPost) {
        if (paymentMethodPost == null) {
            return null;
        }

        DTOIntPaymentMethod dtoIntPaymentMethod = new DTOIntPaymentMethod();

        dtoIntPaymentMethod.setId(translateFrontendEnumIfNecessary(
                "cards.paymentMethod.id", paymentMethodPost.getId()));
        dtoIntPaymentMethod.setFrequency(frecuencyToDTOIntFrequency(paymentMethodPost.getFrecuency()));

        return dtoIntPaymentMethod;
    }

    private DTOIntFrequency frecuencyToDTOIntFrequency(final FrecuencyPost frequency) {
        if (frequency == null) {
            return null;
        }

        DTOIntFrequency dtoIntFrequency = new DTOIntFrequency();
        dtoIntFrequency.setId(translateFrontendEnumIfNecessary(
                "cards.paymentMethod.frecuency", frequency.getId()));
        dtoIntFrequency.setDaysOfMonth(daysOfMonthToDaysOfMonth(frequency.getDaysOfMonth()));
        return dtoIntFrequency;
    }

    private DTOIntDaysOfMonth daysOfMonthToDaysOfMonth(final DaysOfMonth daysOfMonth) {
        if (daysOfMonth == null) {
            return null;
        }
        DTOIntDaysOfMonth dtoIntDaysOfMonth = new DTOIntDaysOfMonth();
        dtoIntDaysOfMonth.setDay(daysOfMonth.getDay());
        return dtoIntDaysOfMonth;
    }

    private DTOIntContact contactToDTOIntContact(Contact contact) {
        if (contact == null) {
            return null;
        }

        DTOIntContact dtoIntContact = new DTOIntContact();
        dtoIntContact.setContactType(translateFrontendEnumIfNecessary(
                "cards.delivery.contactType", contact.getContactType()));
        if (STORED.equals(contact.getContactType())) {
            dtoIntContact.setId(contact.getId());
        }
        return dtoIntContact;
    }

    private DTOIntDestination destinationToDTOIntDestination(Destination destination) {
        if (destination == null) {
            return null;
        }

        DTOIntDestination dtoIntDestination = new DTOIntDestination();
        dtoIntDestination.setId(translateFrontendEnumIfNecessary(
                "cards.delivery.destination", destination.getId()));
        return dtoIntDestination;
    }

    private DTOIntDelivery deliveryPostToDTOIntDelivery(final DeliveryPost delivery) {
        if (delivery == null) {
            return null;
        }

        DTOIntDelivery dtoIntDelivery = new DTOIntDelivery();
        dtoIntDelivery.setServiceTypeId(delivery.getServiceType() == null ? null : delivery.getServiceType().getId());
        dtoIntDelivery.setContact(contactToDTOIntContact(delivery.getContact()));
        dtoIntDelivery.setDestination(destinationToDTOIntDestination(delivery.getDestination()));

        destinationIdToDelivery(dtoIntDelivery, delivery);

        return dtoIntDelivery;
    }

    private DTOIntAddress addressToDTOIntAddress(Address address) {
        if (address == null) {
            return null;
        }

        DTOIntAddress dtoIntAddress = new DTOIntAddress();
        dtoIntAddress.setAddressType(translateFrontendEnumIfNecessary(
                "cards.delivery.addressType", address.getAddressType()));
        if (STORED.equals(address.getAddressType())) {
            dtoIntAddress.setId(address.getId());
        }
        return dtoIntAddress;
    }

    private DTOIntBranch branchToDTOIntBranch(Branch branch) {
        if (branch == null) {
            return null;
        }

        DTOIntBranch dtoIntBranch = new DTOIntBranch();
        dtoIntBranch.setId(branch.getId());
        return dtoIntBranch;
    }

    private void destinationIdToDelivery(DTOIntDelivery dtoIntDelivery, DeliveryPost delivery) {
        Destination destination = delivery.getDestination();
        if (destination == null) {
            return;
        }

        if (HOME.equals(destination.getId()) ||
                CUSTOM.equals(destination.getId())) {
            dtoIntDelivery.setAddress(addressToDTOIntAddress(delivery.getAddress()));
        } else if (BRANCH.equals(destination.getId())) {
            dtoIntDelivery.getDestination().setBranch(branchToDTOIntBranch(destination.getBranch()));
        }
    }

    private List<DTOIntDelivery> deliveryPostListToDTOIntDeliveryList(List<DeliveryPost> list) {
        if (list == null) {
            return null;
        }

        List<DTOIntDelivery> list1 = new ArrayList<>(list.size());
        for (DeliveryPost deliveryPost : list) {
            list1.add(deliveryPostToDTOIntDelivery(deliveryPost));
        }

        return list1;
    }

    private String membershipsToDTOIntMemberships(final List<Membership> memberships) {
        if (CollectionUtils.isEmpty(memberships)) {
            return null;
        }
        return memberships.get(0).getNumber();
    }

    private String contractingBranchIdToDTOIntContractingBranch(final AttentionBranch contractingBranch) {
        if (contractingBranch == null) {
            return null;
        }
        return contractingBranch.getId();
    }

    private String managementBranchIdToDTOIntManagementBranch(final AttentionBranch managementBranch) {
        if (managementBranch == null) {
            return null;
        }
        return managementBranch.getId();
    }

    private String contractingBusinessAgentIdToDTOIntContractingBusinessAgent(final ContractingBusinessAgent contractingBusinessAgent) {
        if (contractingBusinessAgent == null) {
            return null;
        }
        return contractingBusinessAgent.getId();
    }

    private String marketBusinessAgentIdToDTOIntMarketBusinessAgent(final MarketBusinessAgent marketBusinessAgent) {
        if (marketBusinessAgent == null) {
            return null;
        }
        return marketBusinessAgent.getId();
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponseCreated<CardPost> mapOut(final CardPost cardPost) {
        LOG.info("... called method CreateCardMapper.mapOut ...");
        if (cardPost == null) {
            return null;
        }
        return ServiceResponseCreated.data(cardPost).build();
    }
}

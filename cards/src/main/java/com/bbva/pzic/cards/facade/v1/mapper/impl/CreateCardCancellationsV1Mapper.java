package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntReason;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancellations;
import com.bbva.pzic.cards.facade.v1.dto.Cancellation;
import com.bbva.pzic.cards.facade.v1.dto.Reason;
import com.bbva.pzic.cards.facade.v1.mapper.ICreateCardCancellationsV1Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.bbva.pzic.cards.util.Enums.CARD_CANCELLATIONS_REASONID;

@Component
public class CreateCardCancellationsV1Mapper implements ICreateCardCancellationsV1Mapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public InputCreateCardCancellations mapIn(final String cardId, final Cancellation cancellation) {
        InputCreateCardCancellations dtoInt = new InputCreateCardCancellations();
        dtoInt.setCardId(cardId);
        dtoInt.setReason(mapInReason(cancellation.getReason()));
        return dtoInt;
    }

    private DTOIntReason mapInReason(final Reason reason) {
        if (reason == null) {
            return null;
        }
        DTOIntReason dtoIntReason = new DTOIntReason();
        dtoIntReason.setId(translator.translateFrontendEnumValueStrictly(CARD_CANCELLATIONS_REASONID, reason.getId()));
        return dtoIntReason;
    }

    @Override
    public ServiceResponse<Cancellation> mapOut(final Cancellation cancellation) {
        if (cancellation == null) {
            return null;
        }
        return ServiceResponse.data(cancellation).build();
    }
}

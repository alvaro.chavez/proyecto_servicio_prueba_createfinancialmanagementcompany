package com.bbva.pzic.cards.dao.model.kb89;


import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>KTECKB89</code> de la transacci&oacute;n <code>KB89</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KTECKB89")
@RooJavaBean
@RooSerializable
public class FormatoKTECKB89 {

	/**
	 * <p>Campo <code>IDTIPTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDTIPTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtipta;
	
	/**
	 * <p>Campo <code>IDPRODU</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "IDPRODU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 6, longitudMaxima = 6)
	private String idprodu;
	
	/**
	 * <p>Campo <code>IDSPROD</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "IDSPROD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 6, longitudMaxima = 6)
	private String idsprod;
	
	/**
	 * <p>Campo <code>IDDESTI</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "IDDESTI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String iddesti;
	
	/**
	 * <p>Campo <code>IDTPAGO</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "IDTPAGO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String idtpago;
	
	/**
	 * <p>Campo <code>IDPERIO</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "IDPERIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idperio;
	
	/**
	 * <p>Campo <code>DIAPAGO</code>, &iacute;ndice: <code>7</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 7, nombre = "DIAPAGO", tipo = TipoCampo.ENTERO, longitudMinima = 2, longitudMaxima = 2)
	private Integer diapago;
	
	/**
	 * <p>Campo <code>MONLINE</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 8, nombre = "MONLINE", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, decimales = 2)
	private BigDecimal monline;
	
	/**
	 * <p>Campo <code>DIVISAL</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "DIVISAL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divisal;
	
	/**
	 * <p>Campo <code>IDTSTEA</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "IDTSTEA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idtstea;
	
	/**
	 * <p>Campo <code>TASATEA</code>, &iacute;ndice: <code>11</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 11, nombre = "TASATEA", tipo = TipoCampo.DECIMAL, longitudMinima = 5, longitudMaxima = 5, decimales = 2)
	private BigDecimal tasatea;
	
	/**
	 * <p>Campo <code>IDTTCEA</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "IDTTCEA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idttcea;
	
	/**
	 * <p>Campo <code>TASTCEA</code>, &iacute;ndice: <code>13</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 13, nombre = "TASTCEA", tipo = TipoCampo.DECIMAL, longitudMinima = 5, longitudMaxima = 5, decimales = 2)
	private BigDecimal tastcea;
	
	/**
	 * <p>Campo <code>FECTASA</code>, &iacute;ndice: <code>14</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 14, nombre = "FECTASA", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fectasa;
	
	/**
	 * <p>Campo <code>TIPTASA</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "TIPTASA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tiptasa;
	
	/**
	 * <p>Campo <code>IDCOMS1</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "IDCOMS1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idcoms1;
	
	/**
	 * <p>Campo <code>NOMCOM1</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "NOMCOM1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nomcom1;
	
	/**
	 * <p>Campo <code>IMPCOM1</code>, &iacute;ndice: <code>18</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 18, nombre = "IMPCOM1", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, decimales = 2)
	private BigDecimal impcom1;
	
	/**
	 * <p>Campo <code>IDCOMS2</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "IDCOMS2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idcoms2;
	
	/**
	 * <p>Campo <code>NOMCOM2</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "NOMCOM2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nomcom2;
	
	/**
	 * <p>Campo <code>IMPCOM2</code>, &iacute;ndice: <code>21</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 21, nombre = "IMPCOM2", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, decimales = 2)
	private BigDecimal impcom2;
	
	/**
	 * <p>Campo <code>TIPCOMI</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "TIPCOMI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipcomi;
	
	/**
	 * <p>Campo <code>PRODUCT</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 23, nombre = "PRODUCT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String product;
	
	/**
	 * <p>Campo <code>IMPPROD</code>, &iacute;ndice: <code>24</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 24, nombre = "IMPPROD", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, decimales = 2)
	private BigDecimal impprod;
	
	/**
	 * <p>Campo <code>IDBANCO</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 25, nombre = "IDBANCO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String idbanco;
	
	/**
	 * <p>Campo <code>IDOFICI</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "IDOFICI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String idofici;
	
	/**
	 * <p>Campo <code>IDPMEMB</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 27, nombre = "IDPMEMB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idpmemb;
	
	/**
	 * <p>Campo <code>NUMPMEM</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 28, nombre = "NUMPMEM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String numpmem;
	
	/**
	 * <p>Campo <code>ECENVIO</code>, &iacute;ndice: <code>29</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 29, nombre = "ECENVIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String ecenvio;
	
	/**
	 * <p>Campo <code>IDCELUL</code>, &iacute;ndice: <code>30</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 30, nombre = "IDCELUL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String idcelul;
	
	/**
	 * <p>Campo <code>IDOFERT</code>, &iacute;ndice: <code>31</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 31, nombre = "IDOFERT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String idofert;
	
	/**
	 * <p>Campo <code>INDNOTF</code>, &iacute;ndice: <code>32</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 32, nombre = "INDNOTF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indnotf;
	
}
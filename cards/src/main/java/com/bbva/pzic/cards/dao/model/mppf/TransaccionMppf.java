package com.bbva.pzic.cards.dao.model.mppf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPPF</code>
 *
 * @see PeticionTransaccionMppf
 * @see RespuestaTransaccionMppf
 */
@Component
public class TransaccionMppf implements InvocadorTransaccion<PeticionTransaccionMppf,RespuestaTransaccionMppf> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMppf invocar(PeticionTransaccionMppf transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMppf.class, RespuestaTransaccionMppf.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMppf invocarCache(PeticionTransaccionMppf transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMppf.class, RespuestaTransaccionMppf.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}

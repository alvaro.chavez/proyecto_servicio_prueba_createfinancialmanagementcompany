package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntCardProposal {

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntTitle title;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntCardType cardType;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntAuthorizedParticipant participant;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(min = 1, groups = ValidationGroup.CreateCardsCardProposal.class)
    private List<DTOIntImport> grantedCredits;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntDelivery delivery;
    @Valid
    private DTOIntBankType bank;
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(max = 20, groups = ValidationGroup.CreateCardsCardProposal.class)
    private String offerId;
    private String id;
    private List<DTOIntContactDetail> contactDetails;
    private Date operationDate;

    public DTOIntTitle getTitle() {
        return title;
    }

    public void setTitle(DTOIntTitle title) {
        this.title = title;
    }

    public DTOIntCardType getCardType() {
        return cardType;
    }

    public void setCardType(DTOIntCardType cardType) {
        this.cardType = cardType;
    }

    public DTOIntAuthorizedParticipant getParticipant() {
        return participant;
    }

    public void setParticipant(DTOIntAuthorizedParticipant participant) {
        this.participant = participant;
    }

    public List<DTOIntImport> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<DTOIntImport> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public DTOIntDelivery getDelivery() {
        return delivery;
    }

    public void setDelivery(DTOIntDelivery delivery) {
        this.delivery = delivery;
    }

    public DTOIntBankType getBank() {
        return bank;
    }

    public void setBank(DTOIntBankType bank) {
        this.bank = bank;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<DTOIntContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<DTOIntContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public Date getOperationDate() {
        if (operationDate == null) {
            return null;
        }
        return new Date(operationDate.getTime());
    }

    public void setOperationDate(Date operationDate) {
        if (operationDate == null) {
            this.operationDate = null;
        } else {
            this.operationDate = new Date(operationDate.getTime());
        }
    }
}
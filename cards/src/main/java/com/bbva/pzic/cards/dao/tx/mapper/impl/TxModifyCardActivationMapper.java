package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.dao.model.mpb1.FormatoMPM0BX;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardActivationMapper;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Entelgy
 */
@Mapper
public class TxModifyCardActivationMapper implements ITxModifyCardActivationMapper {

    private static final Log LOG = LogFactory.getLog(TxModifyCardActivationMapper.class);

    @Autowired
    private EnumMapper enumMapper;

    /**
     * @see ITxModifyCardActivationMapper#mapInput(DTOIntActivation)
     */
    @Override
    public FormatoMPM0BX mapInput(DTOIntActivation dtoIntActivation) {
        LOG.info("... called method TxModifyCardActivationMapper.mapInput ...");
        final FormatoMPM0BX formatoMPM0BX = new FormatoMPM0BX();
        formatoMPM0BX.setIdetarj(dtoIntActivation.getCard().getCardId());
        formatoMPM0BX.setCodactv(
                enumMapper.getBackendValue("cards.activation.activationId", dtoIntActivation.getActivationId()));
        if (dtoIntActivation.getIsActive() != null) {
            formatoMPM0BX.setIndactv(dtoIntActivation.getIsActive() ? "S" : "N");
        }
        return formatoMPM0BX;
    }
}
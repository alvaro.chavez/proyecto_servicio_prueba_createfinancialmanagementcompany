package com.bbva.pzic.cards.dao.model.mpde;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS2DE</code> de la transacci&oacute;n <code>MPDE</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS2DE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS2DE {

	/**
	 * <p>Campo <code>IDETTAS</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDETTAS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String idettas;

	/**
	 * <p>Campo <code>DESTTAS</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "DESTTAS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String desttas;

	/**
	 * <p>Campo <code>FECCTAS</code>, &iacute;ndice: <code>3</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 3, nombre = "FECCTAS", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecctas;

	/**
	 * <p>Campo <code>IDMODTA</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "IDMODTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idmodta;

	/**
	 * <p>Campo <code>NOMMOD</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "NOMMOD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String nommod;

	/**
	 * <p>Campo <code>VALTASA</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 6, nombre = "VALTASA", tipo = TipoCampo.DECIMAL, longitudMinima = 11, longitudMaxima = 11, decimales = 4)
	private BigDecimal valtasa;

	/**
	 * <p>Campo <code>INTDEUT</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 7, nombre = "INTDEUT", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal intdeut;

	/**
	 * <p>Campo <code>MONTASA</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "MONTASA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String montasa;

}
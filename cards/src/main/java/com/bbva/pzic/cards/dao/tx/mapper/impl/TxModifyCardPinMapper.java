package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntPin;
import com.bbva.pzic.cards.dao.model.mpwp.FormatoMPMENWP;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardPinMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

/**
 * Created on 3/10/2017.
 *
 * @author Entelgy
 */
@Component
public class TxModifyCardPinMapper implements ITxModifyCardPinMapper {

    private static final Log LOG = LogFactory.getLog(TxModifyCardPinMapper.class);

    @Override
    public FormatoMPMENWP mapInput(final DTOIntPin dtoIntCard) {
        LOG.info("... called method TxModifyCardPinMapper.mapInput ...");
        String pin = dtoIntCard.getPin();

        FormatoMPMENWP formatoMPMENWP = new FormatoMPMENWP();
        formatoMPMENWP.setNumtarj(dtoIntCard.getCardId());
        formatoMPMENWP.setDtcif01(pin.substring(0, 75));
        formatoMPMENWP.setDtcif02(pin.substring(75, 150));
        formatoMPMENWP.setDtcif03(pin.substring(150, 225));
        formatoMPMENWP.setDtcif04(pin.substring(225, 300));
        formatoMPMENWP.setDtcif05(pin.substring(300, 375));
        formatoMPMENWP.setDtcif06(pin.substring(375, 450));
        formatoMPMENWP.setDtcif07(pin.substring(450, 525));
        formatoMPMENWP.setDtcif08(pin.substring(525, 600));
        formatoMPMENWP.setDtcif09(pin.substring(600, 675));
        formatoMPMENWP.setDtcif10(pin.substring(675, 750));
        formatoMPMENWP.setDtcif11(pin.substring(750, 825));
        formatoMPMENWP.setDtcif12(pin.substring(825, 900));
        formatoMPMENWP.setDtcif13(pin.substring(900, 975));
        formatoMPMENWP.setDtcif14(StringUtils.trimToNull(StringUtils.substring(pin, 975, 1050)));

        return formatoMPMENWP;
    }
}

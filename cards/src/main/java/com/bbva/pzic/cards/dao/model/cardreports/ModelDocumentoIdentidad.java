package com.bbva.pzic.cards.dao.model.cardreports;

public class ModelDocumentoIdentidad {

    private String id;

    private String numero;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}

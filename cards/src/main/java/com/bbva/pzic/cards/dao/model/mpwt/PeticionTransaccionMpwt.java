package com.bbva.pzic.cards.dao.model.mpwt;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPWT</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpwt</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpwt</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPWT.D1180511.txt
 * MPWTMPM0DET MPECMPWDMP2CMPWT2S                             XP88366 2017-06-21-18.21.23.844729XP88366 2017-06-21-18.21.23.844764
 * MPWTMPM0TSC MPECMPTCMP2CMPWT1S                             XP88366 2017-06-21-18.21.06.381197XP88366 2017-06-21-18.21.06.381882
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0TSE.D1180511.txt
 * MPM0TSE �ENTRADA TRASPASO DE CUOTAS    �F�03�00038�01�00001�NUMTARJ�NUMERO TARJ OPERA   �A�016�0�R�        �
 * MPM0TSE �ENTRADA TRASPASO DE CUOTAS    �F�03�00038�02�00017�NUMOPER�NUMERO DE OPERACION �A�020�0�R�        �
 * MPM0TSE �ENTRADA TRASPASO DE CUOTAS    �F�03�00038�03�00037�NCUOTAS�NRO CUOTAS TRASPASAR�N�002�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPWT.D1180511.txt
 * MPWTTRASPASO DE CONSUMO DE CUOTAS      MP        MP2CMPWTPBDMPPO MPM0TSE             MPWT  NS0000CNNNNN    SSTN    C   NNNNSNNN  NN                2017-06-21XP88366 2017-07-3118.57.01P025061 2017-06-21-18.06.01.613301XP88366 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0DET.D1180511.txt
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�01�00001�FECPAGO�FECHA PAGO CUOTA    �A�010�0�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�02�00011�CAPITAL�CAPITAL DE CUOTA    �N�011�2�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�03�00022�DIVCAPC�DIVISA CAPITAL CUOTA�A�003�0�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�04�00025�INTERES�INTERES DE CUOTA    �N�011�2�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�05�00036�DIVINTE�DIVISA INTERES CUOTA�A�003�0�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�06�00039�COMISIO�COMISION DE CUOTA   �N�011�2�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�07�00050�DIVCOMI�DIVISA COMISION CUOT�A�003�0�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�08�00053�IMPCUOT�IMPORTE CUOTA       �N�015�2�S�        �
 * MPM0DET �DETALLE CRONOGRAMA CUOTAS     �X�09�00070�09�00068�DIVCUOT�DIVISA IMP CUOTA    �A�003�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0TSC.D1180511.txt
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�01�00001�NUMOPCU�NRO OPERA TRASLADADA�A�020�0�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�02�00021�TASAEFA�TASA EFECTIVA ANUAL �N�009�2�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�03�00030�TOTCAPI�TOTAL CAP CRONO     �N�013�2�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�04�00043�DIVTCAP�DIVISA TOTAL CAP    �A�003�0�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�05�00046�TOTINTE�TOTAL INTERES CRONO �N�013�2�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�06�00059�DIVTINT�DIVISA TOTAL INTERES�A�003�0�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�07�00062�TOTCUOT�TOTAL SUMA CUOTAS   �N�013�2�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�08�00075�DIVTCUO�DIVISA TOTAL CUOTAS �A�003�0�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�09�00078�PORTCEA�PORCENTAJE TCEA     �N�007�4�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�10�00085�FECCUO1�FECHA PRIMERA CUOTA �A�010�0�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�11�00095�IMPCUO1�IMPORT PRIMERA CUOTA�N�013�2�S�        �
 * MPM0TSC �CABECERA TRASPASO DE CUOTAS   �X�12�00109�12�00108�NCUOTAS�NUMERO DE CUOTAS    �N�002�0�S�        �
</pre></code>
 *
 * @see RespuestaTransaccionMpwt
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPWT",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpwt.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPM0TSE.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpwt implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
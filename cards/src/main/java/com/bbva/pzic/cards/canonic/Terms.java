package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 03/07/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Terms", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Terms", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Terms implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer number;
    /**
     * Number of pending terms the client has of the installment plan.
     */
    private Integer pending;
    /**
     * Period in which terms of the installment are defined.
     */
    private String frequency;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getPending() {
        return pending;
    }

    public void setPending(Integer pending) {
        this.pending = pending;

    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }
}

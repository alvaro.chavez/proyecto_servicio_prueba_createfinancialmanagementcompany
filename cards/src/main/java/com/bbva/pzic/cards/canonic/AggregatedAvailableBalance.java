package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "AggregatedAvailableBalance", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "AggregatedAvailableBalance", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AggregatedAvailableBalance implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * This balance means the maximum expendable amount at the moment of
     * retrieval. This balance may be provided in several currencies (depending
     * on the country).
     */
    private List<CurrentBalance> currentBalances;
    /**
     * This balance is the maximum expendable amount calculated at the end of
     * the last business day. This balance may be provided in several currencies
     * (depending on the country).
     */
    private List<PostedBalance> postedBalances;
    /**
     * This balance is the aggregated amount of all pending transactions. This
     * amount is the result of substracting postedBalances to currentBalances.
     * This balance may be provided in several currencies (depending on the
     * country).
     */
    private List<PendingBalance> pendingBalances;

    public List<CurrentBalance> getCurrentBalances() {
        return currentBalances;
    }

    public void setCurrentBalances(List<CurrentBalance> currentBalances) {
        this.currentBalances = currentBalances;
    }

    public List<PostedBalance> getPostedBalances() {
        return postedBalances;
    }

    public void setPostedBalances(List<PostedBalance> postedBalances) {
        this.postedBalances = postedBalances;
    }

    public List<PendingBalance> getPendingBalances() {
        return pendingBalances;
    }

    public void setPendingBalances(List<PendingBalance> pendingBalances) {
        this.pendingBalances = pendingBalances;
    }
}
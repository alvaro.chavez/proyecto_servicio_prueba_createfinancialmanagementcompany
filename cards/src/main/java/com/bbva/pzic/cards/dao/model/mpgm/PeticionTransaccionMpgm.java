package com.bbva.pzic.cards.dao.model.mpgm;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPGM</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpgm</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpgm</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPGM.D1190513.TXT
 * MPGMDETALLE DE MOVIMIENTO REALIZADO    MP        MP2CMPGMPBDMPPO MPMENGM             MPGM  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2017-12-04XP92348 2019-04-1611.47.29XP87388 2017-12-04-16.55.13.735367XP92348 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENGM.D1190513.TXT
 * MPMENGM �DETALLE MOVIMIENTO TARJETA    �F�02�00040�01�00001�NUMTARJ�NUMERO DE TARJETA   �A�020�0�R�        �
 * MPMENGM �DETALLE MOVIMIENTO TARJETA    �F�02�00040�02�00021�IDENTIF�IDENTIF MOVIMIENTO  �A�020�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1GM.D1190513.TXT
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�01�00001�IDENTIF�IDENTIF. MOVTO      �A�020�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�02�00021�IMPORTE�IMPORTE MOVTO       �S�017�2�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�03�00038�DIVISA �DIVISA MOVTO        �A�003�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�04�00041�TIPMOV �TIPO MOVTO          �A�002�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�05�00043�NOMMOV �DESCRIP. MOVIMIENTO �A�025�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�06�00068�INTCOD �INDENT. CONCEPTO MOV�A�040�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�07�00108�NOMCOD �DESCRIP.CONCEPTO MOV�A�040�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�08�00148�CONCEPT�CONCEPTO            �A�080�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�09�00228�INDOPER�INDICADOR OPERACION �A�001�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�10�00229�NOMIOPE�DESCRIP IND. OPERAC.�A�007�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�11�00236�FECHOPE�FECHA DE OPERACION  �A�010�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�12�00246�HOROPE �HORA DE OPERACION   �A�008�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�13�00254�FECHCON�FECHA CONTABLE      �A�010�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�14�00264�IDFINAN�TIPO FINANCIAMIENTO �A�001�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�15�00265�TIPFINA�DESCRIP TIPO FINANC �A�020�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�16�00285�IDESTAD�IDENTIFICADOR ESTADO�A�001�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�17�00286�TIPESTA�DESCRIP IDENT ESTADO�A�020�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�18�00306�TIPCAMB�TIPO DE CAMBIO      �N�013�9�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�19�00319�MODCAMB�MODALIDAD CAMBIO    �A�010�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�20�00329�IDCONT �IDENT. CONTRATO     �A�018�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�21�00347�NUMTARJ�NUMERO TARJETA      �A�019�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�22�00366�TIPNUM �TIPO NUMERO         �A�002�0�S�        �
 * MPMS1GM �DETALLE DE MOVIMIENTO         �X�23�00377�23�00368�DESCNUM�DESCRIP TIPO NUMERO �A�010�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS2GM.D1190513.TXT
 * MPMS2GM �DETALLE MOVIMIENTO PURCHASES  �X�06�00115�01�00001�IDTIEND�ID ESTABLECIMIENTO  �A�009�0�S�        �
 * MPMS2GM �DETALLE MOVIMIENTO PURCHASES  �X�06�00115�02�00010�NOMTIEN�NOM. ESTABLECIMIENTO�A�050�0�S�        �
 * MPMS2GM �DETALLE MOVIMIENTO PURCHASES  �X�06�00115�03�00060�IDCATEG�ID CATEGORIA        �A�004�0�S�        �
 * MPMS2GM �DETALLE MOVIMIENTO PURCHASES  �X�06�00115�04�00064�NOMCAT �DESCRIP. CATEGORIA  �A�030�0�S�        �
 * MPMS2GM �DETALLE MOVIMIENTO PURCHASES  �X�06�00115�05�00094�TIPPAG �TIPO DE PAGO        �A�002�0�S�        �
 * MPMS2GM �DETALLE MOVIMIENTO PURCHASES  �X�06�00115�06�00096�DESPAG �DESCRIP. TIPO PAGO  �A�020�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS3GM.D1190513.TXT
 * MPMS3GM �DETALLE MOVIMIENTO CASH WITHDR�X�06�00033�01�00001�IDATM  �IDENTIFICADOR ATM   �A�004�0�S�        �
 * MPMS3GM �DETALLE MOVIMIENTO CASH WITHDR�X�06�00033�02�00005�IDRED  �IDENTIF. RED ATM    �A�002�0�S�        �
 * MPMS3GM �DETALLE MOVIMIENTO CASH WITHDR�X�06�00033�03�00007�TIPATM �DESCRIP. RED ATM    �A�007�0�S�        �
 * MPMS3GM �DETALLE MOVIMIENTO CASH WITHDR�X�06�00033�04�00014�ORIGRET�ORIGEN DEL RETIRO   �A�002�0�S�        �
 * MPMS3GM �DETALLE MOVIMIENTO CASH WITHDR�X�06�00033�05�00016�IDOFIC �IDENT. OFICINA      �A�004�0�S�        �
 * MPMS3GM �DETALLE MOVIMIENTO CASH WITHDR�X�06�00033�06�00020�DESOFIC�DESCP. OFICINA      �A�014�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS4GM.D1190513.TXT
 * MPMS4GM �CUOTA PAGO POR MENBRESIA      �X�06�00045�01�00001�IMPCONS�IMP.ACUM. CONSUMIDO �N�017�2�S�        �
 * MPMS4GM �CUOTA PAGO POR MENBRESIA      �X�06�00045�02�00018�DIVCONS�DIVISA IMPORTE ACUM.�A�003�0�S�        �
 * MPMS4GM �CUOTA PAGO POR MENBRESIA      �X�06�00045�03�00021�PORCONS�% CONSUMIDO VS META �N�003�0�S�        �
 * MPMS4GM �CUOTA PAGO POR MENBRESIA      �X�06�00045�04�00024�PERIODO�PERIOD APLI.COMISION�A�002�0�S�        �
 * MPMS4GM �CUOTA PAGO POR MENBRESIA      �X�06�00045�05�00026�IMPMETA�IMP.META CONSUMP    �N�017�2�S�        �
 * MPMS4GM �CUOTA PAGO POR MENBRESIA      �X�06�00045�06�00043�DIVMETA�DIV.IMP.META CONSUMI�A�003�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPGM.D1190513.TXT
 * MPGMMPMS4GM MPNCS4GMMP2CMPGM1S                             XP90638 2018-11-25-21.37.05.026494XP90638 2018-11-25-21.37.05.026523
 * MPGMMPMS1GM MPMCS1GMMP2CMPGM1S                             XP92348 2017-12-04-17.00.37.646056XP92348 2017-12-05-08.47.24.341583
 * MPGMMPMS2GM MPMCS2GMMP2CMPGM1S                             XP92348 2017-12-04-17.00.51.605959XP92348 2017-12-05-08.47.34.273873
 * MPGMMPMS3GM MPMCS3GMMP2CMPGM1S                             XP92348 2017-12-04-17.00.58.613777XP92348 2017-12-05-08.47.42.301911
</pre></code>
 *
 * @see RespuestaTransaccionMpgm
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPGM",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpgm.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENGM.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpgm implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
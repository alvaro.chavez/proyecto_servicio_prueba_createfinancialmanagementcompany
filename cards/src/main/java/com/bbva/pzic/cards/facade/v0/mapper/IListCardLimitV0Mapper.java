package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Limits;

import java.util.List;

/**
 * Created on 14/05/2020.
 *
 * @author Entelgy
 */
public interface IListCardLimitV0Mapper {

    DTOIntCard mapIn(String cardId);

    ServiceResponse<List<Limits>> mapOut(List<Limits> limits);
}

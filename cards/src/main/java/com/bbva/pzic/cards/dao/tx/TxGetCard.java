package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.dao.model.mpg1.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.EightfoldOutputFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 16/10/2017.
 *
 * @author Entelgy
 */
@Component("txGetCard")
public class TxGetCard
        extends EightfoldOutputFormat<DTOIntCard, FormatoMPMENG1, Card, FormatoMPMS1G1, FormatoMPMS2G1, FormatoMPMS3G1, FormatoMPMS4G1, FormatoMPMS5G1, FormatoMPMS6G1, FormatoMPMS7G1, FormatoMPMS8G1> {

    private static final Log LOG = LogFactory.getLog(TxGetCard.class);

    @Resource(name = "txGetCardMapper")
    private ITxGetCardMapper mapper;

    @Autowired
    public TxGetCard(@Qualifier("transaccionMpg1") InvocadorTransaccion<PeticionTransaccionMpg1, RespuestaTransaccionMpg1> transaction) {
        super(transaction, PeticionTransaccionMpg1::new, Card::new, FormatoMPMS1G1.class, FormatoMPMS2G1.class, FormatoMPMS3G1.class, FormatoMPMS4G1.class, FormatoMPMS5G1.class, FormatoMPMS6G1.class, FormatoMPMS7G1.class, FormatoMPMS8G1.class);
    }

    @Override
    protected FormatoMPMENG1 mapInput(DTOIntCard dtoIntCard) {
        LOG.info(" ... call TxGetCard.mapInput ... ");
        return mapper.mapIn(dtoIntCard);
    }

    @Override
    protected Card mapFirstOutputFormat(FormatoMPMS1G1 formatoMPMS1G1, DTOIntCard dtoIntCard, Card dtoOut) {
        LOG.info(" ... call TxGetCard.mapFirstOutputFormat ... ");
        return mapper.mapOut1(formatoMPMS1G1, dtoOut);
    }

    @Override
    protected Card mapSecondOutputFormat(FormatoMPMS2G1 formatoMPMS2G1, DTOIntCard dtoIntCard, Card dtoOut) {
        LOG.info(" ... call TxGetCard.mapSecondOutputFormat ... ");
        return mapper.mapOut2(formatoMPMS2G1, dtoOut);
    }

    @Override
    protected Card mapThirdOutputFormat(FormatoMPMS3G1 formatoMPMS3G1, DTOIntCard dtoIntCard, Card dtoOut) {
        LOG.info(" ... call TxGetCard.mapThirdOutputFormat ... ");
        return mapper.mapOut3(formatoMPMS3G1, dtoOut);
    }

    @Override
    protected Card mapFourthOutputFormat(FormatoMPMS4G1 formatoMPMS4G1, DTOIntCard dtoIntCard, Card dtoOut) {
        LOG.info(" ... call TxGetCard.mapFourthOutputFormat ... ");
        return mapper.mapOut4(formatoMPMS4G1, dtoOut);
    }

    @Override
    protected Card mapFifthOutputFormat(FormatoMPMS5G1 formatoMPMS5G1, DTOIntCard dtoIntCard, Card dtoOut) {
        LOG.info(" ... call TxGetCard.mapFifthOutputFormat ... ");
        return mapper.mapOut5(formatoMPMS5G1, dtoOut);
    }

    @Override
    protected Card mapSixthOutputFormat(FormatoMPMS6G1 formatoMPMS6G1, DTOIntCard dtoIntCard, Card dtoOut) {
        LOG.info(" ... call TxGetCard.mapSixthOutputFormat ... ");
        return mapper.mapOut6(formatoMPMS6G1, dtoOut);
    }

    @Override
    protected Card mapSeventhOutputFormat(FormatoMPMS7G1 formatoMPMS7G1, DTOIntCard dtoIntCard, Card dtoOut) {
        LOG.info(" ... call TxGetCard.mapSeventhOutputFormat ... ");
        return mapper.mapOut7(formatoMPMS7G1, dtoOut);
    }

    @Override
    protected Card mapEighthOutputFormat(FormatoMPMS8G1 formatoMPMS8G1, DTOIntCard dtoIntCard, Card dtoOut) {
        LOG.info(" ... call TxGetCard.mapEighthOutputFormat ... ");
        return mapper.mapOut8(formatoMPMS8G1, dtoOut);
    }
}

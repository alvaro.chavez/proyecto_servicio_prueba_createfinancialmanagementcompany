package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.pzic.cards.business.dto.InputCreateOffersGenerateCards;
import com.bbva.pzic.cards.facade.v0.dto.OfferGenerate;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
public interface ICreateOffersGenerateCardsMapper {

    InputCreateOffersGenerateCards mapIn(OfferGenerate offerGenerate, String bcsDeviceScreenSize);

    ServiceResponseOK<OfferGenerate> mapOut(OfferGenerate offersGenerateCards);
}

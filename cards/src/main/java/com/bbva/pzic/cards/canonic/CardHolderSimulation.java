package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 17/12/2018.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "cardHolderSimulation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "cardHolderSimulation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardHolderSimulation implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Title of the financial product.
     */
    private Title title;

    /**
     * Limit amount that the customer can request.
     */
    private Import limitAmount;
    /**
     * Financial product type.
     */
    private CardType cardType;
    /**
     * Image.
     */
    private Image image;
    /**
     * Financial product associated with the contract.
     */
    private Product product;
    /**
     * Limit minimum that the customer can request. This amount could be provided in serveral currencies.
     */
    private MinimumAmount minimumAmount;
    /**
     * Limit maximum  that the customer can request. This amount could be provided in several currencies.
     */
    private MaximumAmount maximumAmount;
    /**
     * Amount offered to the client. This value is between the minimum and maximum limits.
     * This amount could be provided in several currencies.
     */
    private SuggestedAmount suggestedAmount;
    /**
     * Commissions associated with the contracting of a Credit Card.
     * They may or may not come more than one fee depending on the offer related.
     */
    private Fees fees;
    /**
     * Rate charge applied to the type of offer associated.
     */
    private List<Rate> rates;
    /**
     * Additional products related to the new card.
     */
    private List<AdditionalProducts> additionalProducts;
    /**
     * Benefits related to the card that will be hired.
     */
    private List<Benefit> benefits;
    /**
     * Membership associated to the card.
     */
    private Membership membership;
    /**
     * Payment period related to a specific credit card.
     */
    private PaymentPeriod paymentPeriod;
    /**
     * Payment method related to a specific credit card.
     */
    private PaymentMethod paymentMethod;

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Import getLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(Import limitAmount) {
        this.limitAmount = limitAmount;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public MinimumAmount getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(MinimumAmount minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public MaximumAmount getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(MaximumAmount maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public SuggestedAmount getSuggestedAmount() {
        return suggestedAmount;
    }

    public void setSuggestedAmount(SuggestedAmount suggestedAmount) {
        this.suggestedAmount = suggestedAmount;
    }

    public Fees getFees() {
        return fees;
    }

    public void setFees(Fees fees) {
        this.fees = fees;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }

    public List<AdditionalProducts> getAdditionalProducts() {
        return additionalProducts;
    }

    public void setAdditionalProducts(List<AdditionalProducts> additionalProducts) {
        this.additionalProducts = additionalProducts;
    }

    public List<Benefit> getBenefits() {
        return benefits;
    }

    public void setBenefits(List<Benefit> benefits) {
        this.benefits = benefits;
    }

    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }

    public PaymentPeriod getPaymentPeriod() {
        return paymentPeriod;
    }

    public void setPaymentPeriod(PaymentPeriod paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}

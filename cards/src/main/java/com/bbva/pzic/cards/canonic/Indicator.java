package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Indicator", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Indicator", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Indicator implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier associated to the behavior indicator.
     */
    private String indicatorId;
    /**
     * Localized description of the behavior indicator.
     */
    private String name;
    /**
     * Indicates whether the indicator is enabled.
     */
    private Boolean isActive;

    public String getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(String indicatorId) {
        this.indicatorId = indicatorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
package com.bbva.pzic.cards.dao.model.mpb2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPB2</code>
 *
 * @see PeticionTransaccionMpb2
 * @see RespuestaTransaccionMpb2
 */
@Component
public class TransaccionMpb2 implements InvocadorTransaccion<PeticionTransaccionMpb2,RespuestaTransaccionMpb2> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpb2 invocar(PeticionTransaccionMpb2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpb2.class, RespuestaTransaccionMpb2.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpb2 invocarCache(PeticionTransaccionMpb2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpb2.class, RespuestaTransaccionMpb2.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}

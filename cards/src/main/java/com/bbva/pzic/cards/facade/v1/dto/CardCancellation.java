package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "cardCancellation", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "cardCancellation", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardCancellation implements Serializable {

    private static final long serialVersionUID = 1L;
    @DatoAuditable(omitir = true)
    private String id;
    @DatoAuditable(omitir = true)
    private String number;

    private NumberType numberType;

    private Product product;

    private Currency currencies;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public NumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(NumberType numberType) {
        this.numberType = numberType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Currency getCurrencies() {
        return currencies;
    }

    public void setCurrencies(Currency currencies) {
        this.currencies = currencies;
    }
}

package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancellations;
import com.bbva.pzic.cards.dao.model.mpcn.FormatoMPME1NC;
import com.bbva.pzic.cards.dao.model.mpcn.FormatoMPMS1NC;
import com.bbva.pzic.cards.dao.model.mpcn.PeticionTransaccionMpcn;
import com.bbva.pzic.cards.dao.model.mpcn.RespuestaTransaccionMpcn;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardCancellationsV1Mapper;
import com.bbva.pzic.cards.facade.v1.dto.Cancellation;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class TxCreateCardCancellationsV1 extends SingleOutputFormat<InputCreateCardCancellations, FormatoMPME1NC, Cancellation, FormatoMPMS1NC> {

    @Resource(name = "txCreateCardCancellationsV1Mapper")
    private ITxCreateCardCancellationsV1Mapper mapper;

    public TxCreateCardCancellationsV1(@Qualifier("transaccionMpcn") InvocadorTransaccion<PeticionTransaccionMpcn, RespuestaTransaccionMpcn> transaction) {
        super(transaction, PeticionTransaccionMpcn::new, Cancellation::new, FormatoMPMS1NC.class);
    }

    @Override
    protected FormatoMPME1NC mapInput(InputCreateCardCancellations input) {
        return mapper.mapIn(input);
    }

    @Override
    protected Cancellation mapFirstOutputFormat(FormatoMPMS1NC format, InputCreateCardCancellations input, Cancellation output) {
        return mapper.mapOutFormatoMPMS1NC(format);
    }
}

package com.bbva.pzic.cards.dao.model.kb93.mock;

import com.bbva.pzic.cards.dao.model.kb93.FormatoKTSKB931;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public final class FormatsKb93Mock {

    private static final FormatsKb93Mock INSTANCE = new FormatsKb93Mock();
    private ObjectMapperHelper objectMapper;

    private FormatsKb93Mock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public static FormatsKb93Mock getInstance() {
        return INSTANCE;
    }

    public FormatoKTSKB931 getFormatoKTSKB931() throws IOException {
        return objectMapper
                .readValue(
                        Thread.currentThread()
                                .getContextClassLoader()
                                .getResourceAsStream(
                                        "com/bbva/pzic/cards/dao/model/kb93/mock/formatoKTSKB931.json"),
                        FormatoKTSKB931.class);
    }

    public FormatoKTSKB931 getFormatoKTSKB931Empty() throws IOException {
        return objectMapper
                .readValue(
                        Thread.currentThread()
                                .getContextClassLoader()
                                .getResourceAsStream(
                                        "com/bbva/pzic/cards/dao/model/kb93/mock/formatoKTSKB931-EMPTY.json"),
                        FormatoKTSKB931.class);
    }
}
// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mpe2;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.pzic.cards.dao.model.mpe2.RespuestaTransaccionMpe2;

privileged aspect RespuestaTransaccionMpe2_Roo_JavaBean {
    
    /**
     * Gets codigoRetorno value
     * 
     * @return String
     */
    public String RespuestaTransaccionMpe2.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    /**
     * Sets codigoRetorno value
     * 
     * @param codigoRetorno
     * @return RespuestaTransaccionMpe2
     */
    public RespuestaTransaccionMpe2 RespuestaTransaccionMpe2.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
        return this;
    }
    
    /**
     * Gets codigoControl value
     * 
     * @return String
     */
    public String RespuestaTransaccionMpe2.getCodigoControl() {
        return this.codigoControl;
    }
    
    /**
     * Sets codigoControl value
     * 
     * @param codigoControl
     * @return RespuestaTransaccionMpe2
     */
    public RespuestaTransaccionMpe2 RespuestaTransaccionMpe2.setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
        return this;
    }
    
    /**
     * Sets cuerpo value
     * 
     * @param cuerpo
     * @return RespuestaTransaccionMpe2
     */
    public RespuestaTransaccionMpe2 RespuestaTransaccionMpe2.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String RespuestaTransaccionMpe2.toString() {
        return "RespuestaTransaccionMpe2 {" + 
                "codigoRetorno='" + codigoRetorno + '\'' + 
                ", codigoControl='" + codigoControl + '\'' + "}" + super.toString();
    }
    
}

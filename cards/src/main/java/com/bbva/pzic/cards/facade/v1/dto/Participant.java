package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Participant", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "Participant", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Participant implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Participant identifier.
     */
    private String id;
    /**
     * Participant identifier.
     */
    @DatoAuditable(omitir = true)
    private String participantId;
    /**
     * Participant first name.
     */
    @DatoAuditable(omitir = true)
    private String firstName;
    /**
     * Participant last name.
     */
    @DatoAuditable(omitir = true)
    private String lastName;
    /**
     * Status of the relation of the persons with the bank.
     */
    private String personType;
    /**
     * Participation role.
     */
    private ParticipantType participantType;
    /**
     * Legal person type.
     */
    private LegalPersonType legalPersonType;
    /**
     * Indicator to identify if the participant is customer or not.
     */
    private Boolean isCustomer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public ParticipantType getParticipantType() {
        return participantType;
    }

    public void setParticipantType(ParticipantType participantType) {
        this.participantType = participantType;
    }

    public LegalPersonType getLegalPersonType() {
        return legalPersonType;
    }

    public void setLegalPersonType(LegalPersonType legalPersonType) {
        this.legalPersonType = legalPersonType;
    }

    public Boolean getIsCustomer() {
        return isCustomer;
    }

    public void setIsCustomer(Boolean customer) {
        isCustomer = customer;
    }
}

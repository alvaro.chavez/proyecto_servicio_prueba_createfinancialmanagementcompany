package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.managers.InputHeaderManager;
import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOIntPagination;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMENL1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS1L1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS2L1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardsV0Mapper;
import com.bbva.pzic.cards.util.BusinessServiceUtil;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.converter.builtin.BooleanToStringConverter;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
@Mapper
public class TxListCardsV0Mapper implements ITxListCardsV0Mapper {

    private static final String PIPE_SEPARATOR = "|";

    private Translator translator;
    private InputHeaderManager inputHeaderManager;
    private BooleanToStringConverter booleanToStringNumberConverter;

    public TxListCardsV0Mapper(Translator translator, InputHeaderManager inputHeaderManager) {
        this.translator = translator;
        this.inputHeaderManager = inputHeaderManager;
        this.booleanToStringNumberConverter = new BooleanToStringConverter();
    }

    /**
     * @see com.bbva.pzic.cards.dao.tx.mapper.ITxListCardsMapper#mapIn(com.bbva.pzic.cards.business.dto.DTOIntListCards)
     */
    @Override
    public FormatoMPMENL1 mapIn(final DTOIntListCards dtoIn) {
        FormatoMPMENL1 formatoMPMENL1 = new FormatoMPMENL1();
        formatoMPMENL1.setNumclie(dtoIn.getCustomerId());
        //enum
        formatoMPMENL1.setTiptarc(mapInEnumPipeConcatenate("cards.cardType.id", dtoIn.getCardTypeId()));
        formatoMPMENL1.setSopfisl(translator.translateFrontendEnumValueStrictly("cards.physicalSupport.id", dtoIn.getPhysicalSupportId()));
        formatoMPMENL1.setEstarjo(mapInEnumPipeConcatenate("cards.status.id", dtoIn.getStatusId()));
        formatoMPMENL1.setTippart(mapInEnumPipeConcatenate("cards.participantType.id", dtoIn.getParticipantsParticipantTypeId()));
        formatoMPMENL1.setIdpagin(dtoIn.getPaginationKey());
        formatoMPMENL1.setTampagi(dtoIn.getPageSize());
        return formatoMPMENL1;
    }

    private String mapInEnumPipeConcatenate(final String enumPropertyValue, final List<String> backendIds) {
        if (backendIds == null) {
            return null;
        }

        StringBuilder hostEnumIds = new StringBuilder();
        for (String id : backendIds) {
            hostEnumIds.append(translator.translateFrontendEnumValueStrictly(enumPropertyValue, id));
            hostEnumIds.append(PIPE_SEPARATOR);
        }
        return hostEnumIds.substring(0, hostEnumIds.length() - 1);
    }

    /**
     * @see com.bbva.pzic.cards.dao.tx.mapper.ITxListCardsMapper#mapOut(com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS1L1, com.bbva.pzic.cards.business.dto.DTOIntListCards, com.bbva.pzic.cards.business.dto.DTOOutListCards)
     */
    @Override
    public DTOOutListCards mapOut(final FormatoMPMS1L1 formatOutput, final DTOIntListCards dtoIn, final DTOOutListCards dtoOut) {
        DTOOutListCards cards = dtoOut;
        if (cards == null) {
            cards = new DTOOutListCards();
        }
        if (cards.getData() == null) {
            cards.setData(new ArrayList<>());
        }
        //atributos simples
        Card card = new Card();
        card.setNumber(formatOutput.getNumtarj());
        card.setExpirationDate(formatOutput.getFecvenc());
        card.setHolderName(formatOutput.getNombcli());
        card.setNumberType(getNumberType("cards.numberType.id", formatOutput.getIdtnuco(), formatOutput.getDetnuco()));
        card.setCurrencies(getCurrencies(formatOutput.getMdispue(), formatOutput.getMdispub()));
        card.setCardType(getCardType(formatOutput.getTiptarj(), formatOutput.getDtitarj()));
        card.setPhysicalSupport(getPhysicalSupport(formatOutput.getSopfiss(), formatOutput.getDsopfis()));
        card.setStatus(getStatus(formatOutput.getEstarjs(), formatOutput.getDestarj(), formatOutput.getFecsitt()));
        card.setAvailableBalance(getAvailableBalance(formatOutput.getSdispon(), formatOutput.getMdispon()));
        card.setDisposedBalance(getDisposedBalance(formatOutput.getSdispue(), formatOutput.getMdispue(), formatOutput.getSdispub(), formatOutput.getMdispub()));
        card.setTitle(getTitle(formatOutput.getIprotar(), formatOutput.getDprotar()));
        card.setBrandAssociation(getBrandAssociation(formatOutput.getIdmatar(), formatOutput.getNomatar()));
        card.setGrantedCredits(mapGrantedCredits(formatOutput.getIlimcre(), formatOutput.getDlimcre()));
        card.setImages(mapOutListImages(formatOutput));
        card.setRelatedContracts(mapRelatedContracts(formatOutput));
        card.setBlocks(mapBlocks(formatOutput.getIdebloq(), formatOutput.getDesbloq(), formatOutput.getIderazo(), formatOutput.getDesrazo(), formatOutput.getFecbloq(), formatOutput.getIdactbl()));
        card.setActivations(mapOutActivations(formatOutput));
        card.setParticipants(mapParticipants(formatOutput.getIdpart(), formatOutput.getNompart(), formatOutput.getApepart(), formatOutput.getTippart(), formatOutput.getDespart()));
        cards.getData().add(card);
        return cards;
    }

    private List<Participant> mapParticipants(final String participantId, final String firstName, final String lastName, final String participantTypeId, final String participantTypeName) {
        if (participantId == null && firstName == null && lastName == null && participantTypeId == null && participantTypeName == null) {
            return null;
        }
        Participant participant = new Participant();
        participant.setParticipantId(participantId);
        participant.setFirstName(firstName);
        participant.setLastName(lastName);
        participant.setParticipantType(getParticipantType(participantTypeId, participantTypeName));
        List<Participant> participants = new ArrayList<>();
        participants.add(participant);
        return participants;
    }

    private ParticipantType getParticipantType(final String participantTypeId, final String participantTypeName) {
        if (participantTypeId == null && participantTypeName == null) {
            return null;
        }
        ParticipantType participantType = new ParticipantType();
        participantType.setId(translator.translateBackendEnumValueStrictly("participants.participantType.id", participantTypeId));
        participantType.setName(participantTypeName);
        return participantType;
    }

    private List<Activation> mapOutActivations(final FormatoMPMS1L1 formatOutput) {
        List<Activation> activations = new ArrayList<>();
        mapOutActivation(formatOutput.getCodactv(), formatOutput.getIndactv(), activations);
        mapOutActivation(formatOutput.getCodreti(), formatOutput.getIndreti(), activations);
        mapOutActivation(formatOutput.getCodinte(), formatOutput.getIndinte(), activations);
        mapOutActivation(formatOutput.getCodcoex(), formatOutput.getIndcoex(), activations);
        mapOutActivation(formatOutput.getCodsobr(), formatOutput.getIndsobr(), activations);

        return CollectionUtils.isEmpty(activations) ? null : activations;
    }

    private void mapOutActivation(final String activationId, final String isActive, final List<Activation> activations) {
        if (activationId == null && isActive == null) {
            return;
        }

        Activation activation = new Activation();
        activation.setActivationId(translator.translateBackendEnumValueStrictly("cards.activation.activationId", activationId));
        activation.setIsActive(booleanToStringNumberConverter.convertFrom(isActive, null));
        activations.add(activation);
    }

    private List<Block> mapBlocks(final String blockId, final String blockName, final String reasonId, final String reasonName, final Date blockDate, final String isActive) {
        if (blockId == null && blockName == null && reasonId == null && reasonName == null && blockDate == null && isActive == null) {
            return null;
        }
        List<Block> blocks = new ArrayList<>();
        Block block = new Block();
        block.setBlockId(translator.translateBackendEnumValueStrictly("cards.block.blockId", blockId));
        block.setName(blockName);
        block.setBlockDate(FunctionUtils.buildDatetime(blockDate, Constants.DEFAULT_TIME));
        block.setIsActive(booleanToStringNumberConverter.convertFrom(isActive, null));

        Reason reason = new Reason();
        reason.setId(reasonId);
        reason.setName(reasonName);
        block.setReason(reason);

        blocks.add(block);
        return blocks;
    }

    /**
     * @see com.bbva.pzic.cards.dao.tx.mapper.ITxListCardsMapper#mapOut2(com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS2L1, com.bbva.pzic.cards.business.dto.DTOIntListCards, com.bbva.pzic.cards.business.dto.DTOOutListCards)
     */
    @Override
    public DTOOutListCards mapOut2(final FormatoMPMS2L1 formatOutput, final DTOIntListCards dtoIn, final DTOOutListCards dtoOut) {
        if (formatOutput.getTampagi() == null && formatOutput.getIdpagin() == null) {
            return dtoOut;
        }
        DTOIntPagination pagination = new DTOIntPagination();
        pagination.setPaginationKey(formatOutput.getIdpagin());
        pagination.setPageSize(formatOutput.getTampagi() == null ? null : formatOutput.getTampagi().longValue());
        dtoOut.setPagination(pagination);
        return dtoOut;
    }

    private List<RelatedContract> mapRelatedContracts(final FormatoMPMS1L1 formatOutput) {
        if (formatOutput.getIdcorel() == null && formatOutput.getNucorel() == null
                && formatOutput.getIdtcore() == null && formatOutput.getDetcore() == null
                && formatOutput.getIdcortv() == null && formatOutput.getNucortv() == null
                && formatOutput.getIdtcotv() == null && formatOutput.getDetcotv() == null) {
            return null;
        }
        List<RelatedContract> relatedContracts = new ArrayList<>();
        RelatedContract relatedContract = new RelatedContract();
        if (formatOutput.getIdcorel() != null || formatOutput.getNucorel() != null
                || formatOutput.getIdtcore() != null || formatOutput.getDetcore() != null) {
            relatedContract.setRelatedContractId(formatOutput.getIdcorel());
            relatedContract.setNumber(formatOutput.getNucorel());
            relatedContract.setNumberType(getNumberType("relatedContracts.numberType.id", formatOutput.getIdtcore(), formatOutput.getDetcore()));
        }
        relatedContracts.add(relatedContract);
        relatedContract = new RelatedContract();
        if (formatOutput.getIdcortv() != null || formatOutput.getNucortv() != null
                || formatOutput.getIdtcotv() != null || formatOutput.getDetcotv() != null) {
            relatedContract.setRelatedContractId(formatOutput.getIdcortv());
            relatedContract.setNumber(formatOutput.getNucortv());
            relatedContract.setNumberType(getNumberType("relatedContracts.numberType.id", formatOutput.getIdtcotv(), formatOutput.getDetcotv()));
        }
        relatedContracts.add(relatedContract);

        return relatedContracts;
    }

    private List<GrantedCredit> mapGrantedCredits(final BigDecimal amount, final String currency) {
        if (amount == null && currency == null) {
            return null;
        }
        GrantedCredit grantedCredit = new GrantedCredit();
        grantedCredit.setAmount(amount);
        grantedCredit.setCurrency(currency);
        List<GrantedCredit> grantedCredits = new ArrayList<>();
        grantedCredits.add(grantedCredit);
        return grantedCredits;
    }

    private BrandAssociation getBrandAssociation(final String id, final String name) {
        if (id == null && name == null) {
            return null;
        }

        BrandAssociation brandAssociation = new BrandAssociation();
        brandAssociation.setId(translator.translateBackendEnumValueStrictly("cards.brandAssociation.id", id));
        brandAssociation.setName(name);
        return brandAssociation;
    }

    private Title getTitle(final String id, final String name) {
        if (id == null && name == null) {
            return null;
        }
        Title title = new Title();
        title.setId(id);
        title.setName(name);
        return title;
    }

    private DisposedBalance getDisposedBalance(final BigDecimal amount, final String currency, final BigDecimal amount2, final String currency2) {
        if (amount == null && currency == null && amount2 == null && currency2 == null) {
            return null;
        }
        List<CurrentBalance> currentBalances = new ArrayList<>();
        CurrentBalance currentBalance;
        if (amount != null || currency != null) {
            currentBalance = new CurrentBalance();
            currentBalance.setAmount(amount);
            currentBalance.setCurrency(currency);
            currentBalances.add(currentBalance);
        }
        if (amount2 != null || currency2 != null) {
            currentBalance = new CurrentBalance();
            currentBalance.setAmount(amount2);
            currentBalance.setCurrency(currency2);
            currentBalances.add(currentBalance);
        }
        DisposedBalance disposedBalance = new DisposedBalance();
        disposedBalance.setCurrentBalances(currentBalances);
        return disposedBalance;
    }

    private AvailableBalance getAvailableBalance(final BigDecimal amount, final String currency) {
        if (amount == null && currency == null) {
            return null;
        }
        List<CurrentBalance> currentBalances = new ArrayList<>();
        CurrentBalance currentBalance = new CurrentBalance();
        currentBalance.setCurrency(currency);
        currentBalance.setAmount(amount);
        currentBalances.add(currentBalance);
        AvailableBalance availableBalance = new AvailableBalance();
        availableBalance.setCurrentBalances(currentBalances);
        return availableBalance;
    }

    private PhysicalSupport getPhysicalSupport(final String id, final String name) {
        if (id == null && name == null) {
            return null;
        }
        PhysicalSupport physicalSupport = new PhysicalSupport();
        physicalSupport.setId(translator.translateBackendEnumValueStrictly("cards.physicalSupport.id", id));
        physicalSupport.setName(name);
        return physicalSupport;
    }

    private Status getStatus(final String id, final String name, final Date lastUpdatedDate) {
        if (id == null && name == null && lastUpdatedDate == null) {
            return null;
        }
        Status status = new Status();
        status.setId(translator.translateBackendEnumValueStrictly("cards.status.id", id));
        status.setName(name);
        status.setLastUpdatedDate(lastUpdatedDate);
        return status;
    }

    private CardType getCardType(final String id, final String name) {
        if (id == null && name == null) {
            return null;
        }
        CardType cardType = new CardType();
        cardType.setId(translator.translateBackendEnumValueStrictly("cards.cardType.id", id));
        cardType.setName(name);
        return cardType;
    }

    private NumberType getNumberType(final String field, final String id, final String name) {
        if (id == null && name == null) {
            return null;
        }
        NumberType numberType = new NumberType();
        numberType.setId(translator.translateBackendEnumValueStrictly(field, id));
        numberType.setName(name);
        return numberType;
    }

    private List<Currency> getCurrencies(final String mdispue, final String mdispub) {
        if (mdispue == null && mdispub == null) {
            return null;
        }
        List<Currency> currencies = new ArrayList<>();
        Currency currency;
        if (mdispue != null) {
            currency = new Currency();
            currency.setCurrency(mdispue);
            currency.setIsMajor(Boolean.TRUE);
            currencies.add(currency);
        }
        if (mdispub != null) {
            currency = new Currency();
            currency.setCurrency(mdispub);
            currency.setIsMajor(Boolean.FALSE);
            currencies.add(currency);
        }
        return currencies;
    }

    private List<Image> mapOutListImages(final FormatoMPMS1L1 formatOutput) {
        List<Image> images = new ArrayList<>();

        Image image = mapImage(formatOutput.getIdimgt1(), formatOutput.getNimgta1(), formatOutput.getImgtar1(), false);
        if (image != null) {
            images.add(image);
        }

        image = mapImage(formatOutput.getIdimgt2(), formatOutput.getNimgta2(), formatOutput.getImgtar2(), true);
        if (image != null) {
            images.add(image);
        }

        return images.isEmpty() ? null : images;
    }

    private Image mapImage(final String idimgt, final String nimgta, final String imgtar, boolean isImgBack) {
        if (idimgt == null && nimgta == null && imgtar == null)
            return null;

        if (StringUtils.isEmpty(imgtar) && isImgBack)
            return null;

        Image image = new Image();
        image.setId(idimgt);
        image.setName(nimgta);
        image.setUrl(BusinessServiceUtil.buildUrl(inputHeaderManager.getHeader(Constants.HEADER_USER_AGENT), imgtar));
        return image;
    }
}

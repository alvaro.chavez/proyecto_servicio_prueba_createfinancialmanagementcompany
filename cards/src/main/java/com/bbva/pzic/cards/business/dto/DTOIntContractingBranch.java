package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntContractingBranch {
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String id;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.bbva.pzic.cards.facade.v01.impl;

import com.bbva.jee.arq.spring.core.managers.InputHeaderManager;
import com.bbva.jee.arq.spring.core.servicing.annotations.PATCH;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;
import com.bbva.pzic.cards.business.ISrvIntCards;
import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v01.ISrvCardsV01;
import com.bbva.pzic.cards.facade.v01.mapper.*;
import com.bbva.pzic.cards.util.BusinessServiceUtil;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATION;
import static com.bbva.pzic.cards.util.Enums.*;

/**
 * @author Entelgy
 */
@Path("/V01")
@SN(registryID = "SNPE1500085", logicalID = "cards")
@VN(vnn = "V01")
@Produces(MediaType.APPLICATION_JSON)
@Service
public class SrvCardsV01 implements ISrvCardsV01, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

    private static final Log LOG = LogFactory.getLog(SrvCardsV01.class);

    public UriInfo uriInfo;
    public HttpHeaders httpHeaders;

    @Autowired
    private BusinessServicesToolKit businessToolKit;

    @Autowired
    private ICreateCardMapper createCardMapper;

    @Autowired
    private IModifyCardActivationMapper modifyCardActivationMapper;

    @Autowired
    private IListCardsMapper listCardsMapper;

    @Autowired
    private IListCardTransactionsMapper listTransactionsMapper;

    @Autowired
    private ICreateCardRelatedContractMapper createRelatedContractMapper;

    @Autowired
    private ISrvIntCards srvIntCards;

    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;

    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;

    private InputHeaderManager inputHeaderManager;

    @Override
    public void setUriInfo(UriInfo ui) {
        this.uriInfo = ui;
    }

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    @Autowired
    public void setInputHeaderManager(InputHeaderManager inputHeaderManager) {
        this.inputHeaderManager = inputHeaderManager;
    }

    /**
     * @see com.bbva.pzic.cards.facade.v01.ISrvCardsV01#createCard(com.bbva.pzic.cards.canonic.Card)
     */
    @Override
    @POST
    @Path("/cards")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD, logicalID = "createCard")
    public CardData createCard(final Card card) {

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD, card, null, null);

        CardData data = srvIntCards.createCard(createCardMapper.mapIn(card));

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD, data, null, null);

        return data;
    }

    /**
     * @see ISrvCardsV01#modifyCardActivation(String, String, Activation)
     */
    @Override
    @PATCH
    @Path("/cards/{card-id}/activations/{activation-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATION, logicalID = "modifyCardActivation", forcedCatalog = "asoCatalog")
    public Response modifyCardActivation(@PathParam(CARD_ID) final String cardId,
                                         @PathParam(ACTIVATION_ID) final String activationId,
                                         final Activation activation) {
        LOG.info("... called method SrvCardsV01.modifyCardActivation ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(ACTIVATION_ID, activationId);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATION, activation, pathParams, null);

        final URI uri = UriBuilder.fromPath(uriInfo.getPath()).build();

        DTOIntActivation data = modifyCardActivationMapper.mapInput((String) pathParams.get(CARD_ID), (String) pathParams.get(ACTIVATION_ID), activation);

        srvIntCards.modifyCardActivation(data);

        ActivationData activationData = modifyCardActivationMapper.mapOut(data);

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATION, activationData, pathParams, null);

        return Response.ok(activationData).contentLocation(uri).build();
    }

    /**
     * @see com.bbva.pzic.cards.facade.v01.ISrvCardsV01#listCards(String, java.util.List, String, java.util.List, String, String, Integer)
     */
    @Override
    @GET
    @Path("/")
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARDS_V01, logicalID = "listCards")
    public CardsList listCards(
            @QueryParam(CUSTOMER_ID) final String customerId,
            @QueryParam(CARD_TYPE_ID) final List<String> cardTypeId,
            @QueryParam(PHYSICAL_SUPPORT_ID) final String physicalSupportId,
            @QueryParam(STATUS_ID) final List<String> statusId,
            @QueryParam(EXPAND) final String expand,
            @QueryParam(PAGINATION_KEY) final String paginationKey,
            @QueryParam(PAGE_SIZE) final Integer pageSize) {

        HashMap<String, Object> queryParam = new HashMap<>();
        queryParam.put(CUSTOMER_ID, customerId);
        queryParam.put(CARD_TYPE_ID, cardTypeId);
        queryParam.put(PHYSICAL_SUPPORT_ID, physicalSupportId);
        queryParam.put(STATUS_ID, statusId);
        queryParam.put(PAGINATION_KEY, paginationKey);
        queryParam.put(PAGE_SIZE, pageSize);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARDS_V01, null, null, queryParam);

        DTOOutListCards dto = srvIntCards.listCards(listCardsMapper.mapIn(
                (String) queryParam.get(CUSTOMER_ID),
                (List<String>) queryParam.get(CARD_TYPE_ID),
                (String) queryParam.get(PHYSICAL_SUPPORT_ID),
                (List<String>) queryParam.get(STATUS_ID),
                (String) queryParam.get(PAGINATION_KEY),
                (Integer) queryParam.get(PAGE_SIZE)));

        CardsList cardsList = listCardsMapper.mapOut(dto, inputHeaderManager.getHeader(Constants.HEADER_USER_AGENT));
        if (cardsList == null) {
            return null;

        }
        BusinessServiceUtil.expand(cardsList.getData(),
                "relatedContracts",
                expand == null ? null : expand.replace("-c", "C"));

        if (dto.getPagination() != null) {
            cardsList.setPagination(businessToolKit.getPaginationBuider()
                    .setPagination(SrvCardsV01.class, "listCards", uriInfo,
                            dto.getPagination().getPaginationKey(), null,
                            dto.getPagination().getPageSize(), null,
                            null, null, null).build());
        }

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARDS_V01, cardsList, null, queryParam);

        return cardsList;
    }

    /**
     * @see ISrvCardsV01#listCardTransactions1(String, String, String, String, Long)
     */
    @Override
    @GET
    @Path("/{card-id}/transactions")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS1_V01, logicalID = "listCardTransactions")
    public TransactionsData listCardTransactions1(@PathParam(CARD_ID) String cardId,
                                                  @QueryParam("fromOperationDate") String fromOperationDate,
                                                  @QueryParam("toOperationDate") String toOperationDate,
                                                  @QueryParam("paginationKey") String paginationKey,
                                                  @QueryParam("pageSize") Long pageSize) {
        LOG.info("... called method SrvCardsV01.listTransactions ...");
        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(FROM_OPERATION_DATE, fromOperationDate);
        queryParams.put(TO_OPERATION_DATE, toOperationDate);
        queryParams.put(PAGINATION_KEY, paginationKey);
        queryParams.put(PAGE_SIZE, pageSize);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS1_V01, null, pathParams, queryParams);

        TransactionsData data = listCardTransactions("listCardTransactions1", false,
                (String) pathParams.get(CARD_ID),
                (String) queryParams.get(FROM_OPERATION_DATE),
                (String) queryParams.get(TO_OPERATION_DATE),
                (String) queryParams.get(PAGINATION_KEY),
                (Long) queryParams.get(PAGE_SIZE),
                RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS1_V01);

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS1_V01, data, pathParams, queryParams);

        return data;
    }

    /**
     * @see ISrvCardsV01#listCardTransactions2(String, String, String, String, Long)
     */
    @Override
    @GET
    @Path("/cards/{card-id}/transactions")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS2_V01, logicalID = "listCardTransactions")
    public TransactionsData listCardTransactions2(@PathParam(CARD_ID) String cardId,
                                                  @QueryParam(FROM_OPERATION_DATE) String fromOperationDate,
                                                  @QueryParam(TO_OPERATION_DATE) String toOperationDate,
                                                  @QueryParam(PAGINATION_KEY) String paginationKey,
                                                  @QueryParam(PAGE_SIZE) Long pageSize) {
        LOG.info("... called method SrvCardsV01.listTransactions ...");
        HashMap<String, Object> pathParam = new HashMap<>();
        pathParam.put(CARD_ID, cardId);

        HashMap<String, Object> queryParam = new HashMap<>();
        queryParam.put(FROM_OPERATION_DATE, fromOperationDate);
        queryParam.put(TO_OPERATION_DATE, toOperationDate);
        queryParam.put(PAGINATION_KEY, paginationKey);
        queryParam.put(PAGE_SIZE, pageSize);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS2_V01, null, pathParam, queryParam);

        TransactionsData data = listCardTransactions("listCardTransactions2", true,
                (String) pathParam.get(CARD_ID),
                (String) queryParam.get(FROM_OPERATION_DATE),
                (String) queryParam.get(TO_OPERATION_DATE),
                (String) queryParam.get(PAGINATION_KEY),
                (Long) queryParam.get(PAGE_SIZE),
                RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS2_V01);

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS2_V01, data, null, null);

        return data;
    }

    private TransactionsData listCardTransactions(final String methodName, final boolean haveFinancingType, final String cardId,
                                                  final String fromOperationDate, final String toOperationDate,
                                                  final String paginationKey, final Long pageSize,
                                                  final String registryId) {
        final TransactionsData transactionsData =
                srvIntCards.listCardTransactions(
                        listTransactionsMapper.mapInput(haveFinancingType, cardId, fromOperationDate, toOperationDate,
                                paginationKey, pageSize, registryId));

        if (transactionsData.getData() == null) {
            return null;
        }

        if (transactionsData.getPagination() == null || transactionsData.getPagination().getPageSize() == null) {
            return transactionsData;
        }

        transactionsData.setPagination(businessToolKit.getPaginationBuider()
                .setPagination(SrvCardsV01.class, methodName, uriInfo,
                        transactionsData.getPagination().getNextPage(), null,
                        transactionsData.getPagination().getPageSize(), null,
                        null, null, null).build());
        return transactionsData;
    }

    /**
     * @see ISrvCardsV01#createCardRelatedContract(String, com.bbva.pzic.cards.canonic.RelatedContract)
     */
    @Override
    @POST
    @Path("/{card-id}/related-contracts")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT, logicalID = "createCardRelatedContract")
    public RelatedContractData createCardRelatedContract(
            @PathParam(CARD_ID) final String cardId,
            final RelatedContract relatedContract) {
        LOG.info("... called method SrvCardsV01.createCardRelatedContract ...");

        HashMap<String, Object> pathParam = new HashMap<>();
        pathParam.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT, relatedContract, pathParam, null);

        RelatedContractData data = createRelatedContractMapper.mapOut(
                srvIntCards.createCardRelatedContract(
                        createRelatedContractMapper.mapIn((String) pathParam.get(CARD_ID), relatedContract)));

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT, data, null, null);

        return data;
    }
}

package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class DTOIntDetailSimulation {

    @NotNull(groups = {ValidationGroup.CreateCardsOfferSimulate.class})
    private String offerId;
    @NotNull(groups = {ValidationGroup.CreateCardsOfferSimulate.class})
    private String clientId;
    @NotNull(groups = {ValidationGroup.CreateCardsOfferSimulate.class})
    @Valid
    private DTOIntProduct product;

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }
}

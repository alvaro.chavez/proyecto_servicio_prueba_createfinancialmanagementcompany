package com.bbva.pzic.cards.facade;

/**
 * @author Entelgy
 */
public final class RegistryIds {

    public static final String SMC_REGISTRY_ID_OF_GET_CARD_TRANSACTION = "SMCPE1710139";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS = "SMCPE1710115";

    public static final String SMC_REGISTRY_ID_OF_LIST_INSTALLMENT_PLANS = "SMCPE1710159";

    public static final String SMC_REGISTRY_ID_OF_GET_CARDS_CARD_OFFER = "SMCPE1810360";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL = "SMCPE1810378";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARDS_OFFER_SIMULATE = "SMCPE1810379";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL = "SMCPE1810396";

    public static final String SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATION = "SMCPE1510171";

    public static final String SMC_REGISTRY_ID_OF_SIMULATE_CARD_INSTALLMENTS_PLAN = "SMCPE1710077";

    public static final String SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK = "SMCPE1510170";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARDS = "SMCPE1710094";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARD_LIMITS = "SMCPE2010182";

    public static final String SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA = "SMCPE1710040";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_V0 = "SMCPE1710167";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_V02 = "SMCPE1700167";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN = "SMCPE1810187";

    public static final String SMC_REGISTRY_ID_OF_GET_CARD_CONDITIONS = "SMCPE1810240";

    public static final String SMC_REGISTRY_ID_OF_GET_CARD = "SMCPE1710102";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARD_ACTIVATIONS = "SMCPE1710098";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARD_FINANCIAL_STATEMENTS = "SMCPE1810267";

    public static final String SMC_REGISTRY_ID_OF_GET_CARD_PAYMENT_METHODS = "SMCPE1710103";

    public static final String SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT = "SMCPE1810268";

    public static final String SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATIONS = "SMCPE1710097";

    public static final String SMC_REGISTRY_ID_OF_MODIFY_CARD_LIMIT = "SMCPE1710118";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARD_PROPOSALS = "SMCPE1910126";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_PROPOSAL = "SMCPE1910115";

    public static final String SMC_REGISTRY_ID_OF_UPDATE_CARD_PROPOSAL = "SMCPE1910128";

    public static final String SMC_REGISTRY_ID_OF_MODIFY_CARD_PROPOSAL = "SMCPE1910127";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_DELIVERY = "SMCPE1910132";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_V1 = "SMCPE2010160";

    public static final String SMC_REGISTRY_ID_OF_MODIFY_CARD_PIN = "SMCPE1710096";

    public static final String SMC_REGISTRY_ID_OF_MODIFY_CARD = "SMCPE1810197";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARD_SHIPMENTS = "SMCPE2020140";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_SHIPMENT_ADDRESS = "SMCPE2020141";

    public static final String SMC_REGISTRY_ID_OF_CONFIRM_CARD_SHIPMENT = "SMCPE2020144";

    public static final String SMC_REGISTRY_ID_OF_GET_CARD_SHIPMENT = "SMCPE2020143";

    public static final String SMC_REGISTRY_ID_OF_INITIALIZE_CARD_SHIPMENT = "SMCPE2020142";

    public static final String SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA_V1 = "SMCPE2010193";

    public static final String SMC_REGISTRY_ID_OF_MODIFY_CARDS_ACTIVATIONS = "SMCPE2010195";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_REPORTS = "SMGG20200411";

    public static final String SMC_REGISTRY_ID_OF_GET_CARD_CANCELLATION_VERIFICATION_V1 = "SMGG20200506";

    public static final String SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT_V1 = "SMCPE2010212";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN_V00 = "SMCPE1710076";

    public static final String SMC_REGISTRY_ID_OF_MODIFY_CARD_V00 = "SMCPE1710080";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT = "SMCPE1710007";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT_V0 = "SMCPE1810198";

    public static final String SMC_REGISTRY_ID_OF_MODIFY_PARTIAL_CARD_BLOCK = "SMCPE1710085";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD = "SMCPE1510169";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARDS_V01 = "SMCPE1710005";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS2_V01 = "SMCPE1710105";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS1_V01 = "SMCPE1710006";

    public static final String SMC_REGISTRY_ID_OF_SIMULATE_CARD_TRANSACTION_REFUND = "SMGG20203651";

    public static final String SMC_REGISTRY_ID_OF_REIMBURSE_CARD_TRANSACTION_TRANSACTION_REFUND = "SMGG20203650";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_CANCEL_REQUEST_V1 = "SMGG20203705";

    public static final String SMC_REGISTRY_ID_OF_CREATE_CARD_CANCELLATIONS_V1 = "SMPE20200293";

    public static final String SMC_REGISTRY_ID_OF_CREATE_OFFERS_GENERATE_CARDS = "SMGG20203798";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARD_RELATED_CONTRACT = "SMCPE1810368";

    public static final String SMC_REGISTRY_ID_OF_LIST_CARD_INSTALLMENTS_PLANS = "SMPE20200037";

    public static final String SMC_REGISTRY_ID_OF_GET_CARD_INSTALLMENTS_PLAN = "SMPE20200038";

    private RegistryIds() {
    }
}

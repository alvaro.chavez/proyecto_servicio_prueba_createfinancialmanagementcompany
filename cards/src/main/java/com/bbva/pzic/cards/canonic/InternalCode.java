package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "InternalCode", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "InternalCode", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class InternalCode implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier associated to the transaction type internal code that can\`t
     * be categorized and must be considered.
     */
    private String id;
    /**
     * Name associated to the transaction internal code.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
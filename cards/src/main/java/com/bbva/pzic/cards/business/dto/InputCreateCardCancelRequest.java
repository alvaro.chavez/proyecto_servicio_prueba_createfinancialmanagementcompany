package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class InputCreateCardCancelRequest {

    private String cardId;

    @NotNull(groups = ValidationGroup.CreateCardCancelRequestV1.class)
    @Valid
    private DTOIntReason reason;

    @Valid
    private DTOIntCardRequestCancel card;

    @NotNull(groups = ValidationGroup.CreateCardCancelRequestV1.class)
    private String customerId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public DTOIntReason getReason() {
        return reason;
    }

    public void setReason(DTOIntReason reason) {
        this.reason = reason;
    }

    public DTOIntCardRequestCancel getCard() {
        return card;
    }

    public void setCard(DTOIntCardRequestCancel card) {
        this.card = card;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}

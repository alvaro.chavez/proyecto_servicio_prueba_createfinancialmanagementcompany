package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanData;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Created on 3/07/2017.
 *
 * @author Entelgy
 */
public interface ICreateCardInstallmentsPlanV0Mapper {

    /**
     * Method that creates a DTO with the input data.
     *
     * @param cardId           unique card identifier
     * @param installmentsPlan Object with the data of the card
     * @return Object with the data of input
     */
    DTOIntCardInstallmentsPlan mapIn(String cardId, InstallmentsPlan installmentsPlan);

    Response mapOut(InstallmentsPlanData cardInstallmentsPlan, UriInfo uriInfo);
}

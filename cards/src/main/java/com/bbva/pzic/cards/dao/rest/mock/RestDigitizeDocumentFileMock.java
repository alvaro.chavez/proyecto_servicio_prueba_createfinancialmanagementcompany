package com.bbva.pzic.cards.dao.rest.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.dao.model.filenet.Documents;
import com.bbva.pzic.cards.dao.rest.RestDigitizeDocumentFile;
import com.bbva.pzic.cards.dao.rest.mock.stubs.ResponseDocumentsMock;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created on 27/03/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestDigitizeDocumentFileMock extends RestDigitizeDocumentFile {

    public static final String TEMPLATE_NOT_FOUND_RESPONSE = "99999997";
    public static final String WRONG_PARAMETERS_RESPONSE = "99999996";
    public static final String ERROR_RESPONSE = "99999995";

    @Override
    protected Documents connect(String urlPropertyValue, DocumentRequest entityPayload) {

        int statusCode = 200;

        String formatsPan = entityPayload.getContenido().get(0).getFormatsPan();

        if (TEMPLATE_NOT_FOUND_RESPONSE.equals(formatsPan)) {
            statusCode = 404;
        } else if (WRONG_PARAMETERS_RESPONSE.equals(formatsPan)) {
            statusCode = 400;
        } else if (ERROR_RESPONSE.equals(formatsPan)) {
            statusCode = 500;
        }
        Documents response = null;
        try {
            response = ResponseDocumentsMock.INSTANCE.buildDocumentFile();
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }

        evaluateResponse(response, statusCode);

        return response;

    }
}
package com.bbva.pzic.cards.dao.model.mpb5;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPB5</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpb5</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpb5</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPB5.D1181025.txt
 * MPB5ACTIVACION DE TARJETAS             MP        MP2CMPB5PBDMPPO MPM0B5E             MPB5  NS3000CNNNNN    SSTN    C   NNNNSNNN  NN                2017-08-10XP92512 2018-01-2317.49.58P019956 2017-08-10-15.41.39.552700XP92512 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPM0B5E.D1181025.txt
 * MPM0B5E �ENTRADA ACTIVACION DE TARJETA �F�03�00019�01�00001�IDETARJ�NUMERO DE TARJETA   �A�016�0�R�        �
 * MPM0B5E �ENTRADA ACTIVACION DE TARJETA �F�03�00019�02�00017�ESTTARJ�ESTADO DE LA TARJETA�A�001�0�R�        �
 * MPM0B5E �ENTRADA ACTIVACION DE TARJETA �F�03�00019�03�00018�CODRAZ �CODIGO DE LA RAZON  �A�002�0�O�        �
</pre></code>
 *
 * @see RespuestaTransaccionMpb5
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPB5",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpb5.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPM0B5E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpb5 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntImport {

    @NotNull(groups = {
            ValidationGroup.CreateCardsCardProposal.class,
            ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class
    })
    @Digits(integer = 10, fraction = 2, groups = {
            ValidationGroup.CreateCardsCardProposal.class,
            ValidationGroup.CreateCardsProposal.class
    })
    private BigDecimal amount;
    @NotNull(groups = {
            ValidationGroup.CreateCardsCardProposal.class,
            ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class
    })
    @Size(max = 3, groups = {
            ValidationGroup.CreateCardsCardProposal.class,
            ValidationGroup.CreateCardsProposal.class
    })
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
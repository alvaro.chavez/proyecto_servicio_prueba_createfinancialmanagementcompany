package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatement;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatementDetail;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatementProgram;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.filenet.Cliente;
import com.bbva.pzic.cards.dao.model.filenet.Contenido;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardFinancialStatementMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.encrypt.impl.CypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.converter.builtin.DateToStringConverter;
import com.bbva.pzic.cards.util.orika.converter.builtin.LongToIntegerConverter;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.bbva.pzic.cards.facade.RegistryIds;
import java.util.Collections;

/**
 * Created on 22/12/2017.
 *
 * @author Entelgy
 */
@Mapper("getCardFinancialStatementMapper")
public class GetCardFinancialStatementMapper extends ConfigurableMapper implements IGetCardFinancialStatementMapper {

    private static final Log LOG = LogFactory.getLog(GetCardFinancialStatementMapper.class);

    private static final String PARTICIPANT_TYPE_NATURAL = "N";
    private static final String PARTICIPANT_RELATIONSHIP_TYPE = "TITULAR";
    private static final String PARTICIPANT_RELATIONSHIP_ORDER_FIRST = "PRIMERO";
    private static final String PARTICIPANT_RELATIONSHIP_ORDER_SECOND = "SEGUNDO";
    private static final String PUNTOS_VIDA_ID = "PV";
    private static final String PUNTOS_VIDA_NAME = "PUNTOS_VIDA";
    private static final String LIFEMILES_ID = "LM";
    private static final String LIFEMILES_NAME = "LIFEMILES";

    private static final String TEA_COMPRAS_SOLES = "TEA_COMPRAS_SOLES";
    private static final String TEA_COMPRAS_DOLARES = "TEA_COMPRAS_DOLARES";
    private static final String TEA_AVANCE_SOLES = "TEA_AVANCE_SOLES";
    private static final String TEA_AVANCE_DOLARES = "TEA_AVANCE_DOLARES";
    private static final String TEA_COMPENSATORIO_SOLES = "TEA_COMPENSATORIO_SOLES";
    private static final String TEA_COMPENSATORIO_DOLARES = "TEA_COMPENSATORIO_DOLARES";
    private static final String TEA_MORATORIO_SOLES = "TEA_MORATORIO_SOLES";
    private static final String TEA_MORATORIO_DOLARES = "TEA_MORATORIO_DOLARES";

    private static final String FIELD_CURRENCY = "currency";
    private static final String TRUE = "TRUE";

    @Autowired
    private ConfigurationManager configurationManager;

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    private CypherTool cypherTool;

    @Override
    protected void configure(MapperFactory factory) {
        factory.getConverterFactory().registerConverter(new LongToIntegerConverter());
        factory.getConverterFactory().registerConverter(new DateToStringConverter("yyyy-MM-dd"));

        factory.classMap(Contenido.class, DTOIntCardStatementDetail.class)
                .field("cardId", "cardId")
                .field("formatsPan", "formatsPan")
                .field("productName", "productName")
                .field("branchName", "branchName")
                .field("currencyId", FIELD_CURRENCY)
                .field("participantType", "participantType")
                /*.field("programId", "program.lifeMilesNumber")
                .field("programCurrentLoyaltyUnitsBalance", "program.monthPoints")
                .field("programLifetimeAccumulatedLoyaltyUnits", "program.totalPoints")
                .field("programBonus", "program.bonus")*/
                .field("endDate", "endDate")
                .field("payTypeName", "payType")
                .field("creditLimitValueAmount", "creditLimit")
                .field("creditLimitValueCurrency", FIELD_CURRENCY)
                .field("paymentDate", "paymentDate")
                .field("disposedBalanceLocalCurrencyCurrency", "disposedBalanceLocalCurrency.currency")
                .field("disposedBalanceLocalCurrencyAmount", "disposedBalanceLocalCurrency.amount")
                .field("disposedBalanceCurrency", "disposedBalance.currency")
                .field("disposedBalanceAmount", "disposedBalance.amount")
                .field("totalTransactions", "totalTransactions")
                .field("outstandingBalanceLocalCurrencyAmount", "extraPayments.outstandingBalanceLocalCurrency.amount")
                .field("minimunPaymentMinimumCapitalLocalCurrencyAmount", "extraPayments.minimumCapitalLocalCurrency.amount")
                .field("interestsBalanceLocalCurrencyAmount", "extraPayments.interestsBalanceLocalCurrency.amount")
                .field("feesBalanceLocalCurrencyAmount", "extraPayments.feesBalanceLocalCurrency.amount")
                .field("financingTransactionLocalBalanceAmount", "extraPayments.financingTransactionLocalBalance.amount")
                .field("minimumPaymentInvoiceMinimumAmountLocalCurrencyAmount", "extraPayments.invoiceMinimumAmountLocalCurrency.amount")
                .field("invoiceAmountLocalCurrencyAmount", "extraPayments.invoiceAmountLocalCurrency.amount")
                .field("outstandingBalanceAmount", "extraPayments.outstandingBalance.amount")
                .field("minimunPaymentMinimumCapitalCurrencyAmount", "extraPayments.minimumCapitalCurrency.amount")
                .field("interestsBalanceAmount", "extraPayments.interestsBalance.amount")
                .field("feesBalanceAmount", "extraPayments.feesBalance.amount")
                .field("financingTransactionBalanceAmount", "extraPayments.financingTransactionBalance.amount")
                .field("minimumPaymentInvoiceMinimumAmountAmount", "extraPayments.invoiceMinimumAmountCurrency.amount")
                .field("invoiceAmountAmount", "extraPayments.invoiceAmount.amount")
                .field("totalDebtLocalCurrencyAmount", "totalDebtLocalCurrency.amount")
                .field("totalDebtAmount", "totalDebt.amount")
                .field("totalMonthsAmortizationMinimum", "totalMonthsAmortizationMinimum")
                .field("totalMonthsAmortizationMinimumFull", "totalMonthsAmortizationMinimumFull")
                //Limits
                .field("limitsFinancingDisposedBalanceAmount", "limits.financingDisposedBalance")
                .field("limitsFinancingDisposedBalanceCurrency", FIELD_CURRENCY)
                .field("limitsAvailableBalanceAmount", "limits.availableBalance")
                .field("limitsAvailableBalanceCurrency", FIELD_CURRENCY)
                .field("limitsDisposedBalanceAmount", "limits.disposedBalance")
                .field("limitsDisposedBalanceCurrency", FIELD_CURRENCY)
                //InterestRates
                .field("interestRates1Percentage", "interest.purchasePEN")
                .field("interestRates2Percentage", "interest.purchaseUSD")
                .field("interestRates3Percentage", "interest.advancedPEN")
                .field("interestRates4Percentage", "interest.advancedUSD")
                .field("interestRates5Percentage", "interest.countervailingPEN")
                .field("interestRates6Percentage", "interest.countervailingUSD")
                .field("interestRates7Percentage", "interest.arrearsPEN")
                .field("interestRates8Percentage", "interest.arrearsUSD")
                //Address
                .field("participant1AddresessName", "address.name")
                .field("participant1AddresessStreetTypeName", "address.streetType")
                .field("participant1AddresessState", "address.state")
                .field("participant1AddresessUbigeo", "address.ubigeo")
                .register();

        factory.classMap(Contenido.class, DTOIntCardStatementProgram.class)
                .field("programId", "lifeMilesNumber")
                .field("programCurrentLoyaltyUnitsBalance", "monthPoints")
                .field("programLifetimeAccumulatedLoyaltyUnits", "totalPoints")
                .field("programBonus", "bonus")
                .register();
    }

    @Override
    public InputGetCardFinancialStatement mapIn(final String cardId, final String financialStatementId, String contentType) {
        LOG.info("... called method GetCardFinancialStatementMapper.mapIn ...");
        InputGetCardFinancialStatement inputGetCardFinancialStatement = new InputGetCardFinancialStatement();
        inputGetCardFinancialStatement.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT));
        inputGetCardFinancialStatement.setFinancialStatementId(financialStatementId);
        inputGetCardFinancialStatement.setContentType(contentType);

        return inputGetCardFinancialStatement;
    }

    @Override
    public DocumentRequest mapOut(DTOIntCardStatement dtoInt) {
        LOG.info("... called method GetCardFinancialStatementMapper.mapOut ...");
        if (dtoInt.getDetail() == null) {
            return null;
        }

        final DTOIntCardStatementDetail detail = dtoInt.getDetail();
        final DocumentRequest documentRequest = new DocumentRequest();
        final Contenido contenido = map(detail, Contenido.class);

        mapParticipants(contenido, detail);
        mapChargedAccounts(contenido, detail);

        if (detail.getProgram() != null && PARTICIPANT_TYPE_NATURAL.equalsIgnoreCase(detail.getParticipantType())) {
            map(detail.getProgram(), contenido);
            mapProgramType(contenido);
        }

        mapInterestsRate(contenido, detail);
        mapLocalCurrency(contenido);
        mapCurrency(contenido);

        contenido.setRowCardStatement(dtoInt.getRowCardStatement());
        contenido.setMessage(StringUtils.join(dtoInt.getMessages(), " "));
        documentRequest.setNumeroContrato(dtoInt.getDetail().getAccountNumberPEN());
        documentRequest.setContenido(Collections.singletonList(contenido));
        documentRequest.setIdContrato(getProperty("servicing.cards.contractId"));
        documentRequest.setIdGrupo(getProperty("servicing.cards.groupId"));
        String user = serviceInvocationContext.getUser();
        if (user == null) {
            return documentRequest;
        }
        Cliente cliente = new Cliente();
        cliente.setCodigoCentral(user);
        documentRequest.setListaClientes(Collections.singletonList(cliente));
        return documentRequest;
    }

    private String getProperty(final String key) {
        String value = configurationManager.getProperty(key);
        LOG.debug(String.format("Loaded property '%s = %s'", key, value));
        return value;
    }

    private void mapLocalCurrency(Contenido documentRequest) {
        if (documentRequest.getOutstandingBalanceLocalCurrencyAmount() != null) {
            documentRequest.setOutstandingBalanceLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (documentRequest.getMinimunPaymentMinimumCapitalLocalCurrencyAmount() != null) {
            documentRequest.setMinimunPaymentMinimumCapitalLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (documentRequest.getInterestsBalanceLocalCurrencyAmount() != null) {
            documentRequest.setInterestsBalanceLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (documentRequest.getFeesBalanceLocalCurrencyAmount() != null) {
            documentRequest.setFeesBalanceLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (documentRequest.getFinancingTransactionLocalBalanceAmount() != null) {
            documentRequest.setFinancingTransactionLocalBalanceCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (documentRequest.getMinimumPaymentInvoiceMinimumAmountLocalCurrencyAmount() != null) {
            documentRequest.setMinimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (documentRequest.getInvoiceAmountLocalCurrencyAmount() != null) {
            documentRequest.setInvoiceAmountLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
        if (documentRequest.getTotalDebtLocalCurrencyAmount() != null) {
            documentRequest.setTotalDebtLocalCurrencyCurrency(DTOIntCardStatementDetail.PEN);
        }
    }

    private void mapCurrency(Contenido documentRequest) {
        if (documentRequest.getOutstandingBalanceAmount() != null) {
            documentRequest.setOutstandingBalanceCurrency(DTOIntCardStatementDetail.USD);
        }
        if (documentRequest.getMinimunPaymentMinimumCapitalCurrencyAmount() != null) {
            documentRequest.setMinimunPaymentMinimumCapitalCurrencyCurrency(DTOIntCardStatementDetail.USD);
        }
        if (documentRequest.getInterestsBalanceAmount() != null) {
            documentRequest.setInterestsBalanceCurrency(DTOIntCardStatementDetail.USD);
        }
        if (documentRequest.getFeesBalanceAmount() != null) {
            documentRequest.setFeesBalanceCurrency(DTOIntCardStatementDetail.USD);
        }
        if (documentRequest.getFinancingTransactionBalanceAmount() != null) {
            documentRequest.setFinancingTransactionBalanceCurrency(DTOIntCardStatementDetail.USD);
        }
        if (documentRequest.getMinimumPaymentInvoiceMinimumAmountAmount() != null) {
            documentRequest.setMinimumPaymentInvoiceMinimumAmountCurrency(DTOIntCardStatementDetail.USD);
        }
        if (documentRequest.getInvoiceAmountAmount() != null) {
            documentRequest.setInvoiceAmountCurrency(DTOIntCardStatementDetail.USD);
        }
        if (documentRequest.getTotalDebtAmount() != null) {
            documentRequest.setTotalDebtCurrency(DTOIntCardStatementDetail.USD);
        }
    }

    private void mapChargedAccounts(final Contenido documentRequest, final DTOIntCardStatementDetail detail) {
        if (detail.getAccountNumberPEN() == null && detail.getAccountNumberUSD() == null) {
            return;
        }

        if (detail.getAccountNumberPEN() != null) {
            documentRequest.setChargedAccount1Id(detail.getAccountNumberPEN());
            documentRequest.setChargedAccount1Currency(DTOIntCardStatementDetail.PEN);
        }
        if (detail.getAccountNumberUSD() != null) {
            documentRequest.setChargedAccount2Id(detail.getAccountNumberUSD());
            documentRequest.setChargedAccount2Currency(DTOIntCardStatementDetail.USD);
        }
    }

    private void mapProgramType(final Contenido documentRequest) {
        if (documentRequest.getProgramId() == null) {
            documentRequest.setProgramTypeId(PUNTOS_VIDA_ID);
            documentRequest.setProgramTypeName(PUNTOS_VIDA_NAME);
            documentRequest.setProgramBonus(null);
        } else {
            documentRequest.setProgramTypeId(LIFEMILES_ID);
            documentRequest.setProgramTypeName(LIFEMILES_NAME);
            documentRequest.setProgramLifetimeAccumulatedLoyaltyUnits(null);
        }
    }

    private void mapInterestsRate(final Contenido documentRequest, final DTOIntCardStatementDetail detail) {
        if (detail.getInterest() == null) {
            return;
        }

        if (detail.getInterest().getPurchasePEN() != null) {
            documentRequest.setInterestRates1TypeId("1");
            documentRequest.setInterestRates1TypeName(TEA_COMPRAS_SOLES);
        }

        if (detail.getInterest().getPurchaseUSD() != null) {
            documentRequest.setInterestRates2TypeId("2");
            documentRequest.setInterestRates2TypeName(TEA_COMPRAS_DOLARES);
        }

        if (detail.getInterest().getAdvancedPEN() != null) {
            documentRequest.setInterestRates3TypeId("3");
            documentRequest.setInterestRates3TypeName(TEA_AVANCE_SOLES);
        }

        if (detail.getInterest().getAdvancedUSD() != null) {
            documentRequest.setInterestRates4TypeId("4");
            documentRequest.setInterestRates4TypeName(TEA_AVANCE_DOLARES);
        }

        if (detail.getInterest().getCountervailingPEN() != null) {
            documentRequest.setInterestRates5TypeId("5");
            documentRequest.setInterestRates5TypeName(TEA_COMPENSATORIO_SOLES);
        }

        if (detail.getInterest().getCountervailingUSD() != null) {
            documentRequest.setInterestRates6TypeId("6");
            documentRequest.setInterestRates6TypeName(TEA_COMPENSATORIO_DOLARES);
        }

        if (detail.getInterest().getArrearsPEN() != null) {
            documentRequest.setInterestRates7TypeId("7");
            documentRequest.setInterestRates7TypeName(TEA_MORATORIO_SOLES);
        }

        if (detail.getInterest().getArrearsUSD() != null) {
            documentRequest.setInterestRates8TypeId("8");
            documentRequest.setInterestRates8TypeName(TEA_MORATORIO_DOLARES);
        }
    }

    private void mapParticipants(final Contenido documentRequest, final DTOIntCardStatementDetail detail) {

        documentRequest.setParticipant1RelationshipTypeName(PARTICIPANT_RELATIONSHIP_TYPE);
        documentRequest.setParticipant1RelationshipOrder(PARTICIPANT_RELATIONSHIP_ORDER_FIRST);
        documentRequest.setParticipant1Name(detail.getParticipantNameFirst());
        documentRequest.setParticipant1AddresessIsMainAddress(TRUE);

        documentRequest.setParticipant2RelationshipTypeName(PARTICIPANT_RELATIONSHIP_TYPE);
        documentRequest.setParticipant2RelationshipOrder(PARTICIPANT_RELATIONSHIP_ORDER_SECOND);
        documentRequest.setParticipant2Name(detail.getParticipantNameSecond());
    }

}

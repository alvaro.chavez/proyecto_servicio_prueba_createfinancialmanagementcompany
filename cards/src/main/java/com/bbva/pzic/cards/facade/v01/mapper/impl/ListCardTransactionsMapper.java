package com.bbva.pzic.cards.facade.v01.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions;
import com.bbva.pzic.cards.facade.v01.mapper.IListCardTransactionsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 06/02/2017.
 *
 * @author Entelgy
 */
@Mapper
public class ListCardTransactionsMapper implements IListCardTransactionsMapper {

    private static final Log LOG = LogFactory.getLog(ListCardTransactionsMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    /**
     * @see com.bbva.pzic.cards.facade.v01.mapper.IListCardTransactionsMapper#mapInput(boolean, String, String, String, String, Long, String)
     */
    @Override
    public DTOInputListCardTransactions mapInput(boolean haveFinancingType, String cardId, String fromOperationDate, String toOperationDate, String paginationKey, Long pageSize, String registryId) {
        LOG.info("... called method ListTransactionsMapper.mapInput ...");
        final DTOInputListCardTransactions dtoInputListCardTransactions = new DTOInputListCardTransactions();
        dtoInputListCardTransactions.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, registryId));
        dtoInputListCardTransactions.setFromOperationDate(fromOperationDate);
        dtoInputListCardTransactions.setToOperationDate(toOperationDate);
        dtoInputListCardTransactions.setPaginationKey(paginationKey);
        dtoInputListCardTransactions.setPageSize(pageSize);
        dtoInputListCardTransactions.setHaveFinancingType(haveFinancingType);
        dtoInputListCardTransactions.setRegistryId(registryId);
        return dtoInputListCardTransactions;
    }

}

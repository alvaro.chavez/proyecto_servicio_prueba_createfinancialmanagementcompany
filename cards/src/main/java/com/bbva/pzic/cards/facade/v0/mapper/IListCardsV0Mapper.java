package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.canonic.Card;

import java.util.List;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
public interface IListCardsV0Mapper {

    /**
     * Input Mapping to DTO
     *
     * @param customerId        Customer ID
     * @param cardTypeId        Type card ID
     * @param physicalSupportId Physical Support ID
     * @param statusId          Status ID
     * @param paginationKey     Pagination ID
     * @param pageSize          Size of pagination
     * @return DTOIntListCards
     */
    DTOIntListCards mapIn(String customerId, List<String> cardTypeId, String physicalSupportId,
                          List<String> statusId, List<String> participantsParticipantTypeId, String paginationKey, Integer pageSize);

    /**
     * Output Mapping to CardsList
     *
     * @param listCards  cards Information
     * @param pagination pagination
     * @return cards list
     */
    ServiceResponse<List<Card>> mapOut(DTOOutListCards listCards, Pagination pagination);
}

package com.bbva.pzic.cards.dao.model.mpwt;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPM0DET</code> de la transacci&oacute;n <code>MPWT</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPM0DET")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPM0DET {

	/**
	 * <p>Campo <code>FECPAGO</code>, &iacute;ndice: <code>1</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 1, nombre = "FECPAGO", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecpago;

	/**
	 * <p>Campo <code>CAPITAL</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 2, nombre = "CAPITAL", tipo = TipoCampo.DECIMAL, longitudMinima = 11, longitudMaxima = 11, decimales = 2)
	private BigDecimal capital;

	/**
	 * <p>Campo <code>DIVCAPC</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "DIVCAPC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divcapc;

	/**
	 * <p>Campo <code>INTERES</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 4, nombre = "INTERES", tipo = TipoCampo.DECIMAL, longitudMinima = 11, longitudMaxima = 11, decimales = 2)
	private BigDecimal interes;

	/**
	 * <p>Campo <code>DIVINTE</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "DIVINTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divinte;

	/**
	 * <p>Campo <code>COMISIO</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 6, nombre = "COMISIO", tipo = TipoCampo.DECIMAL, longitudMinima = 11, longitudMaxima = 11, decimales = 2)
	private BigDecimal comisio;

	/**
	 * <p>Campo <code>DIVCOMI</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "DIVCOMI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divcomi;

	/**
	 * <p>Campo <code>IMPCUOT</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 8, nombre = "IMPCUOT", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
	private BigDecimal impcuot;

	/**
	 * <p>Campo <code>DIVCUOT</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "DIVCUOT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divcuot;

}
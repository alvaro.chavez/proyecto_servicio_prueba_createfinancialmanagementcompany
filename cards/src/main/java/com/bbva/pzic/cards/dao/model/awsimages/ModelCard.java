package com.bbva.pzic.cards.dao.model.awsimages;

/**
 * Created on 30/07/2019.
 *
 * @author Entelgy
 */
public class ModelCard {

    private String bin;
    private String codProd;
    private ModelType type;
    private String issueDate;
    private Boolean isBackImage;

    public ModelCard() {
        this.type = new ModelType();
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getCodProd() {
        return codProd;
    }

    public void setCodProd(String codProd) {
        this.codProd = codProd;
    }

    public ModelType getType() {
        return type;
    }

    public void setType(ModelType type) {
        this.type = type;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public Boolean getIsBackImage() {
        return isBackImage;
    }

    public void setIsBackImage(Boolean isBackImage) {
        this.isBackImage = isBackImage;
    }
}

package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntStatementList;
import com.bbva.pzic.cards.business.dto.InputListCardFinancialStatements;
import com.bbva.pzic.cards.canonic.StatementList;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardFinancialStatementsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.bbva.pzic.cards.facade.RegistryIds;

/**
 * Created on 17/07/2018.
 *
 * @author Entelgy
 */
@Mapper
public class ListCardFinancialStatementsMapper implements IListCardFinancialStatementsMapper {

    private static final Log LOG = LogFactory.getLog(ListCardFinancialStatementsMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    /**
     * {@inheritDoc}
     */
    @Override
    public InputListCardFinancialStatements mapIn(final String cardId,
                                                  final Boolean isActive, final String paginationKey,
                                                  final Integer pageSize) {
        LOG.info("... called method ListCardFinancialStatementsMapper.mapIn ...");
        InputListCardFinancialStatements input = new InputListCardFinancialStatements();
        input.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_FINANCIAL_STATEMENTS));
        input.setIsActive(isActive);
        input.setPaginationKey(paginationKey);
        input.setPageSize(pageSize);
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StatementList mapOut(final DTOIntStatementList statementList) {
        LOG.info("... called method ListCardFinancialStatementsMapper.mapOut ...");
        if (statementList == null || statementList.getData() == null || statementList.getData().isEmpty()) {
            return null;
        }
        StatementList response = new StatementList();
        response.setData(statementList.getData());
        return response;
    }
}

package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.PaymentMethod;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardPaymentMethodsMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * Created on 24/10/2017.
 *
 * @author Entelgy
 */
@Component
public class GetCardPaymentMethodsMapper implements IGetCardPaymentMethodsMapper {

    @Override
    public DTOIntCard mapIn(final String cardId) {
        DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardId(cardId);
        return dtoIntCard;
    }

    @Override
    public ServiceResponse<List<PaymentMethod>> mapOut(final PaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            return null;
        }

        return ServiceResponse.data(Collections.singletonList(paymentMethod)).build();
    }
}

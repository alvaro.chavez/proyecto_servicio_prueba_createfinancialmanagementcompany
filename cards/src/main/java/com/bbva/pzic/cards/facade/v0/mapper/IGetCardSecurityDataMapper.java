package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.canonic.SecurityData;
import com.bbva.pzic.cards.canonic.SecurityDataArray;

/**
 * Created on 09/05/2017.
 *
 * @author Entelgy
 */
public interface IGetCardSecurityDataMapper {

    InputGetCardSecurityData mapIn(String cardId, String publicKey);

    SecurityDataArray mapOut(SecurityData securityData);
}

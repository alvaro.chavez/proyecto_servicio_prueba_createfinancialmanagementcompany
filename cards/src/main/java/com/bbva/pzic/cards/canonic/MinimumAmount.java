package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "minimumAmount", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "minimumAmount", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class MinimumAmount implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Monetary amount.
     */
    private BigDecimal amount;
    /**
     * String based on ISO-4217 for specifying the currency related to the monetary amount.
     */
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}

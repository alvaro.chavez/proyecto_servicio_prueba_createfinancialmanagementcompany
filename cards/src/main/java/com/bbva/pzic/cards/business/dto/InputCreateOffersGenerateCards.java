package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import java.util.List;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
public class InputCreateOffersGenerateCards {

    private String bcsDeviceScreenSize;
    @Valid
    private List<DTOIntQuestion> questions;

    public String getBcsDeviceScreenSize() {
        return bcsDeviceScreenSize;
    }

    public void setBcsDeviceScreenSize(String bcsDeviceScreenSize) {
        this.bcsDeviceScreenSize = bcsDeviceScreenSize;
    }

    public List<DTOIntQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<DTOIntQuestion> questions) {
        this.questions = questions;
    }
}

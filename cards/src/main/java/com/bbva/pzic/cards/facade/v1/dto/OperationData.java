package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;

@XmlRootElement(name = "operationData", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "operationData", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class OperationData implements Serializable {

    private static final long serialVersionUID = 1L;

    private String operationNumber;

    private String referenceNumber;

    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar operationDate;

    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar cancellationDate;

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Calendar getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Calendar operationDate) {
        this.operationDate = operationDate;
    }

    public Calendar getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Calendar cancellationDate) {
        this.cancellationDate = cancellationDate;
    }
}

package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCashAdvancesSearchCriteria;
import com.bbva.pzic.cards.business.dto.DTOIntListCashAdvances;
import com.bbva.pzic.cards.facade.v0.dto.CashAdvances;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardCashAdvancesMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper
public class ListCardCashAdvancesMapper implements IListCardCashAdvancesMapper {

    private static final Log LOG = LogFactory.getLog(ListCardCashAdvancesMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntCashAdvancesSearchCriteria mapInput(final String cardId) {
        LOG.info("----- Dentro de ListCardCashAdvancesMapper.mapInput() -----");
        DTOIntCashAdvancesSearchCriteria dtoIntCashAdvancesSearchCriteria = new DTOIntCashAdvancesSearchCriteria();
        dtoIntCashAdvancesSearchCriteria.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.CARDID));
        return dtoIntCashAdvancesSearchCriteria;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<List<CashAdvances>> mapOutput(final DTOIntListCashAdvances dtoIntListCashAdvances) {

        LOG.info("----- Dentro de ListCardCashAdvancesMapper.mapOutput() -----");

        if (dtoIntListCashAdvances == null || CollectionUtils.isEmpty(dtoIntListCashAdvances.getData())) {
            return null;
        }

        return ServiceResponse.data(dtoIntListCashAdvances.getData()).build();

    }

}

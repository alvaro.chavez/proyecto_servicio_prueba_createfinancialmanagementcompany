package com.bbva.pzic.cards.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "shipmentAddress", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "shipmentAddress", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ShipmentAddress implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier of the address.
     */
    @DatoAuditable(omitir = true)
    private String id;
    /**
     * Shipment address type identifier..
     */
    private String addressType;

    private Location location;

    private Destination destination;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }
}

package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 14/05/2020.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Limits", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Limits", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Limits implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Limit identifier.
     */
    private String id;
    /**
     * Monetary restriction related to the current limit. This amount may be provided in several currencies (depending on the country).
     */
    private List<AmountLimit> amountLimits;
    /**
     * Range of values that may have a certain limit of the card.
     */
    private AllowedInterval allowedInterval;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<AmountLimit> getAmountLimits() {
        return amountLimits;
    }

    public void setAmountLimits(List<AmountLimit> amountLimits) {
        this.amountLimits = amountLimits;
    }

    public AllowedInterval getAllowedInterval() {
        return allowedInterval;
    }

    public void setAllowedInterval(AllowedInterval allowedInterval) {
        this.allowedInterval = allowedInterval;
    }
}

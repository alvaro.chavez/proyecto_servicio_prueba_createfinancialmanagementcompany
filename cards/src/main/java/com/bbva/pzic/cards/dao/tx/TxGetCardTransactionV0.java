package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputGetCardTransaction;
import com.bbva.pzic.cards.canonic.TransactionData;
import com.bbva.pzic.cards.dao.model.mpgm.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardTransactionV0Mapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.FourfoldOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 22/12/2017.
 *
 * @author Entelgy
 */
@Component("txGetCardTransactionV0")
public class TxGetCardTransactionV0
        extends FourfoldOutputFormat<InputGetCardTransaction, FormatoMPMENGM, TransactionData, FormatoMPMS1GM, FormatoMPMS2GM, FormatoMPMS3GM, FormatoMPMS4GM> {

    @Resource(name = "txGetCardTransactionV0Mapper")
    private ITxGetCardTransactionV0Mapper transactionV0Mapper;

    @Autowired
    public TxGetCardTransactionV0(@Qualifier("transaccionMpgm") InvocadorTransaccion<PeticionTransaccionMpgm, RespuestaTransaccionMpgm> transaction) {
        super(transaction, PeticionTransaccionMpgm::new, TransactionData::new, FormatoMPMS1GM.class, FormatoMPMS2GM.class, FormatoMPMS3GM.class, FormatoMPMS4GM.class);
    }

    @Override
    protected FormatoMPMENGM mapInput(InputGetCardTransaction inputGetCardTransaction) {
        return transactionV0Mapper.mapIn(inputGetCardTransaction);
    }

    @Override
    protected TransactionData mapFirstOutputFormat(FormatoMPMS1GM formatoMPMS1GM, InputGetCardTransaction inputGetCardTransaction, TransactionData dtoOut) {
        return transactionV0Mapper.mapOut(formatoMPMS1GM, dtoOut);
    }

    @Override
    protected TransactionData mapSecondOutputFormat(FormatoMPMS2GM formatoMPMS2GM, InputGetCardTransaction inputGetCardTransaction, TransactionData dtoOut) {
        return transactionV0Mapper.mapOut2(formatoMPMS2GM, dtoOut);
    }

    @Override
    protected TransactionData mapThirdOutputFormat(FormatoMPMS3GM formatoMPMS3GM, InputGetCardTransaction inputGetCardTransaction, TransactionData dtoOut) {
        return transactionV0Mapper.mapOut3(formatoMPMS3GM, dtoOut);
    }

    @Override
    protected TransactionData mapFourthOutputFormat(FormatoMPMS4GM formatoMPMS4GM, InputGetCardTransaction inputGetCardTransaction, TransactionData dtoOut) {
        return transactionV0Mapper.mapOut4(formatoMPMS4GM, dtoOut);
    }
}

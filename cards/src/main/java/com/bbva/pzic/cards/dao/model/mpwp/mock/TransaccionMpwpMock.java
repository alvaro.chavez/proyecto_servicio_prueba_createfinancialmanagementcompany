package com.bbva.pzic.cards.dao.model.mpwp.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.dao.model.mpwp.PeticionTransaccionMpwp;
import com.bbva.pzic.cards.dao.model.mpwp.RespuestaTransaccionMpwp;
import org.springframework.stereotype.Component;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpwp")
public class TransaccionMpwpMock implements InvocadorTransaccion<PeticionTransaccionMpwp, RespuestaTransaccionMpwp> {

    @Override
    public RespuestaTransaccionMpwp invocar(PeticionTransaccionMpwp peticionTransaccionMpwp) {
        RespuestaTransaccionMpwp response = new RespuestaTransaccionMpwp();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        return response;
    }

    @Override
    public RespuestaTransaccionMpwp invocarCache(PeticionTransaccionMpwp peticionTransaccionMpwp) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

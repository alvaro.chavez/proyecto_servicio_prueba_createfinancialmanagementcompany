package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntReason;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancellations;
import com.bbva.pzic.cards.dao.model.mpcn.FormatoMPME1NC;
import com.bbva.pzic.cards.dao.model.mpcn.FormatoMPMS1NC;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardCancellationsV1Mapper;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.Converter;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.bbva.pzic.cards.util.Converter.dateToDateTime;
import static com.bbva.pzic.cards.util.Converter.isAllEmpty;
import static com.bbva.pzic.cards.util.Enums.*;

@Component
public class TxCreateCardCancellationsV1Mapper implements ITxCreateCardCancellationsV1Mapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public FormatoMPME1NC mapIn(final InputCreateCardCancellations input) {
        FormatoMPME1NC formatoMPME1NC = new FormatoMPME1NC();
        formatoMPME1NC.setCodtar(input.getCardId());
        formatoMPME1NC.setCodraz(mapInReasonId(input.getReason()));
        return formatoMPME1NC;
    }

    private String mapInReasonId(final DTOIntReason reason) {
        if (reason == null) {
            return null;
        }
        return reason.getId();
    }

    @Override
    public Cancellation mapOutFormatoMPMS1NC(final FormatoMPMS1NC format) {
        if (Converter.isAllEmpty(format.getNumope(),
                format.getNumope(), format.getFechope(),
                format.getFechcan(), format.getEstcan(), format.getCodraz(),
                format.getNumtar(), format.getNumtar(),
                format.getIdtnuta(), format.getDetnuta(),
                format.getTipprod(), format.getDescpro(),
                format.getDivtar(), format.getInmopr())) {
            return null;
        }
        Cancellation cancellation = new Cancellation();
        cancellation.setId(format.getNumope());
        cancellation.setOperationData(mapOutOperationData(format));
        cancellation.setStatus(mapOutStatus(format));
        cancellation.setReason(mapOutReason(format));
        cancellation.setCard(mapOutCard(format));
        return cancellation;
    }

    private OperationData mapOutOperationData(final FormatoMPMS1NC format) {
        if (Converter.isAllEmpty(format.getNumope(), format.getFechope(), format.getFechcan())) {
            return null;
        }
        OperationData operationData = new OperationData();
        operationData.setOperationNumber(format.getNumope());
        operationData.setOperationDate(dateToDateTime(format.getFechope()));
        operationData.setCancellationDate(dateToDateTime(format.getFechcan()));
        return operationData;
    }

    private Status mapOutStatus(final FormatoMPMS1NC format) {
        if (format.getEstcan() == null) {
            return null;
        }
        Status status = new Status();
        status.setId(translator.translateBackendEnumValueStrictly(CARD_CANCELLATIONS_STATUSID, format.getEstcan()));
        return status;
    }

    private Reason mapOutReason(final FormatoMPMS1NC format) {
        if (format.getCodraz() == null) {
            return null;
        }
        Reason reason = new Reason();
        reason.setId(translator.translateBackendEnumValueStrictly(CARD_CANCELLATIONS_REASONID, format.getCodraz()));
        return reason;
    }

    private CardCancellation mapOutCard(final FormatoMPMS1NC format) {
        if (Converter.isAllEmpty(format.getNumtar(), format.getIdtnuta(),
                format.getDetnuta(), format.getTipprod(), format.getDescpro(),
                format.getDivtar(), format.getInmopr())) {
            return null;
        }
        CardCancellation cardCancellation = new CardCancellation();
        cardCancellation.setId(format.getNumtar());
        cardCancellation.setNumber(format.getNumtar());
        cardCancellation.setNumberType(mapOutNumberType(format));
        cardCancellation.setProduct(mapOutProduct(format));
        cardCancellation.setCurrencies(mapOutCurrencies(format));
        return cardCancellation;
    }

    private NumberType mapOutNumberType(final FormatoMPMS1NC format) {
        if (isAllEmpty(format.getIdtnuta(), format.getDetnuta())) {
            return null;
        }
        NumberType numberType = new NumberType();
        numberType.setId(translator.translateBackendEnumValueStrictly(CARDS_NUMBERTYPE_ID, format.getIdtnuta()));
        numberType.setDescription(format.getDetnuta());
        return numberType;
    }

    private Product mapOutProduct(final FormatoMPMS1NC format) {
        if (isAllEmpty(format.getTipprod(), format.getDescpro())) {
            return null;
        }
        Product product = new Product();
        product.setId(format.getTipprod());
        product.setDescription(format.getDescpro());
        return product;
    }

    private Currency mapOutCurrencies(final FormatoMPMS1NC format) {
        if (isAllEmpty(format.getDivtar(), format.getInmopr())) {
            return null;
        }
        Currency currency = new Currency();
        currency.setCurrency(format.getDivtar());
        currency.setIsMajor(Converter.stringToBoolean(format.getInmopr(), "1", "0"));
        return currency;
    }
}

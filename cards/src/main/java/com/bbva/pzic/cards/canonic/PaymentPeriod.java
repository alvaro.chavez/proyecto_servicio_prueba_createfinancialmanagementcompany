package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 17/12/2018.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "paymentPeriod", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "paymentPeriod", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentPeriod implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Period identifier.
     */
    private String id;
    /**
     * Period name.
     */
    private String name;
    /**
     * Period available for the payment to be done.
     */
    private List<EndDay> endDays;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EndDay> getEndDays() {
        return endDays;
    }

    public void setEndDays(List<EndDay> endDays) {
        this.endDays = endDays;
    }
}

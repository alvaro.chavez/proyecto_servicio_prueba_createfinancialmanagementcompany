package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class DTOIntProduct {

    @NotNull(groups = {ValidationGroup.CreateCardsOfferSimulate.class,
            ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    private String id;

    private String name;

    @Valid
    private DTOIntProductType productType;

    @NotNull(groups = {ValidationGroup.CreateCardsOfferSimulate.class})
    @Valid
    private DTOIntSubProduct subproduct;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntSubProduct getSubproduct() {
        return subproduct;
    }

    public void setSubproduct(DTOIntSubProduct subproduct) {
        this.subproduct = subproduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntProductType getProductType() {
        return productType;
    }

    public void setProductType(DTOIntProductType productType) {
        this.productType = productType;
    }
}

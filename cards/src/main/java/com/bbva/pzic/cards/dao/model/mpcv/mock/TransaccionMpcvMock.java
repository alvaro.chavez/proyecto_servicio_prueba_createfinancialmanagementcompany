package com.bbva.pzic.cards.dao.model.mpcv.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpcv.FormatoMPMENCV;
import com.bbva.pzic.cards.dao.model.mpcv.PeticionTransaccionMpcv;
import com.bbva.pzic.cards.dao.model.mpcv.RespuestaTransaccionMpcv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MPCV</code>
 *
 * @see com.bbva.pzic.cards.dao.model.mpcv.PeticionTransaccionMpcv
 * @see com.bbva.pzic.cards.dao.model.mpcv.RespuestaTransaccionMpcv
 */
@Component("transaccionMpcv")
public class TransaccionMpcvMock implements InvocadorTransaccion<PeticionTransaccionMpcv, RespuestaTransaccionMpcv> {

    public static final String TEST_EMPTY = "9999999999999999";

    @Autowired
    private FormatMpcvMock mock;

    @Override
    public RespuestaTransaccionMpcv invocar(PeticionTransaccionMpcv transaccion) {
        RespuestaTransaccionMpcv response = new RespuestaTransaccionMpcv();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENCV format = transaccion.getCuerpo().getParte(FormatoMPMENCV.class);

        if (TEST_EMPTY.equalsIgnoreCase(format.getNumtarj())) {
            return response;
        }

        CopySalida copy = new CopySalida();
        copy.setCopy(mock.getFormatoMPMS1CV());
        response.getCuerpo().getPartes().add(copy);
        return response;
    }

    @Override
    public RespuestaTransaccionMpcv invocarCache(PeticionTransaccionMpcv transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

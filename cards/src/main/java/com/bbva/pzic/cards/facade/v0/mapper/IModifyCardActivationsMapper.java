package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputModifyCardActivations;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import java.util.List;

/**
 * Created on 9/10/2017.
 *
 * @author Entelgy
 */
public interface IModifyCardActivationsMapper {

    InputModifyCardActivations mapIn(String cardId, List<Activation> items);

    ServiceResponse<List<Activation>> mapOut(List<Activation> activations);
}

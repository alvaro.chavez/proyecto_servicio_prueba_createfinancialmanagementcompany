package com.bbva.pzic.cards.business.dto;

import java.math.BigDecimal;

/**
 * Created on 01/04/2016.
 *
 * @author Entelgy
 */
public class DTOIntCardStatementInterest {

    private BigDecimal purchasePEN;
    private BigDecimal purchaseUSD;
    private BigDecimal advancedPEN;
    private BigDecimal advancedUSD;
    private BigDecimal countervailingPEN;
    private BigDecimal countervailingUSD;
    private BigDecimal arrearsPEN;
    private BigDecimal arrearsUSD;

    public BigDecimal getPurchasePEN() {
        return purchasePEN;
    }

    public void setPurchasePEN(BigDecimal purchasePEN) {
        this.purchasePEN = purchasePEN;
    }

    public BigDecimal getPurchaseUSD() {
        return purchaseUSD;
    }

    public void setPurchaseUSD(BigDecimal purchaseUSD) {
        this.purchaseUSD = purchaseUSD;
    }

    public BigDecimal getAdvancedPEN() {
        return advancedPEN;
    }

    public void setAdvancedPEN(BigDecimal advancedPEN) {
        this.advancedPEN = advancedPEN;
    }

    public BigDecimal getAdvancedUSD() {
        return advancedUSD;
    }

    public void setAdvancedUSD(BigDecimal advancedUSD) {
        this.advancedUSD = advancedUSD;
    }

    public BigDecimal getCountervailingPEN() {
        return countervailingPEN;
    }

    public void setCountervailingPEN(BigDecimal countervailingPEN) {
        this.countervailingPEN = countervailingPEN;
    }

    public BigDecimal getCountervailingUSD() {
        return countervailingUSD;
    }

    public void setCountervailingUSD(BigDecimal countervailingUSD) {
        this.countervailingUSD = countervailingUSD;
    }

    public BigDecimal getArrearsPEN() {
        return arrearsPEN;
    }

    public void setArrearsPEN(BigDecimal arrearsPEN) {
        this.arrearsPEN = arrearsPEN;
    }

    public BigDecimal getArrearsUSD() {
        return arrearsUSD;
    }

    public void setArrearsUSD(BigDecimal arrearsUSD) {
        this.arrearsUSD = arrearsUSD;
    }

}

package com.bbva.pzic.cards.dao.model.ppcut003_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>rates</code>, utilizado por la clase <code>PeticionTransaccionPpcut003_1</code></p>
 * 
 * @see PeticionTransaccionPpcut003_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Rates {
	
	/**
	 * <p>Campo <code>itemizeRates</code>, &iacute;ndice: <code>1</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 1, nombre = "itemizeRates", tipo = TipoCampo.LIST)
	private List<Itemizerates> itemizerates;
	
}
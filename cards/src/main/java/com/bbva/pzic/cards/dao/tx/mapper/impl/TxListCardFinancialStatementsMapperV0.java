package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntPagination;
import com.bbva.pzic.cards.business.dto.DTOIntStatementList;
import com.bbva.pzic.cards.business.dto.InputListCardFinancialStatements;
import com.bbva.pzic.cards.canonic.Statement;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPME1E2;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPMS1E2;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPMS2E2;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardFinancialStatementsMapperV0;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.converter.builtin.BooleanToStringNumberConverter;
import com.bbva.pzic.cards.util.orika.converter.builtin.LongToIntegerConverter;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;

import java.util.ArrayList;

/**
 * Created on 17/07/2018.
 *
 * @author Entelgy
 */
@Mapper("txListCardFinancialStatementsMapper")
public class TxListCardFinancialStatementsMapperV0 extends ConfigurableMapper implements ITxListCardFinancialStatementsMapperV0 {

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.getConverterFactory().registerConverter(new LongToIntegerConverter());
        factory.getConverterFactory().registerConverter(new BooleanToStringNumberConverter());

        factory.classMap(InputListCardFinancialStatements.class, FormatoMPME1E2.class)
                .field("cardId", "numtarj")
                .field("isActive", "indacti")
                .field("paginationKey", "idpagin")
                .field("pageSize", "tampagi")
                .register();

        factory.classMap(FormatoMPMS1E2.class, Statement.class)
                .field("iddocta", "id")
                .field("fecorte", "cutOffDate")
                .field("indacti", "isActive")
                .register();

        factory.classMap(FormatoMPMS2E2.class, DTOIntPagination.class)
                .field("idpagin", "paginationKey")
                .field("tampagi", "pageSize")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormatoMPME1E2 mapIn(final InputListCardFinancialStatements dtoIn) {
        FormatoMPME1E2 formatoMPME1E2 = map(dtoIn, FormatoMPME1E2.class);
        if (dtoIn.getIsActive() != null) {
            formatoMPME1E2.setIndacti(dtoIn.getIsActive() ? "1" : "0");
        }
        return formatoMPME1E2;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntStatementList mapOut(final FormatoMPMS1E2 formatOutput,
                                      final DTOIntStatementList dtoOut) {
        DTOIntStatementList statementList = dtoOut;
        if (statementList == null) {
            statementList = new DTOIntStatementList();
        }
        if (statementList.getData() == null) {
            statementList.setData(new ArrayList<Statement>());
        }
        statementList.getData().add(map(formatOutput, Statement.class));
        return statementList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntStatementList mapOut2(final FormatoMPMS2E2 formatOutput,
                                       final DTOIntStatementList dtoOut) {
        DTOIntStatementList statementList = dtoOut;
        if (formatOutput == null) {
            return statementList;
        }
        if (formatOutput.getIdpagin() == null && formatOutput.getTampagi() == null) {
            return statementList;
        }
        if (statementList == null) {
            statementList = new DTOIntStatementList();
        }
        DTOIntPagination pagination = map(formatOutput, DTOIntPagination.class);
        statementList.setPagination(pagination);
        return statementList;
    }

}
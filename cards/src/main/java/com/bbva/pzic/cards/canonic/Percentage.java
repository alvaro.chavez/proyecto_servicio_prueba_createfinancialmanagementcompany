package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "percentage", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "percentage", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Percentage implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Percentage to be charged.
     */
    private BigDecimal percentage;
    /**
     * Identifier of the mode that applies the collection of the fee.
     */
    private String id;
    /**
     * Name of the mode that applies the collection of the fee.
     */
    private String name;

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "Factor", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Factor", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Factor implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Value of the exchange for purchase or sale. According to the exchange
     * type this value should be associated as a magnitude of the base currency
     * for sale or exchange currency for purchase. DISCLAIMER: If this attribute
     * is not provided, the attribute ratio must be provided.
     */
    private BigDecimal value;
    /**
     * Purchase or sale exchange ratio. Represents a percentage with the
     * relation between the base and target currencies.
     */
    private String ratio;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getRatio() {
        return ratio;
    }

    public void setRatio(String ratio) {
        this.ratio = ratio;
    }
}

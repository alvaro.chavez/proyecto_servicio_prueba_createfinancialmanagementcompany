package com.bbva.pzic.cards.dao.model.mpws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPWS</code>
 *
 * @see PeticionTransaccionMpws
 * @see RespuestaTransaccionMpws
 */
@Component
public class TransaccionMpws implements InvocadorTransaccion<PeticionTransaccionMpws,RespuestaTransaccionMpws> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpws invocar(PeticionTransaccionMpws transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpws.class, RespuestaTransaccionMpws.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpws invocarCache(PeticionTransaccionMpws transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpws.class, RespuestaTransaccionMpws.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}

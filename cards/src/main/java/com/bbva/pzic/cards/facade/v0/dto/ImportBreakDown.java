package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement(name = "ImportBreakDown", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "ImportBreakDown", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ImportBreakDown  implements Serializable {

    private static final long serialVersionUID = 1L;

    private BigDecimal amount;
    private String currency;
    private BreakDown breakDown;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BreakDown getBreakDown() {
        return breakDown;
    }

    public void setBreakDown(BreakDown breakDown) {
        this.breakDown = breakDown;
    }
}

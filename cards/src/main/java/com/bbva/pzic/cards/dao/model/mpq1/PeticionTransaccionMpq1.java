package com.bbva.pzic.cards.dao.model.mpq1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPQ1</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpq1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpq1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPQ1.D1181114
 * MPQ1CONSULTA AVANCE DE MEMBRESIA       MP        MP2CMECCPBDMPPO MPFMCE1             MPQ1  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2018-02-20XP85153 2018-05-1817.34.48XP85153 2018-02-20-09.53.46.183474XP85153 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPFMCE1.D1181114
 * MPFMCE1 �CONSULTA AVANCE MEMBRESIA CA  �F�01�00019�01�00001�IDETARJ�NUMERO DE TARJETA CR�A�019�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPQ1.D1181114
 * MPQ1MPNCCS1 MPNCCS01MP2CMECC1S                             XP92348 2018-04-30-18.10.26.428566XP92348 2018-04-30-18.14.35.092107
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPNCCS1.D1181114
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�01�00001�IDCONME�ID. CONDICION       �A�002�0�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�02�00003�DSCONME�DES. CONDICION      �A�030�0�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�03�00033�IDPERME�IDE. PERIODO        �A�002�0�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�04�00035�DSPERME�DES. PERIODO        �A�030�0�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�05�00065�FECMEM �FEC. CONDICION      �A�010�0�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�06�00075�TIPCOME�TIP. COMISION       �A�002�0�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�07�00077�DSCOMME�DES. TIP. COMISION  �A�030�0�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�08�00107�IMPORTE�IMPORTE COMISION    �N�013�2�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�09�00120�MONIMP �MONEDA IMPORTE      �A�003�0�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�10�00123�FECINME�FECHA EXPIRACION TJ.�A�010�0�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�11�00133�CANMESM�NUM. MESES RESTANTES�N�002�0�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�12�00135�IMPMETA�IMPORTE DE LA META  �N�013�2�S�        �
 * MPNCCS1 �AVANCE DE MEMBRESIA           �X�13�00160�13�00148�IMPACUM�IMPORTE ACUMUL.X C/S�N�013�2�S�        �
</pre></code>
 *
 * @see RespuestaTransaccionMpq1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPQ1",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpq1.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPFMCE1.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpq1 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntFee {

    @Valid
    @NotNull(groups = {ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    private List<DTOIntItemizeFee> itemizeFees;

    public List<DTOIntItemizeFee> getItemizeFees() {
        return itemizeFees;
    }

    public void setItemizeFees(List<DTOIntItemizeFee> itemizeFees) {
        this.itemizeFees = itemizeFees;
    }
}
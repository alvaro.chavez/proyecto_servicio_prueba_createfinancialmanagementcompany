package com.bbva.pzic.cards.dao.model.mpde;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPDE</code>
 *
 * @see PeticionTransaccionMpde
 * @see RespuestaTransaccionMpde
 */
@Component
public class TransaccionMpde implements InvocadorTransaccion<PeticionTransaccionMpde,RespuestaTransaccionMpde> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpde invocar(PeticionTransaccionMpde transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpde.class, RespuestaTransaccionMpde.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpde invocarCache(PeticionTransaccionMpde transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpde.class, RespuestaTransaccionMpde.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}

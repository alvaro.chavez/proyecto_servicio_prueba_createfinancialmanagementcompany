package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.jee.arq.spring.core.managers.OutputHeaderManager;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateCardProposalMapper;
import com.bbva.pzic.cards.dao.model.ppcut001_1.Contact;
import com.bbva.pzic.cards.dao.model.ppcut001_1.Participant;
import com.bbva.pzic.cards.dao.model.ppcut001_1.Product;
import com.bbva.pzic.cards.dao.model.ppcut001_1.*;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@Mapper
public class ApxCreateCardProposalMapper implements IApxCreateCardProposalMapper {

    private OutputHeaderManager outputHeaderManager;

    public ApxCreateCardProposalMapper(OutputHeaderManager outputHeaderManager) {
        this.outputHeaderManager = outputHeaderManager;
    }

    @Override
    public PeticionTransaccionPpcut001_1 mapIn(final InputCreateCardProposal input) {
        PeticionTransaccionPpcut001_1 request = new PeticionTransaccionPpcut001_1();
        Entityin entityin = new Entityin();
        entityin.setCardtype(mapInCardType(input.getCardType()));
        entityin.setProduct(mapInProduct(input.getProduct()));
        entityin.setPhysicalsupport(mapInPhysicalSupport(input.getPhysicalSupport()));
        entityin.setGrantedcredits(mapInGrantedCredits(input.getGrantedCredits()));
        entityin.setContact(mapInContact(input.getContact()));
        entityin.setRates(mapInRate(input.getRates()));
        entityin.setFees(mapInFees(input.getFees()));
        entityin.setParticipants(mapInParticipants(input.getParticipants()));
        entityin.setOfferid(input.getOfferId());
        request.setEntityin(entityin);
        return request;
    }

    private List<Participants> mapInParticipants(final List<DTOIntParticipant> dtoIntParticipantList) {
        if (CollectionUtils.isEmpty(dtoIntParticipantList)) {
            return null;
        }

        return dtoIntParticipantList.stream().filter(Objects::nonNull).map(this::mapInParticipant).collect(Collectors.toList());
    }

    private Participants mapInParticipant(final DTOIntParticipant dtoIntParticipant) {
        Participants participants = new Participants();
        Participant knowParticipant = new Participant();
        knowParticipant.setPersontype(dtoIntParticipant.getPersonType());
        knowParticipant.setId(dtoIntParticipant.getId());
        knowParticipant.setIscustomer(dtoIntParticipant.getIsCustomer());
        knowParticipant.setParticipanttype(mapInParticipantType(dtoIntParticipant.getParticipantTypeId()));
        participants.setParticipant(knowParticipant);
        return participants;
    }

    private Participanttype mapInParticipantType(final String participantTypeId) {
        if (StringUtils.isEmpty(participantTypeId)) {
            return null;
        }

        Participanttype result = new Participanttype();
        result.setId(participantTypeId);
        return result;
    }

    private Fees mapInFees(final DTOIntFee dtoIntFee) {
        if (dtoIntFee == null) {
            return null;
        }

        Fees fees = new Fees();
        fees.setItemizefees(mapInItemizeFees(dtoIntFee.getItemizeFees()));
        return fees;
    }

    private List<Itemizefees> mapInItemizeFees(final List<DTOIntItemizeFee> dtoIntItemizeFees) {
        if (dtoIntItemizeFees == null) {
            return null;
        }

        return dtoIntItemizeFees.stream().map(this::mapInItemizeFee).collect(Collectors.toList());

    }

    private Itemizefees mapInItemizeFee(final DTOIntItemizeFee dtoIntItemizeFee) {
        if (dtoIntItemizeFee == null) {
            return null;
        }

        Itemizefees itemizefees = new Itemizefees();
        Itemizefee itemizefee = new Itemizefee();
        itemizefee.setFeetype(dtoIntItemizeFee.getFeeType());
        itemizefee.setItemizefeeunit(mapInItemizeFreeUnit(dtoIntItemizeFee.getItemizeFeeUnit()));

        itemizefees.setItemizefee(itemizefee);
        return itemizefees;
    }

    private Itemizefeeunit mapInItemizeFreeUnit(final DTOIntItemizeFeeUnit dtoIntItemizeFeeUnit) {
        if (dtoIntItemizeFeeUnit == null) {
            return null;
        }

        Itemizefeeunit itemizefeeunit = new Itemizefeeunit();
        itemizefeeunit.setAmount(dtoIntItemizeFeeUnit.getAmount() == null ? null : dtoIntItemizeFeeUnit.getAmount().toString());
        itemizefeeunit.setCurrency(dtoIntItemizeFeeUnit.getCurrency());
        itemizefeeunit.setUnittype(dtoIntItemizeFeeUnit.getUnitType());
        return itemizefeeunit;
    }

    private Rates mapInRate(final DTOIntRate dtoIntRate) {
        if (dtoIntRate == null) {
            return null;
        }

        Rates rates = new Rates();
        rates.setItemizerates(mapInItemizeRates(dtoIntRate.getItemizeRates()));
        return rates;
    }

    private List<Itemizerates> mapInItemizeRates(final List<DTOIntItemizeRate> dtoIntItemizeRates) {
        if (CollectionUtils.isEmpty(dtoIntItemizeRates)) {
            return null;
        }

        return dtoIntItemizeRates.stream().map(this::mapInItemizeRate).collect(Collectors.toList());
    }

    private Itemizerates mapInItemizeRate(final DTOIntItemizeRate dtoIntItemizeRate) {
        if (dtoIntItemizeRate == null) {
            return null;
        }

        Itemizerates itemizerates = new Itemizerates();
        Itemizerate itemizerate = new Itemizerate();
        itemizerate.setRatetype(dtoIntItemizeRate.getRateType());
        itemizerate.setItemizeratesunit(mapInItemizeRateUnit(dtoIntItemizeRate.getItemizeRatesUnit()));

        itemizerates.setItemizerate(itemizerate);
        return itemizerates;
    }

    private Itemizeratesunit mapInItemizeRateUnit(final DTOIntItemizeRatesUnit dtoIntItemizeRatesUnit) {
        if (dtoIntItemizeRatesUnit == null) {
            return null;
        }

        Itemizeratesunit itemizeratesunit = new Itemizeratesunit();
        itemizeratesunit.setUnitratetype(dtoIntItemizeRatesUnit.getUnitRateType());
        itemizeratesunit.setPercentage(dtoIntItemizeRatesUnit.getPercentage() == null ? null : dtoIntItemizeRatesUnit.getPercentage().toString());
        return itemizeratesunit;
    }

    private Contact mapInContact(final DTOIntSpecificProposalContact dtoIntContact) {
        if (dtoIntContact == null) {
            return null;
        }

        Contact contact = new Contact();
        contact.setSpecificcontact(mapInSpecificContact(dtoIntContact.getContact()));
        contact.setContacttype(dtoIntContact.getContactType());
        return contact;
    }

    private Specificcontact mapInSpecificContact(final DTOIntMobileProposal dtoIntContact) {
        if (dtoIntContact == null) {
            return null;
        }

        Specificcontact contact = new Specificcontact();
        contact.setContactdetailtype(dtoIntContact.getContactDetailType());
        contact.setNumber(dtoIntContact.getNumber());
        contact.setPhonecompany(mapInPhoneCompany(dtoIntContact.getPhoneCompany()));
        return contact;
    }

    private Phonecompany mapInPhoneCompany(final DTOIntPhoneCompanyProposal contactPhoneCompany) {
        if (contactPhoneCompany == null) {
            return null;
        }

        Phonecompany phonecompany = new Phonecompany();
        phonecompany.setId(contactPhoneCompany.getId());
        return phonecompany;
    }

    private List<Grantedcredits> mapInGrantedCredits(final List<DTOIntImport> dtoIntGrantedCredits) {
        if (CollectionUtils.isEmpty(dtoIntGrantedCredits)) {
            return null;
        }

        return dtoIntGrantedCredits.stream().map(this::mapInGrantedCredit).collect(Collectors.toList());
    }

    private Grantedcredits mapInGrantedCredit(DTOIntImport dtoIntGrantedCredits) {
        Grantedcredits grantedcredits = new Grantedcredits();
        Grantedcredit grantedcredit = new Grantedcredit();
        grantedcredit.setAmount(dtoIntGrantedCredits.getAmount() == null ? null : dtoIntGrantedCredits.getAmount().toString());
        grantedcredit.setCurrency(dtoIntGrantedCredits.getCurrency());
        grantedcredits.setGrantedcredit(grantedcredit);
        return grantedcredits;
    }

    private Physicalsupport mapInPhysicalSupport(final DTOIntPhysicalSupport dtoIntPhysicalSupport) {
        if (dtoIntPhysicalSupport == null) {
            return null;
        }

        Physicalsupport physicalsupport = new Physicalsupport();
        physicalsupport.setId(dtoIntPhysicalSupport.getId());
        return physicalsupport;
    }

    private Product mapInProduct(final DTOIntProduct dtoIntProduct) {
        if (dtoIntProduct == null) {
            return null;
        }

        Product product = new Product();
        product.setId(dtoIntProduct.getId());
        return product;
    }

    private Cardtype mapInCardType(final DTOIntCardType cardType) {
        if (cardType == null) {
            return null;
        }

        Cardtype cardtype = new Cardtype();
        cardtype.setId(cardType.getId());
        return cardtype;
    }

    @Override
    public ProposalCard mapOut(final RespuestaTransaccionPpcut001_1 response) {
        if (response == null || response.getEntityout() == null) {
            return null;
        }

        Entityout respuestaTx = response.getEntityout();
        if (StringUtils.isNotEmpty(respuestaTx.getId())) {
            outputHeaderManager.setHeader(Constants.LOCATION_HTTP_HEADER, "/proposals/".concat(respuestaTx.getId()));
        }
        ProposalCard result = new ProposalCard();
        result.setId(respuestaTx.getId());
        result.setCardType(mapOutCardType(respuestaTx.getCardtype()));
        result.setProduct(mapOutProduct(respuestaTx.getProduct()));
        result.setPhysicalSupport(mapOutPhysicalSupport(respuestaTx.getPhysicalsupport()));
        result.setGrantedCredits(mapOutGrantedCredits(respuestaTx.getGrantedcredits()));
        result.setContact(mapOutContact(respuestaTx.getContact()));
        result.setRates(mapOutRates(respuestaTx.getRates()));
        result.setFees(mapOutFees(respuestaTx.getFees()));
        result.setOperationDate(respuestaTx.getOperationdate() == null ? null : DateUtils.rebuildDateTime(respuestaTx.getOperationdate()));
        result.setOperationNumber(respuestaTx.getOperationnumber());
        result.setStatus(respuestaTx.getStatus());
        result.setOfferId(respuestaTx.getOfferid());
        result.setParticipants(mapOutParticipants(respuestaTx.getParticipants()));
        return result;
    }

    private List<KnowParticipant> mapOutParticipants(final List<Participants> participantsOut) {
        if (CollectionUtils.isEmpty(participantsOut)) {
            return null;
        }

        return participantsOut.stream().filter(Objects::nonNull).map(this::mapOutKnowParticipant).collect(Collectors.toList());
    }

    private KnowParticipant mapOutKnowParticipant(final Participants participants) {
        if (participants.getParticipant() == null) {
            return null;
        }
        KnowParticipant knowParticipant = new KnowParticipant();
        knowParticipant.setPersonType(participants.getParticipant().getPersontype());
        knowParticipant.setId(participants.getParticipant().getId());
        knowParticipant.setIsCustomer(participants.getParticipant().getIscustomer());
        knowParticipant.setParticipantType(mapOutParticipantType(participants.getParticipant().getParticipanttype()));
        return knowParticipant;
    }

    private ParticipantType mapOutParticipantType(final Participanttype participanttype) {
        if (participanttype == null) {
            return null;
        }

        ParticipantType result = new ParticipantType();
        result.setId(participanttype.getId());
        return result;
    }

    private TransferFees mapOutFees(final Fees feesOut) {
        if (feesOut == null) {
            return null;
        }
        TransferFees fee = new TransferFees();
        fee.setItemizeFees(mapOutItemizeFees(feesOut.getItemizefees()));
        return fee;
    }

    private List<ItemizeFee> mapOutItemizeFees(final List<Itemizefees> itemizefeesOut) {
        if (CollectionUtils.isEmpty(itemizefeesOut)) {
            return null;
        }

        return itemizefeesOut.stream().filter(Objects::nonNull).map(this::mapOutItemizeFee).collect(Collectors.toList());
    }

    private ItemizeFee mapOutItemizeFee(final Itemizefees itemizefeesOut) {
        if (itemizefeesOut.getItemizefee() == null) {
            return null;
        }

        ItemizeFee itemizeFee = new ItemizeFee();
        itemizeFee.setFeeType(itemizefeesOut.getItemizefee().getFeetype());
        itemizeFee.setDescription(itemizefeesOut.getItemizefee().getDescription());
        itemizeFee.setItemizeFeeUnit(mapOutItemizeFreeUnit(itemizefeesOut.getItemizefee().getItemizefeeunit()));
        return itemizeFee;
    }

    private ItemizeFeeUnit mapOutItemizeFreeUnit(final Itemizefeeunit itemizefeeunitOut) {
        if (itemizefeeunitOut == null) {
            return null;
        }

        ItemizeFeeUnit itemizeFeeUnit = new ItemizeFeeUnit();
        itemizeFeeUnit.setUnitType(itemizefeeunitOut.getUnittype());
        itemizeFeeUnit.setAmount(NumberUtils.isNumber(itemizefeeunitOut.getAmount()) ? BigDecimal.valueOf(Double.parseDouble(itemizefeeunitOut.getAmount())) : null);
        itemizeFeeUnit.setCurrency(itemizefeeunitOut.getCurrency());
        return itemizeFeeUnit;
    }

    private ProposalRate mapOutRates(final Rates ratesOut) {
        if (ratesOut == null) {
            return null;
        }

        ProposalRate proposalRate = new ProposalRate();
        proposalRate.setItemizeRates(mapOutItemizeRates(ratesOut.getItemizerates()));
        return proposalRate;
    }

    private List<ItemizeRate> mapOutItemizeRates(final List<Itemizerates> itemizeratesOut) {
        if (CollectionUtils.isEmpty(itemizeratesOut)) {
            return null;
        }

        return itemizeratesOut.stream().map(this::mapOutItemizeRate).collect(Collectors.toList());
    }

    private ItemizeRate mapOutItemizeRate(final Itemizerates itemizeratesOut) {
        if (itemizeratesOut.getItemizerate() == null) {
            return null;
        }

        ItemizeRate itemizeRate = new ItemizeRate();
        itemizeRate.setRateType(itemizeratesOut.getItemizerate().getRatetype());
        itemizeRate.setDescription(itemizeratesOut.getItemizerate().getDescription());
        itemizeRate.setItemizeRatesUnit(mapOutItemizeRatesUnit(itemizeratesOut.getItemizerate().getItemizeratesunit()));
        return itemizeRate;
    }

    private ItemizeRateUnit mapOutItemizeRatesUnit(Itemizeratesunit itemizeratesunitOut) {
        if (itemizeratesunitOut == null) {
            return null;
        }

        ItemizeRateUnit itemizeRateUnit = new ItemizeRateUnit();
        itemizeRateUnit.setPercentage(NumberUtils.isNumber(itemizeratesunitOut.getPercentage()) ? BigDecimal.valueOf(Double.parseDouble(itemizeratesunitOut.getPercentage())) : null);
        itemizeRateUnit.setUnitRateType(itemizeratesunitOut.getUnitratetype());
        return itemizeRateUnit;
    }

    private SpecificProposalContact mapOutContact(final Contact contact) {
        if (contact == null) {
            return null;
        }
        SpecificProposalContact result = new SpecificProposalContact();
        result.setId(contact.getId());
        result.setContactType(contact.getContacttype());
        result.setContact(mapOutSpecificContact(contact.getSpecificcontact()));
        return result;
    }

    private MobileProposal mapOutSpecificContact(final Specificcontact specificcontact) {
        if (specificcontact == null) {
            return null;
        }

        MobileProposal result = new MobileProposal();
        result.setContactDetailType(specificcontact.getContactdetailtype());
        result.setNumber(specificcontact.getNumber());
        result.setPhoneCompany(mapOutPhoneCompany(specificcontact.getPhonecompany()));
        return result;
    }

    private PhoneCompanyProposal mapOutPhoneCompany(final Phonecompany phonecompanyOut) {
        if (phonecompanyOut == null) {
            return null;
        }

        PhoneCompanyProposal phoneCompany = new PhoneCompanyProposal();
        phoneCompany.setId(phonecompanyOut.getId());
        phoneCompany.setName(phonecompanyOut.getName());
        return phoneCompany;
    }

    private List<Import> mapOutGrantedCredits(final List<Grantedcredits> grantedcreditsOut) {
        if (CollectionUtils.isEmpty(grantedcreditsOut)) {
            return null;
        }

        return grantedcreditsOut.stream().map(this::mapOutGrantedCredit).collect(Collectors.toList());
    }

    private Import mapOutGrantedCredit(final Grantedcredits grantedcreditsOut) {
        if (grantedcreditsOut.getGrantedcredit() == null) {
            return null;
        }

        Import amount = new Import();
        amount.setAmount(NumberUtils.isNumber(grantedcreditsOut.getGrantedcredit().getAmount()) ? BigDecimal.valueOf(Double.parseDouble(grantedcreditsOut.getGrantedcredit().getAmount())) : null);
        amount.setCurrency(grantedcreditsOut.getGrantedcredit().getCurrency());
        return amount;
    }

    private PhysicalSupport mapOutPhysicalSupport(final Physicalsupport physicalsupportOut) {
        if (physicalsupportOut == null) {
            return null;
        }

        PhysicalSupport physicalSupport = new PhysicalSupport();
        physicalSupport.setId(physicalsupportOut.getId());
        physicalSupport.setDescription(physicalsupportOut.getDescription());
        return physicalSupport;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Product mapOutProduct(final Product productOut) {
        if (productOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Product product = new com.bbva.pzic.cards.facade.v1.dto.Product();
        product.setId(productOut.getId());
        product.setName(productOut.getName());
        return product;
    }

    private CardType mapOutCardType(final Cardtype cardtype) {
        if (cardtype == null) {
            return null;
        }

        CardType cardType = new CardType();
        cardType.setId(cardtype.getId());
        cardType.setName(cardtype.getName());
        return cardType;
    }
}

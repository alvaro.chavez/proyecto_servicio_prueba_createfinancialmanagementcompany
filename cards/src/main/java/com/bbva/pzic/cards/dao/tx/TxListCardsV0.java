package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.dao.model.mpl1.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardsV0Mapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
@Component("txListCardsV0")
public class TxListCardsV0
        extends DoubleOutputFormat<DTOIntListCards, FormatoMPMENL1, DTOOutListCards, FormatoMPMS1L1, FormatoMPMS2L1> {

    private static final Log LOG = LogFactory.getLog(TxListCardsV0.class);

    @Resource(name = "txListCardsV0Mapper")
    private ITxListCardsV0Mapper txListCardsV0Mapper;

    @Autowired
    public TxListCardsV0(@Qualifier("transaccionMpl1") InvocadorTransaccion<PeticionTransaccionMpl1, RespuestaTransaccionMpl1> transaction) {
        super(transaction, PeticionTransaccionMpl1::new, DTOOutListCards::new, FormatoMPMS1L1.class, FormatoMPMS2L1.class);
    }

    @Override
    protected FormatoMPMENL1 mapInput(DTOIntListCards dtoIntListCards) {
        LOG.info(" ... call TxListCardsV0.mapInput ... ");
        return txListCardsV0Mapper.mapIn(dtoIntListCards);
    }

    @Override
    protected DTOOutListCards mapFirstOutputFormat(FormatoMPMS1L1 formatoMPMS1L1, DTOIntListCards dtoIntListCards, DTOOutListCards dtoOut) {
        LOG.info(" ... call TxListCardsV0.mapFirstOutputFormat ... ");
        return txListCardsV0Mapper.mapOut(formatoMPMS1L1, null, dtoOut);
    }

    @Override
    protected DTOOutListCards mapSecondOutputFormat(FormatoMPMS2L1 formatoMPMS2L1, DTOIntListCards dtoIntListCards, DTOOutListCards dtoOut) {
        LOG.info(" ... call TxListCardsV0.mapSecondOutputFormat ... ");
        return txListCardsV0Mapper.mapOut2(formatoMPMS2L1, null, dtoOut);
    }
}

package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
public class DTOIntDaysOfMonth {

    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    private String day;

    private String cutOffDay;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCutOffDay() {
        return cutOffDay;
    }

    public void setCutOffDay(String cutOffDay) {
        this.cutOffDay = cutOffDay;
    }
}

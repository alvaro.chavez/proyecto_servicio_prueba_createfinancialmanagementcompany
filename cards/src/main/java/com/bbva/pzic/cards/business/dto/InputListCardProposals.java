package com.bbva.pzic.cards.business.dto;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
public class InputListCardProposals {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

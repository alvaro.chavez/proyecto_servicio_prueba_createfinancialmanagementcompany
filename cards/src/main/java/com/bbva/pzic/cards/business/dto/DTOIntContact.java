package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
public class DTOIntContact {
    private String id;
    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    private String contactType;
    private String contactTypeOriginal;
    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    private String contactDetailType;
    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    private String contactAddress;

    private DTOIntSpecificContact contact;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getContactTypeOriginal() {
        return contactTypeOriginal;
    }

    public void setContactTypeOriginal(String contactTypeOriginal) {
        this.contactTypeOriginal = contactTypeOriginal;
    }

    public String getContactDetailType() {
        return contactDetailType;
    }

    public void setContactDetailType(String contactDetailType) {
        this.contactDetailType = contactDetailType;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public DTOIntSpecificContact getContact() {
        return contact;
    }

    public void setContact(DTOIntSpecificContact contact) {
        this.contact = contact;
    }
}

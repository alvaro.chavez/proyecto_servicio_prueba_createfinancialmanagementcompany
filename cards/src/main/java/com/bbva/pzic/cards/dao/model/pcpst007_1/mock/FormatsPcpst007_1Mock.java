package com.bbva.pzic.cards.dao.model.pcpst007_1.mock;

import com.bbva.pzic.cards.dao.model.pcpst007_1.RespuestaTransaccionPcpst007_1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

public final class FormatsPcpst007_1Mock {

    private static final FormatsPcpst007_1Mock INSTANCE = new FormatsPcpst007_1Mock();
    private final ObjectMapperHelper objectMapperHelper = ObjectMapperHelper.getInstance();

    private FormatsPcpst007_1Mock() {
    }

    public static FormatsPcpst007_1Mock getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPcpst007_1 getRespuestaTransaccionPcpst007_1() throws IOException {
        return objectMapperHelper.readValue(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(
                        "com/bbva/pzic/cards/dao/model/pcpst007_1/mock/respuestaTransaccionPcpst007_1.json"),
                RespuestaTransaccionPcpst007_1.class);
    }
}

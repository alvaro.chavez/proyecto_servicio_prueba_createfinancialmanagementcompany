package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "location", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "location", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Location implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Complete address of the location. This value will be masked when basic
     * authentication state. If the authentication state is advanced the
     * value will be clear.
     * His value will be masked when basic authentication state. If the authentication state is advanced the value will be clear.
     */
    private String formattedAddress;
    /**
     * This is an array containing the separate components applicable to
     * this address.
     */
    private List<AddressComponents> addressComponents;

    private String additionalInformation;

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public List<AddressComponents> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<AddressComponents> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }
}

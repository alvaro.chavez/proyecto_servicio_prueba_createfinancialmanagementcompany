package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCashAdvancesSearchCriteria;
import com.bbva.pzic.cards.business.dto.DTOIntListCashAdvances;
import com.bbva.pzic.cards.facade.v0.dto.CashAdvances;

import java.util.List;

public interface IListCardCashAdvancesMapper {

    DTOIntCashAdvancesSearchCriteria mapInput(String cardId);

    ServiceResponse<List<CashAdvances>> mapOutput(DTOIntListCashAdvances dtoIntListCashAdvances);
}

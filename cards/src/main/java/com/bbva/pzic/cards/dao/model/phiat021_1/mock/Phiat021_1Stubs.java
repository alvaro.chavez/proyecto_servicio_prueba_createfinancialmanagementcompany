package com.bbva.pzic.cards.dao.model.phiat021_1.mock;

import com.bbva.pzic.cards.dao.model.phiat021_1.RespuestaTransaccionPhiat021_1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 19/02/2020.
 *
 * @author Entelgy
 */
public final class Phiat021_1Stubs {

    private static final Phiat021_1Stubs INSTANCE = new Phiat021_1Stubs();
    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private Phiat021_1Stubs() {
    }

    public static Phiat021_1Stubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPhiat021_1 buildRespuestaTransaccionPhiat021_1() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/phiat021_1/mock/respuestaTransaccionPhiat021_1.json"), RespuestaTransaccionPhiat021_1.class);
    }
}

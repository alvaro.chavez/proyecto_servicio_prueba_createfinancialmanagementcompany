package com.bbva.pzic.cards.dao.model.mprt;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPRMRT0</code> de la transacci&oacute;n <code>MPRT</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPRMRT0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPRMRT0 {

	/**
	 * <p>Campo <code>NUCOREL</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
    @DatoAuditable(omitir = true)
	@Campo(indice = 1, nombre = "NUCOREL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nucorel;

	/**
	 * <p>Campo <code>TARINOM</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
    @DatoAuditable(omitir = true)
	@Campo(indice = 2, nombre = "TARINOM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String tarinom;

	/**
	 * <p>Campo <code>CTACARG</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "CTACARG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String ctacarg;

	/**
	 * <p>Campo <code>NUMCLIE</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
    @DatoAuditable(omitir = true)
	@Campo(indice = 4, nombre = "NUMCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String numclie;

	/**
	 * <p>Campo <code>INDCSMS</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "INDCSMS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indcsms;

	/**
	 * <p>Campo <code>IDTESMS</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
    @DatoAuditable(omitir = true)
	@Campo(indice = 6, nombre = "IDTESMS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 13, longitudMaxima = 13)
	private String idtesms;

}
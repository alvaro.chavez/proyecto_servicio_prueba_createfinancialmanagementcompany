package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 14/02/2017.
 *
 * @author Entelgy
 */
public class DTOInputCreateCardRelatedContract {
    @NotNull(groups = ValidationGroup.CreateRelatedContract.class)
    @Size(max = 16, groups = ValidationGroup.CreateRelatedContract.class)
    private String cardId;
    @NotNull(groups = ValidationGroup.CreateRelatedContract.class)
    @Size(max = 20, groups = ValidationGroup.CreateRelatedContract.class)
    private String contractId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }
}

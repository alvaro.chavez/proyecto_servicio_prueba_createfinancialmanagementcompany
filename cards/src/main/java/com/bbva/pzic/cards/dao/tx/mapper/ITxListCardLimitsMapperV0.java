package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Limits;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMENGL;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMS1GL;

import java.util.List;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy
 */
public interface ITxListCardLimitsMapperV0 {

    FormatoMPMENGL mapIn(DTOIntCard dtoIn);

    List<Limits> mapOut(FormatoMPMS1GL formatOutput, List<Limits> dtoOut);
}

package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntPin;
import com.bbva.pzic.cards.canonic.Pin;
import com.bbva.pzic.cards.facade.v0.mapper.IModifyCardPinMapper;
import org.springframework.stereotype.Component;

/**
 * Created on 3/10/2017.
 *
 * @author Entelgy
 */
@Component
public class ModifyCardPinMapper implements IModifyCardPinMapper {

    @Override
    public DTOIntPin mapIn(final String cardId, final Pin pin) {
        DTOIntPin dtoIntPin = new DTOIntPin();
        dtoIntPin.setCardId(cardId);
        dtoIntPin.setPin(pin.getPin());
        return dtoIntPin;
    }
}

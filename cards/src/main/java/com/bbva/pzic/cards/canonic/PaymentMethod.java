package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "PaymentMethod", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "PaymentMethod", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentMethod implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Payment method type identifier.
     */
    private String id;
    /**
     * Payment method type name.
     */
    private String name;
    /**
     * Payment method type. This type refers to the selected amount to be paid
     * in each term.
     */
    private PaymentType paymentType;
    /**
     * Payment method period selected by the user for paying credit debt.
     */
    private Period period;
    /**
     * Monetary amount for the selected payment method. This attribute will not
     * be provided if either percentage or free payment methods are selected.
     * This amount may be provided in several currencies (depending on the
     * country).
     */
    private List<MonetaryPayment> monetaryPayments;
    /**
     * Percentage value used within the selected payment method. This attribute
     * is mandatory for percentage payment method, and will not be provided if
     * monetary payment method is selected.
     */
    private BigDecimal percentage;
    /**
     * String based on ISO-8601 date format for specifying the last date on
     * which the current payment term must be made before being considered in
     * arrears.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date endDate;
    /**
     * The day of the month when the payment of each card period must be made before
     * being considered in arrears. This field is only used at the moment that the
     * customer contracts the card and is selected by him or her. This day is about
     * 25 days after your cut off day.
     */
    private Integer endDay;
    /**
     * List of precalculated amounts for paying credit debt. These amounts are
     * calculated based on the existing total debt amount at the moment the last
     * period ended.
     */
    private List<PaymentAmount> paymentAmounts;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public List<MonetaryPayment> getMonetaryPayments() {
        return monetaryPayments;
    }

    public void setMonetaryPayments(List<MonetaryPayment> monetaryPayments) {
        this.monetaryPayments = monetaryPayments;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getEndDay() {
        return endDay;
    }

    public void setEndDay(Integer endDay) {
        this.endDay = endDay;
    }

    public List<PaymentAmount> getPaymentAmounts() {
        return paymentAmounts;
    }

    public void setPaymentAmounts(List<PaymentAmount> paymentAmounts) {
        this.paymentAmounts = paymentAmounts;
    }
}
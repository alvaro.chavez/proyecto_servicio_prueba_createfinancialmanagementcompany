package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.mp6h.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardFinancialStatementV1Mapper;
import com.bbva.pzic.cards.facade.v1.dto.FinancialStatement;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class TxGetCardFinancialStatementV1 extends DoubleOutputFormat<InputGetCardFinancialStatement, FormatoMPMQ6HE, FinancialStatement, FormatoMPMQ6HS, FormatoMPMQ6GS> {

    @Resource(name = "txGetCardFinancialStatementV1Mapper")
    private ITxGetCardFinancialStatementV1Mapper mapper;

    public TxGetCardFinancialStatementV1(@Qualifier("transaccionMp6h") InvocadorTransaccion<PeticionTransaccionMp6h, RespuestaTransaccionMp6h> transaction) {
        super(transaction, PeticionTransaccionMp6h::new, FinancialStatement::new, FormatoMPMQ6HS.class, FormatoMPMQ6GS.class);
    }

    @Override
    protected FormatoMPMQ6HE mapInput(InputGetCardFinancialStatement input) {
        return mapper.mapIn(input);
    }

    @Override
    protected FinancialStatement mapFirstOutputFormat(FormatoMPMQ6HS format, InputGetCardFinancialStatement input, FinancialStatement dtoOut) {
        return mapper.mapOutFormatoMPMQ6HS(format, dtoOut);
    }

    @Override
    protected FinancialStatement mapSecondOutputFormat(FormatoMPMQ6GS format, InputGetCardFinancialStatement input, FinancialStatement dtoOut) {
        return mapper.mapOutFormatoMPMQ6GS(format, dtoOut);
    }
}

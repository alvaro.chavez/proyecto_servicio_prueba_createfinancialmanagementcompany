package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntStatementList;
import com.bbva.pzic.cards.business.dto.InputListCardFinancialStatements;
import com.bbva.pzic.cards.dao.model.mpe2.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardFinancialStatementsMapperV0;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 17/07/2018.
 *
 * @author Entelgy
 */
@Component("txListCardFinancialStatements")
public class TxListCardFinancialStatementsV0
        extends DoubleOutputFormat<InputListCardFinancialStatements, FormatoMPME1E2, DTOIntStatementList, FormatoMPMS1E2, FormatoMPMS2E2> {

    @Resource(name = "txListCardFinancialStatementsMapper")
    private ITxListCardFinancialStatementsMapperV0 mapper;

    @Autowired
    public TxListCardFinancialStatementsV0(@Qualifier("transaccionMpe2") InvocadorTransaccion<PeticionTransaccionMpe2, RespuestaTransaccionMpe2> transaction) {
        super(transaction, PeticionTransaccionMpe2::new, DTOIntStatementList::new, FormatoMPMS1E2.class, FormatoMPMS2E2.class);
    }

    @Override
    protected FormatoMPME1E2 mapInput(InputListCardFinancialStatements inputListCardFinancialStatements) {
        return mapper.mapIn(inputListCardFinancialStatements);
    }

    @Override
    protected DTOIntStatementList mapFirstOutputFormat(FormatoMPMS1E2 formatoMPMS1E2, InputListCardFinancialStatements inputListCardFinancialStatements, DTOIntStatementList dtoOut) {
        return mapper.mapOut(formatoMPMS1E2, dtoOut);
    }

    @Override
    protected DTOIntStatementList mapSecondOutputFormat(FormatoMPMS2E2 formatoMPMS2E2, InputListCardFinancialStatements inputListCardFinancialStatements, DTOIntStatementList dtoOut) {
        return mapper.mapOut2(formatoMPMS2E2, dtoOut);
    }
}
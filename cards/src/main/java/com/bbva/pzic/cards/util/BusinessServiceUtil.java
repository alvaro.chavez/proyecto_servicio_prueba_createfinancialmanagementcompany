package com.bbva.pzic.cards.util;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @author Entelgy
 */
public final class BusinessServiceUtil {

    private static final Log LOG = LogFactory.getLog(BusinessServiceUtil.class);

    private BusinessServiceUtil() {
        // Prevent instantiation
    }

    public static String buildUrl(final String header, final String partialUrl) {
        LOG.debug(String.format("Processing url part with header value [%s]", header));
        String width = "";
        String height = "";
        try {
            if (header != null) {
                String[] headerPositions = header.toLowerCase().split(";");
                int index = 4;
                if (headerPositions.length == 1) {
                    index = 0;
                }
                String[] dimensions = headerPositions[index].split("x");
                width = dimensions[0];
                height = dimensions[1];
            }
        } catch (IndexOutOfBoundsException e) {
            LOG.warn("Error al obtener el ancho y alto para la imagen", e);
        }
        boolean acceptDimension = true;
        if (width.isEmpty() || height.isEmpty()) {
            acceptDimension = false;
        }

        List<NameValuePair> parameters = URLEncodedUtils.parse(partialUrl, StandardCharsets.UTF_8);

        List<NameValuePair> newParameters = new ArrayList<>();
        for (NameValuePair nameValuePair : parameters) {
            if ("width".equalsIgnoreCase(nameValuePair.getName())) {
                if (acceptDimension) {
                    newParameters.add(new BasicNameValuePair(nameValuePair.getName(), width));
                }
            } else if ("height".equalsIgnoreCase(nameValuePair.getName())) {
                if (acceptDimension) {
                    newParameters.add(new BasicNameValuePair(nameValuePair.getName(), height));
                }
            } else {
                newParameters.add(nameValuePair);
            }
        }

        String url = URLEncodedUtils.format(newParameters, StandardCharsets.UTF_8.name());
        LOG.debug(String.format("Processed part of url [%s]", url));
        return url;
    }

    /**
     * Expande los atributos del objeto enviado
     *
     * @param object                  objeto
     * @param possibleJoinedExpanders nombre de todos los atributos, separados por comas, que podr&aacute;n ser expandidos.
     * @param joinedExpandersReceived nombre de los atributos, separados por comas, que se quieren expandir.
     */
    @SuppressWarnings("unchecked")
    public static <T> void expand(T object, String possibleJoinedExpanders, String joinedExpandersReceived) {
        if (object != null && possibleJoinedExpanders != null) {
            if (joinedExpandersReceived != null && joinedExpandersReceived.isEmpty()) {
                throw new BusinessServiceException(Errors.EXPAND_EMPTY);
            }

            final List<String> possibleExpanders = Arrays.asList(possibleJoinedExpanders.split(","));
            final List<String> expandersReceived = joinedExpandersReceived == null
                    ? Collections.emptyList()
                    : Arrays.asList(joinedExpandersReceived.split(","));

            for (final String expander : expandersReceived) {
                if (!possibleExpanders.contains(expander)) {
                    throw new BusinessServiceException(Errors.EXPAND_ERROR, expander);
                }
            }

            final List<String> expandersNotReceived = new ArrayList<>(possibleExpanders);
            expandersNotReceived.removeAll(expandersReceived);
            for (String expander : expandersNotReceived) {
                if (object instanceof Collection<?>) {
                    for (T element : (Collection<T>) object) {
                        setNullOnField(element, expander);
                    }
                } else {
                    setNullOnField(object, expander);
                }
            }
        }
    }

    private static <T> void setNullOnField(T object, String expander) {
        Field field = getFieldByName(object.getClass(), expander);
        if (field != null) {
            field.setAccessible(true);
            try {
                if (!field.getType().isPrimitive()) {
                    field.set(object, null);
                }
            } catch (IllegalAccessException e) {
                throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
            }
        }
    }

    private static Field getFieldByName(Class<?> clazz, String name) {
        if (clazz != null) {
            for (Field field : clazz.getDeclaredFields()) {
                if (!field.isSynthetic() && field.getName().equals(name)) {
                    return field;
                }
            }
            return getFieldByName(clazz.getSuperclass(), name);
        }
        return null;
    }

    public static String buildDeliveryId(final String cardAgreement, final String id) {
        if (StringUtils.isBlank(cardAgreement) &&
                StringUtils.isBlank(id)) {
            return null;
        } else if (StringUtils.isBlank(cardAgreement) &&
                StringUtils.isNotBlank(id)) {
            return cardAgreement;
        } else if (StringUtils.isNotBlank(cardAgreement) &&
                StringUtils.isBlank(id)) {
            return id;
        } else {
            return cardAgreement.concat(id);
        }
    }
}

package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 24/10/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "paymentMethodData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "paymentMethodData", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentMethodData implements Serializable {

    private static final long serialVersionUID = 1L;

    private PaymentMethod data;

    public PaymentMethod getData() {
        return data;
    }

    public void setData(PaymentMethod data) {
        this.data = data;
    }
}

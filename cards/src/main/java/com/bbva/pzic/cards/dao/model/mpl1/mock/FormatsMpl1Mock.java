package com.bbva.pzic.cards.dao.model.mpl1.mock;

import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS1L1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS2L1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 07/02/2017.
 *
 * @author Entelgy
 */
public class FormatsMpl1Mock {

    private ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    public List<FormatoMPMS1L1> getFormatoMPMS1L1() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpl1/mock/formatoMPMS1L1.json"), new TypeReference<List<FormatoMPMS1L1>>() {
        });
    }

    public FormatoMPMS2L1 getFormatoMPMS2L1(String idPagin) throws IOException {
        String name;
        if (TransaccionMpl1Mock.TEST_NULL_IDPAGIN.equalsIgnoreCase(idPagin)) {
            name = "com/bbva/pzic/cards/dao/model/mpl1/mock/formatoMPMS2L1_null_idpagin.json";
        } else if (TransaccionMpl1Mock.TEST_NULL_TAMPAGI.equalsIgnoreCase(idPagin)) {
            name = "com/bbva/pzic/cards/dao/model/mpl1/mock/formatoMPMS2L1_null_tampagi.json";
        } else {
            name = "com/bbva/pzic/cards/dao/model/mpl1/mock/formatoMPMS2L1.json";
        }
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                name), new TypeReference<FormatoMPMS2L1>() {
        });
    }
}

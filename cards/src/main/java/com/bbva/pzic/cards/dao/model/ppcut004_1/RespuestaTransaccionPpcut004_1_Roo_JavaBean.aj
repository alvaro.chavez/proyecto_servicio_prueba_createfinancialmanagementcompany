// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.pzic.cards.dao.model.ppcut004_1.Entityout;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Paginationout;
import com.bbva.pzic.cards.dao.model.ppcut004_1.RespuestaTransaccionPpcut004_1;
import java.util.List;

privileged aspect RespuestaTransaccionPpcut004_1_Roo_JavaBean {
    
    /**
     * Gets codigoAviso value
     * 
     * @return String
     */
    public String RespuestaTransaccionPpcut004_1.getCodigoAviso() {
        return this.codigoAviso;
    }
    
    /**
     * Sets codigoAviso value
     * 
     * @param codigoAviso
     * @return RespuestaTransaccionPpcut004_1
     */
    public RespuestaTransaccionPpcut004_1 RespuestaTransaccionPpcut004_1.setCodigoAviso(String codigoAviso) {
        this.codigoAviso = codigoAviso;
        return this;
    }
    
    /**
     * Gets descripcionAviso value
     * 
     * @return String
     */
    public String RespuestaTransaccionPpcut004_1.getDescripcionAviso() {
        return this.descripcionAviso;
    }
    
    /**
     * Sets descripcionAviso value
     * 
     * @param descripcionAviso
     * @return RespuestaTransaccionPpcut004_1
     */
    public RespuestaTransaccionPpcut004_1 RespuestaTransaccionPpcut004_1.setDescripcionAviso(String descripcionAviso) {
        this.descripcionAviso = descripcionAviso;
        return this;
    }
    
    /**
     * Gets aplicacionAviso value
     * 
     * @return String
     */
    public String RespuestaTransaccionPpcut004_1.getAplicacionAviso() {
        return this.aplicacionAviso;
    }
    
    /**
     * Sets aplicacionAviso value
     * 
     * @param aplicacionAviso
     * @return RespuestaTransaccionPpcut004_1
     */
    public RespuestaTransaccionPpcut004_1 RespuestaTransaccionPpcut004_1.setAplicacionAviso(String aplicacionAviso) {
        this.aplicacionAviso = aplicacionAviso;
        return this;
    }
    
    /**
     * Gets codigoRetorno value
     * 
     * @return String
     */
    public String RespuestaTransaccionPpcut004_1.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    /**
     * Sets codigoRetorno value
     * 
     * @param codigoRetorno
     * @return RespuestaTransaccionPpcut004_1
     */
    public RespuestaTransaccionPpcut004_1 RespuestaTransaccionPpcut004_1.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
        return this;
    }
    
    /**
     * Gets entityout value
     * 
     * @return List
     */
    public List<Entityout> RespuestaTransaccionPpcut004_1.getEntityout() {
        return this.entityout;
    }
    
    /**
     * Sets entityout value
     * 
     * @param entityout
     * @return RespuestaTransaccionPpcut004_1
     */
    public RespuestaTransaccionPpcut004_1 RespuestaTransaccionPpcut004_1.setEntityout(List<Entityout> entityout) {
        this.entityout = entityout;
        return this;
    }
    
    /**
     * Gets paginationout value
     * 
     * @return Paginationout
     */
    public Paginationout RespuestaTransaccionPpcut004_1.getPaginationout() {
        return this.paginationout;
    }
    
    /**
     * Sets paginationout value
     * 
     * @param paginationout
     * @return RespuestaTransaccionPpcut004_1
     */
    public RespuestaTransaccionPpcut004_1 RespuestaTransaccionPpcut004_1.setPaginationout(Paginationout paginationout) {
        this.paginationout = paginationout;
        return this;
    }
    
}

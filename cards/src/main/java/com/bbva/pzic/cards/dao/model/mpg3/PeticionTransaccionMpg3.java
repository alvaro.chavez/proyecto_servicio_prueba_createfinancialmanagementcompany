package com.bbva.pzic.cards.dao.model.mpg3;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>MPG3</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpg3</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpg3</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPG3.D2200604.txt
 * MPG3ACTUALIZA ACTIVACIONES OPE TJ      MP        MP2CMPG3PBDMPPO MPMENG3             MPG3  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2017-10-05P014658 2019-05-1017.15.48XP87388 2017-10-05-15.32.06.539910P014658 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENG3.D2200604.txt
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�01�00001�IDETARJ�NRO.TARJETA CLIENTE �A�019�0�R�        �
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�02�00020�CODACTA�IND.BLOQUEO OPER    �A�002�0�O�        �
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�03�00022�INVACTA�IND.PRENDIDO APAGADO�A�001�0�O�        �
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�04�00023�CODACTB�IND.BLOQUEO OPER    �A�002�0�O�        �
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�05�00025�INVACTB�IND.PRENDIDO APAGADO�A�001�0�O�        �
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�06�00026�CODACTC�IND.BLOQUEO OPER    �A�002�0�O�        �
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�07�00028�INVACTC�IND.PRENDIDO APAGADO�A�001�0�O�        �
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�08�00029�CODACTD�IND.BLOQUEO OPER    �A�002�0�O�        �
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�09�00031�INVACTD�IND.PRENDIDO APAGADO�A�001�0�O�        �
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�10�00032�CODACTE�IND.BLOQUEO OPER    �A�002�0�O�        �
 * MPMENG3 �ACT. OPERA. RELACIONADO A TJ  �F�11�00034�11�00034�INVACTE�IND.PRENDIDO APAGADO�A�001�0�O�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1G3.D2200604.txt
 * MPMS1G3 �ACT. OPERA. RELACIONADO A TJ  �X�10�00015�01�00001�CODACTA�IND.BLOQUEO OPER    �A�002�0�S�        �
 * MPMS1G3 �ACT. OPERA. RELACIONADO A TJ  �X�10�00015�02�00003�INVACTA�IND.PRENDIDO APAGADO�A�001�0�S�        �
 * MPMS1G3 �ACT. OPERA. RELACIONADO A TJ  �X�10�00015�03�00004�CODACTB�IND.BLOQUEO OPER    �A�002�0�S�        �
 * MPMS1G3 �ACT. OPERA. RELACIONADO A TJ  �X�10�00015�04�00006�INVACTB�IND.PRENDIDO APAGADO�A�001�0�S�        �
 * MPMS1G3 �ACT. OPERA. RELACIONADO A TJ  �X�10�00015�05�00007�CODACTC�IND.BLOQUEO OPER    �A�002�0�S�        �
 * MPMS1G3 �ACT. OPERA. RELACIONADO A TJ  �X�10�00015�06�00009�INVACTC�IND.PRENDIDO APAGADO�A�001�0�S�        �
 * MPMS1G3 �ACT. OPERA. RELACIONADO A TJ  �X�10�00015�07�00010�CODACTD�IND.BLOQUEO OPER    �A�002�0�S�        �
 * MPMS1G3 �ACT. OPERA. RELACIONADO A TJ  �X�10�00015�08�00012�INVACTD�IND.PRENDIDO APAGADO�A�001�0�S�        �
 * MPMS1G3 �ACT. OPERA. RELACIONADO A TJ  �X�10�00015�09�00013�CODACTE�IND. PERM. SOBREGIRO�A�002�0�S�        �
 * MPMS1G3 �ACT. OPERA. RELACIONADO A TJ  �X�10�00015�10�00015�INVACTE�IND.PRENDIDO APAGADO�A�001�0�S�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPG3.D2200604.txt
 * MPG3MPMS1G3 MPNCS1G3MP2CMPG31S                             P014658 2017-10-06-10.23.56.074046P014658 2017-10-06-10.23.56.074082
 *
</pre></code>
 *
 * @see RespuestaTransaccionMpg3
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPG3",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpg3.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENG3.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionMpg3 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}

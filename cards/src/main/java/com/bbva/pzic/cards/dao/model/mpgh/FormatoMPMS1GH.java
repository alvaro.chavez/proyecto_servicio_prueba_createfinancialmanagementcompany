package com.bbva.pzic.cards.dao.model.mpgh;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS1GH</code> de la transacci&oacute;n <code>MPGH</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS1GH")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS1GH {

	/**
	 * <p>Campo <code>TIPTARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "TIPTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String tiptarj;

	/**
	 * <p>Campo <code>NUMTARJ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable
	@Campo(indice = 2, nombre = "NUMTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String numtarj;

	/**
	 * <p>Campo <code>TITPTAR</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable
	@Campo(indice = 3, nombre = "TITPTAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String titptar;

	/**
	 * <p>Campo <code>TITSTAR</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable
	@Campo(indice = 4, nombre = "TITSTAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String titstar;

	/**
	 * <p>Campo <code>INDTPER</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "INDTPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indtper;

	/**
	 * <p>Campo <code>FECCIER</code>, &iacute;ndice: <code>6</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 6, nombre = "FECCIER", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date feccier;

	/**
	 * <p>Campo <code>PAGCARG</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "PAGCARG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String pagcarg;

	/**
	 * <p>Campo <code>DIRPTIT</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable
	@Campo(indice = 8, nombre = "DIRPTIT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String dirptit;

	/**
	 * <p>Campo <code>DIRCTIT</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable
	@Campo(indice = 9, nombre = "DIRCTIT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String dirctit;

	/**
	 * <p>Campo <code>DIRPPRO</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "DIRPPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String dirppro;

	/**
	 * <p>Campo <code>DIRPUBI</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "DIRPUBI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String dirpubi;

	/**
	 * <p>Campo <code>LINCRED</code>, &iacute;ndice: <code>12</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 12, nombre = "LINCRED", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal lincred;

	/**
	 * <p>Campo <code>CREUTIL</code>, &iacute;ndice: <code>13</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 13, nombre = "CREUTIL", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal creutil;

	/**
	 * <p>Campo <code>CREDISP</code>, &iacute;ndice: <code>14</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 14, nombre = "CREDISP", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal credisp;

	/**
	 * <p>Campo <code>FECULPA</code>, &iacute;ndice: <code>15</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 15, nombre = "FECULPA", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date feculpa;

	/**
	 * <p>Campo <code>NUMCUEP</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable
	@Campo(indice = 16, nombre = "NUMCUEP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 23, longitudMaxima = 23)
	private String numcuep;

	/**
	 * <p>Campo <code>NUMCUEU</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable
	@Campo(indice = 17, nombre = "NUMCUEU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 23, longitudMaxima = 23)
	private String numcueu;

	/**
	 * <p>Campo <code>DIVCONT</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "DIVCONT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divcont;

	/**
	 * <p>Campo <code>OFICINA</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "OFICINA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
	private String oficina;

	/**
	 * <p>Campo <code>TEACOMP</code>, &iacute;ndice: <code>20</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 20, nombre = "TEACOMP", tipo = TipoCampo.DECIMAL, longitudMinima = 7, longitudMaxima = 7, decimales = 4)
	private BigDecimal teacomp;

	/**
	 * <p>Campo <code>TEACOMU</code>, &iacute;ndice: <code>21</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 21, nombre = "TEACOMU", tipo = TipoCampo.DECIMAL, longitudMinima = 7, longitudMaxima = 7, decimales = 4)
	private BigDecimal teacomu;

	/**
	 * <p>Campo <code>TEACMPP</code>, &iacute;ndice: <code>22</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 22, nombre = "TEACMPP", tipo = TipoCampo.DECIMAL, longitudMinima = 7, longitudMaxima = 7, decimales = 4)
	private BigDecimal teacmpp;

	/**
	 * <p>Campo <code>TEACMPU</code>, &iacute;ndice: <code>23</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 23, nombre = "TEACMPU", tipo = TipoCampo.DECIMAL, longitudMinima = 7, longitudMaxima = 7, decimales = 4)
	private BigDecimal teacmpu;

	/**
	 * <p>Campo <code>CREUTIP</code>, &iacute;ndice: <code>24</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 24, nombre = "CREUTIP", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal creutip;

	/**
	 * <p>Campo <code>CREUTIU</code>, &iacute;ndice: <code>25</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 25, nombre = "CREUTIU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal creutiu;

	/**
	 * <p>Campo <code>TEAAVAP</code>, &iacute;ndice: <code>26</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 26, nombre = "TEAAVAP", tipo = TipoCampo.DECIMAL, longitudMinima = 7, longitudMaxima = 7, decimales = 4)
	private BigDecimal teaavap;

	/**
	 * <p>Campo <code>TEAAVAU</code>, &iacute;ndice: <code>27</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 27, nombre = "TEAAVAU", tipo = TipoCampo.DECIMAL, longitudMinima = 7, longitudMaxima = 7, decimales = 4)
	private BigDecimal teaavau;

	/**
	 * <p>Campo <code>TEAMORP</code>, &iacute;ndice: <code>28</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 28, nombre = "TEAMORP", tipo = TipoCampo.DECIMAL, longitudMinima = 7, longitudMaxima = 7, decimales = 4)
	private BigDecimal teamorp;

	/**
	 * <p>Campo <code>TEAMORU</code>, &iacute;ndice: <code>29</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 29, nombre = "TEAMORU", tipo = TipoCampo.DECIMAL, longitudMinima = 7, longitudMaxima = 7, decimales = 4)
	private BigDecimal teamoru;

	/**
	 * <p>Campo <code>NUMMOVI</code>, &iacute;ndice: <code>30</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 30, nombre = "NUMMOVI", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer nummovi;

	/**
	 * <p>Campo <code>IMPCUOT</code>, &iacute;ndice: <code>31</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 31, nombre = "IMPCUOT", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal impcuot;

	/**
	 * <p>Campo <code>MONMIPE</code>, &iacute;ndice: <code>32</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 32, nombre = "MONMIPE", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal monmipe;

	/**
	 * <p>Campo <code>CUOPAPE</code>, &iacute;ndice: <code>33</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 33, nombre = "CUOPAPE", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal cuopape;

	/**
	 * <p>Campo <code>PAGMIPE</code>, &iacute;ndice: <code>34</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 34, nombre = "PAGMIPE", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal pagmipe;

	/**
	 * <p>Campo <code>PAGTOPE</code>, &iacute;ndice: <code>35</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 35, nombre = "PAGTOPE", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal pagtope;

	/**
	 * <p>Campo <code>PUNTMES</code>, &iacute;ndice: <code>36</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 36, nombre = "PUNTMES", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
	private Integer puntmes;

	/**
	 * <p>Campo <code>MONMIUS</code>, &iacute;ndice: <code>37</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 37, nombre = "MONMIUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal monmius;

	/**
	 * <p>Campo <code>CUOPAUS</code>, &iacute;ndice: <code>38</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 38, nombre = "CUOPAUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal cuopaus;

	/**
	 * <p>Campo <code>PAGMIUS</code>, &iacute;ndice: <code>39</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 39, nombre = "PAGMIUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal pagmius;

	/**
	 * <p>Campo <code>PAGTOUS</code>, &iacute;ndice: <code>40</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 40, nombre = "PAGTOUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal pagtous;

	/**
	 * <p>Campo <code>IMPDETP</code>, &iacute;ndice: <code>41</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 41, nombre = "IMPDETP", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal impdetp;

	/**
	 * <p>Campo <code>IMPDETU</code>, &iacute;ndice: <code>42</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 42, nombre = "IMPDETU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal impdetu;

	/**
	 * <p>Campo <code>IMPATPE</code>, &iacute;ndice: <code>43</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 43, nombre = "IMPATPE", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal impatpe;

	/**
	 * <p>Campo <code>IMPINPE</code>, &iacute;ndice: <code>44</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 44, nombre = "IMPINPE", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal impinpe;

	/**
	 * <p>Campo <code>IMPCOPE</code>, &iacute;ndice: <code>45</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 45, nombre = "IMPCOPE", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal impcope;

	/**
	 * <p>Campo <code>IMPATUS</code>, &iacute;ndice: <code>46</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 46, nombre = "IMPATUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal impatus;

	/**
	 * <p>Campo <code>IMPINUS</code>, &iacute;ndice: <code>47</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 47, nombre = "IMPINUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal impinus;

	/**
	 * <p>Campo <code>IMPCOUS</code>, &iacute;ndice: <code>48</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 48, nombre = "IMPCOUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, decimales = 2)
	private BigDecimal impcous;

	/**
	 * <p>Campo <code>PUNACUM</code>, &iacute;ndice: <code>49</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 49, nombre = "PUNACUM", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
	private Integer punacum;

	/**
	 * <p>Campo <code>BONOMES</code>, &iacute;ndice: <code>50</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 50, nombre = "BONOMES", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
	private Integer bonomes;

	/**
	 * <p>Campo <code>MAPMINI</code>, &iacute;ndice: <code>51</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 51, nombre = "MAPMINI", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer mapmini;

	/**
	 * <p>Campo <code>MAPMINF</code>, &iacute;ndice: <code>52</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 52, nombre = "MAPMINF", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer mapminf;

	/**
	 * <p>Campo <code>NUMLIFE</code>, &iacute;ndice: <code>53</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable
	@Campo(indice = 53, nombre = "NUMLIFE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 11, longitudMaxima = 11)
	private String numlife;

	/**
	 * <p>Campo <code>NUMCONT</code>, &iacute;ndice: <code>54</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable
	@Campo(indice = 54, nombre = "NUMCONT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 23, longitudMaxima = 23)
	private String numcont;

}

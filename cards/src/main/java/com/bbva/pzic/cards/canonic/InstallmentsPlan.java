package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.bbva.jee.arq.spring.core.servicing.utils.Money;
import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created on 03/07/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "InstallmentsPlan", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "InstallmentsPlan", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstallmentsPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String transactionId;

    private Terms terms;

    private Money capital;

    private Money interest;

    private Money total;

    private List<Rate> rates;

    private List<ScheduleInstallment> scheduledPayments;
    /**
     * Total amount amortized at the moment of the retrieval.
     */
    private Import amortizedAmount;

    /**
     * Installment plan.
     */
    private FinancingType financingType;

    /**
     * Pending amount to pay of the installment plan.
     */
    private Import outstandingBalance;
    /**
     * Evolution of installments paid made to date expressed as a percentage.
     */
    private BigDecimal amortizedPercentage;
    /**
     * Description of installment plan.
     */
    private String description;
    /**
     * String based on ISO-8601 date format for providing the date when the
     * installment plan was applied.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar startDate;
    /**
     * String based on ISO-8601 date format for providing the first installment date.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date firstInstallmentDate;
    /**
     * String based on ISO-8601 date format for the date when the original transaction was made.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date operationDate;

    /**
     * Amount corresponding to the first payment of the financing plan.
     */
    private Import firstInstallment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Terms getTerms() {
        return terms;
    }

    public void setTerms(Terms terms) {
        this.terms = terms;
    }

    public Money getCapital() {
        return capital;
    }

    public void setCapital(Money capital) {
        this.capital = capital;
    }

    public Money getInterest() {
        return interest;
    }

    public void setInterest(Money interest) {
        this.interest = interest;
    }

    public Money getTotal() {
        return total;
    }

    public void setTotal(Money total) {
        this.total = total;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }

    public List<ScheduleInstallment> getScheduledPayments() {
        return scheduledPayments;
    }

    public void setScheduledPayments(List<ScheduleInstallment> scheduledPayments) {
        this.scheduledPayments = scheduledPayments;
    }

    public Import getAmortizedAmount() {
        return amortizedAmount;
    }

    public void setAmortizedAmount(Import amortizedAmount) {
        this.amortizedAmount = amortizedAmount;
    }

    public FinancingType getFinancingType() {
        return financingType;
    }

    public void setFinancingType(FinancingType financingType) {
        this.financingType = financingType;
    }

    public Import getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(Import outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    public BigDecimal getAmortizedPercentage() {
        return amortizedPercentage;
    }

    public void setAmortizedPercentage(BigDecimal amortizedPercentage) {
        this.amortizedPercentage = amortizedPercentage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Date getFirstInstallmentDate() {
        return firstInstallmentDate;
    }

    public void setFirstInstallmentDate(Date firstInstallmentDate) {
        this.firstInstallmentDate = firstInstallmentDate;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    public Import getFirstInstallment() {
        return firstInstallment;
    }

    public void setFirstInstallment(Import firstInstallment) {
        this.firstInstallment = firstInstallment;
    }
}

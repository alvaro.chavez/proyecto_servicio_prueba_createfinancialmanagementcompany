package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>participant</code>, utilizado por la clase <code>Participants</code></p>
 * 
 * @see Participants
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Participant {
	
	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 16, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>isCustomer</code>, &iacute;ndice: <code>2</code>, tipo: <code>BOOLEAN</code>
	 */
	@Campo(indice = 2, nombre = "isCustomer", tipo = TipoCampo.BOOLEAN, signo = true, obligatorio = true)
	private Boolean iscustomer;
	
	/**
	 * <p>Campo <code>personType</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "personType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
	private String persontype;
	
	/**
	 * <p>Campo <code>participantType</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "participantType", tipo = TipoCampo.DTO)
	private Participanttype participanttype;
	
	/**
	 * <p>Campo <code>firstName</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "firstName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
	private String firstname;
	
	/**
	 * <p>Campo <code>lastName</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "lastName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true)
	private String lastname;
	
}
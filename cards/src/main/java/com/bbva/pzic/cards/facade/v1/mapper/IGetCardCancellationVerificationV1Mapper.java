package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardCancellationVerification;
import com.bbva.pzic.cards.facade.v1.dto.CancellationVerification;

public interface IGetCardCancellationVerificationV1Mapper {

    InputGetCardCancellationVerification mapIn(String cardId);

    ServiceResponse<CancellationVerification> mapOut(CancellationVerification cancellationVerification);

}

package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class DTOIntMembership {

    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    @Size(max = 2, groups = ValidationGroup.CreateCardsProposal.class)
    private String id;

    private String description;

    @Size(max = 20, groups = ValidationGroup.CreateCardsProposal.class)
    private String number;

    private Date enrollmentDate;
    private Date cancellationDate;
    private DTOIntStatus status;
    private Boolean issuedDueToMigration;
    private Date expirationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getEnrollmentDate() {
        return enrollmentDate;
    }

    public void setEnrollmentDate(Date enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public DTOIntStatus getStatus() {
        return status;
    }

    public void setStatus(DTOIntStatus status) {
        this.status = status;
    }

    public Boolean getIssuedDueToMigration() {
        return issuedDueToMigration;
    }

    public void setIssuedDueToMigration(Boolean issuedDueToMigration) {
        this.issuedDueToMigration = issuedDueToMigration;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}

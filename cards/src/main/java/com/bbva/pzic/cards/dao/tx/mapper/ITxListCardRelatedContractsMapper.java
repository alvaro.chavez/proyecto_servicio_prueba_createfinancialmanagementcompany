package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntRelatedContractsSearchCriteria;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMENL5;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMS1L5;
import com.bbva.pzic.cards.facade.v0.dto.RelatedContracts;

import java.util.List;

public interface ITxListCardRelatedContractsMapper {

    FormatoMPMENL5 mapInput(DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria);

    List<RelatedContracts> mapOutput(FormatoMPMS1L5 formatoMPMS1L5, List<RelatedContracts> dtoIntRelatedContractsList);
}

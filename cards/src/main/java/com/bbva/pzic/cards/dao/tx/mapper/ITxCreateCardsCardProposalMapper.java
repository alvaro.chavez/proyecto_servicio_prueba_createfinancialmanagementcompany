package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCardsCardProposal;
import com.bbva.pzic.cards.canonic.CardProposal;
import com.bbva.pzic.cards.dao.model.kb93.FormatoKTECKB93;
import com.bbva.pzic.cards.dao.model.kb93.FormatoKTSKB931;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public interface ITxCreateCardsCardProposalMapper {

    FormatoKTECKB93 mapIn(InputCreateCardsCardProposal dtoIn);

    CardProposal mapOut(FormatoKTSKB931 formatOutput);
}
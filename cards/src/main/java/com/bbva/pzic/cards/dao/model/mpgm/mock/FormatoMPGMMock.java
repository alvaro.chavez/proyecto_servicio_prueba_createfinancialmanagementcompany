package com.bbva.pzic.cards.dao.model.mpgm.mock;

import com.bbva.pzic.cards.dao.model.mpgm.FormatoMPMS1GM;
import com.bbva.pzic.cards.dao.model.mpgm.FormatoMPMS2GM;
import com.bbva.pzic.cards.dao.model.mpgm.FormatoMPMS3GM;
import com.bbva.pzic.cards.dao.model.mpgm.FormatoMPMS4GM;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 26/12/2017.
 *
 * @author Entelgy
 */

public class FormatoMPGMMock {

    private static final FormatoMPGMMock INSTANCE = new FormatoMPGMMock();

    private ObjectMapperHelper mapper;

    private FormatoMPGMMock() {
        mapper = ObjectMapperHelper.getInstance();
    }

    public static FormatoMPGMMock getInstance() {
        return INSTANCE;
    }

    public FormatoMPMS1GM getFormatoMPMS1GM() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgm/mock/formatoMPMS1GM.json"), FormatoMPMS1GM.class);
    }

    public FormatoMPMS2GM getFormatoMPMS2GM() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgm/mock/formatoMPMS2GM.json"), FormatoMPMS2GM.class);
    }

    public FormatoMPMS3GM getFormatoMPMS3GM() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgm/mock/formatoMPMS3GM.json"), FormatoMPMS3GM.class);
    }

    public FormatoMPMS4GM getFormatoMPMS4GM() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpgm/mock/formatoMPMS4GM.json"), FormatoMPMS4GM.class);
    }
}

package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.facade.v0.dto.*;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardCashRefundMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Mapper
public class CreateCardCashRefundMapper implements ICreateCardCashRefundMapper {

    private static final Log LOG = LogFactory.getLog(CreateCardCashRefundMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    @Autowired
    private EnumMapper enumMapper;

    @Override
    public DTOInputCreateCashRefund mapInput(final String cardId, final CashRefund cashRefund) {
        LOG.info("----- Dentro de CreateCardCashRefundMapper.mapInput() -----");
        DTOInputCreateCashRefund dtoInputCreateCashRefund = new DTOInputCreateCashRefund();
        dtoInputCreateCashRefund.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.CARDID));
        DTOIntCashRefund dtoIntCashRefund = new DTOIntCashRefund();
        dtoIntCashRefund.setRefundType(enumMapper.getBackendValue("cards.refundType", cashRefund.getRefundType()));
        DTOIntRefundAmount dtoIntRefundAmount = new DTOIntRefundAmount();
        if (cashRefund.getRefundAmount() != null) {
            dtoIntRefundAmount.setCurrency(cashRefund.getRefundAmount().getCurrency());
        }
        dtoIntCashRefund.setRefundAmount(dtoIntRefundAmount);
        DTOIntReceivingAccount dtoIntReceivingAccount = new DTOIntReceivingAccount();
        if (cashRefund.getReceivingAccount() != null &&
                "COLLECT_IN_ACCOUNT".equals(cashRefund.getRefundType())) {
            dtoIntReceivingAccount.setId(cashRefund.getReceivingAccount().getId());
        }
        dtoIntCashRefund.setReceivingAccount(dtoIntReceivingAccount);
        dtoInputCreateCashRefund.setCashRefund(dtoIntCashRefund);
        return dtoInputCreateCashRefund;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<CashRefund> mapOutput(final DTOIntCashRefund dtoIntCashRefund) {
        LOG.info("----- Dentro de CreateCardCashRefundMapper.mapOutput() -----");

        if (dtoIntCashRefund == null) {
            return null;
        }

        CashRefund cashRefund = new CashRefund();
        cashRefund.setId(dtoIntCashRefund.getId());
        cashRefund.setDescription(dtoIntCashRefund.getDescription());
        cashRefund.setRefundType(dtoIntCashRefund.getRefundType());

        if (dtoIntCashRefund.getRefundAmount() != null) {
            RefundAmount refundAmount = new RefundAmount();
            refundAmount.setAmount(dtoIntCashRefund.getRefundAmount().getAmount());
            refundAmount.setCurrency(dtoIntCashRefund.getRefundAmount().getCurrency());
            cashRefund.setRefundAmount(refundAmount);
        }

        if (dtoIntCashRefund.getReceivingAccount() != null) {
            ReceivingAccount receivingAccount = new ReceivingAccount();
            receivingAccount.setId(dtoIntCashRefund.getReceivingAccount().getId());
            cashRefund.setReceivingAccount(receivingAccount);
        }

        cashRefund.setOperationDate(dtoIntCashRefund.getOperationDate());
        cashRefund.setAccountingDate(dtoIntCashRefund.getAccoutingDate());

        if (dtoIntCashRefund.getReceivedAmount() != null) {
            ReceivedAmount receivedAmount = new ReceivedAmount();
            receivedAmount.setAmount(dtoIntCashRefund.getReceivedAmount().getAmount());
            receivedAmount.setCurrency(dtoIntCashRefund.getReceivedAmount().getCurrency());
            cashRefund.setReceivedAmount(receivedAmount);
        }

        if (dtoIntCashRefund.getExchangeRate() != null) {
            ExchangeRate exchangeRate = new ExchangeRate();
            exchangeRate.setDate(dtoIntCashRefund.getExchangeRate().getDate());
            if (dtoIntCashRefund.getExchangeRate().getValues() != null) {
                Values values = new Values();
                if (dtoIntCashRefund.getExchangeRate().getValues().getFactor() != null) {
                    Factor factor = new Factor();
                    factor.setValue(dtoIntCashRefund.getExchangeRate().getValues().getFactor().getValue());
                    values.setFactor(factor);
                }
                values.setPriceType(dtoIntCashRefund.getExchangeRate().getValues().getPriceType());
                exchangeRate.setValues(values);
            }
            cashRefund.setExchangeRate(exchangeRate);
        }

        if (dtoIntCashRefund.getTaxes() != null) {
            Taxes taxes = new Taxes();

            if (dtoIntCashRefund.getTaxes().getTotalTaxes() != null) {
                TotalTaxes totalTaxes = new TotalTaxes();
                totalTaxes.setAmount(dtoIntCashRefund.getTaxes().getTotalTaxes().getAmount());
                totalTaxes.setCurrency(dtoIntCashRefund.getTaxes().getTotalTaxes().getCurrency());
                taxes.setTotalTaxes(totalTaxes);

            }

            if (dtoIntCashRefund.getTaxes().getItemizeTaxes() != null) {
                List<ItemizeTaxes> itemizeTaxesList = new ArrayList<>();
                for (DTOIntItemizeTaxes dtoIntItemizeTaxes : dtoIntCashRefund.getTaxes().getItemizeTaxes()) {
                    ItemizeTaxes itemizeTaxes = new ItemizeTaxes();

                    itemizeTaxes.setTaxType(dtoIntItemizeTaxes.getTaxType());

                    if (dtoIntItemizeTaxes.getMonetaryAmount() != null) {
                        MonetaryAmount monetaryAmount = new MonetaryAmount();
                        monetaryAmount.setAmount(dtoIntItemizeTaxes.getMonetaryAmount().getAmount());
                        monetaryAmount.setCurrency(dtoIntItemizeTaxes.getMonetaryAmount().getCurrency());
                        itemizeTaxes.setMonetaryAmount(monetaryAmount);
                    }

                    itemizeTaxesList.add(itemizeTaxes);
                }

                taxes.setItemizeTaxes(itemizeTaxesList);
            }

            cashRefund.setTaxes(taxes);
        }

        return ServiceResponse.data(cashRefund).build();
    }
}

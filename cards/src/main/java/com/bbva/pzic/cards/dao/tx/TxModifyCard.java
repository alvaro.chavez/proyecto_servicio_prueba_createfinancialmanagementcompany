package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.dao.model.mpb5.FormatoMPM0B5E;
import com.bbva.pzic.cards.dao.model.mpb5.PeticionTransaccionMpb5;
import com.bbva.pzic.cards.dao.model.mpb5.RespuestaTransaccionMpb5;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.NoneOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created on 14/08/2017.
 *
 * @author Entelgy
 */
@Component("txModifyCard")
public class TxModifyCard
        extends NoneOutputFormat<DTOIntCard, FormatoMPM0B5E> {

    @Autowired
    private ITxModifyCardMapper txModifyCardMapper;

    public TxModifyCard(@Qualifier("transaccionMpb5") InvocadorTransaccion<PeticionTransaccionMpb5, RespuestaTransaccionMpb5> transaction) {
        super(transaction, PeticionTransaccionMpb5::new);
    }

    @Override
    protected FormatoMPM0B5E mapInput(DTOIntCard dtoIntCard) {
        return txModifyCardMapper.mapInput(dtoIntCard);
    }
}

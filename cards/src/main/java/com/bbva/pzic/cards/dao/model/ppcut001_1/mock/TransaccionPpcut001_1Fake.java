package com.bbva.pzic.cards.dao.model.ppcut001_1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.ppcut001_1.PeticionTransaccionPpcut001_1;
import com.bbva.pzic.cards.dao.model.ppcut001_1.RespuestaTransaccionPpcut001_1;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Invocador de la transacci&oacute;n <code>PCUNT001</code>
 *
 * @see PeticionTransaccionPpcut001_1
 * @see RespuestaTransaccionPpcut001_1
 */
@Component
public class TransaccionPpcut001_1Fake implements InvocadorTransaccion<PeticionTransaccionPpcut001_1, RespuestaTransaccionPpcut001_1> {

    public static final String TEST_EMPTY = "999";

    @Override
    public RespuestaTransaccionPpcut001_1 invocar(PeticionTransaccionPpcut001_1 transaccion) {
        try {
            if (transaccion.getEntityin() != null && transaccion.getEntityin().getCardtype() != null &&
                    transaccion.getEntityin().getCardtype().getId().equalsIgnoreCase(TEST_EMPTY)) {
                return new RespuestaTransaccionPpcut001_1();
            }
            return Ppcut001_1Stubs.getInstance().getProposal();
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionPpcut001_1 invocarCache(PeticionTransaccionPpcut001_1 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

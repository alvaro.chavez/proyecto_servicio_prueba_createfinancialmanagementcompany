package com.bbva.pzic.cards.dao;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.*;

import java.util.List;

/**
 * @author Entelgy
 */
public interface ICardsDAO {

    /**
     * Adds a new card
     *
     * @param input card data
     * @return the id of card created
     */
    CardData createCard(DTOIntCard input);

    /**
     * Adds a new block to the card.
     *
     * @param block block data
     * @return the block created or modified
     */
    BlockData modifyCardBlock(DTOIntBlock block);

    /**
     * Updates an operational activation of the card
     *
     * @param activation activation data
     * @return updated activation data
     */
    void modifyCardActivation(DTOIntActivation activation);

    /**
     * Card listing query
     *
     * @param dtoIn DTO with the input fields
     * @return List cards
     */
    DTOOutListCards listCards(DTOIntListCards dtoIn);

    /**
     * Gets all transactions that match the query filter
     *
     * @param queryFilter Object with the datas of input
     * @return a list of transactions
     */
    TransactionsData listCardTransactions(DTOInputListCardTransactions queryFilter);

    /**
     * Method that communicates with HOST
     *
     * @param dtoIn Object with the datas of input
     * @return with the datas of output
     */
    DTOOutCreateCardRelatedContract createCardRelatedContract(DTOInputCreateCardRelatedContract dtoIn);

    /**
     * Method that communicates with HOST
     *
     * @param input Object with the datas of input
     * @return with the datas of output
     */
    SecurityData getCardSecurityData(InputGetCardSecurityData input);

    InstallmentsPlanData createInstallmentsPlan(DTOIntCardInstallmentsPlan dtoIn);

    InstallmentsPlanSimulationData simulateInstallmentsPlan(DTOIntCardInstallmentsPlan dtoIn);

    void modifyCard(DTOIntCard dtoIn);

    void modifyPartialCardBlock(DTOIntBlock dtoInt);

    void modifyCardPin(DTOIntPin dtoInt);

    List<Activation> listCardActivations(DTOIntCard dtoIn);

    List<Activation> modifyCardActivations(InputModifyCardActivations dtoIn);

    CardProposal createCardsCardProposal(InputCreateCardsCardProposal input);
}

package com.bbva.pzic.cards.business.dto;

public class DTOIntItemizeTaxes {

    private String taxType;
    private DTOIntMonetaryAmount monetaryAmount;

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public DTOIntMonetaryAmount getMonetaryAmount() {
        return monetaryAmount;
    }

    public void setMonetaryAmount(DTOIntMonetaryAmount monetaryAmount) {
        this.monetaryAmount = monetaryAmount;
    }
}

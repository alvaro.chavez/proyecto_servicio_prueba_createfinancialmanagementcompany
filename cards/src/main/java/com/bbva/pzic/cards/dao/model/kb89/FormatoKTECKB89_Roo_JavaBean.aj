// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.kb89;

import com.bbva.pzic.cards.dao.model.kb89.FormatoKTECKB89;
import java.math.BigDecimal;
import java.util.Date;

privileged aspect FormatoKTECKB89_Roo_JavaBean {
    
    /**
     * Gets idtipta value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdtipta() {
        return this.idtipta;
    }
    
    /**
     * Sets idtipta value
     * 
     * @param idtipta
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdtipta(String idtipta) {
        this.idtipta = idtipta;
        return this;
    }
    
    /**
     * Gets idprodu value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdprodu() {
        return this.idprodu;
    }
    
    /**
     * Sets idprodu value
     * 
     * @param idprodu
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdprodu(String idprodu) {
        this.idprodu = idprodu;
        return this;
    }
    
    /**
     * Gets idsprod value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdsprod() {
        return this.idsprod;
    }
    
    /**
     * Sets idsprod value
     * 
     * @param idsprod
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdsprod(String idsprod) {
        this.idsprod = idsprod;
        return this;
    }
    
    /**
     * Gets iddesti value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIddesti() {
        return this.iddesti;
    }
    
    /**
     * Sets iddesti value
     * 
     * @param iddesti
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIddesti(String iddesti) {
        this.iddesti = iddesti;
        return this;
    }
    
    /**
     * Gets idtpago value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdtpago() {
        return this.idtpago;
    }
    
    /**
     * Sets idtpago value
     * 
     * @param idtpago
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdtpago(String idtpago) {
        this.idtpago = idtpago;
        return this;
    }
    
    /**
     * Gets idperio value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdperio() {
        return this.idperio;
    }
    
    /**
     * Sets idperio value
     * 
     * @param idperio
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdperio(String idperio) {
        this.idperio = idperio;
        return this;
    }
    
    /**
     * Gets diapago value
     * 
     * @return Integer
     */
    public Integer FormatoKTECKB89.getDiapago() {
        return this.diapago;
    }
    
    /**
     * Sets diapago value
     * 
     * @param diapago
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setDiapago(Integer diapago) {
        this.diapago = diapago;
        return this;
    }
    
    /**
     * Gets monline value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoKTECKB89.getMonline() {
        return this.monline;
    }
    
    /**
     * Sets monline value
     * 
     * @param monline
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setMonline(BigDecimal monline) {
        this.monline = monline;
        return this;
    }
    
    /**
     * Gets divisal value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getDivisal() {
        return this.divisal;
    }
    
    /**
     * Sets divisal value
     * 
     * @param divisal
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setDivisal(String divisal) {
        this.divisal = divisal;
        return this;
    }
    
    /**
     * Gets idtstea value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdtstea() {
        return this.idtstea;
    }
    
    /**
     * Sets idtstea value
     * 
     * @param idtstea
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdtstea(String idtstea) {
        this.idtstea = idtstea;
        return this;
    }
    
    /**
     * Gets tasatea value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoKTECKB89.getTasatea() {
        return this.tasatea;
    }
    
    /**
     * Sets tasatea value
     * 
     * @param tasatea
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setTasatea(BigDecimal tasatea) {
        this.tasatea = tasatea;
        return this;
    }
    
    /**
     * Gets idttcea value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdttcea() {
        return this.idttcea;
    }
    
    /**
     * Sets idttcea value
     * 
     * @param idttcea
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdttcea(String idttcea) {
        this.idttcea = idttcea;
        return this;
    }
    
    /**
     * Gets tastcea value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoKTECKB89.getTastcea() {
        return this.tastcea;
    }
    
    /**
     * Sets tastcea value
     * 
     * @param tastcea
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setTastcea(BigDecimal tastcea) {
        this.tastcea = tastcea;
        return this;
    }
    
    /**
     * Gets fectasa value
     * 
     * @return Date
     */
    public Date FormatoKTECKB89.getFectasa() {
        return this.fectasa;
    }
    
    /**
     * Sets fectasa value
     * 
     * @param fectasa
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setFectasa(Date fectasa) {
        this.fectasa = fectasa;
        return this;
    }
    
    /**
     * Gets tiptasa value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getTiptasa() {
        return this.tiptasa;
    }
    
    /**
     * Sets tiptasa value
     * 
     * @param tiptasa
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setTiptasa(String tiptasa) {
        this.tiptasa = tiptasa;
        return this;
    }
    
    /**
     * Gets idcoms1 value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdcoms1() {
        return this.idcoms1;
    }
    
    /**
     * Sets idcoms1 value
     * 
     * @param idcoms1
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdcoms1(String idcoms1) {
        this.idcoms1 = idcoms1;
        return this;
    }
    
    /**
     * Gets nomcom1 value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getNomcom1() {
        return this.nomcom1;
    }
    
    /**
     * Sets nomcom1 value
     * 
     * @param nomcom1
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setNomcom1(String nomcom1) {
        this.nomcom1 = nomcom1;
        return this;
    }
    
    /**
     * Gets impcom1 value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoKTECKB89.getImpcom1() {
        return this.impcom1;
    }
    
    /**
     * Sets impcom1 value
     * 
     * @param impcom1
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setImpcom1(BigDecimal impcom1) {
        this.impcom1 = impcom1;
        return this;
    }
    
    /**
     * Gets idcoms2 value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdcoms2() {
        return this.idcoms2;
    }
    
    /**
     * Sets idcoms2 value
     * 
     * @param idcoms2
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdcoms2(String idcoms2) {
        this.idcoms2 = idcoms2;
        return this;
    }
    
    /**
     * Gets nomcom2 value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getNomcom2() {
        return this.nomcom2;
    }
    
    /**
     * Sets nomcom2 value
     * 
     * @param nomcom2
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setNomcom2(String nomcom2) {
        this.nomcom2 = nomcom2;
        return this;
    }
    
    /**
     * Gets impcom2 value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoKTECKB89.getImpcom2() {
        return this.impcom2;
    }
    
    /**
     * Sets impcom2 value
     * 
     * @param impcom2
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setImpcom2(BigDecimal impcom2) {
        this.impcom2 = impcom2;
        return this;
    }
    
    /**
     * Gets tipcomi value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getTipcomi() {
        return this.tipcomi;
    }
    
    /**
     * Sets tipcomi value
     * 
     * @param tipcomi
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setTipcomi(String tipcomi) {
        this.tipcomi = tipcomi;
        return this;
    }
    
    /**
     * Gets product value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getProduct() {
        return this.product;
    }
    
    /**
     * Sets product value
     * 
     * @param product
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setProduct(String product) {
        this.product = product;
        return this;
    }
    
    /**
     * Gets impprod value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoKTECKB89.getImpprod() {
        return this.impprod;
    }
    
    /**
     * Sets impprod value
     * 
     * @param impprod
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setImpprod(BigDecimal impprod) {
        this.impprod = impprod;
        return this;
    }
    
    /**
     * Gets idbanco value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdbanco() {
        return this.idbanco;
    }
    
    /**
     * Sets idbanco value
     * 
     * @param idbanco
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdbanco(String idbanco) {
        this.idbanco = idbanco;
        return this;
    }
    
    /**
     * Gets idofici value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdofici() {
        return this.idofici;
    }
    
    /**
     * Sets idofici value
     * 
     * @param idofici
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdofici(String idofici) {
        this.idofici = idofici;
        return this;
    }
    
    /**
     * Gets idpmemb value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdpmemb() {
        return this.idpmemb;
    }
    
    /**
     * Sets idpmemb value
     * 
     * @param idpmemb
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdpmemb(String idpmemb) {
        this.idpmemb = idpmemb;
        return this;
    }
    
    /**
     * Gets numpmem value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getNumpmem() {
        return this.numpmem;
    }
    
    /**
     * Sets numpmem value
     * 
     * @param numpmem
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setNumpmem(String numpmem) {
        this.numpmem = numpmem;
        return this;
    }
    
    /**
     * Gets ecenvio value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getEcenvio() {
        return this.ecenvio;
    }
    
    /**
     * Sets ecenvio value
     * 
     * @param ecenvio
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setEcenvio(String ecenvio) {
        this.ecenvio = ecenvio;
        return this;
    }
    
    /**
     * Gets idcelul value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdcelul() {
        return this.idcelul;
    }
    
    /**
     * Sets idcelul value
     * 
     * @param idcelul
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdcelul(String idcelul) {
        this.idcelul = idcelul;
        return this;
    }
    
    /**
     * Gets idofert value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIdofert() {
        return this.idofert;
    }
    
    /**
     * Sets idofert value
     * 
     * @param idofert
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIdofert(String idofert) {
        this.idofert = idofert;
        return this;
    }
    
    /**
     * Gets indnotf value
     * 
     * @return String
     */
    public String FormatoKTECKB89.getIndnotf() {
        return this.indnotf;
    }
    
    /**
     * Sets indnotf value
     * 
     * @param indnotf
     * @return FormatoKTECKB89
     */
    public FormatoKTECKB89 FormatoKTECKB89.setIndnotf(String indnotf) {
        this.indnotf = indnotf;
        return this;
    }
    
}

package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.facade.v1.dto.SecurityData;
import com.bbva.pzic.cards.facade.v1.mapper.IGetCardSecurityDataV1Mapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

@Mapper
public class GetCardSecurityDataV1Mapper implements IGetCardSecurityDataV1Mapper {

    @Override
    public InputGetCardSecurityData mapIn(final String cardId, final String publicKey) {
        InputGetCardSecurityData input = new InputGetCardSecurityData();
        input.setCardId(cardId);
        input.setPublicKey(publicKey);
        return input;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<List<SecurityData>> mapOut(final List<SecurityData> securityDataList) {
        if (CollectionUtils.isEmpty(securityDataList)) {
            return null;
        }
        return ServiceResponse.data(securityDataList).build();
    }
}

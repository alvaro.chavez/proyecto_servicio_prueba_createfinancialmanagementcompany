package com.bbva.pzic.cards.facade.v00.impl;

import com.bbva.jee.arq.spring.core.servicing.annotations.PATCH;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.pzic.cards.business.ISrvIntCards;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.IModifyCardBlockMapper;
import com.bbva.pzic.cards.facade.v0.mapper.ISimulateCardInstallmentsPlanMapper;
import com.bbva.pzic.cards.facade.v00.ISrvCardsV00;
import com.bbva.pzic.cards.facade.v00.mapper.ICreateCardInstallmentsPlanMapper;
import com.bbva.pzic.cards.facade.v00.mapper.IModifyCardMapper;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
@Path("/V00")
@SN(registryID = "SNPE1710030", logicalID = "cards")
@VN(vnn = "V00")
@Produces(MediaType.APPLICATION_JSON)
@Service
public class SrvCardsV00 implements ISrvCardsV00, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

    private static final Log LOG = LogFactory.getLog(SrvCardsV00.class);

    public UriInfo uriInfo;
    public HttpHeaders httpHeaders;

    @Autowired
    private IModifyCardMapper modifyCardMapper;

    @Autowired
    private ICreateCardInstallmentsPlanMapper createCardInstallmentsPlanMapper;

    @Autowired
    private ISimulateCardInstallmentsPlanMapper simulateCardInstallmentsPlanMapper;

    @Autowired
    private IModifyCardBlockMapper modifyCardBlockMapper;

    @Autowired
    private ISrvIntCards srvIntCards;

    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;

    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;

    @Override
    public void setUriInfo(UriInfo ui) {
        this.uriInfo = ui;
    }

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    @Override
    @POST
    @Path("cards/{card-id}/installments-plans")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN_V00, logicalID = "createCardInstallmentsPlan")
    public InstallmentsPlanData createCardInstallmentsPlan(@PathParam(CARD_ID) final String cardId,
                                                           final InstallmentsPlan installmentsPlan) {
        LOG.info("... called method SrvCardsV00.createCardInstallmentsPlan ...");
        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN_V00, installmentsPlan, pathParams, null);

        InstallmentsPlanData data = srvIntCards.createCardInstallmentsPlan(createCardInstallmentsPlanMapper.mapIn((String) pathParams.get(CARD_ID), installmentsPlan));

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN_V00, data, pathParams, null);

        return data;
    }

    @Override
    @POST
    @Path("/cards/{card-id}/installments-plans/simulate")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = "SMCPE1710077", logicalID = "simulateCardInstallmentsPlan")
    public InstallmentsPlanSimulationData simulateCardInstallmentsPlan(@PathParam(CARD_ID) final String cardId,
                                                                       InstallmentsPlanSimulation installmentsPlanSimulation) {
        LOG.info("... called method SrvCardsV00.simulateCardInstallmentsPlanV00 ...");
        return srvIntCards.simulateCardInstallmentsPlanV00(simulateCardInstallmentsPlanMapper.mapIn(cardId, installmentsPlanSimulation));
    }

    @Override
    @PATCH
    @Path("cards/{card-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_V00, logicalID = "modifyCard")
    public Response modifyCard(@PathParam(CARD_ID) final String cardId,
                               Card card) {
        LOG.info("... called method SrvCardsV00.modifyCard ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_V00, card, pathParams, null);

        srvIntCards.modifyCard(modifyCardMapper.mapIn((String) pathParams.get(CARD_ID), card));

        return Response.ok().build();
    }

    @Override
    @PUT
    @Path("/cards/{card-id}/blocks/{block-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK, logicalID = "modifyCardBlock")
    public BlockData modifyCardBlock(@PathParam(CARD_ID) final String cardId,
                                     @PathParam(BLOCK_ID) final String blockId,
                                     final Block block) {
        LOG.info("... called method SrvCardsV00.modifyCardBlock ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(BLOCK_ID, blockId);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK, block, pathParams, null);

        BlockData blockData = srvIntCards.modifyCardBlock(
                modifyCardBlockMapper.mapInput((String) pathParams.get(CARD_ID), (String) pathParams.get(BLOCK_ID), block));

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK, blockData, pathParams, null);


        return blockData;
    }
}

package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created on 2/11/2020.
 *
 * @author Entelgy.
 */
public class DTOIntGeolocation {

    private BigDecimal latitude;
    private BigDecimal longitude;

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
}

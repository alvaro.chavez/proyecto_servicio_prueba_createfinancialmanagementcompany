// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.ppcut003_1;

import com.bbva.pzic.cards.dao.model.ppcut003_1.Branch;
import com.bbva.pzic.cards.dao.model.ppcut003_1.Destination;

privileged aspect Destination_Roo_JavaBean {
    
    /**
     * Gets id value
     * 
     * @return String
     */
    public String Destination.getId() {
        return this.id;
    }
    
    /**
     * Sets id value
     * 
     * @param id
     * @return Destination
     */
    public Destination Destination.setId(String id) {
        this.id = id;
        return this;
    }
    
    /**
     * Gets name value
     * 
     * @return String
     */
    public String Destination.getName() {
        return this.name;
    }
    
    /**
     * Sets name value
     * 
     * @param name
     * @return Destination
     */
    public Destination Destination.setName(String name) {
        this.name = name;
        return this;
    }
    
    /**
     * Gets branch value
     * 
     * @return Branch
     */
    public Branch Destination.getBranch() {
        return this.branch;
    }
    
    /**
     * Sets branch value
     * 
     * @param branch
     * @return Destination
     */
    public Destination Destination.setBranch(Branch branch) {
        this.branch = branch;
        return this;
    }
    
}

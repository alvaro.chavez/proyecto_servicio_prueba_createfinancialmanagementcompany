// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.phiat021_1;

import com.bbva.pzic.cards.dao.model.phiat021_1.Answer;
import com.bbva.pzic.cards.dao.model.phiat021_1.Question;

privileged aspect Question_Roo_JavaBean {
    
    /**
     * Gets id value
     * 
     * @return String
     */
    public String Question.getId() {
        return this.id;
    }
    
    /**
     * Sets id value
     * 
     * @param id
     * @return Question
     */
    public Question Question.setId(String id) {
        this.id = id;
        return this;
    }
    
    /**
     * Gets answer value
     * 
     * @return Answer
     */
    public Answer Question.getAnswer() {
        return this.answer;
    }
    
    /**
     * Sets answer value
     * 
     * @param answer
     * @return Question
     */
    public Question Question.setAnswer(Answer answer) {
        this.answer = answer;
        return this;
    }
    
}

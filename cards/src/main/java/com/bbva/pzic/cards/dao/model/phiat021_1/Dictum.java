package com.bbva.pzic.cards.dao.model.phiat021_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>dictum</code>, utilizado por la clase <code>RespuestaTransaccionPhiat021_1</code></p>
 * 
 * @see RespuestaTransaccionPhiat021_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Dictum {
	
	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 11, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>reasons</code>, &iacute;ndice: <code>2</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 2, nombre = "reasons", tipo = TipoCampo.LIST)
	private List<Reasons> reasons;
	
}
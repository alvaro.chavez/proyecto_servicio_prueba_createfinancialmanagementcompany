package com.bbva.pzic.cards.facade.v01.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOInputCreateCardRelatedContract;
import com.bbva.pzic.cards.business.dto.DTOOutCreateCardRelatedContract;
import com.bbva.pzic.cards.canonic.NumberType;
import com.bbva.pzic.cards.canonic.RelatedContract;
import com.bbva.pzic.cards.canonic.RelatedContractData;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v01.mapper.ICreateCardRelatedContractMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 14/02/2017.
 *
 * @author Entelgy
 */
@Mapper
public class CreateCardRelatedContractMapper implements ICreateCardRelatedContractMapper {

    private static final Log LOG = LogFactory.getLog(CreateCardRelatedContractMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    /**
     * @see com.bbva.pzic.cards.facade.v01.mapper.ICreateCardRelatedContractMapper#mapIn(String, com.bbva.pzic.cards.canonic.RelatedContract)
     */
    @Override
    public DTOInputCreateCardRelatedContract mapIn(final String cardId, final RelatedContract relatedContract) {
        LOG.info("... called method CreateRelatedContractMapper.mapIn ...");

        DTOInputCreateCardRelatedContract dtoInput = new DTOInputCreateCardRelatedContract();
        //desencriptacion de cardId
        LOG.info("... encryt - cardId ...");
        dtoInput.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT));
        if (relatedContract != null && relatedContract.getContractId() != null) {
            LOG.info("... encryt - contraId ...");
            //desencriptacion contractId
            dtoInput.setContractId(cypherTool.decrypt(relatedContract.getContractId(), AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT));
        }
        return dtoInput;
    }

    /**
     * @see com.bbva.pzic.cards.facade.v01.mapper.ICreateCardRelatedContractMapper#mapOut(com.bbva.pzic.cards.business.dto.DTOOutCreateCardRelatedContract)
     */
    @Override
    public RelatedContractData mapOut(DTOOutCreateCardRelatedContract dtoOut) {
        LOG.info("... called method CreateRelatedContractMapper.mapOut ...");
        if (dtoOut.getRelatedContractId() == null && dtoOut.getContractId() == null &&
                dtoOut.getNumberTypeId() == null && dtoOut.getNumberTypeName() == null) {
            return null;
        }
        RelatedContract relatedContract = new RelatedContract();
        //encrypt
        relatedContract.setRelatedContractId(cypherTool.encrypt(dtoOut.getRelatedContractId(), AbstractCypherTool.IDCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT));
        relatedContract.setContractId(cypherTool.encrypt(dtoOut.getContractId(), AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_RELATED_CONTRACT));
        if (dtoOut.getNumberTypeId() != null || dtoOut.getNumberTypeName() != null) {
            NumberType numberType = new NumberType();
            numberType.setId(dtoOut.getNumberTypeId());
            numberType.setName(dtoOut.getNumberTypeName());
            relatedContract.setNumberType(numberType);
        }
        RelatedContractData data = new RelatedContractData();
        data.setData(relatedContract);
        return data;
    }
}

package com.bbva.pzic.cards.util;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.routine.commons.utils.DateUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class FunctionUtils {

    private FunctionUtils() {
    }

    public static Calendar buildDatetime(Date date, String time) {
        if (date == null || time == null) {
            return null;
        }

        try {
            return DateUtils.toDateTime(date, time);
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

    public static Calendar buildDatetime(String date, String time) {
        if (date == null || time == null) {
            return null;
        }

        try {
            return DateUtils.toDateTime(date, time);
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

    public static Calendar buildDatetime(String datetime) {
        if (datetime == null) {
            return null;
        }

        try {
            return DateUtils.toDateTime(datetime, "", null, Constants.DEFAULT_DATE);
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

    public static Date buildDate(String date) {
        if (date == null) {
            return null;
        }

        try {
            return DateUtils.toDate(date);
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }
}

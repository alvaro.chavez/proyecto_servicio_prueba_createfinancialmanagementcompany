package com.bbva.pzic.cards.dao.model.mcdv;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>MCDV</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMcdv</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMcdv</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: QGDTFDF.MPMENDV.D1200624.TXT
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�01�00001�NUMTARJ�NUMERO DE TARJETA   �A�016�0�R�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�02�00017�KEYPB01�LLAVE PUB. MOVIL 01 �A�075�0�R�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�03�00092�KEYPB02�LLAVE PUB. MOVIL 02 �A�075�0�R�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�04�00167�KEYPB03�LLAVE PUB. MOVIL 03 �A�075�0�R�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�05�00242�KEYPB04�LLAVE PUB. MOVIL 04 �A�075�0�R�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�06�00317�KEYPB05�LLAVE PUB. MOVIL 05 �A�075�0�O�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�07�00392�KEYPB06�LLAVE PUB. MOVIL 06 �A�075�0�O�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�08�00467�KEYPB07�LLAVE PUB. MOVIL 07 �A�075�0�O�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�09�00542�KEYPB08�LLAVE PUB. MOVIL 08 �A�075�0�O�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�10�00617�KEYPB09�LLAVE PUB. MOVIL 09 �A�075�0�O�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�11�00692�KEYPB10�LLAVE PUB. MOVIL 10 �A�075�0�O�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�12�00767�KEYPB11�LLAVE PUB. MOVIL 11 �A�075�0�O�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�13�00842�KEYPB12�LLAVE PUB. MOVIL 12 �A�075�0�O�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�14�00917�KEYPB13�LLAVE PUB. MOVIL 13 �A�075�0�O�        �
 * MPMENDV �LISTADO DE CODIGOS DE SEGURIDA�F�15�01066�15�00992�KEYPB14�LLAVE PUB. MOVIL 14 �A�075�0�O�        �
 * 
 * FICHERO: QGDTFDF.MPMS1DV.D1200624.TXT
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�01�00001�IDCODSG�ID CODIGO SEGURIDAD �A�004�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�02�00005�DSCODSG�DES.CODIGO SEGURIDAD�A�030�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�03�00035�CODSE01�COD.CVV2 CIFRADO 01 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�04�00110�CODSE02�COD.CVV2 CIFRADO 02 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�05�00185�CODSE03�COD.CVV2 CIFRADO 03 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�06�00260�CODSE04�COD.CVV2 CIFRADO 04 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�07�00335�CODSE05�COD.CVV2 CIFRADO 05 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�08�00410�CODSE06�COD.CVV2 CIFRADO 06 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�09�00485�CODSE07�COD.CVV2 CIFRADO 07 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�10�00560�CODSE08�COD.CVV2 CIFRADO 08 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�11�00635�CODSE09�COD.CVV2 CIFRADO 09 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�12�00710�CODSE10�COD.CVV2 CIFRADO 10 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�13�00785�CODSE11�COD.CVV2 CIFRADO 11 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�14�00860�CODSE12�COD.CVV2 CIFRADO 12 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�15�00935�CODSE13�COD.CVV2 CIFRADO 13 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�16�01010�CODSE14�COD.CVV2 CIFRADO 14 �A�075�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�17�01085�TIMERNS�TIMER NUME.SEGUNDOS �N�003�0�S�        �
 * MPMS1DV �LISTADO DE CODIGOS DE SEGURIDA�X�18�01088�18�01088�TIMERUN�UNIDAD DE TIEMPO    �A�001�0�S�        �
 * 
 * FICHERO: QGDTCCT.MCDV.D1200624.TXT
 * MCDVLISTADO DE CODIGOS DE SEGURIDAD    MP        MC2CMCDV     01 MPMENDV             MCDV  NN3000CNNNNN    SSTN     E  NNNSNNNN  NN                2020-06-24XP92347 2020-06-2415.16.36XP92347 2020-06-24-15.16.02.926882XP92347 0001-01-010001-01-01
 * 
 * FICHERO: QGDTFDX.MCDV.D1200624.TXT
 * MCDVMPMS1DV MPNCS1DVMC2CMCDV1S                             XP92347 2020-06-24-14.20.24.366778XP92347 2020-06-24-14.20.24.366834
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionMcdv
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MCDV",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMcdv.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENDV.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionMcdv implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}
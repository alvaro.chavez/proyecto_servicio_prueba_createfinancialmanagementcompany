package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardsCardOffer;
import com.bbva.pzic.cards.canonic.Offer;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardsCardOfferMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_GET_CARDS_CARD_OFFER;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class GetCardsCardOfferMapper implements IGetCardsCardOfferMapper {

    private static final Log LOG = LogFactory.getLog(GetCardsCardOfferMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    /**
     * {@inheritDoc}
     */
    @Override
    public InputGetCardsCardOffer mapIn(String cardId, String offerId) {
        LOG.info("... called method GetCardsCardOfferMapper.mapIn ...");
        if (cardId == null && offerId == null) {
            return null;
        }

        return new InputGetCardsCardOffer(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_GET_CARDS_CARD_OFFER), offerId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<Offer> mapOut(final Offer offer) {
        LOG.info("... called method GetCardsCardOfferMapper.mapOut ...");
        if (offer == null) {
            return null;
        }

        return ServiceResponse.data(offer).build();
    }
}

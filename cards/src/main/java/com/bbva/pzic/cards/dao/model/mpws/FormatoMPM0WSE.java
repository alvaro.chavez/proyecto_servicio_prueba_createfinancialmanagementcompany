package com.bbva.pzic.cards.dao.model.mpws;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import java.math.BigDecimal;

/**
 * Formato de datos <code>MPM0WSE</code> de la transacci&oacute;n <code>MPWS</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPM0WSE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPM0WSE {

    /**
     * <p>Campo <code>NUMTARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @DatoAuditable(omitir = true)
    @Campo(indice = 1, nombre = "NUMTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String numtarj;

    /**
     * <p>Campo <code>IMPOPER</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 2, nombre = "IMPOPER", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
    private BigDecimal impoper;

    /**
     * <p>Campo <code>NCUOTAS</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 3, nombre = "NCUOTAS", tipo = TipoCampo.ENTERO, longitudMinima = 2, longitudMaxima = 2)
    private Integer ncuotas;

    /**
     * <p>Campo <code>DIVOPER</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "DIVOPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String divoper;

    /**
     * <p>Campo <code>NUMOPER</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "NUMOPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String numoper;

}
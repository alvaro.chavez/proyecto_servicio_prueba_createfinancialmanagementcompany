package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "proposalCard", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "proposalCard", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProposalCard implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Porposal unique identifier.
     */
    private String id;
    /**
     * Financial product type.
     */
    private CardType cardType;
    /**
     * Financial product title.
     */
    private Product product;
    /*
     * Physical support
     */
    private PhysicalSupport physicalSupport;
    /**
     * Card delivery details for a physical card.
     */
    private List<Delivery> deliveries;
    /**
     * Entity associated to card.
     */
    private BankType bank;
    /**
     * Rate charge applied to the type of offer associated.
     */
    private ProposalRate rates;
    /**
     * Commissions associated with the contracting of a Credit Card. They may or may not come more than one fee depending on the offer related.
     */
    private TransferFees fees;
    /**
     * Additional products related to the new card.
     */
    private List<AdditionalProduct> additionalProducts;
    /**
     * Membership associated to the card.
     */
    private Membership membership;
    /**
     * Payment method related to a specific credit card.
     */
    private PaymentMethod paymentMethod;
    /**
     * Granted credit. This amount may be provided in several currencies
     * (depending on the country). This attribute is mandatory for credit cards.
     */
    private List<Import> grantedCredits;
    /**
     * On a business card the participantId will be the authorized person that will hold the card. It is required to specify AUTHORIZED as participant type also.
     */
    private List<KnowParticipant> participants;
    /**
     * The customer contact information.
     */
    private List<ContactDetail> contactDetails;
    /**
     * Porposal type identifier.
     */
    private ProposalType proposalType;
    /**
     * String based on the ISO-8601 time stamp format to specify the operation
     * date.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar operationDate;
    /**
     * Unique offer associated with the card.
     */
    private String offerId;

    private String operationNumber;

    private String status;

    private SpecificProposalContact contact;

    /**
     * Image selected for the customer to be printed on the card.
     */
    private Image image;
    /**
     * Detail of the schedule in which the customer can be contacted if necessary.
     */
    private ContactAbility contactability;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public PhysicalSupport getPhysicalSupport() {
        return physicalSupport;
    }

    public void setPhysicalSupport(PhysicalSupport physicalSupport) {
        this.physicalSupport = physicalSupport;
    }

    public List<Delivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<Delivery> deliveries) {
        this.deliveries = deliveries;
    }

    public BankType getBank() {
        return bank;
    }

    public void setBank(BankType bank) {
        this.bank = bank;
    }

    public ProposalRate getRates() {
        return rates;
    }

    public void setRates(ProposalRate rates) {
        this.rates = rates;
    }

    public TransferFees getFees() {
        return fees;
    }

    public void setFees(TransferFees fees) {
        this.fees = fees;
    }

    public List<AdditionalProduct> getAdditionalProducts() {
        return additionalProducts;
    }

    public void setAdditionalProducts(List<AdditionalProduct> additionalProducts) {
        this.additionalProducts = additionalProducts;
    }

    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<Import> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<Import> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public List<KnowParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<KnowParticipant> participants) {
        this.participants = participants;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public ProposalType getProposalType() {
        return proposalType;
    }

    public void setProposalType(ProposalType proposalType) {
        this.proposalType = proposalType;
    }

    public Calendar getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Calendar operationDate) {
        this.operationDate = operationDate;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public SpecificProposalContact getContact() {
        return contact;
    }

    public void setContact(SpecificProposalContact contact) {
        this.contact = contact;
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public ContactAbility getContactability() {
        return contactability;
    }

    public void setContactability(ContactAbility contactability) {
        this.contactability = contactability;
    }
}

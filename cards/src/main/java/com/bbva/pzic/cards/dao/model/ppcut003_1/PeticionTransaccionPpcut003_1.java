package com.bbva.pzic.cards.dao.model.ppcut003_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PPCUT003</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPpcut003_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPpcut003_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: modifyCardProposal - PPCUT003-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PPCUT003&quot; application=&quot;PPCU&quot; version=&quot;01&quot; country=&quot;PE&quot; language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;proposal-id&quot; type=&quot;String&quot; size=&quot;36&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;list order=&quot;2&quot; name=&quot;deliveries&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;delivery&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.DeliveryDTO&quot; artifactId=&quot;PPCUC001&quot;&gt;
 * &lt;dto name=&quot;serviceType&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;specificContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;contact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDetailDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactDetailType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;address&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;address&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AddressDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;addressType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;destination&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.DestinationDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;branch&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto order=&quot;3&quot; name=&quot;paymentMethod&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.PaymentMethodDTO&quot; artifactId=&quot;PPCUC001&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;frecuency&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FrequencyDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;daysOfMonth&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.DaysOfMonthDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;day&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;cutOffDay&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto order=&quot;4&quot; name=&quot;contactability&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactabilityDTO&quot; artifactId=&quot;PPCUC001&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;dto name=&quot;scheduleTimes&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ScheduleTimesDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;startTime&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;endTime&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;list name=&quot;grantedCredits&quot; order=&quot;5&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;grantedCredit&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.AmountDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;rates&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.RateDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;6&quot;&gt;
 * &lt;list name=&quot;itemizeRates&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeRate&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;rateType&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;itemizeRatesUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitRateType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;percentage&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;fees&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FeeDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;7&quot;&gt;
 * &lt;list name=&quot;itemizeFees&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeFee&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;feeType&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;itemizeFeeUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitType&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;offerId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;product&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ProductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;9&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;notificationsByOperation&quot; type=&quot;Boolean&quot; size=&quot;0&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;list order=&quot;1&quot; name=&quot;deliveries&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;delivery&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.DeliveryDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;serviceType&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;specificContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;contact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDetailDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactDetailType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;address&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;address&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AddressDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;addressType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;destination&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.DestinationDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;dto name=&quot;branch&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto order=&quot;2&quot; name=&quot;paymentMethod&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.PaymentMethodDTO&quot; artifactId=&quot;PPCUC001&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;frecuency&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FrecuencyDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;dto name=&quot;daysOfMonth&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.DaysOfMonthDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;day&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;cutOffDay&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto order=&quot;3&quot; name=&quot;contactability&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactabilityDTO&quot; artifactId=&quot;PPCUC001&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;dto name=&quot;scheduleTimes&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ScheduleTimesDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;startTime&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;endTime&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;list name=&quot;grantedCredits&quot; order=&quot;4&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;grantedCredit&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.AmountDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;rates&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.RateDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;5&quot;&gt;
 * &lt;list name=&quot;itemizeRates&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeRate&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;rateType&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;itemizeRatesUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitRateType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;percentage&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;fees&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FeeDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;6&quot;&gt;
 * &lt;list name=&quot;itemizeFees&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeFee&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;feeType&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;itemizeFeeUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitType&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;offerId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;product&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ProductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;8&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;notificationsByOperation&quot; type=&quot;Boolean&quot; size=&quot;0&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion que modifica parcialmente una solicitud&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPpcut003_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PPCUT003",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPpcut003_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPpcut003_1 {
		
		/**
	 * <p>Campo <code>proposal-id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "proposal-id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 36, signo = true, obligatorio = true)
	private String proposalId;
	
	/**
	 * <p>Campo <code>deliveries</code>, &iacute;ndice: <code>2</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 2, nombre = "deliveries", tipo = TipoCampo.LIST)
	private List<Deliveries> deliveries;
	
	/**
	 * <p>Campo <code>paymentMethod</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "paymentMethod", tipo = TipoCampo.DTO)
	private Paymentmethod paymentmethod;
	
	/**
	 * <p>Campo <code>contactability</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "contactability", tipo = TipoCampo.DTO)
	private Contactability contactability;
	
	/**
	 * <p>Campo <code>grantedCredits</code>, &iacute;ndice: <code>5</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 5, nombre = "grantedCredits", tipo = TipoCampo.LIST)
	private List<Grantedcredits> grantedcredits;
	
	/**
	 * <p>Campo <code>rates</code>, &iacute;ndice: <code>6</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 6, nombre = "rates", tipo = TipoCampo.DTO)
	private Rates rates;
	
	/**
	 * <p>Campo <code>fees</code>, &iacute;ndice: <code>7</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 7, nombre = "fees", tipo = TipoCampo.DTO)
	private Fees fees;
	
	/**
	 * <p>Campo <code>offerId</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "offerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String offerid;
	
	/**
	 * <p>Campo <code>product</code>, &iacute;ndice: <code>9</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 9, nombre = "product", tipo = TipoCampo.DTO)
	private Product product;
	
	/**
	 * <p>Campo <code>notificationsByOperation</code>, &iacute;ndice: <code>10</code>, tipo: <code>BOOLEAN</code>
	 */
	@Campo(indice = 10, nombre = "notificationsByOperation", tipo = TipoCampo.BOOLEAN, signo = true)
	private Boolean notificationsbyoperation;

}
package com.bbva.pzic.cards.dao.model.kb89;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>KTSCKB89</code> de la transacci&oacute;n <code>KB89</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KTSCKB89")
@RooJavaBean
@RooSerializable
public class FormatoKTSCKB89 {
	
	/**
	 * <p>Campo <code>IDSOLIC</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDSOLIC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
	private String idsolic;
	
	/**
	 * <p>Campo <code>IDDESTI</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "IDDESTI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String iddesti;
	
	/**
	 * <p>Campo <code>NOMDEST</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "NOMDEST", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String nomdest;
	
	/**
	 * <p>Campo <code>IDTPAGO</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "IDTPAGO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String idtpago;
	
	/**
	 * <p>Campo <code>NOMPAGO</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "NOMPAGO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nompago;
	
	/**
	 * <p>Campo <code>IDPERIO</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "IDPERIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idperio;
	
	/**
	 * <p>Campo <code>NOMPERI</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "NOMPERI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String nomperi;
	
	/**
	 * <p>Campo <code>DIAPAGO</code>, &iacute;ndice: <code>8</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 8, nombre = "DIAPAGO", tipo = TipoCampo.ENTERO, longitudMinima = 2, longitudMaxima = 2)
	private Integer diapago;
	
	/**
	 * <p>Campo <code>MONLINE</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 9, nombre = "MONLINE", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, decimales = 2)
	private BigDecimal monline;
	
	/**
	 * <p>Campo <code>DIVISAL</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "DIVISAL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divisal;
	
	/**
	 * <p>Campo <code>PRODUCT</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "PRODUCT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String product;
	
	/**
	 * <p>Campo <code>IMPPROD</code>, &iacute;ndice: <code>12</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 12, nombre = "IMPPROD", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, decimales = 2)
	private BigDecimal impprod;
	
	/**
	 * <p>Campo <code>IDBANCO</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "IDBANCO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String idbanco;
	
	/**
	 * <p>Campo <code>IDOFICI</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "IDOFICI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String idofici;
	
	/**
	 * <p>Campo <code>IDPMEMB</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "IDPMEMB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idpmemb;
	
	/**
	 * <p>Campo <code>NOMPMEB</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "NOMPMEB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nompmeb;
	
	/**
	 * <p>Campo <code>NUMPMEM</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "NUMPMEM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String numpmem;
	
	/**
	 * <p>Campo <code>ECENVIO</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 18, nombre = "ECENVIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String ecenvio;
	
	/**
	 * <p>Campo <code>IDCELUL</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 19, nombre = "IDCELUL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String idcelul;
	
	/**
	 * <p>Campo <code>FECOPE</code>, &iacute;ndice: <code>20</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 20, nombre = "FECOPE", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecope;
	
	/**
	 * <p>Campo <code>HOROPE</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 21, nombre = "HOROPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String horope;
	
	/**
	 * <p>Campo <code>INDNOTF</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "INDNOTF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indnotf;
	
}
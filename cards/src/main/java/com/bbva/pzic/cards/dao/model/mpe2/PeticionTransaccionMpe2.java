package com.bbva.pzic.cards.dao.model.mpe2;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPE2</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpe2</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpe2</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPE2.D1180820.txt
 * MPE2LISTADO DE ESTADOS DE CUENTAS      MP        MP2CMPE2PBDMPPO MPME1E2             MPE2  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2018-07-13XP92348 2018-08-2009.42.50XP92348 2018-07-13-15.00.21.676555XP92348 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPME1E2.D1180820.txt
 * MPME1E2 �LISTADO DE ESTADOS DE CUENTA  �F�04�00026�01�00001�NUMTARJ�NUMERO DE TARJETA   �A�016�0�R�        �
 * MPME1E2 �LISTADO DE ESTADOS DE CUENTA  �F�04�00026�02�00017�INDACTI�IND. ESTADO CUENTA  �A�001�0�O�        �
 * MPME1E2 �LISTADO DE ESTADOS DE CUENTA  �F�04�00026�03�00018�IDPAGIN�IND. DE PAGINA      �A�006�0�O�        �
 * MPME1E2 �LISTADO DE ESTADOS DE CUENTA  �F�04�00026�04�00024�TAMPAGI�TAMA#O DE PAGINACION�N�003�0�O�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1E2.D1180820.txt
 * MPMS1E2 �LISTADO DE ESTADO DE CUENTA   �X�03�00037�01�00001�IDDOCTA�IDENT. ESTADO CUENTA�A�026�0�S�        �
 * MPMS1E2 �LISTADO DE ESTADO DE CUENTA   �X�03�00037�02�00027�FECORTE�FECHA CORTE EDO. CTA�A�010�0�S�        �
 * MPMS1E2 �LISTADO DE ESTADO DE CUENTA   �X�03�00037�03�00037�INDACTI�IND. ACTIVO/INACTIVO�A�001�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS2E2.D1180820.txt
 * MPMS2E2 �FORMATO DE PAGINACION         �X�02�00009�01�00001�IDPAGIN�ID. DE PAGINACION   �A�006�0�S�        �
 * MPMS2E2 �FORMATO DE PAGINACION         �X�02�00009�02�00007�TAMPAGI�TAMA#O PAGINACION   �N�003�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPE2.D1180820.txt
 * MPE2MPMS1E2 MPWCS1E2MP2CMPE21S                             XP92348 2018-07-13-16.32.50.724698XP92348 2018-07-13-16.32.50.724878
 * MPE2MPMS2E2 MPWCS1E2MP2CMPE21S                             XP92348 2018-07-13-17.11.53.606672XP92348 2018-07-13-17.11.53.607150
</pre></code>
 *
 * @see RespuestaTransaccionMpe2
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPE2",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpe2.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPME1E2.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpe2 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
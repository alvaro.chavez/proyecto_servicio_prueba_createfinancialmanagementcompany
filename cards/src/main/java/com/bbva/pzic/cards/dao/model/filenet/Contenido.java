package com.bbva.pzic.cards.dao.model.filenet;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.pzic.cards.business.dto.ValidationGroup;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 15/06/2018.
 *
 * @author Entelgy
 */
public class Contenido {

    @DatoAuditable
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    private String formatsPan;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("product.name")
    private String productName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("branch.name")
    private String branchName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("participant1.relationship.type.name")
    private String participant1RelationshipTypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("participant1.relationship.order")
    private String participant1RelationshipOrder;
    @DatoAuditable
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("participant1.name")
    private String participant1Name;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("participant2.relationship.type.name")
    private String participant2RelationshipTypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("participant2.relationship.order")
    private String participant2RelationshipOrder;
    @DatoAuditable
    @JsonProperty("participant2.name")
    private String participant2Name;
    @DatoAuditable
    @JsonProperty("chargedAccount1.id")
    private String chargedAccount1Id;
    @JsonProperty("chargedAccount1.currency")
    private String chargedAccount1Currency;
    @DatoAuditable
    @JsonProperty("chargedAccount2.id")
    private String chargedAccount2Id;
    @JsonProperty("chargedAccount2.currency")
    private String chargedAccount2Currency;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    private String participantType;
    @DatoAuditable
    @JsonProperty("program.id")
    private String programId;
    @JsonProperty("program.type.id")
    private String programTypeId;
    @JsonProperty("program.type.name")
    private String programTypeName;
    @JsonProperty("program.currentLoyaltyUnitsBalance")
    private String programCurrentLoyaltyUnitsBalance;
    @JsonProperty("program.lifetimeAccumulatedLoyaltyUnits")
    private String programLifetimeAccumulatedLoyaltyUnits;
    @JsonProperty("program.bonus")
    private String programBonus;
    @DatoAuditable
    @JsonProperty("card.id")
    private String cardId;
    @JsonProperty("currency.id")
    private String currencyId;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("endDate")
    private String endDate;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("payType.name")
    private String payTypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("participant1.addresess.isMainAddress")
    private String participant1AddresessIsMainAddress;
    @DatoAuditable
    @JsonProperty("participant1.addresess.name")
    private String participant1AddresessName;
    @DatoAuditable
    @JsonProperty("participant1.addresess.streetType.name")
    private String participant1AddresessStreetTypeName;

    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("participant1.addresess.state")
    private String participant1AddresessState;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("participant1.addresess.ubigeo")
    private String participant1AddresessUbigeo;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("creditLimit.value.amount")
    private String creditLimitValueAmount;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("creditLimit.value.currency")
    private String creditLimitValueCurrency;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("limits.disposedBalance.amount")
    private String limitsDisposedBalanceAmount;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("limits.disposedBalance.currency")
    private String limitsDisposedBalanceCurrency;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("limits.availableBalance.amount")
    private String limitsAvailableBalanceAmount;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("limits.availableBalance.currency")
    private String limitsAvailableBalanceCurrency;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("paymentDate")
    private String paymentDate;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates1.type.id")
    private String interestRates1TypeId;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates1.type.name")
    private String interestRates1TypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates1.percentage")
    private String interestRates1Percentage;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates2.type.id")
    private String interestRates2TypeId;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates2.type.name")
    private String interestRates2TypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates2.percentage")
    private String interestRates2Percentage;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates3.type.id")
    private String interestRates3TypeId;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates3.type.name")
    private String interestRates3TypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates3.percentage")
    private String interestRates3Percentage;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates4.type.id")
    private String interestRates4TypeId;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates4.type.name")
    private String interestRates4TypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates4.percentage")
    private String interestRates4Percentage;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates5.type.id")
    private String interestRates5TypeId;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates5.type.name")
    private String interestRates5TypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates5.percentage")
    private String interestRates5Percentage;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates6.type.id")
    private String interestRates6TypeId;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates6.type.name")
    private String interestRates6TypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates6.percentage")
    private String interestRates6Percentage;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates7.type.id")
    private String interestRates7TypeId;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates7.type.name")
    private String interestRates7TypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates7.percentage")
    private String interestRates7Percentage;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates8.type.id")
    private String interestRates8TypeId;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates8.type.name")
    private String interestRates8TypeName;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("interestRates8.percentage")
    private String interestRates8Percentage;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("disposedBalanceLocalCurrency.amount")
    private String disposedBalanceLocalCurrencyAmount;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("disposedBalanceLocalCurrency.currency")
    private String disposedBalanceLocalCurrencyCurrency;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("disposedBalance.amount")
    private String disposedBalanceAmount;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("disposedBalance.currency")
    private String disposedBalanceCurrency;
    @NotNull(groups = ValidationGroup.GetCardFinancialStatementDocument.class)
    @JsonProperty("totalTransactions")
    private String totalTransactions;
    @JsonProperty("limits.financingDisposedBalance.amount")
    private String limitsFinancingDisposedBalanceAmount;
    @JsonProperty("limits.financingDisposedBalance.currency")
    private String limitsFinancingDisposedBalanceCurrency;
    @JsonProperty("outstandingBalanceLocalCurrency.amount")
    private String outstandingBalanceLocalCurrencyAmount;
    @JsonProperty("outstandingBalanceLocalCurrency.currency")
    private String outstandingBalanceLocalCurrencyCurrency;
    @JsonProperty("minimunPayment.minimumCapitalLocalCurrency.amount")
    private String minimunPaymentMinimumCapitalLocalCurrencyAmount;
    @JsonProperty("minimunPayment.minimumCapitalLocalCurrency.currency")
    private String minimunPaymentMinimumCapitalLocalCurrencyCurrency;
    @JsonProperty("interestsBalanceLocalCurrency.amount")
    private String interestsBalanceLocalCurrencyAmount;
    @JsonProperty("interestsBalanceLocalCurrency.currency")
    private String interestsBalanceLocalCurrencyCurrency;
    @JsonProperty("feesBalanceLocalCurrency.amount")
    private String feesBalanceLocalCurrencyAmount;
    @JsonProperty("feesBalanceLocalCurrency.currency")
    private String feesBalanceLocalCurrencyCurrency;
    @JsonProperty("financingTransactionLocalBalance.amount")
    private String financingTransactionLocalBalanceAmount;

    private String financingTransactionLocalBalanceCurrency;

    private String minimumPaymentInvoiceMinimumAmountLocalCurrencyAmount;

    private String minimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency;

    private String invoiceAmountLocalCurrencyAmount;

    private String invoiceAmountLocalCurrencyCurrency;

    private String outstandingBalanceAmount;

    private String outstandingBalanceCurrency;

    private String minimunPaymentMinimumCapitalCurrencyAmount;

    private String minimunPaymentMinimumCapitalCurrencyCurrency;

    private String interestsBalanceAmount;

    private String interestsBalanceCurrency;

    private String feesBalanceAmount;

    private String feesBalanceCurrency;

    private String financingTransactionBalanceAmount;

    private String financingTransactionBalanceCurrency;

    private String minimumPaymentInvoiceMinimumAmountAmount;

    private String minimumPaymentInvoiceMinimumAmountCurrency;

    private String invoiceAmountAmount;

    private String invoiceAmountCurrency;

    private String totalDebtLocalCurrencyAmount;

    private String totalDebtLocalCurrencyCurrency;

    private String totalDebtAmount;

    private String totalDebtCurrency;
    private String totalMonthsAmortizationMinimum;
    private String totalMonthsAmortizationMinimumFull;
    private List<String> rowCardStatement;
    private String message;

    @JsonProperty("formats.pan")
    public String getFormatsPan() {
        return formatsPan;
    }

    public void setFormatsPan(String formatsPan) {
        this.formatsPan = formatsPan;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getParticipant1RelationshipTypeName() {
        return participant1RelationshipTypeName;
    }

    public void setParticipant1RelationshipTypeName(String participant1RelationshipTypeName) {
        this.participant1RelationshipTypeName = participant1RelationshipTypeName;
    }

    public String getParticipant1RelationshipOrder() {
        return participant1RelationshipOrder;
    }

    public void setParticipant1RelationshipOrder(String participant1RelationshipOrder) {
        this.participant1RelationshipOrder = participant1RelationshipOrder;
    }

    public String getParticipant1Name() {
        return participant1Name;
    }

    public void setParticipant1Name(String participant1Name) {
        this.participant1Name = participant1Name;
    }

    public String getParticipant2RelationshipTypeName() {
        return participant2RelationshipTypeName;
    }

    public void setParticipant2RelationshipTypeName(String participant2RelationshipTypeName) {
        this.participant2RelationshipTypeName = participant2RelationshipTypeName;
    }

    public String getParticipant2RelationshipOrder() {
        return participant2RelationshipOrder;
    }

    public void setParticipant2RelationshipOrder(String participant2RelationshipOrder) {
        this.participant2RelationshipOrder = participant2RelationshipOrder;
    }

    public String getParticipant2Name() {
        return participant2Name;
    }

    public void setParticipant2Name(String participant2Name) {
        this.participant2Name = participant2Name;
    }

    public String getChargedAccount1Id() {
        return chargedAccount1Id;
    }

    public void setChargedAccount1Id(String chargedAccount1Id) {
        this.chargedAccount1Id = chargedAccount1Id;
    }

    public String getChargedAccount1Currency() {
        return chargedAccount1Currency;
    }

    public void setChargedAccount1Currency(String chargedAccount1Currency) {
        this.chargedAccount1Currency = chargedAccount1Currency;
    }

    public String getChargedAccount2Id() {
        return chargedAccount2Id;
    }

    public void setChargedAccount2Id(String chargedAccount2Id) {
        this.chargedAccount2Id = chargedAccount2Id;
    }

    public String getChargedAccount2Currency() {
        return chargedAccount2Currency;
    }

    public void setChargedAccount2Currency(String chargedAccount2Currency) {
        this.chargedAccount2Currency = chargedAccount2Currency;
    }

    public String getParticipantType() {
        return participantType;
    }

    public void setParticipantType(String participantType) {
        this.participantType = participantType;
    }

    public String getProgramId() {
        return programId;
    }

    public void setProgramId(String programId) {
        this.programId = programId;
    }

    public String getProgramTypeId() {
        return programTypeId;
    }

    public void setProgramTypeId(String programTypeId) {
        this.programTypeId = programTypeId;
    }

    public String getProgramTypeName() {
        return programTypeName;
    }

    public void setProgramTypeName(String programTypeName) {
        this.programTypeName = programTypeName;
    }

    public String getProgramCurrentLoyaltyUnitsBalance() {
        return programCurrentLoyaltyUnitsBalance;
    }

    public void setProgramCurrentLoyaltyUnitsBalance(String programCurrentLoyaltyUnitsBalance) {
        this.programCurrentLoyaltyUnitsBalance = programCurrentLoyaltyUnitsBalance;
    }

    public String getProgramLifetimeAccumulatedLoyaltyUnits() {
        return programLifetimeAccumulatedLoyaltyUnits;
    }

    public void setProgramLifetimeAccumulatedLoyaltyUnits(String programLifetimeAccumulatedLoyaltyUnits) {
        this.programLifetimeAccumulatedLoyaltyUnits = programLifetimeAccumulatedLoyaltyUnits;
    }

    public String getProgramBonus() {
        return programBonus;
    }

    public void setProgramBonus(String programBonus) {
        this.programBonus = programBonus;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPayTypeName() {
        return payTypeName;
    }

    public void setPayTypeName(String payTypeName) {
        this.payTypeName = payTypeName;
    }

    public String getParticipant1AddresessIsMainAddress() {
        return participant1AddresessIsMainAddress;
    }

    public void setParticipant1AddresessIsMainAddress(String participant1AddresessIsMainAddress) {
        this.participant1AddresessIsMainAddress = participant1AddresessIsMainAddress;
    }

    public String getParticipant1AddresessName() {
        return participant1AddresessName;
    }

    public void setParticipant1AddresessName(String participant1AddresessName) {
        this.participant1AddresessName = participant1AddresessName;
    }

    public String getParticipant1AddresessStreetTypeName() {
        return participant1AddresessStreetTypeName;
    }

    public void setParticipant1AddresessStreetTypeName(String participant1AddresessStreetTypeName) {
        this.participant1AddresessStreetTypeName = participant1AddresessStreetTypeName;
    }

    public String getParticipant1AddresessState() {
        return participant1AddresessState;
    }

    public void setParticipant1AddresessState(String participant1AddresessState) {
        this.participant1AddresessState = participant1AddresessState;
    }

    public String getParticipant1AddresessUbigeo() {
        return participant1AddresessUbigeo;
    }

    public void setParticipant1AddresessUbigeo(String participant1AddresessUbigeo) {
        this.participant1AddresessUbigeo = participant1AddresessUbigeo;
    }

    public String getCreditLimitValueAmount() {
        return creditLimitValueAmount;
    }

    public void setCreditLimitValueAmount(String creditLimitValueAmount) {
        this.creditLimitValueAmount = creditLimitValueAmount;
    }

    public String getCreditLimitValueCurrency() {
        return creditLimitValueCurrency;
    }

    public void setCreditLimitValueCurrency(String creditLimitValueCurrency) {
        this.creditLimitValueCurrency = creditLimitValueCurrency;
    }

    public String getLimitsDisposedBalanceAmount() {
        return limitsDisposedBalanceAmount;
    }

    public void setLimitsDisposedBalanceAmount(String limitsDisposedBalanceAmount) {
        this.limitsDisposedBalanceAmount = limitsDisposedBalanceAmount;
    }

    public String getLimitsDisposedBalanceCurrency() {
        return limitsDisposedBalanceCurrency;
    }

    public void setLimitsDisposedBalanceCurrency(String limitsDisposedBalanceCurrency) {
        this.limitsDisposedBalanceCurrency = limitsDisposedBalanceCurrency;
    }

    public String getLimitsAvailableBalanceAmount() {
        return limitsAvailableBalanceAmount;
    }

    public void setLimitsAvailableBalanceAmount(String limitsAvailableBalanceAmount) {
        this.limitsAvailableBalanceAmount = limitsAvailableBalanceAmount;
    }

    public String getLimitsAvailableBalanceCurrency() {
        return limitsAvailableBalanceCurrency;
    }

    public void setLimitsAvailableBalanceCurrency(String limitsAvailableBalanceCurrency) {
        this.limitsAvailableBalanceCurrency = limitsAvailableBalanceCurrency;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getInterestRates1TypeId() {
        return interestRates1TypeId;
    }

    public void setInterestRates1TypeId(String interestRates1TypeId) {
        this.interestRates1TypeId = interestRates1TypeId;
    }

    public String getInterestRates1TypeName() {
        return interestRates1TypeName;
    }

    public void setInterestRates1TypeName(String interestRates1TypeName) {
        this.interestRates1TypeName = interestRates1TypeName;
    }

    public String getInterestRates1Percentage() {
        return interestRates1Percentage;
    }

    public void setInterestRates1Percentage(String interestRates1Percentage) {
        this.interestRates1Percentage = interestRates1Percentage;
    }

    public String getInterestRates2TypeId() {
        return interestRates2TypeId;
    }

    public void setInterestRates2TypeId(String interestRates2TypeId) {
        this.interestRates2TypeId = interestRates2TypeId;
    }

    public String getInterestRates2TypeName() {
        return interestRates2TypeName;
    }

    public void setInterestRates2TypeName(String interestRates2TypeName) {
        this.interestRates2TypeName = interestRates2TypeName;
    }

    public String getInterestRates2Percentage() {
        return interestRates2Percentage;
    }

    public void setInterestRates2Percentage(String interestRates2Percentage) {
        this.interestRates2Percentage = interestRates2Percentage;
    }

    public String getInterestRates3TypeId() {
        return interestRates3TypeId;
    }

    public void setInterestRates3TypeId(String interestRates3TypeId) {
        this.interestRates3TypeId = interestRates3TypeId;
    }

    public String getInterestRates3TypeName() {
        return interestRates3TypeName;
    }

    public void setInterestRates3TypeName(String interestRates3TypeName) {
        this.interestRates3TypeName = interestRates3TypeName;
    }

    public String getInterestRates3Percentage() {
        return interestRates3Percentage;
    }

    public void setInterestRates3Percentage(String interestRates3Percentage) {
        this.interestRates3Percentage = interestRates3Percentage;
    }

    public String getInterestRates4TypeId() {
        return interestRates4TypeId;
    }

    public void setInterestRates4TypeId(String interestRates4TypeId) {
        this.interestRates4TypeId = interestRates4TypeId;
    }

    public String getInterestRates4TypeName() {
        return interestRates4TypeName;
    }

    public void setInterestRates4TypeName(String interestRates4TypeName) {
        this.interestRates4TypeName = interestRates4TypeName;
    }

    public String getInterestRates4Percentage() {
        return interestRates4Percentage;
    }

    public void setInterestRates4Percentage(String interestRates4Percentage) {
        this.interestRates4Percentage = interestRates4Percentage;
    }

    public String getInterestRates5TypeId() {
        return interestRates5TypeId;
    }

    public void setInterestRates5TypeId(String interestRates5TypeId) {
        this.interestRates5TypeId = interestRates5TypeId;
    }

    public String getInterestRates5TypeName() {
        return interestRates5TypeName;
    }

    public void setInterestRates5TypeName(String interestRates5TypeName) {
        this.interestRates5TypeName = interestRates5TypeName;
    }

    public String getInterestRates5Percentage() {
        return interestRates5Percentage;
    }

    public void setInterestRates5Percentage(String interestRates5Percentage) {
        this.interestRates5Percentage = interestRates5Percentage;
    }

    public String getInterestRates6TypeId() {
        return interestRates6TypeId;
    }

    public void setInterestRates6TypeId(String interestRates6TypeId) {
        this.interestRates6TypeId = interestRates6TypeId;
    }

    public String getInterestRates6TypeName() {
        return interestRates6TypeName;
    }

    public void setInterestRates6TypeName(String interestRates6TypeName) {
        this.interestRates6TypeName = interestRates6TypeName;
    }

    public String getInterestRates6Percentage() {
        return interestRates6Percentage;
    }

    public void setInterestRates6Percentage(String interestRates6Percentage) {
        this.interestRates6Percentage = interestRates6Percentage;
    }

    public String getInterestRates7TypeId() {
        return interestRates7TypeId;
    }

    public void setInterestRates7TypeId(String interestRates7TypeId) {
        this.interestRates7TypeId = interestRates7TypeId;
    }

    public String getInterestRates7TypeName() {
        return interestRates7TypeName;
    }

    public void setInterestRates7TypeName(String interestRates7TypeName) {
        this.interestRates7TypeName = interestRates7TypeName;
    }

    public String getInterestRates7Percentage() {
        return interestRates7Percentage;
    }

    public void setInterestRates7Percentage(String interestRates7Percentage) {
        this.interestRates7Percentage = interestRates7Percentage;
    }

    public String getInterestRates8TypeId() {
        return interestRates8TypeId;
    }

    public void setInterestRates8TypeId(String interestRates8TypeId) {
        this.interestRates8TypeId = interestRates8TypeId;
    }

    public String getInterestRates8TypeName() {
        return interestRates8TypeName;
    }

    public void setInterestRates8TypeName(String interestRates8TypeName) {
        this.interestRates8TypeName = interestRates8TypeName;
    }

    public String getInterestRates8Percentage() {
        return interestRates8Percentage;
    }

    public void setInterestRates8Percentage(String interestRates8Percentage) {
        this.interestRates8Percentage = interestRates8Percentage;
    }

    public String getDisposedBalanceLocalCurrencyAmount() {
        return disposedBalanceLocalCurrencyAmount;
    }

    public void setDisposedBalanceLocalCurrencyAmount(String disposedBalanceLocalCurrencyAmount) {
        this.disposedBalanceLocalCurrencyAmount = disposedBalanceLocalCurrencyAmount;
    }

    public String getDisposedBalanceLocalCurrencyCurrency() {
        return disposedBalanceLocalCurrencyCurrency;
    }

    public void setDisposedBalanceLocalCurrencyCurrency(String disposedBalanceLocalCurrencyCurrency) {
        this.disposedBalanceLocalCurrencyCurrency = disposedBalanceLocalCurrencyCurrency;
    }

    public String getDisposedBalanceAmount() {
        return disposedBalanceAmount;
    }

    public void setDisposedBalanceAmount(String disposedBalanceAmount) {
        this.disposedBalanceAmount = disposedBalanceAmount;
    }

    public String getDisposedBalanceCurrency() {
        return disposedBalanceCurrency;
    }

    public void setDisposedBalanceCurrency(String disposedBalanceCurrency) {
        this.disposedBalanceCurrency = disposedBalanceCurrency;
    }

    public String getTotalTransactions() {
        return totalTransactions;
    }

    public void setTotalTransactions(String totalTransactions) {
        this.totalTransactions = totalTransactions;
    }

    public String getLimitsFinancingDisposedBalanceAmount() {
        return limitsFinancingDisposedBalanceAmount;
    }

    public void setLimitsFinancingDisposedBalanceAmount(String limitsFinancingDisposedBalanceAmount) {
        this.limitsFinancingDisposedBalanceAmount = limitsFinancingDisposedBalanceAmount;
    }

    public String getLimitsFinancingDisposedBalanceCurrency() {
        return limitsFinancingDisposedBalanceCurrency;
    }

    public void setLimitsFinancingDisposedBalanceCurrency(String limitsFinancingDisposedBalanceCurrency) {
        this.limitsFinancingDisposedBalanceCurrency = limitsFinancingDisposedBalanceCurrency;
    }

    public String getOutstandingBalanceLocalCurrencyAmount() {
        return outstandingBalanceLocalCurrencyAmount;
    }

    public void setOutstandingBalanceLocalCurrencyAmount(String outstandingBalanceLocalCurrencyAmount) {
        this.outstandingBalanceLocalCurrencyAmount = outstandingBalanceLocalCurrencyAmount;
    }

    public String getOutstandingBalanceLocalCurrencyCurrency() {
        return outstandingBalanceLocalCurrencyCurrency;
    }

    public void setOutstandingBalanceLocalCurrencyCurrency(String outstandingBalanceLocalCurrencyCurrency) {
        this.outstandingBalanceLocalCurrencyCurrency = outstandingBalanceLocalCurrencyCurrency;
    }

    public String getMinimunPaymentMinimumCapitalLocalCurrencyAmount() {
        return minimunPaymentMinimumCapitalLocalCurrencyAmount;
    }

    public void setMinimunPaymentMinimumCapitalLocalCurrencyAmount(String minimunPaymentMinimumCapitalLocalCurrencyAmount) {
        this.minimunPaymentMinimumCapitalLocalCurrencyAmount = minimunPaymentMinimumCapitalLocalCurrencyAmount;
    }

    public String getMinimunPaymentMinimumCapitalLocalCurrencyCurrency() {
        return minimunPaymentMinimumCapitalLocalCurrencyCurrency;
    }

    public void setMinimunPaymentMinimumCapitalLocalCurrencyCurrency(String minimunPaymentMinimumCapitalLocalCurrencyCurrency) {
        this.minimunPaymentMinimumCapitalLocalCurrencyCurrency = minimunPaymentMinimumCapitalLocalCurrencyCurrency;
    }

    public String getInterestsBalanceLocalCurrencyAmount() {
        return interestsBalanceLocalCurrencyAmount;
    }

    public void setInterestsBalanceLocalCurrencyAmount(String interestsBalanceLocalCurrencyAmount) {
        this.interestsBalanceLocalCurrencyAmount = interestsBalanceLocalCurrencyAmount;
    }

    public String getInterestsBalanceLocalCurrencyCurrency() {
        return interestsBalanceLocalCurrencyCurrency;
    }

    public void setInterestsBalanceLocalCurrencyCurrency(String interestsBalanceLocalCurrencyCurrency) {
        this.interestsBalanceLocalCurrencyCurrency = interestsBalanceLocalCurrencyCurrency;
    }

    public String getFeesBalanceLocalCurrencyAmount() {
        return feesBalanceLocalCurrencyAmount;
    }

    public void setFeesBalanceLocalCurrencyAmount(String feesBalanceLocalCurrencyAmount) {
        this.feesBalanceLocalCurrencyAmount = feesBalanceLocalCurrencyAmount;
    }

    public String getFeesBalanceLocalCurrencyCurrency() {
        return feesBalanceLocalCurrencyCurrency;
    }

    public void setFeesBalanceLocalCurrencyCurrency(String feesBalanceLocalCurrencyCurrency) {
        this.feesBalanceLocalCurrencyCurrency = feesBalanceLocalCurrencyCurrency;
    }

    public String getFinancingTransactionLocalBalanceAmount() {
        return financingTransactionLocalBalanceAmount;
    }

    public void setFinancingTransactionLocalBalanceAmount(String financingTransactionLocalBalanceAmount) {
        this.financingTransactionLocalBalanceAmount = financingTransactionLocalBalanceAmount;
    }

    @JsonProperty("financingTransactionLocalBalance.currency")
    public String getFinancingTransactionLocalBalanceCurrency() {
        return financingTransactionLocalBalanceCurrency;
    }

    public void setFinancingTransactionLocalBalanceCurrency(String financingTransactionLocalBalanceCurrency) {
        this.financingTransactionLocalBalanceCurrency = financingTransactionLocalBalanceCurrency;
    }

    @JsonProperty("minimumPayment.invoiceMinimumAmountLocalCurrency.amount")
    public String getMinimumPaymentInvoiceMinimumAmountLocalCurrencyAmount() {
        return minimumPaymentInvoiceMinimumAmountLocalCurrencyAmount;
    }

    public void setMinimumPaymentInvoiceMinimumAmountLocalCurrencyAmount(String minimumPaymentInvoiceMinimumAmountLocalCurrencyAmount) {
        this.minimumPaymentInvoiceMinimumAmountLocalCurrencyAmount = minimumPaymentInvoiceMinimumAmountLocalCurrencyAmount;
    }

    @JsonProperty("minimumPayment.invoiceMinimumAmountLocalCurrency.currency")
    public String getMinimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency() {
        return minimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency;
    }

    public void setMinimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency(String minimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency) {
        this.minimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency = minimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency;
    }

    @JsonProperty("invoiceAmountLocalCurrency.amount")
    public String getInvoiceAmountLocalCurrencyAmount() {
        return invoiceAmountLocalCurrencyAmount;
    }

    public void setInvoiceAmountLocalCurrencyAmount(String invoiceAmountLocalCurrencyAmount) {
        this.invoiceAmountLocalCurrencyAmount = invoiceAmountLocalCurrencyAmount;
    }

    @JsonProperty("invoiceAmountLocalCurrency.currency")
    public String getInvoiceAmountLocalCurrencyCurrency() {
        return invoiceAmountLocalCurrencyCurrency;
    }

    public void setInvoiceAmountLocalCurrencyCurrency(String invoiceAmountLocalCurrencyCurrency) {
        this.invoiceAmountLocalCurrencyCurrency = invoiceAmountLocalCurrencyCurrency;
    }

    @JsonProperty("outstandingBalance.amount")
    public String getOutstandingBalanceAmount() {
        return outstandingBalanceAmount;
    }

    public void setOutstandingBalanceAmount(String outstandingBalanceAmount) {
        this.outstandingBalanceAmount = outstandingBalanceAmount;
    }

    @JsonProperty("outstandingBalance.currency")
    public String getOutstandingBalanceCurrency() {
        return outstandingBalanceCurrency;
    }

    public void setOutstandingBalanceCurrency(String outstandingBalanceCurrency) {
        this.outstandingBalanceCurrency = outstandingBalanceCurrency;
    }

    @JsonProperty("minimunPayment.minimumCapitalCurrency.amount")
    public String getMinimunPaymentMinimumCapitalCurrencyAmount() {
        return minimunPaymentMinimumCapitalCurrencyAmount;
    }

    public void setMinimunPaymentMinimumCapitalCurrencyAmount(String minimunPaymentMinimumCapitalCurrencyAmount) {
        this.minimunPaymentMinimumCapitalCurrencyAmount = minimunPaymentMinimumCapitalCurrencyAmount;
    }

    @JsonProperty("minimunPayment.minimumCapitalCurrency.currency")
    public String getMinimunPaymentMinimumCapitalCurrencyCurrency() {
        return minimunPaymentMinimumCapitalCurrencyCurrency;
    }

    public void setMinimunPaymentMinimumCapitalCurrencyCurrency(String minimunPaymentMinimumCapitalCurrencyCurrency) {
        this.minimunPaymentMinimumCapitalCurrencyCurrency = minimunPaymentMinimumCapitalCurrencyCurrency;
    }

    @JsonProperty("interestsBalance.amount")
    public String getInterestsBalanceAmount() {
        return interestsBalanceAmount;
    }

    public void setInterestsBalanceAmount(String interestsBalanceAmount) {
        this.interestsBalanceAmount = interestsBalanceAmount;
    }

    @JsonProperty("interestsBalance.currency")
    public String getInterestsBalanceCurrency() {
        return interestsBalanceCurrency;
    }

    public void setInterestsBalanceCurrency(String interestsBalanceCurrency) {
        this.interestsBalanceCurrency = interestsBalanceCurrency;
    }

    @JsonProperty("feesBalance.amount")
    public String getFeesBalanceAmount() {
        return feesBalanceAmount;
    }

    public void setFeesBalanceAmount(String feesBalanceAmount) {
        this.feesBalanceAmount = feesBalanceAmount;
    }

    @JsonProperty("feesBalance.currency")
    public String getFeesBalanceCurrency() {
        return feesBalanceCurrency;
    }

    public void setFeesBalanceCurrency(String feesBalanceCurrency) {
        this.feesBalanceCurrency = feesBalanceCurrency;
    }

    @JsonProperty("financingTransactionBalance.amount")
    public String getFinancingTransactionBalanceAmount() {
        return financingTransactionBalanceAmount;
    }

    public void setFinancingTransactionBalanceAmount(String financingTransactionBalanceAmount) {
        this.financingTransactionBalanceAmount = financingTransactionBalanceAmount;
    }

    @JsonProperty("financingTransactionBalance.currency")
    public String getFinancingTransactionBalanceCurrency() {
        return financingTransactionBalanceCurrency;
    }

    public void setFinancingTransactionBalanceCurrency(String financingTransactionBalanceCurrency) {
        this.financingTransactionBalanceCurrency = financingTransactionBalanceCurrency;
    }

    @JsonProperty("minimumPayment.invoiceMinimumAmount.amount")
    public String getMinimumPaymentInvoiceMinimumAmountAmount() {
        return minimumPaymentInvoiceMinimumAmountAmount;
    }

    public void setMinimumPaymentInvoiceMinimumAmountAmount(String minimumPaymentInvoiceMinimumAmountAmount) {
        this.minimumPaymentInvoiceMinimumAmountAmount = minimumPaymentInvoiceMinimumAmountAmount;
    }

    @JsonProperty("minimumPayment.invoiceMinimumAmount.currency")
    public String getMinimumPaymentInvoiceMinimumAmountCurrency() {
        return minimumPaymentInvoiceMinimumAmountCurrency;
    }

    public void setMinimumPaymentInvoiceMinimumAmountCurrency(String minimumPaymentInvoiceMinimumAmountCurrency) {
        this.minimumPaymentInvoiceMinimumAmountCurrency = minimumPaymentInvoiceMinimumAmountCurrency;
    }

    @JsonProperty("invoiceAmount.amount")
    public String getInvoiceAmountAmount() {
        return invoiceAmountAmount;
    }

    public void setInvoiceAmountAmount(String invoiceAmountAmount) {
        this.invoiceAmountAmount = invoiceAmountAmount;
    }

    @JsonProperty("invoiceAmount.currency")
    public String getInvoiceAmountCurrency() {
        return invoiceAmountCurrency;
    }

    public void setInvoiceAmountCurrency(String invoiceAmountCurrency) {
        this.invoiceAmountCurrency = invoiceAmountCurrency;
    }

    @JsonProperty("totalDebtLocalCurrency.amount")
    public String getTotalDebtLocalCurrencyAmount() {
        return totalDebtLocalCurrencyAmount;
    }

    public void setTotalDebtLocalCurrencyAmount(String totalDebtLocalCurrencyAmount) {
        this.totalDebtLocalCurrencyAmount = totalDebtLocalCurrencyAmount;
    }

    @JsonProperty("totalDebtLocalCurrency.currency")
    public String getTotalDebtLocalCurrencyCurrency() {
        return totalDebtLocalCurrencyCurrency;
    }

    public void setTotalDebtLocalCurrencyCurrency(String totalDebtLocalCurrencyCurrency) {
        this.totalDebtLocalCurrencyCurrency = totalDebtLocalCurrencyCurrency;
    }

    @JsonProperty("totalDebt.amount")
    public String getTotalDebtAmount() {
        return totalDebtAmount;
    }

    public void setTotalDebtAmount(String totalDebtAmount) {
        this.totalDebtAmount = totalDebtAmount;
    }

    @JsonProperty("totalDebt.currency")
    public String getTotalDebtCurrency() {
        return totalDebtCurrency;
    }

    public void setTotalDebtCurrency(String totalDebtCurrency) {
        this.totalDebtCurrency = totalDebtCurrency;
    }

    public String getTotalMonthsAmortizationMinimum() {
        return totalMonthsAmortizationMinimum;
    }

    public void setTotalMonthsAmortizationMinimum(String totalMonthsAmortizationMinimum) {
        this.totalMonthsAmortizationMinimum = totalMonthsAmortizationMinimum;
    }

    public String getTotalMonthsAmortizationMinimumFull() {
        return totalMonthsAmortizationMinimumFull;
    }

    public void setTotalMonthsAmortizationMinimumFull(String totalMonthsAmortizationMinimumFull) {
        this.totalMonthsAmortizationMinimumFull = totalMonthsAmortizationMinimumFull;
    }

    public List<String> getRowCardStatement() {
        return rowCardStatement;
    }

    public void setRowCardStatement(List<String> rowCardStatement) {
        this.rowCardStatement = rowCardStatement;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

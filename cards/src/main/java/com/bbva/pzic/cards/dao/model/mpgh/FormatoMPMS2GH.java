package com.bbva.pzic.cards.dao.model.mpgh;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS2GH</code> de la transacci&oacute;n <code>MPGH</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS2GH")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS2GH {

	/**
	 * <p>Campo <code>LINESCU</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "LINESCU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 132, longitudMaxima = 132, limpiar = false)
	private String linescu;

}
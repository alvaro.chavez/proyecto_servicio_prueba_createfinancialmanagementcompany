package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.managers.InputHeaderManager;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Currency;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.mpg1.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardMapper;
import com.bbva.pzic.cards.util.BusinessServiceUtil;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.Converter;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.bbva.pzic.cards.util.Constants.DEFAULT_TIME;

/**
 * Created on 16/10/2017.
 *
 * @author Entelgy
 */
@Mapper("txGetCardMapper")
public class TxGetCardMapper implements ITxGetCardMapper {

    private static final Log LOG = LogFactory.getLog(TxGetCardMapper.class);

    private final Translator translator;
    private final InputHeaderManager inputHeaderManager;

    public TxGetCardMapper(Translator translator,
                           InputHeaderManager inputHeaderManager) {
        this.translator = translator;
        this.inputHeaderManager = inputHeaderManager;
    }

    @Override
    public FormatoMPMENG1 mapIn(DTOIntCard dtoIn) {
        LOG.info("... called method TxGetCardMapper.mapIn ...");
        FormatoMPMENG1 formatoMPMENG1 = new FormatoMPMENG1();
        formatoMPMENG1.setIdetarj(dtoIn.getCardId());
        return formatoMPMENG1;
    }

    private void mapTitle(final FormatoMPMS1G1 formatoMPMS1G1, final Card dtoOut) {
        Title title = new Title();
        title.setId(formatoMPMS1G1.getTipprod());
        title.setName(formatoMPMS1G1.getDescpro());
        dtoOut.setTitle(title);
    }

    private void mapBranch(final FormatoMPMS1G1 formatoMPMS1G1, final BankType bankType) {
        if (formatoMPMS1G1.getNuofges() != null || formatoMPMS1G1.getDeofges() != null) {
            Branch branch = new Branch();
            branch.setId(formatoMPMS1G1.getNuofges());
            branch.setName(formatoMPMS1G1.getDeofges());
            bankType.setBranch(branch);
        }
    }

    private void mapBank(final FormatoMPMS1G1 formatoMPMS1G1, final Card dtoOut) {
        if (formatoMPMS1G1.getCodofba() != null || formatoMPMS1G1.getDeofban() != null) {
            BankType bankType = new BankType();
            bankType.setId(formatoMPMS1G1.getCodofba());
            bankType.setName(formatoMPMS1G1.getDeofban());
            mapBranch(formatoMPMS1G1, bankType);
            dtoOut.setBank(bankType);
        }
    }

    private void mapMembership(final FormatoMPMS1G1 formatoMPMS1G1, final Card dtoOut) {
        if (formatoMPMS1G1.getIdbenef() != null || formatoMPMS1G1.getDesbenf() != null) {
            Membership membership = new Membership();
            membership.setId(formatoMPMS1G1.getIdbenef());
            membership.setDescription(formatoMPMS1G1.getDesbenf());
            dtoOut.setMembership(membership);
        }
    }

    @Override
    public Card mapOut1(final FormatoMPMS1G1 formatoMPMS1G1, final Card dtoOut) {
        LOG.info("... called method TxGetCardMapper.mapOut ...");

        dtoOut.setNumber(formatoMPMS1G1.getNumtarj());
        dtoOut.setExpirationDate(formatoMPMS1G1.getFecvenc());
        dtoOut.setHolderName(formatoMPMS1G1.getNomtit());
        dtoOut.setCutOffDate(formatoMPMS1G1.getFecorte());

        mapTitle(formatoMPMS1G1, dtoOut);
        mapBank(formatoMPMS1G1, dtoOut);
        mapMembership(formatoMPMS1G1, dtoOut);

        dtoOut.setNumberType(mapOutNumberType(formatoMPMS1G1.getIdtnuco(), formatoMPMS1G1.getDetnuco()));
        dtoOut.setCardType(mapOutCardType(formatoMPMS1G1.getTiptarj(), formatoMPMS1G1.getDtitarj()));

        dtoOut.setCardId(formatoMPMS1G1.getIdetarj());

        List<Currency> currencyList = mapOutCurrency(formatoMPMS1G1.getDdispue(), formatoMPMS1G1.getDdispub());
        dtoOut.setCurrencies(currencyList);

        dtoOut.setGrantedCredits(mapOutGrantedCredits(formatoMPMS1G1.getLimcre(), formatoMPMS1G1.getMontar()));

        dtoOut.setAvailableBalance(mapOutAvailableBalance(formatoMPMS1G1.getSaldisp(), formatoMPMS1G1.getMontar()));

        dtoOut.setDisposedBalance(mapOutDisposedBalance(formatoMPMS1G1.getSalusad(), formatoMPMS1G1.getMonusad()));

        dtoOut.setStatus(mapOutStatus(formatoMPMS1G1.getEstarjo(), formatoMPMS1G1.getDestarj(), formatoMPMS1G1.getFecsitt()));
        mapOutImages(dtoOut, formatoMPMS1G1.getIdimgt(), formatoMPMS1G1.getNimgta(), formatoMPMS1G1.getImgtar1(), false);
        mapOutImages(dtoOut, formatoMPMS1G1.getIdimgt2(), formatoMPMS1G1.getNimgta2(), formatoMPMS1G1.getImgtar2(), true);

        if (dtoOut.getImages().get(0) == null && dtoOut.getImages().get(1) == null) {
            dtoOut.setImages(null);
        }
        dtoOut.setIssueDate(FunctionUtils.buildDatetime(formatoMPMS1G1.getFecent(), DEFAULT_TIME));

        if (CollectionUtils.isEmpty(dtoOut.getCurrencies())) {
            dtoOut.setCurrencies(null);
        }

        dtoOut.setIsBusiness(Converter.convertFrom(formatoMPMS1G1.getIndempr()));
        mapOut1BrandAssociation(dtoOut, formatoMPMS1G1);
        return dtoOut;
    }

    private void mapOut1BrandAssociation(final Card dtoOut, final FormatoMPMS1G1 formatoMPMS1G1) {
        if (formatoMPMS1G1.getIdmatar() != null || formatoMPMS1G1.getNomatar() != null) {
            BrandAssociation brandAssociation = new BrandAssociation();
            brandAssociation.setId(translator.translateBackendEnumValueStrictly("cards.brandAssociation.id", formatoMPMS1G1.getIdmatar()));
            brandAssociation.setName(formatoMPMS1G1.getNomatar());
            dtoOut.setBrandAssociation(brandAssociation);
        }
    }

    private void mapOutImages(final Card dtoOut, final String id, final String name, final String url, final boolean isImageBack) {
        if (dtoOut.getImages() == null) {
            dtoOut.setImages(new ArrayList<>());
        }

        if (id == null && name == null && url == null) {
            dtoOut.getImages().add(null);
            return;
        }

        if (StringUtils.isEmpty(url) && isImageBack) {
            return;
        }

        Image image = new Image();
        image.setId(id);
        image.setName(name);
        image.setUrl(BusinessServiceUtil.buildUrl(inputHeaderManager.getHeader(Constants.HEADER_USER_AGENT), url));

        dtoOut.getImages().add(image);
    }

    private List<GrantedCredit> mapOutGrantedCredits(final BigDecimal amount, final String currency) {
        if (amount == null && currency == null) {
            return null;
        }
        GrantedCredit grantedCredit = new GrantedCredit();
        grantedCredit.setAmount(amount);
        grantedCredit.setCurrency(currency);
        return Collections.singletonList(grantedCredit);
    }

    private List<Currency> mapOutCurrency(String mdispue, String mdispub) {

        List<Currency> currencies = new ArrayList<>();
        Currency currency = mapOutCurrency(mdispue, Boolean.TRUE);
        if (currency != null) {
            currencies.add(currency);
        }

        currency = mapOutCurrency(mdispub, Boolean.FALSE);
        if (currency != null) {
            currencies.add(currency);
        }
        if (currencies.isEmpty()) {
            return null;
        }
        return currencies;
    }

    private Currency mapOutCurrency(String name, Boolean isMajor) {
        if (name == null) {
            return null;
        }
        Currency currency = new Currency();
        currency.setCurrency(name);
        currency.setIsMajor(isMajor);
        return currency;
    }

    private DisposedBalance mapOutDisposedBalance(final BigDecimal amount, final String currency) {
        CurrentBalance currentBalance = mapOutCurrentBalance(amount, currency);

        if (currentBalance == null) {
            return null;
        }

        DisposedBalance disposedBalance = new DisposedBalance();
        disposedBalance.setCurrentBalances(Collections.singletonList(currentBalance));
        return disposedBalance;
    }

    private CurrentBalance mapOutCurrentBalance(final BigDecimal amount, final String currency) {
        if (amount.equals(BigDecimal.ZERO) && currency == null) {
            return null;
        }
        CurrentBalance currentBalance = new CurrentBalance();
        currentBalance.setAmount(amount);
        currentBalance.setCurrency(currency);
        return currentBalance;
    }

    private Status mapOutStatus(String id, String name, Date lastUpdated) {
        if (id == null && name == null) {
            return null;
        }
        Status status = new Status();
        status.setId(translator.translateBackendEnumValueStrictly("cards.status.id", id));
        status.setName(name);
        status.setLastUpdatedDate(lastUpdated);
        return status;
    }

    private NumberType mapOutNumberType(String id, String name) {
        if (id == null && name == null) {
            return null;
        }
        NumberType numberType = new NumberType();
        numberType.setName(name);
        numberType.setId(translator.translateBackendEnumValueStrictly("cards.numberType.id", id));
        return numberType;
    }

    private CardType mapOutCardType(final String id, final String name) {
        if (id == null && name == null) {
            return null;
        }
        CardType cardType = new CardType();
        cardType.setId(translator.translateBackendEnumValueStrictly("cards.cardType.id", id));
        cardType.setName(name);
        return cardType;
    }

    private AvailableBalance mapOutAvailableBalance(final BigDecimal amount, final String currency) {
        CurrentBalance currentBalance = mapOutCurrentBalance(amount, currency);
        if (currentBalance == null) {
            return null;
        }

        AvailableBalance availableBalance = new AvailableBalance();
        availableBalance.setCurrentBalances(Collections.singletonList(currentBalance));
        return availableBalance;
    }

    @Override
    public Card mapOut2(final FormatoMPMS2G1 formatoMPMS2G1, final Card dtoOut) {
        LOG.info("... called method TxGetCardMapper.mapOut2 ...");
        if (dtoOut.getRelatedContracts() == null) {
            dtoOut.setRelatedContracts(new ArrayList<>());
        }
        RelatedContract relatedContract = mapOutRelatedContract(formatoMPMS2G1.getIdcorel(), formatoMPMS2G1.getNumber(),
                formatoMPMS2G1.getNucorel(), formatoMPMS2G1.getIdtcore(), formatoMPMS2G1.getDetcore());

        dtoOut.getRelatedContracts().add(relatedContract);

        return dtoOut;
    }

    private RelatedContract mapOutRelatedContract(final String idcorel, final String number, final String nucorel,
                                                  final String idtcore, final String detcore) {
        RelatedContract relatedContract = new RelatedContract();
        relatedContract.setRelatedContractId(idcorel);
        relatedContract.setNumber(number);
        relatedContract.setContractId(nucorel);
        relatedContract.setNumberType(mapOutNumberType(idtcore, detcore));
        return relatedContract;

    }

    @Override
    public Card mapOut3(final FormatoMPMS3G1 formatoMPMS3G1, final Card dtoOut) {
        LOG.info("... called method TxGetCardMapper.mapOut3 ...");
        if (dtoOut.getParticipants() == null) {
            dtoOut.setParticipants(new ArrayList<>());
        }
        Participant participant = mapOutParticipant(
                formatoMPMS3G1.getIdpart(), formatoMPMS3G1.getNompart(), formatoMPMS3G1.getApepart(),
                formatoMPMS3G1.getTippart(), formatoMPMS3G1.getDespart()
        );

        dtoOut.getParticipants().add(participant);

        return dtoOut;
    }

    private Participant mapOutParticipant(String idPart, String nomPart, String apePart, String tipPart, String desPart) {
        Participant participant = new Participant();
        participant.setParticipantId(idPart);
        participant.setFirstName(nomPart);
        participant.setLastName(apePart);
        participant.setParticipantType(mapOutParticipantType(tipPart, desPart));
        return participant;
    }

    private ParticipantType mapOutParticipantType(String id, String name) {
        if (id == null && name == null) {
            return null;
        }
        ParticipantType participantType = new ParticipantType();
        participantType.setId(translator.translateBackendEnumValueStrictly("participants.participantType.id", id));
        participantType.setName(name);
        return participantType;
    }

    @Override
    public Card mapOut4(final FormatoMPMS4G1 formatoMPMS4G1, final Card dtoOut) {
        LOG.info("... called method TxGetCardMapper.mapOut4 ...");
        if (dtoOut.getConditions() == null) {
            dtoOut.setConditions(new ArrayList<>());
        }

        Condition condition = new Condition();
        condition.setName(formatoMPMS4G1.getDsconme());
        mapOutcomes(formatoMPMS4G1, condition);
        mapPeriod(formatoMPMS4G1, condition);
        mapAccumulateAmount(formatoMPMS4G1, condition);
        mapFacts(formatoMPMS4G1, condition);

        condition.setConditionId(translator.translateBackendEnumValueStrictly("conditions.conditionId", formatoMPMS4G1.getIdconme()));
        dtoOut.getConditions().add(condition);

        return dtoOut;
    }

    private void mapOutcomeType(final FormatoMPMS4G1 formatoMPMS4G1, final Outcome outcome) {
        if (formatoMPMS4G1.getTipcome() != null || formatoMPMS4G1.getDscomme() != null) {
            OutcomeType outcomeType = new OutcomeType();
            outcomeType.setId(translator.translateBackendEnumValueStrictly("conditions.outcomes.outcomeType.id", formatoMPMS4G1.getTipcome()));
            outcomeType.setName(formatoMPMS4G1.getDscomme());
            outcome.setOutcomeType(outcomeType);
        }
    }

    private void mapFeeAmount(final FormatoMPMS4G1 formatoMPMS4G1, final Outcome outcome) {
        if (formatoMPMS4G1.getImporte() != null || formatoMPMS4G1.getMonimp() != null) {
            FeeAmount feeAmount = new FeeAmount();
            feeAmount.setAmount(formatoMPMS4G1.getImporte());
            feeAmount.setCurrency(formatoMPMS4G1.getMonimp());
            outcome.setFeeAmount(feeAmount);
        }
    }

    private void mapOutcomes(final FormatoMPMS4G1 formatoMPMS4G1, final Condition condition) {
        List<Outcome> outcomes = new ArrayList<>();
        Outcome outcome = new Outcome();
        outcome.setDueDate(FunctionUtils.buildDatetime(formatoMPMS4G1.getFecmem(), DEFAULT_TIME));
        mapOutcomeType(formatoMPMS4G1, outcome);
        mapFeeAmount(formatoMPMS4G1, outcome);
        outcomes.add(outcome);
        condition.setOutcomes(outcomes);
    }

    private void mapRemainingTime(final FormatoMPMS4G1 formatoMPMS4G1, final Period period) {
        if (formatoMPMS4G1.getCanmesm() != null) {
            RemainingTime remainingTime = new RemainingTime();
            remainingTime.setNumber(formatoMPMS4G1.getCanmesm());
            remainingTime.setUnit("MONTHS");
            period.setRemainingTime(remainingTime);
        }
    }

    private void mapPeriod(final FormatoMPMS4G1 formatoMPMS4G1, final Condition condition) {
        if (formatoMPMS4G1.getIdperio() != null || formatoMPMS4G1.getDsperio() != null ||
                formatoMPMS4G1.getFeceval() != null || formatoMPMS4G1.getFecinme() != null ||
                formatoMPMS4G1.getCanmesm() != null) {

            Period period = new Period();
            period.setId(formatoMPMS4G1.getIdperio());
            period.setName(formatoMPMS4G1.getDsperio());
            period.setCheckDate(FunctionUtils.buildDatetime(formatoMPMS4G1.getFeceval(), DEFAULT_TIME));
            period.setStartDate(FunctionUtils.buildDatetime(formatoMPMS4G1.getFecinme(), DEFAULT_TIME));
            mapRemainingTime(formatoMPMS4G1, period);
            condition.setPeriod(period);
        }
    }

    private void mapAccumulateAmount(final FormatoMPMS4G1 formatoMPMS4G1, final Condition condition) {
        if (formatoMPMS4G1.getImpacum() != null || formatoMPMS4G1.getMonimp() != null) {
            AccumulatedAmount accumulateAmount = new AccumulatedAmount();
            accumulateAmount.setAmount(formatoMPMS4G1.getImpacum());
            accumulateAmount.setCurrency(formatoMPMS4G1.getMonimp());
            condition.setAccumulatedAmount(accumulateAmount);
        }
    }

    private void mapFactType(final FormatoMPMS4G1 formatoMPMS4G1, final Fact fact) {
        FactType factType = new FactType();
        factType.setId(translator.translateBackendEnumValueStrictly("conditions.outcomes.factType.id", formatoMPMS4G1.getIdlsgol()));
        factType.setName(formatoMPMS4G1.getDslsgol());
        fact.setFactType(factType);
    }

    private void mapApply(final FormatoMPMS4G1 formatoMPMS4G1, final Fact fact) {
        Apply apply = new Apply();
        apply.setId(translator.translateBackendEnumValueStrictly("conditions.outcomes.apply.id", formatoMPMS4G1.getIdcubal()));
        apply.setName(formatoMPMS4G1.getDscubal());
        fact.setApply(apply);
    }

    private void mapConditionAmount(final FormatoMPMS4G1 formatoMPMS4G1, final Fact fact) {
        if (formatoMPMS4G1.getImpmeta() != null || formatoMPMS4G1.getMonimp() != null) {
            ConditionAmount conditionAmount = new ConditionAmount();
            conditionAmount.setAmount(formatoMPMS4G1.getImpmeta());
            conditionAmount.setCurrency(formatoMPMS4G1.getMonimp());
            fact.setConditionAmount(conditionAmount);
        }
    }

    private void mapFacts(final FormatoMPMS4G1 formatoMPMS4G1, final Condition condition) {
        List<Fact> facts = new ArrayList<>();
        Fact fact = new Fact();
        mapFactType(formatoMPMS4G1, fact);
        mapApply(formatoMPMS4G1, fact);
        mapConditionAmount(formatoMPMS4G1, fact);
        facts.add(fact);
        condition.setFacts(facts);
    }

    @Override
    public Card mapOut5(final FormatoMPMS5G1 formatOutput, final Card dtoOut) {
        LOG.info("... called method TxGetCardMapper.mapOut5 ...");
        if (CollectionUtils.isEmpty(dtoOut.getPaymentMethods())) {
            dtoOut.setPaymentMethods(new ArrayList<>());
            dtoOut.getPaymentMethods().add(new PaymentMethod());
        }

        dtoOut.getPaymentMethods().get(0).setId(translator.translateBackendEnumValueStrictly("paymentMethod.paymentType.id", formatOutput.getIdforpa()));
        dtoOut.getPaymentMethods().get(0).setName(formatOutput.getDsforpa());
        dtoOut.getPaymentMethods().get(0).setEndDate(formatOutput.getFecpago());
        return dtoOut;
    }

    @Override
    public Card mapOut6(final FormatoMPMS6G1 formatOutput, final Card dtoOut) {
        LOG.info("... called method TxGetCardMapper.mapOut6 ...");
        if (CollectionUtils.isEmpty(dtoOut.getPaymentMethods())) {
            dtoOut.setPaymentMethods(new ArrayList<>());
            dtoOut.getPaymentMethods().add(new PaymentMethod());
        }
        if (dtoOut.getPaymentMethods().get(0).getPaymentAmounts() == null) {
            dtoOut.getPaymentMethods().get(0).setPaymentAmounts(new ArrayList<>());
        }
        PaymentAmount paymentAmount = new PaymentAmount();
        paymentAmount.setId(translator.translateBackendEnumValueStrictly("paymentMethod.paymentAmountsType.id", formatOutput.getIdforpa()));
        paymentAmount.setName(formatOutput.getDsforpa());
        paymentAmount.setValues(mapOutListValue(formatOutput.getImpamis(), formatOutput.getMopamis()));

        dtoOut.getPaymentMethods().get(0).setPaymentAmounts(updatePaymentAmounts(dtoOut.getPaymentMethods().get(0).getPaymentAmounts(), paymentAmount));
        return dtoOut;
    }

    @Override
    public Card mapOut7(final FormatoMPMS7G1 formatOutput, final Card dtoOut) {
        LOG.info("... called method TxGetCardMapper.mapOut7 ...");
        if (CollectionUtils.isEmpty(dtoOut.getRates())) {
            dtoOut.setRates(new ArrayList<>());
        }

        Rate rate = new Rate();
        mapRateType(formatOutput, rate);
        mapUnit(formatOutput, rate);
        dtoOut.getRates().add(rate);
        return dtoOut;
    }

    private void mapRateType(final FormatoMPMS7G1 formatOutput, final Rate rate) {
        if (formatOutput.getIdttasa() != null || formatOutput.getNomtasa() != null) {
            RateType rateType = new RateType();
            rateType.setId(formatOutput.getIdttasa());
            rateType.setName(formatOutput.getNomtasa());
            rate.setRateType(rateType);
        }
    }

    private void mapUnit(final FormatoMPMS7G1 formatOutput, final Rate rate) {
        if (formatOutput.getTiptasa() != null || formatOutput.getNomtipt() != null || formatOutput.getValtasa() != null) {
            Percentage percentage = new Percentage();
            percentage.setId(translator.translateBackendEnumValueStrictly("cards.rates.mode", formatOutput.getTiptasa()));
            percentage.setName(formatOutput.getNomtipt());
            percentage.setPercentage(formatOutput.getValtasa());
            rate.setUnit(percentage);
        }
    }

    private List<Value> mapOutListValue(BigDecimal amount, String currency) {
        if (amount == null && currency == null) {
            return null;
        }
        Value value = new Value();
        value.setAmount(amount);
        value.setCurrency(currency);
        List<Value> valueList = new ArrayList<>();
        valueList.add(value);
        return valueList;
    }

    private List<PaymentAmount> updatePaymentAmounts(final List<PaymentAmount> paymentAmounts, final PaymentAmount paymentAmount) {
        boolean found = true;
        for (PaymentAmount amount : paymentAmounts) {
            if (amount.getId().equals(paymentAmount.getId())) {
                amount.getValues().addAll(paymentAmount.getValues());
                found = false;
            }
        }
        if (found) {
            paymentAmounts.add(paymentAmount);
        }
        return paymentAmounts;
    }

    @Override
    public Card mapOut8(final FormatoMPMS8G1 formatOutput, final Card card) {
        LOG.info("... called method TxGetCardMapper.mapOut8 ...");
        List<Activation> tmp = new ArrayList<>();
        tmp.add(formatToActivation(formatOutput.getCodactv(), formatOutput.getIndactv()));
        tmp.add(formatToActivation(formatOutput.getCodreti(), formatOutput.getIndreti()));
        tmp.add(formatToActivation(formatOutput.getCodinte(), formatOutput.getIndinte()));
        tmp.add(formatToActivation(formatOutput.getCodcoex(), formatOutput.getIndcoex()));
        tmp.add(formatToActivation(formatOutput.getCodsobr(), formatOutput.getIndsobr()));
        tmp.add(formatToActivation(formatOutput.getCoddcvv(), formatOutput.getInddcvv()));
        List<Activation> activations = tmp.stream().filter(Objects::nonNull).collect(Collectors.toList());

        card.setActivations(activations.isEmpty() ? null : activations);
        return card;
    }

    private Activation formatToActivation(final String activationId, final String isActive) {
        if (activationId == null && isActive == null) {
            return null;
        }
        Activation activation = new Activation();
        activation.setActivationId(translator.translateBackendEnumValueStrictly("cards.activation.activationId", activationId));

        if (isActive != null)
            activation.setIsActive(stringToBoolean(isActive));

        return activation;
    }

    private Boolean stringToBoolean(String value) {
        return "S".equalsIgnoreCase(value) ? Boolean.TRUE : Boolean.FALSE;
    }
}

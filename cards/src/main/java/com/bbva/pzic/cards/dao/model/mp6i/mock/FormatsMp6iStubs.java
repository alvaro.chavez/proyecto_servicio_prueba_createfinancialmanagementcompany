package com.bbva.pzic.cards.dao.model.mp6i.mock;

import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS23G;
import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS33G;
import com.bbva.pzic.cards.dao.model.mp6i.FormatoMPMS16I;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;

public class FormatsMp6iStubs {

    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private static final FormatsMp6iStubs INSTANCE = new FormatsMp6iStubs();

    private FormatsMp6iStubs() {
    }

    public static FormatsMp6iStubs getInstance() {
        return INSTANCE;
    }

    public FormatoMPMS16I getFormatoMPMS16I() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mp6i/mock/formatoMPMS16I.json"), FormatoMPMS16I.class);
    }
}
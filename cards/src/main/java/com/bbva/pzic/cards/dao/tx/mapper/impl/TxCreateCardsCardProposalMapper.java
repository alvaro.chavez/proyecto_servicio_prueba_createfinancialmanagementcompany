package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.InputCreateCardsCardProposal;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.kb93.FormatoKTECKB93;
import com.bbva.pzic.cards.dao.model.kb93.FormatoKTSKB931;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardsCardProposalMapper;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
@Mapper
public class TxCreateCardsCardProposalMapper extends ConfigurableMapper
        implements
        ITxCreateCardsCardProposalMapper {

    @Autowired
    private EnumMapper enumMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public FormatoKTECKB93 mapIn(final InputCreateCardsCardProposal dtoIn) {
        return map(dtoIn, FormatoKTECKB93.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CardProposal mapOut(final FormatoKTSKB931 formatOutput) {
        CardProposal cardProposal = map(formatOutput, CardProposal.class);

        cardProposal.setCardType(enumMapCardType(cardProposal.getCardType()));

        cardProposal.setDelivery(enumMapDelivery(cardProposal.getDelivery()));

        cardProposal.setParticipant(enumMapParticipantIdentityDocumentType(cardProposal.getParticipant()));

        cardProposal.setOperationDate(buildDate(formatOutput.getFecope(), formatOutput.getHorope()));

        return cardProposal;
    }

    private Delivery enumMapDelivery(final Delivery delivery) {
        if (delivery == null || delivery.getDestination() == null) {
            return delivery;
        }
        delivery.getDestination().setId(enumMapper.getEnumValue("cards.delivery.destination.id", delivery.getDestination().getId()));
        return delivery;
    }

    private AuthorizedParticipant enumMapParticipantIdentityDocumentType(final AuthorizedParticipant participant) {
        if (participant == null || participant.getIdentityDocument() == null || participant.getIdentityDocument().getDocumentType() == null) {
            return participant;
        }
        final IdentityDocument identityDocument = participant.getIdentityDocument();
        final DocumentType documentType = identityDocument.getDocumentType();
        documentType.setId(enumMapper.getEnumValue("cards.identityDocument.documentType.id", documentType.getId()));
        return participant;
    }

    private CardType enumMapCardType(final CardType cardType) {
        if (cardType == null) {
            return cardType;
        }
        cardType.setId(enumMapper.getEnumValue("cards.cardType.id", cardType.getId()));
        return cardType;
    }

    private Calendar buildDate(final Date fecope, final String horope) {
        if (fecope == null || horope == null) {
            return null;
        }

        try {
            return DateUtils.toDateTime(fecope, horope);
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.classMap(InputCreateCardsCardProposal.class,
                FormatoKTECKB93.class)
                .field("cardId", "numtarj")
                .field("cardProposal.title.id", "idsprod")
                .field("cardProposal.cardType.id", "idtipta")
                .field("cardProposal.participant.identityDocument.documentType.id",
                        "tipdoc")
                .field("cardProposal.participant.identityDocument.number",
                        "numdoc")
                .field("cardProposal.participant.firstName", "nombre1")
                .field("cardProposal.participant.middleName", "nombre2")
                .field("cardProposal.participant.lastName", "apelli1")
                .field("cardProposal.participant.secondLastName", "apelli2")
                .field("cardProposal.participant.profession.id", "idprofe")
                .field("cardProposal.participant.maritalStatus.id", "idestci")
                .field("cardProposal.participant.relationType.id", "idvinc")
                .field("cardProposal.participant.contactDetails[0].contactValue",
                        "valcon1")
                .field("cardProposal.participant.contactDetails[0].contactType.id",
                        "tipcon1")
                .field("cardProposal.participant.contactDetails[1].contactValue",
                        "valcon2")
                .field("cardProposal.participant.contactDetails[1].contactType.id",
                        "tipcon2")
                .field("cardProposal.grantedCredits[0].amount", "monline")
                .field("cardProposal.grantedCredits[0].currency", "divisal")
                .field("cardProposal.delivery.destination.id", "iddesti")
                .field("cardProposal.bank.id", "idbanco")
                .field("cardProposal.bank.branch.id", "idofici")
                .field("cardProposal.offerId", "idofert").register();
        factory.classMap(FormatoKTSKB931.class, CardProposal.class)
                .field("idsolic", "id")
                .field("idprodu", "title.id")
                .field("desprod", "title.name")
                .field("idtipta", "cardType.id")
                .field("descta", "cardType.name")
                .field("idbanco", "bank.id")
                .field("desbanc", "bank.name")
                .field("idofici", "bank.branch.id")
                .field("descofi", "bank.branch.name")
                .field("iddesti", "delivery.destination.id")
                .field("nomdest", "delivery.destination.name")
                .field("tipdoc", "participant.identityDocument.documentType.id")
                .field("destdo",
                        "participant.identityDocument.documentType.description")
                .field("numdoc", "participant.identityDocument.number")
                .field("nombre1", "participant.firstName")
                .field("nombre2", "participant.middleName")
                .field("apelli1", "participant.lastName")
                .field("apelli2", "participant.secondLastName")
                .field("idprofe", "participant.profession.id")
                .field("descpro", "participant.profession.name")
                .field("idestci", "participant.maritalStatus.id")
                .field("descec", "participant.maritalStatus.name")
                .field("idvinc", "participant.relationType.id")
                .field("desvinc", "participant.relationType.name")
                .field("monline", "grantedCredits[0].amount")
                .field("divisal", "grantedCredits[0].currency").register();
    }
}
package com.bbva.pzic.cards.facade.v0.dto;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "Product", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "Product", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;

    private Subproduct subproduct;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Subproduct getSubproduct() {
        return subproduct;
    }

    public void setSubproduct(Subproduct subproduct) {
        this.subproduct = subproduct;
    }
}

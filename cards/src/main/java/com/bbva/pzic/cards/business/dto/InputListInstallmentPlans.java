package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

/**
 * Created on 08/02/2018.
 *
 * @author Entelgy
 */
public class InputListInstallmentPlans {
    @Size(max = 20, groups = ValidationGroup.ListInstallmentPlansV0.class)
    private String cardId;
    @Size(max = 24, groups = ValidationGroup.ListInstallmentPlansV0.class)
    private String paginationKey;
    @Digits(integer = 3, fraction = 0, groups = ValidationGroup.ListInstallmentPlansV0.class)
    private Long pageSize;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getPaginationKey() {
        return paginationKey;
    }

    public void setPaginationKey(String paginationKey) {
        this.paginationKey = paginationKey;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }
}

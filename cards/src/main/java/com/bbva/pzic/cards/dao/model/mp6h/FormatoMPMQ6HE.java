package com.bbva.pzic.cards.dao.model.mp6h;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPMQ6HE</code> de la transacci&oacute;n <code>MP6H</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMQ6HE")
@RooJavaBean
@RooSerializable
public class FormatoMPMQ6HE {

	/**
	 * <p>Campo <code>IDETARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDETARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	private String idetarj;
	
	/**
	 * <p>Campo <code>IDDOCTA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "IDDOCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 26, longitudMaxima = 26)
	private String iddocta;
	
}
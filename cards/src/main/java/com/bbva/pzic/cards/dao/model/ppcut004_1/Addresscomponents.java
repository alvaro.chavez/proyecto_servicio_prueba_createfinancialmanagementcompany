package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>addressComponents</code>, utilizado por la clase <code>Location</code></p>
 * 
 * @see Location
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Addresscomponents {
	
	/**
	 * <p>Campo <code>addressComponent</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "addressComponent", tipo = TipoCampo.DTO)
	private Addresscomponent addresscomponent;
	
}
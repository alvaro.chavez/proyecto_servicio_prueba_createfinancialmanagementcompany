package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 20/04/2020.
 *
 * @author Entelgy.
 */
public class DTOIntContactAbility {

    private String reason;
    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    @Valid
    private DTOIntScheduletimes scheduletimes;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public DTOIntScheduletimes getScheduletimes() {
        return scheduletimes;
    }

    public void setScheduletimes(DTOIntScheduletimes scheduletimes) {
        this.scheduletimes = scheduletimes;
    }
}


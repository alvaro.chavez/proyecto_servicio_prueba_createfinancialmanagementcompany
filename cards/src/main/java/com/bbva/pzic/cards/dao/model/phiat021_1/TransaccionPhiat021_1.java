package com.bbva.pzic.cards.dao.model.phiat021_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PHIAT021</code>
 * 
 * @see PeticionTransaccionPhiat021_1
 * @see RespuestaTransaccionPhiat021_1
 */
@Component
public class TransaccionPhiat021_1 implements InvocadorTransaccion<PeticionTransaccionPhiat021_1,RespuestaTransaccionPhiat021_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPhiat021_1 invocar(PeticionTransaccionPhiat021_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPhiat021_1.class, RespuestaTransaccionPhiat021_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPhiat021_1 invocarCache(PeticionTransaccionPhiat021_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPhiat021_1.class, RespuestaTransaccionPhiat021_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
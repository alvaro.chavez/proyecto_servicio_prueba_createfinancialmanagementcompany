package com.bbva.pzic.cards.dao.model.mpe2.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPME1E2;
import com.bbva.pzic.cards.dao.model.mpe2.FormatoMPMS1E2;
import com.bbva.pzic.cards.dao.model.mpe2.PeticionTransaccionMpe2;
import com.bbva.pzic.cards.dao.model.mpe2.RespuestaTransaccionMpe2;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 18/07/2018.
 *
 * @author Entelgy
 */
@Component("transaccionMpe2")
public class TransaccionMpe2Mock implements InvocadorTransaccion<PeticionTransaccionMpe2, RespuestaTransaccionMpe2> {

    public static final String TEST_NOT_PAGINATION = "111";
    public static final String TEST_EMPTY = "999";
    public static final String TEST_NULL_IDPAGIN = "100";
    public static final String TEST_NULL_TAMPAGI = "666";

    @Override
    public RespuestaTransaccionMpe2 invocar(PeticionTransaccionMpe2 peticion) {
        final RespuestaTransaccionMpe2 response = new RespuestaTransaccionMpe2();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPME1E2 format = peticion.getCuerpo().getParte(FormatoMPME1E2.class);

        if (TEST_EMPTY.equalsIgnoreCase(format.getIdpagin())) {
            return response;
        }

        try {
            response.getCuerpo().getPartes().addAll(buildDataCopiesFormats());

            if (TEST_NOT_PAGINATION.equalsIgnoreCase(format.getIdpagin())) {
                return response;
            }

            response.getCuerpo().getPartes().add(buildPaginationCopy(format.getIdpagin()));

            return response;
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionMpe2 invocarCache(PeticionTransaccionMpe2 peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private List<CopySalida> buildDataCopiesFormats() throws IOException {
        List<FormatoMPMS1E2> formats = FormatsMpe2Mock.getInstance().getFormatoMPMS1E2();

        List<CopySalida> copies = new ArrayList<>();
        for (FormatoMPMS1E2 format : formats) {
            CopySalida copy = new CopySalida();
            copy.setCopy(format);
            copies.add(copy);
        }
        return copies;
    }

    private CopySalida buildPaginationCopy(String idPagin) throws IOException {
        CopySalida copy = new CopySalida();
        copy.setCopy(FormatsMpe2Mock.getInstance().getFormatoMPMS2E2(idPagin));
        return copy;
    }
}

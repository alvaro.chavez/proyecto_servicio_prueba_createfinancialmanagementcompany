package com.bbva.pzic.cards.facade.v0.impl;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.*;
import com.bbva.jee.arq.spring.core.managers.InputHeaderManager;
import com.bbva.jee.arq.spring.core.managers.OutputHeaderManager;
import com.bbva.jee.arq.spring.core.servicing.annotations.PATCH;
import com.bbva.jee.arq.spring.core.servicing.annotations.*;
import com.bbva.jee.arq.spring.core.servicing.providers.wadl.MultipartDescription;
import com.bbva.jee.arq.spring.core.servicing.providers.wadl.PartDescription;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;
import com.bbva.jee.arq.spring.core.servicing.utils.toolkit.BusinessServicesToolkitManager;
import com.bbva.pzic.cards.business.ISrvIntCards;
import com.bbva.pzic.cards.business.ISrvIntCardsV0;
import com.bbva.pzic.cards.business.dto.DTOInstallmentsPlanList;
import com.bbva.pzic.cards.business.dto.DTOIntStatementList;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.canonic.PaymentMethod;
import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.ISrvCardsV0;
import com.bbva.pzic.cards.facade.v0.dto.Membership;
import com.bbva.pzic.cards.facade.v0.dto.*;
import com.bbva.pzic.cards.facade.v0.mapper.*;
import com.bbva.pzic.cards.util.BusinessServiceUtil;
import com.bbva.pzic.cards.util.mappers.GabiPaginationMapper;
import com.bbva.pzic.cards.util.mappers.PaginationMapper;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.cards.facade.RegistryIds.*;
import static com.bbva.pzic.cards.util.Enums.*;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
@Path("/v0")
@SN(registryID = "SNPE1710030", logicalID = "cards")
@VN(vnn = "v0")
@Produces(MediaType.APPLICATION_JSON)
@Service
public class SrvCardsV0 implements ISrvCardsV0, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

    private static final Log LOG = LogFactory.getLog(SrvCardsV0.class);

    private static final String HEADER_BCS_DEVICE_SCREEN_SIZE = "BCS-Device-Screen-Size";

    public UriInfo uriInfo;
    public HttpHeaders httpHeaders;

    @Autowired
    @Deprecated
    private BusinessServicesToolKit businessToolKit;
    @Autowired
    private BusinessServicesToolkitManager businessServicesToolkitManager;

    private InputHeaderManager inputHeaderManager;
    private OutputHeaderManager outputHeaderManager;

    @Autowired
    private ISrvIntCards srvIntCards;

    @Autowired
    private ISrvIntCardsV0 srvIntCardsV0;

    @Autowired
    private IGetCardSecurityDataMapper getCardSecurityDataMapper;

    @Autowired
    private IListCardsV0Mapper listCardsMapper;

    @Autowired
    private IModifyCardBlockMapper modifyCardBlockMapper;

    @Autowired
    private ISimulateCardInstallmentsPlanMapper simulateCardInstallmentsPlanMapper;

    @Autowired
    private IListCardActivationsMapper listCardActivationsMapper;

    @Autowired
    private IModifyCardActivationsMapper modifyCardActivationsMapper;

    @Autowired
    private IGetCardMapper getCardMapper;

    @Autowired
    private IGetCardPaymentMethodsMapper getCardPaymentMethodsMapper;

    @Autowired
    private IModifyCardPinMapper modifyCardPinMapper;

    @Autowired
    private IModifyPartialCardBlockMapper modifyPartialCardBlockMapper;

    @Autowired
    private IListCardTransactionsV0Mapper listCardTransactionsV0Mapper;

    @Autowired
    private IModifyCardLimitMapper modifyCardLimitMapper;

    @Resource(name = "getCardTransactionMapper")
    private IGetCardTransactionMapper transactionMapper;

    @Resource(name = "createCardMapperV0")
    private ICreateCardMapper createCardMapper;

    @Resource(name = "listInstallmentPlansMapper")
    private IListInstallmentPlansMapper plansMapper;

    @Autowired
    private ICreateCardInstallmentsPlanV0Mapper createCardInstallmentsPlanMapper;

    @Autowired
    private IModifyCardV0Mapper modifyCardV0Mapper;

    @Autowired
    private IGetCardConditionsMapper getCardConditionsMapper;

    @Autowired
    private IListCardFinancialStatementsMapper listCardFinancialStatementsMapper;

    @Autowired
    private IGetCardFinancialStatementMapper getCardFinancialStatementMapper;

    @Autowired
    private IGetCardFinancialStatementDocumentMapper getCardFinancialStatementDocumentMapper;

    @Autowired
    private ICreateCardsMaskedToken createCardsMaskedTokenMapper;

    @Autowired
    private IGetCardMembershipMapper getCardMembershipMapper;

    @Autowired
    private ICreateCardsOfferSimulateMapper createCardsOfferSimulateMapper;

    @Autowired
    private IGetCardsCardOfferMapper getCardsCardOfferMapper;

    @Autowired
    private ICreateCardsProposalMapper createCardsProposalMapper;

    @Autowired
    private IListCardRelatedContractsMapper listCardRelatedContractsMapper;

    @Autowired
    private ICreateCardsCardProposalMapper createCardsCardProposalMapper;

    @Autowired
    private IListCardCashAdvancesMapper listCardCashAdvancesMapper;

    @Autowired
    private ICreateCardCashRefundMapper createCardCashRefundMapper;

    @Autowired
    private IListCardLimitV0Mapper listCardLimitsMapper;

    @Autowired
    private ICreateCardReportsV0Mapper createCardReportsV0Mapper;

    @Autowired
    private ICreateOffersGenerateCardsMapper createOffersGenerateCardsMapper;

    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;
    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;

    @Override
    public void setUriInfo(UriInfo ui) {
        this.uriInfo = ui;
    }

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    @Autowired
    public void setInputHeaderManager(InputHeaderManager inputHeaderManager) {
        this.inputHeaderManager = inputHeaderManager;
    }

    @Autowired
    public void setOutputHeaderManager(OutputHeaderManager outputHeaderManager) {
        this.outputHeaderManager = outputHeaderManager;
    }

    @Override
    @GET
    @Path("/cards")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_CARDS, logicalID = "listCards", forcedCatalog = "asoCatalog")
    public ServiceResponse<List<Card>> listCards(
            @CasCustomer @QueryParam(CUSTOMER_ID) final String customerId,
            @QueryParam(CARD_TYPE_ID) final List<String> cardTypeId,
            @QueryParam(PHYSICAL_SUPPORT_ID) final String physicalSupportId,
            @QueryParam(STATUS_ID) final List<String> statusId,
            @QueryParam(PARTICIPANTS_PARTICIPANT_TYPE_ID) final List<String> participantsParticipantTypeId,
            @QueryParam(EXPAND) final String expand,
            @QueryParam(PAGINATION_KEY) final String paginationKey,
            @QueryParam(PAGE_SIZE) final Integer pageSize) {

        LOG.info("... called method SrvCardsV0.listCards ...");

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(CUSTOMER_ID, customerId);
        queryParams.put(CARD_TYPE_ID, cardTypeId);
        queryParams.put(PHYSICAL_SUPPORT_ID, physicalSupportId);
        queryParams.put(STATUS_ID, statusId);
        queryParams.put(PARTICIPANTS_PARTICIPANT_TYPE_ID, participantsParticipantTypeId);
        queryParams.put(EXPAND, expand);
        queryParams.put(PAGINATION_KEY, paginationKey);
        queryParams.put(PAGE_SIZE, pageSize);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARDS, null, null, queryParams);

        DTOOutListCards dto = srvIntCardsV0.listCardsV0(
                listCardsMapper.mapIn((String) queryParams.get(CUSTOMER_ID),
                        (List<String>) queryParams.get(CARD_TYPE_ID),
                        (String) queryParams.get(PHYSICAL_SUPPORT_ID),
                        (List<String>) queryParams.get(STATUS_ID),
                        (List<String>) queryParams.get(PARTICIPANTS_PARTICIPANT_TYPE_ID),
                        (String) queryParams.get(PAGINATION_KEY),
                        (Integer) queryParams.get(PAGE_SIZE)));

        BusinessServiceUtil.expand(dto.getData(),
                "relatedContracts,blocks,activations,participants",
                expand == null ? null : queryParams.get(EXPAND).toString().replace("-c", "C"));

        Pagination pagination = null;
        if (dto.getPagination() != null && dto.getPagination().getPageSize() != null) {
            pagination = GabiPaginationMapper.perform(businessServicesToolkitManager.getPaginationBuilder()
                    .setPagination(SrvCardsV0.class, "listCards", uriInfo,
                            dto.getPagination().getPaginationKey(), null,
                            dto.getPagination().getPageSize(), null,
                            null, null, null).build());
        }

        ServiceResponse<List<Card>> serviceResponse = listCardsMapper.mapOut(dto, pagination);

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARDS, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @GET
    @Path("/cards/{card-id}/limits")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_CARD_LIMITS, logicalID = "listCardLimits")
    public ServiceResponse<List<Limits>> listCardLimits(@PathParam(CARD_ID) final String cardId) {
        LOG.info("... called method SrvCardsV0.listCardLimits ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARD_LIMITS, null, pathParams, null);
        ServiceResponse<List<Limits>> serviceResponse = listCardLimitsMapper.mapOut(
                srvIntCardsV0.listCardLimits(listCardLimitsMapper.mapIn((String) pathParams.get(CARD_ID))));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARD_LIMITS, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @PUT
    @Path("/cards/{card-id}/blocks/{block-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK, logicalID = "modifyCardBlock", securityValidator = "modifyCardBlockSecurityValidatorV0", forcedCatalog = "asoCatalog")
    public BlockData modifyCardBlock(@CasContract @SecurityFunction(inFunction = "decypher") @PathParam(CARD_ID) final String cardId,
                                     @PathParam(BLOCK_ID) final String blockId,
                                     final Block block) {
        LOG.info("... called method SrvCardsV0.modifyCardBlock ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(BLOCK_ID, blockId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK, block, pathParams, null);

        BlockData data = srvIntCards.modifyCardBlock(
                modifyCardBlockMapper.mapInput((String) pathParams.get(CARD_ID), (String) pathParams.get(BLOCK_ID), block));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK, data, pathParams, null);

        return data;
    }

    @Override
    @POST
    @Path("/cards/{card-id}/installments-plans/simulate")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_SIMULATE_CARD_INSTALLMENTS_PLAN, logicalID = "simulateCardInstallmentsPlan", forcedCatalog = "asoCatalog")
    public InstallmentsPlanSimulationData simulateCardInstallmentsPlan(@PathParam(CARD_ID) final String cardId,
                                                                       InstallmentsPlanSimulation installmentsPlanSimulation) {
        LOG.info("... called method SrvCardsV0.simulateCardInstallmentsPlan ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SIMULATE_CARD_INSTALLMENTS_PLAN, installmentsPlanSimulation, pathParams, null);

        InstallmentsPlanSimulationData data = srvIntCardsV0.simulateCardInstallmentsPlan(simulateCardInstallmentsPlanMapper.mapIn((String) pathParams.get(CARD_ID), installmentsPlanSimulation));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SIMULATE_CARD_INSTALLMENTS_PLAN, data, pathParams, null);

        return data;
    }

    @Override
    @GET
    @Path("/cards/{card-id}/activations")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_CARD_ACTIVATIONS, logicalID = "listCardActivations")
    public ServiceResponse<List<Activation>> listCardActivations(@CasContract @SecurityFunction(inFunction = "decypher") @PathParam(CARD_ID) String cardId) {
        LOG.info("... called method SrvCardsV0.listCardActivations ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARD_ACTIVATIONS, null, pathParams, null);

        ServiceResponse<List<Activation>> serviceResponse = listCardActivationsMapper.mapOut(
                srvIntCards.listCardActivations(
                        listCardActivationsMapper.mapIn(pathParams.get(CARD_ID).toString())));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARD_ACTIVATIONS, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @PATCH
    @Path("/cards/{card-id}/activations")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATIONS, logicalID = "modifyCardActivations")
    public ServiceResponse<List<Activation>> modifyCardActivations(@CasContract @SecurityFunction(inFunction = "decypher") @PathParam(CARD_ID) final String cardId,
                                                                   final List<Activation> items) {
        LOG.info("... called method SrvCardsV0.modifyCardActivations ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATIONS, items, pathParams, null);

        ServiceResponse<List<Activation>> serviceResponse = modifyCardActivationsMapper.mapOut(
                srvIntCards.modifyCardActivations(
                        modifyCardActivationsMapper.mapIn(pathParams.get(CARD_ID).toString(), items)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATIONS, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @GET
    @Path("/cards/{card-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CARD, logicalID = "getCard")
    public ServiceResponse<Card> getCard(@CasContract @DatoAuditable(omitir = true) @SecurityFunction(inFunction = "decypher") @PathParam(CARD_ID) final String cardId,
                                         @QueryParam(EXPAND) final String expand) {

        LOG.info("... called method SrvCardsV0.getCard ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD, null, pathParams, null);

        ServiceResponse<Card> serviceResponse = getCardMapper.mapOut(
                srvIntCardsV0.getCard(
                        getCardMapper.mapIn(pathParams.get(CARD_ID).toString())));
        if (serviceResponse == null) {
            return null;
        }
        BusinessServiceUtil.expand(serviceResponse.getData(), "relatedContracts,participants,conditions,paymentMethods,activations",
                expand == null ? null : expand.replace("-c", "C").replace("-m", "M"));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD, serviceResponse, pathParams, null);

        return serviceResponse;
    }

    @Override
    @GET
    @Path("/cards/{card-id}/payment-methods")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CARD_PAYMENT_METHODS, logicalID = "getCardPaymentMethods")
    public ServiceResponse<List<com.bbva.pzic.cards.canonic.PaymentMethod>> getCardPaymentMethods(@CasContract @SecurityFunction(inFunction = "decypher") @DatoAuditable(omitir = true) @PathParam(CARD_ID) final String cardId) {
        LOG.info("... called method SrvCardsV0.getCardPaymentMethods ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_PAYMENT_METHODS, null, pathParams, null);

        ServiceResponse<List<com.bbva.pzic.cards.canonic.PaymentMethod>> serviceResponse = getCardPaymentMethodsMapper.mapOut(srvIntCardsV0
                .getCardPaymentMethods(getCardPaymentMethodsMapper
                        .mapIn((String) pathParams.get(CARD_ID))));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_PAYMENT_METHODS, serviceResponse, pathParams, null);

        return serviceResponse;
    }

    @Override
    @PUT
    @Path("/cards/{card-id}/pin")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_CARD_PIN, logicalID = "modifyCardPin")
    public Response modifyCardPin(@CasContract @SecurityFunction(inFunction = "decypher") @DatoAuditable(omitir = true) @PathParam(CARD_ID) final String cardId, final Pin pin) {
        LOG.info("... called method SrvCardsV0.modifyCardPin ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CARD_PIN, pin, pathParams, null);

        srvIntCards.modifyCardPin(modifyCardPinMapper.mapIn((String) pathParams.get(CARD_ID), pin));

        return Response.ok().build();
    }

    @Override
    @GET
    @Path("/cards/{card-id}/security-data")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA, logicalID = "getCardSecurityData", forcedCatalog = "asoCatalog")
    public SecurityDataArray getCardSecurityData(@DatoAuditable(omitir = true) @PathParam(CARD_ID) final String cardId,
                                                 @DatoAuditable(omitir = true) @QueryParam(PUBLIC_KEY) String publicKey) {

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(PUBLIC_KEY, publicKey);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA, null, pathParams, queryParams);

        SecurityDataArray dataArray = getCardSecurityDataMapper.mapOut(
                srvIntCards.getCardSecurityData(
                        getCardSecurityDataMapper.mapIn((String) pathParams.get(CARD_ID), (String) queryParams.get(PUBLIC_KEY))));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA, dataArray, pathParams, queryParams);

        return dataArray;
    }

    @Override
    @PATCH
    @Path("cards/{card-id}/blocks/{block-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_PARTIAL_CARD_BLOCK, logicalID = "modifyPartialCardBlock")
    public Response modifyPartialCardBlock(@DatoAuditable(omitir = true) @PathParam(CARD_ID) final String cardId,
                                           @PathParam(BLOCK_ID) final String blockId,
                                           Block block) {
        LOG.info("... called method SrvCardsV00.modifyPartialCardBlock ...");
        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(BLOCK_ID, blockId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_PARTIAL_CARD_BLOCK, block, pathParams, null);

        srvIntCards.modifyPartialCardBlock(modifyPartialCardBlockMapper.mapIn(
                (String) pathParams.get(CARD_ID),
                (String) pathParams.get(BLOCK_ID),
                block));

        return Response.ok().build();
    }

    @Override
    @GET
    @Path("/cards/{card-id}/transactions")
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS, logicalID = "listCardTransactions", forcedCatalog = "asoCatalog")
    public ServiceResponse<List<Transaction>> listCardTransactions(@PathParam(CARD_ID) final String cardId,
                                                                   @QueryParam(FROM_OPERATION_DATE) final String fromOperationDate,
                                                                   @QueryParam(TO_OPERATION_DATE) final String toOperationDate,
                                                                   @QueryParam(PAGINATION_KEY) final String paginationKey,
                                                                   @QueryParam(PAGE_SIZE) final Long pageSize) {
        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(FROM_OPERATION_DATE, fromOperationDate);
        queryParams.put(TO_OPERATION_DATE, toOperationDate);
        queryParams.put(PAGINATION_KEY, paginationKey);
        queryParams.put(PAGE_SIZE, pageSize);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS, null, pathParams, queryParams);

        final TransactionsData transactionsData =
                srvIntCards.listCardTransactions(
                        listCardTransactionsV0Mapper.mapInput(
                                true,
                                pathParams.get(CARD_ID).toString(),
                                (String) queryParams.get(FROM_OPERATION_DATE),
                                (String) queryParams.get(TO_OPERATION_DATE),
                                (String) queryParams.get(PAGINATION_KEY),
                                (Long) queryParams.get(PAGE_SIZE),
                                RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS));

        Pagination pagination = null;
        if (transactionsData != null && transactionsData.getPagination() != null) {
            pagination = GabiPaginationMapper.perform(businessToolKit.getPaginationBuider()
                    .setPagination(SrvCardsV0.class, "listCardTransactions", uriInfo,
                            transactionsData.getPagination().getNextPage(), null,
                            transactionsData.getPagination().getPageSize(), null,
                            null, null, null).build());
        }

        ServiceResponse<List<Transaction>> serviceResponse = listCardTransactionsV0Mapper.mapOut(transactionsData, pagination);

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS, serviceResponse, pathParams, queryParams);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PUT
    @Path("/cards/{card-id}/limits/{limit-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_CARD_LIMIT, logicalID = "modifyCardLimit")
    public ServiceResponse<Limit> modifyCardLimit(@PathParam(CARD_ID) final String cardId,
                                                  @PathParam(LIMIT_ID) final String limitId,
                                                  final Limit limit) {
        LOG.info("----- Invoking service modifyCardLimit -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(LIMIT_ID, limitId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CARD_LIMIT, limit, pathParams, null);

        ServiceResponse<Limit> serviceResponse = modifyCardLimitMapper.mapOut(
                srvIntCardsV0.modifyCardLimit(
                        modifyCardLimitMapper.mapIn((String) pathParams.get(CARD_ID), (String) pathParams.get(LIMIT_ID), limit)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_CARD_LIMIT, serviceResponse, pathParams, null);

        return serviceResponse;
    }

    @Override
    @GET
    @Path("/cards/{card-id}/transactions/{transaction-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CARD_TRANSACTION, logicalID = "getCardTransaction")
    public ServiceResponse<Transaction> getCardTransaction(@PathParam(CARD_ID) final String cardId,
                                                           @PathParam(TRANSACTION_ID) final String transactionId) {
        LOG.info("----- Invoking service getCardTransaction -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(TRANSACTION_ID, transactionId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_TRANSACTION, null, pathParams, null);

        ServiceResponse<Transaction> serviceResponse =
                transactionMapper.mapOut(srvIntCardsV0.getCardTransaction(transactionMapper.mapIn(
                        pathParams.get(CARD_ID).toString(), pathParams.get(TRANSACTION_ID).toString())));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_TRANSACTION, serviceResponse, pathParams, null);

        return serviceResponse;
    }

    @Override
    @POST
    @Path("/cards")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_CARD_V0, logicalID = "createCard", forcedCatalog = "asoCatalog")
    public CardData createCard(Card card) {
        LOG.info("----- Invoking service createCard -----");

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_V0, card, null, null);

        CardData data = createCardMapper.mapOut(srvIntCardsV0.createCard(createCardMapper.mapIn(card)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_V0, data, null, null);

        return data;
    }

    @Override
    @GET
    @Path("/cards/{card-id}/installments-plans")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_INSTALLMENT_PLANS, logicalID = "listInstallmentPlans", forcedCatalog = "asoCatalog")
    public ServiceResponse<List<InstallmentsPlan>> listInstallmentPlans(
            @PathParam(CARD_ID) final String cardId,
            @QueryParam(PAGINATION_KEY) final String paginationKey,
            @QueryParam(PAGE_SIZE) final Long pageSize) {

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(PAGINATION_KEY, paginationKey);
        queryParams.put(PAGE_SIZE, pageSize);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_INSTALLMENT_PLANS, null, pathParams, queryParams);

        final DTOInstallmentsPlanList dtoInstallmentsPlanList = srvIntCardsV0.listInstallmentPlans(
                plansMapper.mapIn((String) pathParams.get(CARD_ID), (String) queryParams.get(PAGINATION_KEY), (Long) queryParams.get(PAGE_SIZE)));

        Pagination pagination = null;
        if (dtoInstallmentsPlanList != null && dtoInstallmentsPlanList.getPagination() != null) {
            pagination = GabiPaginationMapper.perform(businessServicesToolkitManager.getPaginationBuilder()
                    .setPagination(SrvCardsV0.class, "listInstallmentPlans", uriInfo,
                            dtoInstallmentsPlanList.getPagination().getPaginationKey(), null,
                            dtoInstallmentsPlanList.getPagination().getPageSize(), null,
                            null, null, null).build());
        }

        ServiceResponse<List<InstallmentsPlan>> serviceResponse = plansMapper.mapOut(dtoInstallmentsPlanList, pagination);

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_INSTALLMENT_PLANS, serviceResponse, pathParams, queryParams);

        return serviceResponse;
    }

    @Override
    @POST
    @Path("/cards/{card-id}/installments-plans")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN, logicalID = "createCardInstallmentsPlan", forcedCatalog = "asoCatalog")
    public Response createCardInstallmentsPlan(@PathParam(CARD_ID) final String cardId,
                                               final InstallmentsPlan installmentsPlan) {
        LOG.info("... called method SrvCardsV0.createCardInstallmentsPlan ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN, installmentsPlan, pathParams, null);

        InstallmentsPlanData data = srvIntCardsV0.createCardInstallmentsPlan(
                createCardInstallmentsPlanMapper.mapIn((String) pathParams.get(CARD_ID), installmentsPlan));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN, data, pathParams, null);

        return createCardInstallmentsPlanMapper.mapOut(data, uriInfo);
    }

    @Override
    @PATCH
    @Path("/cards/{card-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD, logicalID = "modifyCard")
    public Response modifyCard(@PathParam(CARD_ID) final String cardId,
                               Card card) {
        LOG.info("... called method SrvCardsV0.modifyCard ...");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARD_INSTALLMENTS_PLAN, card, pathParams, null);

        srvIntCardsV0.modifyCard(modifyCardV0Mapper.mapIn((String) pathParams.get(CARD_ID), card));

        return Response.ok().build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/cards/{card-id}/conditions")
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CARD_CONDITIONS, logicalID = "getCardConditions", forcedCatalog = "asoCatalog")
    public Conditions getCardConditions(@DatoAuditable(omitir = true) @PathParam(CARD_ID) final String cardId) {
        LOG.info("----- Invoking service getCardConditions -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_CONDITIONS, null, pathParams, null);

        Conditions data = getCardConditionsMapper.mapOut(
                srvIntCardsV0.getCardConditions(
                        getCardConditionsMapper.mapIn((String) pathParams.get(CARD_ID))));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_CONDITIONS, data, pathParams, null);

        return data;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/cards/{card-id}/financial-statements")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_CARD_FINANCIAL_STATEMENTS, logicalID = "listCardFinancialStatements")
    public StatementList listCardFinancialStatements(
            @PathParam(CARD_ID) final String cardId,
            @QueryParam(IS_ACTIVE) final Boolean isActive,
            @QueryParam(PAGINATION_KEY) final String paginationKey,
            @QueryParam(PAGE_SIZE) final Integer pageSize) {

        LOG.info("----- Invoking service listCardFinancialStatements -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(IS_ACTIVE, isActive);
        queryParams.put(PAGINATION_KEY, paginationKey);
        queryParams.put(PAGE_SIZE, pageSize);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARD_FINANCIAL_STATEMENTS, null, pathParams, queryParams);

        DTOIntStatementList dtoIntStatementList = srvIntCardsV0
                .listCardFinancialStatements(listCardFinancialStatementsMapper
                        .mapIn((String) pathParams.get(CARD_ID), (Boolean) queryParams.get(IS_ACTIVE),
                                (String) queryParams.get(PAGINATION_KEY), (Integer) queryParams.get(PAGE_SIZE)));

        StatementList response = listCardFinancialStatementsMapper.mapOut(dtoIntStatementList);

        if (response == null) {
            return null;
        }
        if (dtoIntStatementList.getPagination() == null) {
            return response;
        }
        response.setPagination(PaginationMapper.build(businessToolKit.getPaginationBuider()
                .setPagination(SrvCardsV0.class, "listCardFinancialStatements", uriInfo,
                        dtoIntStatementList.getPagination().getPaginationKey(), null,
                        dtoIntStatementList.getPagination().getPageSize(),
                        null, null, null, null).build()));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARD_FINANCIAL_STATEMENTS, response, pathParams, null);

        return response;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/cards/{card-id}/financial-statements/{financial-statement-id}")
    @Produces("multipart/mixed")
    @MultipartDescription({
            @PartDescription(value = "rootPart", type = MediaType.APPLICATION_JSON, javaType = Document.class),
            @PartDescription(value = "attachmentPart")
    })
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT, logicalID = "getCardFinancialStatement")
    public MultipartBody getCardFinancialStatement(
            @PathParam(CARD_ID) @DatoAuditable String cardId,
            @PathParam(FINANCIAL_STATEMENT_ID) String financialStatementId) {
        LOG.info("----- Invoking service getCardFinancialStatement -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(FINANCIAL_STATEMENT_ID, financialStatementId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT, null, pathParams, null);

        final InputGetCardFinancialStatement inputGetCardFinancialStatement = getCardFinancialStatementMapper.mapIn(
                (String) pathParams.get(CARD_ID), (String) pathParams.get(FINANCIAL_STATEMENT_ID), inputHeaderManager.getHeader(CONTENT_TYPE));

        final DocumentRequest documentRequest = getCardFinancialStatementMapper.mapOut(
                srvIntCardsV0.getCardFinancialStatement(inputGetCardFinancialStatement));

        Document data = srvIntCardsV0.getCardFinancialStatementDocument(documentRequest);

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT, data, pathParams, null);

        return getCardFinancialStatementDocumentMapper.mapOut(data, inputGetCardFinancialStatement.getContentType());
    }

    @POST
    @Path("/cards/{card-id}/masked-tokens")
    @SMC(registryID = "SMCPE1920056", logicalID = "createCardsMaskedToken")
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public ServiceResponse<MaskedToken> createCardsMaskedToken(
            @DatoAuditable(omitir = true) @PathParam("card-id") final String cardId,
            final MaskedToken maskedToken) {
        LOG.info("----- Invoking service createCardsMaskedToken -----");
        ServiceResponse<MaskedToken> maskedTokenData = createCardsMaskedTokenMapper.mapOut(
                srvIntCardsV0.createCardsMaskedToken(
                        createCardsMaskedTokenMapper.mapIn(cardId, maskedToken)));

        if (maskedTokenData != null && maskedTokenData.getData() != null && maskedTokenData.getData().getId() != null) {
            outputHeaderManager.setLocationHeader(maskedTokenData.getData().getId());
        }

        return maskedTokenData;
    }

    //REQ-005778: Servicio para Contact Center para mostrar el Pasajero Frecuente de Lifemiles
    @GET
    @Path("/cards/{card-id}/memberships/{membership-id}")
    @SMC(registryID = "SMCPE1810269", logicalID = "getCardMembership")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public ServiceResponse<Membership> getCardMembership(
            @DatoAuditable(omitir = true)
            @PathParam("card-id") String cardId,
            @PathParam("membership-id") String membershipId) {
        LOG.info("----- Invoking service getCardMembership -----");
        return getCardMembershipMapper.mapOutput(srvIntCardsV0.getCardMembership(getCardMembershipMapper.mapInput(cardId, membershipId)));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/cards/{card-id}/offers/{offer-id}")
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_CARDS_CARD_OFFER, logicalID = "getCardsCardOffer", forcedCatalog = "asoCatalog")
    public ServiceResponse<Offer> getCardsCardOffer(
            @PathParam(CARD_ID) final String cardId,
            @PathParam(OFFER_ID) final String offerId) {
        LOG.info("----- Invoking service getCardsCardOffer -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);
        pathParams.put(OFFER_ID, offerId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARDS_CARD_OFFER, null, pathParams, null);

        ServiceResponse<Offer> serviceResponse = getCardsCardOfferMapper.mapOut(
                srvIntCardsV0.getCardsCardOffer(
                        getCardsCardOfferMapper.mapIn((String) pathParams.get(CARD_ID), (String) pathParams.get(OFFER_ID))));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_CARDS_CARD_OFFER, serviceResponse, pathParams, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/offers/{offer-id}/simulate")
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_CARDS_OFFER_SIMULATE, logicalID = "createCardsOfferSimulate", forcedCatalog = "asoCatalog")
    public ServiceResponseOK<HolderSimulation> createCardsOfferSimulate(@PathParam(OFFER_ID) final String offerId,
                                                                        final HolderSimulation holderSimulation) {

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(OFFER_ID, offerId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARDS_OFFER_SIMULATE, holderSimulation, pathParams, null);

        ServiceResponseOK<HolderSimulation> serviceResponseOK = createCardsOfferSimulateMapper.mapOut(
                srvIntCardsV0.createCardsOfferSimulate(
                        createCardsOfferSimulateMapper.mapIn((String) pathParams.get(OFFER_ID), holderSimulation)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARDS_OFFER_SIMULATE, serviceResponseOK, pathParams, null);

        return serviceResponseOK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/proposals")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL, logicalID = "createCardsProposal", forcedCatalog = "asoCatalog")
    public Response createCardsProposal(final Proposal proposal) {
        LOG.info("----- Invoking service createCardsProposal -----");

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL, proposal, null, null);

        ProposalData proposalData = createCardsProposalMapper.mapOut(srvIntCardsV0.createCardsProposal(createCardsProposalMapper.mapIn(proposal)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL, proposalData, null, null);

        return Response.status(Response.Status.CREATED).entity(proposalData).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("cards/{card-id}/related-contracts")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_CARD_RELATED_CONTRACT, logicalID = "listCardRelatedContracts")
    @Consumes(MediaType.APPLICATION_JSON)
    public ServiceResponse<List<RelatedContracts>> listCardRelatedContracts(
            @DatoAuditable(omitir = true) @PathParam(CARD_ID) final String cardId) {
        LOG.info("----- Invoking service listCardRelatedContracts -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARD_RELATED_CONTRACT, null, pathParams, null);

        ServiceResponse<List<RelatedContracts>> data = listCardRelatedContractsMapper.mapOut(
                srvIntCardsV0.listCardRelatedContracts(
                        listCardRelatedContractsMapper.mapIn(pathParams.get(CARD_ID).toString())));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_CARD_RELATED_CONTRACT, data, pathParams, null);

        return data;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/cards/{card-id}/cash-advances")
    @SMC(registryID = "SMCPE1810367", logicalID = "listCardCashAdvances")
    @Consumes(MediaType.APPLICATION_JSON)
    public ServiceResponse<List<CashAdvances>> listCardCashAdvances(
            @DatoAuditable(omitir = true)
            @PathParam(CARD_ID) final String cardId) {
        LOG.info("----- Invoking service listCardCashAdvances -----");
        return listCardCashAdvancesMapper.mapOutput(
                srvIntCardsV0.listCardCashAdvances(
                        listCardCashAdvancesMapper.mapInput(cardId)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/cards/{card-id}/proposals")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL, logicalID = "createCardsCardProposal", forcedCatalog = "asoCatalog")
    public Response createCardsCardProposal(
            @PathParam(CARD_ID) final String cardId,
            final CardProposal cardProposal) {
        LOG.info("----- Invoking service createCardsCardProposal -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL, cardProposal, pathParams, null);

        CardProposal data = srvIntCards.createCardsCardProposal(
                createCardsCardProposalMapper.mapIn((String) pathParams.get(CARD_ID), cardProposal));

        if (data == null) {
            return null;
        }

        outputHeaderManager.setLocationHeader();


        CardProposalData serviceResponse = new CardProposalData();
        serviceResponse.setData(data);

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL, serviceResponse, null, null);

        return createCardsCardProposalMapper.mapOut(serviceResponse);
    }

    @Override
    @POST
    @Path("/cards/{card-id}/cash-refunds")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = "SMCPE1810366", logicalID = "createCardCashRefund")
    public ServiceResponse<CashRefund> createCardCashRefund(
            @PathParam(CARD_ID) final String cardId,
            final CashRefund cashRefund) {
        LOG.info("----- Invoking service createCardCashRefund -----");
        return createCardCashRefundMapper.mapOutput(
                srvIntCardsV0.createCardCashRefund(
                        createCardCashRefundMapper.mapInput(cardId, cashRefund)));
    }

    @Override
    @POST
    @Path("/cards/{card-id}/card-reports")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_REPORTS, logicalID = "createCardReports")
    public ServiceResponseNoContent createCardReports(@DatoAuditable(omitir = true) @PathParam(CARD_ID) final String cardId,
                                                      final ReportCardCreation reportCardCreation) {
        LOG.info("----- Invoking service createCardReports -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(CARD_ID, cardId);

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_REPORTS, reportCardCreation, pathParams, null);

        srvIntCardsV0.createCardReports(
                createCardReportsV0Mapper.mapIn(
                        (String) pathParams.get(CARD_ID),
                        reportCardCreation
                )
        );

        return ServiceResponseNoContent.ServiceResponseNoContentBuilder.build();
    }

    @Override
    @POST
    @Path("/offers/generate")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = RegistryIds.SMC_REGISTRY_ID_OF_CREATE_OFFERS_GENERATE_CARDS, logicalID = "createOffersGenerateCards", forcedCatalog = "gabiCatalog")
    public ServiceResponseOK<OfferGenerate> createOffersGenerateCards(final OfferGenerate offerGenerate) {
        LOG.info("----- Invoking service createOffersGenerateCards -----");

        Map<String, Object> inputHeaders = new HashMap<>();
        inputHeaders.put(HEADER_BCS_DEVICE_SCREEN_SIZE, inputHeaderManager.getHeader(HEADER_BCS_DEVICE_SCREEN_SIZE));

        inputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_OFFERS_GENERATE_CARDS, offerGenerate, null, null, inputHeaders, null);

        ServiceResponseOK<OfferGenerate> serviceResponse = createOffersGenerateCardsMapper.mapOut(
                srvIntCardsV0.createOffersGenerateCards(
                        createOffersGenerateCardsMapper.mapIn(offerGenerate,
                                (String) inputHeaders.get(HEADER_BCS_DEVICE_SCREEN_SIZE))));

        outputDataProcessingExecutor.perform(RegistryIds.SMC_REGISTRY_ID_OF_CREATE_OFFERS_GENERATE_CARDS, serviceResponse, null, null);

        return serviceResponse;
    }
}

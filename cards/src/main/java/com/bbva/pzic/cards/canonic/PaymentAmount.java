package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "PaymentAmount", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "PaymentAmount", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentAmount implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Payment amount identifier.
     */
    private String id;
    /**
     * Payment amount name.
     */
    private String name;
    /**
     * Payment amount value. This amount may be provided in several currencies
     * (depending on the country).
     */
    private List<Value> values;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }
}
package com.bbva.pzic.cards.facade.v01.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.CardsList;
import com.bbva.pzic.cards.canonic.Image;
import com.bbva.pzic.cards.canonic.RelatedContract;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v01.mapper.IListCardsMapper;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.mappers.MapperUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 03/02/2017.
 *
 * @author Entelgy
 */
@Mapper
public class ListCardsMapper implements IListCardsMapper {

    private static final Log LOG = LogFactory.getLog(ListCardsMapper.class);

    @Autowired
    private MapperUtils mapperUtils;

    @Autowired
    private AbstractCypherTool cypherTool;

    @Autowired
    private ConfigurationManager configurationManager;

    /**
     * @see com.bbva.pzic.cards.facade.v01.mapper.IListCardsMapper#mapIn(String, java.util.List, String, java.util.List, String, Integer)
     */
    @Override
    public DTOIntListCards mapIn(final String customerId, final List<String> cardTypeId, final String physicalSupportId,
                                 final List<String> statusId, final String paginationKey, final Integer pageSize) {

        LOG.info("... called method DTOIntListCards.mapIn ...");
        DTOIntListCards dtoIntListCards = new DTOIntListCards();
        dtoIntListCards.setCustomerId(customerId);
        dtoIntListCards.setCardTypeId(cardTypeId.isEmpty() ? null : cardTypeId);
        dtoIntListCards.setPhysicalSupportId(physicalSupportId);
        dtoIntListCards.setStatusId(statusId.isEmpty() ? null : statusId);
        dtoIntListCards.setPaginationKey(paginationKey);
        dtoIntListCards.setPageSize(pageSize);
        return dtoIntListCards;
    }

    /**
     * @see com.bbva.pzic.cards.facade.v01.mapper.IListCardsMapper#mapOut(com.bbva.pzic.cards.business.dto.DTOOutListCards, String)
     */
    @Override
    public CardsList mapOut(final DTOOutListCards listCards, final String userAgent) {
        if (listCards == null || listCards.getData() == null || listCards.getData().isEmpty()) {
            return null;
        }
        CardsList cardsList = new CardsList();
        cardsList.setData(listCards.getData());
        if (cardsList.getData() != null) {
            encryptRelatedContractNumbers(cardsList.getData(), userAgent);
        }
        return cardsList;
    }

    private void encryptRelatedContractNumbers(List<Card> cards, String userAgent) {
        for (final Card card : cards) {
            card.setCardId(cypherTool.encrypt(card.getNumber(), AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARDS_V01));
            if (card.getRelatedContracts() != null) {
                for (final RelatedContract relatedContract : card.getRelatedContracts()) {
                    if (relatedContract.getNumber() != null) {
                        relatedContract.setContractId(cypherTool.encrypt(relatedContract.getNumber(), AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARDS_V01));
                        relatedContract.setRelatedContractId(cypherTool.encrypt(relatedContract.getRelatedContractId(), AbstractCypherTool.IDCOREL, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARDS_V01));
                    }
                }
            }
            card.setImages(imageList(card, userAgent));
        }
    }

    private List<Image> imageList(Card card, String userAgent) {
        if (card.getImages() == null) {
            return null;
        }
        List<Image> images = new ArrayList<>();
        for (Image image : card.getImages()) {
            image.setUrl(mapperUtils.buildUrl(configurationManager.getProperty(Constants.PROPERTY_IMAGE_WALLET_HOSTNAME),
                    userAgent, image.getUrl()));
            images.add(image);
        }
        return images;
    }
}


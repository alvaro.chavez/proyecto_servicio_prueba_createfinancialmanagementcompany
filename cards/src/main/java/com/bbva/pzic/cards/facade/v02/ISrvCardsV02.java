package com.bbva.pzic.cards.facade.v02;

import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.CardData;

/**
 * @author Entelgy
 */
public interface ISrvCardsV02 {

    /**
     * Method for creating a new card related to a specific user.
     *
     * @param card payload
     * @return the id of card
     */
    CardData createCard(Card card);
}
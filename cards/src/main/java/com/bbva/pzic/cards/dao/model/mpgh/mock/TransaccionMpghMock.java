package com.bbva.pzic.cards.dao.model.mpgh.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpgh.*;
import com.bbva.pzic.cards.dao.rest.mock.RestDigitizeDocumentFileMock;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 9/02/2018.
 *
 * @author Entelgy
 */
@Component("transaccionMpgh")
public class TransaccionMpghMock implements InvocadorTransaccion<PeticionTransaccionMpgh, RespuestaTransaccionMpgh> {

    public static final String TEST_EMPTY = "6666";
    public static final String TEST_NO_RESPONSE = "9999";
    public static final String TEST_VALIDATE_MANDATORY_PARTICIPANT_NAME_1 = "1111";
    public static final String TEST_VALIDATE_MANDATORY_PARAMS = "2222";
    public static final String TEST_PV_RESPONSE = "9998";
    public static final String TEST_PERSONA_EMPRESARIAL_RESPONSE = "9997";
    public static final String TEST_PERSONA_NATURAL_MANDATORY_RESPONSE = "9996";

    @Override
    public RespuestaTransaccionMpgh invocar(PeticionTransaccionMpgh peticion) throws ExcepcionTransaccion {
        RespuestaTransaccionMpgh response = new RespuestaTransaccionMpgh();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPME1GH format = peticion.getCuerpo().getParte(FormatoMPME1GH.class);
        final String numberCard = format.getIdetarj();

        if (TEST_NO_RESPONSE.equals(numberCard)) {
            return response;
        }
        try {
            if (TEST_EMPTY.equals(numberCard)) {
                response.getCuerpo().getPartes().add(buildData1(FormatsMPGHMock.INSTANCE.getFormatoMPMS1GHEmpty()));
                return response;
            }

            FormatoMPMS1GH formatoMPMS1GH = FormatsMPGHMock.INSTANCE.getFormatoMPMS1GHNaturalLifeMilles();
            if (TEST_PV_RESPONSE.equals(numberCard)) {
                formatoMPMS1GH = FormatsMPGHMock.INSTANCE.getFormatoMPMS1GHNaturalPuntosVida();

            } else if (TEST_PERSONA_EMPRESARIAL_RESPONSE.equals(numberCard)) {
                formatoMPMS1GH = FormatsMPGHMock.INSTANCE.getFormatoMPMS1GHEnterprise();

            } else if (TEST_PERSONA_NATURAL_MANDATORY_RESPONSE.equals(numberCard)) {
                formatoMPMS1GH = FormatsMPGHMock.INSTANCE.getFormatoMPMS1GHNaturalMandatories();

            } else if (RestDigitizeDocumentFileMock.ERROR_RESPONSE.equals(numberCard) ||
                    RestDigitizeDocumentFileMock.TEMPLATE_NOT_FOUND_RESPONSE.equals(numberCard) ||
                    RestDigitizeDocumentFileMock.WRONG_PARAMETERS_RESPONSE.equals(numberCard)) {
                formatoMPMS1GH.setNumtarj(numberCard);

            } else if (TEST_VALIDATE_MANDATORY_PARTICIPANT_NAME_1.equals(numberCard)) {
                formatoMPMS1GH = FormatsMPGHMock.INSTANCE.getFormatoMPMS1GHValidateMandatoryParams();
                formatoMPMS1GH.setTitptar(null);

            } else if (TEST_VALIDATE_MANDATORY_PARAMS.equals(numberCard)) {
                formatoMPMS1GH = FormatsMPGHMock.INSTANCE.getFormatoMPMS1GHValidateMandatoryParams();
                formatoMPMS1GH.setTitstar(null);
            }

            response.getCuerpo().getPartes().add(buildData1(formatoMPMS1GH));

            response.getCuerpo().getPartes().addAll(buildData2(FormatsMPGHMock.INSTANCE.getFormatoMPMS2GH()));
            response.getCuerpo().getPartes().addAll(buildData3(FormatsMPGHMock.INSTANCE.getFormatoMPMS3GH()));
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }

        return response;
    }

    @Override
    public RespuestaTransaccionMpgh invocarCache(PeticionTransaccionMpgh peticion) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida buildData1(FormatoMPMS1GH formatoMPMS1GH) {
        CopySalida copy = new CopySalida();
        copy.setCopy(formatoMPMS1GH);
        return copy;
    }

    private List<CopySalida> buildData2(List<FormatoMPMS2GH> formats) {
        List<CopySalida> copySalidaList = new ArrayList<>();
        for (FormatoMPMS2GH f : formats) {
            CopySalida copySalida = new CopySalida();
            copySalida.setCopy(f);
            copySalidaList.add(copySalida);
        }
        return copySalidaList;
    }

    private List<CopySalida> buildData3(List<FormatoMPMS3GH> formats) {
        List<CopySalida> copySalidaList = new ArrayList<>();
        for (FormatoMPMS3GH f : formats) {
            CopySalida copySalida = new CopySalida();
            copySalida.setCopy(f);
            copySalidaList.add(copySalida);
        }
        return copySalidaList;
    }
}

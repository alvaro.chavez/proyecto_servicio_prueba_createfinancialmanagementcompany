// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.phiat021_1;

import com.bbva.pzic.cards.dao.model.phiat021_1.Ratetype;

privileged aspect Ratetype_Roo_JavaBean {
    
    /**
     * Gets id value
     * 
     * @return String
     */
    public String Ratetype.getId() {
        return this.id;
    }
    
    /**
     * Sets id value
     * 
     * @param id
     * @return Ratetype
     */
    public Ratetype Ratetype.setId(String id) {
        this.id = id;
        return this;
    }
    
}

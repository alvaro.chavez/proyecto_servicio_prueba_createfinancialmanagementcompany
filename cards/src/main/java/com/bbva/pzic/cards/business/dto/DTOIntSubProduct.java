package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntSubProduct {
    @NotNull(groups = {ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.CreateCardReportsV0.class})
    @Size(max = 6, groups = ValidationGroup.CreateCardsProposal.class)
    private String id;

    private String name;

    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
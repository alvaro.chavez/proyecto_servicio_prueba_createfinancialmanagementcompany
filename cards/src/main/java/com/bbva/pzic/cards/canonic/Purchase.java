package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "Purchase", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Purchase", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Purchase extends TransactionTypeAgreement implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Human readable information about transaction number.
     */
    private String authorizationNumber;
    /**
     * Store information where purchase took place.
     */
    private Store store;
    /**
     * Payment channel that customer used when purchasing.
     */
    private PaymentChannel paymentChannel;
    /**
     * Contract that is linked to the contrat which is being queried on
     * transaction. It occurs when the purchase is performed using a debit card
     * that is related to an account. According to this when customers are
     * performing a query to search transations from a debit card, in this
     * attribute will be placed information about the account that back up all
     * opertations that are performed using that card. In the case of quering on
     * an account, the purchase trasactions will have this attribute informed
     * only when the purchase has been performed using a debit card.
     */
    private LinkedContract linkedContract;
    /**
     * It represents reward points earned every time a credit card is used. They
     * can be redeemed for the total amount of the purchase or discounts over
     * the purchase on commercial businesses.
     */
    private Points points;
    /**
     * Number of total installments associated with the movement.
     */
    private BigDecimal totalInstallmentsNumber;
    /**
     * Interest rate associated with the movement.
     */
    private String interestRate;

    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public PaymentChannel getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(PaymentChannel paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public LinkedContract getLinkedContract() {
        return linkedContract;
    }

    public void setLinkedContract(LinkedContract linkedContract) {
        this.linkedContract = linkedContract;
    }

    public Points getPoints() {
        return points;
    }

    public void setPoints(Points points) {
        this.points = points;
    }

    public BigDecimal getTotalInstallmentsNumber() {
        return totalInstallmentsNumber;
    }

    public void setTotalInstallmentsNumber(BigDecimal totalInstallmentsNumber) {
        this.totalInstallmentsNumber = totalInstallmentsNumber;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }
}
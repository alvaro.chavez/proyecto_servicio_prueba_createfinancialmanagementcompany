package com.bbva.pzic.cards.dao.model.awsimages;

/**
 * Created on 30/07/2019.
 *
 * @author Entelgy
 */
public class ModelImage {

    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

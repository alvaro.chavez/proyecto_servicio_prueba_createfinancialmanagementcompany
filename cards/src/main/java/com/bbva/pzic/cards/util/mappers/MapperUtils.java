package com.bbva.pzic.cards.util.mappers;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.pzic.cards.util.orika.converter.builtin.BooleanToStringConverter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 12/12/2016.
 *
 * @author Entelgy
 */
@Component
public class MapperUtils {

    private static final Log LOG = LogFactory.getLog(MapperUtils.class);

    private static final String IMAGE_WALLET_HOSTNAME = "servicing.imagen.wallet.hostname";

    private String url;

    private BooleanToStringConverter stringNumberConverter;

    @Autowired
    private ConfigurationManager configurationManager;

    @PostConstruct
    public void init(){
        stringNumberConverter = new BooleanToStringConverter();
    }

    public String convertBooleanToString(Boolean bool) {
        return stringNumberConverter.convertTo(bool, null);
    }

    public String buildUrl(final String userAgent, final String hostData) {
        if (this.url == null) {
            this.url = configurationManager.getProperty(IMAGE_WALLET_HOSTNAME);
        }

        String width = "";
        String height = "";
        try {
            LOG.info("Obteniendo los valores del header: " + userAgent);
            if (userAgent != null) {
                String[] dimensions = userAgent.split(";")[4].split("x");
                width = dimensions[0];
                height = dimensions[1];
            }
        } catch (IndexOutOfBoundsException e) {
            LOG.error("Error al obtener el ancho y alto para la imagen");
        }

        boolean acceptDimension = true;
        if (width.isEmpty() || height.isEmpty()) {
            acceptDimension = false;
        }

        List<NameValuePair> parameters = URLEncodedUtils.parse(hostData, Charset.forName("UTF-8"));

        List<NameValuePair> newParameters = new ArrayList<>();
        for (NameValuePair nameValuePair : parameters) {
            if ("width".equalsIgnoreCase(nameValuePair.getName())) {
                if (acceptDimension) {
                    newParameters.add(new BasicNameValuePair(nameValuePair.getName(), width));
                }
            } else if ("height".equalsIgnoreCase(nameValuePair.getName())) {
                if (acceptDimension) {
                    newParameters.add(new BasicNameValuePair(nameValuePair.getName(), height));
                }
            } else {
                newParameters.add(nameValuePair);
            }
        }

        return url.concat("?").concat(URLEncodedUtils.format(newParameters, "UTF-8"));
    }

    public Boolean convertStringToBoolean(String value) {
        return stringNumberConverter.convertFrom(value,null);
    }


    public String buildUrl(final String url, final String header, final String hostData) {
        LOG.debug(String.format("Processing url [%s] with header value [%s]", url, header));
        String width = "";
        String height = "";
        try {
            if (header != null) {
                String[] headerPositions = header.toLowerCase().split(";");
                int index = 4;
                if (headerPositions.length == 1) {
                    index = 0;
                }
                String[] dimensions = headerPositions[index].split("x");
                width = dimensions[0];
                height = dimensions[1];
            }
        } catch (IndexOutOfBoundsException e) {
            LOG.error("Error al obtener el ancho y alto para la imagen", e);
        }

        boolean acceptDimension = true;
        if (width.isEmpty() || height.isEmpty()) {
            acceptDimension = false;
        }

        List<NameValuePair> parameters = URLEncodedUtils.parse(hostData, Charset.forName("UTF-8"));

        List<NameValuePair> newParameters = new ArrayList<>();
        for (NameValuePair nameValuePair : parameters) {
            if ("width".equalsIgnoreCase(nameValuePair.getName())) {
                if (acceptDimension) {
                    newParameters.add(new BasicNameValuePair(nameValuePair.getName(), width));
                }
            } else if ("height".equalsIgnoreCase(nameValuePair.getName())) {
                if (acceptDimension) {
                    newParameters.add(new BasicNameValuePair(nameValuePair.getName(), height));
                }
            } else {
                newParameters.add(nameValuePair);
            }
        }

        String s;
        if (url != null) {
            s = url;
            if (!s.endsWith("?")) {
                s = s.concat("?");
            }

        } else {
            s = new String();
        }
        s = s.concat(URLEncodedUtils.format(newParameters, "UTF-8"));
        LOG.debug(String.format("Processed url [%s]", s));
        return s;
    }
}

package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardCancellationVerification;
import com.bbva.pzic.cards.facade.v1.dto.CancellationVerification;
import com.bbva.pzic.cards.facade.v1.mapper.IGetCardCancellationVerificationV1Mapper;
import com.bbva.pzic.cards.util.mappers.Mapper;

@Mapper
public class GetCardCancellationVerificationV1Mapper implements IGetCardCancellationVerificationV1Mapper {

    @Override
    public InputGetCardCancellationVerification mapIn(final String cardId) {
        InputGetCardCancellationVerification dtoInt = new InputGetCardCancellationVerification();
        dtoInt.setCardId(cardId);
        return dtoInt;
    }

    @Override
    public ServiceResponse mapOut(final CancellationVerification cancellationVerification) {
        if (cancellationVerification == null) {
            return null;
        }
        return ServiceResponse.data(cancellationVerification).build();
    }
}

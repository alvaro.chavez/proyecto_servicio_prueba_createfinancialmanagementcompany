package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputListCardProposals;
import com.bbva.pzic.cards.dao.apx.mapper.IApxListCardProposalsMapper;
import com.bbva.pzic.cards.dao.model.ppcut004_1.PeticionTransaccionPpcut004_1;
import com.bbva.pzic.cards.dao.model.ppcut004_1.RespuestaTransaccionPpcut004_1;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
@Component("apxListCardProposals")
public class ApxListCardProposals {

    @Resource(name = "apxListCardProposalsMapper")
    private IApxListCardProposalsMapper mapper;

    @Resource(name = "transaccionPpcut004_1")
    private transient InvocadorTransaccion<PeticionTransaccionPpcut004_1, RespuestaTransaccionPpcut004_1> transaccion;

    public List<Proposal> invoke(final InputListCardProposals input) {
        PeticionTransaccionPpcut004_1 request = mapper.mapIn(input);
        RespuestaTransaccionPpcut004_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}

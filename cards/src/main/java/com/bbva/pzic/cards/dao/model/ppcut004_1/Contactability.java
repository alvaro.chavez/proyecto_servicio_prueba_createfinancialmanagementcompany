package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>contactability</code>, utilizado por la clase <code>Data</code></p>
 * 
 * @see Data
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Contactability {
	
	/**
	 * <p>Campo <code>reason</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "reason", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 80, signo = true)
	private String reason;
	
	/**
	 * <p>Campo <code>scheduleTimes</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "scheduleTimes", tipo = TipoCampo.DTO)
	private Scheduletimes scheduletimes;
	
}
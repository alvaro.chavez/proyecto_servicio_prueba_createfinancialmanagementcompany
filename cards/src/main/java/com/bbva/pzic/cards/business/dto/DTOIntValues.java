package com.bbva.pzic.cards.business.dto;

public class DTOIntValues {

    private DTOIntFactor factor;
    private String priceType;

    public DTOIntFactor getFactor() {
        return factor;
    }

    public void setFactor(DTOIntFactor factor) {
        this.factor = factor;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }
}

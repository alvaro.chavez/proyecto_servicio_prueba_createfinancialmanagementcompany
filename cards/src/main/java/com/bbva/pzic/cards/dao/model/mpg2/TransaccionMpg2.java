package com.bbva.pzic.cards.dao.model.mpg2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPG2</code>
 *
 * @see PeticionTransaccionMpg2
 * @see RespuestaTransaccionMpg2
 */
@Component("transaccionMpg2")
public class TransaccionMpg2 implements InvocadorTransaccion<PeticionTransaccionMpg2,RespuestaTransaccionMpg2> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpg2 invocar(PeticionTransaccionMpg2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpg2.class, RespuestaTransaccionMpg2.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpg2 invocarCache(PeticionTransaccionMpg2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpg2.class, RespuestaTransaccionMpg2.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}

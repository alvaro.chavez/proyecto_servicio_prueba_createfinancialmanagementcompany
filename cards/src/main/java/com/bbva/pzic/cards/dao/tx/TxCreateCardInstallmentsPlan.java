package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanData;
import com.bbva.pzic.cards.dao.model.mpwt.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardInstallmentsPlanMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 3/07/2017.
 *
 * @author Entelgy
 */
@Component("txCreateCardInstallmentsPlan")
public class TxCreateCardInstallmentsPlan
        extends DoubleOutputFormat<DTOIntCardInstallmentsPlan, FormatoMPM0TSE, InstallmentsPlanData, FormatoMPM0TSC, FormatoMPM0DET> {

    @Resource(name = "txCreateCardInstallmentsPlanMapper")
    private ITxCreateCardInstallmentsPlanMapper mapper;

    @Autowired
    public TxCreateCardInstallmentsPlan(@Qualifier("transaccionMpwt") InvocadorTransaccion<PeticionTransaccionMpwt, RespuestaTransaccionMpwt> transaction) {
        super(transaction, PeticionTransaccionMpwt::new, InstallmentsPlanData::new, FormatoMPM0TSC.class, FormatoMPM0DET.class);
    }

    @Override
    protected FormatoMPM0TSE mapInput(DTOIntCardInstallmentsPlan dtoIntCardInstallmentsPlan) {
        return mapper.mapIn(dtoIntCardInstallmentsPlan);
    }

    @Override
    protected InstallmentsPlanData mapFirstOutputFormat(FormatoMPM0TSC formatoMPM0TSC, DTOIntCardInstallmentsPlan dtoIntCardInstallmentsPlan, InstallmentsPlanData dtoOut) {
        return mapper.mapOut1(formatoMPM0TSC, dtoIntCardInstallmentsPlan);
    }

    @Override
    protected InstallmentsPlanData mapSecondOutputFormat(FormatoMPM0DET formatoMPM0DET, DTOIntCardInstallmentsPlan dtoIntCardInstallmentsPlan, InstallmentsPlanData dtoOut) {
        return mapper.mapOut2(formatoMPM0DET, dtoOut);
    }
}

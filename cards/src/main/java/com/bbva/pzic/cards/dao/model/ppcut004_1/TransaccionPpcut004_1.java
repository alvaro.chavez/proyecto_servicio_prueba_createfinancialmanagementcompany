package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>PPCUT004</code>
 * 
 * @see PeticionTransaccionPpcut004_1
 * @see RespuestaTransaccionPpcut004_1
 */
@Component
public class TransaccionPpcut004_1 implements InvocadorTransaccion<PeticionTransaccionPpcut004_1,RespuestaTransaccionPpcut004_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPpcut004_1 invocar(PeticionTransaccionPpcut004_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPpcut004_1.class, RespuestaTransaccionPpcut004_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPpcut004_1 invocarCache(PeticionTransaccionPpcut004_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPpcut004_1.class, RespuestaTransaccionPpcut004_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 12/12/2018.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "fees", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "fees", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Fees implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Detail of each fee.
     */
    private List<ItemizeFee> itemizeFees;

    public List<ItemizeFee> getItemizeFees() {
        return itemizeFees;
    }

    public void setItemizeFees(List<ItemizeFee> itemizeFees) {
        this.itemizeFees = itemizeFees;
    }
}

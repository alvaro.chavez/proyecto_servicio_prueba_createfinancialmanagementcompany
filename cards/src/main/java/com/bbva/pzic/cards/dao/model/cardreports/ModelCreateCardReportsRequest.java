package com.bbva.pzic.cards.dao.model.cardreports;

public class ModelCreateCardReportsRequest {

    private ModelCardReport data;

    private ModelSecurity security;

    public ModelCardReport getData() {
        return data;
    }

    public void setData(ModelCardReport data) {
        this.data = data;
    }

    public ModelSecurity getSecurity() {
        return security;
    }

    public void setSecurity(ModelSecurity security) {
        this.security = security;
    }
}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputModifyCardActivations;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMENG3;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMS1G3;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardActivationsMapperV0;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.converter.builtin.BooleanToStringConverter;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 10/10/2017.
 *
 * @author Entelgy
 */
@Mapper
public class TxModifyCardActivationsMapperV0 extends ConfigurableMapper implements ITxModifyCardActivationsMapperV0 {

    private static final Log LOG = LogFactory.getLog(TxModifyCardActivationsMapperV0.class);

    private static final String CARD_ACTIVATIONS_ENUM = "cards.activation.activationId";

    private Translator translator;
    private BooleanToStringConverter booleanToStringConverter;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        booleanToStringConverter = new BooleanToStringConverter();

        factory.getConverterFactory().registerConverter(booleanToStringConverter);

        factory.classMap(InputModifyCardActivations.class, FormatoMPMENG3.class)
                .field("cardId", "idetarj")
                .field("dtoIntActivationPosition1.activationId", "codacta")
                .field("dtoIntActivationPosition1.isActive", "invacta")

                .field("dtoIntActivationPosition2.activationId", "codactb")
                .field("dtoIntActivationPosition2.isActive", "invactb")

                .field("dtoIntActivationPosition3.activationId", "codactc")
                .field("dtoIntActivationPosition3.isActive", "invactc")

                .field("dtoIntActivationPosition4.activationId", "codactd")
                .field("dtoIntActivationPosition4.isActive", "invactd")

                .field("dtoIntActivationPosition5.activationId", "codacte")
                .field("dtoIntActivationPosition5.isActive", "invacte")
                .register();
    }

    @Override
    public FormatoMPMENG3 mapIn(final InputModifyCardActivations dtoIn) {
        LOG.info("... called method TxModifyCardActivationsMapperV0.mapIn ...");
        return map(dtoIn, FormatoMPMENG3.class);
    }

    @Override
    public List<Activation> mapOut(final FormatoMPMS1G3 formatoMPMS1G3) {
        LOG.info("... called method TxModifyCardActivationsMapperV0.mapOut ...");
        List<Activation> activations = new ArrayList<>();

        Activation activation = mapOutActivation(formatoMPMS1G3.getCodacta(), formatoMPMS1G3.getInvacta());
        if (activation != null) {
            activations.add(activation);
        }

        activation = mapOutActivation(formatoMPMS1G3.getCodactb(), formatoMPMS1G3.getInvactb());
        if (activation != null) {
            activations.add(activation);
        }

        activation = mapOutActivation(formatoMPMS1G3.getCodactc(), formatoMPMS1G3.getInvactc());
        if (activation != null) {
            activations.add(activation);
        }

        activation = mapOutActivation(formatoMPMS1G3.getCodactd(), formatoMPMS1G3.getInvactd());
        if (activation != null) {
            activations.add(activation);
        }

        activation = mapOutActivation(formatoMPMS1G3.getCodacte(), formatoMPMS1G3.getInvacte());
        if (activation != null) {
            activations.add(activation);
        }
        return activations;
    }

    private Activation mapOutActivation(final String activationId, final String isActive) {
        if (activationId == null && isActive == null) {
            return null;
        }
        Activation activation = new Activation();
        activation.setActivationId(translator.translateBackendEnumValueStrictly(CARD_ACTIVATIONS_ENUM, activationId));
        activation.setIsActive(booleanToStringConverter.convertFrom(isActive, null));
        return activation;
    }

}

package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.cards.dao.model.mp6j.FormatoMPMEN6J;
import com.bbva.pzic.cards.dao.model.mp6j.FormatoMPMS16J;
import com.bbva.pzic.cards.dao.model.mp6j.PeticionTransaccionMp6j;
import com.bbva.pzic.cards.dao.model.mp6j.RespuestaTransaccionMp6j;
import com.bbva.pzic.cards.dao.tx.mapper.ITxReimburseCardTransactionTransactionRefundMapper;
import com.bbva.pzic.cards.facade.v1.dto.ReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.routine.commons.utils.host.templates.Tx;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;

@Tx("reimburseCardTransactionTransactionRefund")
public class TxReimburseCardTransactionTransactionRefund extends SingleOutputFormat<
        InputReimburseCardTransactionTransactionRefund, FormatoMPMEN6J,
        ReimburseCardTransactionTransactionRefund, FormatoMPMS16J> {

    @Resource(name = "txReimburseCardTransactionTransactionRefundMapper")
    private ITxReimburseCardTransactionTransactionRefundMapper mapper;

    @Autowired
    public TxReimburseCardTransactionTransactionRefund(@Qualifier("transaccionMp6j") InvocadorTransaccion<PeticionTransaccionMp6j, RespuestaTransaccionMp6j> transaction) {
        super(transaction, PeticionTransaccionMp6j::new, ReimburseCardTransactionTransactionRefund::new, FormatoMPMS16J.class);
    }

    @Override
    protected FormatoMPMEN6J mapInput(InputReimburseCardTransactionTransactionRefund inputReimburseCardTransactionTransactionRefund) {
        return mapper.mapIn(inputReimburseCardTransactionTransactionRefund);
    }

    @Override
    protected ReimburseCardTransactionTransactionRefund mapFirstOutputFormat(FormatoMPMS16J formatoMPMS16J, InputReimburseCardTransactionTransactionRefund inputReimburseCardTransactionTransactionRefund, ReimburseCardTransactionTransactionRefund reimburseCardTransactionTransactionRefund) {
        return mapper.mapOut(formatoMPMS16J);
    }
}

package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputGetCardsCardOffer;
import com.bbva.pzic.cards.canonic.Offer;
import com.bbva.pzic.cards.dao.model.mpg9.FormatoMPMENG9;
import com.bbva.pzic.cards.dao.model.mpg9.FormatoMPMS1G9;

/**
 * Created on 09/11/2018.
 *
 * @author Entelgy
 */
public interface ITxGetCardsCardOfferMapper {

    FormatoMPMENG9 mapIn(InputGetCardsCardOffer input);

    Offer mapOut(FormatoMPMS1G9 input);

}

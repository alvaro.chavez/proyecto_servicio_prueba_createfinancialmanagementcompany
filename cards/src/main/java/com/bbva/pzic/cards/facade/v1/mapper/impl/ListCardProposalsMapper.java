package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputListCardProposals;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;
import com.bbva.pzic.cards.facade.v1.mapper.IListCardProposalsMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
@Component
public class ListCardProposalsMapper implements IListCardProposalsMapper {

    private static final Log LOG = LogFactory.getLog(ListCardProposalsMapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public InputListCardProposals mapIn(final String status) {
        LOG.info("... called method ListCardProposalsMapper.mapIn ...");
        InputListCardProposals input = new InputListCardProposals();
        input.setStatus(status);
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<List<Proposal>> mapOut(final List<Proposal> proposals) {
        LOG.info("... called method ListCardProposalsMapper.mapOut ...");
        if (CollectionUtils.isEmpty(proposals)) {
            return null;
        }
        return ServiceResponse.data(proposals).pagination(null).build();
    }
}

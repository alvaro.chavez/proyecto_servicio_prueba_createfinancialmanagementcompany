package com.bbva.pzic.cards.dao.rest;

import com.bbva.pzic.cards.business.dto.DTOIntDetailSimulation;
import com.bbva.pzic.cards.canonic.HolderSimulation;
import com.bbva.pzic.cards.dao.model.productofferdetail.CardHolderSimulationResponse;
import com.bbva.pzic.cards.dao.rest.mapper.IRestCreateCardsOfferSimulateMapper;
import com.bbva.pzic.cards.util.connection.rest.RestGetConnection;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_OFFER_SIMULATE;

/**
 * Created on 18/12/2018.
 *
 * @author Entelgy
 */
@Component
public class RestCreateCardsOfferSimulate extends RestGetConnection<CardHolderSimulationResponse> {

    private static final String URL_PROPERTY = "servicing.smc.configuration.SMCPE1810379.backend.url";
    private static final String URL_PROPERTY_PROXY = "servicing.smc.configuration.SMCPE1810379.backend.proxy";

    @Autowired
    private IRestCreateCardsOfferSimulateMapper mapper;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @PostConstruct
    public void init() {
        useProxy = Boolean.parseBoolean(translator.translate(URL_PROPERTY_PROXY, "false"));
    }

    public HolderSimulation perform(final DTOIntDetailSimulation input) {
        return mapper.mapOut(connect(URL_PROPERTY, mapper.mapInQueryParams(input)));
    }

    @Override
    protected void evaluateResponse(final CardHolderSimulationResponse response, final int statusCode) {
        evaluateMessagesResponse(response.getMessages(), SMC_REGISTRY_ID_OF_CREATE_CARDS_OFFER_SIMULATE, statusCode);
    }
}

package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.PaymentMethod;

import java.util.List;

/**
 * Created on 24/10/2017.
 *
 * @author Entelgy
 */
public interface IGetCardPaymentMethodsMapper {

    DTOIntCard mapIn(String cardId);

    ServiceResponse<List<PaymentMethod>> mapOut(PaymentMethod paymentMethod);
}

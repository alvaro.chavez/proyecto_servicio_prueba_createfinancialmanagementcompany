package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCardStatement;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatementDetail;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPME1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS2GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS3GH;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardFinancialStatementMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.converter.builtin.LongToIntegerConverter;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;

import java.util.ArrayList;

/**
 * Created on 8/02/2018.
 *
 * @author Entelgy
 */

@Mapper("txGetCardFinancialStatementMapper")
public class TxGetCardFinancialStatementMapper extends ConfigurableMapper implements ITxGetCardFinancialStatementMapper {

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.getConverterFactory().registerConverter(new LongToIntegerConverter());

        factory.classMap(InputGetCardFinancialStatement.class, FormatoMPME1GH.class)
                .field("cardId", "idetarj")
                .field("financialStatementId", "iddocta")
                .register();

        factory.classMap(FormatoMPMS1GH.class, DTOIntCardStatementDetail.class)
                .field("numcont", "cardId")
                .field("numtarj", "formatsPan")
                .field("tiptarj", "productName")
                .field("oficina", "branchName")
                .field("titptar", "participantNameFirst")
                .field("titstar", "participantNameSecond")
                .field("indtper", "participantType")
                .field("numcuep", "accountNumberPEN")
                .field("numcueu", "accountNumberUSD")
                .field("divcont", "currency")
                .field("numlife", "program.lifeMilesNumber")
                .field("puntmes", "program.monthPoints")
                .field("punacum", "program.totalPoints")
                .field("bonomes", "program.bonus")
                .field("feccier", "endDate")
                .field("pagcarg", "payType")
                .field("dirptit", "address.name")
                .field("dirctit", "address.streetType")
                .field("dirppro", "address.state")
                .field("dirpubi", "address.ubigeo")
                .field("lincred", "creditLimit")
                .field("creutil", "limits.disposedBalance")
                .field("credisp", "limits.availableBalance")
                .field("feculpa", "paymentDate")
                .field("teacomp", "interest.purchasePEN")
                .field("teacomu", "interest.purchaseUSD")
                .field("teaavap", "interest.advancedPEN")
                .field("teaavau", "interest.advancedUSD")
                .field("teacmpp", "interest.countervailingPEN")
                .field("teacmpu", "interest.countervailingUSD")
                .field("teamorp", "interest.arrearsPEN")
                .field("teamoru", "interest.arrearsUSD")
                .field("creutip", "disposedBalanceLocalCurrency.amount")
                .field("creutiu", "disposedBalance.amount")
                .field("nummovi", "totalTransactions")
                .field("impcuot", "limits.financingDisposedBalance")
                .field("impatpe", "extraPayments.outstandingBalanceLocalCurrency.amount")
                .field("monmipe", "extraPayments.minimumCapitalLocalCurrency.amount")
                .field("impinpe", "extraPayments.interestsBalanceLocalCurrency.amount")
                .field("impcope", "extraPayments.feesBalanceLocalCurrency.amount")
                .field("cuopape", "extraPayments.financingTransactionLocalBalance.amount")
                .field("pagmipe", "extraPayments.invoiceMinimumAmountLocalCurrency.amount")
                .field("pagtope", "extraPayments.invoiceAmountLocalCurrency.amount")
                .field("impatus", "extraPayments.outstandingBalance.amount")
                .field("monmius", "extraPayments.minimumCapitalCurrency.amount")
                .field("impinus", "extraPayments.interestsBalance.amount")
                .field("impcous", "extraPayments.feesBalance.amount")
                .field("cuopaus", "extraPayments.financingTransactionBalance.amount")
                .field("pagmius", "extraPayments.invoiceMinimumAmountCurrency.amount")
                .field("pagtous", "extraPayments.invoiceAmount.amount")
                .field("impdetp", "totalDebtLocalCurrency.amount")
                .field("impdetu", "totalDebt.amount")
                .field("mapmini", "totalMonthsAmortizationMinimum")
                .field("mapminf", "totalMonthsAmortizationMinimumFull")
                .register();
    }

    @Override
    public FormatoMPME1GH mapIn(final InputGetCardFinancialStatement dtoIn) {
        return map(dtoIn, FormatoMPME1GH.class);
    }

    @Override
    public DTOIntCardStatement mapOut(final FormatoMPMS1GH formatOutput) {
        final DTOIntCardStatement dto = new DTOIntCardStatement();
        dto.setDetail(map(formatOutput, DTOIntCardStatementDetail.class));
        return dto;
    }

    @Override
    public DTOIntCardStatement mapOut2(final FormatoMPMS2GH formatOutput,
                                       final DTOIntCardStatement dtoOut) {
        DTOIntCardStatement dto = dtoOut;
        if (dto == null) {
            dto = new DTOIntCardStatement();
        }
        if (dto.getRowCardStatement() == null) {
            dto.setRowCardStatement(new ArrayList<String>());
        }
        dto.getRowCardStatement().add(formatOutput.getLinescu());
        return dto;
    }

    @Override
    public DTOIntCardStatement mapOut3(final FormatoMPMS3GH formatOutput,
                                       final DTOIntCardStatement dtoOut) {
        DTOIntCardStatement dto = dtoOut;
        if (dto == null) {
            dto = new DTOIntCardStatement();
        }
        if (dto.getMessages() == null) {
            dto.setMessages(new ArrayList<String>());
        }
        dto.getMessages().add(
                concatenateIfNotNull(
                        formatOutput.getLinme01(), formatOutput.getLinme02(),
                        formatOutput.getLinme03(), formatOutput.getLinme04()));
        return dto;
    }

    private String concatenateIfNotNull(String... str) {
        String result = "";
        for (String s : str) {
            if (s != null) {
                result = result.concat(s).concat(" ");
            }
        }
        return result.isEmpty() ? null : result.trim();
    }
}

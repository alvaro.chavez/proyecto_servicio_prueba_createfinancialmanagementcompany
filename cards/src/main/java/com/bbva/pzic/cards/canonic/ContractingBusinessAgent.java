package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "ContractingBusinessAgent", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "ContractingBusinessAgent", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContractingBusinessAgent implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * Unique employee identifier.
     */
    private String id;
    /**
     * Unique source identifier of the acquisition on which the hiring is carried out.
     */
    private String originId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }
}

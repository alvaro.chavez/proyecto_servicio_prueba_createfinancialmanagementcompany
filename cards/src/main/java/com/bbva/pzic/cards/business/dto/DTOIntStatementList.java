package com.bbva.pzic.cards.business.dto;

import com.bbva.pzic.cards.canonic.Statement;

import java.util.List;

/**
 * Created on 18/07/2018.
 *
 * @author Entelgy
 */
public class DTOIntStatementList {
    private List<Statement> data;
    private DTOIntPagination pagination;

    public List<Statement> getData() {
        return data;
    }

    public void setData(List<Statement> data) {
        this.data = data;
    }

    public DTOIntPagination getPagination() {
        return pagination;
    }

    public void setPagination(DTOIntPagination pagination) {
        this.pagination = pagination;
    }
}

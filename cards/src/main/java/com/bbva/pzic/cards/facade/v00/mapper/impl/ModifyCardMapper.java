package com.bbva.pzic.cards.facade.v00.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v00.mapper.IModifyCardMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 11/08/2017.
 *
 * @author Entelgy
 */
@Mapper
public class ModifyCardMapper implements IModifyCardMapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    @Autowired
    private EnumMapper enumMapper;

    @Override
    public DTOIntCard mapIn(String cardId, Card card) {
        DTOIntCard dtoInt = new DTOIntCard();
        dtoInt.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_V00));
        if (card.getStatus() != null) {
            dtoInt.setStatusId(enumMapper.getBackendValue("cards.status.id", card.getStatus().getId()));
        }
        return dtoInt;
    }
}

package com.bbva.pzic.cards.dao.model.mpcv;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MPMS1CV</code> de la transacci&oacute;n <code>MPCV</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMS1CV")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMS1CV {

	/**
	 * <p>Campo <code>IDCODSG</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDCODSG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String idcodsg;

	/**
	 * <p>Campo <code>DSCODSG</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "DSCODSG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String dscodsg;

	/**
	 * <p>Campo <code>CODSE01</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "CODSE01", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse01;

	/**
	 * <p>Campo <code>CODSE02</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "CODSE02", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse02;

	/**
	 * <p>Campo <code>CODSE03</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "CODSE03", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse03;

	/**
	 * <p>Campo <code>CODSE04</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "CODSE04", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse04;

	/**
	 * <p>Campo <code>CODSE05</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "CODSE05", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse05;

	/**
	 * <p>Campo <code>CODSE06</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "CODSE06", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse06;

	/**
	 * <p>Campo <code>CODSE07</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "CODSE07", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse07;

	/**
	 * <p>Campo <code>CODSE08</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "CODSE08", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse08;

	/**
	 * <p>Campo <code>CODSE09</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "CODSE09", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse09;

	/**
	 * <p>Campo <code>CODSE10</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "CODSE10", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse10;

	/**
	 * <p>Campo <code>CODSE11</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "CODSE11", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse11;

	/**
	 * <p>Campo <code>CODSE12</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "CODSE12", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse12;

	/**
	 * <p>Campo <code>CODSE13</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "CODSE13", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse13;

	/**
	 * <p>Campo <code>CODSE14</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "CODSE14", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 75, longitudMaxima = 75)
	private String codse14;

}
package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntPin;
import com.bbva.pzic.cards.canonic.Pin;

/**
 * Created on 3/10/2017.
 *
 * @author Entelgy
 */
public interface IModifyCardPinMapper {

    DTOIntPin mapIn(String cardId, Pin pin);
}

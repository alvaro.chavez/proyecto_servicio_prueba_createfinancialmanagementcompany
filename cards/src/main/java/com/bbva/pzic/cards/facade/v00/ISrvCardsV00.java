package com.bbva.pzic.cards.facade.v00;

import com.bbva.pzic.cards.canonic.*;

import javax.ws.rs.core.Response;

/**
 * Created on 18/07/2017.
 *
 * @author Entelgy
 */
public interface ISrvCardsV00 {

    String CARD_ID = "card-id";
    String BLOCK_ID = "block-id";

    /**
     * Method for retrieving the purchase in installments.
     *
     * @param installmentsPlan payload
     * @param cardId           card identifier
     * @return object cardData
     */
    InstallmentsPlanData createCardInstallmentsPlan(String cardId, InstallmentsPlan installmentsPlan);

    /**
     * Method for simulate the purchase in installments.
     *
     * @param cardId                     card identifier
     * @param installmentsPlanSimulation payload
     * @return object cardData
     */
    InstallmentsPlanSimulationData simulateCardInstallmentsPlan(String cardId, InstallmentsPlanSimulation installmentsPlanSimulation);

    /**
     * Method for updating a card. The status of a card can only be changed from INOPERATIVE to OPERATIVE when the card is new. Once the card has been set to operative, its status cannot be changed by using this method.
     *
     * @param cardId card identifier
     * @param card   payload
     */
    Response modifyCard(String cardId, Card card);

    /**
     * Method for adding a new block to the card. Independently of the kind of blocking,
     * the status of the card becomes "INOPERATIVE".
     *
     * @param cardId  card identifier
     * @param blockId block identifier
     * @param block   card block
     * @return the card block added or modified
     */
    BlockData modifyCardBlock(String cardId, String blockId, Block block);
}

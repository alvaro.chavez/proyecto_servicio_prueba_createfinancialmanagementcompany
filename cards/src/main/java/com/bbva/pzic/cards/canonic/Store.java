package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "Store", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Store", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Store implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Store unique identifer.
     */
    private String id;
    /**
     * Store name.
     */
    private String name;
    /**
     * Store logo.
     */
    private Image logo;
    /**
     * Represents the store category.
     */
    private Category category;
    /**
     * Identifier of the terminal with which the purchase was made.
     */
    private String terminal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getLogo() {
        return logo;
    }

    public void setLogo(Image logo) {
        this.logo = logo;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
}
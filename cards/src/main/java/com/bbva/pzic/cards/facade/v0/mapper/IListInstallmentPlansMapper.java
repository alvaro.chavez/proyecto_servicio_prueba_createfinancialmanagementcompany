package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOInstallmentsPlanList;
import com.bbva.pzic.cards.business.dto.InputListInstallmentPlans;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;

import java.util.List;

/**
 * Created on 08/02/2018.
 *
 * @author Entelgy
 */
public interface IListInstallmentPlansMapper {

    InputListInstallmentPlans mapIn(String cardId, String paginationKey, Long pageSize);

    ServiceResponse<List<InstallmentsPlan>> mapOut(DTOInstallmentsPlanList dtoInstallmentsPlanList, Pagination pagination);
}

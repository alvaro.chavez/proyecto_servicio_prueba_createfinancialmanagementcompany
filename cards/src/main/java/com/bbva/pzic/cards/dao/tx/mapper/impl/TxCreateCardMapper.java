package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPME0W1;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPMS1W1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardMapper;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.converter.builtin.DateToStringConverter;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.util.Constants.DEFAULT_TIME;

/**
 * Created on 12/06/2017.
 *
 * @author Entelgy
 */
@Mapper
public class TxCreateCardMapper extends ConfigurableMapper implements ITxCreateCardMapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.getConverterFactory().registerConverter(new DateToStringConverter("yyyy-MM-dd"));

        factory.classMap(DTOIntCard.class, FormatoMPME0W1.class)
                .field("cardTypeId", "tiprodf")
                .field("physicalSupportId", "sopfisi")
                .field("relatedContractId", "nucorel")
                .register();

        factory.classMap(FormatoMPMS1W1.class, Card.class)
                .field("numtarj", "number")
                .field("fecvenc", "expirationDate")
                .field("nombcli", "holderName")
                .register();

    }

    @Override
    public FormatoMPME0W1 mapIn(DTOIntCard dtoIn) {
        return map(dtoIn, FormatoMPME0W1.class);
    }

    @Override
    public CardData mapOut(FormatoMPMS1W1 formatOutput) {
        if (formatOutput == null) {
            return null;
        }
        CardData data = new CardData();

        Card dtoOut = map(formatOutput, Card.class);
        dtoOut.setOpeningDate(FunctionUtils.buildDatetime(formatOutput.getFecalta(), DEFAULT_TIME));
        if (formatOutput.getNumtarj() != null) {
            dtoOut.setCardId(cypherTool.encrypt(formatOutput.getNumtarj(), AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD));
        }

        dtoOut.setNumberType(mapNumberType("cards.numberType.id", formatOutput.getIdtiptj(), formatOutput.getDstiptj()));
        dtoOut.setCardType(mapCardType(new CardType(), formatOutput));
        dtoOut.setBrandAssociation(mapBrandAssociation(new BrandAssociation(), formatOutput));
        dtoOut.setPhysicalSupport(mapPhysicalSupport(new PhysicalSupport(), formatOutput));
        dtoOut.setCurrencies(mapCurrencies(new ArrayList<Currency>(), formatOutput));
        dtoOut.setGrantedCredits(mapGrantedCredits(new ArrayList<GrantedCredit>(), formatOutput));
        dtoOut.setStatus(mapStatus(new Status(), formatOutput));
        dtoOut.setRelatedContracts(mapRelatedContracts(new ArrayList<RelatedContract>(), formatOutput));

        data.setData(dtoOut);
        return data;
    }

    private List<RelatedContract> mapRelatedContracts(List<RelatedContract> relatedContracts, FormatoMPMS1W1 formatOutput) {
        RelatedContract relatedContract = new RelatedContract();
        relatedContract.setRelatedContractId(formatOutput.getIdcorel());
        relatedContract.setNumber(formatOutput.getNucorel());
        if (formatOutput.getNucorel() != null) {
            relatedContract.setContractId(cypherTool.encrypt(formatOutput.getNucorel(), AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD));
        }
        relatedContract.setNumberType(mapNumberType("cards.relatedContracts.relatedContract.numberType.id", formatOutput.getIdtcore(), formatOutput.getDetcore()));

        relatedContracts.add(relatedContract);
        return relatedContracts;
    }

    private Status mapStatus(Status status, FormatoMPMS1W1 formatOutput) {
        status.setId(enumMapper.getEnumValue("cards.status.id", formatOutput.getEstarjs()));
        status.setName(formatOutput.getDestarj());

        return status;
    }

    private List<GrantedCredit> mapGrantedCredits(List<GrantedCredit> grantedCredits, FormatoMPMS1W1 formatOutput) {
        GrantedCredit grantedCredit = new GrantedCredit();
        grantedCredit.setAmount(formatOutput.getIlimcre());
        grantedCredit.setCurrency(formatOutput.getDlimcre());
        grantedCredits.add(grantedCredit);

        return grantedCredits;
    }

    private List<Currency> mapCurrencies(List<Currency> currencies, FormatoMPMS1W1 formatOutput) {
        currencies.add(mapOutCurrency(formatOutput.getMonpri(), Boolean.TRUE));
        currencies.add(mapOutCurrency(formatOutput.getMonsec(), Boolean.FALSE));

        return currencies;
    }

    private Currency mapOutCurrency(String value, Boolean isMajor) {
        Currency currency = new Currency();
        currency.setCurrency(value);
        currency.setIsMajor(isMajor);

        return currency;
    }

    private PhysicalSupport mapPhysicalSupport(PhysicalSupport physicalSupport, FormatoMPMS1W1 formatOutput) {
        physicalSupport.setId(enumMapper.getEnumValue("cards.physicalSupport.id", formatOutput.getSopfisi()));
        physicalSupport.setName(formatOutput.getDsopfis());

        return physicalSupport;
    }

    private BrandAssociation mapBrandAssociation(BrandAssociation brandAssociation, FormatoMPMS1W1 formatOutput) {
        brandAssociation.setId(enumMapper.getEnumValue("cards.brandAssociation.id", formatOutput.getIdmatar()));
        brandAssociation.setName(formatOutput.getNomatar());

        return brandAssociation;
    }

    private CardType mapCardType(CardType cardType, FormatoMPMS1W1 formatOutput) {
        cardType.setId(enumMapper.getEnumValue("cards.cardType.id", formatOutput.getTiprodf()));
        cardType.setName(formatOutput.getDsprodf());

        return cardType;
    }

    private NumberType mapNumberType(String catalogDesc, String idValue, String nameValue) {
        NumberType numberType = new NumberType();
        numberType.setId(enumMapper.getEnumValue(catalogDesc, idValue));
        numberType.setName(nameValue);

        return numberType;
    }
}

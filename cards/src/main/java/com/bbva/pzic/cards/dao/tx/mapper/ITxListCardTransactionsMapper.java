package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions;
import com.bbva.pzic.cards.canonic.TransactionsData;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMENL2;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMS1L2;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMS2L2;

/**
 * @author Entelgy
 */
public interface ITxListCardTransactionsMapper {

    /**
     * Creates a new {@link FormatoMPMENL2} instance and initializes it with the provided
     * parameter.
     *
     * @param dtoInputListCardTransactions
     * @return the created object.
     */
    FormatoMPMENL2 mapInput(DTOInputListCardTransactions dtoInputListCardTransactions);

    /**
     * Creates a new or reuse a {@link TransactionsData} instance and initializes it with the provided
     * parameter.
     *
     * @param formatoMPMS1L2
     * @param transactionsData
     * @return the created object.
     */
    TransactionsData mapOutputList(FormatoMPMS1L2 formatoMPMS1L2, TransactionsData transactionsData, DTOInputListCardTransactions dtoIn);

    /**
     * Creates a new or reuse a {@link TransactionsData} instance and initializes it with the provided
     * parameter.
     *
     * @param formatoMPMS2L2
     * @param transactionsData
     * @return the created object.
     */
    TransactionsData mapOutputPagination(FormatoMPMS2L2 formatoMPMS2L2, TransactionsData transactionsData);

}
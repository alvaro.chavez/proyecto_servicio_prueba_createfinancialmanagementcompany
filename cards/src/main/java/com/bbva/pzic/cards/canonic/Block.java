package com.bbva.pzic.cards.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Block", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Block", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Block implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Block identifier. It refers to the kind of block of the card. Due this
     * identifier is unique, there is not possible to have more than one block
     * of the same type, ie with the same identifier
     */
    private String blockId;
    /**
     * Block name.
     */
    private String name;
    /**
     * String based on ISO-8601 timestamp format for specifying when the it was
     * requested the card block.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar blockDate;
    /**
     * Reason for blocking. This reason could extend the meaning of the blockade
     * by classifying it within certain options available for such kind of
     * block. A card could be blocked due things like\: The customer has a debt
     * that is not collectible, the card was sent but it was not recieved and
     * returned to sender, an ATM has captured it, or a bank\'s business rule
     * has determined to block it temporarily or definitively.
     */
    private Reason reason;
    /**
     * Additional information that complements the reason on which the card has
     * been blocked by providing extra explanation about the conditions on which
     * such blocking has been made.
     */
    private String additionalInformation;
    /**
     * Generated code to identify the block.
     */
    private String reference;
    /**
     * Indicates whether the current block is active. A card can have only an
     * active block. A block with this indicator setted to false means that a
     * more restrictive block has been placed replacing those inactives. Default
     * value is true due it is considered that any new block attempts to replace
     * a previous one, if exists.
     */
    private Boolean isActive;
    /**
     * Indicates whether the card is reissued due to the block. If true, the
     * card will be reissued. The reissue may result in fees, depending on the
     * card conditions (see REISSUE definition on /cards/{card-id}/conditions).
     * Depending on the country, the type of blocking may lead to is reissued of
     * the card.
     */
    private Boolean isReissued;

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getBlockDate() {
        return blockDate;
    }

    public void setBlockDate(Calendar blockDate) {
        this.blockDate = blockDate;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsReissued() {
        return isReissued;
    }

    public void setIsReissued(Boolean isReissued) {
        this.isReissued = isReissued;
    }
}
package com.bbva.pzic.cards.dao.model.mpcn.mock;

import com.bbva.pzic.cards.dao.model.mpcn.FormatoMPMS1NC;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

public final class FormatsMpcnMock {
    private static final FormatsMpcnMock INSTANCE = new FormatsMpcnMock();
    private final ObjectMapperHelper objectMapperHelper = ObjectMapperHelper.getInstance();

    private FormatsMpcnMock() {
    }

    public static FormatsMpcnMock getInstance() {
        return INSTANCE;
    }

    public FormatoMPMS1NC getFormatoMPMS1NC() throws IOException {
        return objectMapperHelper.readValue(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(
                        "com/bbva/pzic/cards/dao/model/mpcn/mock/formatoMPMS1NC.json"), FormatoMPMS1NC.class);
    }
}

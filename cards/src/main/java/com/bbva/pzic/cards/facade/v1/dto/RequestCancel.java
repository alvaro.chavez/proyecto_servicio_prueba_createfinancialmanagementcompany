package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "requestCancel", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "requestCancel", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestCancel implements Serializable {

    private static final long serialVersionUID = 1L;

    private Reason reason;

    private OperationTracking operationTracking;

    private CardRequestCancel card;

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public OperationTracking getOperationTracking() {
        return operationTracking;
    }

    public void setOperationTracking(OperationTracking operationTracking) {
        this.operationTracking = operationTracking;
    }

    public CardRequestCancel getCard() {
        return card;
    }

    public void setCard(CardRequestCancel card) {
        this.card = card;
    }
}

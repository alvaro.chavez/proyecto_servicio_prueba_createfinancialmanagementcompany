// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mp3g;

import com.bbva.pzic.cards.dao.model.mp3g.FormatoMPMS33G;
import java.math.BigDecimal;

privileged aspect FormatoMPMS33G_Roo_JavaBean {
    
    /**
     * Gets moneda2 value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getMoneda2() {
        return this.moneda2;
    }
    
    /**
     * Sets moneda2 value
     * 
     * @param moneda2
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setMoneda2(String moneda2) {
        this.moneda2 = moneda2;
        return this;
    }
    
    /**
     * Gets amityp2 value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getAmityp2() {
        return this.amityp2;
    }
    
    /**
     * Sets amityp2 value
     * 
     * @param amityp2
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setAmityp2(String amityp2) {
        this.amityp2 = amityp2;
        return this;
    }
    
    /**
     * Gets amctyp2 value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getAmctyp2() {
        return this.amctyp2;
    }
    
    /**
     * Sets amctyp2 value
     * 
     * @param amctyp2
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setAmctyp2(String amctyp2) {
        this.amctyp2 = amctyp2;
        return this;
    }
    
    /**
     * Gets capvenc value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getCapvenc() {
        return this.capvenc;
    }
    
    /**
     * Sets capvenc value
     * 
     * @param capvenc
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setCapvenc(BigDecimal capvenc) {
        this.capvenc = capvenc;
        return this;
    }
    
    /**
     * Gets idpvmon value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvmon() {
        return this.idpvmon;
    }
    
    /**
     * Sets idpvmon value
     * 
     * @param idpvmon
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvmon(String idpvmon) {
        this.idpvmon = idpvmon;
        return this;
    }
    
    /**
     * Gets interes value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getInteres() {
        return this.interes;
    }
    
    /**
     * Sets interes value
     * 
     * @param interes
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setInteres(BigDecimal interes) {
        this.interes = interes;
        return this;
    }
    
    /**
     * Gets idpvec2 value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvec2() {
        return this.idpvec2;
    }
    
    /**
     * Sets idpvec2 value
     * 
     * @param idpvec2
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvec2(String idpvec2) {
        this.idpvec2 = idpvec2;
        return this;
    }
    
    /**
     * Gets segven1 value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getSegven1() {
        return this.segven1;
    }
    
    /**
     * Sets segven1 value
     * 
     * @param segven1
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setSegven1(BigDecimal segven1) {
        this.segven1 = segven1;
        return this;
    }
    
    /**
     * Gets segven2 value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getSegven2() {
        return this.segven2;
    }
    
    /**
     * Sets segven2 value
     * 
     * @param segven2
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setSegven2(BigDecimal segven2) {
        this.segven2 = segven2;
        return this;
    }
    
    /**
     * Gets idpvcuo value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvcuo() {
        return this.idpvcuo;
    }
    
    /**
     * Sets idpvcuo value
     * 
     * @param idpvcuo
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvcuo(String idpvcuo) {
        this.idpvcuo = idpvcuo;
        return this;
    }
    
    /**
     * Gets cincpev value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getCincpev() {
        return this.cincpev;
    }
    
    /**
     * Sets cincpev value
     * 
     * @param cincpev
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setCincpev(BigDecimal cincpev) {
        this.cincpev = cincpev;
        return this;
    }
    
    /**
     * Gets idpvmem value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvmem() {
        return this.idpvmem;
    }
    
    /**
     * Sets idpvmem value
     * 
     * @param idpvmem
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvmem(String idpvmem) {
        this.idpvmem = idpvmem;
        return this;
    }
    
    /**
     * Gets cuovenc value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getCuovenc() {
        return this.cuovenc;
    }
    
    /**
     * Sets cuovenc value
     * 
     * @param cuovenc
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setCuovenc(BigDecimal cuovenc) {
        this.cuovenc = cuovenc;
        return this;
    }
    
    /**
     * Gets idpvdis value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvdis() {
        return this.idpvdis;
    }
    
    /**
     * Sets idpvdis value
     * 
     * @param idpvdis
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvdis(String idpvdis) {
        this.idpvdis = idpvdis;
        return this;
    }
    
    /**
     * Gets comfven value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getComfven() {
        return this.comfven;
    }
    
    /**
     * Sets comfven value
     * 
     * @param comfven
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setComfven(BigDecimal comfven) {
        this.comfven = comfven;
        return this;
    }
    
    /**
     * Gets idpvgri value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvgri() {
        return this.idpvgri;
    }
    
    /**
     * Sets idpvgri value
     * 
     * @param idpvgri
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvgri(String idpvgri) {
        this.idpvgri = idpvgri;
        return this;
    }
    
    /**
     * Gets comgven value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getComgven() {
        return this.comgven;
    }
    
    /**
     * Sets comgven value
     * 
     * @param comgven
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setComgven(BigDecimal comgven) {
        this.comgven = comgven;
        return this;
    }
    
    /**
     * Gets idpvman value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvman() {
        return this.idpvman;
    }
    
    /**
     * Sets idpvman value
     * 
     * @param idpvman
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvman(String idpvman) {
        this.idpvman = idpvman;
        return this;
    }
    
    /**
     * Gets compven value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getCompven() {
        return this.compven;
    }
    
    /**
     * Sets compven value
     * 
     * @param compven
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setCompven(BigDecimal compven) {
        this.compven = compven;
        return this;
    }
    
    /**
     * Gets idpvpen value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvpen() {
        return this.idpvpen;
    }
    
    /**
     * Sets idpvpen value
     * 
     * @param idpvpen
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvpen(String idpvpen) {
        this.idpvpen = idpvpen;
        return this;
    }
    
    /**
     * Gets compag3 value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getCompag3() {
        return this.compag3;
    }
    
    /**
     * Sets compag3 value
     * 
     * @param compag3
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setCompag3(BigDecimal compag3) {
        this.compag3 = compag3;
        return this;
    }
    
    /**
     * Gets idpvsob value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvsob() {
        return this.idpvsob;
    }
    
    /**
     * Sets idpvsob value
     * 
     * @param idpvsob
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvsob(String idpvsob) {
        this.idpvsob = idpvsob;
        return this;
    }
    
    /**
     * Gets comveso value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getComveso() {
        return this.comveso;
    }
    
    /**
     * Sets comveso value
     * 
     * @param comveso
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setComveso(BigDecimal comveso) {
        this.comveso = comveso;
        return this;
    }
    
    /**
     * Gets idpvec1 value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvec1() {
        return this.idpvec1;
    }
    
    /**
     * Sets idpvec1 value
     * 
     * @param idpvec1
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvec1(String idpvec1) {
        this.idpvec1 = idpvec1;
        return this;
    }
    
    /**
     * Gets porvenc value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getPorvenc() {
        return this.porvenc;
    }
    
    /**
     * Sets porvenc value
     * 
     * @param porvenc
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setPorvenc(BigDecimal porvenc) {
        this.porvenc = porvenc;
        return this;
    }
    
    /**
     * Gets monexcv value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getMonexcv() {
        return this.monexcv;
    }
    
    /**
     * Sets monexcv value
     * 
     * @param monexcv
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setMonexcv(BigDecimal monexcv) {
        this.monexcv = monexcv;
        return this;
    }
    
    /**
     * Gets idpvimo value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvimo() {
        return this.idpvimo;
    }
    
    /**
     * Sets idpvimo value
     * 
     * @param idpvimo
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvimo(String idpvimo) {
        this.idpvimo = idpvimo;
        return this;
    }
    
    /**
     * Gets moraven value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getMoraven() {
        return this.moraven;
    }
    
    /**
     * Sets moraven value
     * 
     * @param moraven
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setMoraven(BigDecimal moraven) {
        this.moraven = moraven;
        return this;
    }
    
    /**
     * Gets idpvico value
     * 
     * @return String
     */
    public String FormatoMPMS33G.getIdpvico() {
        return this.idpvico;
    }
    
    /**
     * Sets idpvico value
     * 
     * @param idpvico
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIdpvico(String idpvico) {
        this.idpvico = idpvico;
        return this;
    }
    
    /**
     * Gets intvenc value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getIntvenc() {
        return this.intvenc;
    }
    
    /**
     * Sets intvenc value
     * 
     * @param intvenc
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setIntvenc(BigDecimal intvenc) {
        this.intvenc = intvenc;
        return this;
    }
    
    /**
     * Gets ciscpev value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoMPMS33G.getCiscpev() {
        return this.ciscpev;
    }
    
    /**
     * Sets ciscpev value
     * 
     * @param ciscpev
     * @return FormatoMPMS33G
     */
    public FormatoMPMS33G FormatoMPMS33G.setCiscpev(BigDecimal ciscpev) {
        this.ciscpev = ciscpev;
        return this;
    }
    
}

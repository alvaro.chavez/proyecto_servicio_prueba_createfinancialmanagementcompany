package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;

/**
 * Created on 30/01/2018.
 *
 * @author Entelgy
 */
public class InputCreateCard {

    private String headerBCSOperationTracer;

    @Valid
    private DTOIntCard dtoIntCard;

    @Valid
    private DTOIntCard card;

    public DTOIntCard getCard() {
        return card;
    }

    public void setCard(DTOIntCard card) {
        this.card = card;
    }

    public DTOIntCard getDtoIntCard() {
        return dtoIntCard;
    }

    public void setDtoIntCard(DTOIntCard dtoIntCard) {
        this.dtoIntCard = dtoIntCard;
    }

    public String getHeaderBCSOperationTracer() {
        return headerBCSOperationTracer;
    }

    public void setHeaderBCSOperationTracer(String headerBCSOperationTracer) {
        this.headerBCSOperationTracer = headerBCSOperationTracer;
    }
}
package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
public class DTOIntProductType {

    @NotNull(groups = ValidationGroup.CreateCardV1.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class DTOIntUnit {

    private BigDecimal amount;
    private String currency;

    @NotNull(groups = {ValidationGroup.CreateCardsProposal.class})
    @Digits(integer = 5, fraction = 2, groups = ValidationGroup.CreateCardsProposal.class)
    private BigDecimal percentage;

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}

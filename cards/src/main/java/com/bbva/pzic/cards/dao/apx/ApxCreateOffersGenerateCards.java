package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputCreateOffersGenerateCards;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateOffersGenerateCardsMapper;
import com.bbva.pzic.cards.dao.model.phiat021_1.PeticionTransaccionPhiat021_1;
import com.bbva.pzic.cards.dao.model.phiat021_1.RespuestaTransaccionPhiat021_1;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.PeticionTransaccionPpcutc01_1;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.RespuestaTransaccionPpcutc01_1;
import com.bbva.pzic.cards.facade.v0.dto.OfferGenerate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy
 */
@Component
public class ApxCreateOffersGenerateCards {

    @Resource(name = "apxCreateOffersGenerateCardsMapper")
    private IApxCreateOffersGenerateCardsMapper mapper;

    @Resource(name = "transaccionPhiat021_1")
    private InvocadorTransaccion<PeticionTransaccionPhiat021_1, RespuestaTransaccionPhiat021_1> transaccion;

    public OfferGenerate perform(final InputCreateOffersGenerateCards input) {
        PeticionTransaccionPhiat021_1 request = mapper.mapIn(input);
        RespuestaTransaccionPhiat021_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}

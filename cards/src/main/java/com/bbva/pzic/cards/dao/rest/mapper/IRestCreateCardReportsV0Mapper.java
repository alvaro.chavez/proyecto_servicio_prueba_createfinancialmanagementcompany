package com.bbva.pzic.cards.dao.rest.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCardReports;
import com.bbva.pzic.cards.dao.model.cardreports.ModelCreateCardReportsRequest;

public interface IRestCreateCardReportsV0Mapper {

    ModelCreateCardReportsRequest mapInBody(InputCreateCardReports dtoInt);

}

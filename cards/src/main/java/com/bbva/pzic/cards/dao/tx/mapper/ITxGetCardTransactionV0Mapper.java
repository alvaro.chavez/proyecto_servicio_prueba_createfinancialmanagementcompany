package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputGetCardTransaction;
import com.bbva.pzic.cards.canonic.TransactionData;
import com.bbva.pzic.cards.dao.model.mpgm.*;

/**
 * Created on 22/12/2017.
 *
 * @author Entelgy
 */
public interface ITxGetCardTransactionV0Mapper {
    /**
     * @param dtoIn
     * @return
     */
    FormatoMPMENGM mapIn(InputGetCardTransaction dtoIn);

    /**
     * @param formatOutput
     * @param dtoOut
     * @return
     */
    TransactionData mapOut(FormatoMPMS1GM formatOutput, TransactionData dtoOut);

    /**
     * @param formatOutput
     * @param dtoOut
     * @return
     */
    TransactionData mapOut2(FormatoMPMS2GM formatOutput, TransactionData dtoOut);

    /**
     * @param formatOutput
     * @param dtoOut
     * @return
     */
    TransactionData mapOut3(FormatoMPMS3GM formatOutput, TransactionData dtoOut);

    /**
     * @param formatOutput
     * @param dtoOut
     * @return
     */
    TransactionData mapOut4(FormatoMPMS4GM formatOutput, TransactionData dtoOut);
}

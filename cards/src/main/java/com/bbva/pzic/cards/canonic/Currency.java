package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Currency", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Currency", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Currency implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * String based on ISO-4217 for specifying the currency.
     */
    private String currency;
    /**
     * Flag indicating whether this currency corresponds to the main currency.
     */
    private Boolean isMajor;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getIsMajor() {
        return isMajor;
    }

    public void setIsMajor(Boolean isMajor) {
        this.isMajor = isMajor;
    }
}
package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 19/11/2019.
 *
 * @author Entelgy
 */
public class InputCreateCardProposal {

    @NotNull(groups = ValidationGroup.CreateCardProposal.class)
    @Valid
    private DTOIntCardType cardType;
    @NotNull(groups = ValidationGroup.CreateCardProposal.class)
    @Valid
    private DTOIntProduct product;
    @NotNull(groups = ValidationGroup.CreateCardProposal.class)
    @Valid
    private DTOIntPhysicalSupport physicalSupport;
    @Valid
    private List<DTOIntImport> grantedCredits;
    @Valid
    private DTOIntSpecificProposalContact contact;
    @Valid
    private DTOIntRate rates;
    @Valid
    private DTOIntFee fees;
    @Valid
    private List<DTOIntParticipant> participants;

    private String offerId;

    public DTOIntCardType getCardType() {
        return cardType;
    }

    public void setCardType(DTOIntCardType cardType) {
        this.cardType = cardType;
    }

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }

    public DTOIntPhysicalSupport getPhysicalSupport() {
        return physicalSupport;
    }

    public void setPhysicalSupport(DTOIntPhysicalSupport physicalSupport) {
        this.physicalSupport = physicalSupport;
    }

    public List<DTOIntImport> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<DTOIntImport> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public DTOIntSpecificProposalContact getContact() {
        return contact;
    }

    public void setContact(DTOIntSpecificProposalContact contact) {
        this.contact = contact;
    }

    public DTOIntRate getRates() {
        return rates;
    }

    public void setRates(DTOIntRate rates) {
        this.rates = rates;
    }

    public DTOIntFee getFees() {
        return fees;
    }

    public void setFees(DTOIntFee fees) {
        this.fees = fees;
    }

    public List<DTOIntParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<DTOIntParticipant> participants) {
        this.participants = participants;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
package com.bbva.pzic.cards.canonic;

import com.bbva.pzic.routine.commons.utils.BooleanAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "proposal", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "proposal", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Proposal implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Financial product type.
     */
    private CardType cardType;
    /**
     * Financial product title.
     */
    private Title title;
    /**
     * Payment method related to a specific credit card.
     */
    private PaymentMethod paymentMethod;
    /**
     * Granted credit. This amount may be provided in several currencies
     * (depending on the country). This attribute is mandatory for credit cards.
     */
    private List<Import> grantedCredits;
    /**
     * Card delivery details for a physical card.
     */
    private Delivery delivery;
    /**
     * Entity associated to card.
     */
    private BankType bank;
    /**
     * Commissions associated with the contracting of a Credit Card. They may or
     * may not come more than one fee depending on the offer related.
     */
    private Fee fees;
    /**
     * Rate charge applied to the type of offer associated.
     */
    private List<Rate> rates;
    /**
     * Rate charge applied to the type of offer associated.
     */
    private List<AdditionalProduct> additionalProducts;
    /**
     * Memberships associated to the card.
     */
    private Membership membership;
    /**
     * Contact information value. This value will be masked when basic
     * authentication state. If the authentication state is advanced the value
     * will be clear.
     */
    private List<ContactDetail> contactDetails;
    /**
     * Unique offer associated with the card.
     */
    private String offerId;
    /**
     * Identifier of the request that is pending to be formalized, associated
     * with a card. For example when the client requested a credit card and go
     * to the branch to pick it up.
     */
    private String id;
    /**
     * String based on the ISO-8601 time stamp format to specify the operation
     * date.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar operationDate;
    /**
     * Number of the operation, when the credit card was generated.
     */
    private String operationNumber;

    @XmlJavaTypeAdapter(BooleanAdapter.class)
    private String notificationsByOperation;

    private Import minimunPayment;

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<Import> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<Import> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public BankType getBank() {
        return bank;
    }

    public void setBank(BankType bank) {
        this.bank = bank;
    }

    public Fee getFees() {
        return fees;
    }

    public void setFees(Fee fees) {
        this.fees = fees;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }

    public List<AdditionalProduct> getAdditionalProducts() {
        return additionalProducts;
    }

    public void setAdditionalProducts(List<AdditionalProduct> additionalProducts) {
        this.additionalProducts = additionalProducts;
    }

    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Calendar getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Calendar operationDate) {
        this.operationDate = operationDate;
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public String getNotificationsByOperation() {
        return notificationsByOperation;
    }

    public void setNotificationsByOperation(String notificationsByOperation) {
        this.notificationsByOperation = notificationsByOperation;
    }

    public Import getMinimunPayment() {
        return minimunPayment;
    }

    public void setMinimunPayment(Import minimunPayment) {
        this.minimunPayment = minimunPayment;
    }
}
package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class InputCreateCardsCardProposal {

    @Size(max = 16, groups = ValidationGroup.CreateCardsCardProposal.class)
    private String cardId;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntCardProposal cardProposal;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public DTOIntCardProposal getCardProposal() {
        return cardProposal;
    }

    public void setCardProposal(DTOIntCardProposal cardProposal) {
        this.cardProposal = cardProposal;
    }
}
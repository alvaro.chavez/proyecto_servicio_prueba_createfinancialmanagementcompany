package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancellations;
import com.bbva.pzic.cards.facade.v1.dto.Cancellation;

public interface ICreateCardCancellationsV1Mapper {

    InputCreateCardCancellations mapIn(String cardId, Cancellation cancellation);

    ServiceResponse<Cancellation> mapOut(Cancellation cancellation);

}

package com.bbva.pzic.cards.dao.model.mp6i;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>MP6I</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMp6i</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMp6i</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: QGDTCCT_MP6I.TXT
 * MP6ISIM. OPERACION NO RECONOCIDA       MP        MP2CMP6I     01 MPMEN6I             MP6I  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2020-09-08XP86479 2020-09-1416.55.54XP94134 2020-09-08-21.46.42.683358XP86479 0001-01-010001-01-01
 * 
 * FICHERO: QGDTFDF_MPMEN6I.TXT
 * MPMEN6I �SIM.OP.NO RECONOCIDA          �F�03�00041�01�00001�MCINDOP�TIPO DE MOVIMIENTO  �A�002�0�R�        �
 * MPMEN6I �SIM.OP.NO RECONOCIDA          �F�03�00041�02�00003�IDETARJ�NUMERO DE TARJETA   �A�019�0�R�        �
 * MPMEN6I �SIM.OP.NO RECONOCIDA          �F�03�00041�03�00022�IDENTIF�ID.MOVIMIENTO       �A�020�0�R�        �
 * 
 * FICHERO: QGDTFDF_MPMS16I.TXT
 * MPMS16I �SALIDA SIM.OP.NO RECONOCIDA   �X�06�00082�01�00001�MCINDOP�TIPO DE MOVIMIENTO  �A�002�0�S�        �
 * MPMS16I �SALIDA SIM.OP.NO RECONOCIDA   �X�06�00082�02�00003�MCINDES�DESCRIP.TP.MOVIMIENT�A�010�0�S�        �
 * MPMS16I �SALIDA SIM.OP.NO RECONOCIDA   �X�06�00082�03�00013�IDETARJ�NUMERO DE TARJETA   �A�019�0�S�        �
 * MPMS16I �SALIDA SIM.OP.NO RECONOCIDA   �X�06�00082�04�00032�IDENTIF�IDENT.NUM.MOVIMIENTO�A�020�0�S�        �
 * MPMS16I �SALIDA SIM.OP.NO RECONOCIDA   �X�06�00082�05�00052�CODOPE �CODIGO RESULT.OPERAC�A�001�0�S�        �
 * MPMS16I �SALIDA SIM.OP.NO RECONOCIDA   �X�06�00082�06�00053�DESOPER�DESCRIP.RESLT.OPERAC�A�030�0�S�        �
 * 
 * FICHERO: QGDTFDX_MP6I.TXT
 * MP6IMPMS16I MPWC6I1 MP2CMP6I1S                             XP86479 2020-09-08-22.14.06.027227XP86479 2020-09-08-22.40.22.871083
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionMp6i
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MP6I",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMp6i.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMEN6I.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionMp6i implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}
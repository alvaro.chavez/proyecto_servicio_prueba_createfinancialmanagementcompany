package com.bbva.pzic.cards.dao.model.phiat021_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>itemizeFee</code>, utilizado por la clase <code>Itemizefees</code></p>
 * 
 * @see Itemizefees
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Itemizefee {
	
	/**
	 * <p>Campo <code>feeType</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "feeType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 27, signo = true, obligatorio = true)
	private String feetype;
	
	/**
	 * <p>Campo <code>itemizeFeeUnit</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "itemizeFeeUnit", tipo = TipoCampo.DTO)
	private Itemizefeeunit itemizefeeunit;
	
}
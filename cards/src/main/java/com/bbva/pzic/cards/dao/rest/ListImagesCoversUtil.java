package com.bbva.pzic.cards.dao.rest;

import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesRequest;
import com.bbva.pzic.cards.dao.model.awsimages.ModelCard;
import com.bbva.pzic.cards.dao.model.awsimages.ModelCountry;
import com.bbva.pzic.cards.dao.rest.mapper.impl.RestListImagesCoversMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Created on 20/02/2020.
 *
 * @author Entelgy
 */
public final class ListImagesCoversUtil {

    private static final Log LOG = LogFactory.getLog(RestListImagesCoversMapper.class);

    private static final DateTimeFormatter BASE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static final DateTimeFormatter DESTINY_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private ListImagesCoversUtil() {
        // prevent instantiation
    }

    public static ModelCard getValorDTOUrl(final String urlParams) {
        String[] nameValuePair = urlParams.split("&");
        ModelCard card = new ModelCard();

        for (String s : nameValuePair) {
            String[] keyValue = s.split("=");
            LOG.debug(String.format("Length of keyValue '%s' for '%s'", keyValue.length, s));

            if (keyValue.length == 2) {
                String key = keyValue[0];
                String value = keyValue[1];

                switch (key) {
                    case "pg":
                        card.setCodProd(value);
                        break;

                    case "bin":
                        card.setBin(value);
                        break;

                    case "type":
                        card.getType().setId(value);
                        break;

                    case "issue_date":
                        card.setIssueDate(parseDate(value));
                        break;

                    case "back":
                        card.setIsBackImage(Boolean.valueOf(value));
                        break;

                    default:
                        LOG.warn(String.format("Unrecognized key %s", key));
                        break;
                }
            }
        }
        return card;
    }

    private static String parseDate(final String value) {
        try {
            // "yyyyMMdd" -> "yyyy-MM-dd"
            LocalDate parse = LocalDate.parse(value, BASE_FORMAT);
            return parse.format(DESTINY_FORMAT);
        } catch (DateTimeParseException e) {
            LOG.warn(e.getMessage());
            return null;
        }
    }

    public static void getValorDTOUrlData0(final AWSImagesRequest input, final String urlParams) {
        String[] nameValuePair = urlParams.split("&");

        for (String s : nameValuePair) {
            String[] keyValue = s.split("=");
            LOG.debug(String.format("Length of keyValue '%s'", keyValue.length));

            if (keyValue.length == 2) {
                String key = keyValue[0];
                String value = keyValue[1];

                switch (key) {
                    case "country":
                        input.setCountry(new ModelCountry());
                        input.getCountry().setId(value);
                        break;

                    case "width":
                        input.setWidth(value);
                        break;

                    case "height":
                        input.setHeight(value);
                        break;

                    default:
                        LOG.warn(String.format("Unrecognized key %s", key));
                        break;
                }
            }
        }
    }
}

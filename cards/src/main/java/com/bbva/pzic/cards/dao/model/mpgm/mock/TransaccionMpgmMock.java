package com.bbva.pzic.cards.dao.model.mpgm.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpgm.FormatoMPMENGM;
import com.bbva.pzic.cards.dao.model.mpgm.FormatoMPMS1GM;
import com.bbva.pzic.cards.dao.model.mpgm.PeticionTransaccionMpgm;
import com.bbva.pzic.cards.dao.model.mpgm.RespuestaTransaccionMpgm;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * Created on 26/12/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpgm")
public class TransaccionMpgmMock implements InvocadorTransaccion<PeticionTransaccionMpgm, RespuestaTransaccionMpgm> {
    public static final String TEST_EMPTY = "6666";
    public static final String TEST_RESPONSE_PURCHASE = "9999";
    public static final String TEST_RESPONSE_CASH_WITHDRAWAL = "9998";
    public static final String TEST_RESPONSE_FEE_PAYMENT = "9997";

    private FormatoMPGMMock formatoMPGMMock;

    @PostConstruct
    public void init() {
        formatoMPGMMock = FormatoMPGMMock.getInstance();
    }

    @Override
    public RespuestaTransaccionMpgm invocar(PeticionTransaccionMpgm peticion) throws ExcepcionTransaccion {
        RespuestaTransaccionMpgm response = new RespuestaTransaccionMpgm();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENGM format = peticion.getCuerpo().getParte(FormatoMPMENGM.class);
        final String numberCard = format.getNumtarj();
        try {
            if (TEST_EMPTY.equals(numberCard)) {
                return response;
            } else if (TEST_RESPONSE_FEE_PAYMENT.equals(numberCard)) {
                FormatoMPMS1GM formatoMPMS1GM = formatoMPGMMock.getFormatoMPMS1GM();
                formatoMPMS1GM.setTipmov("04");

                response.getCuerpo().getPartes().add(buildData(formatoMPMS1GM));
                response.getCuerpo().getPartes().add(buildData(formatoMPGMMock.getFormatoMPMS2GM()));
                response.getCuerpo().getPartes().add(buildData(formatoMPGMMock.getFormatoMPMS3GM()));
                response.getCuerpo().getPartes().add(buildData(formatoMPGMMock.getFormatoMPMS4GM()));
            } else if (TEST_RESPONSE_PURCHASE.equals(numberCard)) {
                response.getCuerpo().getPartes().add(buildData(formatoMPGMMock.getFormatoMPMS1GM()));
                response.getCuerpo().getPartes().add(buildData(formatoMPGMMock.getFormatoMPMS2GM()));
                response.getCuerpo().getPartes().add(buildData(formatoMPGMMock.getFormatoMPMS3GM()));
            } else if (TEST_RESPONSE_CASH_WITHDRAWAL.equals(numberCard)) {
                FormatoMPMS1GM formatoMPMS1GM = formatoMPGMMock.getFormatoMPMS1GM();
                formatoMPMS1GM.setTipmov("02");
                response.getCuerpo().getPartes().add(buildData(formatoMPMS1GM));
                response.getCuerpo().getPartes().add(buildData(formatoMPGMMock.getFormatoMPMS2GM()));
                response.getCuerpo().getPartes().add(buildData(formatoMPGMMock.getFormatoMPMS3GM()));
            } else {
                FormatoMPMS1GM formatoMPMS1GM = formatoMPGMMock.getFormatoMPMS1GM();
                formatoMPMS1GM.setTipmov(null);
                response.getCuerpo().getPartes().add(buildData(formatoMPMS1GM));
                response.getCuerpo().getPartes().add(buildData(formatoMPGMMock.getFormatoMPMS2GM()));
                response.getCuerpo().getPartes().add(buildData(formatoMPGMMock.getFormatoMPMS3GM()));
            }
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }

        return response;
    }

    @Override
    public RespuestaTransaccionMpgm invocarCache(PeticionTransaccionMpgm peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida buildData(Object object) {
        CopySalida copy = new CopySalida();
        copy.setCopy(object);
        return copy;
    }
}

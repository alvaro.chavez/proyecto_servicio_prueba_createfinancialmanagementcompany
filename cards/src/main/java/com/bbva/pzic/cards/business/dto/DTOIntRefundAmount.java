package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class DTOIntRefundAmount {

    private BigDecimal amount;
    @NotNull(groups = {ValidationGroup.CreateCardCashRefund.class})
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}

package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntIdentityDocument {

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    private DTOIntDocumentType documentType;
    @NotNull(groups = ValidationGroup.CreateCardsCardProposal.class)
    @Size(max = 11, groups = ValidationGroup.CreateCardsCardProposal.class)
    private String number;

    public DTOIntDocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DTOIntDocumentType documentType) {
        this.documentType = documentType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "itemizeFee", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "itemizeFee", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemizeFee implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier of the fee type.
     */
    private String feeType;
    /**
     * Description of the feeType.
     */
    private String name;
    /**
     * Mode in which the collection fee is applied.
     */
    private Mode mode;

    private ItemizeFeeUnit itemizeFeeUnit;

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public ItemizeFeeUnit getItemizeFeeUnit() {
        return itemizeFeeUnit;
    }

    public void setItemizeFeeUnit(ItemizeFeeUnit itemizeFeeUnit) {
        this.itemizeFeeUnit = itemizeFeeUnit;
    }
}
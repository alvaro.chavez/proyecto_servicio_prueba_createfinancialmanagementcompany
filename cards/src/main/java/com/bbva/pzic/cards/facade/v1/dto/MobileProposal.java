package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "mobileProposal", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "mobileProposal", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class MobileProposal implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact mobile number. This value will be masked when basicauthentication
     * state. If the authentication state is advanced thevalue will be clear.
     */
    private String number;

    private String contactDetailType;
    /**
     * Contact vendor information.
     */
    private PhoneCompanyProposal phoneCompany;

    public String getContactDetailType() {
        return contactDetailType;
    }

    public void setContactDetailType(String contactDetailType) {
        this.contactDetailType = contactDetailType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PhoneCompanyProposal getPhoneCompany() {
        return phoneCompany;
    }

    public void setPhoneCompany(PhoneCompanyProposal phoneCompany) {
        this.phoneCompany = phoneCompany;
    }
}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputGetCardConditions;
import com.bbva.pzic.cards.canonic.Condition;
import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPFMCE1;
import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPNCCS1;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardConditionsMapper;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.bbva.pzic.cards.util.Constants.DEFAULT_TIME;

/**
 * Created on 22/05/2018.
 *
 * @author Entelgy
 */
@Mapper
public class TxGetCardConditionsMapper extends ConfigurableMapper implements ITxGetCardConditionsMapper {

    private static final String MONIMP = "monimp";

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(InputGetCardConditions.class, FormatoMPFMCE1.class)
                .field("cardId", "idetarj")
                .register();

        factory.classMap(FormatoMPNCCS1.class, Condition.class)
                .field("idconme", "conditionId")
                .field("dsconme", "name")
                .field("tipcome", "outcomes[0].outcomeType.id")
                .field("dscomme", "outcomes[0].outcomeType.name")
                .field("importe", "outcomes[0].feeAmount.amount")
                .field(MONIMP, "outcomes[0].feeAmount.currency")
                .field("canmesm", "period.remainingTime.number")
                .field("impacum", "accumulatedAmount.amount")
                .field(MONIMP, "accumulatedAmount.currency")
                .field("impmeta", "facts[0].conditionAmount.amount")
                .field(MONIMP, "facts[0].conditionAmount.currency")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormatoMPFMCE1 mapIn(final InputGetCardConditions dtoIn) {
        return map(dtoIn, FormatoMPFMCE1.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Condition> mapOut(final FormatoMPNCCS1 formatOutput, List<Condition> conditionList) {
        Condition condition = map(formatOutput, Condition.class);
        if (formatOutput.getTipcome() != null) {
            condition.getOutcomes().get(0).getOutcomeType().setId(
                    enumMapper.getEnumValue("conditions.outcomes.outcomeType.id", formatOutput.getTipcome()));
        }

        if (CollectionUtils.isNotEmpty(condition.getOutcomes()) && condition.getOutcomes().get(0) != null) {
            condition.getOutcomes().get(0).setDueDate(FunctionUtils.buildDatetime(formatOutput.getFecmem(), DEFAULT_TIME));
        }

        if (formatOutput.getCanmesm() != null) {
            condition.getPeriod().getRemainingTime().setUnit("MONTHS");
        }
        if (condition.getPeriod() != null) {
            condition.getPeriod().setStartDate(FunctionUtils.buildDatetime(formatOutput.getFecinme(), DEFAULT_TIME));
        }

        conditionList.add(condition);
        return conditionList;
    }
}
// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.pcpst007_1;

import com.bbva.pzic.cards.dao.model.pcpst007_1.Card;
import com.bbva.pzic.cards.dao.model.pcpst007_1.PeticionTransaccionPcpst007_1;
import com.bbva.pzic.cards.dao.model.pcpst007_1.Reason;

privileged aspect PeticionTransaccionPcpst007_1_Roo_JavaBean {
    
    /**
     * Gets cardid value
     * 
     * @return String
     */
    public String PeticionTransaccionPcpst007_1.getCardid() {
        return this.cardid;
    }
    
    /**
     * Sets cardid value
     * 
     * @param cardid
     * @return PeticionTransaccionPcpst007_1
     */
    public PeticionTransaccionPcpst007_1 PeticionTransaccionPcpst007_1.setCardid(String cardid) {
        this.cardid = cardid;
        return this;
    }
    
    /**
     * Gets reason value
     * 
     * @return Reason
     */
    public Reason PeticionTransaccionPcpst007_1.getReason() {
        return this.reason;
    }
    
    /**
     * Sets reason value
     * 
     * @param reason
     * @return PeticionTransaccionPcpst007_1
     */
    public PeticionTransaccionPcpst007_1 PeticionTransaccionPcpst007_1.setReason(Reason reason) {
        this.reason = reason;
        return this;
    }
    
    /**
     * Gets card value
     * 
     * @return Card
     */
    public Card PeticionTransaccionPcpst007_1.getCard() {
        return this.card;
    }
    
    /**
     * Sets card value
     * 
     * @param card
     * @return PeticionTransaccionPcpst007_1
     */
    public PeticionTransaccionPcpst007_1 PeticionTransaccionPcpst007_1.setCard(Card card) {
        this.card = card;
        return this;
    }
    
    /**
     * Gets customerid value
     * 
     * @return String
     */
    public String PeticionTransaccionPcpst007_1.getCustomerid() {
        return this.customerid;
    }
    
    /**
     * Sets customerid value
     * 
     * @param customerid
     * @return PeticionTransaccionPcpst007_1
     */
    public PeticionTransaccionPcpst007_1 PeticionTransaccionPcpst007_1.setCustomerid(String customerid) {
        this.customerid = customerid;
        return this;
    }
    
}

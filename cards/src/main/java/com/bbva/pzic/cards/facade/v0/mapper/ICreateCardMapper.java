package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.CardData;

/**
 * Created on 30/01/2018.
 *
 * @author Entelgy
 */
public interface ICreateCardMapper {

    InputCreateCard mapIn(Card card);
    CardData mapOut(Card card);
}

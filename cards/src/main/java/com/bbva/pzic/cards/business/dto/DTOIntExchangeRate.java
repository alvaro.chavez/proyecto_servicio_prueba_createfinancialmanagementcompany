package com.bbva.pzic.cards.business.dto;

import java.util.Calendar;

public class DTOIntExchangeRate {

    private Calendar date;
    private DTOIntValues values;

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public DTOIntValues getValues() {
        return values;
    }

    public void setValues(DTOIntValues values) {
        this.values = values;
    }
}

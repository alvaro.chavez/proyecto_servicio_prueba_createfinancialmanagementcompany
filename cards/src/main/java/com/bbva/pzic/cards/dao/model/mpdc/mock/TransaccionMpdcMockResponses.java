package com.bbva.pzic.cards.dao.model.mpdc.mock;

import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCRMDC0;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCRMDC1;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TransaccionMpdcMockResponses {

    private static ObjectMapperHelper objectMapper;

    static {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public static CopySalida getFormatoMCRMDC0() throws IOException {
        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/cards/dao/model/mpdc/mock/formatoMCRMDC0.json"), FormatoMCRMDC0.class));
        return copySalida;
    }

    public static List<CopySalida> getFormatoMCRMDC1() throws IOException {
        List<FormatoMCRMDC1> formatoMCRMDC1List = objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/mpdc/mock/formatoMCRMDC1.json"),
                new TypeReference<List<FormatoMCRMDC1>>() {
                });
        List<CopySalida> copySalidas = new ArrayList<>();
        for (FormatoMCRMDC1 formatoMCRMDC1 : formatoMCRMDC1List) {
            CopySalida copySalida = new CopySalida();
            copySalida.setCopy(formatoMCRMDC1);
            copySalidas.add(copySalida);
        }
        return copySalidas;
    }


}

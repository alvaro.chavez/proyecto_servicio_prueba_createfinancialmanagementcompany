package com.bbva.pzic.cards.dao.model.mp2f.mock;

import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMS12F;

/**
 * Created on 23/07/2019.
 *
 * @author Entelgy
 */
public class FormatsMp2fStubs {

    private static final FormatsMp2fStubs INSTANCE = new FormatsMp2fStubs();

    private FormatsMp2fStubs() {
    }

    public static FormatsMp2fStubs getInstance() {
        return INSTANCE;
    }

    public FormatoMPMS12F getFormatoMPMS12F() {
        FormatoMPMS12F formatoMPMS12F = new FormatoMPMS12F();
        formatoMPMS12F.setIdtoken("id");
        formatoMPMS12F.setNrotarj("5455654388907655");
        return formatoMPMS12F;
    }
}

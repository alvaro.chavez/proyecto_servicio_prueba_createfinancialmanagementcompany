package com.bbva.pzic.cards.business.dto;

public class DTOIntRelatedContractsSearchCriteria {

    private String cardId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
}

package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 17/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Operation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Operation", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Operation implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Product operation identifier.
     */
    private String id;
    /**
     * Product operation name.
     */
    private String name;
    /**
     * Indicates whether the operation turns into money flow.
     */
    private Boolean isMoneyFlow;
    /**
     * Indicates whether the customer meets all the requirements for performing
     * the referred operation.
     */
    private Boolean isFull;
    /**
     * For operations that turn into money flow, this attribute indicates
     * whether the operation only allows BBVA products as destination products.
     * If false, products from other banks can be selected as the destination of
     * the operation.
     */
    private Boolean isOnlyInternal;
    /**
     * Role type.
     */
    private RoleType roleType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsMoneyFlow() {
        return isMoneyFlow;
    }

    public void setIsMoneyFlow(Boolean isMoneyFlow) {
        this.isMoneyFlow = isMoneyFlow;
    }

    public Boolean getIsFull() {
        return isFull;
    }

    public void setIsFull(Boolean isFull) {
        this.isFull = isFull;
    }

    public Boolean getIsOnlyInternal() {
        return isOnlyInternal;
    }

    public void setIsOnlyInternal(Boolean isOnlyInternal) {
        this.isOnlyInternal = isOnlyInternal;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
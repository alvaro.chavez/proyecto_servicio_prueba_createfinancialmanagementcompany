package com.bbva.pzic.cards.dao.model.mpgl.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMENGL;
import com.bbva.pzic.cards.dao.model.mpgl.FormatoMPMS1GL;
import com.bbva.pzic.cards.dao.model.mpgl.PeticionTransaccionMpgl;
import com.bbva.pzic.cards.dao.model.mpgl.RespuestaTransaccionMpgl;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 19/05/2020.
 *
 * @author Entelgy
 */
@Component("transaccionMpgl")
public class TransaccionMpglMock implements InvocadorTransaccion<PeticionTransaccionMpgl, RespuestaTransaccionMpgl> {

    public static final String EMPTY_TEST = "9999";
    private FormatoMPMS1GLMock formatoMPMS1GLMock;

    @PostConstruct
    private void setUp() {
        formatoMPMS1GLMock = new FormatoMPMS1GLMock();
    }

    @Override
    public RespuestaTransaccionMpgl invocar(PeticionTransaccionMpgl peticion) {
        final RespuestaTransaccionMpgl response = new RespuestaTransaccionMpgl();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENGL formatIn = peticion.getCuerpo().getParte(FormatoMPMENGL.class);
        String ideTarj = formatIn.getIdetarj();

        if (EMPTY_TEST.equals(ideTarj)) {
            return response;
        } else {
            try {
                response.getCuerpo().getPartes().addAll(buildDataCopies());
            } catch (IOException e) {
                throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
            }
        }

        return response;
    }

    @Override
    public RespuestaTransaccionMpgl invocarCache(PeticionTransaccionMpgl peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private List<CopySalida> buildDataCopies() throws IOException {
        List<FormatoMPMS1GL> formatoMPMS1GLs = formatoMPMS1GLMock.getFormatoMPMS1GL();

        List<CopySalida> copies = new ArrayList<>();
        for (FormatoMPMS1GL format : formatoMPMS1GLs) {
            CopySalida copy = new CopySalida();
            copy.setCopy(format);
            copies.add(copy);
        }
        return copies;
    }
}

package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCardCancellations;
import com.bbva.pzic.cards.dao.model.mpcn.FormatoMPME1NC;
import com.bbva.pzic.cards.dao.model.mpcn.FormatoMPMS1NC;
import com.bbva.pzic.cards.facade.v1.dto.Cancellation;

public interface ITxCreateCardCancellationsV1Mapper {

    FormatoMPME1NC mapIn(InputCreateCardCancellations input);

    Cancellation mapOutFormatoMPMS1NC(FormatoMPMS1NC format);
}

package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

public class DTOIntRate {

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private DTOIntRateType rateType;
    private Date calculationDate;
    private DTOIntMode mode;
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    @Size(max = 1, groups = ValidationGroup.CreateCardsProposal.class)
    private String modeid;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private DTOIntUnit unit;
    @Valid
    @NotNull(groups = {ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    private List<DTOIntItemizeRate> itemizeRates;

    public DTOIntRateType getRateType() {
        return rateType;
    }

    public void setRateType(DTOIntRateType rateType) {
        this.rateType = rateType;
    }

    public Date getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(Date calculationDate) {
        this.calculationDate = calculationDate;
    }

    public DTOIntMode getMode() {
        return mode;
    }

    public void setMode(DTOIntMode mode) {
        this.mode = mode;
    }

    public String getModeid() {
        return modeid;
    }

    public void setModeid(String modeid) {
        this.modeid = modeid;
    }

    public DTOIntUnit getUnit() {
        return unit;
    }

    public void setUnit(DTOIntUnit unit) {
        this.unit = unit;
    }

    public List<DTOIntItemizeRate> getItemizeRates() {
        return itemizeRates;
    }

    public void setItemizeRates(List<DTOIntItemizeRate> itemizeRates) {
        this.itemizeRates = itemizeRates;
    }
}


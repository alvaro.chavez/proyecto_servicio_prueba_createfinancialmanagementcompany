package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPME0W1;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPMS1W1;

/**
 * Created on 30/01/2018.
 *
 * @author Entelgy
 */
public interface ITxCreateCardMapperV0 {

    FormatoMPME0W1 mapIn(InputCreateCard input);

    Card mapOut(FormatoMPMS1W1 formatOutput);

}

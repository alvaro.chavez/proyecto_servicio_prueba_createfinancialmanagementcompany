package com.bbva.pzic.cards.business.dto;

import java.math.BigDecimal;

/**
 * Created on 26/07/2018.
 *
 * @author Entelgy
 */
public class DTOMoney {
    private String currency;
    private BigDecimal amount;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}

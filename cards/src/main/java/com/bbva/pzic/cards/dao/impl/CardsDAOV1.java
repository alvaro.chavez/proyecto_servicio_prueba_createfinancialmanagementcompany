package com.bbva.pzic.cards.dao.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.ICardsDAOV1;
import com.bbva.pzic.cards.dao.apx.*;
import com.bbva.pzic.cards.dao.tx.*;
import com.bbva.pzic.cards.facade.v1.dto.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Entelgy
 */
@Component
public class CardsDAOV1 implements ICardsDAOV1 {

    private static final Log LOG = LogFactory.getLog(CardsDAOV1.class);

    @Autowired
    private ApxListCardProposals apxListCardProposals;
    @Autowired
    private ApxCreateCardProposal apxCreateCardProposal;
    @Autowired
    private ApxModifyCardProposal apxModifyCardProposal;
    @Autowired
    private ApxCreateCard apxCreateCard;
    @Autowired
    private TxCreateCardV1 txCreateCard;
    @Autowired
    private TxCreateCardDelivery txCreateCardDelivery;
    @Autowired
    private TxGetCardSecurityDataV1 txGetCardSecurityDataV1;
    @Autowired
    private TxGetCardCancellationVerificationV1 txGetCardCancellationVerificationV1;
    @Autowired
    private TxSimulateCardTransactionTransactionRefund txSimulateCardTransactionTransactionRefund;
    @Autowired
    private TxReimburseCardTransactionTransactionRefund txReimburseCardTransactionTransactionRefund;
    @Resource(name = "txGetCardFinancialStatementV1")
    private TxGetCardFinancialStatementV1 txGetCardFinancialStatementV1;
    @Resource(name = "apxCreateCardCancelRequestV1")
    private ApxCreateCardCancelRequestV1 apxCreateCardCancelRequestV1;
    @Resource(name = "txCreateCardCancellationsV1")
    private TxCreateCardCancellationsV1 txCreateCardCancellationsV1;
    @Override
    public List<Proposal> listCardProposals(final InputListCardProposals input) {
        LOG.info("... Invoking method CardsDAOV1.listCardProposals ...");
        return apxListCardProposals.invoke(input);
    }

    @Override
    public ProposalCard createCardProposal(final InputCreateCardProposal input) {
        LOG.info("... Invoking method CardsDAOV1.createCardProposal ...");
        return apxCreateCardProposal.invoke(input);
    }

    @Override
    public Proposal modifyCardProposal(final InputModifyCardProposal input) {
        LOG.info("... Invoking method CardsDAOV1.modifyCardProposal ...");
        return apxModifyCardProposal.invoke(input);
    }

    @Override
    public CardPost createCard(final InputCreateCard input) {
        if (input.getHeaderBCSOperationTracer() == null) {
            LOG.info("... Invoking method CardsDAOV1.createCard APX ...");
            return apxCreateCard.perform(input.getCard());
        } else {
            LOG.info("... Invoking method CardsDAOV1.createCard Host ...");
            return txCreateCard.perform(input.getCard());
        }
    }

    @Override
    public Delivery createCardDelivery(final DTOIntDelivery dtoInt) {
        LOG.info("... Invoking method CardsDAOV1.createCardDelivery ...");
        return txCreateCardDelivery.perform(dtoInt);
    }

    @Override
    public List<SecurityData> getCardSecurityData(final InputGetCardSecurityData input) {
        LOG.info("... Invoking method CardsDAOV1.createCardDelivery ...");
        return txGetCardSecurityDataV1.perform(input);
    }

    @Override
    public CancellationVerification getCardCancellationVerification(final InputGetCardCancellationVerification input) {
        LOG.info("... Invoking method CardsDAOV1.getCardCancellationVerification ...");
        return txGetCardCancellationVerificationV1.perform(input);
    }

    @Override
    public SimulateTransactionRefund simulateCardTransactionTransactionRefund(InputSimulateCardTransactionTransactionRefund input) {
        LOG.info("... Invoking method CardsDAOV1.getCardCancellationVerification ...");
        return txSimulateCardTransactionTransactionRefund.perform(input);
    }

    @Override
    public ReimburseCardTransactionTransactionRefund reimburseCardTransactionTransactionRefund(InputReimburseCardTransactionTransactionRefund input) {
        LOG.info("... Invoking method CardsDAOV1.reimburseCardTransactionTransactionRefund ...");
        return txReimburseCardTransactionTransactionRefund.perform(input);
    }

    @Override
    public FinancialStatement getCardFinancialStatement(final InputGetCardFinancialStatement input) {
        LOG.info("... Invoking method CardsDAOV1.getCardFinancialStatement ...");
        return txGetCardFinancialStatementV1.perform(input);
    }

    @Override
    public RequestCancel createCardCancelRequest(final InputCreateCardCancelRequest input) {
        LOG.info("... Invoking method CardsDAOV1.createCardCancelRequest ...");
        return apxCreateCardCancelRequestV1.perform(input);
    }

    @Override
    public Cancellation createCardCancellations(final InputCreateCardCancellations input) {
        LOG.info("... Invoking method CardsDAOV1.createCardCancellations ...");
        return txCreateCardCancellationsV1.perform(input);
    }
}

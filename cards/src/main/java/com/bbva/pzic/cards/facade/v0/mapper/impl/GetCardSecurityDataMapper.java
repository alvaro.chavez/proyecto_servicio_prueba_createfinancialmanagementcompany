package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.canonic.SecurityData;
import com.bbva.pzic.cards.canonic.SecurityDataArray;
import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardSecurityDataMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA;

/**
 * Created on 09/05/2017.
 *
 * @author Entelgy
 */
@Mapper("getCardSecurityDataMapper")
public class GetCardSecurityDataMapper implements IGetCardSecurityDataMapper {

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public InputGetCardSecurityData mapIn(final String cardId, final String publicKey) {
        InputGetCardSecurityData input = new InputGetCardSecurityData();
        input.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA));
        input.setPublicKey(publicKey);
        return input;
    }

    @Override
    public SecurityDataArray mapOut(final SecurityData securityData) {
        if (securityData == null) {
            return null;
        }
        List<SecurityData> data = new ArrayList<>();
        data.add(securityData);

        SecurityDataArray securityDataArray = new SecurityDataArray();
        securityDataArray.setData(data);
        return securityDataArray;
    }
}

package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import java.util.List;

/**
 * Created on 6/10/2017.
 *
 * @author Entelgy
 */
public interface IListCardActivationsMapper {

    /**
     * Method that creates a DTO with the input data.
     *
     * @param cardId unique card identifier
     */
    DTOIntCard mapIn(String cardId);

    ServiceResponse<List<Activation>> mapOut(List<Activation> activations);

}

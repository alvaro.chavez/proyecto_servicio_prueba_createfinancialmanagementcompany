package com.bbva.pzic.cards.facade.v1.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancelRequest;
import com.bbva.pzic.cards.facade.v1.dto.RequestCancel;

public interface ICreateCardCancelRequestV1Mapper {

    InputCreateCardCancelRequest mapIn(String cardId, RequestCancel requestCancel);

    ServiceResponse<RequestCancel> mapOut(RequestCancel cardCancelRequest);

}

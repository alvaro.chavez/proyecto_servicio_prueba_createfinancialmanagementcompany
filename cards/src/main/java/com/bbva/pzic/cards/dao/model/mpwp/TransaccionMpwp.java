package com.bbva.pzic.cards.dao.model.mpwp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPWP</code>
 *
 * @see PeticionTransaccionMpwp
 * @see RespuestaTransaccionMpwp
 */
@Component
public class TransaccionMpwp implements InvocadorTransaccion<PeticionTransaccionMpwp,RespuestaTransaccionMpwp> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpwp invocar(PeticionTransaccionMpwp transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpwp.class, RespuestaTransaccionMpwp.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpwp invocarCache(PeticionTransaccionMpwp transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpwp.class, RespuestaTransaccionMpwp.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}

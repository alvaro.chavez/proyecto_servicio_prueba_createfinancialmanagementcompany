package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.DateAdapter;
import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "cardPost", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "cardPost", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardPost implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Financial product type.
     */
    private CardTypePost cardType;
    /**
     * Financial product.
     */
    private Product product;
    /**
     * Physical support type associated to the card.
     */
    private PhysicalSupportPost physicalSupport;
    /**
     * Card holder. This name matches the printed name on the card. Note - Id
     * the physical support is VIRTUAL the field does not apply.
     */
    private String holderName;
    /**
     * Currencies related to the card. Monetary amounts related to the card may
     * be provided in one or many of these currencies.
     */
    private List<Currency> currencies;
    /**
     * Granted credit. This amount may be provided in several currencies
     * (depending on the country, major currency and equivalent in another
     * currency). This attribute is mandatory for credit cards.
     */
    private List<Amount> grantedCredits;
    /**
     * The day of the month when the current credit card period ends and the
     * next one starts. All the transactions performed until this day will be
     * added to the next payment term. DISCLAIMER: This field is only used when
     * in the contracting process the customer can negotiate and provide
     * alternative cut off days.
     */
    private Integer cutOffDay;
    /**
     * List of contracts associated to the card. Note - If the card type is
     * CREDIT_CARD or DEBIT_CARD the field is required.
     */
    private List<RelatedContractPost> relatedContracts;
    /**
     * List of images printed on the card.
     */
    private List<Image> images;
    /**
     * Details of delivery of products or documents associated to the
     * registration of a card. Note - If the physical support is different from
     * VIRTUAL, the field becomes mandatory.
     */
    private List<DeliveryPost> deliveries;
    /**
     * Required for business accounts. On a business card the participantId will be
     * the authorized person that will hold the card. It is required to specify
     * AUTHORIZED as participant type also.
     */
    private List<Participant> participants;
    /**
     * Payment method related to a specific credit card. Note - If the card type
     * is different from PREPAID_CARD, the field becomes mandatory.
     */
    private PaymentMethodPost paymentMethod;
    /**
     * Indicates the type of contract support where customer acceptance is
     * received.
     */
    private String supportContractType;
    /**
     * Memberships associated to the card.
     */
    private List<Membership> memberships;
    /**
     * Card contract agreement. It´s the identifier of the card contract
     * containing the card. A card contract can contain multiple cards, with
     * different PAN numbers. It will bew mandatory in case of creation of a
     * card for an existing contract.
     */
    private String cardAgreement;
    /**
     * Center with which the card contract will be registered.
     */
    private AttentionBranch contractingBranch;
    /**
     * Managing office of the contract. To identify the operating office where
     * the contract is managed.
     */
    private AttentionBranch managementBranch;
    /**
     * Information that corresponds to the person registering the card.
     */
    private ContractingBusinessAgent contractingBusinessAgent;
    /**
     * Information that corresponds to a seller of card.
     */
    private MarketBusinessAgent marketBusinessAgent;
    /**
     * Identification number of the Bank to which the card belongs. (BIN). Are
     * the first 6 digits of the card number.
     */
    private String bankIdentificationNumber;
    /**
     * Card identifier.
     */
    private String id;
    /**
     * Number of the card.
     */
    private String number;
    /**
     * Card number type
     */
    private NumberType numberType;
    /**
     * Commercial brand associated to the card.
     */
    private BrandAssociation brandAssociation;
    /**
     * String based on ISO-8601 for specifying the date when the card will
     * expire. For normal plastic cards, the embossed date is usually this
     * expiration date without the day (YYYY-MM).
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date expirationDate;
    /**
     * Card current status.
     */
    private StatusPost status;
    /**
     * String based on ISO-8601 timestamp format for specifying the date when
     * the card was created.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar openingDate;

    public CardTypePost getCardType() {
        return cardType;
    }

    public void setCardType(CardTypePost cardType) {
        this.cardType = cardType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public PhysicalSupportPost getPhysicalSupport() {
        return physicalSupport;
    }

    public void setPhysicalSupport(PhysicalSupportPost physicalSupport) {
        this.physicalSupport = physicalSupport;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public List<Amount> getGrantedCredits() {
        return grantedCredits;
    }

    public void setGrantedCredits(List<Amount> grantedCredits) {
        this.grantedCredits = grantedCredits;
    }

    public Integer getCutOffDay() {
        return cutOffDay;
    }

    public void setCutOffDay(Integer cutOffDay) {
        this.cutOffDay = cutOffDay;
    }

    public List<RelatedContractPost> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<RelatedContractPost> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<DeliveryPost> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<DeliveryPost> deliveries) {
        this.deliveries = deliveries;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public PaymentMethodPost getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethodPost paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getSupportContractType() {
        return supportContractType;
    }

    public void setSupportContractType(String supportContractType) {
        this.supportContractType = supportContractType;
    }

    public List<Membership> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<Membership> memberships) {
        this.memberships = memberships;
    }

    public String getCardAgreement() {
        return cardAgreement;
    }

    public void setCardAgreement(String cardAgreement) {
        this.cardAgreement = cardAgreement;
    }

    public AttentionBranch getContractingBranch() {
        return contractingBranch;
    }

    public void setContractingBranch(AttentionBranch contractingBranch) {
        this.contractingBranch = contractingBranch;
    }

    public AttentionBranch getManagementBranch() {
        return managementBranch;
    }

    public void setManagementBranch(AttentionBranch managementBranch) {
        this.managementBranch = managementBranch;
    }

    public ContractingBusinessAgent getContractingBusinessAgent() {
        return contractingBusinessAgent;
    }

    public void setContractingBusinessAgent(
            ContractingBusinessAgent contractingBusinessAgent) {
        this.contractingBusinessAgent = contractingBusinessAgent;
    }

    public MarketBusinessAgent getMarketBusinessAgent() {
        return marketBusinessAgent;
    }

    public void setMarketBusinessAgent(MarketBusinessAgent marketBusinessAgent) {
        this.marketBusinessAgent = marketBusinessAgent;
    }

    public String getBankIdentificationNumber() {
        return bankIdentificationNumber;
    }

    public void setBankIdentificationNumber(String bankIdentificationNumber) {
        this.bankIdentificationNumber = bankIdentificationNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public NumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(NumberType numberType) {
        this.numberType = numberType;
    }

    public BrandAssociation getBrandAssociation() {
        return brandAssociation;
    }

    public void setBrandAssociation(BrandAssociation brandAssociation) {
        this.brandAssociation = brandAssociation;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public StatusPost getStatus() {
        return status;
    }

    public void setStatus(StatusPost status) {
        this.status = status;
    }

    public Calendar getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Calendar openingDate) {
        this.openingDate = openingDate;
    }
}

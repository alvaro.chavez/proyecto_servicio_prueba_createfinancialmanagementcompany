package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPME0W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS1W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS2W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS3W2;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardMapperV1;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.BusinessServiceUtil;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.converter.builtin.BooleanToStringConverter;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.bbva.pzic.cards.util.Constants.*;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
@Mapper
public class TxCreateCardMapperV1 implements ITxCreateCardMapperV1 {

    private final Translator translator;
    private final BooleanToStringConverter booleanToStringConverterWithNumeric;

    public TxCreateCardMapperV1(Translator translator) {
        this.translator = translator;
        this.booleanToStringConverterWithNumeric = new BooleanToStringConverter("0", "1");
    }

    @Override
    public FormatoMPME0W2 mapIn(final DTOIntCard dtoIn) {
        FormatoMPME0W2 format = new FormatoMPME0W2();
        format.setTiprodf(dtoIn.getCardTypeId());
        format.setMctarje(dtoIn.getProductId());
        format.setSopfisi(dtoIn.getPhysicalSupportId());
        format.setNombcli(dtoIn.getHolderName());
        format.setDivipri(dtoIn.getCurrenciesCurrency());
        format.setInddiv1(booleanToStringConverterWithNumeric.convertTo(dtoIn.getCurrenciesIsMajor(), null));
        format.setLincred(dtoIn.getGrantedCreditsAmount());
        format.setDivipri(dtoIn.getGrantedCreditsCurrency());
        format.setFcierre(dtoIn.getCutOffDay());

        mapInRelatedContractsToFormat(dtoIn.getRelatedContracts(), format);

        if (CollectionUtils.isNotEmpty(dtoIn.getImages())) {
            format.setDistarj(dtoIn.getImages().get(0).getId());
        }

        mapInDeliveriesToFormat(dtoIn.getDeliveries(), format);
        mapInParticipantsToFormat(dtoIn.getParticipants(), format);
        mapInPaymentMethodToFormat(dtoIn.getPaymentMethod(), format);

        format.setTcontra(dtoIn.getSupportContractType());
        format.setCodpafr(dtoIn.getMembershipsNumber());
        format.setNucorel(dtoIn.getCardAgreement());
        format.setCodofig(dtoIn.getContractingBranchId());
        format.setCodgest(dtoIn.getManagementBranchId());
        format.setRegisco(dtoIn.getContractingBusinessAgentId());
        format.setRegisve(dtoIn.getMarketBusinessAgentId());
        format.setBintarj(dtoIn.getBankIdentificationNumber());

        return format;
    }

    private void mapInRelatedContractsToFormat(List<DTOIntRelatedContract> relatedContracts, FormatoMPME0W2 format) {
        if (CollectionUtils.isNotEmpty(relatedContracts)) {
            DTOIntRelatedContract dtoIntRelatedContract = relatedContracts.get(0);

            format.setIdprodr(dtoIntRelatedContract.getContractId());
            if (dtoIntRelatedContract.getRelationType() != null) {
                format.setIndprod(dtoIntRelatedContract.getRelationType().getId());
            }

            if (dtoIntRelatedContract.getProduct() != null) {
                format.setIdprore(dtoIntRelatedContract.getProduct().getId());

                if (dtoIntRelatedContract.getProduct().getProductType() != null) {
                    format.setIdprore(dtoIntRelatedContract.getProduct().getProductType().getId());
                }
            }
        }
    }

    private void mapInParticipantsToFormat(final List<DTOIntParticipant> participants, final FormatoMPME0W2 format) {
        if (CollectionUtils.isNotEmpty(participants)) {
            for (int i = 0; i < participants.size(); i++) {
                DTOIntParticipant participant = participants.get(i);

                if (i == 0) {
                    format.setNumcli1(participant.getId());
                    format.setPartcl1(participant.getParticipantTypeId());
                    format.setTipper1(participant.getLegalPersonTypeId());

                } else if (i == 1) {
                    format.setNumcli2(participant.getId());
                    format.setPartcl2(participant.getParticipantTypeId());
                    format.setTipper2(participant.getLegalPersonTypeId());
                }
            }
        }
    }

    private void mapInDeliveriesToFormat(final List<DTOIntDelivery> deliveries, final FormatoMPME0W2 format) {
        if (CollectionUtils.isNotEmpty(deliveries)) {
            for (int i = 0; i < deliveries.size(); i++) {
                DTOIntDelivery delivery = deliveries.get(i);

                if (i == 0) {
                    mapInDeliveryToFormatIndex0(delivery, format);

                } else if (i == 1) {
                    mapInDeliveryToFormatIndex1(delivery, format);

                } else if (i == 2) {
                    mapInDeliveryToFormatIndex2(delivery, format);
                }
            }
        }
    }

    private void mapInDeliveryToFormatIndex0(final DTOIntDelivery delivery, final FormatoMPME0W2 format) {
        format.setTipent1(delivery.getServiceTypeId());
        if (delivery.getContact() != null) {
            format.setNatcon1(delivery.getContact().getContactType());
            format.setIdcone1(delivery.getContact().getId());
        }
        if (delivery.getDestination() != null) {
            format.setTipdes1(delivery.getDestination().getId());

            if (delivery.getDestination().getBranch() != null) {
                format.setCodofe1(delivery.getDestination().getBranch().getId());
            }
        }
        if (delivery.getAddress() != null) {
            format.setNatcon1(delivery.getAddress().getAddressType());
            format.setIdiren1(delivery.getAddress().getId());
        }
    }

    private void mapInDeliveryToFormatIndex1(final DTOIntDelivery delivery, final FormatoMPME0W2 format) {
        format.setTipent2(delivery.getServiceTypeId());
        if (delivery.getContact() != null) {
            format.setNatcon2(delivery.getContact().getContactType());
            format.setIdcone2(delivery.getContact().getId());
        }
        if (delivery.getDestination() != null) {
            format.setTipdes2(delivery.getDestination().getId());

            if (delivery.getDestination().getBranch() != null) {
                format.setCodofe2(delivery.getDestination().getBranch().getId());
            }
        }
        if (delivery.getAddress() != null) {
            format.setNatcon2(delivery.getAddress().getAddressType());
            format.setIdiren2(delivery.getAddress().getId());
        }
    }

    private void mapInDeliveryToFormatIndex2(final DTOIntDelivery delivery, final FormatoMPME0W2 format) {
        format.setTipent3(delivery.getServiceTypeId());
        if (delivery.getContact() != null) {
            format.setNatcon3(delivery.getContact().getContactType());
            format.setIdcone3(delivery.getContact().getId());
        }
        if (delivery.getDestination() != null) {
            format.setTipdes3(delivery.getDestination().getId());

            if (delivery.getDestination().getBranch() != null) {
                format.setCodofe3(delivery.getDestination().getBranch().getId());
            }
        }
        if (delivery.getAddress() != null) {
            format.setNatcon3(delivery.getAddress().getAddressType());
            format.setIdiren3(delivery.getAddress().getId());
        }
    }

    private void mapInPaymentMethodToFormat(DTOIntPaymentMethod paymentMethod, FormatoMPME0W2 format) {
        if (paymentMethod != null) {
            format.setTipmpag(paymentMethod.getId());

            if (paymentMethod.getFrequency() != null) {
                format.setFrempag(paymentMethod.getFrequency().getId());

                if (paymentMethod.getFrequency().getDaysOfMonth() != null) {
                    format.setFcargo(paymentMethod.getFrequency().getDaysOfMonth().getDay() == null ?
                            null : Integer.valueOf(paymentMethod.getFrequency().getDaysOfMonth().getDay()));
                }
            }
        }
    }

    @Override
    public CardPost mapOut1(final FormatoMPMS1W2 formatOutput) {
        CardPost card = new CardPost();
        card.setId(formatOutput.getNumtarj());
        card.setNumber(formatOutput.getNumtarj());
        card.setNumberType(formatToNumberType(formatOutput.getIdtiptj(), formatOutput.getDstiptj()));
        card.setCardType(formatToCardType(formatOutput.getTiprodf(), formatOutput.getDsprodf()));
        card.setProduct(formatToProduct(formatOutput.getMctarje(), formatOutput.getDesmcta()));
        card.setBrandAssociation(formatToBrandAssociation(formatOutput.getIdmatar(), formatOutput.getNomatar()));
        card.setPhysicalSupport(formatToPhysicalSupport(formatOutput.getSopfisi(), formatOutput.getDsopfis()));
        card.setExpirationDate(formatOutput.getFecvenc());
        card.setHolderName(formatOutput.getNombcli());
        card.setCurrencies(formatToCurrencies(formatOutput.getMonpri(), formatOutput.getIndmonp(), formatOutput.getMonsec(), formatOutput.getIndmons()));
        card.setGrantedCredits(formatGrantedCredits(formatOutput.getLincred(), formatOutput.getDlimcre()));
        card.setStatus(formatToStatus(formatOutput.getEstarjs(), formatOutput.getDestarj()));
        card.setCutOffDay(formatOutput.getFcierre());
        card.setOpeningDate(FunctionUtils.buildDatetime(formatOutput.getFecalta(), DEFAULT_TIME));
        card.setSupportContractType(translator.translateBackendEnumValueStrictly("cards.supportContractType.id", formatOutput.getTcontra()));
        card.setMemberships(formatToMemberships(formatOutput.getCodpafr()));
        card.setCardAgreement(formatOutput.getNucorel());
        card.setContractingBranch(formatToAttentionBranch(formatOutput.getCodofig()));
        card.setManagementBranch(formatToAttentionBranch(formatOutput.getCodgest()));
        card.setContractingBusinessAgent(formatToContractingBusinessAgent(formatOutput.getRegisco()));
        card.setMarketBusinessAgent(formatToMarketBusinessAgent(formatOutput.getRegisve()));
        card.setBankIdentificationNumber(formatOutput.getBintarj());
        return card;
    }

    private NumberType formatToNumberType(String id, String description) {
        if (id == null && description == null) {
            return null;
        }
        NumberType numberType = new NumberType();
        numberType.setId(translator.translateBackendEnumValueStrictly("cards.numberType.id", id));
        numberType.setDescription(description);
        return numberType;
    }

    private CardTypePost formatToCardType(String id, String description) {
        if (id == null && description == null) {
            return null;
        }
        CardTypePost cardType = new CardTypePost();
        cardType.setId(translator.translateBackendEnumValueStrictly("cards.cardType.id", id));
        cardType.setDescription(description);
        return cardType;
    }

    private Product formatToProduct(String id, String name) {
        return formatToProduct(id, name, null);
    }

    private Product formatToProduct(String id, String name, String productTypeId) {
        if (id == null && name == null && productTypeId == null) {
            return null;
        }
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setProductType(formatToProductTypeId(productTypeId));
        return product;
    }

    private ProductType formatToProductTypeId(String productTypeId) {
        if (productTypeId == null) {
            return null;
        }
        ProductType productType = new ProductType();
        productType.setId(translator.translateBackendEnumValueStrictly("cards.relatedContracts.productType", productTypeId));
        return productType;
    }

    private BrandAssociation formatToBrandAssociation(String id, String description) {
        if (id == null && description == null) {
            return null;
        }
        BrandAssociation brandAssociation = new BrandAssociation();
        brandAssociation.setId(translator.translateBackendEnumValueStrictly("cards.brandAssociation.id", id));
        brandAssociation.setDescription(description);
        return brandAssociation;
    }

    private PhysicalSupportPost formatToPhysicalSupport(String id, String description) {
        if (id == null && description == null) {
            return null;
        }
        PhysicalSupportPost physicalSupport = new PhysicalSupportPost();
        physicalSupport.setId(id);
        physicalSupport.setDescription(description);
        return physicalSupport;
    }

    private List<Currency> formatToCurrencies(String currency1, String isMajor1, String currency2, String isMajor2) {
        List<Currency> currencies = new ArrayList<>();
        if (currency1 != null || isMajor1 != null) {
            currencies.add(formatToCurrency(currency1, isMajor1));
        }
        if (currency2 != null || isMajor2 != null) {
            currencies.add(formatToCurrency(currency2, isMajor2));
        }
        return currencies.isEmpty() ? null : currencies;
    }

    private Currency formatToCurrency(String currency, String isMajor) {
        Currency c = new Currency();
        c.setCurrency(currency);
        c.setIsMajor(booleanToStringConverterWithNumeric.convertFrom(isMajor, null));
        return c;
    }

    private List<Amount> formatGrantedCredits(BigDecimal amount, String currency) {
        Amount grantedCredit = new Amount();
        grantedCredit.setAmount(amount);
        grantedCredit.setCurrency(currency);
        return Collections.singletonList(grantedCredit);
    }

    private StatusPost formatToStatus(String id, String description) {
        if (id == null && description == null) {
            return null;
        }
        StatusPost status = new StatusPost();
        status.setId(translator.translateBackendEnumValueStrictly("cards.status.id", id));
        status.setDescription(description);
        return status;
    }

    private List<Membership> formatToMemberships(String id) {
        if (id == null) {
            return null;
        }
        Membership membership = new Membership();
        membership.setId(id);
        membership.setNumber(id);
        return Collections.singletonList(membership);
    }

    private AttentionBranch formatToAttentionBranch(String id) {
        if (id == null) {
            return null;
        }
        AttentionBranch attentionBranch = new AttentionBranch();
        attentionBranch.setId(id);
        return attentionBranch;
    }

    private ContractingBusinessAgent formatToContractingBusinessAgent(String id) {
        if (id == null) {
            return null;
        }
        ContractingBusinessAgent contractingBusinessAgent = new ContractingBusinessAgent();
        contractingBusinessAgent.setId(id);
        return contractingBusinessAgent;
    }

    private MarketBusinessAgent formatToMarketBusinessAgent(String id) {
        if (id == null) {
            return null;
        }
        MarketBusinessAgent marketBusinessAgent = new MarketBusinessAgent();
        marketBusinessAgent.setId(id);
        return marketBusinessAgent;
    }

    @Override
    public CardPost mapOut2(final FormatoMPMS2W2 formatOutput, final CardPost dtoOut) {
        if (CollectionUtils.isEmpty(dtoOut.getDeliveries())) {
            dtoOut.setDeliveries(new ArrayList<>());
        }

        DeliveryPost delivery = new DeliveryPost();
        delivery.setServiceType(formatToServiceType(formatOutput.getTipent()));

        if (delivery.getServiceType() != null) {
            delivery.setContact(formatToContact(formatOutput.getNatcon(), formatOutput.getIdcone()));

            if (delivery.getContact() != null && STORED.equals(delivery.getContact().getContactType())) {
                delivery.setId(BusinessServiceUtil.buildDeliveryId(dtoOut.getCardAgreement(), formatOutput.getIdcone()));
            }

            delivery.setDestination(formatToDestination(formatOutput.getTipdes(), formatOutput.getCodofe()));

            if (delivery.getDestination() != null) {
                formatToDestinationDetail(formatOutput, dtoOut, delivery);
            }
        }

        dtoOut.getDeliveries().add(delivery);
        return dtoOut;
    }

    private ServiceTypePost formatToServiceType(String id) {
        if (id == null) {
            return null;
        }
        ServiceTypePost serviceType = new ServiceTypePost();
        serviceType.setId(id);
        return serviceType;
    }

    private Contact formatToContact(String contactType, String id) {
        if (contactType == null) {
            return null;
        }
        Contact contact = new Contact();
        contact.setContactType(translator.translateBackendEnumValueStrictly("cards.delivery.contactType", contactType));
        if (STORED.equals(contact.getContactType())) {
            contact.setId(id);
        }
        return contact;
    }

    private Destination formatToDestination(String id, String branchId) {
        if (id == null && branchId == null) {
            return null;
        }
        Destination destination = new Destination();
        destination.setId(translator.translateBackendEnumValueStrictly("cards.delivery.destination", id));
        destination.setBranch(formatToBranch(branchId));
        return destination;
    }

    private void formatToDestinationDetail(final FormatoMPMS2W2 formatOutput, final CardPost dtoOut, final DeliveryPost delivery) {
        if (HOME.equals(delivery.getDestination().getId()) ||
                CUSTOM.equals(delivery.getDestination().getId())) {
            delivery.setAddress(formatToAddress(formatOutput.getNatcon(), formatOutput.getIdiren()));

            if (STORED.equals(delivery.getAddress().getAddressType())) {
                delivery.setId(BusinessServiceUtil.buildDeliveryId(dtoOut.getCardAgreement(), formatOutput.getIdiren()));
            }
        } else if (BRANCH.equals(delivery.getDestination().getId()) && delivery.getDestination().getBranch() != null) {
            delivery.setId(BusinessServiceUtil.buildDeliveryId(dtoOut.getCardAgreement(), formatOutput.getCodofe()));
        }
    }

    private Branch formatToBranch(String branchId) {
        if (branchId == null) {
            return null;
        }
        Branch branch = new Branch();
        branch.setId(branchId);
        return branch;
    }

    private Address formatToAddress(String addressType, String id) {
        if (id == null && addressType == null) {
            return null;
        }
        Address address = new Address();
        address.setAddressType(translator.translateBackendEnumValueStrictly("cards.delivery.addressType", addressType));
        if (STORED.equals(address.getAddressType())) {
            address.setId(id);
        }
        return address;
    }

    @Override
    public CardPost mapOut3(final FormatoMPMS3W2 formatOutput, final CardPost dtoOut) {
        if (CollectionUtils.isEmpty(dtoOut.getRelatedContracts())) {
            dtoOut.setRelatedContracts(new ArrayList<>());
        }

        RelatedContractPost relatedContract = new RelatedContractPost();
        relatedContract.setId(formatOutput.getIdprodr());
        relatedContract.setContractId(formatOutput.getIdprodr());
        relatedContract.setProduct(formatToProduct(formatOutput.getIdprore(), null, formatOutput.getIdprore()));
        relatedContract.setRelationType(formatOutputToRelationType(formatOutput.getIndprod()));

        dtoOut.getRelatedContracts().add(relatedContract);
        return dtoOut;
    }

    private RelationType formatOutputToRelationType(String id) {
        if (id == null) {
            return null;
        }
        RelationType relationType = new RelationType();
        relationType.setId(translator.translateBackendEnumValueStrictly("cards.relatedContracts.relationType", id));
        return relationType;
    }
}

package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;

public class DTOIntCardRequestCancel {

    @Valid
    private DTOIntProduct product;

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }
}

package com.bbva.pzic.cards.dao.model.mpgm;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPMENGM</code> de la transacci&oacute;n <code>MPGM</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMENGM")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMENGM {

	/**
	 * <p>Campo <code>NUMTARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "NUMTARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String numtarj;

	/**
	 * <p>Campo <code>IDENTIF</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "IDENTIF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String identif;

}
package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 20/04/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "contactAbility", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "contactAbility", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactAbility implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Reason the participant wants to be contacted
     */
    private String reason;
    /**
     * Permitted hours in which the client can be contacted.
     */
    private ScheduleTimes scheduleTimes;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ScheduleTimes getScheduleTimes() {
        return scheduleTimes;
    }

    public void setScheduleTimes(ScheduleTimes scheduleTimes) {
        this.scheduleTimes = scheduleTimes;
    }
}

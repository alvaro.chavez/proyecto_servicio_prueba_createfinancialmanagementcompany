package com.bbva.pzic.cards.util.mappers;

import com.bbva.pzic.cards.canonic.Links;
import com.bbva.pzic.cards.canonic.Pagination;

import java.math.BigDecimal;

/**
 * @author Entelgy
 */
@Deprecated
public final class PaginationMapper {

    private PaginationMapper() {
        // Prevent instantiation
    }

    public static Pagination build(com.bbva.jee.arq.spring.core.servicing.utils.Pagination source) {
        Links links = new Links();
        links.setFirst(source.getFirstPage());
        links.setLast(source.getLastPage());
        links.setPrevious(source.getPreviousPage());
        links.setNext(source.getNextPage());

        Pagination pagination = new Pagination();
        pagination.setLinks(links);

        if (source.getPage() != null) {
            pagination.setPage(BigDecimal.valueOf(source.getPage()));
        }
        if (source.getNumPages() != null) {
            pagination.setTotalPages(BigDecimal.valueOf(source.getNumPages()));
        }
        if (source.getTotal() != null) {
            pagination.setTotalElements(BigDecimal.valueOf(source.getTotal()));
        }
        if (source.getPageSize() != null) {
            pagination.setPageSize(BigDecimal.valueOf(source.getPageSize()));
        }
        return pagination;
    }
}

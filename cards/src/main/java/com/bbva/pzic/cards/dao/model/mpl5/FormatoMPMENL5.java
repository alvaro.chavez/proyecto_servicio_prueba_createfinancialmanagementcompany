package com.bbva.pzic.cards.dao.model.mpl5;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MPMENL5</code> de la transacci&oacute;n <code>MPL5</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MPMENL5")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMPMENL5 {

	/**
	 * <p>Campo <code>IDETARJ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDETARJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	@DatoAuditable(omitir = true)
	private String idetarj;

}
package com.bbva.pzic.cards.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputModifyCardProposal;
import com.bbva.pzic.cards.dao.apx.mapper.IApxModifyCardProposalMapper;
import com.bbva.pzic.cards.dao.model.ppcut003_1.PeticionTransaccionPpcut003_1;
import com.bbva.pzic.cards.dao.model.ppcut003_1.RespuestaTransaccionPpcut003_1;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 27/11/2019.
 *
 * @author Entelgy
 */
@Component
public class ApxModifyCardProposal {

    @Resource(name = "apxModifyCardProposalMapper")
    private IApxModifyCardProposalMapper mapper;

    @Autowired
    private transient InvocadorTransaccion<PeticionTransaccionPpcut003_1, RespuestaTransaccionPpcut003_1> transaccion;

    public Proposal invoke(final InputModifyCardProposal input) {
        PeticionTransaccionPpcut003_1 request = mapper.mapIn(input);
        RespuestaTransaccionPpcut003_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }
}


package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 27/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntSpecificProposalContact {

    @NotNull(groups = {ValidationGroup.CreateCardProposal.class})
    private String contactType;
    @NotNull(groups = {ValidationGroup.CreateCardProposal.class})
    @Valid
    private DTOIntMobileProposal contact;
    private String id;

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public DTOIntMobileProposal getContact() {
        return contact;
    }

    public void setContact(DTOIntMobileProposal contact) {
        this.contact = contact;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
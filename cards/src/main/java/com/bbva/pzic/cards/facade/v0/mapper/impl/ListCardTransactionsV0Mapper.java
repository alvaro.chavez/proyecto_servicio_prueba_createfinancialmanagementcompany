package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions;
import com.bbva.pzic.cards.canonic.Transaction;
import com.bbva.pzic.cards.canonic.TransactionsData;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardTransactionsV0Mapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created on 06/02/2017.
 *
 * @author Entelgy
 */
@Mapper
public class ListCardTransactionsV0Mapper implements IListCardTransactionsV0Mapper {

    private static final Log LOG = LogFactory.getLog(ListCardTransactionsV0Mapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOInputListCardTransactions mapInput(boolean haveFinancingType, String cardId, String fromOperationDate, String toOperationDate,
                                                 String paginationKey, Long pageSize, String registryId) {
        LOG.info("... called method ListCardTransactionsV0Mapper.mapInput ...");
        final DTOInputListCardTransactions dtoInputListCardTransactions = new DTOInputListCardTransactions();
        dtoInputListCardTransactions.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS));
        dtoInputListCardTransactions.setFromOperationDate(fromOperationDate);
        dtoInputListCardTransactions.setToOperationDate(toOperationDate);
        dtoInputListCardTransactions.setPaginationKey(paginationKey);
        dtoInputListCardTransactions.setPageSize(pageSize);
        dtoInputListCardTransactions.setHaveFinancingType(haveFinancingType);
        dtoInputListCardTransactions.setRegistryId(registryId);
        return dtoInputListCardTransactions;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<List<Transaction>> mapOut(final TransactionsData transactionsData, final Pagination pagination) {
        LOG.info("... called method ListCardTransactionsV0Mapper.mapOut ...");
        if (transactionsData == null || transactionsData.getData() == null) {
            return null;
        }

        return ServiceResponse
                .data(transactionsData.getData())
                .pagination(pagination)
                .build();
    }

}

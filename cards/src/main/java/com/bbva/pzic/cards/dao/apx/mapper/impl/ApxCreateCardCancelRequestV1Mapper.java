package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCardRequestCancel;
import com.bbva.pzic.cards.business.dto.DTOIntReason;
import com.bbva.pzic.cards.business.dto.DTOIntSubProduct;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancelRequest;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateCardCancelRequestV1Mapper;
import com.bbva.pzic.cards.dao.model.pcpst007_1.*;
import com.bbva.pzic.cards.facade.v1.dto.CardRequestCancel;
import com.bbva.pzic.cards.facade.v1.dto.NumberType;
import com.bbva.pzic.cards.facade.v1.dto.OperationTracking;
import com.bbva.pzic.cards.facade.v1.dto.RequestCancel;
import com.bbva.pzic.cards.util.FunctionUtils;
import org.springframework.stereotype.Component;

@Component
public class ApxCreateCardCancelRequestV1Mapper implements IApxCreateCardCancelRequestV1Mapper {

    @Override
    public PeticionTransaccionPcpst007_1 mapIn(final InputCreateCardCancelRequest input) {
        PeticionTransaccionPcpst007_1 peticionTransaccionPcpst007_1 = new PeticionTransaccionPcpst007_1();
        peticionTransaccionPcpst007_1.setCardid(input.getCardId());
        peticionTransaccionPcpst007_1.setReason(mapInReason(input.getReason()));
        peticionTransaccionPcpst007_1.setCard(mapInCard(input.getCard()));
        peticionTransaccionPcpst007_1.setCustomerid(input.getCustomerId());
        return peticionTransaccionPcpst007_1;
    }

    private Reason mapInReason(final DTOIntReason reason) {
        if (reason == null) {
            return null;
        }
        Reason modelReason = new Reason();
        modelReason.setId(reason.getId());
        modelReason.setComments(reason.getComments());
        return modelReason;
    }

    private Card mapInCard(final DTOIntCardRequestCancel card) {
        if (card == null || card.getProduct() == null) {
            return null;
        }
        Card modelCard = new Card();
        Product modelProduct = new Product();
        modelProduct.setId(card.getProduct().getId());
        modelProduct.setSubproduct(mapInSubproduct(card.getProduct().getSubproduct()));
        modelCard.setProduct(modelProduct);
        return modelCard;
    }

    private Subproduct mapInSubproduct(final DTOIntSubProduct subproduct) {
        if (subproduct == null) {
            return null;
        }
        Subproduct modelSubproduct = new Subproduct();
        modelSubproduct.setId(subproduct.getId());
        return modelSubproduct;
    }

    @Override
    public RequestCancel mapOut(final RespuestaTransaccionPcpst007_1 response) {
        if (response == null) {
            return null;
        }
        RequestCancel requestCancel = new RequestCancel();
        requestCancel.setOperationTracking(mapOutOperationTracking(response.getOperationtracking()));
        requestCancel.setCard(mapOutCard(response.getCampo_2_card()));
        return requestCancel;
    }

    private OperationTracking mapOutOperationTracking(final Operationtracking modelOperationtracking) {
        if (modelOperationtracking == null) {
            return null;
        }
        OperationTracking operationTracking = new OperationTracking();
        operationTracking.setOperationNumber(modelOperationtracking.getOperationnumber());
        operationTracking.setOperationDate(FunctionUtils.buildDatetime(modelOperationtracking.getOperationdate(), "00:00:00"));
        return operationTracking;
    }

    private CardRequestCancel mapOutCard(final Campo_2_card modelCard) {
        if (modelCard == null) {
            return null;
        }
        CardRequestCancel cardRequestCancel = new CardRequestCancel();
        cardRequestCancel.setId(modelCard.getId());
        cardRequestCancel.setNumber(modelCard.getNumber());
        cardRequestCancel.setNumberType(mapOutNumberType(modelCard.getNumbertype()));
        cardRequestCancel.setProduct(mapOutProduct(modelCard.getProduct()));
        return cardRequestCancel;
    }

    private NumberType mapOutNumberType(final Numbertype modelNumbertype) {
        if (modelNumbertype == null) {
            return null;
        }
        NumberType numberType = new NumberType();
        numberType.setId(modelNumbertype.getId());
        numberType.setDescription(modelNumbertype.getDescription());
        return numberType;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Product mapOutProduct(final Product modelProduct) {
        if (modelProduct == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Product product = new com.bbva.pzic.cards.facade.v1.dto.Product();
        product.setId(modelProduct.getId());
        product.setDescription(modelProduct.getDescription());
        product.setSubproduct(mapOutSubProduct(modelProduct.getSubproduct()));
        return product;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Subproduct mapOutSubProduct(final Subproduct modelSubproduct) {
        if (modelSubproduct == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Subproduct subProduct = new com.bbva.pzic.cards.facade.v1.dto.Subproduct();
        subProduct.setId(modelSubproduct.getId());
        return subProduct;
    }
}

package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputListCardProposals;
import com.bbva.pzic.cards.dao.apx.mapper.IApxListCardProposalsMapper;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Address;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Branch;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Destination;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Frecuency;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Location;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Membership;
import com.bbva.pzic.cards.dao.model.ppcut004_1.Product;
import com.bbva.pzic.cards.dao.model.ppcut004_1.*;
import com.bbva.pzic.cards.facade.v1.dto.Image;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.mappers.DateMapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
@Component
public class ApxListCardProposalsMapper implements IApxListCardProposalsMapper {

    @Override
    public PeticionTransaccionPpcut004_1 mapIn(final InputListCardProposals input) {
        PeticionTransaccionPpcut004_1 request = new PeticionTransaccionPpcut004_1();
        request.setStatus(input.getStatus());
        return request;
    }

    @Override
    public List<Proposal> mapOut(final RespuestaTransaccionPpcut004_1 response) {
        if (response == null || CollectionUtils.isEmpty(response.getEntityout())) {
            return null;
        }

        return response.getEntityout().stream().filter(Objects::nonNull).map(this::mapOutEntity).collect(Collectors.toList());
    }

    private Proposal mapOutEntity(final Entityout entityout) {
        if(entityout.getData() == null){
            return null;
        }
        Proposal result = new Proposal();
        result.setId(entityout.getData().getId());
        result.setCardType(mapOutCardType(entityout.getData().getCardtype()));
        result.setProduct(mapOutProduct(entityout.getData().getProduct()));
        result.setPhysicalSupport(mapOutPhysicalSupport(entityout.getData().getPhysicalsupport()));
        result.setDeliveries(mapOutDeliveries(entityout.getData().getDeliveries()));
        result.setPaymentMethod(mapOutPaymentMethod(entityout.getData().getPaymentmethod()));
        result.setGrantedCredits(mapOutGrantedCredits(entityout.getData().getGrantedcredits()));
        result.setContact(mapOutSpecificContact(entityout.getData().getSpecificcontact()));
        result.setRates(mapOutRates(entityout.getData().getRates()));
        result.setFees(mapOutFees(entityout.getData().getFees()));
        result.setAdditionalProducts(mapOutAdditionalProducts(entityout.getData().getAdditionalproducts()));
        result.setMembership(mapOutMembership(entityout.getData().getMembership()));
        result.setOperationDate(entityout.getData().getOperationdate() == null ? null : DateUtils.rebuildDateTime(entityout.getData().getOperationdate()));
        result.setStatus(entityout.getData().getStatus());
        result.setImage(mapOutImage(entityout.getData().getImage()));
        result.setContactability(mapOutContactAbility(entityout.getData().getContactability()));
        result.setOfferId(entityout.getData().getOfferid());
        result.setParticipants(mapOutParticipants(entityout.getData().getParticipants()));
        result.setNotificationsByOperation(
                entityout.getData().getNotificationsbyoperation() == null ? null : entityout.getData().getNotificationsbyoperation().toString());
        return result;
    }

    private List<KnowParticipant> mapOutParticipants(final List<Participants> participants) {
        if (CollectionUtils.isEmpty(participants)) {
            return null;
        }

        return participants.stream().filter(Objects::nonNull).map(this::mapOutKnowParticipant).collect(Collectors.toList());
    }

    private KnowParticipant mapOutKnowParticipant(final Participants participants) {
        if (participants.getParticipant() == null) {
            return null;
        }
        KnowParticipant knowParticipant = new KnowParticipant();
        knowParticipant.setPersonType(participants.getParticipant().getPersontype());
        knowParticipant.setId(participants.getParticipant().getId());
        knowParticipant.setFirstName(participants.getParticipant().getFirstname());
        knowParticipant.setLastName(participants.getParticipant().getLastname());
        knowParticipant.setParticipantType(mapOutParticipantType(participants.getParticipant().getParticipanttype()));
        knowParticipant.setIsCustomer(participants.getParticipant().getIscustomer());
        return knowParticipant;
    }

    private ParticipantType mapOutParticipantType(final Participanttype participantType) {
        if (participantType == null) {
            return null;
        }

        ParticipantType result = new ParticipantType();
        result.setId(participantType.getId());
        return result;
    }

    private ContactAbility mapOutContactAbility(final Contactability contactability) {
        if (contactability == null) {
            return null;
        }

        ContactAbility result = new ContactAbility();
        result.setReason(contactability.getReason());
        result.setScheduleTimes(mapOutScheduleTimes(contactability.getScheduletimes()));
        return result;
    }

    private ScheduleTimes mapOutScheduleTimes(final Scheduletimes scheduletimes) {
        if (scheduletimes == null) {
            return null;
        }

        ScheduleTimes result = new ScheduleTimes();
        result.setStartTime(DateMapper.mapDateToStringHour(scheduletimes.getStarttime()));
        result.setEndTime(DateMapper.mapDateToStringHour(scheduletimes.getEndtime()));
        return result;
    }

    private Image mapOutImage(final com.bbva.pzic.cards.dao.model.ppcut004_1.Image image) {
        if (image == null) {
            return null;
        }

        Image result = new Image();
        result.setId(image.getId());
        result.setName(image.getName());
        result.setUrl(image.getUrl());
        return result;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Membership mapOutMembership(final Membership membershipOut) {
        if (membershipOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Membership membership = new com.bbva.pzic.cards.facade.v1.dto.Membership();
        membership.setId(membershipOut.getId());
        membership.setDescription(membershipOut.getDescription());
        return membership;
    }

    private List<AdditionalProduct> mapOutAdditionalProducts(final List<Additionalproducts> additionalproductsOut) {
        if (CollectionUtils.isEmpty(additionalproductsOut)) {
            return null;
        }

        return additionalproductsOut.stream().map(this::mapOutAdditionalProduct).collect(Collectors.toList());
    }

    private AdditionalProduct mapOutAdditionalProduct(final Additionalproducts additionalproductsOut) {
        if (additionalproductsOut.getAdditionalproduct() == null) {
            return null;
        }

        AdditionalProduct additionalProduct = new AdditionalProduct();
        additionalProduct.setProductType(additionalproductsOut.getAdditionalproduct().getProducttype());
        additionalProduct.setAmount(NumberUtils.isNumber(additionalproductsOut.getAdditionalproduct().getAmount()) ? NumberUtils.createBigDecimal(additionalproductsOut.getAdditionalproduct().getAmount()) : null);
        additionalProduct.setCurrency(additionalproductsOut.getAdditionalproduct().getCurrency());
        return additionalProduct;
    }

    private TransferFees mapOutFees(final Fees feesOut) {
        if (feesOut == null) {
            return null;
        }
        TransferFees fee = new TransferFees();
        fee.setItemizeFees(mapOutItemizeFees(feesOut.getItemizefees()));
        return fee;
    }

    private List<ItemizeFee> mapOutItemizeFees(final List<Itemizefees> itemizefeesOut) {
        if (CollectionUtils.isEmpty(itemizefeesOut)) {
            return null;
        }

        return itemizefeesOut.stream().map(this::mapOutItemizeFee).collect(Collectors.toList());
    }

    private ItemizeFee mapOutItemizeFee(final Itemizefees itemizefeesOut) {
        if (itemizefeesOut.getItemizefee() == null) {
            return null;
        }

        ItemizeFee itemizeFee = new ItemizeFee();
        itemizeFee.setFeeType(itemizefeesOut.getItemizefee().getFeetype());
        itemizeFee.setDescription(itemizefeesOut.getItemizefee().getDescription());
        itemizeFee.setItemizeFeeUnit(mapOutItemizeFreeUnit(itemizefeesOut.getItemizefee().getItemizefeeunit()));
        return itemizeFee;
    }

    private ItemizeFeeUnit mapOutItemizeFreeUnit(final Itemizefeeunit itemizefeeunitOut) {
        if (itemizefeeunitOut == null) {
            return null;
        }

        ItemizeFeeUnit itemizeFeeUnit = new ItemizeFeeUnit();
        itemizeFeeUnit.setUnitType(itemizefeeunitOut.getUnittype());
        itemizeFeeUnit.setAmount(NumberUtils.isNumber(itemizefeeunitOut.getAmount()) ? NumberUtils.createBigDecimal(itemizefeeunitOut.getAmount()) : null);
        itemizeFeeUnit.setCurrency(itemizefeeunitOut.getCurrency());
        return itemizeFeeUnit;
    }

    private ProposalRate mapOutRates(final Rates ratesOut) {
        if (ratesOut == null) {
            return null;
        }

        ProposalRate proposalRate = new ProposalRate();
        proposalRate.setItemizeRates(mapOutItemizeRates(ratesOut.getItemizerates()));
        return proposalRate;
    }

    private List<ItemizeRate> mapOutItemizeRates(final List<Itemizerates> itemizeratesOut) {
        if (CollectionUtils.isEmpty(itemizeratesOut)) {
            return null;
        }

        return itemizeratesOut.stream().map(this::mapOutItemizeRate).collect(Collectors.toList());
    }

    private ItemizeRate mapOutItemizeRate(final Itemizerates itemizeratesOut) {
        if (itemizeratesOut.getItemizerate() == null) {
            return null;
        }

        ItemizeRate itemizeRate = new ItemizeRate();
        itemizeRate.setRateType(itemizeratesOut.getItemizerate().getRatetype());
        itemizeRate.setDescription(itemizeratesOut.getItemizerate().getDescription());
        itemizeRate.setItemizeRatesUnit(mapOutItemizeRatesUnit(itemizeratesOut.getItemizerate().getItemizeratesunit()));
        return itemizeRate;
    }

    private ItemizeRateUnit mapOutItemizeRatesUnit(Itemizeratesunit itemizeratesunitOut) {
        if (itemizeratesunitOut == null) {
            return null;
        }

        ItemizeRateUnit itemizeRateUnit = new ItemizeRateUnit();
        itemizeRateUnit.setPercentage(NumberUtils.isNumber(itemizeratesunitOut.getPercentage()) ? NumberUtils.createBigDecimal(itemizeratesunitOut.getPercentage()) : null);
        itemizeRateUnit.setUnitRateType(itemizeratesunitOut.getUnitratetype());
        return itemizeRateUnit;
    }

    private SpecificProposalContact mapOutSpecificContact(final Specificcontact specificcontactOut) {
        if (specificcontactOut == null) {
            return null;
        }
        SpecificProposalContact contact = new SpecificProposalContact();
        contact.setId(specificcontactOut.getId());
        contact.setContactType(specificcontactOut.getContacttype());
        contact.setContact(mapOutMobileContact(specificcontactOut.getMobilecontact()));
        return contact;
    }

    private MobileProposal mapOutMobileContact(final Mobilecontact contactOut) {
        if (contactOut == null) {
            return null;
        }

        MobileProposal specificContact = new MobileProposal();
        specificContact.setContactDetailType(contactOut.getContactdetailtype());
        specificContact.setNumber(contactOut.getNumber());
        specificContact.setPhoneCompany(mapOutPhoneCompany(contactOut.getPhonecompany()));
        return specificContact;
    }

    private PhoneCompanyProposal mapOutPhoneCompany(final Phonecompany phonecompanyOut) {
        if (phonecompanyOut == null) {
            return null;
        }

        PhoneCompanyProposal phoneCompany = new PhoneCompanyProposal();
        phoneCompany.setId(phonecompanyOut.getId());
        phoneCompany.setName(phonecompanyOut.getName());
        return phoneCompany;
    }

    private List<Import> mapOutGrantedCredits(final List<Grantedcredits> grantedcreditsOut) {
        if (CollectionUtils.isEmpty(grantedcreditsOut)) {
            return null;
        }

        return grantedcreditsOut.stream().map(this::mapOutGrantedCredit).collect(Collectors.toList());
    }

    private Import mapOutGrantedCredit(final Grantedcredits grantedcreditsOut) {
        if (grantedcreditsOut.getGrantedcredit() == null) {
            return null;
        }

        Import amount = new Import();
        amount.setAmount(NumberUtils.isNumber(grantedcreditsOut.getGrantedcredit().getAmount()) ? NumberUtils.createBigDecimal(grantedcreditsOut.getGrantedcredit().getAmount()) : null);
        amount.setCurrency(grantedcreditsOut.getGrantedcredit().getCurrency());
        return amount;
    }

    private PaymentMethod mapOutPaymentMethod(final Paymentmethod paymentmethodOut) {
        if (paymentmethodOut == null) {
            return null;
        }

        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(paymentmethodOut.getId());
        paymentMethod.setFrecuency(mapOutFrecuency(paymentmethodOut.getFrecuency()));
        return paymentMethod;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Frecuency mapOutFrecuency(final Frecuency frecuencyOut) {
        if (frecuencyOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Frecuency frequency = new com.bbva.pzic.cards.facade.v1.dto.Frecuency();
        frequency.setId(frecuencyOut.getId());
        frequency.setDescription(frecuencyOut.getDescription());
        frequency.setDaysOfMonth(mapOutDaysOfMonth(frecuencyOut.getDaysofmonth()));
        return frequency;
    }

    private DaysOfMonth mapOutDaysOfMonth(final Daysofmonth daysofmonthOut) {
        if (daysofmonthOut == null) {
            return null;
        }

        DaysOfMonth daysOfMonth = new DaysOfMonth();
        daysOfMonth.setCutOffDay(daysofmonthOut.getCutoffday());
        daysOfMonth.setDay(daysofmonthOut.getDay());
        return daysOfMonth;
    }

    private List<com.bbva.pzic.cards.facade.v1.dto.Delivery> mapOutDeliveries(List<Deliveries> deliveriesOut) {
        if (CollectionUtils.isEmpty(deliveriesOut)) {
            return null;
        }

        return deliveriesOut.stream().map(this::mapOutDelivery).collect(Collectors.toList());
    }

    private com.bbva.pzic.cards.facade.v1.dto.Delivery mapOutDelivery(final Deliveries deliveriesOut) {
        if (deliveriesOut.getDelivery() == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Delivery delivery = new com.bbva.pzic.cards.facade.v1.dto.Delivery();
        delivery.setId(deliveriesOut.getDelivery().getId());
        delivery.setServiceType(mapOutServiceType(deliveriesOut.getDelivery().getServicetype()));
        delivery.setContact(mapOutDeliveryContact(deliveriesOut.getDelivery().getDeliverycontact()));
        delivery.setAddress(mapOutAddress(deliveriesOut.getDelivery().getAddress()));
        delivery.setDestination(mapOutDestination(deliveriesOut.getDelivery().getDestination()));
        return delivery;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Destination mapOutDestination(final Destination destinationOut) {
        if (destinationOut == null) {
            return null;
        }
        com.bbva.pzic.cards.facade.v1.dto.Destination destination = new com.bbva.pzic.cards.facade.v1.dto.Destination();
        destination.setId(destinationOut.getId());
        destination.setName(destinationOut.getName());
        destination.setBranch(mapOutBranch(destinationOut.getBranch()));
        return destination;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Branch mapOutBranch(final Branch branchOut) {
        if (branchOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Branch branch = new com.bbva.pzic.cards.facade.v1.dto.Branch();
        branch.setId(branchOut.getId());
        branch.setName(branchOut.getName());
        return branch;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Address mapOutAddress(final Address addressOut) {
        if (addressOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Address address = new com.bbva.pzic.cards.facade.v1.dto.Address();
        address.setId(addressOut.getId());
        address.setAddressType(addressOut.getAddresstype());
        address.setLocation(mapOutLocation(addressOut.getLocation()));
        return address;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Location mapOutLocation(final Location locationOut) {
        if (locationOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Location location = new com.bbva.pzic.cards.facade.v1.dto.Location();
        location.setAddressComponents(mapOutAddressComponents(locationOut.getAddresscomponents()));
        location.setAdditionalInformation(locationOut.getAdditionalinformation());
        return location;
    }

    private List<AddressComponents> mapOutAddressComponents(final List<Addresscomponents> addresscomponentsOut) {
        if (addresscomponentsOut == null) {
            return null;
        }

        return addresscomponentsOut.stream().map(this::mapOutAddressComponent).collect(Collectors.toList());
    }

    private AddressComponents mapOutAddressComponent(final Addresscomponents addresscomponentsOut) {
        if (addresscomponentsOut.getAddresscomponent() == null) {
            return null;
        }

        AddressComponents addressComponents = new AddressComponents();
        addressComponents.setCode(addresscomponentsOut.getAddresscomponent().getCode());
        addressComponents.setName(addresscomponentsOut.getAddresscomponent().getName());
        addressComponents.setComponentTypes(mapOutComponentTypes(addresscomponentsOut.getAddresscomponent().getComponenttypes()));
        return addressComponents;
    }

    private List<String> mapOutComponentTypes(final List<Componenttypes> componenttypesOut) {
        if (CollectionUtils.isEmpty(componenttypesOut)) {
            return null;
        }

        return componenttypesOut.stream().map(Componenttypes::getComponenttype).collect(Collectors.toList());
    }


    private com.bbva.pzic.cards.facade.v1.dto.Contact mapOutDeliveryContact(final Deliverycontact deliverycontactOut) {
        if (deliverycontactOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Contact contact = new com.bbva.pzic.cards.facade.v1.dto.Contact();
        contact.setId(deliverycontactOut.getId());
        contact.setContactType(deliverycontactOut.getContacttype());
        contact.setContact(mapOutContactDetail(deliverycontactOut.getEmailcontact()));
        return contact;
    }

    private SpecificContact mapOutContactDetail(final Emailcontact contactdetailOut) {
        if (contactdetailOut == null) {
            return null;
        }

        SpecificContact specificContact = new SpecificContact();
        specificContact.setContactDetailType(contactdetailOut.getContactdetailtype());
        specificContact.setAddress(contactdetailOut.getAddress());
        return specificContact;
    }

    private ServiceType mapOutServiceType(final Servicetype servicetypeOut) {
        if (servicetypeOut == null) {
            return null;
        }

        ServiceType serviceType = new ServiceType();
        serviceType.setId(servicetypeOut.getId());
        serviceType.setDescription(servicetypeOut.getDescription());
        return serviceType;
    }

    private PhysicalSupport mapOutPhysicalSupport(final Physicalsupport physicalsupportOut) {
        if (physicalsupportOut == null) {
            return null;
        }

        PhysicalSupport physicalSupport = new PhysicalSupport();
        physicalSupport.setId(physicalsupportOut.getId());
        physicalSupport.setDescription(physicalsupportOut.getDescription());
        return physicalSupport;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Product mapOutProduct(final Product productOut) {
        if (productOut == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Product product = new com.bbva.pzic.cards.facade.v1.dto.Product();
        product.setId(productOut.getId());
        product.setName(productOut.getName());
        if (productOut.getSubproduct() == null) {
            return product;
        }

        com.bbva.pzic.cards.facade.v1.dto.Subproduct subProduct = new com.bbva.pzic.cards.facade.v1.dto.Subproduct();
        subProduct.setId(productOut.getSubproduct().getId());
        product.setSubproduct(subProduct);
        return product;
    }

    private CardType mapOutCardType(final Cardtype cardtype) {
        if (cardtype == null) {
            return null;
        }

        CardType cardType = new CardType();
        cardType.setId(cardtype.getId());
        cardType.setName(cardtype.getName());
        return cardType;
    }
}

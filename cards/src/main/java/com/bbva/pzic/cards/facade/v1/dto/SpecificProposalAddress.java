package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "specificProposalAddress", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "specificProposalAddress", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SpecificProposalAddress implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Address
     */
    private LocationProposal location;

    public LocationProposal getLocation() {
        return location;
    }

    public void setLocation(LocationProposal location) {
        this.location = location;
    }
}

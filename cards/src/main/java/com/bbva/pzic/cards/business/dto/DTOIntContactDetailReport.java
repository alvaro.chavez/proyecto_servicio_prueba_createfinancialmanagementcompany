package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;

public class DTOIntContactDetailReport {
    @Valid
    private DTOIntEmailContact emailContact;
    @Valid
    private DTOIntMobileContact mobileContact;

    public DTOIntEmailContact getEmailContact() {
        return emailContact;
    }

    public void setEmailContact(DTOIntEmailContact emailContact) {
        this.emailContact = emailContact;
    }

    public DTOIntMobileContact getMobileContact() {
        return mobileContact;
    }

    public void setMobileContact(DTOIntMobileContact mobileContact) {
        this.mobileContact = mobileContact;
    }
}

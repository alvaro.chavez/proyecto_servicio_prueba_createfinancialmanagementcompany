package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 05/04/2016.
 *
 * @author Entelgy
 */
public class DTOIntAddress {

    @NotNull(groups = ValidationGroup.ModifyCardProposal.class)
    private String id;
    private String name;
    private String streetType;
    private String state;
    private String ubigeo;
    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    private String addressType;
    @Valid
    private DTOIntLocation location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetType() {
        return streetType;
    }

    public void setStreetType(String streetType) {
        this.streetType = streetType;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public DTOIntLocation getLocation() {
        return location;
    }

    public void setLocation(DTOIntLocation location) {
        this.location = location;
    }
}

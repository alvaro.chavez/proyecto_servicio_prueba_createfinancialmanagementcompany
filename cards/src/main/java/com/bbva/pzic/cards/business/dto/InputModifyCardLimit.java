package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * Created on 17/11/2017.
 *
 * @author Entelgy
 */
public class InputModifyCardLimit {
    @NotNull(groups = ValidationGroup.ModifyCardLimitV0.class)
    @Size(max = 19, groups = ValidationGroup.ModifyCardLimitV0.class)
    private String cardId;
    @NotNull(groups = ValidationGroup.ModifyCardLimitV0.class)
    @Size(max = 20, groups = ValidationGroup.ModifyCardLimitV0.class)
    private String limitId;
    @NotNull(groups = ValidationGroup.ModifyCardLimitV0.class)
    @Digits(integer = 13, fraction = 2, groups = ValidationGroup.ModifyCardLimitV0.class)
    private BigDecimal amountLimitAmount;
    @NotNull(groups = ValidationGroup.ModifyCardLimitV0.class)
    @Size(max = 3, groups = ValidationGroup.ModifyCardLimitV0.class)
    private String amountLimitCurrency;
    @Size(max = 10, groups = ValidationGroup.ModifyCardLimitV0.class)
    private String offerId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getLimitId() {
        return limitId;
    }

    public void setLimitId(String limitId) {
        this.limitId = limitId;
    }

    public BigDecimal getAmountLimitAmount() {
        return amountLimitAmount;
    }

    public void setAmountLimitAmount(BigDecimal amountLimitAmount) {
        this.amountLimitAmount = amountLimitAmount;
    }

    public String getAmountLimitCurrency() {
        return amountLimitCurrency;
    }

    public void setAmountLimitCurrency(String amountLimitCurrency) {
        this.amountLimitCurrency = amountLimitCurrency;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
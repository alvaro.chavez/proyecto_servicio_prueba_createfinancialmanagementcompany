package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.DTOIntStatementList;
import com.bbva.pzic.cards.business.dto.InputListCardFinancialStatements;
import com.bbva.pzic.cards.canonic.StatementList;

/**
 * Created on 17/07/2018.
 *
 * @author Entelgy
 */
public interface IListCardFinancialStatementsMapper {

    InputListCardFinancialStatements mapIn(String cardId, Boolean isActive,
                                           String paginationKey, Integer pageSize);

    StatementList mapOut(DTOIntStatementList statementList);
}
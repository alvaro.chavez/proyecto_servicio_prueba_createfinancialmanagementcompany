package com.bbva.pzic.cards.business;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.*;

import java.util.List;

/**
 * @author Entelgy
 */
public interface ISrvIntCards {

    /**
     * Validates all constraints on input
     *
     * @param input dto to add
     * @return the card to add
     */
    CardData createCard(DTOIntCard input);

    /**
     * Validates all constraints on {@link DTOIntBlock} and then adds or modifies the card block
     *
     * @param block dto to modify
     * @return the block added o modified
     * @throws com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException if a constraint violation occurred
     */
    BlockData modifyCardBlock(DTOIntBlock block);

    /**
     * Validates all constraints on {@link DTOIntActivation} and then updates the operational activation of the card
     *
     * @param activation dto to modify
     * @throws com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException if a constraint violation occurred
     */
    void modifyCardActivation(DTOIntActivation activation);

    /**
     * Method that validates input data
     *
     * @param dtoIn DTO with the input fields to validate
     * @return List cards
     */
    DTOOutListCards listCards(DTOIntListCards dtoIn);

    /**
     * Validates all constraints on queryFilter and then gets all transactions that
     * match the query filter
     *
     * @param queryFilter filter
     * @return a list of transactions
     * @throws com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException if a constraint violation occurred
     */
    TransactionsData listCardTransactions(DTOInputListCardTransactions queryFilter);

    /**
     * Method that validates mandatory and size of the input data.
     *
     * @param dtoIn Object that contains the input data
     * @return Objeto que contiene los datos de salida
     */
    DTOOutCreateCardRelatedContract createCardRelatedContract(DTOInputCreateCardRelatedContract dtoIn);

    /**
     * Validates all constraints on queryFilter and then gets all transactions that
     *
     * @param input Object that contains the input data
     * @return Objeto que contiene los datos de salida
     */
    SecurityData getCardSecurityData(InputGetCardSecurityData input);

    InstallmentsPlanData createCardInstallmentsPlan(DTOIntCardInstallmentsPlan dtoIn);

    InstallmentsPlanSimulationData simulateCardInstallmentsPlanV00(DTOIntCardInstallmentsPlan dtoIn);

    void modifyCard(DTOIntCard dtoInt);

    void modifyPartialCardBlock(DTOIntBlock dtoIntBlock);

    void modifyCardPin(DTOIntPin dtoIntPin);

    List<Activation> listCardActivations(DTOIntCard dtoInt);

    List<Activation> modifyCardActivations(InputModifyCardActivations dtoInt);

    /**
     * Manages the pre-application that the customer has generated for the
     * formalization of an additional credit card, which is finalized when the
     * customer goes to the branch of his choice for a pick up the card or the
     * card is sent to the customer´s preferential address. This operation could
     * be derived from an offer.
     *
     * @param input dto with input fields to validate
     * @return {@link CardProposal}
     */
    CardProposal createCardsCardProposal(InputCreateCardsCardProposal input);
}

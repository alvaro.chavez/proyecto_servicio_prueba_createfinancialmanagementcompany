package com.bbva.pzic.cards.dao.tx.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCardsMaskedToken;
import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMEN2F;
import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMS12F;
import com.bbva.pzic.cards.facade.v0.dto.MaskedToken;

/**
 * Created on 23/07/2019.
 *
 * @author Entelgy
 */
public interface ITxCreateCardsMaskedTokenMapper {

    FormatoMPMEN2F mapIn(InputCreateCardsMaskedToken dtoIn);

    MaskedToken mapOut(FormatoMPMS12F formatOutput);
}

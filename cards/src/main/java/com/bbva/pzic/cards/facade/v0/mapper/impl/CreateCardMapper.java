package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateCardMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.encrypt.impl.CypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 30/01/2018.
 *
 * @author Entelgy
 */
@Mapper("createCardMapperV0")
public class CreateCardMapper implements ICreateCardMapper {

    @Autowired
    private Translator translator;

    @Autowired
    private CypherTool cypherTool;

    @Override
    public InputCreateCard mapIn(Card card) {
        InputCreateCard input = new InputCreateCard();
        DTOIntCard dto;

        if (card.getNumber() == null) {
            dto = mapInMPW1(card);
        } else {
            dto = mapInMPRT(card);
        }

        input.setDtoIntCard(dto);
        return input;
    }

    private DTOIntCard mapInMPW1(Card card) {
        DTOIntCard dto = new DTOIntCard();

        if (card.getCardType() != null) {
            dto.setCardTypeId(translator.translateFrontendEnumValueStrictly("cards.cardType.id", card.getCardType().getId()));
        }

        if (card.getPhysicalSupport() != null) {
            dto.setPhysicalSupportId(translator.translateFrontendEnumValueStrictly("cards.physicalSupport.id", card.getPhysicalSupport().getId()));
        }

        dto.setStatementType(translator.translateFrontendEnumValueStrictly("cards.statementType", card.getStatementType()));
        dto.setSupportContractType(translator.translateFrontendEnumValueStrictly("accounts.delivery.deliveryType", card.getSupportContractType()));
        dto.setCutOffDay(card.getCutOffDay());
        dto.setPaymentMethodEndDay(card.getPaymentMethod() == null ? null : card.getPaymentMethod().getEndDay());
        dto.setTitleId(card.getTitle() == null ? null : card.getTitle().getId());
        mapInDeliveryDestination(dto, card);
        mapInRelatedContract(dto, card);
        mapInGrantedCredit(dto, card);
        mapInMembership(dto, card);
        mapInDestination(dto, card);
        mapInParticipants(dto, card);
        mapInDeliveriesManagement(dto, card);

        if (card.getContractingBusinessAgent() != null)
            dto.setContractingBussinesAgentId(card.getContractingBusinessAgent().getId());
        if (card.getContractingBusinessAgent() != null)
            dto.setContractingBussinesAgentOriginId(card.getContractingBusinessAgent().getOriginId());
        if (card.getMarketBusinessAgent() != null)
            dto.setMarketBusinessAgentId(card.getMarketBusinessAgent().getId());
        if (card.getImage() != null)
            dto.setImageId(card.getImage().getId());
        dto.setOfferId(card.getOfferId());
        if (card.getManagementBranch() != null)
            dto.setManagementBranchId(card.getManagementBranch().getId());
        if (card.getContractingBranch() != null)
            dto.setContractingBranchId(card.getContractingBranch().getId());
        return dto;
    }

    private DTOIntCard mapInMPRT(Card card) {
        DTOIntCard dto = new DTOIntCard();

        if (CollectionUtils.isNotEmpty(card.getRelatedContracts())) {

            RelatedContract relatedContract = card.getRelatedContracts().get(0);

            String relTypeId = relatedContract.getRelationType() == null ? null : relatedContract.getRelationType().getId();

            if ("LINKED_WITH".equals(relTypeId)) {
                dto.setRelatedContractIdLinkedWith(relatedContract.getContractId());
            } else if ("RENEWAL_DUE_TO_DAMAGE".equals(relTypeId) || "RENEWAL_DUE_TO_EXPIRATION".equals(relTypeId)) {
                dto.setRelatedContractIdRenewalDueTo(cypherTool.decrypt(relatedContract.getContractId(), AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V02));
            }
        }

        if (card.getDeliveriesManagement() != null) {
            for (DeliveriesManagement deliveriesManagement : card.getDeliveriesManagement()) {
                if (deliveriesManagement.getServiceType() != null) {
                    if ("SMS_CODE".equals(deliveriesManagement.getServiceType().getId())) {
                        dto.setDeliveriesManagementContactDetailIdSmsCode(deliveriesManagement.getContactDetail().getId());
                        dto.setDeliveryManagementConstant("S");
                    } else {
                        dto.setDeliveryManagementConstant("N");
                    }
                }
            }
        }

        dto.setNumber(card.getNumber());
        dto.setParticipantId(card.getParticipants().get(0).getParticipantId());

        return dto;
    }

    private void mapInDeliveriesManagement(DTOIntCard dto, Card card) {

        if (card.getDeliveriesManagement() == null)
            return;

        for (DeliveriesManagement deliveriesManagement : card.getDeliveriesManagement()) {
            if ("STATEMENT".equals(deliveriesManagement.getServiceType().getId())) {
                dto.setDeliveriesManagementServiceTypeIdStatement(translator.translateFrontendEnumValueStrictly("cards.deliveriesManagement.id", deliveriesManagement.getServiceType().getId()));
                if (deliveriesManagement.getContactDetail() != null)
                    dto.setDeliveriesManagementContactDetailIdStatement(deliveriesManagement.getContactDetail().getId());
                if (deliveriesManagement.getAddress() != null)
                    dto.setDeliveriesManagementAddressIdStatement(deliveriesManagement.getAddress().getId());
                continue;
            }

            if ("STICKER".equals(deliveriesManagement.getServiceType().getId())) {
                dto.setDeliveriesManagementServiceTypeIdSticker(translator.translateFrontendEnumValueStrictly("cards.deliveriesManagement.id", deliveriesManagement.getServiceType().getId()));
                dto.setDeliveriesManagementAddressIdSticker(deliveriesManagement.getAddress().getId());
                continue;
            }

            if ("SMS_CODE".equals(deliveriesManagement.getServiceType().getId())) {
                dto.setDeliveriesManagementContactDetailIdSmsCode(deliveriesManagement.getContactDetail().getId());
                dto.setDeliveryManagementConstant("S");
            } else {
                dto.setDeliveryManagementConstant("N");
            }
        }
    }

    private void mapInParticipants(final DTOIntCard dto, final Card card) {
        if (CollectionUtils.isEmpty(card.getParticipants())) {
            return;
        }

        for (Participant participant : card.getParticipants()) {
            if (participant == null || participant.getParticipantId() == null) {
                continue;
            }
            dto.setParticipantId(participant.getParticipantId());
        }
    }

    @Override
    public CardData mapOut(final Card card) {
        if (card == null) {
            return null;
        }

        CardData cardData = new CardData();
        cardData.setData(card);
        return cardData;
    }

    private void mapInDestination(final DTOIntCard dto, final Card card) {
        if (card.getDelivery() == null || card.getDelivery().getDestination() == null) {
            return;
        }

        dto.setDeliveryDestinationId(card.getDelivery().getDestination().getId());
    }

    private void mapInMembership(final DTOIntCard dto, final Card card) {
        if (CollectionUtils.isEmpty(card.getMemberships())) {
            return;
        }

        dto.setMembershipsNumber(card.getMemberships().get(0).getNumber());
    }

    private void mapInGrantedCredit(final DTOIntCard dto, final Card card) {
        if (CollectionUtils.isEmpty(card.getGrantedCredits())) {
            return;
        }

        GrantedCredit grantedCredit = card.getGrantedCredits().get(0);
        dto.setGrantedCreditsAmount(grantedCredit.getAmount());
        dto.setGrantedCreditsCurrency(grantedCredit.getCurrency());
    }

    private void mapInRelatedContract(final DTOIntCard dto, final Card card) {
        if (CollectionUtils.isEmpty(card.getRelatedContracts())) {
            return;
        }

        RelatedContract relatedContract = card.getRelatedContracts().get(0);

        String relationTypeId = relatedContract.getRelationType() == null ?
                null : relatedContract.getRelationType().getId();

        String relatedContradId = relatedContract.getContractId();
        if ("LINKED_WITH".equals(relationTypeId)) {
            dto.setRelatedContractIdLinkedWith(relatedContradId);
        } else if ("EXPEDITION_DUE_TO_ADDITIONAL".equals(relationTypeId)) {
            dto.setRelatedContractIdExpeditionConstant("S");
            dto.setRelatedContractIdExpedition(cypherTool.decrypt(relatedContradId, AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V0));
        } else {
            dto.setRelatedContractIdExpedition(cypherTool.decrypt(relatedContradId, AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V0));
        }
    }

    private void mapInDeliveryDestination(final DTOIntCard dto, final Card card) {
        if (card.getDelivery() == null) {
            return;
        }
        String destinationId = card.getDelivery().getDestination() == null ? null : card.getDelivery().getDestination().getId();
        String addressId = card.getDelivery().getAddress().getId();
        if ("HOME".equals(destinationId)) {
            dto.setDestinationIdHome(addressId);
            dto.setDeliveryAdressConstant("002");
        } else if ("BRANCH".equals(destinationId)) {
            dto.setDestinationIdBranch(addressId);
            dto.setDeliveryAdressConstant("002");
        }
    }
}

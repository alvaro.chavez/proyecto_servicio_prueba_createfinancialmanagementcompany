package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class InputRefundType {

    @NotNull(groups = ValidationGroup.ReimburseCardTransactionTransactionRefund.class)
    @Size(max = 2, groups = ValidationGroup.ReimburseCardTransactionTransactionRefund.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

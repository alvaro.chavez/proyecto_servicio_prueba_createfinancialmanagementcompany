package com.bbva.pzic.cards.dao.model.mpgg.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMENGG;
import com.bbva.pzic.cards.dao.model.mpgg.PeticionTransaccionMpgg;
import com.bbva.pzic.cards.dao.model.mpgg.RespuestaTransaccionMpgg;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * Created on 9/02/2018.
 *
 * @author Entelgy
 */
@Component("transaccionMpgg")
public class TransaccionMpggMock implements InvocadorTransaccion<PeticionTransaccionMpgg, RespuestaTransaccionMpgg> {

    public static final String TEST_EMPTY = "6666";
    public static final String TEST_NO_RESPONSE = "9999";
    public static final String TEST_NO_PAGINATION = "8888";

    private FormatoMPMEMock formatoMPMEMock;

    @PostConstruct
    public void init() {
        formatoMPMEMock = new FormatoMPMEMock();
    }

    @Override
    public RespuestaTransaccionMpgg invocar(PeticionTransaccionMpgg peticion) throws ExcepcionTransaccion {
        RespuestaTransaccionMpgg response = new RespuestaTransaccionMpgg();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENGG format = peticion.getCuerpo().getParte(FormatoMPMENGG.class);
        final String numberCard = format.getNumtarj();

        if (TEST_NO_RESPONSE.equals(numberCard)) {
            return response;
        }
        try {
            if (TEST_EMPTY.equals(numberCard)) {
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS1GGEmpty()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS2GGEmpty()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS3GGEmpty()));
            } else if (TEST_NO_PAGINATION.equals(numberCard)) {
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS1GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS2GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS1GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS1GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS2GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS2GG()));
            } else {
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS1GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS2GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS1GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS1GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS2GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS2GG()));
                response.getCuerpo().getPartes().add(buildData(formatoMPMEMock.getFormatoMPMS3GG()));
            }
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);

        }

        return response;
    }

    @Override
    public RespuestaTransaccionMpgg invocarCache(PeticionTransaccionMpgg peticion) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private CopySalida buildData(Object object) {
        CopySalida copy = new CopySalida();
        copy.setCopy(object);
        return copy;
    }
}

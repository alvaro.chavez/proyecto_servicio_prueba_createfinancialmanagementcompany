// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.pzic.cards.dao.model.ppcut004_1.Membership;
import java.io.Serializable;

privileged aspect Membership_Roo_Serializable {
    
    declare parents: Membership implements Serializable;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private static final long Membership.serialVersionUID = 1L;
    
}

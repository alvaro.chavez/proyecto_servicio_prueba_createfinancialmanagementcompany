package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "AccumulatedDisposedBalance", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "AccumulatedDisposedBalance", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccumulatedDisposedBalance implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Amount of the accumulated disposed balance.
     */
    private BigDecimal amount;
    /**
     * String based on ISO-4217 for specifying the currency of the balance amount.
     */
    private String currency;
    /**
     * It represents the percentage of the accumulated disposed balance in respect of the exoneration amount.
     * If accumulated disposed balance amount is equals to exoneration amount, this percentage is 100. When the value of this percentage is equals or greater than 100 the customer will avoid to pay the total fee amount otherwise the customer will pay it totally.
     */
    private Integer percentage;
    /**
     * It represents the period where the accumulated disposed balance is calculated.
     */
    private String period;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
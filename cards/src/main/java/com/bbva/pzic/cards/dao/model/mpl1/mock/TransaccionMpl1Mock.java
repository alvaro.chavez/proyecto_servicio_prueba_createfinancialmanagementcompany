package com.bbva.pzic.cards.dao.model.mpl1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMENL1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS1L1;
import com.bbva.pzic.cards.dao.model.mpl1.PeticionTransaccionMpl1;
import com.bbva.pzic.cards.dao.model.mpl1.RespuestaTransaccionMpl1;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 06/02/2017.
 *
 * @author Entelgy
 */
@Component("transaccionMpl1")
public class TransaccionMpl1Mock implements InvocadorTransaccion<PeticionTransaccionMpl1, RespuestaTransaccionMpl1> {

    public static final String TEST_NOT_PAGINATION = "111";
    public static final String TEST_EMPTY = "999";
    public static final String TEST_NULL_IDPAGIN = "100";
    public static final String TEST_NULL_TAMPAGI = "666";

    private FormatsMpl1Mock mock;

    @PostConstruct
    private void init() {
        mock = new FormatsMpl1Mock();
    }

    @Override
    public RespuestaTransaccionMpl1 invocar(PeticionTransaccionMpl1 peticion) {
        final RespuestaTransaccionMpl1 response = new RespuestaTransaccionMpl1();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPMENL1 format = peticion.getCuerpo().getParte(FormatoMPMENL1.class);

        if (TEST_EMPTY.equalsIgnoreCase(format.getIdpagin())) {
            return response;
        }

        try {
            response.getCuerpo().getPartes().addAll(buildDataCopiesFormatCard());

            if (TEST_NOT_PAGINATION.equalsIgnoreCase(format.getIdpagin())) {
                return response;
            }

            response.getCuerpo().getPartes().add(buildPaginationCopy(format.getIdpagin()));

            return response;
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionMpl1 invocarCache(PeticionTransaccionMpl1 peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }

    private List<CopySalida> buildDataCopiesFormatCard() throws IOException {
        List<FormatoMPMS1L1> formats = mock.getFormatoMPMS1L1();
        formats.get(0).setImgtar1("pg=&bin=491910&default_image=false&v=4&width=&height=&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=false");
        formats.get(0).setImgtar2("pg=&bin=491910&default_image=false&v=4&width=&height=&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=true");

        List<CopySalida> copies = new ArrayList<>();
        for (FormatoMPMS1L1 format : formats) {
            CopySalida copy = new CopySalida();
            copy.setCopy(format);
            copies.add(copy);
        }
        return copies;
    }

    private CopySalida buildPaginationCopy(String idPagin) throws IOException {
        CopySalida copy = new CopySalida();
        copy.setCopy(mock.getFormatoMPMS2L1(idPagin));
        return copy;
    }
}

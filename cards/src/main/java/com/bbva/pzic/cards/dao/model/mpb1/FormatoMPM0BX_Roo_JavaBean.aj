// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.mpb1;

import com.bbva.pzic.cards.dao.model.mpb1.FormatoMPM0BX;

privileged aspect FormatoMPM0BX_Roo_JavaBean {
    
    /**
     * Gets idetarj value
     * 
     * @return String
     */
    public String FormatoMPM0BX.getIdetarj() {
        return this.idetarj;
    }
    
    /**
     * Sets idetarj value
     * 
     * @param idetarj
     * @return FormatoMPM0BX
     */
    public FormatoMPM0BX FormatoMPM0BX.setIdetarj(String idetarj) {
        this.idetarj = idetarj;
        return this;
    }
    
    /**
     * Gets codactv value
     * 
     * @return String
     */
    public String FormatoMPM0BX.getCodactv() {
        return this.codactv;
    }
    
    /**
     * Sets codactv value
     * 
     * @param codactv
     * @return FormatoMPM0BX
     */
    public FormatoMPM0BX FormatoMPM0BX.setCodactv(String codactv) {
        this.codactv = codactv;
        return this;
    }
    
    /**
     * Gets indactv value
     * 
     * @return String
     */
    public String FormatoMPM0BX.getIndactv() {
        return this.indactv;
    }
    
    /**
     * Sets indactv value
     * 
     * @param indactv
     * @return FormatoMPM0BX
     */
    public FormatoMPM0BX FormatoMPM0BX.setIndactv(String indactv) {
        this.indactv = indactv;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String FormatoMPM0BX.toString() {
        return "FormatoMPM0BX {" + 
                "idetarj='" + idetarj + '\'' + 
                ", codactv='" + codactv + '\'' + 
                ", indactv='" + indactv + '\'' + "}" + super.toString();
    }
    
}

package com.bbva.pzic.cards.dao.model.mpb2.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2S;
import com.bbva.pzic.cards.dao.model.mpb2.PeticionTransaccionMpb2;
import com.bbva.pzic.cards.dao.model.mpb2.RespuestaTransaccionMpb2;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Invocador de la transacci&oacute;n <code>MPB2</code>
 *
 * @see com.bbva.pzic.cards.dao.model.mpb2.PeticionTransaccionMpb2
 * @see com.bbva.pzic.cards.dao.model.mpb2.RespuestaTransaccionMpb2
 */
@Component("transaccionMpb2")
public class TransaccionMpb2Mock implements InvocadorTransaccion<PeticionTransaccionMpb2, RespuestaTransaccionMpb2> {

    public static final String TEST_EMPTY = "000";

    @Override
    public RespuestaTransaccionMpb2 invocar(PeticionTransaccionMpb2 transaccion) {
        final RespuestaTransaccionMpb2 respuestaTransaccionMpb2 = new RespuestaTransaccionMpb2();
        respuestaTransaccionMpb2.setCodigoRetorno("OK_COMMIT");
        respuestaTransaccionMpb2.setCodigoControl("OK");

        final FormatoMPM0B2 formatoMPM0B2 = transaccion.getCuerpo().getParte(FormatoMPM0B2.class);
        if (TEST_EMPTY.equalsIgnoreCase(formatoMPM0B2.getIdetarj())) {
            return respuestaTransaccionMpb2;
        }
        FormatoMPM0B2S formatoMPM0B2S = new FormatoMPM0B2S();
        formatoMPM0B2S.setIdebloq("LO");
        formatoMPM0B2S.setDesbloq("Additional Information");
        formatoMPM0B2S.setIderazo("reason Id");
        formatoMPM0B2S.setDesrazo("reason Descrip");
        formatoMPM0B2S.setMcnbloq(123456);
        formatoMPM0B2S.setFecbloq(new Date());
        formatoMPM0B2S.setHorablq("06:34:55");
        final CopySalida copySalida = new CopySalida();
        copySalida.setCopy(formatoMPM0B2S);
        respuestaTransaccionMpb2.getCuerpo().getPartes().add(copySalida);
        return respuestaTransaccionMpb2;
    }

    @Override
    public RespuestaTransaccionMpb2 invocarCache(PeticionTransaccionMpb2 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

package com.bbva.pzic.cards.dao.model.mpdc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPDC</code>
 *
 * @see PeticionTransaccionMpdc
 * @see RespuestaTransaccionMpdc
 */
@Component
public class TransaccionMpdc implements InvocadorTransaccion<PeticionTransaccionMpdc,RespuestaTransaccionMpdc> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpdc invocar(PeticionTransaccionMpdc transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpdc.class, RespuestaTransaccionMpdc.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpdc invocarCache(PeticionTransaccionMpdc transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpdc.class, RespuestaTransaccionMpdc.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}

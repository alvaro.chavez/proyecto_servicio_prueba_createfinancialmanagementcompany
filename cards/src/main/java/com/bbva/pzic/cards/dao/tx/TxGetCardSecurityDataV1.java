package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMENDV;
import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMS1DV;
import com.bbva.pzic.cards.dao.model.mcdv.PeticionTransaccionMcdv;
import com.bbva.pzic.cards.dao.model.mcdv.RespuestaTransaccionMcdv;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardSecurityDataV1Mapper;
import com.bbva.pzic.cards.facade.v1.dto.SecurityData;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component("txGetCardSecurityDataV1")
public class TxGetCardSecurityDataV1 extends SingleOutputFormat<InputGetCardSecurityData, FormatoMPMENDV, List<SecurityData>, FormatoMPMS1DV> {

    @Resource(name = "txGetCardSecurityDataV1Mapper")
    private ITxGetCardSecurityDataV1Mapper mapper;

    @Autowired
    public TxGetCardSecurityDataV1(@Qualifier("transaccionMcdv") InvocadorTransaccion<PeticionTransaccionMcdv, RespuestaTransaccionMcdv> transaction) {
        super(transaction, PeticionTransaccionMcdv::new, ArrayList::new, FormatoMPMS1DV.class);
    }

    @Override
    protected FormatoMPMENDV mapInput(InputGetCardSecurityData input) {
        return mapper.mapIn(input);
    }

    @Override
    protected List<SecurityData> mapFirstOutputFormat(FormatoMPMS1DV format, InputGetCardSecurityData input, List<SecurityData> dtoOut) {
        return mapper.mapOut(format, dtoOut);
    }
}

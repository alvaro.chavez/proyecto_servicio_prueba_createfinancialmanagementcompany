package com.bbva.pzic.cards.util;

/**
 * @author Entelgy
 */
public final class Constants {

    public static final String PROPERTY_IMAGE_WALLET_HOSTNAME = "servicing.imagen.wallet.hostname";
    public static final String PROPERTY_DOCUMENT_TYPE_ID = "documentType.id";
    public static final String PROPERTY_CONTACT_CONTACT_DETAIL_TYPE = "contact.contactDetailType";
    public static final String PROPERTY_PROPOSAL_STATUS = "proposalStatus";

    public static final String HEADER_USER_AGENT = "User-Agent";
    public static final String HEADER_BCS_OPERATION_TRACER = "BCS-Operation-Tracer";
    public static final String AAP = "aap";
    public static final String SPECIFIC = "SPECIFIC";
    public static final String STORED = "STORED";

    public static final String BRANCH = "BRANCH";
    public static final String CUSTOM = "CUSTOM";
    public static final String HOME = "HOME";

    public static final String EMAIL = "EMAIL";
    public static final String LANDLINE = "LANDLINE";
    public static final String MOBILE = "MOBILE";
    public static final String SOCIAL_MEDIA = "SOCIAL_MEDIA";
    public static final String LOCATION_HTTP_HEADER = "Location";

    public static final String FINANCIAL_STATEMENT_CONDITION_FORMAT_TYPE_FRONTEND = "AMOUNT";
    public static final String FINANCIAL_STATEMENT_CONDITION_FORMAT_TYPE_BACKEND = "M";
    public static final String FINANCIAL_STATEMENT_SPECIFIC_AMOUNTS_ID_P_FRONTEND = "PERIOD_BALANCE";
    public static final String FINANCIAL_STATEMENT_SPECIFIC_AMOUNTS_ID_P_BACKEND = "P";
    public static final String FINANCIAL_STATEMENT_SPECIFIC_AMOUNTS_ID_PL_FRONTEND = "PENDING_LIQUIDATION";
    public static final String FINANCIAL_STATEMENT_SPECIFIC_AMOUNTS_ID_PL_BACKEND = "PL";
    public static final String FINANCIAL_STATEMENT_SPECIFIC_AMOUNTS_ID_PB_FRONTEND = "PREVIOUS_PERIOD_BALANCE";
    public static final String FINANCIAL_STATEMENT_SPECIFIC_AMOUNTS_ID_PB_BACKEND = "PB";
    public static final String FINANCIAL_STATEMENT_RATE_UNIT_RATE_TYPE_PERCENTAGE_FRONTEND = "PERCENTAGE";
    public static final String FINANCIAL_STATEMENT_RATE_UNIT_RATE_TYPE_PERCENTAGE_BACKEND = "P";
    public static final String FINANCIAL_STATEMENT_RATE_UNIT_RATE_TYPE_AMOUNT_FRONTEND = "AMOUNT";
    public static final String FINANCIAL_STATEMENT_RATE_UNIT_RATE_TYPE_AMOUNT_BACKEND = "M";
    public static final String FINANCIAL_STATEMENT_SPECIFIC_AMOUNTS_ID_NP_FRONTEND = "NEXT_PERIOD_BALANCE";
    public static final String FINANCIAL_STATEMENT_SPECIFIC_AMOUNTS_ID_NP_BACKEND = "NP";

    public static final String REFUND = "REFUND";
    public static final String APPROVED = "APPROVED";

    public static final String DEFAULT_TIME = "00:00:00";
    public static final String DEFAULT_DATE = "yyyy-MM-dd";
    public static final String DEFAULT_LONG_DATE_FORMAT = "yyyy-MM-ddHH:mm:ss";


    private Constants() {
    }
}

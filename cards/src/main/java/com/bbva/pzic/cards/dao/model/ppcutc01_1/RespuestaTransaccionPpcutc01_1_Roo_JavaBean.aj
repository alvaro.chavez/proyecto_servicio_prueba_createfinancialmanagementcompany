// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.ppcutc01_1;

import com.bbva.pzic.cards.dao.model.ppcutc01_1.Entityout;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.RespuestaTransaccionPpcutc01_1;

privileged aspect RespuestaTransaccionPpcutc01_1_Roo_JavaBean {
    
    /**
     * Gets codigoAviso value
     * 
     * @return String
     */
    public String RespuestaTransaccionPpcutc01_1.getCodigoAviso() {
        return this.codigoAviso;
    }
    
    /**
     * Sets codigoAviso value
     * 
     * @param codigoAviso
     * @return RespuestaTransaccionPpcutc01_1
     */
    public RespuestaTransaccionPpcutc01_1 RespuestaTransaccionPpcutc01_1.setCodigoAviso(String codigoAviso) {
        this.codigoAviso = codigoAviso;
        return this;
    }
    
    /**
     * Gets descripcionAviso value
     * 
     * @return String
     */
    public String RespuestaTransaccionPpcutc01_1.getDescripcionAviso() {
        return this.descripcionAviso;
    }
    
    /**
     * Sets descripcionAviso value
     * 
     * @param descripcionAviso
     * @return RespuestaTransaccionPpcutc01_1
     */
    public RespuestaTransaccionPpcutc01_1 RespuestaTransaccionPpcutc01_1.setDescripcionAviso(String descripcionAviso) {
        this.descripcionAviso = descripcionAviso;
        return this;
    }
    
    /**
     * Gets aplicacionAviso value
     * 
     * @return String
     */
    public String RespuestaTransaccionPpcutc01_1.getAplicacionAviso() {
        return this.aplicacionAviso;
    }
    
    /**
     * Sets aplicacionAviso value
     * 
     * @param aplicacionAviso
     * @return RespuestaTransaccionPpcutc01_1
     */
    public RespuestaTransaccionPpcutc01_1 RespuestaTransaccionPpcutc01_1.setAplicacionAviso(String aplicacionAviso) {
        this.aplicacionAviso = aplicacionAviso;
        return this;
    }
    
    /**
     * Gets codigoRetorno value
     * 
     * @return String
     */
    public String RespuestaTransaccionPpcutc01_1.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    /**
     * Sets codigoRetorno value
     * 
     * @param codigoRetorno
     * @return RespuestaTransaccionPpcutc01_1
     */
    public RespuestaTransaccionPpcutc01_1 RespuestaTransaccionPpcutc01_1.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
        return this;
    }
    
    /**
     * Gets entityout value
     * 
     * @return Entityout
     */
    public Entityout RespuestaTransaccionPpcutc01_1.getEntityout() {
        return this.entityout;
    }
    
    /**
     * Sets entityout value
     * 
     * @param entityout
     * @return RespuestaTransaccionPpcutc01_1
     */
    public RespuestaTransaccionPpcutc01_1 RespuestaTransaccionPpcutc01_1.setEntityout(Entityout entityout) {
        this.entityout = entityout;
        return this;
    }
    
}

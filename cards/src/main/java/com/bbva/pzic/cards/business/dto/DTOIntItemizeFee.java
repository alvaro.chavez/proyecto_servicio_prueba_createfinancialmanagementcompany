package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntItemizeFee {

    @NotNull(groups = {ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    @Size(max = 2, groups = ValidationGroup.CreateCardsProposal.class)
    private String feeType;

    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    @Size(max = 20, groups = ValidationGroup.CreateCardsProposal.class)
    private String name;

    @Valid
    @NotNull(groups = ValidationGroup.CreateCardsProposal.class)
    private DTOIntMode mode;

    @Valid
    @NotNull(groups = {ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.CreateCardProposal.class,
            ValidationGroup.ModifyCardProposal.class})
    private DTOIntItemizeFeeUnit itemizeFeeUnit;

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntMode getMode() {
        return mode;
    }

    public void setMode(DTOIntMode mode) {
        this.mode = mode;
    }

    public DTOIntItemizeFeeUnit getItemizeFeeUnit() {
        return itemizeFeeUnit;
    }

    public void setItemizeFeeUnit(DTOIntItemizeFeeUnit itemizeFeeUnit) {
        this.itemizeFeeUnit = itemizeFeeUnit;
    }
}
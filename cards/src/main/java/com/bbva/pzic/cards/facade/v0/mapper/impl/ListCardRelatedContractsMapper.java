package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntRelatedContractsSearchCriteria;
import com.bbva.pzic.cards.facade.v0.dto.RelatedContracts;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardRelatedContractsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ListCardRelatedContractsMapper implements IListCardRelatedContractsMapper {

    private static final Log LOG = LogFactory.getLog(ListCardRelatedContractsMapper.class);

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public DTOIntRelatedContractsSearchCriteria mapIn(final String cardId) {
        LOG.info("----- ListCardRelatedContractsMapper.mapIn -----");
        DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria = new DTOIntRelatedContractsSearchCriteria();
        dtoIntRelatedContractsSearchCriteria.setCardId(cypherTool.decrypt(cardId, AbstractCypherTool.CARDID));
        return dtoIntRelatedContractsSearchCriteria;
    }

    @Override
    public ServiceResponse<List<RelatedContracts>> mapOut(final List<RelatedContracts> data) {
        LOG.info("----- ListCardRelatedContractsMapper.mapOut -----");
        if (CollectionUtils.isEmpty(data)) {
            return null;
        }

        return ServiceResponse.data(data).build();
    }
}

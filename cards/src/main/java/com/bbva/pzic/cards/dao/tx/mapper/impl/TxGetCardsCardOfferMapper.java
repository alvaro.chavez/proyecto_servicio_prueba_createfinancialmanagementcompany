package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputGetCardsCardOffer;
import com.bbva.pzic.cards.canonic.Offer;
import com.bbva.pzic.cards.dao.model.mpg9.FormatoMPMENG9;
import com.bbva.pzic.cards.dao.model.mpg9.FormatoMPMS1G9;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardsCardOfferMapper;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.converter.builtin.BooleanToStringConverter;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 09/11/2018.
 *
 * @author Entelgy
 */
@Mapper("txGetCardsCardOfferMapper")
public class TxGetCardsCardOfferMapper extends ConfigurableMapper implements ITxGetCardsCardOfferMapper {


    public static final String OUTPUT_CARDS_OFFERTYPE_ID = "cards.offerType.id";
    private static final String DIVISA = "divisa";

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.getConverterFactory().registerConverter(new BooleanToStringConverter());

        factory.classMap(InputGetCardsCardOffer.class, FormatoMPMENG9.class)
                .field("cardId", "numtarj")
                .field("offerId", "numofer")
                .register();

        factory.classMap(FormatoMPMS1G9.class, Offer.class)
                .field("numofer", "id")
                .field("idtofer", "offerType.id")
                .field("desofer", "offerType.name")
                .field("idorige", "origin.id")
                .field("descori", "origin.description")
                .field("idedita", "isEditable")
                .field("limmini", "details.minimumAmount[0].amount")
                .field(DIVISA, "details.minimumAmount[0].currency")
                .field("limmaxi", "details.maximumAmount[0].amount")
                .field(DIVISA, "details.maximumAmount[0].currency")
                .field("limofer", "details.suggestedAmount[0].amount")
                .field(DIVISA, "details.suggestedAmount[0].currency")
                .register();
    }

    @Override
    public FormatoMPMENG9 mapIn(InputGetCardsCardOffer input) {
        return this.map(input, FormatoMPMENG9.class);
    }

    @Override
    public Offer mapOut(FormatoMPMS1G9 input) {
        if (input == null) {
            return null;
        }

        if (input.getIdtofer() != null) {
            String idtofer = enumMapper.getEnumValue(OUTPUT_CARDS_OFFERTYPE_ID, input.getIdtofer());
            input.setIdtofer(idtofer);
        }

        return this.map(input, Offer.class);
    }

}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.jee.arq.spring.core.servicing.utils.Pagination;
import com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions;
import com.bbva.pzic.cards.canonic.Transaction;
import com.bbva.pzic.cards.canonic.TransactionsData;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMENL2;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMS1L2;
import com.bbva.pzic.cards.dao.model.mpl2.FormatoMPMS2L2;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardTransactionsMapper;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.converter.builtin.LongToIntegerConverter;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Entelgy
 */
@Mapper
public class TxListCardTransactionsMapper extends ConfigurableMapper implements ITxListCardTransactionsMapper {

    private static final Log LOG = LogFactory.getLog(TxListCardTransactionsMapper.class);
    private static final String DATE_FORMAT_SHORT = "yyyy-MM-dd";

    @Autowired
    private Translator translator;

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    protected void configure(MapperFactory factory) {

        super.configure(factory);

        factory.getConverterFactory().registerConverter(new LongToIntegerConverter());

        factory.classMap(DTOInputListCardTransactions.class, FormatoMPMENL2.class)
                .field("cardId", "numtarj")
                .field("paginationKey", "idpagin")
                .field("pageSize", "tampagi")
                .register();

        factory.classMap(FormatoMPMS2L2.class, Pagination.class)
                .field("idpagin", "nextPage")
                .field("tampagi", "pageSize")
                .register();

        factory.classMap(FormatoMPMS1L2.class, Transaction.class)
                .field("numoper", "id")
                .field("desoper", "concept")
                .field("impmori", "originAmount.amount")
                .field("monmori", "originAmount.currency")
                .field("impmtit", "localAmount.amount")
                .field("mopmtit", "localAmount.currency")
                .field("estoper", "status.id")
                .field("destope", "status.name")
                .field("numconv", "contract.number")
                .field("idtcore", "contract.numberType.id")
                .field("detcore", "contract.numberType.name")
                .field("idtipop", "transactionType.id")
                .field("notipop", "transactionType.name")
                .field("codoper", "transactionType.internalCode.id")
                .field("nomcod", "transactionType.internalCode.name")
                .field("idtipfn", "financingType.id")
                .field("detipfn", "financingType.name")
                .register();
    }

    /**
     * @see com.bbva.pzic.cards.dao.tx.mapper.ITxListCardTransactionsMapper#mapInput(com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions)
     */
    @Override
    public FormatoMPMENL2 mapInput(DTOInputListCardTransactions dtoInputListCardTransactions) {
        LOG.info("... called method TxListTransactionsMapper.mapInput ...");
        FormatoMPMENL2 format = map(dtoInputListCardTransactions, FormatoMPMENL2.class);
        format.setFechini(formatToShortDate(dtoInputListCardTransactions.getFromOperationDate()));
        format.setFechfin(formatToShortDate(dtoInputListCardTransactions.getToOperationDate()));
        return format;
    }

    private Date formatToShortDate(final String date) {
        if (StringUtils.isEmpty(date)) {
            return null;
        }

        try {
            return DateUtils.toDate(date, DATE_FORMAT_SHORT);
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

    /**
     * @see com.bbva.pzic.cards.dao.tx.mapper.ITxListCardTransactionsMapper#mapOutputList(FormatoMPMS1L2, TransactionsData, DTOInputListCardTransactions)
     */
    @Override
    public TransactionsData mapOutputList(final FormatoMPMS1L2 formatoMPMS1L2, final TransactionsData transactionsData, final DTOInputListCardTransactions dtoIn) {
        LOG.info("... called method TxListTransactionsMapper.mapOutputList ...");
        final Transaction transaction = map(formatoMPMS1L2, Transaction.class);

        if (StringUtils.isEmpty(formatoMPMS1L2.getHoraope())) {
            transaction.setOperationDate(FunctionUtils.buildDatetime(formatoMPMS1L2.getFechope(), Constants.DEFAULT_TIME));
        } else if (formatoMPMS1L2.getFechope() != null) {
            transaction.setOperationDate(FunctionUtils.buildDatetime(formatoMPMS1L2.getFechope(), formatTime(formatoMPMS1L2.getHoraope())));
        }

        transaction.setAccountedDate(FunctionUtils.buildDatetime(formatoMPMS1L2.getFechcon(), Constants.DEFAULT_TIME));
        transaction.setValuationDate(FunctionUtils.buildDatetime(formatoMPMS1L2.getFechcon(), Constants.DEFAULT_TIME));
        if (transaction.getStatus() != null) {
            transaction.getStatus().setId(translator.translateBackendEnumValueStrictly("transactions.status.id",
                    transaction.getStatus().getId()));
        }
        if (transaction.getContract() != null
                && transaction.getContract().getNumberType() != null) {
            transaction.getContract().getNumberType().setId(
                    translator.translateBackendEnumValueStrictly("contracts.numberType.id", transaction.getContract().getNumberType().getId()));
        }
        if (transaction.getContract() != null
                && transaction.getContract().getNumber() != null) {
            transaction.getContract().setId(cypherTool.encrypt(transaction.getContract().getNumber(), AbstractCypherTool.NUCOREL, dtoIn.getRegistryId()));
        }
        if (transaction.getTransactionType() != null) {
            transaction.getTransactionType().setId(
                    translator.translateBackendEnumValueStrictly("transactions.transactionType.id", transaction.getTransactionType().getId()));
        }
        if (transaction.getFinancingType() != null) {
            if (dtoIn.isHaveFinancingType()) {
                transaction.getFinancingType().setId(
                        translator.translateBackendEnumValueStrictly("transactions.financingType.id", transaction.getFinancingType().getId()));
            } else {
                transaction.setFinancingType(null);
            }
        }

        if (transactionsData.getData() == null) {
            transactionsData.setData(new ArrayList<>());
        }
        transactionsData.getData().add(transaction);
        return transactionsData;
    }

    private String formatTime(final String time) {
        if (time.length() != 6) {
            throw new BusinessServiceException(Errors.WRONG_DATE);
        }
        return time.substring(0, 2) + ":" + time.substring(2, 4) + ":" + time.substring(4);
    }

    /**
     * @see com.bbva.pzic.cards.dao.tx.mapper.ITxListCardTransactionsMapper#mapOutputPagination(FormatoMPMS2L2, TransactionsData)
     */
    @Override
    public TransactionsData mapOutputPagination(FormatoMPMS2L2 formatoMPMS2L2, TransactionsData transactionsData) {
        LOG.info("... called method TxListTransactionsMapper.mapOutputPagination ...");
        transactionsData.setPagination(map(formatoMPMS2L2, Pagination.class));
        return transactionsData;
    }
}

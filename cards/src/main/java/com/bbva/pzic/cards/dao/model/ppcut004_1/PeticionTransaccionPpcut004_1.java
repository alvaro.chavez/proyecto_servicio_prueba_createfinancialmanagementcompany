package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PPCUT004</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPpcut004_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPpcut004_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: listCardProposal - PPCUT004-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PPCUT004&quot; application=&quot;PPCU&quot; version=&quot;01&quot; country=&quot;PE&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;status&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;paginationIn&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.PaginationInDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;pageSize&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;paginationKey&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;list name=&quot;EntityOut&quot; order=&quot;1&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;data&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.CardProposalDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;36&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;cardType&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;11&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;product&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ProductDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;subproduct&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.SubproductDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;physicalSupport&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;14&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;list name=&quot;deliveries&quot; order=&quot;5&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;delivery&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.DeliveryDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;serviceType&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;deliveryContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;emailContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDetailDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactDetailType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;address&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;address&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AddressDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;addressType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;location&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.LocationDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;list name=&quot;addressComponents&quot; order=&quot;1&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;addressComponent&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AddressComponentDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;list name=&quot;componentTypes&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;componentType&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/list&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;code&quot; type=&quot;String&quot; size=&quot;7&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;additionalInformation&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;80&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;destination&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.DestinationDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;branch&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;paymentMethod&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.PaymentMethodDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;6&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;frecuency&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FrecuencyDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;daysOfMonth&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.DaysOfMonthDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;day&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;cutOffDay&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;list name=&quot;grantedCredits&quot; order=&quot;7&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;grantedCredit&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.AmountDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;specificContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;8&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;contactType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;mobileContact&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactDetailDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contactDetailType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;phoneCompany&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementTypeDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;rates&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.RateDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;9&quot;&gt;
 * &lt;list name=&quot;itemizeRates&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeRate&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;rateType&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;itemizeRatesUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeRateUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitRateType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;percentage&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;fees&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.FeeDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot;
 * order=&quot;10&quot;&gt;
 * &lt;list name=&quot;itemizeFees&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;itemizeFee&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;feeType&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;itemizeFeeUnit&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ItemizeFeeUnitDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;1&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;unitType&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;list name=&quot;additionalProducts&quot; order=&quot;11&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;additionalProduct&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.AdditionalProductDTO&quot;
 * artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;productType&quot; type=&quot;String&quot; size=&quot;9&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;membership&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;12&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;operationDate&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;14&quot; name=&quot;status&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;image&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ImageDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;15&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;url&quot; type=&quot;String&quot; size=&quot;200&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;offerId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;contactability&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ContactabilityDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;0&quot; order=&quot;16&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;scheduleTimes&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ScheduleTimesDTO&quot; artifactId=&quot;PPCUC001&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;startTime&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;endTime&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;list name=&quot;participants&quot; order=&quot;17&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;participant&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.ParticipantDTO&quot; artifactId=&quot;PPCUC001&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;16&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;isCustomer&quot; mandatory=&quot;1&quot; type=&quot;Boolean&quot; size=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;personType&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;6&quot; /&gt;
 * &lt;dto order=&quot;4&quot; name=&quot;participantType&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.ppcu.dto.proposals.commons.ElementDTO&quot; artifactId=&quot;PPCUC001&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;firstName&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;20&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;lastName&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;30&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;parameter order=&quot;18&quot; name=&quot;notificationsByOperation&quot; type=&quot;Boolean&quot; size=&quot;0&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;dto name=&quot;paginationOut&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.PaginationOutDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;page&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;totalPages&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;totalElements&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;pageSize&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;dto name=&quot;links&quot; package=&quot;com.bbva.ppcu.dto.proposals.dtos.LinkDTO&quot; artifactId=&quot;PPCUC001&quot; mandatory=&quot;0&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;first&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;last&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;previous&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;next&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Listado de Solicitud de Tarjeta&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPpcut004_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PPCUT004",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPpcut004_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPpcut004_1 {
		
		/**
	 * <p>Campo <code>status</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "status", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String status;
	
	/**
	 * <p>Campo <code>paginationIn</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "paginationIn", tipo = TipoCampo.DTO)
	private Paginationin paginationin;
	
}
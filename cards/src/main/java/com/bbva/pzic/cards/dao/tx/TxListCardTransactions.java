package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions;
import com.bbva.pzic.cards.canonic.TransactionsData;
import com.bbva.pzic.cards.dao.model.mpl2.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardTransactionsMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author Entelgy
 */
@Component("txListCardTransactions")
public class TxListCardTransactions
        extends DoubleOutputFormat<DTOInputListCardTransactions, FormatoMPMENL2, TransactionsData, FormatoMPMS1L2, FormatoMPMS2L2> {

    @Autowired
    private ITxListCardTransactionsMapper txListTransactionsMapper;

    @Autowired
    public TxListCardTransactions(@Qualifier("transaccionMpl2") InvocadorTransaccion<PeticionTransaccionMpl2, RespuestaTransaccionMpl2> transaction) {
        super(transaction, PeticionTransaccionMpl2::new, TransactionsData::new, FormatoMPMS1L2.class, FormatoMPMS2L2.class);
    }

    @Override
    protected FormatoMPMENL2 mapInput(DTOInputListCardTransactions dtoInputListCardTransactions) {
        return txListTransactionsMapper.mapInput(dtoInputListCardTransactions);
    }

    @Override
    protected TransactionsData mapFirstOutputFormat(FormatoMPMS1L2 formatoMPMS1L2, DTOInputListCardTransactions dtoIn, TransactionsData dtoOut) {
        return txListTransactionsMapper.mapOutputList(formatoMPMS1L2, dtoOut, dtoIn);
    }

    @Override
    protected TransactionsData mapSecondOutputFormat(FormatoMPMS2L2 formatoMPMS2L2, DTOInputListCardTransactions dtoIn, TransactionsData dtoOut) {
        return txListTransactionsMapper.mapOutputPagination(formatoMPMS2L2, dtoOut);
    }
}
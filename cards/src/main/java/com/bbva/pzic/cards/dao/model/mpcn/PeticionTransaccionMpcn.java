package com.bbva.pzic.cards.dao.model.mpcn;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>MPCN</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpcn</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpcn</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPCN.D1201023
 * MPCNCANCELACION DE CONTRATOS           MP        MP2CMPCN     01 MPME1NC             MPCN  NN3000CNNNNN    SSTN    C   NNNNSNNN  NN                2020-10-22XP86064 2020-10-2312.32.00XP86064 2020-10-22-16.09.20.595296XP86064 0001-01-010001-01-01
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPME1NC.D1201023
 * MPME1NC �ENTRADA CANC. CONTRATOS       �F�02�00021�01�00001�CODTAR �NUMERO DE TARJETA   �A�019�0�R�        �
 * MPME1NC �ENTRADA CANC. CONTRATOS       �F�02�00021�02�00020�CODRAZ �CODIGO RAZON        �A�002�0�R�        �
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1NC.D1201023
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�01�00001�NUMOPE �NUMERO DE OPERACION �A�010�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�02�00011�FECHOPE�FECHA DE OPERACION  �A�010�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�03�00021�FECHCAN�FECHA DE CANCELACION�A�010�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�04�00031�ESTCAN �ESTADO CANCELACION  �A�002�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�05�00033�CODRAZ �CODIGO RAZON        �A�002�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�06�00035�NUMTAR �NUMERO TARJETA      �A�019�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�07�00054�IDTNUTA�IDENTIFICADOR       �A�003�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�08�00057�DETNUTA�DESCRIPCION         �A�030�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�09�00087�TIPPROD�TIPO TARJETA        �A�002�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�10�00089�DESCPRO�DESCRIPCION         �A�030�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�11�00119�DIVTAR �DIVISA              �A�004�0�S�        �
 * MPMS1NC �SALIDA - CANC. CONTRATOS      �X�12�00127�12�00123�INMOPR �INDICADOR MONEDA    �A�005�0�S�        �
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPCN.D1201023
 * MPCNMPMS1NC MPMCS1NCMP2CMPCN1S                             XP86064 2020-10-22-19.14.51.371112XP86064 2020-10-22-19.14.51.371182
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionMpcn
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPCN",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpcn.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPME1NC.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionMpcn implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}
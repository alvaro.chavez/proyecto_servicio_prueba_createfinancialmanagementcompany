package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "CashWithdrawal", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "CashWithdrawal", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class CashWithdrawal extends TransactionTypeAgreement implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Human readable information about transaction number.
     */
    private String authorizationNumber;
    /**
     * ATM where the withdrawal was made effective. DISCLAIMER: It will be
     * forcely fulfilled only in case of ATM withdrawals.
     */
    private Atm atm;
    /**
     * Office where the withdrawal was made effective. It is also the office to
     * which the ATM belongs when the withdrawal is performed using an ATM.
     * Sometimes this information is not sent by other entities and cannot be
     * shown to customers. DISCLAIMER: It must be informed when the withdrawal
     * operations are performed at branches.
     */
    private Branch branch;
    /**
     * Bank to which the branch (and ATM when it applies) belongs. Sometimes
     * this information is not sent by other entities and cannot be shown to
     * customers.
     */
    private Bank bank;
    /**
     * Flag that indicates wether the withdrawal is made against the available
     * credit that customer has.
     */
    private Boolean isCredit;
    /**
     * Contract that is linked to the contrat which is being queried on
     * transaction. This attribute must be informed when the operation is
     * performed using a debit card and will have the information about the
     * account that holds that card. When the operation is performed using an
     * account, this attribue can\'t be informed because in those cases the
     * transaction has no direct reflection on other contracts. This is also
     * applicable to credit cards that don\'t have direct debit selected on its
     * payment method.
     */
    private Contract linkedContract;
    /**
     * Origin of the withdrawal
     */
    private String originType;
    /**
     * Number of total installments associated with the movement.
     */
    private BigDecimal totalInstallmentsNumber;
    /**
     * Interest rate associated with the movement.
     */
    private String interestRate;

    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }

    public Atm getAtm() {
        return atm;
    }

    public void setAtm(Atm atm) {
        this.atm = atm;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Boolean getIsCredit() {
        return isCredit;
    }

    public void setIsCredit(Boolean isCredit) {
        this.isCredit = isCredit;
    }

    public Contract getLinkedContract() {
        return linkedContract;
    }

    public void setLinkedContract(Contract linkedContract) {
        this.linkedContract = linkedContract;
    }

    public String getOriginType() {
        return originType;
    }

    public void setOriginType(String originType) {
        this.originType = originType;
    }

    public BigDecimal getTotalInstallmentsNumber() {
        return totalInstallmentsNumber;
    }

    public void setTotalInstallmentsNumber(BigDecimal totalInstallmentsNumber) {
        this.totalInstallmentsNumber = totalInstallmentsNumber;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }
}
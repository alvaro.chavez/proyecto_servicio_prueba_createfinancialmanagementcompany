package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntRelatedContractsSearchCriteria;
import com.bbva.pzic.cards.facade.v0.dto.RelatedContracts;

import java.util.List;

public interface IListCardRelatedContractsMapper {

    DTOIntRelatedContractsSearchCriteria mapIn(String cardId);

    ServiceResponse<List<RelatedContracts>> mapOut(List<RelatedContracts> dtoIntRelatedContracts);
}

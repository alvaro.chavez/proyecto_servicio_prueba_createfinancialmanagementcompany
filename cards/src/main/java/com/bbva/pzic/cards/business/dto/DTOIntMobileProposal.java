package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 27/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntMobileProposal {

    @NotNull(groups = {ValidationGroup.CreateCardProposal.class})
    private String number;
    @NotNull(groups = {ValidationGroup.CreateCardProposal.class})
    private String contactDetailType;
    @NotNull(groups = {ValidationGroup.CreateCardProposal.class})
    @Valid
    private DTOIntPhoneCompanyProposal phoneCompany;

    public String getContactDetailType() {
        return contactDetailType;
    }

    public void setContactDetailType(String contactDetailType) {
        this.contactDetailType = contactDetailType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DTOIntPhoneCompanyProposal getPhoneCompany() {
        return phoneCompany;
    }

    public void setPhoneCompany(DTOIntPhoneCompanyProposal phoneCompany) {
        this.phoneCompany = phoneCompany;
    }
}
package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 11/19/2019.
 *
 * @author Entelgy
 */
public class DTOIntFrequency {
    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    private String id;

    @Valid
    private DTOIntDaysOfMonth daysOfMonth;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntDaysOfMonth getDaysOfMonth() {
        return daysOfMonth;
    }

    public void setDaysOfMonth(DTOIntDaysOfMonth daysOfMonth) {
        this.daysOfMonth = daysOfMonth;
    }
}

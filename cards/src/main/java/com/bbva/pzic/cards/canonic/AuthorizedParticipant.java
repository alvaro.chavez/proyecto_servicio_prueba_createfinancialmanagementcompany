package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "authorizedParticipant", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "authorizedParticipant", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AuthorizedParticipant implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identification document. The document must be sent when the client goes
     * to consult the financial products offered by the bank.
     */
    private IdentityDocument identityDocument;
    /**
     * Name of the person.
     */
    private String firstName;
    /**
     * Person's name occurring between the firstName and family names, as a
     * second or additonal given name.
     */
    private String middleName;
    /**
     * Family name. Name(s) written after the first name and the middle name.
     * First surname.
     */
    private String lastName;
    /**
     * Second surname. Mother's last name.
     */
    private String secondLastName;
    /**
     * Object where the profession of the person is stored.
     */
    private Profession profession;
    /**
     * Name of marital status.
     */
    private MaritalStatus maritalStatus;
    /**
     * Relationship between the owner and the beneficiary.
     */
    private RelationType relationType;
    /**
     * The customer contact information.
     */
    private List<ParticipantContactDetail> contactDetails;

    public IdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(IdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public RelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(RelationType relationType) {
        this.relationType = relationType;
    }

    public List<ParticipantContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ParticipantContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }
}
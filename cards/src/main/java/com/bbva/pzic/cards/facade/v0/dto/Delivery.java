package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "Delivery", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "Delivery", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Delivery implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private ServiceType serviceType;

    private Contact contact;

    private Address address;

    private Destination destination;

    private Branch branch;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}

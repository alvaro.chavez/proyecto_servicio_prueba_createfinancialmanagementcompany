package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 9/10/2017.
 *
 * @author Entelgy
 */
public class InputModifyCardActivations {

    @NotNull(groups = ValidationGroup.ModifyCardActivations.class)
    @Size(max = 19, groups = ValidationGroup.ModifyCardActivations.class)
    private String cardId;

    private DTOIntActivation dtoIntActivationPosition1;
    private DTOIntActivation dtoIntActivationPosition2;
    private DTOIntActivation dtoIntActivationPosition3;
    private DTOIntActivation dtoIntActivationPosition4;
    private DTOIntActivation dtoIntActivationPosition5;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public DTOIntActivation getDtoIntActivationPosition1() {
        return dtoIntActivationPosition1;
    }

    public void setDtoIntActivationPosition1(DTOIntActivation dtoIntActivationPosition1) {
        this.dtoIntActivationPosition1 = dtoIntActivationPosition1;
    }

    public DTOIntActivation getDtoIntActivationPosition2() {
        return dtoIntActivationPosition2;
    }

    public void setDtoIntActivationPosition2(DTOIntActivation dtoIntActivationPosition2) {
        this.dtoIntActivationPosition2 = dtoIntActivationPosition2;
    }

    public DTOIntActivation getDtoIntActivationPosition3() {
        return dtoIntActivationPosition3;
    }

    public void setDtoIntActivationPosition3(DTOIntActivation dtoIntActivationPosition3) {
        this.dtoIntActivationPosition3 = dtoIntActivationPosition3;
    }

    public DTOIntActivation getDtoIntActivationPosition4() {
        return dtoIntActivationPosition4;
    }

    public void setDtoIntActivationPosition4(DTOIntActivation dtoIntActivationPosition4) {
        this.dtoIntActivationPosition4 = dtoIntActivationPosition4;
    }

    public DTOIntActivation getDtoIntActivationPosition5() {
        return dtoIntActivationPosition5;
    }

    public void setDtoIntActivationPosition5(DTOIntActivation dtoIntActivationPosition5) {
        this.dtoIntActivationPosition5 = dtoIntActivationPosition5;
    }
}

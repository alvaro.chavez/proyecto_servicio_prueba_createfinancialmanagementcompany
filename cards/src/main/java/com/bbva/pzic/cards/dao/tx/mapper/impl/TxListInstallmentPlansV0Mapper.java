package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOInstallmentsPlanList;
import com.bbva.pzic.cards.business.dto.DTOIntPagination;
import com.bbva.pzic.cards.business.dto.InputListInstallmentPlans;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;
import com.bbva.pzic.cards.canonic.ScheduleInstallment;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMENGG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS1GG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS2GG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS3GG;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListInstallmentPlansV0Mapper;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;
import com.bbva.pzic.cards.util.orika.MapperFactory;
import com.bbva.pzic.cards.util.orika.converter.builtin.LongToIntegerConverter;
import com.bbva.pzic.cards.util.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

import static com.bbva.pzic.cards.util.Constants.DEFAULT_TIME;

/**
 * Created on 8/02/2018.
 *
 * @author Entelgy
 */

@Mapper("txListInstallmentPlansV0Mapper")
public class TxListInstallmentPlansV0Mapper extends ConfigurableMapper implements ITxListInstallmentPlansV0Mapper {

    private static final String MONEDA = "moneda";
    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.getConverterFactory().registerConverter(new LongToIntegerConverter());

        factory.classMap(InputListInstallmentPlans.class, FormatoMPMENGG.class)
                .field("cardId", "numtarj")
                .field("paginationKey", "idpagin")
                .field("pageSize", "tampagi")
                .register();

        factory.classMap(FormatoMPMS1GG.class, InstallmentsPlan.class)
                .field("identif", "id")
                .field("dsentif", "description")
                .field("idfinan", "financingType.id")
                .field("dsfinan", "financingType.name")
                .field("identif", "transactionId")
                .field("feccuo1", "firstInstallmentDate")
                .field("fecopeo", "operationDate")
                .field("totcuo", "terms.number")
                .field("pencuo", "terms.pending")
                .field("frecuo", "terms.frequency")
                .field("imptot", "total.amount")
                .field(MONEDA, "total.currency")
                .field("impori", "capital.amount")
                .field(MONEDA, "capital.currency")
                .field("montres", "outstandingBalance.amount")
                .field(MONEDA, "outstandingBalance.currency")
                .field("monpag", "amortizedAmount.amount")
                .field(MONEDA, "amortizedAmount.currency")
                .field("porcpag", "amortizedPercentage")
                .field("impcuo1", "firstInstallment.amount")
                .field(MONEDA, "firstInstallment.currency")
                .register();

        factory.classMap(FormatoMPMS2GG.class, ScheduleInstallment.class)
                .field("fecpago", "maturityDate")
                .field("montpag", "total.amount")
                .register();

        factory.classMap(FormatoMPMS3GG.class, DTOIntPagination.class)
                .field("idpagin", "paginationKey")
                .field("tampagi", "pageSize")
                .register();
    }


    @Override
    public FormatoMPMENGG mapIn(final InputListInstallmentPlans dtoIn) {
        return map(dtoIn, FormatoMPMENGG.class);
    }

    @Override
    public DTOInstallmentsPlanList mapOut(final FormatoMPMS1GG formatOutput,
                                          final DTOInstallmentsPlanList dtoOut) {
        DTOInstallmentsPlanList dtoIntInstallmentsPlans = dtoOut;
        if (dtoIntInstallmentsPlans == null) {
            dtoIntInstallmentsPlans = new DTOInstallmentsPlanList();
        }
        if (dtoIntInstallmentsPlans.getData() == null) {
            dtoIntInstallmentsPlans.setData(new ArrayList<>());
        }
        InstallmentsPlan installmentsPlan = map(formatOutput, InstallmentsPlan.class);
        installmentsPlan.setStartDate(FunctionUtils.buildDatetime(formatOutput.getFectrap(), DEFAULT_TIME));
        if (formatOutput.getIdfinan() != null) {
            installmentsPlan.getFinancingType().setId(enumMapper.getEnumValue("installmentsPlans.financingType.id", formatOutput.getIdfinan()));
        }
        if (formatOutput.getFrecuo() != null) {
            installmentsPlan.getTerms().setFrequency(enumMapper.getEnumValue("installmentsPlans.terms.frequency", formatOutput.getFrecuo()));
        }

        dtoIntInstallmentsPlans.getData().add(installmentsPlan);
        return dtoIntInstallmentsPlans;
    }


    @Override
    public DTOInstallmentsPlanList mapOut2(final FormatoMPMS2GG formatOutput,
                                           final DTOInstallmentsPlanList dtoIntInstallmentsPlans) {

        int indexOfLastDetail = dtoIntInstallmentsPlans.getData().size() - 1;
        InstallmentsPlan installmentsPlan = dtoIntInstallmentsPlans.getData().get(indexOfLastDetail);
        String currency = installmentsPlan.getCapital() == null ? null : installmentsPlan.getCapital().getCurrency();
        if (installmentsPlan.getScheduledPayments() == null) {
            installmentsPlan.setScheduledPayments(new ArrayList<>());
        }
        ScheduleInstallment scheduleInstallment = map(formatOutput, ScheduleInstallment.class);
        if (formatOutput.getMontpag() != null) {
            scheduleInstallment.getTotal().setCurrency(currency);
        }
        installmentsPlan.getScheduledPayments().add(scheduleInstallment);
        dtoIntInstallmentsPlans.getData().set(indexOfLastDetail, installmentsPlan);
        return dtoIntInstallmentsPlans;
    }

    @Override
    public DTOInstallmentsPlanList mapOut3(final FormatoMPMS3GG formatOutput,
                                           final DTOInstallmentsPlanList dtoOut) {
        DTOInstallmentsPlanList dtoIntInstallmentsPlans = dtoOut;
        if (dtoIntInstallmentsPlans == null) {
            dtoIntInstallmentsPlans = new DTOInstallmentsPlanList();
        }
        DTOIntPagination dtoIntPagination = map(formatOutput, DTOIntPagination.class);
        dtoIntInstallmentsPlans.setPagination(dtoIntPagination);
        return dtoIntInstallmentsPlans;
    }


}

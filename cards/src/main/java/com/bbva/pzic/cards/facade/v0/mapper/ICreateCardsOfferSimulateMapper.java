package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.pzic.cards.business.dto.DTOIntDetailSimulation;
import com.bbva.pzic.cards.canonic.HolderSimulation;

public interface ICreateCardsOfferSimulateMapper {

    DTOIntDetailSimulation mapIn(String offerId, HolderSimulation input);

    ServiceResponseOK<HolderSimulation> mapOut(HolderSimulation output);
}

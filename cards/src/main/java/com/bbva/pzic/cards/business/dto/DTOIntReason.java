package com.bbva.pzic.cards.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DTOIntReason {
    @NotNull(groups = {ValidationGroup.CreateCardCancelRequestV1.class,
            ValidationGroup.CreateCardCancellationsV1.class})
    @Size(max = 2, groups = ValidationGroup.CreateCardCancellationsV1.class)
    private String id;

    private String comments;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}

package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.pzic.cards.business.dto.DTOIntQuestion;
import com.bbva.pzic.cards.business.dto.InputCreateOffersGenerateCards;
import com.bbva.pzic.cards.facade.v0.dto.OfferGenerate;
import com.bbva.pzic.cards.facade.v0.dto.Question;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateOffersGenerateCardsMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
@Component
public class CreateOffersGenerateCardsMapper implements ICreateOffersGenerateCardsMapper {

    @Override
    public InputCreateOffersGenerateCards mapIn(final OfferGenerate offerGenerate, final String bcsDeviceScreenSize) {
        InputCreateOffersGenerateCards input = new InputCreateOffersGenerateCards();
        input.setBcsDeviceScreenSize(bcsDeviceScreenSize);
        input.setQuestions(mapInQuestions(offerGenerate.getQuestions()));
        return input;
    }

    private List<DTOIntQuestion> mapInQuestions(final List<Question> questions) {
        if (CollectionUtils.isEmpty(questions)) {
            return null;
        }

        return questions.stream().filter(Objects::nonNull).map(this::mapInQuestion).collect(Collectors.toList());
    }

    private DTOIntQuestion mapInQuestion(final Question question) {
        DTOIntQuestion result = new DTOIntQuestion();
        result.setId(question.getId());
        result.setAnswerValue(question.getAnswer() == null ? null : question.getAnswer().getValue());
        return result;
    }

    @Override
    public ServiceResponseOK<OfferGenerate> mapOut(final OfferGenerate offersGenerateCards) {
        if (offersGenerateCards == null) {
            return null;
        }

        return ServiceResponseOK.data(offersGenerateCards).build();
    }
}

package com.bbva.pzic.cards.dao.model.ppcut001_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import java.util.Calendar;
import java.util.List;

/**
 * <p>Bean fila para el campo tabular <code>EntityOut</code>, utilizado por la clase <code>RespuestaTransaccionPpcut001_1</code></p>
 * 
 * @see RespuestaTransaccionPpcut001_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityout {
	
	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 36, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>cardType</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "cardType", tipo = TipoCampo.DTO)
	private Cardtype cardtype;
	
	/**
	 * <p>Campo <code>product</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "product", tipo = TipoCampo.DTO)
	private Product product;
	
	/**
	 * <p>Campo <code>physicalSupport</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "physicalSupport", tipo = TipoCampo.DTO)
	private Physicalsupport physicalsupport;
	
	/**
	 * <p>Campo <code>grantedCredits</code>, &iacute;ndice: <code>5</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 5, nombre = "grantedCredits", tipo = TipoCampo.LIST)
	private List<Grantedcredits> grantedcredits;
	
	/**
	 * <p>Campo <code>contact</code>, &iacute;ndice: <code>6</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 6, nombre = "contact", tipo = TipoCampo.DTO)
	private Contact contact;
	
	/**
	 * <p>Campo <code>rates</code>, &iacute;ndice: <code>7</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 7, nombre = "rates", tipo = TipoCampo.DTO)
	private Rates rates;
	
	/**
	 * <p>Campo <code>fees</code>, &iacute;ndice: <code>8</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 8, nombre = "fees", tipo = TipoCampo.DTO)
	private Fees fees;
	
	/**
	 * <p>Campo <code>operationNumber</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "operationNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 36, signo = true, obligatorio = true)
	private String operationnumber;
	
	/**
	 * <p>Campo <code>operationDate</code>, &iacute;ndice: <code>10</code>, tipo: <code>TIMESTAMP</code>
	 */
	@Campo(indice = 10, nombre = "operationDate", tipo = TipoCampo.TIMESTAMP, signo = true, formato = "yyyy-MM-dd'T'HH:mm:ss.SSSX", obligatorio = true)
	private Calendar operationdate;
	
	/**
	 * <p>Campo <code>status</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "status", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String status;
	
	/**
	 * <p>Campo <code>offerId</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "offerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String offerid;
	
	/**
	 * <p>Campo <code>participants</code>, &iacute;ndice: <code>13</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 13, nombre = "participants", tipo = TipoCampo.LIST)
	private List<Participants> participants;
	
}
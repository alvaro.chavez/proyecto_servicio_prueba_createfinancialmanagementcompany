package com.bbva.pzic.cards.dao.model.kb89;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>KTS2KB89</code> de la transacci&oacute;n <code>KB89</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KTS2KB89")
@RooJavaBean
@RooSerializable
public class FormatoKTS2KB89 {
	
	/**
	 * <p>Campo <code>IDCOMIM</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDCOMIM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idcomim;
	
	/**
	 * <p>Campo <code>NOMCOMI</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NOMCOMI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nomcomi;
	
	/**
	 * <p>Campo <code>IMPCOMI</code>, &iacute;ndice: <code>3</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 3, nombre = "IMPCOMI", tipo = TipoCampo.DECIMAL, longitudMinima = 12, longitudMaxima = 12, decimales = 2)
	private BigDecimal impcomi;
	
	/**
	 * <p>Campo <code>TIPCOMI</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "TIPCOMI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipcomi;
	
	/**
	 * <p>Campo <code>NOMTIPC</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "NOMTIPC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String nomtipc;
	
}
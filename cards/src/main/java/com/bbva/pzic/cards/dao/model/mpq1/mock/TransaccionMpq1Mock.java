package com.bbva.pzic.cards.dao.model.mpq1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPFMCE1;
import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPNCCS1;
import com.bbva.pzic.cards.dao.model.mpq1.PeticionTransaccionMpq1;
import com.bbva.pzic.cards.dao.model.mpq1.RespuestaTransaccionMpq1;
import com.bbva.pzic.cards.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Invocador de la transacci&oacute;n <code>MPQ1</code>
 *
 * @see PeticionTransaccionMpq1
 * @see RespuestaTransaccionMpq1
 */
@Component("transaccionMpq1")
public class TransaccionMpq1Mock implements InvocadorTransaccion<PeticionTransaccionMpq1, RespuestaTransaccionMpq1> {

    public static final String TEST_EMPTY = "9999999999999999999";

    @Override
    public RespuestaTransaccionMpq1 invocar(PeticionTransaccionMpq1 transaccion) {
        RespuestaTransaccionMpq1 response = new RespuestaTransaccionMpq1();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        FormatoMPFMCE1 format = transaccion.getCuerpo().getParte(FormatoMPFMCE1.class);

        if (TEST_EMPTY.equalsIgnoreCase(format.getIdetarj())) {
            return response;
        }

        try {
            List<FormatoMPNCCS1> formats = FormatsMpq1Mock.getInstance().getFormatoMPNCCS1();

            for (FormatoMPNCCS1 f : formats) {
                CopySalida copy = new CopySalida();
                copy.setCopy(f);

                response.getCuerpo().getPartes().add(copy);
            }

            return response;
        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionMpq1 invocarCache(PeticionTransaccionMpq1 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntBankType {

    @NotNull(groups = {
            ValidationGroup.CreateCardsCardProposal.class
    })
    @Size(max = 15, groups = {
            ValidationGroup.CreateCardsCardProposal.class,
            ValidationGroup.CreateCardsProposal.class
    })
    private String id;
    @Valid
    private DTOIntBranch branch;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntBranch getBranch() {
        return branch;
    }

    public void setBranch(DTOIntBranch branch) {
        this.branch = branch;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
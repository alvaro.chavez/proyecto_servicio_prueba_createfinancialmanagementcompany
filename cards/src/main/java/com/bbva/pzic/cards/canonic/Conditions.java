package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "conditions", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "conditions", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Conditions implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Condition Data
     */
    private List<Condition> data;

    public List<Condition> getData() {
        return data;
    }

    public void setData(List<Condition> data) {
        this.data = data;
    }
}

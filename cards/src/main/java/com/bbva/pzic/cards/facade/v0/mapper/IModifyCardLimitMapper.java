package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.InputModifyCardLimit;
import com.bbva.pzic.cards.canonic.Limit;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;

/**
 * Created on 17/11/2017.
 *
 * @author Entelgy
 */
public interface IModifyCardLimitMapper {

    InputModifyCardLimit mapIn(String cardId, String limitId, Limit limit);

    ServiceResponse<Limit> mapOut(Limit limit);
}
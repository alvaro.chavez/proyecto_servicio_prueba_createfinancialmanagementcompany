package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class InputCreateCardCancellations {
    @Size(min = 1, max = 19, groups = ValidationGroup.CreateCardCancellationsV1.class)
    private String cardId;

    @NotNull(groups = ValidationGroup.CreateCardCancellationsV1.class)
    @Valid
    private DTOIntReason reason;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public DTOIntReason getReason() {
        return reason;
    }

    public void setReason(DTOIntReason reason) {
        this.reason = reason;
    }
}

package com.bbva.pzic.cards.facade.v1.dto;

import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Created on 20/04/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "scheduleTimes", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "scheduleTimes", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ScheduleTimes implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Start time that the customer has available to receive their card. String based on ISO-8601 for specifying the hour.
     */
    private String startTime;
    /**
     * End time that the customer has available to receive their card. String based on ISO-8601 for specifying the hour.
     */
    private String endTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}

package com.bbva.pzic.cards.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "Atm", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlType(name = "Atm", namespace = "urn:com:bbva:pzic:cards:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Atm implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Code the identifies the ATM where operation took place. Sometimes an ATM
     * code can be duplicated between banks, so it\`s neccesary to use the bank
     * code with this code to univocally identify an ATM. An ATM always belongs
     * to a branch (and bank) besides its location.
     */
    private String id;
    /**
     * Network that connects the ATMs of different banks and permits these ATMs
     * can interact with cards from other banks. Depending on banks and networks
     * sometimes fee can be charged when disposing money from an ATM that does
     * not belong to your bank.
     */
    private Net net;
    /**
     * ATM geolocation.
     */
    private Geolocation geolocation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Net getNet() {
        return net;
    }

    public void setNet(Net net) {
        this.net = net;
    }

    public Geolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Geolocation geolocation) {
        this.geolocation = geolocation;
    }
}

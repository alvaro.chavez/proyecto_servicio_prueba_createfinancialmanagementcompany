package com.bbva.pzic.cards.dao.rest.mapper.impl;


import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.Documents;
import com.bbva.pzic.cards.dao.rest.mapper.IRestDigitizeDocumentFileMapper;
import com.bbva.pzic.cards.util.mappers.Mapper;

/**
 * Created on 27/03/2018.
 *
 * @author Entelgy
 */
@Mapper
public class RestDigitizeDocumentFileMapper implements IRestDigitizeDocumentFileMapper {

    @Override
    public Document mapOut(Documents output) {
        if (output.getDocumentos() == null || output.getDocumentos().isEmpty()) {
            return null;
        }
        return output.getDocumentos().get(0);
    }
}

package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMENG2;
import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMS1G2;
import com.bbva.pzic.cards.dao.model.mpg2.PeticionTransaccionMpg2;
import com.bbva.pzic.cards.dao.model.mpg2.RespuestaTransaccionMpg2;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardActivationsMapperV0;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 6/10/2017.
 *
 * @author Entelgy
 */
@Component("txListCardActivations")
public class TxListCardActivations
        extends SingleOutputFormat<DTOIntCard, FormatoMPMENG2, List<Activation>, FormatoMPMS1G2> {

    @Resource(name = "txListCardActivationsMapperV0")
    private ITxListCardActivationsMapperV0 txListCardActivationsMapper;

    @Autowired
    public TxListCardActivations(@Qualifier("transaccionMpg2") InvocadorTransaccion<PeticionTransaccionMpg2, RespuestaTransaccionMpg2> transaction) {
        super(transaction, PeticionTransaccionMpg2::new, ArrayList::new, FormatoMPMS1G2.class);
    }

    @Override
    protected FormatoMPMENG2 mapInput(DTOIntCard dtoIntCard) {
        return txListCardActivationsMapper.mapInt(dtoIntCard);
    }

    @Override
    protected List<Activation> mapFirstOutputFormat(FormatoMPMS1G2 formatoMPMS1G2, DTOIntCard dtoIntCard, List<Activation> activationList) {
        return txListCardActivationsMapper.mapOut(formatoMPMS1G2, activationList);
    }
}

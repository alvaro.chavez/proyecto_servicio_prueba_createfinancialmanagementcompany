package com.bbva.pzic.cards.dao.model.mpcn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPCN</code>
 * 
 * @see PeticionTransaccionMpcn
 * @see RespuestaTransaccionMpcn
 */
@Component
public class TransaccionMpcn implements InvocadorTransaccion<PeticionTransaccionMpcn,RespuestaTransaccionMpcn> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMpcn invocar(PeticionTransaccionMpcn transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpcn.class, RespuestaTransaccionMpcn.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMpcn invocarCache(PeticionTransaccionMpcn transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpcn.class, RespuestaTransaccionMpcn.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
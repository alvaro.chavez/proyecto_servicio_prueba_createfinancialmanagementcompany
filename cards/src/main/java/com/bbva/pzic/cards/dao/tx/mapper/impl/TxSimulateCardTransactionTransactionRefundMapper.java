package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.InputSimulateCardTransactionTransactionRefund;
import com.bbva.pzic.cards.dao.model.mp6i.FormatoMPMEN6I;
import com.bbva.pzic.cards.dao.model.mp6i.FormatoMPMS16I;
import com.bbva.pzic.cards.dao.tx.mapper.ITxSimulateCardTransactionTransactionRefund;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.Enums;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 17/09/2020.
 *
 * @author Entelgy.
 */
@Component
public class TxSimulateCardTransactionTransactionRefundMapper implements ITxSimulateCardTransactionTransactionRefund {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public FormatoMPMEN6I mapIn(final InputSimulateCardTransactionTransactionRefund inputSimulateCardTransactionTransactionRefund) {
        FormatoMPMEN6I format = new FormatoMPMEN6I();
        format.setIdetarj(inputSimulateCardTransactionTransactionRefund.getCardId());
        format.setIdentif(inputSimulateCardTransactionTransactionRefund.getTransactionId());
        format.setMcindop(inputSimulateCardTransactionTransactionRefund.getRefundTypeId());
        return format;
    }

    @Override
    public SimulateTransactionRefund mapOut(final FormatoMPMS16I formatoMPMS16I) {
        SimulateTransactionRefund simulateTransactionRefund = new SimulateTransactionRefund();
        simulateTransactionRefund.setRefundType(mapInRefundType(formatoMPMS16I.getMcindop(), formatoMPMS16I.getMcindes()));
        simulateTransactionRefund.setCard(mapInCard(formatoMPMS16I.getIdetarj()));
        simulateTransactionRefund.setTransaction(mapInTransaction(formatoMPMS16I.getIdentif()));
        simulateTransactionRefund.setStatus(mapInStatus(formatoMPMS16I.getCodope(), formatoMPMS16I.getDesoper()));
        return simulateTransactionRefund;
    }

    private Status mapInStatus(String statusId, String statusDescription) {
        if (StringUtils.isEmpty(statusId) && StringUtils.isEmpty(statusDescription)) {
            return null;
        }

        Status result = new Status();
        result.setId(translator.translateBackendEnumValueStrictly(Enums.TRANSACTION_REFUND_STATUS_ID, statusId));
        result.setDescription(statusDescription);
        return result;
    }

    private Transaction mapInTransaction(final String transactionId) {
        if (StringUtils.isEmpty(transactionId)) {
            return null;
        }

        Transaction result = new Transaction();
        result.setId(transactionId);
        return result;
    }

    private Card mapInCard(final String cardId) {
        if (StringUtils.isEmpty(cardId)) {
            return null;
        }

        Card result = new Card();
        result.setId(cardId);
        return result;
    }

    private RefundType mapInRefundType(final String refundTypeId, final String refundTypeName) {
        if (StringUtils.isEmpty(refundTypeId) && StringUtils.isEmpty(refundTypeName)) {
            return null;
        }

        RefundType result = new RefundType();
        result.setId(translator.translateBackendEnumValueStrictly(Enums.TRANSACTION_REFUND_REFUND_TYPE_ID_KEY, refundTypeId));
        result.setName(refundTypeName);
        return result;
    }
}

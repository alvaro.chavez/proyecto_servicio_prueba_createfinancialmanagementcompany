package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.dao.apx.mapper.IApxModifyCardProposalMapper;
import com.bbva.pzic.cards.dao.model.ppcut003_1.Address;
import com.bbva.pzic.cards.dao.model.ppcut003_1.Branch;
import com.bbva.pzic.cards.dao.model.ppcut003_1.Contact;
import com.bbva.pzic.cards.dao.model.ppcut003_1.Delivery;
import com.bbva.pzic.cards.dao.model.ppcut003_1.Destination;
import com.bbva.pzic.cards.dao.model.ppcut003_1.Frecuency;
import com.bbva.pzic.cards.dao.model.ppcut003_1.Product;
import com.bbva.pzic.cards.dao.model.ppcut003_1.*;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.mappers.DateMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 27/11/2019.
 *
 * @author Entelgy
 */
@Component
public class ApxModifyCardProposalMapper implements IApxModifyCardProposalMapper {

    @Override
    public PeticionTransaccionPpcut003_1 mapIn(final InputModifyCardProposal input) {
        PeticionTransaccionPpcut003_1 request = new PeticionTransaccionPpcut003_1();
        request.setProposalId(input.getProposalId());
        request.setDeliveries(mapInDeliveries(input.getProposal().getDeliveries()));
        request.setPaymentmethod(mapInPaymentMethod(input.getProposal().getPaymentMethod()));
        request.setContactability(mapInContactability(input.getProposal().getContactability()));
        request.setGrantedcredits(mapInGrantedCredits(input.getProposal().getGrantedCredits()));
        request.setFees(mapInFees(input.getProposal().getFees()));
        request.setRates(mapInRates(input.getProposal().getRate()));
        request.setProduct(mapInProduct(input.getProposal().getProduct()));
        request.setOfferid(input.getProposal().getOfferId());
        request.setNotificationsbyoperation(
                input.getProposal().getNotificationsByOperation() == null ? null : Boolean.valueOf(input.getProposal().getNotificationsByOperation().toString()));
        return request;
    }

    private Product mapInProduct(final DTOIntProduct product) {
        if (product == null) {
            return null;
        }

        Product result = new Product();
        result.setId(product.getId());
        return result;
    }

    private Rates mapInRates(final DTOIntRate rate) {
        if (rate == null) {
            return null;
        }

        Rates result = new Rates();
        result.setItemizerates(mapInItemizeRates(rate.getItemizeRates()));
        return result;
    }

    private List<Itemizerates> mapInItemizeRates(final List<DTOIntItemizeRate> itemizeRates) {
        if (CollectionUtils.isEmpty(itemizeRates)) {
            return null;
        }

        return itemizeRates.stream().filter(Objects::nonNull).map(this::mapInItemizeRate).collect(Collectors.toList());
    }

    private Itemizerates mapInItemizeRate(final DTOIntItemizeRate dtoIntItemizeRate) {
        Itemizerates result = new Itemizerates();
        result.setItemizerate(new Itemizerate());
        result.getItemizerate().setRatetype(dtoIntItemizeRate.getRateType());
        result.getItemizerate().setItemizeratesunit(mapInItemizeRatesUnit(dtoIntItemizeRate.getItemizeRatesUnit()));
        return result;
    }

    private Itemizeratesunit mapInItemizeRatesUnit(final DTOIntItemizeRatesUnit itemizeRatesUnit) {
        if (itemizeRatesUnit == null) {
            return null;
        }

        Itemizeratesunit result = new Itemizeratesunit();
        result.setPercentage(itemizeRatesUnit.getPercentage() == null ? null : String.valueOf(itemizeRatesUnit.getPercentage()));
        result.setUnitratetype(itemizeRatesUnit.getUnitRateType());
        return result;
    }

    private Fees mapInFees(final DTOIntFee fees) {
        if (fees == null) {
            return null;
        }

        Fees result = new Fees();
        result.setItemizefees(mapInItemizeFees(fees.getItemizeFees()));
        return result;
    }

    private List<Itemizefees> mapInItemizeFees(final List<DTOIntItemizeFee> itemizeFees) {
        if (CollectionUtils.isEmpty(itemizeFees)) {
            return null;
        }

        return itemizeFees.stream().filter(Objects::nonNull).map(this::mapInItemizeFee).collect(Collectors.toList());
    }

    private Itemizefees mapInItemizeFee(final DTOIntItemizeFee dtoIntItemizeFee) {
        Itemizefees result = new Itemizefees();
        result.setItemizefee(new Itemizefee());
        result.getItemizefee().setFeetype(dtoIntItemizeFee.getFeeType());
        result.getItemizefee().setItemizefeeunit(mapInItemizeFeeUnit(dtoIntItemizeFee.getItemizeFeeUnit()));
        return result;

    }

    private Itemizefeeunit mapInItemizeFeeUnit(final DTOIntItemizeFeeUnit itemizeFeeUnit) {
        if (itemizeFeeUnit == null) {
            return null;
        }

        Itemizefeeunit result = new Itemizefeeunit();
        result.setAmount(itemizeFeeUnit.getAmount() == null ? null : String.valueOf(itemizeFeeUnit.getAmount()));
        result.setCurrency(itemizeFeeUnit.getCurrency());
        result.setUnittype(itemizeFeeUnit.getUnitType());
        return result;
    }

    private List<Grantedcredits> mapInGrantedCredits(final List<DTOIntImport> grantedCredits) {
        if (CollectionUtils.isEmpty(grantedCredits)) {
            return null;
        }

        return grantedCredits.stream().filter(Objects::nonNull).map(this::mapInGrantedCredit).collect(Collectors.toList());
    }

    private Grantedcredits mapInGrantedCredit(final DTOIntImport dtoIntImport) {
        Grantedcredits grantedcredits = new Grantedcredits();
        grantedcredits.setGrantedcredit(new Grantedcredit());
        grantedcredits.getGrantedcredit().setAmount(dtoIntImport.getAmount() == null ? null : String.valueOf(dtoIntImport.getAmount()));
        grantedcredits.getGrantedcredit().setCurrency(dtoIntImport.getCurrency());
        return grantedcredits;
    }

    private Contactability mapInContactability(final DTOIntContactAbility contactability) {
        if (contactability == null) {
            return null;
        }

        Contactability result = new Contactability();
        result.setReason(contactability.getReason());
        result.setScheduletimes(mapInScheduleTimes(contactability.getScheduletimes()));
        return result;
    }

    private Scheduletimes mapInScheduleTimes(final DTOIntScheduletimes scheduletimes) {
        if (scheduletimes == null) {
            return null;
        }

        Scheduletimes result = new Scheduletimes();
        result.setEndtime(scheduletimes.getEndtime());
        result.setStarttime(scheduletimes.getStarttime());
        return result;
    }

    private Paymentmethod mapInPaymentMethod(final DTOIntPaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            return null;
        }

        Paymentmethod result = new Paymentmethod();
        result.setId(paymentMethod.getId());
        result.setFrecuency(mapInFrecuency(paymentMethod.getFrequency()));
        return result;
    }

    private Frecuency mapInFrecuency(final DTOIntFrequency frequency) {
        if (frequency == null) {
            return null;
        }

        Frecuency result = new Frecuency();
        result.setId(frequency.getId());
        result.setDaysofmonth(mapInDaysOfMonth(frequency.getDaysOfMonth()));
        return result;
    }

    private Daysofmonth mapInDaysOfMonth(final DTOIntDaysOfMonth daysOfMonth) {
        if (daysOfMonth == null) {
            return null;
        }

        Daysofmonth result = new Daysofmonth();
        result.setDay(daysOfMonth.getDay());
        result.setCutoffday(daysOfMonth.getCutOffDay());
        return result;
    }

    private List<Deliveries> mapInDeliveries(final List<DTOIntDelivery> dtoIntDeliveries) {
        if (CollectionUtils.isEmpty(dtoIntDeliveries)) {
            return null;
        }

        return dtoIntDeliveries.stream().filter(Objects::nonNull).map(this::mapInDelivery).collect(Collectors.toList());
    }

    private Deliveries mapInDelivery(final DTOIntDelivery dtoIntDelivery) {
        Deliveries deliveries = new Deliveries();
        deliveries.setDelivery(new Delivery());
        deliveries.getDelivery().setServicetype(mapInServiceType(dtoIntDelivery.getServiceTypeId()));
        deliveries.getDelivery().setSpecificcontact(mapInDeliverySpecificContact(dtoIntDelivery.getContact()));
        deliveries.getDelivery().setAddress(mapInAddress(dtoIntDelivery.getAddress()));
        deliveries.getDelivery().setDestination(mapInDestination(dtoIntDelivery.getDestination()));
        return deliveries;
    }

    private Destination mapInDestination(final DTOIntDestination destination) {
        if (destination == null) {
            return null;
        }

        Destination result = new Destination();
        result.setId(destination.getId());
        result.setBranch(mapInBranch(destination.getBranch()));
        return result;
    }

    private Branch mapInBranch(final DTOIntBranch branch) {
        if (branch == null) {
            return null;
        }

        Branch result = new Branch();
        result.setId(branch.getId());
        return result;
    }

    private Address mapInAddress(final DTOIntAddress dtoIntAddress) {
        if (dtoIntAddress == null) {
            return null;
        }

        Address result = new Address();
        result.setAddresstype(dtoIntAddress.getAddressType());
        result.setId(dtoIntAddress.getId());
        return result;
    }

    private Specificcontact mapInDeliverySpecificContact(final DTOIntContact contact) {
        if (contact == null) {
            return null;
        }

        Specificcontact specificcontact = new Specificcontact();
        specificcontact.setContacttype(contact.getContactType());
        specificcontact.setContact(mapInEmailContact(contact));
        return specificcontact;
    }

    private Contact mapInEmailContact(final DTOIntContact contact) {
        if (StringUtils.isEmpty(contact.getContactAddress()) && StringUtils.isEmpty(contact.getContactDetailType())) {
            return null;
        }

        Contact result = new Contact();
        result.setAddress(contact.getContactAddress());
        result.setContactdetailtype(contact.getContactDetailType());
        return result;
    }


    private Servicetype mapInServiceType(final String serviceTypeId) {
        if (StringUtils.isEmpty(serviceTypeId)) {
            return null;
        }

        Servicetype result = new Servicetype();
        result.setId(serviceTypeId);
        return result;
    }

    @Override
    public Proposal mapOut(final RespuestaTransaccionPpcut003_1 response) {
        if (response == null) {
            return null;
        }
        Proposal proposal = new Proposal();
        proposal.setProduct(mapOutProduct(response.getCampo_8_product()));
        proposal.setDeliveries(mapOutDeliveries(response.getCampo_1_deliveries()));
        proposal.setPaymentMethod(mapOutPaymentMethod(response.getCampo_2_paymentmethod()));
        proposal.setContactability(mapOutContactAbility(response.getCampo_3_contactability()));
        proposal.setRates(mapOutRates(response.getCampo_5_rates()));
        proposal.setFees(mapOutFees(response.getCampo_6_fees()));
        proposal.setGrantedCredits(mapOutGrantedCredits(response.getCampo_4_grantedcredits()));
        proposal.setOfferId(response.getCampo_7_offerid());
        proposal.setNotificationsByOperation(response.getCampo_9_notificationsbyoperation() == null ? null : response.getCampo_9_notificationsbyoperation().toString());
        return proposal;
    }

    private List<Import> mapOutGrantedCredits(final List<Campo_4_grantedcredits> grantedcredits) {
        if (CollectionUtils.isEmpty(grantedcredits)) {
            return null;
        }

        return grantedcredits.stream().filter(Objects::nonNull).map(this::mapOutGrantedCredit).collect(Collectors.toList());
    }

    private Import mapOutGrantedCredit(final Campo_4_grantedcredits grantedcredit) {
        if (grantedcredit.getGrantedcredit() == null) {
            return null;
        }

        Import result = new Import();
        result.setAmount(StringUtils.isEmpty(grantedcredit.getGrantedcredit().getAmount()) ? null : new BigDecimal(grantedcredit.getGrantedcredit().getAmount()));
        result.setCurrency(grantedcredit.getGrantedcredit().getCurrency());
        return result;
    }

    private TransferFees mapOutFees(final Campo_6_fees fees) {
        if (fees == null) {
            return null;
        }

        TransferFees result = new TransferFees();
        result.setItemizeFees(mapOutItemizeFees(fees.getItemizefees()));
        return result;
    }

    private List<ItemizeFee> mapOutItemizeFees(final List<Itemizefees> itemizefees) {
        if (CollectionUtils.isEmpty(itemizefees)) {
            return null;
        }

        return itemizefees.stream().filter(Objects::nonNull).map(this::mapOutItemizeFee).collect(Collectors.toList());
    }

    private ItemizeFee mapOutItemizeFee(final Itemizefees itemizefees) {
        if (itemizefees.getItemizefee() == null) {
            return null;
        }

        ItemizeFee result = new ItemizeFee();
        result.setFeeType(itemizefees.getItemizefee().getFeetype());
        result.setDescription(itemizefees.getItemizefee().getDescription());
        result.setItemizeFeeUnit(mapOutItemizeFeeUnit(itemizefees.getItemizefee().getItemizefeeunit()));
        return result;
    }

    private ItemizeFeeUnit mapOutItemizeFeeUnit(final Itemizefeeunit itemizefeeunit) {
        if (itemizefeeunit == null) {
            return null;
        }

        ItemizeFeeUnit result = new ItemizeFeeUnit();
        result.setUnitType(itemizefeeunit.getUnittype());
        result.setCurrency(itemizefeeunit.getCurrency());
        result.setAmount(StringUtils.isEmpty(itemizefeeunit.getAmount()) ? null : new BigDecimal(itemizefeeunit.getAmount()));
        return result;
    }

    private ProposalRate mapOutRates(final Campo_5_rates rates) {
        if (rates == null) {
            return null;
        }

        ProposalRate result = new ProposalRate();
        result.setItemizeRates(mapOutItemizeRates(rates.getItemizerates()));
        return result;
    }

    private List<ItemizeRate> mapOutItemizeRates(final List<Itemizerates> itemizerates) {
        if (CollectionUtils.isEmpty(itemizerates)) {
            return null;
        }

        return itemizerates.stream().filter(Objects::nonNull).map(this::mapOutItemizeRate).collect(Collectors.toList());
    }

    private ItemizeRate mapOutItemizeRate(final Itemizerates itemizerates) {
        if (itemizerates.getItemizerate() == null) {
            return null;
        }

        ItemizeRate result = new ItemizeRate();
        result.setRateType(itemizerates.getItemizerate().getRatetype());
        result.setDescription(itemizerates.getItemizerate().getDescription());
        result.setItemizeRatesUnit(mapOutItemizeRateUnit(itemizerates.getItemizerate().getItemizeratesunit()));
        return result;
    }

    private ItemizeRateUnit mapOutItemizeRateUnit(final Itemizeratesunit itemizeratesunit) {
        if (itemizeratesunit == null) {
            return null;
        }

        ItemizeRateUnit result = new ItemizeRateUnit();
        result.setUnitRateType(itemizeratesunit.getUnitratetype());
        result.setPercentage(StringUtils.isEmpty(itemizeratesunit.getPercentage()) ? null : new BigDecimal(itemizeratesunit.getPercentage()));
        return result;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Product mapOutProduct(final Campo_8_product product) {
        if (product == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Product result = new com.bbva.pzic.cards.facade.v1.dto.Product();
        result.setId(product.getId());
        result.setName(product.getName());
        return result;
    }

    private ContactAbility mapOutContactAbility(final Campo_3_contactability contactability) {
        if (contactability == null) {
            return null;
        }

        ContactAbility result = new ContactAbility();
        result.setReason(contactability.getReason());
        result.setScheduleTimes(mapOutScheduleTimes(contactability.getScheduletimes()));
        return result;
    }

    private ScheduleTimes mapOutScheduleTimes(final Scheduletimes scheduletimes) {
        if (scheduletimes == null) {
            return null;
        }

        ScheduleTimes result = new ScheduleTimes();
        result.setStartTime(DateMapper.mapDateToStringHour(scheduletimes.getStarttime()));
        result.setEndTime(DateMapper.mapDateToStringHour(scheduletimes.getEndtime()));
        return result;
    }

    private PaymentMethod mapOutPaymentMethod(final Campo_2_paymentmethod paymentmethod) {
        if (paymentmethod == null) {
            return null;
        }

        PaymentMethod result = new PaymentMethod();
        result.setId(paymentmethod.getId());
        result.setFrecuency(mapOutFrecuency(paymentmethod.getFrecuency()));
        return result;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Frecuency mapOutFrecuency(final Frecuency frecuency) {
        if (frecuency == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Frecuency result = new com.bbva.pzic.cards.facade.v1.dto.Frecuency();
        result.setId(frecuency.getId());
        result.setDescription(frecuency.getDescription());
        result.setDaysOfMonth(mapOutDaysOfMonth(frecuency.getDaysofmonth()));
        return result;
    }

    private DaysOfMonth mapOutDaysOfMonth(final Daysofmonth daysofmonth) {
        if (daysofmonth == null) {
            return null;
        }

        DaysOfMonth result = new DaysOfMonth();
        result.setCutOffDay(daysofmonth.getCutoffday());
        result.setDay(daysofmonth.getDay());
        return result;
    }

    private List<com.bbva.pzic.cards.facade.v1.dto.Delivery> mapOutDeliveries(final List<Campo_1_deliveries> deliveries) {
        if (CollectionUtils.isEmpty(deliveries)) {
            return null;
        }

        return deliveries.stream().filter(Objects::nonNull).map(this::mapOutDelivery).collect(Collectors.toList());
    }

    private com.bbva.pzic.cards.facade.v1.dto.Delivery mapOutDelivery(final Campo_1_deliveries deliveries) {
        if (deliveries.getDelivery() == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Delivery result = new com.bbva.pzic.cards.facade.v1.dto.Delivery();
        result.setId(deliveries.getDelivery().getId());
        result.setServiceType(mapOutServiceType(deliveries.getDelivery().getServicetype()));
        result.setContact(mapOutDeliverySpecificContact(deliveries.getDelivery().getSpecificcontact()));
        result.setAddress(mapOutDeliveryAddress(deliveries.getDelivery().getAddress()));
        result.setDestination(mapOutDestination(deliveries.getDelivery().getDestination()));
        return result;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Destination mapOutDestination(final Destination destination) {
        if (destination == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Destination result = new com.bbva.pzic.cards.facade.v1.dto.Destination();
        result.setId(destination.getId());
        result.setName(destination.getName());
        result.setBranch(mapOutBranch(destination.getBranch()));
        return result;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Branch mapOutBranch(final Branch branch) {
        if (branch == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Branch result = new com.bbva.pzic.cards.facade.v1.dto.Branch();
        result.setId(branch.getId());
        result.setName(branch.getName());
        return result;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Address mapOutDeliveryAddress(final Address address) {
        if (address == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Address result = new com.bbva.pzic.cards.facade.v1.dto.Address();
        result.setId(address.getId());
        result.setAddressType(address.getAddresstype());
        return result;
    }

    private com.bbva.pzic.cards.facade.v1.dto.Contact mapOutDeliverySpecificContact(final Specificcontact specificcontact) {
        if (specificcontact == null) {
            return null;
        }

        com.bbva.pzic.cards.facade.v1.dto.Contact result = new com.bbva.pzic.cards.facade.v1.dto.Contact();
        result.setId(specificcontact.getId());
        result.setContactType(specificcontact.getContacttype());
        result.setContact(mapOutEmailContact(specificcontact.getContact()));
        return result;
    }

    private SpecificContact mapOutEmailContact(final Contact emailcontact) {
        if (emailcontact == null) {
            return null;
        }

        SpecificContact result = new SpecificContact();
        result.setContactDetailType(emailcontact.getContactdetailtype());
        result.setAddress(emailcontact.getAddress());
        return result;
    }

    private ServiceType mapOutServiceType(final Servicetype servicetype) {
        if (servicetype == null) {
            return null;
        }

        ServiceType result = new ServiceType();
        result.setId(servicetype.getId());
        result.setDescription(servicetype.getDescription());
        return result;
    }
}
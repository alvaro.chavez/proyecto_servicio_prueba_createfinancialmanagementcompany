package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "phoneCompanyProposal", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "phoneCompanyProposal", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PhoneCompanyProposal implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Cellphone company identifier.
     */
    private String id;
    /**
     * Cellphone company name.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

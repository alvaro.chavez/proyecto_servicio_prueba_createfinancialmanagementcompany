package com.bbva.pzic.cards.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.PaymentMethod;
import com.bbva.pzic.cards.dao.model.mpg5.*;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardPaymentMethodsMapperV0;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 25/10/2017.
 *
 * @author Entelgy
 */
@Component("txGetCardPaymentMethodsV0")
public class TxGetCardPaymentMethodsV0
        extends DoubleOutputFormat<DTOIntCard, FormatoMPMENG5, PaymentMethod, FormatoMPMS1G5, FormatoMPMS2G5> {
    @Resource(name = "txGetCardPaymentMethodsMapperV0")

    private ITxGetCardPaymentMethodsMapperV0 mapper;

    @Autowired
    public TxGetCardPaymentMethodsV0(@Qualifier("transaccionMpg5") InvocadorTransaccion<PeticionTransaccionMpg5, RespuestaTransaccionMpg5> transaction) {
        super(transaction, PeticionTransaccionMpg5::new, PaymentMethod::new, FormatoMPMS1G5.class, FormatoMPMS2G5.class);
    }

    @Override
    protected FormatoMPMENG5 mapInput(DTOIntCard dtoIntCard) {
        return mapper.mapIn(dtoIntCard);
    }

    @Override
    protected PaymentMethod mapFirstOutputFormat(FormatoMPMS1G5 formatoMPMS1G5, DTOIntCard dtoIntCard, PaymentMethod dtoOut) {
        return mapper.mapOut1(formatoMPMS1G5, dtoOut);
    }

    @Override
    protected PaymentMethod mapSecondOutputFormat(FormatoMPMS2G5 formatoMPMS2G5, DTOIntCard dtoIntCard, PaymentMethod dtoOut) {
        return mapper.mapOut2(formatoMPMS2G5, dtoOut);
    }
}

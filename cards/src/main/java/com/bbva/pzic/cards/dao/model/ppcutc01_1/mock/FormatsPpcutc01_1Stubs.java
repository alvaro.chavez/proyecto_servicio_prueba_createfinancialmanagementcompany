package com.bbva.pzic.cards.dao.model.ppcutc01_1.mock;

import com.bbva.pzic.cards.dao.model.ppcutc01_1.Entityout;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 08/03/2020.
 *
 * @author Entelgy
 */
public final class FormatsPpcutc01_1Stubs {

    private static final FormatsPpcutc01_1Stubs INSTANCE = new FormatsPpcutc01_1Stubs();
    private ObjectMapperHelper objectMapperHelper;

    private FormatsPpcutc01_1Stubs() {
        objectMapperHelper = ObjectMapperHelper.getInstance();
    }

    public static FormatsPpcutc01_1Stubs getInstance() {
        return INSTANCE;
    }

    public Entityout getEntityOut() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/cards/dao/model/ppcutc01_1/mock/entityOut.json"), Entityout.class);
    }
}

package com.bbva.pzic.cards.business.dto;


/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntContactDetail {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
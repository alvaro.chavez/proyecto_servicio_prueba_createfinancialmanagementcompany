package com.bbva.pzic.cards.dao.model.cardreports;

public class ModelContacto {

    private String id;

    private ModelTipoContacto tipoContacto;

    private ModelTipoDetalleContacto tipo;

    private String valor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModelTipoContacto getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(ModelTipoContacto tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    public ModelTipoDetalleContacto getTipo() {
        return tipo;
    }

    public void setTipo(ModelTipoDetalleContacto tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}

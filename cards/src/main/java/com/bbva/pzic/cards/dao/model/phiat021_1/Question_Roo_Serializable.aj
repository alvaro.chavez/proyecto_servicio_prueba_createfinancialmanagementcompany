// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.cards.dao.model.phiat021_1;

import com.bbva.pzic.cards.dao.model.phiat021_1.Question;
import java.io.Serializable;

privileged aspect Question_Roo_Serializable {
    
    declare parents: Question implements Serializable;
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    private static final long Question.serialVersionUID = 1L;
    
}

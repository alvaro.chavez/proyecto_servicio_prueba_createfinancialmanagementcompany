package com.bbva.pzic.cards.facade.v1.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "cancellation", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlType(name = "cancellation", namespace = "urn:com:bbva:pzic:cards:facade:v1:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Cancellation implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private OperationData operationData;

    private Status status;

    private Reason reason;

    private CardCancellation card;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OperationData getOperationData() {
        return operationData;
    }

    public void setOperationData(OperationData operationData) {
        this.operationData = operationData;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public CardCancellation getCard() {
        return card;
    }

    public void setCard(CardCancellation card) {
        this.card = card;
    }
}

package com.bbva.pzic.cards.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "itemizeFee", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlType(name = "itemizeFee", namespace = "urn:com:bbva:pzic:cards:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemizeFee implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Fee type identifier.
     */
    private String feeType;
    /**
     * Fee type description.
     */
    private String description;
    /**
     * Detail mode in which the collection fee is applied.
     */
    private Unit itemizeFeeUnit;

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Unit getItemizeFeeUnit() {
        return itemizeFeeUnit;
    }

    public void setItemizeFeeUnit(Unit itemizeFeeUnit) {
        this.itemizeFeeUnit = itemizeFeeUnit;
    }
}

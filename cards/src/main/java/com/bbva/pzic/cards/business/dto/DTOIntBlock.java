package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 20/12/2016.
 *
 * @author Entelgy
 */
public class DTOIntBlock {

    private String blockId;

    @Valid
    private DTOIntCard card;

    @NotNull(groups = {ValidationGroup.ModifyCardBlock.class, ValidationGroup.ModifyPartialCardBlock.class})
    @Size(max = 2, groups = {ValidationGroup.ModifyCardBlock.class, ValidationGroup.ModifyPartialCardBlock.class})
    private String reasonId;

    @NotNull(groups = ValidationGroup.ModifyCardBlock.class)
    @Size(max = 1, groups = {ValidationGroup.ModifyCardBlock.class, ValidationGroup.ModifyPartialCardBlock.class})
    private String isActive;

    @Size(max = 1, groups = ValidationGroup.ModifyCardBlock.class)
    private String isReissued;

    @Size(max = 30, groups = ValidationGroup.ModifyCardBlock.class)
    private String additionalInformation;

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public DTOIntCard getCard() {
        return card;
    }

    public void setCard(DTOIntCard card) {
        this.card = card;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getIsReissued() {
        return isReissued;
    }

    public void setIsReissued(String isReissued) {
        this.isReissued = isReissued;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }
}

package com.bbva.pzic.cards.dao.model.mpg1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>MPG1</code>
 * 
 * @see PeticionTransaccionMpg1
 * @see RespuestaTransaccionMpg1
 */
@Component
public class TransaccionMpg1 implements InvocadorTransaccion<PeticionTransaccionMpg1,RespuestaTransaccionMpg1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionMpg1 invocar(PeticionTransaccionMpg1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpg1.class, RespuestaTransaccionMpg1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionMpg1 invocarCache(PeticionTransaccionMpg1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionMpg1.class, RespuestaTransaccionMpg1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
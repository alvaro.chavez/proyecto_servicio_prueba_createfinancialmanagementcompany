package com.bbva.pzic.cards.dao.model.ppcut004_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>links</code>, utilizado por la clase <code>Paginationout</code></p>
 * 
 * @see Paginationout
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Links {
	
	/**
	 * <p>Campo <code>first</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "first", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
	private String first;
	
	/**
	 * <p>Campo <code>last</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "last", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
	private String last;
	
	/**
	 * <p>Campo <code>previous</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "previous", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
	private String previous;
	
	/**
	 * <p>Campo <code>next</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "next", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
	private String next;
	
}
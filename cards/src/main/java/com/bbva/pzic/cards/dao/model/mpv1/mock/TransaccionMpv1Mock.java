package com.bbva.pzic.cards.dao.model.mpv1.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1E;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1S;
import com.bbva.pzic.cards.dao.model.mpv1.PeticionTransaccionMpv1;
import com.bbva.pzic.cards.dao.model.mpv1.RespuestaTransaccionMpv1;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MPV1</code>
 *
 * @see com.bbva.pzic.cards.dao.model.mpv1.PeticionTransaccionMpv1
 * @see com.bbva.pzic.cards.dao.model.mpv1.RespuestaTransaccionMpv1
 */
@Component("transaccionMpv1")
public class TransaccionMpv1Mock implements InvocadorTransaccion<PeticionTransaccionMpv1, RespuestaTransaccionMpv1> {

    public static final String EMPTY_TEST = "9999";

    @Override
    public RespuestaTransaccionMpv1 invocar(PeticionTransaccionMpv1 transaccion) {
        RespuestaTransaccionMpv1 response = new RespuestaTransaccionMpv1();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        FormatoMPM0V1E formatIn = transaccion.getCuerpo().getParte(FormatoMPM0V1E.class);
        if (EMPTY_TEST.equalsIgnoreCase(formatIn.getNumtarj())) {
            return response;
        }
        FormatoMPM0V1S formatoOut = new FormatoMPM0V1S();
        formatoOut.setIdcorel("1ab23nsan3-12327");
        formatoOut.setNucorel("4567");
        formatoOut.setIdtcore("1");
        formatoOut.setDetcore("International Bank Account Number");

        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(formatoOut);
        response.getCuerpo().getPartes().add(copySalida);
        return response;
    }

    @Override
    public RespuestaTransaccionMpv1 invocarCache(PeticionTransaccionMpv1 transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

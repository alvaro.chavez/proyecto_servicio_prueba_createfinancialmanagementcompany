package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
public class DTOIntDelivery {
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String id;

    private String cardId;
    @Valid
    @NotNull(groups = {
            ValidationGroup.CreateCardsCardProposal.class,
            ValidationGroup.CreateCardsProposal.class,
            ValidationGroup.ModifyCardProposal.class,
            ValidationGroup.CreateCardReportsV0.class
    })
    private DTOIntDestination destination;
    @NotNull(groups = {
            ValidationGroup.ModifyCardProposal.class})
    private String serviceTypeId;
    @Valid
    private DTOIntContact contact;
    @Valid
    private DTOIntAddress address;
    @Valid
    private DTOIntBranch branch;
    @Valid
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private DTOIntServiceType serviceType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntDestination getDestination() {
        return destination;
    }

    public void setDestination(DTOIntDestination destination) {
        this.destination = destination;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public DTOIntContact getContact() {
        return contact;
    }

    public void setContact(DTOIntContact contact) {
        this.contact = contact;
    }

    public DTOIntAddress getAddress() {
        return address;
    }

    public void setAddress(DTOIntAddress address) {
        this.address = address;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public DTOIntBranch getBranch() {
        return branch;
    }

    public void setBranch(DTOIntBranch branch) {
        this.branch = branch;
    }

    public DTOIntServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(DTOIntServiceType serviceType) {
        this.serviceType = serviceType;
    }
}

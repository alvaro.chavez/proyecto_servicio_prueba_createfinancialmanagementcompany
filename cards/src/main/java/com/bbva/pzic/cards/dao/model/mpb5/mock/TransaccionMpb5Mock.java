package com.bbva.pzic.cards.dao.model.mpb5.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.cards.dao.model.mpb5.FormatoMPM0B5E;
import com.bbva.pzic.cards.dao.model.mpb5.PeticionTransaccionMpb5;
import com.bbva.pzic.cards.dao.model.mpb5.RespuestaTransaccionMpb5;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>MPB2</code>
 *
 * @see com.bbva.pzic.cards.dao.model.mpb2.PeticionTransaccionMpb2
 * @see com.bbva.pzic.cards.dao.model.mpb2.RespuestaTransaccionMpb2
 */
@Component("transaccionMpb5")
public class TransaccionMpb5Mock implements InvocadorTransaccion<PeticionTransaccionMpb5, RespuestaTransaccionMpb5> {

    public static final String EMPTY_TEST = "9999";

    @Override
    public RespuestaTransaccionMpb5 invocar(PeticionTransaccionMpb5 transaccion) {
        final RespuestaTransaccionMpb5 response = new RespuestaTransaccionMpb5();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        final FormatoMPM0B5E formatoOut = new FormatoMPM0B5E();
        formatoOut.setIdetarj("4321431323412334");
        formatoOut.setEsttarj("O");

        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(formatoOut);
        response.getCuerpo().getPartes().add(copySalida);
        return response;
    }

    @Override
    public RespuestaTransaccionMpb5 invocarCache(PeticionTransaccionMpb5 peticion) {
        return null;
    }

    @Override
    public void vaciarCache() {
        //this method does not have to be used anymore
    }
}

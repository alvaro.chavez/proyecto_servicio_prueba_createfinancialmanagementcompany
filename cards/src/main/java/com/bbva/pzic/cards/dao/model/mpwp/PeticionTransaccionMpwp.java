package com.bbva.pzic.cards.dao.model.mpwp;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPWP</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpwp</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpwp</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPWP.D1171002.txt
 * MPWPGENERACION DE PIN                  MP        MP2CMPWPPBDMPPO MPMENWP             MPWP  NN3000CNNNNN    SSTN     E  NNNSNNNN  NN                2017-09-08XP85517 2017-09-2916.47.43XP85517 2017-09-08-12.07.14.042894XP85517 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENWP.D1171002.txt
 * MPMENWP �GENERACION DE PIN             �F�15�01066�01�00001�NUMTARJ�NUMERO DE TARJETA   �A�016�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�02�00017�DTCIF01�DATO CIFRADO 01     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�03�00092�DTCIF02�DATO CIFRADO 02     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�04�00167�DTCIF03�DATO CIFRADO 03     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�05�00242�DTCIF04�DATO CIFRADO 04     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�06�00317�DTCIF05�DATO CIFRADO 05     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�07�00392�DTCIF06�DATO CIFRADO 06     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�08�00467�DTCIF07�DATO CIFRADO 07     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�09�00542�DTCIF08�DATO CIFRADO 08     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�10�00617�DTCIF09�DATO CIFRADO 09     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�11�00692�DTCIF10�DATO CIFRADO 10     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�12�00767�DTCIF11�DATO CIFRADO 11     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�13�00842�DTCIF12�DATO CIFRADO 12     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�14�00917�DTCIF13�DATO CIFRADO 13     �A�075�0�R�        �
 * MPMENWP �GENERACION DE PIN             �F�15�01066�15�00992�DTCIF14�DATO CIFRADO 14     �A�075�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPWP.D1171002.txt
</pre></code>
 *
 * @see RespuestaTransaccionMpwp
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPWP",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpwp.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENWP.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpwp implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
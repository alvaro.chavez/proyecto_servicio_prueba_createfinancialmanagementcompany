package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created on 20/12/2016.
 *
 * @author Entelgy
 */
public class DTOIntCard {

    @Size(max = 19, groups = {
            ValidationGroup.ListCardActivations.class,
            ValidationGroup.GetCard.class,
            ValidationGroup.GetCardPaymentMethodsV0.class})
    @Size(max = 20, groups = ValidationGroup.ModifyCardActivation.class)
    @Size(max = 16, groups = {
            ValidationGroup.ModifyCardBlock.class,
            ValidationGroup.ModifyCard.class,
            ValidationGroup.ModifyPartialCardBlock.class,
            ValidationGroup.ModifyCardPin.class,
            ValidationGroup.ModifyCardV0.class})
    @NotNull(groups = {
            ValidationGroup.ModifyCard.class,
            ValidationGroup.ModifyPartialCardBlock.class,
            ValidationGroup.ListCardActivations.class,
            ValidationGroup.GetCard.class,
            ValidationGroup.GetCardPaymentMethodsV0.class,
            ValidationGroup.ModifyCardV0.class})
    private String cardId;

    @Size(max = 1, groups = {ValidationGroup.CreateCard.class, ValidationGroup.CreateCardV0.class})
    @NotNull(groups = {ValidationGroup.CreateCard.class, ValidationGroup.CreateCardV0.class})
    private String cardTypeId;

    private String productId;

    @Size(max = 1, groups = {ValidationGroup.CreateCard.class, ValidationGroup.CreateCardV0.class})
    @NotNull(groups = {ValidationGroup.CreateCard.class, ValidationGroup.CreateCardV0.class})
    private String physicalSupportId;

    private String holderName;

    @NotNull(groups = ValidationGroup.CreateCardV1.class)
    private String currenciesCurrency;
    @NotNull(groups = ValidationGroup.CreateCardV1.class)
    private Boolean currenciesIsMajor;

    @NotNull(groups = {ValidationGroup.ModifyCard.class, ValidationGroup.ModifyCardV0.class})
    private String statusId;
    @Size(max = 2, groups = {ValidationGroup.ModifyCardV0.class})
    private String statusReasonId;

    @Size(max = 20, groups = ValidationGroup.CreateCard.class)
    private String relatedContractId;
    @Size(max = 20, groups = {
            ValidationGroup.CreateCardV0.class,
            ValidationGroup.CreateCardAdvanceV0.class
    })
    private String relatedContractIdLinkedWith;
    @Size(max = 20, groups = ValidationGroup.CreateCardV0.class)
    private String relatedContractIdExpedition;
    @Size(max = 1, groups = ValidationGroup.CreateCardV0.class)
    private String relatedContractIdExpeditionConstant;
    @Size(max = 1, groups = ValidationGroup.CreateCardV0.class)
    private String statementType;
    @Size(max = 2, groups = ValidationGroup.CreateCardV0.class)
    private String titleId;
    @Digits(integer = 2, fraction = 0, groups = ValidationGroup.CreateCardV0.class)
    private Integer cutOffDay;

    @Valid
    private List<DTOIntRelatedContract> relatedContracts;

    @Digits(integer = 2, fraction = 0, groups = ValidationGroup.CreateCardV0.class)
    private Integer paymentMethodEndDay;
    private String deliveryDestinationId;
    @Digits(integer = 9, fraction = 2, groups = ValidationGroup.CreateCardV0.class)
    private BigDecimal grantedCreditsAmount;
    @Size(max = 3, groups = ValidationGroup.CreateCardV0.class)
    private String grantedCreditsCurrency;
    @Size(max = 3, groups = ValidationGroup.CreateCardV0.class)
    private String destinationIdHome;
    @Size(max = 4, groups = ValidationGroup.CreateCardV0.class)
    private String destinationIdBranch;
    @Size(max = 11, groups = ValidationGroup.CreateCardV0.class)
    private String membershipsNumber;
    @Size(max = 8, groups = {ValidationGroup.CreateCardV0.class, ValidationGroup.CreateCardAdvanceV0.class})
    private String participantId;

    @Valid
    private DTOIntPaymentMethod paymentMethod;

    @Size(max = 1, groups = ValidationGroup.CreateCardV0.class)
    private String supportContractType;
    @Size(max = 13, groups = ValidationGroup.CreateCardAdvanceV0.class)
    private String deliveriesManagementContactDetailIdStatement;
    @Size(max = 13, groups = ValidationGroup.CreateCardAdvanceV0.class)
    private String deliveriesManagementContactDetailIdSmsCode;
    @Size(max = 3, groups = ValidationGroup.CreateCardV0.class)
    private String deliveriesManagementServiceTypeIdStatement;
    @Size(max = 3, groups = ValidationGroup.CreateCardV0.class)
    private String deliveriesManagementServiceTypeIdSticker;
    @Size(max = 3, groups = ValidationGroup.CreateCardV0.class)
    private String deliveriesManagementAddressIdStatement;
    @Size(max = 3, groups = ValidationGroup.CreateCardV0.class)
    private String deliveriesManagementAddressIdSticker;
    private String addressId;
    private String contractingBusinessAgentId;
    @Size(max = 8, groups = ValidationGroup.CreateCardV0.class)
    private String contractingBussinesAgentId;
    @Size(max = 1, groups = ValidationGroup.CreateCardV0.class)
    private String contractingBussinesAgentOriginId;
    @Size(max = 8, groups = ValidationGroup.CreateCardV0.class)
    private String marketBusinessAgentId;
    private String bankIdentificationNumber;
    @Valid
    private List<DTOIntImage> images;
    @Valid
    private List<DTOIntDelivery> deliveries;
    @Valid
    private List<DTOIntParticipant> participants;
    @Size(max = 1, groups = ValidationGroup.CreateCardV0.class)
    private String imageId;
    @Size(max = 21, groups = ValidationGroup.CreateCardV0.class)
    private String offerId;
    @Size(max = 4, groups = ValidationGroup.CreateCardV0.class)
    private String managementBranchId;
    @Size(max = 4, groups = ValidationGroup.CreateCardV0.class)
    private String contractingBranchId;
    @Size(max = 16, groups = ValidationGroup.CreateCardAdvanceV0.class)
    @NotNull(groups = ValidationGroup.CreateCardAdvanceV0.class)
    private String number;
    private String deliveryAdressConstant;
    @Size(max = 1, groups = ValidationGroup.CreateCardAdvanceV0.class)
    private String deliveryManagementConstant;
    @Size(max = 20, groups = ValidationGroup.CreateCardAdvanceV0.class)
    private String relatedContractIdRenewalDueTo;
    private String cardAgreement;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardTypeId() {
        return cardTypeId;
    }

    public void setCardTypeId(String cardTypeId) {
        this.cardTypeId = cardTypeId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPhysicalSupportId() {
        return physicalSupportId;
    }

    public void setPhysicalSupportId(String physicalSupportId) {
        this.physicalSupportId = physicalSupportId;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getCurrenciesCurrency() {
        return currenciesCurrency;
    }

    public void setCurrenciesCurrency(String currenciesCurrency) {
        this.currenciesCurrency = currenciesCurrency;
    }

    public Boolean getCurrenciesIsMajor() {
        return currenciesIsMajor;
    }

    public void setCurrenciesIsMajor(Boolean currenciesIsMajor) {
        this.currenciesIsMajor = currenciesIsMajor;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusReasonId() {
        return statusReasonId;
    }

    public void setStatusReasonId(String statusReasonId) {
        this.statusReasonId = statusReasonId;
    }

    public String getRelatedContractId() {
        return relatedContractId;
    }

    public void setRelatedContractId(String relatedContractId) {
        this.relatedContractId = relatedContractId;
    }

    public String getRelatedContractIdLinkedWith() {
        return relatedContractIdLinkedWith;
    }

    public void setRelatedContractIdLinkedWith(String relatedContractIdLinkedWith) {
        this.relatedContractIdLinkedWith = relatedContractIdLinkedWith;
    }

    public String getRelatedContractIdExpedition() {
        return relatedContractIdExpedition;
    }

    public void setRelatedContractIdExpedition(String relatedContractIdExpedition) {
        this.relatedContractIdExpedition = relatedContractIdExpedition;
    }

    public String getRelatedContractIdExpeditionConstant() {
        return relatedContractIdExpeditionConstant;
    }

    public void setRelatedContractIdExpeditionConstant(String relatedContractIdExpeditionConstant) {
        this.relatedContractIdExpeditionConstant = relatedContractIdExpeditionConstant;
    }

    public String getStatementType() {
        return statementType;
    }

    public void setStatementType(String statementType) {
        this.statementType = statementType;
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public Integer getCutOffDay() {
        return cutOffDay;
    }

    public void setCutOffDay(Integer cutOffDay) {
        this.cutOffDay = cutOffDay;
    }

    public List<DTOIntRelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<DTOIntRelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public Integer getPaymentMethodEndDay() {
        return paymentMethodEndDay;
    }

    public void setPaymentMethodEndDay(Integer paymentMethodEndDay) {
        this.paymentMethodEndDay = paymentMethodEndDay;
    }

    public String getDeliveryDestinationId() {
        return deliveryDestinationId;
    }

    public void setDeliveryDestinationId(String deliveryDestinationId) {
        this.deliveryDestinationId = deliveryDestinationId;
    }

    public BigDecimal getGrantedCreditsAmount() {
        return grantedCreditsAmount;
    }

    public void setGrantedCreditsAmount(BigDecimal grantedCreditsAmount) {
        this.grantedCreditsAmount = grantedCreditsAmount;
    }

    public String getGrantedCreditsCurrency() {
        return grantedCreditsCurrency;
    }

    public void setGrantedCreditsCurrency(String grantedCreditsCurrency) {
        this.grantedCreditsCurrency = grantedCreditsCurrency;
    }

    public String getDestinationIdHome() {
        return destinationIdHome;
    }

    public void setDestinationIdHome(String destinationIdHome) {
        this.destinationIdHome = destinationIdHome;
    }

    public String getDestinationIdBranch() {
        return destinationIdBranch;
    }

    public void setDestinationIdBranch(String destinationIdBranch) {
        this.destinationIdBranch = destinationIdBranch;
    }

    public DTOIntPaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(DTOIntPaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getMembershipsNumber() {
        return membershipsNumber;
    }

    public void setMembershipsNumber(String membershipsNumber) {
        this.membershipsNumber = membershipsNumber;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getSupportContractType() {
        return supportContractType;
    }

    public void setSupportContractType(String supportContractType) {
        this.supportContractType = supportContractType;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getContractingBussinesAgentId() {
        return contractingBussinesAgentId;
    }

    public void setContractingBussinesAgentId(String contractingBussinesAgentId) {
        this.contractingBussinesAgentId = contractingBussinesAgentId;
    }

    public String getContractingBusinessAgentId() {
        return contractingBusinessAgentId;
    }

    public void setContractingBusinessAgentId(String contractingBusinessAgentId) {
        this.contractingBusinessAgentId = contractingBusinessAgentId;
    }

    public String getContractingBussinesAgentOriginId() {
        return contractingBussinesAgentOriginId;
    }

    public void setContractingBussinesAgentOriginId(String contractingBussinesAgentOriginId) {
        this.contractingBussinesAgentOriginId = contractingBussinesAgentOriginId;
    }

    public String getMarketBusinessAgentId() {
        return marketBusinessAgentId;
    }

    public void setMarketBusinessAgentId(String marketBusinessAgentId) {
        this.marketBusinessAgentId = marketBusinessAgentId;
    }

    public String getBankIdentificationNumber() {
        return bankIdentificationNumber;
    }

    public void setBankIdentificationNumber(String bankIdentificationNumber) {
        this.bankIdentificationNumber = bankIdentificationNumber;
    }

    public List<DTOIntImage> getImages() {
        return images;
    }

    public void setImages(List<DTOIntImage> images) {
        this.images = images;
    }

    public List<DTOIntDelivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<DTOIntDelivery> deliveries) {
        this.deliveries = deliveries;
    }

    public List<DTOIntParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<DTOIntParticipant> participants) {
        this.participants = participants;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getManagementBranchId() {
        return managementBranchId;
    }

    public void setManagementBranchId(String managementBranchId) {
        this.managementBranchId = managementBranchId;
    }

    public String getContractingBranchId() {
        return contractingBranchId;
    }

    public void setContractingBranchId(String contractingBranchId) {
        this.contractingBranchId = contractingBranchId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDeliveryAdressConstant() {
        return deliveryAdressConstant;
    }

    public void setDeliveryAdressConstant(String deliveryAdressConstant) {
        this.deliveryAdressConstant = deliveryAdressConstant;
    }

    public String getDeliveryManagementConstant() {
        return deliveryManagementConstant;
    }

    public void setDeliveryManagementConstant(String deliveryManagementConstant) {
        this.deliveryManagementConstant = deliveryManagementConstant;
    }

    public String getRelatedContractIdRenewalDueTo() {
        return relatedContractIdRenewalDueTo;
    }

    public void setRelatedContractIdRenewalDueTo(String relatedContractIdRenewalDueTo) {
        this.relatedContractIdRenewalDueTo = relatedContractIdRenewalDueTo;
    }

    public String getDeliveriesManagementContactDetailIdStatement() {
        return deliveriesManagementContactDetailIdStatement;
    }

    public void setDeliveriesManagementContactDetailIdStatement(String deliveriesManagementContactDetailIdStatement) {
        this.deliveriesManagementContactDetailIdStatement = deliveriesManagementContactDetailIdStatement;
    }

    public String getDeliveriesManagementContactDetailIdSmsCode() {
        return deliveriesManagementContactDetailIdSmsCode;
    }

    public void setDeliveriesManagementContactDetailIdSmsCode(String deliveriesManagementContactDetailIdSmsCode) {
        this.deliveriesManagementContactDetailIdSmsCode = deliveriesManagementContactDetailIdSmsCode;
    }

    public String getDeliveriesManagementServiceTypeIdStatement() {
        return deliveriesManagementServiceTypeIdStatement;
    }

    public void setDeliveriesManagementServiceTypeIdStatement(String deliveriesManagementServiceTypeIdStatement) {
        this.deliveriesManagementServiceTypeIdStatement = deliveriesManagementServiceTypeIdStatement;
    }

    public String getDeliveriesManagementServiceTypeIdSticker() {
        return deliveriesManagementServiceTypeIdSticker;
    }

    public void setDeliveriesManagementServiceTypeIdSticker(String deliveriesManagementServiceTypeIdSticker) {
        this.deliveriesManagementServiceTypeIdSticker = deliveriesManagementServiceTypeIdSticker;
    }

    public String getDeliveriesManagementAddressIdStatement() {
        return deliveriesManagementAddressIdStatement;
    }

    public void setDeliveriesManagementAddressIdStatement(String deliveriesManagementAddressIdStatement) {
        this.deliveriesManagementAddressIdStatement = deliveriesManagementAddressIdStatement;
    }

    public String getDeliveriesManagementAddressIdSticker() {
        return deliveriesManagementAddressIdSticker;
    }

    public void setDeliveriesManagementAddressIdSticker(String deliveriesManagementAddressIdSticker) {
        this.deliveriesManagementAddressIdSticker = deliveriesManagementAddressIdSticker;
    }

    public String getCardAgreement() {
        return cardAgreement;
    }

    public void setCardAgreement(String cardAgreement) {
        this.cardAgreement = cardAgreement;
    }
}

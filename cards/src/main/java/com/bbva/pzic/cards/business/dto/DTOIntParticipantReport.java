package com.bbva.pzic.cards.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class DTOIntParticipantReport {
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String id;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String firstName;

    private String middleName;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    private String lastName;

    private String secondLastName;
    @NotNull(groups = ValidationGroup.CreateCardReportsV0.class)
    @Valid
    private List<DTOIntIdentityDocument> identityDocuments;
    @Valid
    private List<DTOIntContactDetailReport> contactDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public List<DTOIntIdentityDocument> getIdentityDocuments() {
        return identityDocuments;
    }

    public void setIdentityDocuments(List<DTOIntIdentityDocument> identityDocuments) {
        this.identityDocuments = identityDocuments;
    }

    public List<DTOIntContactDetailReport> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<DTOIntContactDetailReport> contactDetails) {
        this.contactDetails = contactDetails;
    }
}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntRelatedContractsSearchCriteria;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMENL5;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMS1L5;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardRelatedContractsMapper;
import com.bbva.pzic.cards.facade.v0.dto.NumberType;
import com.bbva.pzic.cards.facade.v0.dto.Product;
import com.bbva.pzic.cards.facade.v0.dto.RelatedContracts;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("txListCardRelatedContractsMapper")
public class TxListCardRelatedContractsMapper implements ITxListCardRelatedContractsMapper {

    @Autowired
    private Translator translator;

    @Autowired
    private AbstractCypherTool cypherTool;

    @Override
    public FormatoMPMENL5 mapInput(final DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria) {
        FormatoMPMENL5 formatoMPMENL5 = new FormatoMPMENL5();
        formatoMPMENL5.setIdetarj(dtoIntRelatedContractsSearchCriteria.getCardId());
        return formatoMPMENL5;
    }

    @Override
    public List<RelatedContracts> mapOutput(final FormatoMPMS1L5 formatoMPMS1L5, final List<RelatedContracts> dtoIntRelatedContractsList) {
        RelatedContracts data = new RelatedContracts();
        data.setRelatedContractId(formatoMPMS1L5.getIdrela());
        data.setContractId(cypherTool.encrypt(formatoMPMS1L5.getNucorel(), AbstractCypherTool.RELATED_CONTRACT_ID));
        data.setNumber(cypherTool.mask(formatoMPMS1L5.getNumprod(), AbstractCypherTool.CARD_NUMBER));

        if (formatoMPMS1L5.getTnumid() != null || formatoMPMS1L5.getTnumdes() != null) {
            NumberType numberType = new NumberType();
            numberType.setId(translator.translateBackendEnumValue("cards.numberType.id", formatoMPMS1L5.getTnumid()));
            numberType.setName(formatoMPMS1L5.getTnumdes());
            data.setNumberType(numberType);
        }

        if (formatoMPMS1L5.getProdid() != null || formatoMPMS1L5.getProddes() != null) {
            Product product = new Product();
            product.setId(translator.translateBackendEnumValueStrictly("cards.product.id", formatoMPMS1L5.getProdid()));
            product.setName(formatoMPMS1L5.getProddes());
            data.setProduct(product);
        }

        dtoIntRelatedContractsList.add(data);
        return dtoIntRelatedContractsList;
    }
}

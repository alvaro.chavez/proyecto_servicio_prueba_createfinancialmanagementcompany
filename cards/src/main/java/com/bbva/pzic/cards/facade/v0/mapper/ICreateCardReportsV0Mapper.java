package com.bbva.pzic.cards.facade.v0.mapper;

import com.bbva.pzic.cards.business.dto.InputCreateCardReports;
import com.bbva.pzic.cards.facade.v0.dto.ReportCardCreation;

public interface ICreateCardReportsV0Mapper {

    InputCreateCardReports mapIn(String cardId, ReportCardCreation reportCardCreation);

}

package com.bbva.pzic.cards.util;

/**
 * @author Entelgy
 */
public final class Enums {

    public static final String SHIPMENT_ID = "shipment-id";
    public static final String CARD_ID = "card-id";
    public static final String PUBLIC_KEY = "publicKey";
    public static final String IS_ACTIVE = "isActive";
    public static final String ACTIVATION_ID = "activation-id";
    public static final String CUSTOMER_ID = "customer.id";
    public static final String LIMIT_ID = "limit-id";
    public static final String CARD_TYPE_ID = "cardType.id";
    public static final String PHYSICAL_SUPPORT_ID = "physicalSupport.id";
    public static final String STATUS_ID = "status.id";
    public static final String FROM_OPERATION_DATE = "fromOperationDate";
    public static final String TO_OPERATION_DATE = "toOperationDate";
    public static final String PAGINATION_KEY = "paginationKey";
    public static final String PAGE_SIZE = "pageSize";
    public static final String EXPAND = "expand";
    public static final String OPERATION_OPERATIONDATE = "operation.operationDate";
    public static final String INSTALLMENT_PLAN_ID = "installment-plan-id";

    public static final String BLOCK_ID = "block-id";

    public static final String OFFER_ID = "offer-id";
    public static final String PARTICIPANTS_PARTICIPANT_TYPE_ID = "participants.participantType.id";
    public static final String TRANSACTION_ID = "transaction-id";
    public static final String FINANCIAL_STATEMENT_ID = "financial-statement-id";
    public static final String CONTENT_TYPE = "content-type";

    public static final String CARDS_OFFER_TYPE_ID = "cards.offerType.id";
    public static final String CARDS_CARD_TYPE_ID = "cards.cardType.id";
    public static final String CARDS_ITEMIZE_FEES_FEE_TYPE = "cards.itemizeFees.feeType";
    public static final String CARDS_ITEMIZE_FEES_MODE = "cards.itemizeFees.mode";
    public static final String CARDS_ADDITIONAL_PRODUCTS_PRODUCT_TYPE = "cards.additionalProducts.productType";
    public static final String CARDS_OFFERS_ADDITIONAL_PRODUCTS_PRODUCT_TYPE = "cards.offers.additionalProducts.productType";
    public static final String CARDS_PERIOD_ID = "cards.period.id";
    public static final String CARDS_PAYMENT_METHOD_ID = "cards.paymentMethod.id";
    public static final String CARDS_RATES_MODE = "cards.rates.mode";
    public static final String CARD_AGREEMENT = "cardAgreement";
    public static final String CARDS_REFERENCE_NUMBER = "referenceNumber";
    public static final String CARDS_EXTERNAL_CODE = "externalCode";
    public static final String CARDS_SHIPPING_COMPANY_ID = "shippingCompany.id";
    public static final String FINANCIAL_STATEMENT_CONDITION_FORMAT_TYPE = "financialStatement.condition.formatType";
    public static final String FINANCIAL_STATEMENT_SPECIFIC_AMOUNTS_ID = "financialStatement.specificAmounts.id";
    public static final String FINANCIAL_STATEMENT_RATE_UNIT_RATE_TYPE = "financialStatement.rate.unitRateType";
    public static final String TRANSACTION_REFUND_REFUND_TYPE_ID_KEY = "transactionRefund.refundType.id";
    public static final String TRANSACTION_REFUND_STATUS_ID = "transactionRefund.status.id";
    public static final String CARD_CANCELLATIONS_REASONID = "card.cancellations.reasonId";
    public static final String CARD_CANCELLATIONS_STATUSID = "card.cancellations.statusId";
    public static final String CARDS_NUMBERTYPE_ID = "cards.numberType.id";

    public static final String TRANSACTIONREFUND_REFUNDTYPE_ID = "transactionRefund.refundType.id";
    public static final String TRANSACTIONREFUND_STATUS_ID = "transactionRefund.status.id";
    public static final String CARDS_PHYSICAL_SUPPORT_ID = "cards.physicalSupport.id";

    public static final String INSTALLMENTSPLANS_FINANCINGTYPE_ID = "installmentsPlans.financingType.id";
    public static final String INSTALLMENTSPLANS_TERMS_FREQUENCY = "installmentsPlans.terms.frequency";
    public static final String INSTALLMENTSPLANS_RATES_UNITRATETYPE = "installmentsPlans.rates.unitRateType";
    public static final String INSTALLMENTSPLANS_INSTALLMENTS_STATUS_ID = "installmentsPlans.installments.status.id";
    public static final String INSTALLMENTSPLANS_INSTALLMENTS_SPECIFICAMOUNTS_ID = "installmentsPlans.installments.specificAmounts.id";
    public static final String INSTALLMENTSPLANS_SPECIFICAMOUNTS_ID = "installmentsPlans.specificAmounts.id";
    public static final String INSTALLMENTSPLANS_OPERATION_LINETYPE_ID = "installmentsPlans.operation.lineType.id";
    public static final String INSTALLMENTSPLANS_OPERATION_TRANSACTIONTYPE_ID = "installmentsPlans.operation.transactionType.id";
    public static final String INSTALLMENTSPLANS_FINANCINGCALCULATIONFORMAT_ID = "installmentsPlans.financingCalculationFormat.id";

    private Enums() {
    }
}

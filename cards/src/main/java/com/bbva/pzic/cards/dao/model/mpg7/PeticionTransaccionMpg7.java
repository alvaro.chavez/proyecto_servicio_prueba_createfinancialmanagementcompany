package com.bbva.pzic.cards.dao.model.mpg7;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MPG7</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpg7</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMpg7</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.MPG7.D1190118.txt
 * MPG7INCREMENTO DE LINEA                MP        MP2CMPG7PBDMPPO MPMENG7             MPG7  NN3000CNNNNN    SSTN     E  NNNSSNNN  NN                2017-11-15XP85273 2019-01-1813.07.52XP92348 2017-11-15-12.49.26.630434XP85273 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMENG7.D1190118.txt
 * MPMENG7 �INCREMENTO DE LINEA           �F�05�00067�01�00001�IDETARJ�NRO.TARJETA CLIENTE �A�019�0�R�        �
 * MPMENG7 �INCREMENTO DE LINEA           �F�05�00067�02�00020�TIPLIM �TIPO DE LIMITE      �A�020�0�R�        �
 * MPMENG7 �INCREMENTO DE LINEA           �F�05�00067�03�00040�IMPORTE�NUEVO IMPORTE LINEA �N�015�2�R�        �
 * MPMENG7 �INCREMENTO DE LINEA           �F�05�00067�04�00055�DIVISA �MONEDA              �A�003�0�R�        �
 * MPMENG7 �INCREMENTO DE LINEA           �F�05�00067�05�00058�CODOFER�CODIGO DE OFERTA    �A�010�0�O�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.MPMS1G7.D1190118.txt
 * MPMS1G7 �INCREMENTO DE LINEA           �X�02�00018�01�00001�FECOPER�FECHA DE OPERACION  �A�010�0�S�        �
 * MPMS1G7 �INCREMENTO DE LINEA           �X�02�00018�02�00011�HOROPER�HORA DE OPERACION   �A�008�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.MPG7.D1190118.txt
 * MPG7MPMS1G7 MPNCS1G7MP2CMPG71S                             XP92348 2017-11-22-11.03.22.126157XP92348 2017-11-22-11.03.22.126963
</pre></code>
 *
 * @see RespuestaTransaccionMpg7
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MPG7",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMpg7.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMPMENG7.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpg7 implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
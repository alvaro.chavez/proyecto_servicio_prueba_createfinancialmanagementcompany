package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.canonic.SecurityData;
import com.bbva.pzic.cards.dao.model.mpcv.FormatoMPMENCV;
import com.bbva.pzic.cards.dao.model.mpcv.FormatoMPMS1CV;
import com.bbva.pzic.cards.dao.model.mpcv.mock.FormatMpcvMock;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardSecurityDataMapper;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

public class TxGetCardSecurityDataMapperTest {

    @InjectMocks
    private ITxGetCardSecurityDataMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    private EntityMock entityMock = EntityMock.getInstance();
    private FormatMpcvMock formatMpcvMock;

    @Before
    public void setUp() {
        mapper = new TxGetCardSecurityDataMapper();
        MockitoAnnotations.initMocks(this);

        formatMpcvMock = new FormatMpcvMock();
    }

    @Test
    public void mapInTest() {
        InputGetCardSecurityData dto = entityMock.getInputGetCardSecurityData();
        FormatoMPMENCV result = mapper.mapIn(dto);

        assertNotNull(result.getNumtarj());
        assertNotNull(result.getKeypb01());
        assertNotNull(result.getKeypb02());
        assertNotNull(result.getKeypb03());
        assertNotNull(result.getKeypb04());
        assertNotNull(result.getKeypb05());
        assertNotNull(result.getKeypb06());
        assertNotNull(result.getKeypb07());
        assertNotNull(result.getKeypb08());
        assertNotNull(result.getKeypb09());
        assertNotNull(result.getKeypb10());
        assertNotNull(result.getKeypb11());
        assertNotNull(result.getKeypb12());
        assertNotNull(result.getKeypb13());
        assertNotNull(result.getKeypb14());

        assertEquals(dto.getCardId(), result.getNumtarj());
        assertEquals(CIF_01, result.getKeypb01());
        assertEquals(CIF_02, result.getKeypb02());
        assertEquals(CIF_03, result.getKeypb03());
        assertEquals(CIF_04, result.getKeypb04());
        assertEquals(CIF_05, result.getKeypb05());
        assertEquals(CIF_06, result.getKeypb06());
        assertEquals(CIF_07, result.getKeypb07());
        assertEquals(CIF_08, result.getKeypb08());
        assertEquals(CIF_09, result.getKeypb09());
        assertEquals(CIF_10, result.getKeypb10());
        assertEquals(CIF_11, result.getKeypb11());
        assertEquals(CIF_12, result.getKeypb12());
        assertEquals(CIF_13, result.getKeypb13());
        assertEquals(CIF_14, result.getKeypb14());
    }

    @Test
    public void mapInPartialTest() {
        InputGetCardSecurityData dto = entityMock.getInputGetCardSecurityData();
        final String partial = "12345678";
        dto.setPublicKey(CIF_01 + CIF_02 + CIF_03 + CIF_04 + CIF_05 + CIF_06 + partial);
        FormatoMPMENCV result = mapper.mapIn(dto);

        assertNotNull(result.getNumtarj());
        assertNotNull(result.getKeypb01());
        assertNotNull(result.getKeypb02());
        assertNotNull(result.getKeypb03());
        assertNotNull(result.getKeypb04());
        assertNotNull(result.getKeypb05());
        assertNotNull(result.getKeypb06());
        assertNotNull(result.getKeypb07());
        assertNull(result.getKeypb08());
        assertNull(result.getKeypb09());
        assertNull(result.getKeypb10());
        assertNull(result.getKeypb11());
        assertNull(result.getKeypb12());
        assertNull(result.getKeypb13());
        assertNull(result.getKeypb14());

        assertEquals(dto.getCardId(), result.getNumtarj());
        assertEquals(CIF_01, result.getKeypb01());
        assertEquals(CIF_02, result.getKeypb02());
        assertEquals(CIF_03, result.getKeypb03());
        assertEquals(CIF_04, result.getKeypb04());
        assertEquals(CIF_05, result.getKeypb05());
        assertEquals(CIF_06, result.getKeypb06());
        assertEquals(partial, result.getKeypb07());
    }

    @Test
    public void mapInWithPublicKeyEmptyTest() {
        InputGetCardSecurityData dto = entityMock.getInputGetCardSecurityData();
        dto.setPublicKey("");
        FormatoMPMENCV result = mapper.mapIn(dto);

        assertNotNull(result);
        assertNull(result.getKeypb01());
        assertNull(result.getKeypb02());
        assertNull(result.getKeypb03());
        assertNull(result.getKeypb04());
        assertNull(result.getKeypb05());
        assertNull(result.getKeypb06());
        assertNull(result.getKeypb07());
        assertNull(result.getKeypb08());
        assertNull(result.getKeypb09());
        assertNull(result.getKeypb10());
        assertNull(result.getKeypb11());
        assertNull(result.getKeypb12());
        assertNull(result.getKeypb13());
        assertNull(result.getKeypb14());
    }

    @Test
    public void mapOutTest() {
        FormatoMPMS1CV format = formatMpcvMock.getFormatoMPMS1CV();

        Mockito.when(enumMapper.getEnumValue("securityData.id", format.getIdcodsg())).thenReturn("CVV2");

        SecurityData result = mapper.mapOut(format);

        assertNotNull(result.getId());
        assertNotNull(result.getName());
        assertNotNull(result.getCode());

        assertEquals(format.getIdcodsg(), result.getId());
        assertEquals(format.getDscodsg(), result.getName());
        assertEquals("10735184729553162427453836493751086219732084319542165327", result.getCode());
    }

    @Test
    public void mapOutInitializedTest() {
        SecurityData result = mapper.mapOut(new FormatoMPMS1CV());

        assertNotNull(result);
        assertNull(result.getId());
        assertNull(result.getName());
        assertTrue(result.getCode().isEmpty());
    }

    @Test
    public void mapOutNullTest() {
        SecurityData result = mapper.mapOut(null);

        assertNull(result);
    }
}
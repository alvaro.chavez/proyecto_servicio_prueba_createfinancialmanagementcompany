package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOInstallmentsPlanList;
import com.bbva.pzic.cards.business.dto.InputListInstallmentPlans;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.*;
import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_LIST_INSTALLMENT_PLANS;

/**
 * Created on 09/02/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ListInstallmentPlansMapperTest {

    @InjectMocks
    private ListInstallmentPlansMapper plansMapper;
    @Mock
    private AbstractCypherTool cypherTool;

    @Before
    public void init() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_LIST_INSTALLMENT_PLANS))
                .thenReturn(CARD_ID);
    }

    @Test
    public void mapInFullTest() {
        InputListInstallmentPlans result = plansMapper.mapIn(CARD_ENCRYPT_ID, PAGINATION_KEY, PAGINATION_SIZE.longValue());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getPaginationKey());
        Assert.assertNotNull(result.getPageSize());

        Assert.assertEquals(CARD_ID, result.getCardId());
        Assert.assertEquals(PAGINATION_KEY, result.getPaginationKey());
        Assert.assertEquals(PAGINATION_SIZE.toString(), result.getPageSize().toString());
    }

    @Test
    public void mapInWithParametersNullsTest() {
        InputListInstallmentPlans result = plansMapper.mapIn(null, null, null);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getCardId());
        Assert.assertNull(result.getPaginationKey());
        Assert.assertNull(result.getPageSize());
    }

    @Test
    public void mapOutFullTest() {
        DTOInstallmentsPlanList dtoInstallmentsPlanList = new DTOInstallmentsPlanList();
        dtoInstallmentsPlanList.setData(Collections.singletonList(new InstallmentsPlan()));
        ServiceResponse<List<InstallmentsPlan>> result = plansMapper.mapOut(dtoInstallmentsPlanList, new Pagination());
        Assert.assertNotNull(result);
    }

    @Test
    public void mapOutWithDTOOfOutisNullTest() {
        ServiceResponse<List<InstallmentsPlan>> result = plansMapper.mapOut(null, null);
        Assert.assertNull(result);
    }
}

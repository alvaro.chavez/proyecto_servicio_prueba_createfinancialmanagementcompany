package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputModifyCardLimit;
import com.bbva.pzic.cards.canonic.Limit;
import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMENG7;
import com.bbva.pzic.cards.dao.model.mpg7.FormatoMPMS1G7;
import com.bbva.pzic.cards.dao.model.mpg7.mock.FormatsMpg7Mock;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardLimitV0Mapper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created on 20/11/2017.
 *
 * @author Entelgy
 */
public class TxModifyCardLimitV0MapperTest {

    private ITxModifyCardLimitV0Mapper limitV0Mapper;
    private EntityMock entityMock = EntityMock.getInstance();
    private DummyMock dummyMock;

    @Before
    public void setUp() {
        limitV0Mapper = new TxModifyCardLimitV0Mapper();
        dummyMock = new DummyMock();
    }

    @Test
    public void testMapInFull() {
        InputModifyCardLimit input = entityMock.getInputModifyCardLimit();
        FormatoMPMENG7 result = limitV0Mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertNotNull(result.getTiplim());
        assertNotNull(result.getDivisa());
        assertNotNull(result.getImporte());
        assertNotNull(result.getCodofer());

        assertEquals(result.getIdetarj(), input.getCardId());
        assertEquals(result.getTiplim(), input.getLimitId());
        assertEquals(result.getDivisa(), input.getAmountLimitCurrency());
        assertEquals(result.getImporte(), input.getAmountLimitAmount());
        assertEquals(result.getCodofer(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutIdetarjNull() {
        InputModifyCardLimit input = entityMock.getInputModifyCardLimit();
        input.setCardId(null);
        FormatoMPMENG7 result = limitV0Mapper.mapIn(input);

        assertNotNull(result);
        assertNull(result.getIdetarj());
        assertNotNull(result.getTiplim());
        assertNotNull(result.getDivisa());
        assertNotNull(result.getImporte());
        assertNotNull(result.getCodofer());

        assertEquals(result.getTiplim(), input.getLimitId());
        assertEquals(result.getDivisa(), input.getAmountLimitCurrency());
        assertEquals(result.getImporte(), input.getAmountLimitAmount());
        assertEquals(result.getCodofer(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutTiplin() {
        InputModifyCardLimit input = entityMock.getInputModifyCardLimit();
        input.setLimitId(null);
        FormatoMPMENG7 result = limitV0Mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertNull(result.getTiplim());
        assertNotNull(result.getDivisa());
        assertNotNull(result.getImporte());
        assertNotNull(result.getCodofer());

        assertEquals(result.getIdetarj(), input.getCardId());
        assertEquals(result.getDivisa(), input.getAmountLimitCurrency());
        assertEquals(result.getImporte(), input.getAmountLimitAmount());
        assertEquals(result.getCodofer(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutDivisaNull() {
        InputModifyCardLimit input = entityMock.getInputModifyCardLimit();
        input.setAmountLimitCurrency(null);
        FormatoMPMENG7 result = limitV0Mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertNotNull(result.getTiplim());
        assertNull(result.getDivisa());
        assertNotNull(result.getImporte());
        assertNotNull(result.getCodofer());

        assertEquals(result.getIdetarj(), input.getCardId());
        assertEquals(result.getTiplim(), input.getLimitId());
        assertEquals(result.getImporte(), input.getAmountLimitAmount());
        assertEquals(result.getCodofer(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutImporte() {
        InputModifyCardLimit input = entityMock.getInputModifyCardLimit();
        input.setAmountLimitAmount(null);
        FormatoMPMENG7 result = limitV0Mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertNotNull(result.getTiplim());
        assertNotNull(result.getDivisa());
        assertNull(result.getImporte());
        assertNotNull(result.getCodofer());

        assertEquals(result.getIdetarj(), input.getCardId());
        assertEquals(result.getTiplim(), input.getLimitId());
        assertEquals(result.getDivisa(), input.getAmountLimitCurrency());
        assertEquals(result.getCodofer(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutOfferId() {
        InputModifyCardLimit input = entityMock.getInputModifyCardLimit();
        input.setOfferId(null);
        FormatoMPMENG7 result = limitV0Mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertNotNull(result.getTiplim());
        assertNotNull(result.getDivisa());
        assertNotNull(result.getImporte());
        assertNull(result.getCodofer());

        assertEquals(result.getIdetarj(), input.getCardId());
        assertEquals(result.getTiplim(), input.getLimitId());
        assertEquals(result.getDivisa(), input.getAmountLimitCurrency());
        assertEquals(result.getImporte(), input.getAmountLimitAmount());
    }

    @Test
    public void testMapOutFull() {
        FormatoMPMS1G7 format = FormatsMpg7Mock.getInstance().getFormatoMPMS1G7();
        Limit result = limitV0Mapper.mapOut(format);

        assertNotNull(result);
        assertNotNull(result.getOperationDate());

        assertEquals(dummyMock.buildDate(format.getFecoper(), format.getHoroper(), "yyyy-MM-ddHH:mm:ss"), result.getOperationDate().getTime());
    }

    @Test
    public void testMapOutWithoutFecoper() {
        FormatoMPMS1G7 format = new FormatoMPMS1G7();
        Limit result = limitV0Mapper.mapOut(format);

        assertNull(result);
    }

    @Test
    public void testMapOutWithoutFormatOfResponse() {
        Limit result = limitV0Mapper.mapOut(null);

        assertNull(result);
    }

}

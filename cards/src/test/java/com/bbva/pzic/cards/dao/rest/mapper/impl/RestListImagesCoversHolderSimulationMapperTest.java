package com.bbva.pzic.cards.dao.rest.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.canonic.HolderSimulation;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesRequest;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesResponse;
import com.bbva.pzic.cards.dao.model.awsimages.ModelImage;
import com.bbva.pzic.cards.dao.rest.mock.stubs.AWSImagesStubs;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created on 20/02/2020.
 *
 * @author Entelgy
 */
public class RestListImagesCoversHolderSimulationMapperTest {

    private final RestListImagesCoversHolderSimulationMapper mapper = new RestListImagesCoversHolderSimulationMapper();

    @Test
    public void mapInFullTest() throws IOException {
        List<HolderSimulation> input = EntityMock.getInstance().fakeCardsHolderSimulation();

        AWSImagesRequest response = mapper.mapIn(input.get(0));
        assertNotNull(response);
        assertNotNull(response.getCards());
        assertEquals(1, response.getCards().size());
        assertNotNull(response.getCards().get(0).getCodProd());
        assertNotNull(response.getCards().get(0).getBin());
        assertNotNull(response.getCards().get(0).getType().getId());
        assertNotNull(response.getCards().get(0).getIssueDate());
        assertNull(response.getCards().get(0).getIsBackImage());
        assertNotNull(response.getCountry().getId());
        assertNotNull(response.getWidth());
        assertNotNull(response.getHeight());
        assertNotNull(response.getIsDefaultImage());

        // pg=0021&bin=414791&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171123&type=V&width=512&height=324
        assertEquals("0021", response.getCards().get(0).getCodProd());
        assertEquals("414791", response.getCards().get(0).getBin());
        assertEquals("V", response.getCards().get(0).getType().getId());
        assertEquals("2017-11-23", response.getCards().get(0).getIssueDate());
        assertEquals("pe", response.getCountry().getId());
        assertEquals("512", response.getWidth());
        assertEquals("324", response.getHeight());
        assertTrue(response.getIsDefaultImage());

        response = mapper.mapIn(input.get(1));
        assertNotNull(response);
        assertNotNull(response.getCards());
        assertEquals(1, response.getCards().size());
        assertNotNull(response.getCards().get(0).getCodProd());
        assertNotNull(response.getCards().get(0).getBin());
        assertNotNull(response.getCards().get(0).getType().getId());
        assertNotNull(response.getCards().get(0).getIssueDate());
        assertNotNull(response.getCards().get(0).getIsBackImage());
        assertNotNull(response.getCountry().getId());
        assertNotNull(response.getWidth());
        assertNotNull(response.getHeight());
        assertNotNull(response.getIsDefaultImage());

        // pg=0022&bin=414792&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171124&back=true&type=V&width=512&height=324
        assertEquals("0022", response.getCards().get(0).getCodProd());
        assertEquals("414792", response.getCards().get(0).getBin());
        assertEquals("V", response.getCards().get(0).getType().getId());
        assertEquals("2017-11-24", response.getCards().get(0).getIssueDate());
        assertTrue(response.getCards().get(0).getIsBackImage());
        assertEquals("pe", response.getCountry().getId());
        assertEquals("512", response.getWidth());
        assertEquals("324", response.getHeight());
        assertTrue(response.getIsDefaultImage());

        response = mapper.mapIn(input.get(2));
        assertNotNull(response);
        assertNotNull(response.getCards());
        assertEquals(1, response.getCards().size());
        assertNotNull(response.getCards().get(0).getCodProd());
        assertNotNull(response.getCards().get(0).getBin());
        assertNotNull(response.getCards().get(0).getType().getId());
        assertNotNull(response.getCards().get(0).getIssueDate());
        assertNull(response.getCards().get(0).getIsBackImage());
        assertNotNull(response.getCountry().getId());
        assertNotNull(response.getWidth());
        assertNotNull(response.getHeight());
        assertNotNull(response.getIsDefaultImage());

        // pg=0023&bin=414793&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171124&width=512&height=324
        assertEquals("0023", response.getCards().get(0).getCodProd());
        assertEquals("414793", response.getCards().get(0).getBin());
        assertEquals("N", response.getCards().get(0).getType().getId());
        assertEquals("2017-11-24", response.getCards().get(0).getIssueDate());
        assertEquals("pe", response.getCountry().getId());
        assertEquals("512", response.getWidth());
        assertEquals("324", response.getHeight());
        assertTrue(response.getIsDefaultImage());

        response = mapper.mapIn(input.get(3));
        assertNotNull(response);
        assertNotNull(response.getCards());
        assertEquals(1, response.getCards().size());
        assertNotNull(response.getCards().get(0).getCodProd());
        assertNotNull(response.getCards().get(0).getBin());
        assertNotNull(response.getCards().get(0).getType().getId());
        assertNotNull(response.getCards().get(0).getIssueDate());
        assertNotNull(response.getCards().get(0).getIsBackImage());
        assertNotNull(response.getCountry().getId());
        assertNotNull(response.getWidth());
        assertNotNull(response.getHeight());
        assertNotNull(response.getIsDefaultImage());

        // pg=0024&bin=414794&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171124&back=true&width=512&height=324
        assertEquals("0024", response.getCards().get(0).getCodProd());
        assertEquals("414794", response.getCards().get(0).getBin());
        assertEquals("N", response.getCards().get(0).getType().getId());
        assertEquals("2017-11-24", response.getCards().get(0).getIssueDate());
        assertTrue(response.getCards().get(0).getIsBackImage());
        assertEquals("pe", response.getCountry().getId());
        assertEquals("512", response.getWidth());
        assertEquals("324", response.getHeight());
        assertTrue(response.getIsDefaultImage());

        response = mapper.mapIn(input.get(4));
        assertNotNull(response);
        assertNotNull(response.getCards());
        assertEquals(1, response.getCards().size());
        assertNotNull(response.getCards().get(0).getCodProd());
        assertNotNull(response.getCards().get(0).getBin());
        assertNotNull(response.getCards().get(0).getType().getId());
        assertNull(response.getCards().get(0).getIssueDate());
        assertNotNull(response.getCards().get(0).getIsBackImage());
        assertNotNull(response.getCountry().getId());
        assertNotNull(response.getWidth());
        assertNotNull(response.getHeight());
        assertNotNull(response.getIsDefaultImage());

        // pg=0025&bin=414795&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20111333&back=true&type=V&width=512&height=324
        assertEquals("0025", response.getCards().get(0).getCodProd());
        assertEquals("414795", response.getCards().get(0).getBin());
        assertEquals("V", response.getCards().get(0).getType().getId());
        assertTrue(response.getCards().get(0).getIsBackImage());
        assertEquals("pe", response.getCountry().getId());
        assertEquals("512", response.getWidth());
        assertEquals("324", response.getHeight());
        assertTrue(response.getIsDefaultImage());

        response = mapper.mapIn(input.get(5));
        assertNotNull(response);
        assertNotNull(response.getCards());
        assertEquals(1, response.getCards().size());
        assertNotNull(response.getCards().get(0).getCodProd());
        assertNotNull(response.getCards().get(0).getBin());
        assertNotNull(response.getCards().get(0).getType().getId());
        assertNotNull(response.getCards().get(0).getIssueDate());
        assertNull(response.getCards().get(0).getIsBackImage());
        assertNotNull(response.getCountry().getId());
        assertNotNull(response.getWidth());
        assertNotNull(response.getHeight());
        assertNotNull(response.getIsDefaultImage());

        // pg=0026&bin=414796&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171126&type=V&width=512&height=324
        assertEquals("0026", response.getCards().get(0).getCodProd());
        assertEquals("414796", response.getCards().get(0).getBin());
        assertEquals("V", response.getCards().get(0).getType().getId());
        assertEquals("2017-11-26", response.getCards().get(0).getIssueDate());
        assertEquals("pe", response.getCountry().getId());
        assertEquals("512", response.getWidth());
        assertEquals("324", response.getHeight());
        assertTrue(response.getIsDefaultImage());

        response = mapper.mapIn(input.get(6));
        assertNotNull(response);
        assertNotNull(response.getCards());
        assertEquals(1, response.getCards().size());
        assertNotNull(response.getCards().get(0).getCodProd());
        assertNotNull(response.getCards().get(0).getBin());
        assertNotNull(response.getCards().get(0).getType().getId());
        assertNull(response.getCards().get(0).getIssueDate());
        assertNotNull(response.getCards().get(0).getIsBackImage());
        assertNotNull(response.getCountry().getId());
        assertNotNull(response.getWidth());
        assertNotNull(response.getHeight());
        assertNotNull(response.getIsDefaultImage());

        // pg=0027&bin=414797&default_image=true&v=4&country=co&app_id=com.bbva.wallet&issue_date=20111333&back=true&type=V&width=512&height=1024
        assertEquals("0027", response.getCards().get(0).getCodProd());
        assertEquals("414797", response.getCards().get(0).getBin());
        assertEquals("V", response.getCards().get(0).getType().getId());
        assertTrue(response.getCards().get(0).getIsBackImage());
        assertEquals("co", response.getCountry().getId());
        assertEquals("512", response.getWidth());
        assertEquals("1024", response.getHeight());
        assertTrue(response.getIsDefaultImage());

        response = mapper.mapIn(input.get(7));
        assertNull(response);

        response = mapper.mapIn(input.get(8));
        assertNull(response);
    }

    private String replaceSubstring(String url) {
        String[] segment = url.split("&");
        String[] value = segment[0].split("=");
        return url.replaceFirst(value[1], "");
    }

    @Test
    public void mapInWithoudCodProdPgTest() throws IOException {
        List<HolderSimulation> input = EntityMock.getInstance().fakeCardsHolderSimulation();

        String url = input.get(0).getDetails().getImage().getUrl();
        (input.get(0).getDetails()).getImage().setUrl(replaceSubstring(url));

        AWSImagesRequest response = mapper.mapIn(input.get(0));

        assertNotNull(response);
        assertNotNull(response.getCards());
        assertEquals(1, response.getCards().size());
        assertNull(response.getCards().get(0).getCodProd());
        assertNotNull(response.getCards().get(0).getBin());
        assertNotNull(response.getCards().get(0).getType().getId());
        assertNotNull(response.getCards().get(0).getIssueDate());
        assertNull(response.getCards().get(0).getIsBackImage());
        assertNotNull(response.getCountry().getId());
        assertNotNull(response.getWidth());
        assertNotNull(response.getHeight());
        assertNotNull(response.getIsDefaultImage());

        // pg=&bin=414791&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171123&type=V&width=512&height=324
        assertEquals("414791", response.getCards().get(0).getBin());
        assertEquals("V", response.getCards().get(0).getType().getId());
        assertEquals("2017-11-23", response.getCards().get(0).getIssueDate());
        assertEquals("pe", response.getCountry().getId());
        assertEquals("512", response.getWidth());
        assertEquals("324", response.getHeight());
        assertTrue(response.getIsDefaultImage());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        List<HolderSimulation> input = EntityMock.getInstance().fakeCardsHolderSimulation();
        AWSImagesResponse response = AWSImagesStubs.INSTANCE.buildAWSImagesHolderSimulationResponse();
        AWSImagesResponse responseInicial = AWSImagesStubs.INSTANCE.buildAWSImagesHolderSimulationResponse();

        mapper.mapOut(response, input.get(0));
        assertEquals(10, input.size());

        assertEquals(response.getData().get(0).get(5).getValue(), input.get(0).getDetails().getImage().getUrl());

        List<ModelImage> modelImage = responseInicial.getData().get(1);
        response.setData(new ArrayList<>());
        response.getData().add(modelImage);
        mapper.mapOut(response, input.get(1)); // Not change, because url is null
        assertEquals("pg=0022&bin=414792&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171124&back=true&type=V&width=512&height=324", input.get(1).getDetails().getImage().getUrl());

        modelImage = responseInicial.getData().get(2);
        response.setData(new ArrayList<>());
        response.getData().add(modelImage);
        mapper.mapOut(response, input.get(2));
        assertEquals(response.getData().get(0).get(2).getValue(), input.get(2).getDetails().getImage().getUrl());

        modelImage = responseInicial.getData().get(3);
        response.setData(new ArrayList<>());
        response.getData().add(modelImage);
        mapper.mapOut(response, input.get(3));
        assertEquals(response.getData().get(0).get(3).getValue(), input.get(3).getDetails().getImage().getUrl());

        modelImage = responseInicial.getData().get(4);
        response.setData(new ArrayList<>());
        response.getData().add(modelImage);
        mapper.mapOut(response, input.get(4));
        assertEquals(response.getData().get(0).get(2).getValue(), input.get(4).getDetails().getImage().getUrl());

        modelImage = responseInicial.getData().get(5);
        response.setData(new ArrayList<>());
        response.getData().add(modelImage);
        mapper.mapOut(response, input.get(5));
        assertEquals(response.getData().get(0).get(2).getValue(), input.get(5).getDetails().getImage().getUrl());

        modelImage = responseInicial.getData().get(6);
        response.setData(new ArrayList<>());
        response.getData().add(modelImage);
        mapper.mapOut(response, input.get(6));
        assertEquals(response.getData().get(0).get(2).getValue(), input.get(6).getDetails().getImage().getUrl());

        mapper.mapOut(response, input.get(7));
        assertNull(input.get(7).getDetails().getImage().getUrl());

        mapper.mapOut(response, input.get(8));
        assertNull(input.get(8).getDetails().getImage().getUrl());

        mapper.mapOut(response, input.get(9));
        assertNull(input.get(9).getDetails().getImage());
    }

    @Test
    public void mapOutResponseNullTest() throws IOException {
        List<HolderSimulation> input = EntityMock.getInstance().fakeCardsHolderSimulation();
        mapper.mapOut(null, input.get(0));
        assertEquals(10, input.size());
    }

    @Test
    public void mapOutEmptyTest() {
        HolderSimulation input = new HolderSimulation();
        mapper.mapOut(null, input);
        assertNull(input.getDetails());
    }
}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.business.dto.InputModifyCardActivations;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMENG3;
import com.bbva.pzic.cards.dao.model.mpg3.FormatoMPMS1G3;
import com.bbva.pzic.cards.dao.model.mpg3.mock.FormatoMPMS1G3Mock;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 10/10/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxModifyCardActivationsMapperV0Test {

    private final EntityMock entityMock = EntityMock.getInstance();
    private final FormatoMPMS1G3Mock formatoMPMS1G3Mock = new FormatoMPMS1G3Mock();
    @InjectMocks
    private TxModifyCardActivationsMapperV0 mapper;
    @Mock
    private Translator translator;

    public void enumMapOut() {
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "01")).thenReturn(ON_OFF);
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "02")).thenReturn(ACTIVATION_ID);
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "03")).thenReturn(ECOMMERCE_ACTIVATION);
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "04")).thenReturn(FOREIGN_PURCHASES_ACTIVATION);
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "05")).thenReturn(FOREIGN_CASHWITHDRAWAL_ACTIVATION);
    }

    @Test
    public void mapInFull() {
        final InputModifyCardActivations entity = entityMock.buildDTOInputItemsActivations();
        final FormatoMPMENG3 formatoMPMENG3 = mapper.mapIn(entity);

        assertNotNull(formatoMPMENG3.getInvacta());
        assertNotNull(formatoMPMENG3.getInvactb());
        assertNotNull(formatoMPMENG3.getInvactc());
        assertNotNull(formatoMPMENG3.getInvactd());
        assertNotNull(formatoMPMENG3.getCodacta());
        assertNotNull(formatoMPMENG3.getCodactb());
        assertNotNull(formatoMPMENG3.getCodactc());
        assertNotNull(formatoMPMENG3.getCodactd());

        assertEquals(entity.getCardId(), formatoMPMENG3.getIdetarj());
        assertEquals(entity.getDtoIntActivationPosition1().getActivationId(), formatoMPMENG3.getCodacta());
        assertEquals(entity.getDtoIntActivationPosition2().getActivationId(), formatoMPMENG3.getCodactb());
        assertEquals(entity.getDtoIntActivationPosition3().getActivationId(), formatoMPMENG3.getCodactc());
        assertEquals(entity.getDtoIntActivationPosition4().getActivationId(), formatoMPMENG3.getCodactd());
        assertEquals(entity.getDtoIntActivationPosition5().getActivationId(), formatoMPMENG3.getCodacte());

        assertEquals("S", formatoMPMENG3.getInvacta());
        assertEquals("S", formatoMPMENG3.getInvactb());
        assertEquals("N", formatoMPMENG3.getInvactc());
        assertEquals("N", formatoMPMENG3.getInvactd());
        assertEquals("S", formatoMPMENG3.getInvacte());
    }

    @Test
    public void mapInWithPositionDosEmpty() {
        final InputModifyCardActivations entity = entityMock.buildDTOInputItemsActivations();
        entity.setDtoIntActivationPosition2(new DTOIntActivation());

        final FormatoMPMENG3 formatoMPMENG3 = mapper.mapIn(entity);

        assertNotNull(formatoMPMENG3.getInvacta());
        assertNull(formatoMPMENG3.getInvactb());
        assertNotNull(formatoMPMENG3.getInvactc());
        assertNotNull(formatoMPMENG3.getInvactd());

        assertNotNull(formatoMPMENG3.getCodacta());
        assertNull(formatoMPMENG3.getCodactb());
        assertNotNull(formatoMPMENG3.getCodactc());
        assertNotNull(formatoMPMENG3.getCodactd());

        assertEquals(entity.getCardId(), formatoMPMENG3.getIdetarj());
        assertEquals(entity.getDtoIntActivationPosition1().getActivationId(), formatoMPMENG3.getCodacta());
        assertEquals(entity.getDtoIntActivationPosition3().getActivationId(), formatoMPMENG3.getCodactc());
        assertEquals(entity.getDtoIntActivationPosition4().getActivationId(), formatoMPMENG3.getCodactd());
        assertEquals(entity.getDtoIntActivationPosition5().getActivationId(), formatoMPMENG3.getCodacte());

        assertEquals("S", formatoMPMENG3.getInvacta());
        assertEquals("N", formatoMPMENG3.getInvactc());
        assertEquals("N", formatoMPMENG3.getInvactd());
        assertEquals("S", formatoMPMENG3.getInvacte());
    }

    @Test
    public void mapInWithOut() {
        final FormatoMPMENG3 formatoMPMENG3 = mapper.mapIn(new InputModifyCardActivations());
        assertNotNull(formatoMPMENG3);
        assertNull(formatoMPMENG3.getIdetarj());
        assertNull(formatoMPMENG3.getCodacta());
        assertNull(formatoMPMENG3.getCodactb());
        assertNull(formatoMPMENG3.getCodactd());
        assertNull(formatoMPMENG3.getCodacte());
        assertNull(formatoMPMENG3.getInvacta());
        assertNull(formatoMPMENG3.getInvactb());
        assertNull(formatoMPMENG3.getInvactc());
        assertNull(formatoMPMENG3.getInvactd());
        assertNull(formatoMPMENG3.getInvacte());
    }

    @Test
    public void mapOut() {
        enumMapOut();
        final FormatoMPMS1G3 formatoMPMS1G3 = formatoMPMS1G3Mock.getFormatoMPMS1G3();
        final List<Activation> activationList = mapper.mapOut(formatoMPMS1G3);

        assertNotNull(activationList);

        assertEquals(5, activationList.size());

        assertTrue(activationList.get(0).getIsActive());
        assertEquals(ON_OFF, activationList.get(0).getActivationId());

        assertFalse(activationList.get(1).getIsActive());
        assertEquals(ACTIVATION_ID, activationList.get(1).getActivationId());

        assertTrue(activationList.get(2).getIsActive());
        assertEquals(ECOMMERCE_ACTIVATION, activationList.get(2).getActivationId());

        assertFalse(activationList.get(3).getIsActive());
        assertEquals(FOREIGN_PURCHASES_ACTIVATION, activationList.get(3).getActivationId());

        assertTrue(activationList.get(4).getIsActive());
        assertEquals(FOREIGN_CASHWITHDRAWAL_ACTIVATION, activationList.get(4).getActivationId());
    }

    @Test
    public void mapOutWithOut() {
        enumMapOut();
        final List<Activation> activationList = mapper.mapOut(new FormatoMPMS1G3());
        assertNotNull(activationList);
        assertEquals(0, activationList.size());
    }
}

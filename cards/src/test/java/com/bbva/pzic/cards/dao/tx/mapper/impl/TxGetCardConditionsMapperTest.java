package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputGetCardConditions;
import com.bbva.pzic.cards.canonic.Condition;
import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPFMCE1;
import com.bbva.pzic.cards.dao.model.mpq1.FormatoMPNCCS1;
import com.bbva.pzic.cards.dao.model.mpq1.mock.FormatsMpq1Mock;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.PROPERTY_CONDITIONS_OUTCOMES_OUTCOME_TYPE_ID_KEY_TESTED;
import static com.bbva.pzic.cards.EntityMock.PROPERTY_CONDITIONS_OUTCOMES_OUTCOME_TYPE_ID_VALUE_TESTED;
import static com.bbva.pzic.cards.util.Constants.DEFAULT_TIME;
import static org.mockito.Mockito.when;

/**
 * Created on 22/05/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxGetCardConditionsMapperTest {

    private final EntityMock entityMock = EntityMock.getInstance();
    @InjectMocks
    private TxGetCardConditionsMapper mapper;
    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        when(enumMapper.getEnumValue(
                "conditions.outcomes.outcomeType.id", PROPERTY_CONDITIONS_OUTCOMES_OUTCOME_TYPE_ID_VALUE_TESTED))
                .thenReturn(PROPERTY_CONDITIONS_OUTCOMES_OUTCOME_TYPE_ID_KEY_TESTED);
    }

    @Test
    public void mapInFullTest() {
        InputGetCardConditions input = entityMock.getInputGetConditions();

        FormatoMPFMCE1 format = mapper.mapIn(input);

        Assert.assertNotNull(format);
        Assert.assertNotNull(format.getIdetarj());
        Assert.assertEquals(input.getCardId(), format.getIdetarj());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoMPFMCE1 format = mapper.mapIn(new InputGetCardConditions());
        Assert.assertNotNull(format);
        Assert.assertNull(format.getIdetarj());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoMPNCCS1 format = FormatsMpq1Mock.getInstance().getFormatoMPNCCS1().get(0);
        List<Condition> result = mapper.mapOut(format, new ArrayList<>());
        result = mapper.mapOut(format, result);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.get(0).getConditionId());
        Assert.assertNotNull(result.get(0).getName());
        Assert.assertNotNull(result.get(0).getOutcomes().get(0).getOutcomeType().getId());
        Assert.assertNotNull(result.get(0).getOutcomes().get(0).getOutcomeType().getName());
        Assert.assertNotNull(result.get(0).getOutcomes().get(0).getFeeAmount().getAmount());
        Assert.assertNotNull(result.get(0).getOutcomes().get(0).getFeeAmount().getCurrency());
        Assert.assertNotNull(result.get(0).getOutcomes().get(0).getDueDate());
        Assert.assertNotNull(result.get(0).getPeriod().getStartDate());
        Assert.assertNotNull(result.get(0).getPeriod().getRemainingTime().getUnit());
        Assert.assertNotNull(result.get(0).getPeriod().getRemainingTime().getNumber());
        Assert.assertNotNull(result.get(0).getAccumulatedAmount().getAmount());
        Assert.assertNotNull(result.get(0).getAccumulatedAmount().getCurrency());
        Assert.assertNotNull(result.get(0).getFacts().get(0));
        Assert.assertNotNull(result.get(0).getFacts().get(0).getConditionAmount().getAmount());
        Assert.assertNotNull(result.get(0).getFacts().get(0).getConditionAmount().getCurrency());
        Assert.assertEquals(format.getIdconme(), result.get(0).getConditionId());

        Assert.assertEquals(format.getDsconme(), result.get(0).getName());
        Assert.assertEquals(PROPERTY_CONDITIONS_OUTCOMES_OUTCOME_TYPE_ID_KEY_TESTED, result.get(0).getOutcomes().get(0).getOutcomeType().getId());
        Assert.assertEquals(format.getDscomme(), result.get(0).getOutcomes().get(0).getOutcomeType().getName());
        Assert.assertEquals(format.getImporte(), result.get(0).getOutcomes().get(0).getFeeAmount().getAmount());
        Assert.assertEquals(format.getMonimp(), result.get(0).getOutcomes().get(0).getFeeAmount().getCurrency());
        Assert.assertEquals(FunctionUtils.buildDatetime(format.getFecmem(), DEFAULT_TIME), result.get(0).getOutcomes().get(0).getDueDate());
        Assert.assertEquals(FunctionUtils.buildDatetime(format.getFecinme(), DEFAULT_TIME), result.get(0).getPeriod().getStartDate());
        Assert.assertEquals("MONTHS", result.get(0).getPeriod().getRemainingTime().getUnit());
        Assert.assertEquals(format.getCanmesm(), result.get(0).getPeriod().getRemainingTime().getNumber());
        Assert.assertEquals(format.getImpacum(), result.get(0).getAccumulatedAmount().getAmount());
        Assert.assertEquals(format.getMonimp(), result.get(0).getAccumulatedAmount().getCurrency());
        Assert.assertEquals(format.getImpmeta(), result.get(0).getFacts().get(0).getConditionAmount().getAmount());
        Assert.assertEquals(format.getMonimp(), result.get(0).getFacts().get(0).getConditionAmount().getCurrency());

        // Second Item
        Assert.assertNotNull(result.get(1).getConditionId());
        Assert.assertNotNull(result.get(1).getName());
        Assert.assertNotNull(result.get(1).getOutcomes().get(0).getOutcomeType().getId());
        Assert.assertNotNull(result.get(1).getOutcomes().get(0).getOutcomeType().getName());
        Assert.assertNotNull(result.get(1).getOutcomes().get(0).getFeeAmount().getAmount());
        Assert.assertNotNull(result.get(1).getOutcomes().get(0).getFeeAmount().getCurrency());
        Assert.assertNotNull(result.get(1).getOutcomes().get(0).getDueDate());
        Assert.assertNotNull(result.get(1).getPeriod().getStartDate());
        Assert.assertNotNull(result.get(1).getPeriod().getRemainingTime().getUnit());
        Assert.assertNotNull(result.get(1).getPeriod().getRemainingTime().getNumber());
        Assert.assertNotNull(result.get(1).getAccumulatedAmount().getAmount());
        Assert.assertNotNull(result.get(1).getAccumulatedAmount().getCurrency());
        Assert.assertNotNull(result.get(1).getFacts().get(0));
        Assert.assertNotNull(result.get(1).getFacts().get(0).getConditionAmount().getAmount());
        Assert.assertNotNull(result.get(1).getFacts().get(0).getConditionAmount().getCurrency());
        Assert.assertEquals(format.getIdconme(), result.get(1).getConditionId());

        Assert.assertEquals(format.getDsconme(), result.get(1).getName());
        Assert.assertEquals(PROPERTY_CONDITIONS_OUTCOMES_OUTCOME_TYPE_ID_KEY_TESTED, result.get(1).getOutcomes().get(0).getOutcomeType().getId());
        Assert.assertEquals(format.getDscomme(), result.get(1).getOutcomes().get(0).getOutcomeType().getName());
        Assert.assertEquals(format.getImporte(), result.get(1).getOutcomes().get(0).getFeeAmount().getAmount());
        Assert.assertEquals(format.getMonimp(), result.get(1).getOutcomes().get(0).getFeeAmount().getCurrency());
        Assert.assertEquals(FunctionUtils.buildDatetime(format.getFecmem(), DEFAULT_TIME), result.get(1).getOutcomes().get(0).getDueDate());
        Assert.assertEquals(FunctionUtils.buildDatetime(format.getFecinme(), DEFAULT_TIME), result.get(1).getPeriod().getStartDate());
        Assert.assertEquals("MONTHS", result.get(1).getPeriod().getRemainingTime().getUnit());
        Assert.assertEquals(format.getCanmesm(), result.get(1).getPeriod().getRemainingTime().getNumber());
        Assert.assertEquals(format.getImpacum(), result.get(1).getAccumulatedAmount().getAmount());
        Assert.assertEquals(format.getMonimp(), result.get(1).getAccumulatedAmount().getCurrency());
        Assert.assertEquals(format.getImpmeta(), result.get(1).getFacts().get(0).getConditionAmount().getAmount());
        Assert.assertEquals(format.getMonimp(), result.get(1).getFacts().get(0).getConditionAmount().getCurrency());
    }

    @Test
    public void mapOutEmptyTest() throws IOException {
        FormatoMPNCCS1 format = FormatsMpq1Mock.getInstance().getFormatoMPNCCS1().get(1);
        List<Condition> result = mapper.mapOut(format, new ArrayList<>());
        result = mapper.mapOut(format, result);

        Assert.assertNotNull(result);
        Assert.assertNull(result.get(0).getConditionId());
        Assert.assertNull(result.get(0).getName());
        Assert.assertNull(result.get(0).getOutcomes().get(0).getOutcomeType());
        Assert.assertNotNull(result.get(0).getOutcomes().get(0).getFeeAmount());
        Assert.assertEquals(BigDecimal.ZERO, result.get(0).getOutcomes().get(0).getFeeAmount().getAmount());
        Assert.assertNull(result.get(0).getOutcomes().get(0).getDueDate());
        Assert.assertNull(result.get(0).getPeriod());
        Assert.assertNotNull(result.get(0).getAccumulatedAmount());
        Assert.assertEquals(BigDecimal.ZERO, result.get(0).getAccumulatedAmount().getAmount());
        Assert.assertNotNull(result.get(0).getFacts().get(0));
        Assert.assertEquals(BigDecimal.ZERO, result.get(0).getFacts().get(0).getConditionAmount().getAmount());

        // Second Item
        Assert.assertNotNull(result);
        Assert.assertNull(result.get(1).getConditionId());
        Assert.assertNull(result.get(1).getName());
        Assert.assertNull(result.get(1).getOutcomes().get(0).getOutcomeType());
        Assert.assertNotNull(result.get(1).getOutcomes().get(0).getFeeAmount());
        Assert.assertEquals(BigDecimal.ZERO, result.get(1).getOutcomes().get(0).getFeeAmount().getAmount());
        Assert.assertNull(result.get(1).getOutcomes().get(0).getDueDate());
        Assert.assertNull(result.get(1).getPeriod());
        Assert.assertNotNull(result.get(1).getAccumulatedAmount());
        Assert.assertEquals(BigDecimal.ZERO, result.get(1).getAccumulatedAmount().getAmount());
        Assert.assertNotNull(result.get(1).getFacts().get(0));
        Assert.assertEquals(BigDecimal.ZERO, result.get(1).getFacts().get(0).getConditionAmount().getAmount());
    }
}

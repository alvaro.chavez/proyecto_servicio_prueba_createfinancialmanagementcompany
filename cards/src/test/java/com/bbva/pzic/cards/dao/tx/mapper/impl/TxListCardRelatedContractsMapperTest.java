package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntRelatedContractsSearchCriteria;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMENL5;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMS1L5;
import com.bbva.pzic.cards.dao.tx.mapper.ITxListCardRelatedContractsMapper;
import com.bbva.pzic.cards.facade.v0.dto.RelatedContracts;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TxListCardRelatedContractsMapperTest {

    @InjectMocks
    private ITxListCardRelatedContractsMapper mapper = new TxListCardRelatedContractsMapper();

    @Mock
    private Translator translator;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void setUp() {
        Mockito.when(cypherTool.encrypt(RELATED_CONTRACT_ID, AbstractCypherTool.RELATED_CONTRACT_ID)).thenReturn(RELATED_CONTRACT_ENCRYPT_ID);
        Mockito.when(cypherTool.mask(NUMBER, AbstractCypherTool.CARD_NUMBER)).thenReturn(MASKED_NUMBER);
        Mockito.when(translator.translateBackendEnumValue("cards.numberType.id", NUMBER_TYPE_ID_1)).thenReturn(NUMBER_TYPE_ID_PAN);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.product.id", PRODUCT_ID_50)).thenReturn(PRODUCT_ID_CARDS);
    }

    @Test
    public void testMapInput() {
        final DTOIntRelatedContractsSearchCriteria dtoIntRelatedContractsSearchCriteria = new DTOIntRelatedContractsSearchCriteria();
        dtoIntRelatedContractsSearchCriteria.setCardId(CARD_ENCRYPT_ID);
        final FormatoMPMENL5 formatoMPMENL5 = mapper.mapInput(dtoIntRelatedContractsSearchCriteria);

        assertNotNull(formatoMPMENL5);
        assertNotNull(formatoMPMENL5.getIdetarj());
        assertEquals(dtoIntRelatedContractsSearchCriteria.getCardId(), formatoMPMENL5.getIdetarj());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoMPMENL5 formatoMPMENL5 = mapper.mapInput(new DTOIntRelatedContractsSearchCriteria());

        assertNotNull(formatoMPMENL5);
        assertNull(formatoMPMENL5.getIdetarj());
    }

    @Test
    public void testMapOutput() throws IOException {
        FormatoMPMS1L5 formatoMPMS1L5 = entityMock.buildFormatoMPMS1L5();

        List<RelatedContracts> result = mapper.mapOutput(formatoMPMS1L5, new ArrayList<>());
        result = mapper.mapOutput(formatoMPMS1L5, result);

        assertNotNull(result.get(0));
        assertNotNull(result.get(0).getRelatedContractId());
        assertEquals(formatoMPMS1L5.getIdrela(), result.get(0).getRelatedContractId());
        assertNotNull(result.get(0).getContractId());
        assertEquals(EntityMock.RELATED_CONTRACT_ENCRYPT_ID, result.get(0).getContractId());
        assertNotNull(result.get(0).getNumber());
        assertEquals(MASKED_NUMBER, result.get(0).getNumber());
        assertNotNull(result.get(0).getNumberType());
        assertNotNull(result.get(0).getNumberType().getId());
        assertEquals(EntityMock.NUMBER_TYPE_ID_PAN, result.get(0).getNumberType().getId());
        assertNotNull(result.get(0).getNumberType().getName());
        assertEquals(formatoMPMS1L5.getTnumdes(), result.get(0).getNumberType().getName());
        assertNotNull(result.get(0).getProduct());
        assertNotNull(result.get(0).getProduct().getId());
        assertEquals(EntityMock.PRODUCT_ID_CARDS, result.get(0).getProduct().getId());
        assertNotNull(result.get(0).getProduct().getName());
        assertEquals(formatoMPMS1L5.getProddes(), result.get(0).getProduct().getName());

        //Second Item
        assertNotNull(result.get(1));
        assertNotNull(result.get(1).getRelatedContractId());
        assertEquals(formatoMPMS1L5.getIdrela(), result.get(1).getRelatedContractId());
        assertNotNull(result.get(1).getContractId());
        assertEquals(EntityMock.RELATED_CONTRACT_ENCRYPT_ID, result.get(0).getContractId());
        assertNotNull(result.get(1).getNumber());
        assertEquals(MASKED_NUMBER, result.get(1).getNumber());
        assertNotNull(result.get(1).getNumberType());
        assertNotNull(result.get(1).getNumberType().getId());
        assertEquals(EntityMock.NUMBER_TYPE_ID_PAN, result.get(1).getNumberType().getId());
        assertNotNull(result.get(1).getNumberType().getName());
        assertEquals(formatoMPMS1L5.getTnumdes(), result.get(1).getNumberType().getName());
        assertNotNull(result.get(1).getProduct());
        assertNotNull(result.get(1).getProduct().getId());
        assertEquals(EntityMock.PRODUCT_ID_CARDS, result.get(1).getProduct().getId());
        assertNotNull(result.get(1).getProduct().getName());
        assertEquals(formatoMPMS1L5.getProddes(), result.get(1).getProduct().getName());
    }
}

package com.bbva.pzic.cards.dao.model.mpgl;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test de la transacci&oacute;n <code>MPGL</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionMpgl {

    private static final Log LOG = LogFactory.getLog(TestTransaccionMpgl.class);

    @InjectMocks
    private TransaccionMpgl transaccion;

    @Spy
    private ServicioTransacciones servicioTransacciones = Mockito.mock(ServicioTransacciones.class);

    @Test
    public void test() {
        PeticionTransaccionMpgl peticion = new PeticionTransaccionMpgl();
        RespuestaTransaccionMpgl respuesta = transaccion.invocar(peticion);

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionMpgl.class, RespuestaTransaccionMpgl.class,
                peticion)).thenReturn(respuesta);

        RespuestaTransaccionMpgl result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}
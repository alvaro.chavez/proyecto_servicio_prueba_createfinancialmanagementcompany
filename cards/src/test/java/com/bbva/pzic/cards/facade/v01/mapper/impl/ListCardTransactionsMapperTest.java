package com.bbva.pzic.cards.facade.v01.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v01.mapper.IListCardTransactionsMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class ListCardTransactionsMapperTest {
    @InjectMocks
    private IListCardTransactionsMapper listTransactionsMapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Before
    public void setUp() {
        listTransactionsMapper = new ListCardTransactionsMapper();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testMapInput() {
        final String cardId = "##4232$%6778";
        final String fromOperationDate = "2015-12-01";
        final String toOperationDate = "2015-12-31";
        final String paginationKey = "qwertyuiop1234";
        final Long pageSize = 123L;

        Mockito.when(cypherTool.decrypt("##4232$%6778", AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS1_V01)).thenReturn("12345678");
        final DTOInputListCardTransactions dtoInputListCardTransactions =
                listTransactionsMapper.mapInput(true, cardId, fromOperationDate, toOperationDate, paginationKey, pageSize, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS1_V01);
        assertNotNull(dtoInputListCardTransactions);

        assertNotNull(dtoInputListCardTransactions.getCardId());
        assertEquals("12345678", dtoInputListCardTransactions.getCardId());

        assertNotNull(dtoInputListCardTransactions.getFromOperationDate());
        assertEquals(fromOperationDate, dtoInputListCardTransactions.getFromOperationDate());

        assertNotNull(dtoInputListCardTransactions.getToOperationDate());
        assertEquals(toOperationDate, dtoInputListCardTransactions.getToOperationDate());

        assertNotNull(dtoInputListCardTransactions.getPaginationKey());
        assertEquals(paginationKey, dtoInputListCardTransactions.getPaginationKey());

        assertNotNull(dtoInputListCardTransactions.getPageSize());
        assertEquals(pageSize, dtoInputListCardTransactions.getPageSize());

        assertTrue(dtoInputListCardTransactions.isHaveFinancingType());
    }

}
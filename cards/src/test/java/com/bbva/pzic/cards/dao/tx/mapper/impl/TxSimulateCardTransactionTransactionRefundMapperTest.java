package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputSimulateCardTransactionTransactionRefund;
import com.bbva.pzic.cards.dao.model.mp6i.FormatoMPMEN6I;
import com.bbva.pzic.cards.dao.model.mp6i.FormatoMPMS16I;
import com.bbva.pzic.cards.dao.model.mp6i.mock.FormatsMp6iStubs;
import com.bbva.pzic.cards.facade.v1.dto.SimulateTransactionRefund;
import com.bbva.pzic.cards.util.Enums;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 17/09/2020.
 *
 * @author Entelgy.
 */
@RunWith(MockitoJUnitRunner.class)
public class TxSimulateCardTransactionTransactionRefundMapperTest {

    @InjectMocks
    private TxSimulateCardTransactionTransactionRefundMapper mapper;

    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() throws IOException {
        InputSimulateCardTransactionTransactionRefund input = EntityMock.getInstance().buildInputSimulateCardTransactionTransactionRefund();
        FormatoMPMEN6I result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertNotNull(result.getIdentif());
        assertNotNull(result.getMcindop());

        assertEquals(input.getCardId(), result.getIdetarj());
        assertEquals(input.getTransactionId(), result.getIdentif());
        assertEquals(input.getRefundTypeId(), result.getMcindop());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoMPMEN6I result = mapper.mapIn(new InputSimulateCardTransactionTransactionRefund());

        assertNotNull(result);
        assertNull(result.getIdetarj());
        assertNull(result.getIdentif());
        assertNull(result.getMcindop());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoMPMS16I input = FormatsMp6iStubs.getInstance().getFormatoMPMS16I();
        when(translator.translateBackendEnumValueStrictly(Enums.TRANSACTION_REFUND_REFUND_TYPE_ID_KEY, input.getMcindop())).thenReturn(EntityMock.REFUND_TYPE_ID_VALUE);
        when(translator.translateBackendEnumValueStrictly(Enums.TRANSACTION_REFUND_STATUS_ID, input.getCodope())).thenReturn(EntityMock.PROPOSAL_STATUS_CREATED);
        SimulateTransactionRefund result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getRefundType());
        assertNotNull(result.getRefundType().getId());
        assertNotNull(result.getRefundType().getName());
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getId());
        assertNotNull(result.getTransaction());
        assertNotNull(result.getTransaction().getId());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());

        assertEquals(EntityMock.REFUND_TYPE_ID_VALUE, result.getRefundType().getId());
        assertEquals(input.getMcindes(), result.getRefundType().getName());
        assertEquals(input.getIdetarj(), result.getCard().getId());
        assertEquals(input.getIdentif(), result.getTransaction().getId());
        assertEquals(EntityMock.PROPOSAL_STATUS_CREATED, result.getStatus().getId());
        assertEquals(input.getDesoper(), result.getStatus().getDescription());
    }

    @Test
    public void mapOutEmptyTest() {
        SimulateTransactionRefund result = mapper.mapOut(new FormatoMPMS16I());

        assertNotNull(result);
        assertNull(result.getRefundType());
        assertNull(result.getCard());
        assertNull(result.getTransaction());
        assertNull(result.getStatus());
    }
}
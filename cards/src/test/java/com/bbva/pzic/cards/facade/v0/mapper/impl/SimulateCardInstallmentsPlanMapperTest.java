package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanSimulation;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.CARD_ID_ENCRYPTED;

@RunWith(MockitoJUnitRunner.class)
public class SimulateCardInstallmentsPlanMapperTest {

    @InjectMocks
    private SimulateCardInstallmentsPlanMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock mock = EntityMock.getInstance();

    private void mapInDecrypt() {
        Mockito.when(cypherTool.decrypt("##4232$%6778", AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_SIMULATE_CARD_INSTALLMENTS_PLAN)).thenReturn("12345678");
    }

    @Test
    public void mapInTestFull() {
        mapInDecrypt();
        InstallmentsPlanSimulation installmentsPlanSimulation = mock.getInstallmentsPlanSimulation();
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlanSimulation);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getAmount());
        Assert.assertNotNull(result.getCurrency());
        Assert.assertNotNull(result.getTermsNumber());
        Assert.assertNotNull(result.getTransactionId());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlanSimulation.getCapital().getAmount(), result.getAmount());
        Assert.assertEquals(installmentsPlanSimulation.getCapital().getCurrency(), result.getCurrency());
        Assert.assertEquals(installmentsPlanSimulation.getTerms().getNumber(), result.getTermsNumber());
        Assert.assertEquals(installmentsPlanSimulation.getTransactionId(), result.getTransactionId());
    }

    @Test
    public void mapInTestWithoutAmount() {
        mapInDecrypt();
        InstallmentsPlanSimulation installmentsPlanSimulation = mock.getInstallmentsPlanSimulation();
        installmentsPlanSimulation.getCapital().setAmount(null);
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlanSimulation);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNull(result.getAmount());
        Assert.assertNotNull(result.getCurrency());
        Assert.assertNotNull(result.getTermsNumber());
        Assert.assertNotNull(result.getTransactionId());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlanSimulation.getCapital().getCurrency(), result.getCurrency());
        Assert.assertEquals(installmentsPlanSimulation.getTerms().getNumber(), result.getTermsNumber());
        Assert.assertEquals(installmentsPlanSimulation.getTransactionId(), result.getTransactionId());
    }

    @Test
    public void mapInTestWithoutCurrency() {
        mapInDecrypt();
        InstallmentsPlanSimulation installmentsPlanSimulation = mock.getInstallmentsPlanSimulation();
        installmentsPlanSimulation.getCapital().setCurrency(null);
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlanSimulation);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getAmount());
        Assert.assertNull(result.getCurrency());
        Assert.assertNotNull(result.getTermsNumber());
        Assert.assertNotNull(result.getTransactionId());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlanSimulation.getCapital().getAmount(), result.getAmount());
        Assert.assertEquals(installmentsPlanSimulation.getTerms().getNumber(), result.getTermsNumber());
        Assert.assertEquals(installmentsPlanSimulation.getTransactionId(), result.getTransactionId());
    }

    @Test
    public void mapInTestWithoutTermsTotalNumber() {
        mapInDecrypt();
        InstallmentsPlanSimulation installmentsPlanSimulation = mock.getInstallmentsPlanSimulation();
        installmentsPlanSimulation.getTerms().setNumber(null);
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlanSimulation);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getAmount());
        Assert.assertNotNull(result.getCurrency());
        Assert.assertNull(result.getTermsNumber());
        Assert.assertNotNull(result.getTransactionId());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlanSimulation.getCapital().getAmount(), result.getAmount());
        Assert.assertEquals(installmentsPlanSimulation.getCapital().getCurrency(), result.getCurrency());
        Assert.assertEquals(installmentsPlanSimulation.getTransactionId(), result.getTransactionId());
    }

    @Test
    public void mapInTestWithoutTransactionId() {
        mapInDecrypt();
        InstallmentsPlanSimulation installmentsPlanSimulation = mock.getInstallmentsPlanSimulation();
        installmentsPlanSimulation.setTransactionId(null);
        DTOIntCardInstallmentsPlan result = mapper.mapIn(CARD_ID_ENCRYPTED, installmentsPlanSimulation);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getAmount());
        Assert.assertNotNull(result.getCurrency());
        Assert.assertNotNull(result.getTermsNumber());
        Assert.assertNull(result.getTransactionId());

        Assert.assertEquals("12345678", result.getCardId());
        Assert.assertEquals(installmentsPlanSimulation.getCapital().getAmount(), result.getAmount());
        Assert.assertEquals(installmentsPlanSimulation.getCapital().getCurrency(), result.getCurrency());
        Assert.assertEquals(installmentsPlanSimulation.getTerms().getNumber(), result.getTermsNumber());
    }
}

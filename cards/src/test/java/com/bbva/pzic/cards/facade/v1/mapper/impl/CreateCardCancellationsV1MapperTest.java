package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancellations;
import com.bbva.pzic.cards.facade.v1.dto.Cancellation;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.*;
import static com.bbva.pzic.cards.util.Enums.CARD_CANCELLATIONS_REASONID;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CreateCardCancellationsV1MapperTest {

    @InjectMocks
    private CreateCardCancellationsV1Mapper mapper;

    @Mock
    private Translator translator;


    @Before
    public void init() {
        when(translator.translateFrontendEnumValueStrictly(
                CARD_CANCELLATIONS_REASONID, CARD_CANCELLATIONS_REASONID_FRONTEND)).thenReturn(CARD_CANCELLATIONS_REASONID_BACKEND);
    }

    @Test
    public void mapInFullTest() throws IOException {
        Cancellation input = EntityMock.getInstance().getCancellationV1Mock();

        InputCreateCardCancellations result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(CARD_CANCELLATIONS_REASONID_BACKEND, result.getReason().getId());
    }

    @Test
    public void mapInWithoutReasonTest() throws IOException {
        Cancellation input = EntityMock.getInstance().getCancellationV1Mock();
        input.setReason(null);

        InputCreateCardCancellations result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNull(result.getReason());

        assertEquals(CARD_ID, result.getCardId());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<Cancellation> result = mapper.mapOut(new Cancellation());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<Cancellation> result = mapper.mapOut(null);

        assertNull(result);
    }
}

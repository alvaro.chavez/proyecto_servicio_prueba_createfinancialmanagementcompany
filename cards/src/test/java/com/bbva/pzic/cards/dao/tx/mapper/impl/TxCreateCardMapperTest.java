package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.CardData;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPME0W1;
import com.bbva.pzic.cards.dao.model.mpw1.FormatoMPMS1W1;
import com.bbva.pzic.cards.dao.model.mpw1.mock.FormatsMpw1Mock;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardMapper;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.bbva.pzic.cards.EntityMock.*;
import static com.bbva.pzic.cards.util.Constants.DEFAULT_TIME;

public class TxCreateCardMapperTest {

    @InjectMocks
    private ITxCreateCardMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock entityMock = EntityMock.getInstance();

    private FormatsMpw1Mock formatsMpw1Mock;

    @Before
    public void setUp() {
        mapper = new TxCreateCardMapper();
        formatsMpw1Mock = new FormatsMpw1Mock();
        MockitoAnnotations.initMocks(this);
    }

    public void mapOutEnumMapper() {
        Mockito.when(enumMapper.getEnumValue("cards.numberType.id", "1")).thenReturn(CARD_TYPE_ID_KEY_TESTED);
        Mockito.when(enumMapper.getEnumValue("cards.cardType.id", "D")).thenReturn(CARD_CARDTYPE_ID_KEY_TESTED);
        Mockito.when(enumMapper.getEnumValue("cards.brandAssociation.id", "V")).thenReturn(CARD_BRAND_ASSOCIATION_ID_KEY_TESTED);
        Mockito.when(enumMapper.getEnumValue("cards.physicalSupport.id", "S")).thenReturn(PHYSICAL_SUPPORT_ID);
        Mockito.when(enumMapper.getEnumValue("cards.status.id", "I")).thenReturn(CARD_STATUS_KEY_TESTED);
        Mockito.when(enumMapper.getEnumValue("cards.relatedContracts.relatedContract.numberType.id", "1")).thenReturn(CARD_RELATED_CONTRACT_KEY_TESTED);
        Mockito.when(cypherTool.encrypt("4940 1970 8307 0361", AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD)).thenReturn(CARD_ENCRYPT_NUMTARJ_KEY_TESTED);
        Mockito.when(cypherTool.encrypt("ES90 0182 1642 0302 0153 5555", AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD)).thenReturn(CARD_ENCRYPT_NUCOREL_KEY_TESTED);
    }

    @Test
    public void testMapInFull() throws IOException {
        DTOIntCard dtoIntCard = entityMock.getInputCreateCard();
        FormatoMPME0W1 result = mapper.mapIn(dtoIntCard);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getSopfisi());
        Assert.assertNotNull(result.getTiprodf());
        Assert.assertNotNull(result.getNucorel());

        Assert.assertEquals(dtoIntCard.getPhysicalSupportId(), result.getSopfisi());
        Assert.assertEquals(dtoIntCard.getCardTypeId(), result.getTiprodf());
        Assert.assertEquals(dtoIntCard.getRelatedContractId(), result.getNucorel());
    }

    @Test
    public void testMapInWithoutCardTypeId() throws IOException {
        DTOIntCard dtoIntCard = entityMock.getInputCreateCard();
        dtoIntCard.setCardTypeId(null);
        FormatoMPME0W1 result = mapper.mapIn(dtoIntCard);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getSopfisi());
        Assert.assertNull(result.getTiprodf());
        Assert.assertNotNull(result.getNucorel());

        Assert.assertEquals(dtoIntCard.getPhysicalSupportId(), result.getSopfisi());
        Assert.assertEquals(dtoIntCard.getRelatedContractId(), result.getNucorel());
    }

    @Test
    public void testMapInWithoutPhysicalSupportId() throws IOException {
        DTOIntCard dtoIntCard = entityMock.getInputCreateCard();
        dtoIntCard.setPhysicalSupportId(null);
        FormatoMPME0W1 result = mapper.mapIn(dtoIntCard);

        Assert.assertNotNull(result);
        Assert.assertNull(result.getSopfisi());
        Assert.assertNotNull(result.getTiprodf());
        Assert.assertNotNull(result.getNucorel());

        Assert.assertEquals(dtoIntCard.getPhysicalSupportId(), result.getSopfisi());
        Assert.assertEquals(dtoIntCard.getRelatedContractId(), result.getNucorel());
    }

    @Test
    public void testMapInWithoutRelatedContractId() throws IOException {
        DTOIntCard dtoIntCard = entityMock.getInputCreateCard();
        dtoIntCard.setRelatedContractId(null);
        FormatoMPME0W1 result = mapper.mapIn(dtoIntCard);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getSopfisi());
        Assert.assertNotNull(result.getTiprodf());
        Assert.assertNull(result.getNucorel());

        Assert.assertEquals(dtoIntCard.getPhysicalSupportId(), result.getSopfisi());
        Assert.assertEquals(dtoIntCard.getCardTypeId(), result.getTiprodf());
    }

    @Test
    public void testMapOutFull() throws ParseException {
        mapOutEnumMapper();
        FormatoMPMS1W1 formato = formatsMpw1Mock.getFormatoMPMS1W1();
        CardData cardData = mapper.mapOut(formato);

        Assert.assertNotNull(cardData);
        Assert.assertNotNull(cardData.getData());
        Assert.assertNotNull(cardData.getData().getCardId());
        Assert.assertNotNull(cardData.getData().getPhysicalSupport());
        Assert.assertNotNull(cardData.getData().getCardType());
        Assert.assertNotNull(cardData.getData().getStatus());
        Assert.assertNotNull(cardData.getData().getBrandAssociation());
        Assert.assertNotNull(cardData.getData().getCurrencies());
        Assert.assertNotNull(cardData.getData().getNumberType());
        Assert.assertNotNull(cardData.getData().getGrantedCredits());
        Assert.assertNotNull(cardData.getData().getRelatedContracts());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date expirationDate = sdf.parse("2016-07-01");
        Date openingDate = sdf.parse("2017-02-08");

        Assert.assertEquals(formato.getNumtarj(), cardData.getData().getNumber());
        Assert.assertEquals(CARD_ENCRYPT_NUMTARJ_KEY_TESTED, cardData.getData().getCardId());
        Assert.assertEquals(CARD_TYPE_ID_KEY_TESTED, cardData.getData().getNumberType().getId());
        Assert.assertEquals(formato.getDstiptj(), cardData.getData().getNumberType().getName());
        Assert.assertEquals(CARD_CARDTYPE_ID_KEY_TESTED, cardData.getData().getCardType().getId());
        Assert.assertEquals(formato.getDsprodf(), cardData.getData().getCardType().getName());
        Assert.assertEquals(CARD_BRAND_ASSOCIATION_ID_KEY_TESTED, cardData.getData().getBrandAssociation().getId());
        Assert.assertEquals(formato.getNomatar(), cardData.getData().getBrandAssociation().getName());
        Assert.assertEquals(PHYSICAL_SUPPORT_ID, cardData.getData().getPhysicalSupport().getId());
        Assert.assertEquals(formato.getDsopfis(), cardData.getData().getPhysicalSupport().getName());
        Assert.assertEquals(expirationDate, cardData.getData().getExpirationDate());
        Assert.assertEquals(formato.getNombcli(), cardData.getData().getHolderName());
        Assert.assertEquals(formato.getMonpri(), cardData.getData().getCurrencies().get(0).getCurrency());
        Assert.assertTrue(cardData.getData().getCurrencies().get(0).getIsMajor());
        Assert.assertEquals(formato.getMonsec(), cardData.getData().getCurrencies().get(1).getCurrency());
        Assert.assertFalse(cardData.getData().getCurrencies().get(1).getIsMajor());
        Assert.assertEquals(formato.getIlimcre(), cardData.getData().getGrantedCredits().get(0).getAmount());
        Assert.assertEquals(formato.getDlimcre(), cardData.getData().getGrantedCredits().get(0).getCurrency());
        Assert.assertEquals(CARD_STATUS_KEY_TESTED, cardData.getData().getStatus().getId());
        Assert.assertEquals(formato.getDestarj(), cardData.getData().getStatus().getName());
        Assert.assertEquals(FunctionUtils.buildDatetime(openingDate, DEFAULT_TIME), cardData.getData().getOpeningDate());
        Assert.assertEquals(formato.getIdcorel(), cardData.getData().getRelatedContracts().get(0).getRelatedContractId());
        Assert.assertEquals(formato.getNucorel(), cardData.getData().getRelatedContracts().get(0).getNumber());
        Assert.assertEquals(CARD_ENCRYPT_NUCOREL_KEY_TESTED, cardData.getData().getRelatedContracts().get(0).getContractId());
        Assert.assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, cardData.getData().getRelatedContracts().get(0).getNumberType().getId());
        Assert.assertEquals(formato.getDetcore(), cardData.getData().getRelatedContracts().get(0).getNumberType().getName());
    }

    @Test
    public void mapOutWithoutFormatInitializedTest() {
        CardData cardData = mapper.mapOut(new FormatoMPMS1W1());
        Assert.assertNotNull(cardData);
        Assert.assertNotNull(cardData.getData());
        Assert.assertNull(cardData.getData().getNumber());
        Assert.assertNull(cardData.getData().getCardId());
        Assert.assertNull(cardData.getData().getNumberType().getId());
        Assert.assertNull(cardData.getData().getNumberType().getName());
        Assert.assertNull(cardData.getData().getPhysicalSupport().getId());
        Assert.assertNull(cardData.getData().getPhysicalSupport().getName());
        Assert.assertNull(cardData.getData().getCurrencies().get(0).getCurrency());
        Assert.assertNull(cardData.getData().getCurrencies().get(1).getCurrency());
        Assert.assertNull(cardData.getData().getGrantedCredits().get(0).getCurrency());
        Assert.assertNull(cardData.getData().getGrantedCredits().get(0).getAmount());
        Assert.assertNull(cardData.getData().getStatus().getId());
        Assert.assertNull(cardData.getData().getStatus().getName());
        Assert.assertNull(cardData.getData().getOpeningDate());
        Assert.assertNull(cardData.getData().getExpirationDate());
        Assert.assertNull(cardData.getData().getBrandAssociation().getId());
        Assert.assertNull(cardData.getData().getBrandAssociation().getName());
        Assert.assertNull(cardData.getData().getRelatedContracts().get(0).getContractId());
        Assert.assertNull(cardData.getData().getRelatedContracts().get(0).getNumber());
        Assert.assertNull(cardData.getData().getRelatedContracts().get(0).getRelatedContractId());
        Assert.assertNull(cardData.getData().getRelatedContracts().get(0).getNumberType().getId());
        Assert.assertNull(cardData.getData().getRelatedContracts().get(0).getNumberType().getName());
        Assert.assertNull(cardData.getData().getHolderName());
        Assert.assertNull(cardData.getData().getCardType().getId());
        Assert.assertNull(cardData.getData().getCardType().getName());
    }

    @Test
    public void mapOutWithNullFormatTest() {
        CardData cardData = mapper.mapOut(null);
        Assert.assertNull(cardData);
    }
}
package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPME0W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS1W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS2W2;
import com.bbva.pzic.cards.dao.model.mpw2.FormatoMPMS3W2;
import com.bbva.pzic.cards.dao.model.mpw2.mock.FormatsMpw2Stubs;
import com.bbva.pzic.cards.facade.v1.dto.CardPost;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import com.bbva.pzic.utilTest.RandomGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;

import static com.bbva.pzic.cards.util.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxCreateCardMapperV1Test {

    @InjectMocks
    private TxCreateCardMapperV1 mapper;
    @Mock
    private Translator translator;

    @Test
    public void mapInFullServiceTypeId004Test() throws IOException {
        EntityMock stubs = EntityMock.getInstance();
        DTOIntCard input = stubs.buildDTOIntCard();
        input.getDeliveries().get(0).setDestination(null);
        input.getDeliveries().get(0).setAddress(null);
        input.getDeliveries().get(1).setDestination(null);
        input.getDeliveries().get(1).setAddress(null);
        input.getDeliveries().get(2).setDestination(null);
        input.getDeliveries().get(2).setAddress(null);

        FormatoMPME0W2 result = mapper.mapIn(input);

        commonInputAsserts(input, result);

        assertNotNull(result.getNatcon1());
        assertNotNull(result.getIdcone1());
        assertNull(result.getTipdes1());
        assertNull(result.getIdiren1());
        assertNull(result.getCodofe1());
        assertNotNull(result.getNatcon2());
        assertNotNull(result.getIdcone2());
        assertNull(result.getTipdes2());
        assertNull(result.getIdiren2());
        assertNull(result.getCodofe2());
        assertNotNull(result.getNatcon3());
        assertNotNull(result.getIdcone3());
        assertNull(result.getTipdes3());
        assertNull(result.getIdiren3());
        assertNull(result.getCodofe3());

        assertEquals(input.getDeliveries().get(0).getContact().getContactType(), result.getNatcon1());
        assertEquals(input.getDeliveries().get(0).getContact().getId(), result.getIdcone1());
        assertEquals(input.getDeliveries().get(1).getContact().getContactType(), result.getNatcon2());
        assertEquals(input.getDeliveries().get(1).getContact().getId(), result.getIdcone2());
        assertEquals(input.getDeliveries().get(2).getContact().getContactType(), result.getNatcon3());
        assertEquals(input.getDeliveries().get(2).getContact().getId(), result.getIdcone3());
    }

    @Test
    public void mapInFullServiceTypeIdOtherTest() throws IOException {
        EntityMock stubs = EntityMock.getInstance();
        DTOIntCard input = stubs.buildDTOIntCard();
        input.getDeliveries().get(0).setServiceTypeId("001");
        input.getDeliveries().get(0).setContact(null);
        input.getDeliveries().get(1).setServiceTypeId("002");
        input.getDeliveries().get(1).setContact(null);
        input.getDeliveries().get(2).setServiceTypeId("003");
        input.getDeliveries().get(2).setContact(null);
        input.getDeliveries().get(2).setAddress(null);

        FormatoMPME0W2 result = mapper.mapIn(input);

        commonInputAsserts(input, result);

        // 0 - HOME
        assertNull(result.getIdcone1());
        assertNotNull(result.getTipdes1());
        assertNotNull(result.getNatcon1());
        assertNotNull(result.getIdiren1());
        assertNull(result.getCodofe1());

        assertEquals(input.getDeliveries().get(0).getDestination().getId(), result.getTipdes1());
        assertEquals(input.getDeliveries().get(0).getAddress().getAddressType(), result.getNatcon1());
        assertEquals(input.getDeliveries().get(0).getAddress().getId(), result.getIdiren1());

        // 1 - CUSTOM
        assertNull(result.getIdcone2());
        assertNotNull(result.getTipdes2());
        assertNotNull(result.getNatcon2());
        assertNotNull(result.getIdiren2());
        assertNull(result.getCodofe2());

        assertEquals(input.getDeliveries().get(1).getDestination().getId(), result.getTipdes2());
        assertEquals(input.getDeliveries().get(1).getAddress().getAddressType(), result.getNatcon2());
        assertEquals(input.getDeliveries().get(1).getAddress().getId(), result.getIdiren2());

        // 2 - BRANCH
        assertNull(result.getIdcone3());
        assertNotNull(result.getTipdes3());
        assertNull(result.getNatcon3());
        assertNull(result.getIdiren3());
        assertNotNull(result.getCodofe3());

        assertEquals(input.getDeliveries().get(2).getDestination().getId(), result.getTipdes3());
        assertEquals(input.getDeliveries().get(2).getDestination().getBranch().getId(), result.getCodofe3());
    }

    private void commonInputAsserts(final DTOIntCard input, final FormatoMPME0W2 result) {
        assertNotNull(result.getTiprodf());
        assertNotNull(result.getMctarje());
        assertNotNull(result.getSopfisi());
        assertNotNull(result.getNombcli());
        assertNotNull(result.getDivipri());
        assertNotNull(result.getInddiv1());
        assertNotNull(result.getLincred());
        assertNotNull(result.getDivipri());
        assertNotNull(result.getFcierre());
        assertNotNull(result.getIdprodr());
        assertNotNull(result.getIndprod());
        assertNotNull(result.getIdprore());
        assertNotNull(result.getIdprore());
        assertNotNull(result.getDistarj());
        assertNotNull(result.getTipent1());
        assertNotNull(result.getTipent2());
        assertNotNull(result.getTipent3());
        assertNotNull(result.getNumcli1());
        assertNotNull(result.getPartcl1());
        assertNotNull(result.getTipper1());
        assertNotNull(result.getNumcli2());
        assertNotNull(result.getPartcl2());
        assertNotNull(result.getTipper2());
        assertNotNull(result.getTipmpag());
        assertNotNull(result.getFrempag());
        assertNotNull(result.getFcargo());
        assertNotNull(result.getTcontra());
        assertNotNull(result.getCodpafr());
        assertNotNull(result.getNucorel());
        assertNotNull(result.getCodofig());
        assertNotNull(result.getCodgest());
        assertNotNull(result.getRegisco());
        assertNotNull(result.getRegisve());
        assertNotNull(result.getBintarj());

        assertEquals(input.getCardTypeId(), result.getTiprodf());
        assertEquals(input.getProductId(), result.getMctarje());
        assertEquals(input.getPhysicalSupportId(), result.getSopfisi());
        assertEquals(input.getHolderName(), result.getNombcli());
        assertEquals(input.getCurrenciesCurrency(), result.getDivipri());
        assertEquals("0", result.getInddiv1());
        assertEquals(input.getGrantedCreditsAmount(), result.getLincred());
        assertEquals(input.getGrantedCreditsCurrency(), result.getDivipri());
        assertEquals(input.getCutOffDay(), result.getFcierre());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getIdprodr());
        assertEquals(input.getRelatedContracts().get(0).getRelationType().getId(), result.getIndprod());
        assertEquals(input.getRelatedContracts().get(0).getProduct().getId(), result.getIdprore());
        assertEquals(input.getRelatedContracts().get(0).getProduct().getProductType().getId(), result.getIdprore());
        assertEquals(input.getImages().get(0).getId(), result.getDistarj());
        assertEquals(input.getDeliveries().get(0).getServiceTypeId(), result.getTipent1());
        assertEquals(input.getDeliveries().get(1).getServiceTypeId(), result.getTipent2());
        assertEquals(input.getDeliveries().get(2).getServiceTypeId(), result.getTipent3());
        assertEquals(input.getParticipants().get(0).getId(), result.getNumcli1());
        assertEquals(input.getParticipants().get(0).getParticipantTypeId(), result.getPartcl1());
        assertEquals(input.getParticipants().get(0).getLegalPersonTypeId(), result.getTipper1());
        assertEquals(input.getParticipants().get(1).getId(), result.getNumcli2());
        assertEquals(input.getParticipants().get(1).getParticipantTypeId(), result.getPartcl2());
        assertEquals(input.getParticipants().get(1).getLegalPersonTypeId(), result.getTipper2());
        assertEquals(input.getPaymentMethod().getId(), result.getTipmpag());
        assertEquals(input.getPaymentMethod().getFrequency().getId(), result.getFrempag());
        assertEquals(input.getPaymentMethod().getFrequency().getDaysOfMonth().getDay(), String.valueOf(result.getFcargo()));
        assertEquals(input.getSupportContractType(), result.getTcontra());
        assertEquals(input.getMembershipsNumber(), result.getCodpafr());
        assertEquals(input.getCardAgreement(), result.getNucorel());
        assertEquals(input.getContractingBranchId(), result.getCodofig());
        assertEquals(input.getManagementBranchId(), result.getCodgest());
        assertEquals(input.getContractingBusinessAgentId(), result.getRegisco());
        assertEquals(input.getMarketBusinessAgentId(), result.getRegisve());
        assertEquals(input.getBankIdentificationNumber(), result.getBintarj());
    }

    @Test
    public void mapOut1FullTest() throws IOException {
        FormatoMPMS1W2 format = FormatsMpw2Stubs.getInstance().getFormatoMPMS1W2();

        when(translator.translateBackendEnumValueStrictly("cards.numberType.id", format.getIdtiptj()))
                .thenReturn("PAN");
        when(translator.translateBackendEnumValueStrictly("cards.cardType.id", format.getTiprodf()))
                .thenReturn("DEBIT_CARD");
        when(translator.translateBackendEnumValueStrictly("cards.brandAssociation.id", format.getIdmatar()))
                .thenReturn("AMERICAN_EXPRESS");
        when(translator.translateBackendEnumValueStrictly("cards.status.id", format.getEstarjs()))
                .thenReturn("PENDING_EMBOSSING");
        when(translator.translateBackendEnumValueStrictly("cards.supportContractType.id", format.getTcontra()))
                .thenReturn("PHYSICAL");

        CardPost result = mapper.mapOut1(format);

        assertNotNull(result.getId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getDescription());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getDescription());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getBrandAssociation().getId());
        assertNotNull(result.getBrandAssociation().getDescription());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNotNull(result.getCurrencies().get(1).getCurrency());
        assertNotNull(result.getCurrencies().get(1).getIsMajor());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNull(result.getImages());
        assertNotNull(result.getCutOffDay());
        assertNotNull(result.getOpeningDate());
        assertNotNull(result.getSupportContractType());
        assertNotNull(result.getMemberships().get(0).getId());
        assertNotNull(result.getMemberships().get(0).getNumber());
        assertNotNull(result.getCardAgreement());
        assertNotNull(result.getContractingBranch().getId());
        assertNotNull(result.getManagementBranch().getId());
        assertNotNull(result.getContractingBusinessAgent().getId());
        assertNotNull(result.getMarketBusinessAgent().getId());
        assertNotNull(result.getBankIdentificationNumber());

        assertEquals(format.getNumtarj(), result.getId());
        assertEquals(format.getNumtarj(), result.getNumber());
        assertEquals("PAN", result.getNumberType().getId());
        assertEquals(format.getDstiptj(), result.getNumberType().getDescription());
        assertEquals("DEBIT_CARD", result.getCardType().getId());
        assertEquals(format.getDsprodf(), result.getCardType().getDescription());
        assertEquals(format.getMctarje(), result.getProduct().getId());
        assertEquals(format.getDesmcta(), result.getProduct().getName());
        assertEquals("AMERICAN_EXPRESS", result.getBrandAssociation().getId());
        assertEquals(format.getNomatar(), result.getBrandAssociation().getDescription());
        assertEquals(format.getSopfisi(), result.getPhysicalSupport().getId());
        assertEquals(format.getDsopfis(), result.getPhysicalSupport().getDescription());
        assertEquals(format.getFecvenc(), result.getExpirationDate());
        assertEquals(format.getNombcli(), result.getHolderName());
        assertEquals(format.getMonpri(), result.getCurrencies().get(0).getCurrency());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        assertEquals(format.getMonsec(), result.getCurrencies().get(1).getCurrency());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        assertEquals(format.getLincred(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(format.getDlimcre(), result.getGrantedCredits().get(0).getCurrency());
        assertEquals("PENDING_EMBOSSING", result.getStatus().getId());
        assertEquals(format.getDestarj(), result.getStatus().getDescription());
        assertEquals(format.getFcierre(), result.getCutOffDay());
        assertEquals(FunctionUtils.buildDatetime(format.getFecalta(), DEFAULT_TIME), result.getOpeningDate());
        assertEquals("PHYSICAL", result.getSupportContractType());
        assertEquals(format.getCodpafr(), result.getMemberships().get(0).getId());
        assertEquals(format.getCodpafr(), result.getMemberships().get(0).getNumber());
        assertEquals(format.getNucorel(), result.getCardAgreement());
        assertEquals(format.getCodofig(), result.getContractingBranch().getId());
        assertEquals(format.getCodgest(), result.getManagementBranch().getId());
        assertEquals(format.getRegisco(), result.getContractingBusinessAgent().getId());
        assertEquals(format.getRegisve(), result.getMarketBusinessAgent().getId());
        assertEquals(format.getBintarj(), result.getBankIdentificationNumber());
    }

    @Test
    public void mapOut1EmptyTest() throws IOException {
        FormatoMPMS1W2 format = FormatsMpw2Stubs.getInstance().getFormatoMPMS1W2Empty();

        CardPost result = mapper.mapOut1(format);

        assertNull(result.getId());
        assertNull(result.getNumber());
        assertNull(result.getNumberType());
        assertNull(result.getCardType());
        assertNull(result.getProduct());
        assertNull(result.getBrandAssociation());
        assertNull(result.getPhysicalSupport());
        assertNull(result.getExpirationDate());
        assertNull(result.getHolderName());
        assertNull(result.getCurrencies());
        assertNotNull(result.getGrantedCredits());
        assertNull(result.getStatus());
        assertNull(result.getImages());
        assertNotNull(result.getCutOffDay());
        assertNull(result.getOpeningDate());
        assertNull(result.getSupportContractType());
        assertNull(result.getMemberships());
        assertNull(result.getCardAgreement());
        assertNull(result.getContractingBranch());
        assertNull(result.getManagementBranch());
        assertNull(result.getContractingBusinessAgent());
        assertNull(result.getMarketBusinessAgent());
        assertNull(result.getBankIdentificationNumber());
    }

    @Test
    public void mapOut2FullTest() throws IOException {
        List<FormatoMPMS2W2> stubs = FormatsMpw2Stubs.getInstance().getFormatoMPMS2W2s();
        CardPost result = new CardPost();
        result.setCardAgreement(RandomGenerator.getString(20));

        // Index 0 :: SPECIFIC
        FormatoMPMS2W2 format = stubs.get(0);

        when(translator.translateBackendEnumValueStrictly("cards.delivery.contactType", format.getNatcon()))
                .thenReturn(SPECIFIC);

        result = mapper.mapOut2(format, result);

        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(0).getContact().getContactType());
        assertNull(result.getDeliveries().get(0).getContact().getId());
        assertNull(result.getDeliveries().get(0).getId());

        assertEquals(format.getTipent(), result.getDeliveries().get(0).getServiceType().getId());
        assertEquals(SPECIFIC, result.getDeliveries().get(0).getContact().getContactType());

        // Index 1 :: STORED
        format = stubs.get(1);

        when(translator.translateBackendEnumValueStrictly("cards.delivery.contactType", format.getNatcon()))
                .thenReturn(STORED);

        result = mapper.mapOut2(format, result);

        assertNotNull(result.getDeliveries().get(1).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(1).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(1).getContact().getId());
        assertNotNull(result.getDeliveries().get(1).getId());

        assertEquals(format.getTipent(), result.getDeliveries().get(1).getServiceType().getId());
        assertEquals(STORED, result.getDeliveries().get(1).getContact().getContactType());
        assertEquals(format.getIdcone(), result.getDeliveries().get(1).getContact().getId());
        assertEquals(result.getCardAgreement() + format.getIdcone(), result.getDeliveries().get(1).getId());

        // Index 2 :: HOME :: SPECIFIC
        format = stubs.get(2);

        when(translator.translateBackendEnumValueStrictly("cards.delivery.destination", format.getTipdes()))
                .thenReturn(HOME);
        when(translator.translateBackendEnumValueStrictly("cards.delivery.addressType", format.getNatcon()))
                .thenReturn(SPECIFIC);

        result = mapper.mapOut2(format, result);

        assertNotNull(result.getDeliveries().get(2).getDestination().getId());
        assertNotNull(result.getDeliveries().get(2).getAddress().getAddressType());
        assertNull(result.getDeliveries().get(2).getAddress().getId());
        assertNull(result.getDeliveries().get(2).getId());

        assertEquals(HOME, result.getDeliveries().get(2).getDestination().getId());
        assertEquals(SPECIFIC, result.getDeliveries().get(2).getAddress().getAddressType());

        // Index 3 :: CUSTOM :: STORED
        format = stubs.get(3);

        when(translator.translateBackendEnumValueStrictly("cards.delivery.destination", format.getTipdes()))
                .thenReturn(CUSTOM);
        when(translator.translateBackendEnumValueStrictly("cards.delivery.addressType", format.getNatcon()))
                .thenReturn(STORED);

        result = mapper.mapOut2(format, result);

        assertNotNull(result.getDeliveries().get(3).getDestination().getId());
        assertNotNull(result.getDeliveries().get(3).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(3).getAddress().getId());
        assertNotNull(result.getDeliveries().get(3).getId());

        assertEquals(CUSTOM, result.getDeliveries().get(3).getDestination().getId());
        assertEquals(STORED, result.getDeliveries().get(3).getAddress().getAddressType());
        assertEquals(format.getIdiren(), result.getDeliveries().get(3).getAddress().getId());
        assertEquals(result.getCardAgreement() + format.getIdiren(), result.getDeliveries().get(3).getId());

        // Index 4 :: BRANCH
        format = stubs.get(4);

        when(translator.translateBackendEnumValueStrictly("cards.delivery.destination", format.getTipdes()))
                .thenReturn(BRANCH);

        result = mapper.mapOut2(format, result);

        assertNotNull(result.getDeliveries().get(4).getDestination().getId());
        assertNull(result.getDeliveries().get(4).getAddress());
        assertNotNull(result.getDeliveries().get(4).getDestination().getBranch().getId());
        assertNotNull(result.getDeliveries().get(4).getId());

        assertEquals(BRANCH, result.getDeliveries().get(4).getDestination().getId());
        assertEquals(format.getCodofe(), result.getDeliveries().get(4).getDestination().getBranch().getId());
        assertEquals(result.getCardAgreement() + format.getCodofe(), result.getDeliveries().get(4).getId());
    }

    @Test
    public void mapOut2WithoutFormatToContactTest() throws IOException {
        List<FormatoMPMS2W2> stubs = FormatsMpw2Stubs.getInstance().getFormatoMPMS2W2s();
        CardPost result = new CardPost();
        result.setCardAgreement(RandomGenerator.getString(20));

        // Index 0 :: SPECIFIC
        FormatoMPMS2W2 format = stubs.get(0);

        format.setIdcone(null);
        format.setNatcon(null);

        result = mapper.mapOut2(format, result);

        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNull(result.getDeliveries().get(0).getContact());
        assertNull(result.getDeliveries().get(0).getId());

        assertEquals(format.getTipent(), result.getDeliveries().get(0).getServiceType().getId());

    }

    @Test
    public void mapOut3FullTest() throws IOException {
        List<FormatoMPMS3W2> stubs = FormatsMpw2Stubs.getInstance().getFormatoMPMS3W2s();
        CardPost result = new CardPost();

        // Index 0
        FormatoMPMS3W2 format = stubs.get(0);

        when(translator.translateBackendEnumValueStrictly("cards.relatedContracts.productType", format.getIdprore()))
                .thenReturn("ACCOUNTS");
        when(translator.translateBackendEnumValueStrictly("cards.relatedContracts.relationType", format.getIndprod()))
                .thenReturn("LINKED_WITH");

        result = mapper.mapOut3(format, result);

        assertNotNull(result.getRelatedContracts().get(0).getId());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());

        assertEquals(format.getIdprodr(), result.getRelatedContracts().get(0).getId());
        assertEquals(format.getIdprodr(), result.getRelatedContracts().get(0).getContractId());
        assertEquals(format.getIdprore(), result.getRelatedContracts().get(0).getProduct().getId());
        assertEquals("ACCOUNTS", result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertEquals("LINKED_WITH", result.getRelatedContracts().get(0).getRelationType().getId());

        // Index 1
        format = stubs.get(1);

        when(translator.translateBackendEnumValueStrictly("cards.relatedContracts.productType", format.getIdprore()))
                .thenReturn("INSURANCES");
        when(translator.translateBackendEnumValueStrictly("cards.relatedContracts.relationType", format.getIndprod()))
                .thenReturn("LINKED_WITH");

        result = mapper.mapOut3(format, result);

        assertNotNull(result.getRelatedContracts().get(1).getId());
        assertNotNull(result.getRelatedContracts().get(1).getContractId());
        assertNotNull(result.getRelatedContracts().get(1).getProduct().getId());
        assertNotNull(result.getRelatedContracts().get(1).getProduct().getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(1).getRelationType().getId());

        assertEquals(format.getIdprodr(), result.getRelatedContracts().get(1).getId());
        assertEquals(format.getIdprodr(), result.getRelatedContracts().get(1).getContractId());
        assertEquals(format.getIdprore(), result.getRelatedContracts().get(1).getProduct().getId());
        assertEquals("INSURANCES", result.getRelatedContracts().get(1).getProduct().getProductType().getId());
        assertEquals("LINKED_WITH", result.getRelatedContracts().get(1).getRelationType().getId());
    }
}

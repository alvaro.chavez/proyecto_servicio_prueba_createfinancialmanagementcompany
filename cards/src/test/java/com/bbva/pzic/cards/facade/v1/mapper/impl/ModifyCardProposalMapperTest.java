package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputModifyCardProposal;
import com.bbva.pzic.cards.facade.v1.dto.Address;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;
import com.bbva.pzic.cards.facade.v1.mapper.common.AbstractCardProposalMapper;
import com.bbva.pzic.cards.facade.v1.mapper.common.CardProposalTimeMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 27/11/2019.
 *
 * @author Entelgy
 */
public class ModifyCardProposalMapperTest {

    private ModifyCardProposalMapper mapper;

    private AbstractCardProposalMapper abstractCardProposalMapper = new CardProposalTimeMapper();

    @Before
    public void setUp() {
        mapper = new ModifyCardProposalMapper();
        mapper.setAddressMapper(abstractCardProposalMapper);
    }

    @Test
    public void mapInFullTest() throws IOException {
        Proposal input = EntityMock.getInstance().buildModifyCardProposal();
        InputModifyCardProposal result = mapper.mapIn(EntityMock.PROPOSAL_ID, input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNotNull(result.getProposal());
        assertNotNull(result.getProposal().getDeliveries());
        assertEquals(1, result.getProposal().getDeliveries().size());
        assertNotNull(result.getProposal().getDeliveries().get(0).getServiceTypeId());
        assertNotNull(result.getProposal().getDeliveries().get(0).getContact());
        assertNotNull(result.getProposal().getDeliveries().get(0).getContact().getContactType());
        assertNotNull(result.getProposal().getDeliveries().get(0).getContact().getContactDetailType());
        assertNotNull(result.getProposal().getDeliveries().get(0).getContact().getContactAddress());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getProposal().getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getProposal().getDeliveries().get(0).getDestination());
        assertNotNull(result.getProposal().getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getProposal().getDeliveries().get(0).getDestination().getBranch());
        assertNotNull(result.getProposal().getDeliveries().get(0).getDestination().getBranch().getId());
        assertNotNull(result.getProposal().getPaymentMethod());
        assertNotNull(result.getProposal().getPaymentMethod().getId());
        assertNotNull(result.getProposal().getPaymentMethod().getFrequency());
        assertNotNull(result.getProposal().getPaymentMethod().getFrequency().getId());
        assertNotNull(result.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth());
        assertNotNull(result.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth().getCutOffDay());
        assertNotNull(result.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth().getDay());
        assertNotNull(result.getProposal().getContactability());
        assertNotNull(result.getProposal().getContactability().getReason());
        assertNotNull(result.getProposal().getContactability().getScheduletimes());
        assertNotNull(result.getProposal().getContactability().getScheduletimes().getStarttime());
        assertNotNull(result.getProposal().getContactability().getScheduletimes().getEndtime());
        assertNotNull(result.getProposal().getFees());
        assertNotNull(result.getProposal().getFees().getItemizeFees());
        assertEquals(1, result.getProposal().getFees().getItemizeFees().size());
        assertNotNull(result.getProposal().getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getProposal().getRate());
        assertNotNull(result.getProposal().getRate().getItemizeRates());
        assertEquals(1, result.getProposal().getRate().getItemizeRates().size());
        assertNotNull(result.getProposal().getRate().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getProposal().getRate().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getProposal().getRate().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getProposal().getRate().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getProposal().getGrantedCredits());
        assertEquals(1, result.getProposal().getGrantedCredits().size());
        assertNotNull(result.getProposal().getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getProposal().getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getProposal().getProduct());
        assertNotNull(result.getProposal().getProduct().getId());
        assertNotNull(result.getProposal().getOfferId());
        assertNotNull(result.getProposal().getNotificationsByOperation());

        assertEquals(input.getPaymentMethod().getId(), result.getProposal().getPaymentMethod().getId());
        assertEquals(input.getPaymentMethod().getFrecuency().getId(), result.getProposal().getPaymentMethod().getFrequency().getId());
        assertEquals(input.getPaymentMethod().getFrecuency().getDaysOfMonth().getCutOffDay(), result.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth().getCutOffDay());
        assertEquals(input.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay(), result.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth().getDay());
        assertEquals(input.getDeliveries().get(0).getServiceType().getId(), result.getProposal().getDeliveries().get(0).getServiceTypeId());
        assertEquals(input.getDeliveries().get(0).getContact().getContactType(), result.getProposal().getDeliveries().get(0).getContact().getContactType());
        assertEquals(input.getDeliveries().get(0).getContact().getContact().getContactDetailType(), result.getProposal().getDeliveries().get(0).getContact().getContactDetailType());
        assertEquals(input.getDeliveries().get(0).getContact().getContact().getAddress(), result.getProposal().getDeliveries().get(0).getContact().getContactAddress());
        assertEquals(input.getDeliveries().get(0).getAddress().getAddressType(), result.getProposal().getDeliveries().get(0).getAddress().getAddressType());
        assertEquals(input.getDeliveries().get(0).getAddress().getId(), result.getProposal().getDeliveries().get(0).getAddress().getId());
        assertEquals(input.getDeliveries().get(0).getDestination().getId(), result.getProposal().getDeliveries().get(0).getDestination().getId());
        assertEquals(input.getDeliveries().get(0).getDestination().getBranch().getId(), result.getProposal().getDeliveries().get(0).getDestination().getBranch().getId());
        assertEquals(input.getContactability().getReason(), result.getProposal().getContactability().getReason());
        assertEquals(EntityMock.PROPOSAL_ID, result.getProposalId());
        assertEquals(input.getFees().getItemizeFees().get(0).getFeeType(), result.getProposal().getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType(), result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount(), result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency(), result.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getRates().getItemizeRates().get(0).getRateType(), result.getProposal().getRate().getItemizeRates().get(0).getRateType());
        assertEquals(input.getRates().getItemizeRates().get(0).getCalculationDate(), result.getProposal().getRate().getItemizeRates().get(0).getCalculationDate());
        assertEquals(input.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage(), result.getProposal().getRate().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertEquals(input.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType(), result.getProposal().getRate().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getProposal().getGrantedCredits().get(0).getAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getProposal().getGrantedCredits().get(0).getCurrency());
        assertEquals(input.getProduct().getId(), result.getProposal().getProduct().getId());
        assertEquals(input.getOfferId(), result.getProposal().getOfferId());
        assertEquals(input.getNotificationsByOperation(), result.getProposal().getNotificationsByOperation());
    }

    @Test
    public void mapInEmptyTest() {
        InputModifyCardProposal result = mapper.mapIn(EntityMock.PROPOSAL_ID, new Proposal());

        assertNotNull(result);
        assertNotNull(result.getProposal());
        assertNull(result.getProposal().getContactability());
        assertNull(result.getProposal().getPaymentMethod());
        assertNull(result.getProposal().getDeliveries());
        assertNull(result.getProposal().getProduct());
        assertNull(result.getProposal().getFees());
        assertNull(result.getProposal().getRate());
        assertNull(result.getProposal().getOfferId());
        assertNull(result.getProposal().getGrantedCredits());
        assertNull(result.getProposal().getNotificationsByOperation());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<Proposal> result = mapper.mapOut(new Proposal());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<Proposal> result = mapper.mapOut(null);

        assertNull(result);
    }
}

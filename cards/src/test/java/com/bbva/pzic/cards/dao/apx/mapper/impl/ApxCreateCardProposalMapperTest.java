package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.jee.arq.spring.core.managers.OutputHeaderManager;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardProposal;
import com.bbva.pzic.cards.dao.model.ppcut001_1.PeticionTransaccionPpcut001_1;
import com.bbva.pzic.cards.dao.model.ppcut001_1.RespuestaTransaccionPpcut001_1;
import com.bbva.pzic.cards.dao.model.ppcut001_1.mock.Ppcut001_1Stubs;
import com.bbva.pzic.cards.facade.v1.dto.ProposalCard;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created on 11/20/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ApxCreateCardProposalMapperTest {

    @InjectMocks
    private ApxCreateCardProposalMapper mapper;

    @Mock
    private OutputHeaderManager outputHeaderManager;

    @Test
    public void mapInFullTest() throws IOException {
        InputCreateCardProposal input = EntityMock.getInstance().buildInputCreateCardProposal();
        PeticionTransaccionPpcut001_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getEntityin());
        assertNotNull(result.getEntityin().getCardtype());
        assertNotNull(result.getEntityin().getCardtype().getId());
        assertNotNull(result.getEntityin().getProduct());
        assertNotNull(result.getEntityin().getProduct().getId());
        assertNotNull(result.getEntityin().getPhysicalsupport());
        assertNotNull(result.getEntityin().getPhysicalsupport().getId());
        assertNotNull(result.getEntityin().getGrantedcredits());
        assertEquals(1, result.getEntityin().getGrantedcredits().size());
        assertNotNull(result.getEntityin().getGrantedcredits().get(0).getGrantedcredit().getAmount());
        assertNotNull(result.getEntityin().getGrantedcredits().get(0).getGrantedcredit().getCurrency());
        assertNotNull(result.getEntityin().getContact());
        assertNotNull(result.getEntityin().getContact().getContacttype());
        assertNotNull(result.getEntityin().getContact().getSpecificcontact().getContactdetailtype());
        assertNotNull(result.getEntityin().getContact().getSpecificcontact().getNumber());
        assertNotNull(result.getEntityin().getContact().getSpecificcontact().getPhonecompany());
        assertNotNull(result.getEntityin().getContact().getSpecificcontact().getPhonecompany().getId());
        assertNotNull(result.getEntityin().getRates());
        assertNotNull(result.getEntityin().getRates().getItemizerates());
        assertEquals(1, result.getEntityin().getRates().getItemizerates().size());
        assertNotNull(result.getEntityin().getRates().getItemizerates().get(0).getItemizerate().getRatetype());
        assertNotNull(result.getEntityin().getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit());
        assertNotNull(result.getEntityin().getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getPercentage());
        assertNotNull(result.getEntityin().getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getUnitratetype());
        assertNotNull(result.getEntityin().getFees());
        assertNotNull(result.getEntityin().getFees().getItemizefees());
        assertEquals(1, result.getEntityin().getFees().getItemizefees().size());
        assertNotNull(result.getEntityin().getFees().getItemizefees().get(0).getItemizefee().getFeetype());
        assertNotNull(result.getEntityin().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit());
        assertNotNull(result.getEntityin().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getUnittype());
        assertNotNull(result.getEntityin().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getAmount());
        assertNotNull(result.getEntityin().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getCurrency());
        assertNotNull(result.getEntityin().getParticipants());
        assertNotNull(result.getEntityin().getParticipants().get(0).getParticipant().getPersontype());
        assertNotNull(result.getEntityin().getParticipants().get(0).getParticipant().getId());
        assertNotNull(result.getEntityin().getParticipants().get(0).getParticipant().getIscustomer());
        assertNotNull(result.getEntityin().getParticipants().get(0).getParticipant().getParticipanttype());
        assertNotNull(result.getEntityin().getParticipants().get(0).getParticipant().getParticipanttype().getId());
        assertNotNull(result.getEntityin().getParticipants().get(1).getParticipant().getPersontype());
        assertNotNull(result.getEntityin().getParticipants().get(1).getParticipant().getId());
        assertNotNull(result.getEntityin().getParticipants().get(1).getParticipant().getIscustomer());
        assertNotNull(result.getEntityin().getParticipants().get(1).getParticipant().getParticipanttype());
        assertNotNull(result.getEntityin().getParticipants().get(1).getParticipant().getParticipanttype().getId());
        assertNotNull(result.getEntityin().getOfferid());

        assertEquals(input.getCardType().getId(), result.getEntityin().getCardtype().getId());
        assertEquals(input.getProduct().getId(), result.getEntityin().getProduct().getId());
        assertEquals(input.getPhysicalSupport().getId(), result.getEntityin().getPhysicalsupport().getId());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), new BigDecimal(result.getEntityin().getGrantedcredits().get(0).getGrantedcredit().getAmount()));
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getEntityin().getGrantedcredits().get(0).getGrantedcredit().getCurrency());
        assertEquals(input.getContact().getContactType(), result.getEntityin().getContact().getContacttype());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getEntityin().getContact().getSpecificcontact().getContactdetailtype());
        assertEquals(input.getContact().getContact().getNumber(), result.getEntityin().getContact().getSpecificcontact().getNumber());
        assertEquals(input.getContact().getContact().getPhoneCompany().getId(), result.getEntityin().getContact().getSpecificcontact().getPhonecompany().getId());
        assertEquals(input.getRates().getItemizeRates().get(0).getRateType(), result.getEntityin().getRates().getItemizerates().get(0).getItemizerate().getRatetype());
        assertEquals(input.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage(), new BigDecimal(result.getEntityin().getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getPercentage()));
        assertEquals(input.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType(), result.getEntityin().getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getUnitratetype());
        assertEquals(input.getFees().getItemizeFees().get(0).getFeeType(), result.getEntityin().getFees().getItemizefees().get(0).getItemizefee().getFeetype());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType(), result.getEntityin().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getUnittype());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount(), new BigDecimal(result.getEntityin().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getAmount()));
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency(), result.getEntityin().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getCurrency());
        assertEquals(input.getParticipants().get(0).getPersonType(), result.getEntityin().getParticipants().get(0).getParticipant().getPersontype());
        assertEquals(input.getParticipants().get(0).getId(), result.getEntityin().getParticipants().get(0).getParticipant().getId());
        assertEquals(input.getParticipants().get(0).getIsCustomer(), result.getEntityin().getParticipants().get(0).getParticipant().getIscustomer());
        assertEquals(input.getParticipants().get(0).getParticipantTypeId(), result.getEntityin().getParticipants().get(0).getParticipant().getParticipanttype().getId());
        assertEquals(input.getParticipants().get(1).getPersonType(), result.getEntityin().getParticipants().get(1).getParticipant().getPersontype());
        assertEquals(input.getParticipants().get(1).getId(), result.getEntityin().getParticipants().get(1).getParticipant().getId());
        assertEquals(input.getParticipants().get(1).getIsCustomer(), result.getEntityin().getParticipants().get(1).getParticipant().getIscustomer());
        assertEquals(input.getParticipants().get(1).getParticipantTypeId(), result.getEntityin().getParticipants().get(1).getParticipant().getParticipanttype().getId());
        assertEquals(input.getOfferId(), result.getEntityin().getOfferid());
    }

    @Test
    public void mapInEmptyTest() {
        PeticionTransaccionPpcut001_1 result = mapper.mapIn(new InputCreateCardProposal());

        assertNotNull(result);
        assertNotNull(result.getEntityin());
        assertNull(result.getEntityin().getGrantedcredits());
        assertNull(result.getEntityin().getPhysicalsupport());
        assertNull(result.getEntityin().getCardtype());
        assertNull(result.getEntityin().getFees());
        assertNull(result.getEntityin().getProduct());
        assertNull(result.getEntityin().getRates());
        assertNull(result.getEntityin().getParticipants());
        assertNull(result.getEntityin().getOfferid());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        ProposalCard result = mapper.mapOut(input);

        verify(outputHeaderManager, times(1)).setHeader(OutputHeaderManager.LOCATION_HTTP_HEADER, "/proposals/".concat(input.getEntityout().getId()));

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getOfferId());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());

        assertEquals(input.getEntityout().getId(), result.getId());
        assertEquals(input.getEntityout().getCardtype().getId(), result.getCardType().getId());
        assertEquals(input.getEntityout().getProduct().getId(), result.getProduct().getId());
        assertEquals(input.getEntityout().getProduct().getName(), result.getProduct().getName());
        assertEquals(input.getEntityout().getPhysicalsupport().getId(), result.getPhysicalSupport().getId());
        assertEquals(input.getEntityout().getPhysicalsupport().getDescription(), result.getPhysicalSupport().getDescription());
        assertEquals(input.getEntityout().getGrantedcredits().get(0).getGrantedcredit().getCurrency(), result.getGrantedCredits().get(0).getCurrency());
        assertEquals(input.getEntityout().getGrantedcredits().get(0).getGrantedcredit().getAmount(), result.getGrantedCredits().get(0).getAmount().toString());
        assertEquals(input.getEntityout().getContact().getId(), result.getContact().getId());
        assertEquals(input.getEntityout().getContact().getContacttype(), result.getContact().getContactType());
        assertEquals(input.getEntityout().getContact().getSpecificcontact().getContactdetailtype(), result.getContact().getContact().getContactDetailType());
        assertEquals(input.getEntityout().getContact().getSpecificcontact().getNumber(), result.getContact().getContact().getNumber());
        assertEquals(input.getEntityout().getContact().getSpecificcontact().getPhonecompany().getId(), result.getContact().getContact().getPhoneCompany().getId());
        assertEquals(input.getEntityout().getContact().getSpecificcontact().getPhonecompany().getName(), result.getContact().getContact().getPhoneCompany().getName());
        assertEquals(input.getEntityout().getRates().getItemizerates().get(0).getItemizerate().getRatetype(), result.getRates().getItemizeRates().get(0).getRateType());
        assertEquals(input.getEntityout().getRates().getItemizerates().get(0).getItemizerate().getDescription(), result.getRates().getItemizeRates().get(0).getDescription());
        assertEquals(input.getEntityout().getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getUnitratetype(), result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertEquals(input.getEntityout().getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getPercentage(), result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage().toBigInteger().toString());
        assertEquals(input.getEntityout().getFees().getItemizefees().get(0).getItemizefee().getFeetype(), result.getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(input.getEntityout().getFees().getItemizefees().get(0).getItemizefee().getDescription(), result.getFees().getItemizeFees().get(0).getDescription());
        assertEquals(input.getEntityout().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getUnittype(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertEquals(input.getEntityout().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getAmount(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount().toBigInteger().toString());
        assertEquals(input.getEntityout().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getCurrency(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getEntityout().getOperationdate().getTime(), result.getOperationDate().getTime());
        assertEquals(input.getEntityout().getOperationnumber(), result.getOperationNumber());
        assertEquals(input.getEntityout().getStatus(), result.getStatus());
        assertEquals(input.getEntityout().getParticipants().get(0).getParticipant().getPersontype(), result.getParticipants().get(0).getPersonType());
        assertEquals(input.getEntityout().getParticipants().get(0).getParticipant().getId(), result.getParticipants().get(0).getId());
        assertEquals(input.getEntityout().getParticipants().get(0).getParticipant().getIscustomer(), result.getParticipants().get(0).getIsCustomer());
        assertEquals(input.getEntityout().getParticipants().get(0).getParticipant().getParticipanttype().getId(), result.getParticipants().get(0).getParticipantType().getId());
        assertEquals(input.getEntityout().getParticipants().get(1).getParticipant().getPersontype(), result.getParticipants().get(1).getPersonType());
        assertEquals(input.getEntityout().getParticipants().get(1).getParticipant().getId(), result.getParticipants().get(1).getId());
        assertEquals(input.getEntityout().getParticipants().get(1).getParticipant().getIscustomer(), result.getParticipants().get(1).getIsCustomer());
        assertEquals(input.getEntityout().getParticipants().get(1).getParticipant().getParticipanttype().getId(), result.getParticipants().get(1).getParticipantType().getId());
        assertEquals(input.getEntityout().getOfferid(), result.getOfferId());
    }

    @Test
    public void mapOutWithIdNullTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().setId(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutWithNullCardTypeTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().setCardtype(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNull(result.getCardType());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutWithNullProductTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().setProduct(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNull(result.getProduct());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutWithNullPhysicalSupportTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().setPhysicalsupport(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNull(result.getPhysicalSupport());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutWithNullGrantedCreditsTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().setGrantedcredits(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNull(result.getGrantedCredits());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutWithNullContactTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().setContact(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNull(result.getContact());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutWithNullContactContactTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().getContact().setSpecificcontact(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNull(result.getContact().getContact());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutWithNullRatesTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().setRates(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNull(result.getRates());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutWithNullRatesItemizeRatesTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().getRates().setItemizerates(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNull(result.getRates().getItemizeRates());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutWithNullFeesTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().setFees(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNull(result.getFees());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutNullFeesItemizeFeesTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().getFees().setItemizefees(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNull(result.getFees().getItemizeFees());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getOfferId());
    }

    @Test
    public void mapOutWithoutParticipantsTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().setParticipants(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getOfferId());
        assertNull(result.getParticipants());
    }

    @Test
    public void mapOutWithoutParticipantsParticipantTypeTest() throws IOException {
        RespuestaTransaccionPpcut001_1 input = Ppcut001_1Stubs.getInstance().getProposal();
        input.getEntityout().getParticipants().get(0).getParticipant().setParticipanttype(null);
        ProposalCard result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getId());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getName());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getStatus());
        assertNotNull(result.getOfferId());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNull(result.getParticipants().get(0).getParticipantType());
    }

    @Test
    public void mapOutEmptyTest() {
        ProposalCard result = mapper.mapOut(new RespuestaTransaccionPpcut001_1());

        assertNull(result);
    }
}
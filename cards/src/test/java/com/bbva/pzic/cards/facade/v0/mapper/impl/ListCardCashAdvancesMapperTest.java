package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCashAdvancesSearchCriteria;
import com.bbva.pzic.cards.facade.v0.dto.CashAdvances;
import com.bbva.pzic.cards.facade.v0.mapper.IListCardCashAdvancesMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static com.bbva.pzic.cards.EntityMock.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ListCardCashAdvancesMapperTest {

    @InjectMocks
    private IListCardCashAdvancesMapper mapper = new ListCardCashAdvancesMapper();

    @Mock
    private AbstractCypherTool cypherTool;

    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.CARDID)).thenReturn(CARD_ID);
        Mockito.when(enumMapper.getEnumValue("cards.terms.frequency", FRECCUO_M)).thenReturn(FRECCUO_VAL);
        Mockito.when(enumMapper.getEnumValue("cards.rates.mode", PERCENTAGE_P)).thenReturn(PERCENTAGE_VAL);
        Mockito.when(enumMapper.getEnumValue("cards.rates.mode", AMOUNT_M)).thenReturn(AMOUNT_VAL);
    }

    @Test
    public void mapInFullTest() {
        final DTOIntCashAdvancesSearchCriteria result = mapper.mapInput(EntityMock.CARD_ENCRYPT_ID);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());

    }

    @Test
    public void mapInEmptyTest() {
        final DTOIntCashAdvancesSearchCriteria result = mapper.mapInput(null);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getCardId());

    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<List<CashAdvances>> result = mapper.mapOutput(EntityMock.getInstance().getDTOIntListCashAdvances());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<List<CashAdvances>> result = mapper.mapOutput(null);
        Assert.assertNull(result);
    }

}

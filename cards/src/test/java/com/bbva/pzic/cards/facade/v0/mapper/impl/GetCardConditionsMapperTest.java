package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputGetCardConditions;
import com.bbva.pzic.cards.canonic.Condition;
import com.bbva.pzic.cards.canonic.Conditions;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static com.bbva.pzic.cards.EntityMock.CARD_ENCRYPT_ID;
import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static org.mockito.Mockito.when;

/**
 * Created on 22/05/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class GetCardConditionsMapperTest {

    @InjectMocks
    private GetCardConditionsMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Before
    public void setUp() {
        when(cypherTool.decrypt(CARD_ENCRYPT_ID, "cardId", RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_CONDITIONS)).thenReturn(CARD_ID);
    }

    @Test
    public void mapInFullTest() {
        InputGetCardConditions result = mapper.mapIn(EntityMock.CARD_ENCRYPT_ID);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());
    }

    @Test
    public void mapOutFullTest() {
        Conditions result = mapper.mapOut(Collections.singletonList(new Condition()));
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        Conditions result = mapper.mapOut(Collections.emptyList());
        Assert.assertNull(result);
    }
}

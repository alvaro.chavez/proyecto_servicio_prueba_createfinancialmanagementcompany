package com.bbva.pzic.cards.util.mappers;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Created on 30/07/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ErrorMapperTest {
    private static final String SMC_REGISTRY_ID = "SMCPE1810265";
    private static final int BACKEND_ERROR_CODE = 400;
    private static final String ERROR_STORE_AVATAR_ALIAS = "errorStoreAvatar";

    @InjectMocks
    private ErrorMapper mapper;

    @Mock
    private ConfigurationManager configurationManager;

    @Before
    public void setUp() {
        Mockito.when(configurationManager.getProperty("servicing.smc.configuration.SMCPE1810265.backend.error.code.400")).thenReturn("errorStoreAvatar");
    }

    @Test
    public void testGetAlias() {
        String result = mapper.getAlias(SMC_REGISTRY_ID, BACKEND_ERROR_CODE);
        Assert.assertNotNull(result);
        Assert.assertEquals(ERROR_STORE_AVATAR_ALIAS, result);
    }

    @Test(expected = BusinessServiceException.class)
    public void testGetAliasNotFound() {
        mapper.getAlias("NOEXISTE", BACKEND_ERROR_CODE);
    }

    @Test(expected = BusinessServiceException.class)
    public void testGetAliasWithoutSmcRegistryId() {
        mapper.getAlias(null, BACKEND_ERROR_CODE);
    }
}
package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.RelatedContract;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.*;

/**
 * Created on 07/02/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ListCardsV0MapperTest {

    @InjectMocks
    private ListCardsV0Mapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private final EntityMock mock = EntityMock.getInstance();

    @Test
    public void mapInFullTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, STATUS_IDS, PARTICIPANTS_PARTICIPANT_TYPE_ID, PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertEquals(dtoIn.getParticipantsParticipantTypeId(), PARTICIPANTS_PARTICIPANT_TYPE_ID);
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutCustomerIdTest() {
        DTOIntListCards dtoIn = mapper.mapIn(null, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, STATUS_IDS, PARTICIPANTS_PARTICIPANT_TYPE_ID, PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertNull(dtoIn.getCustomerId());
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutCardTypeIdTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, Collections.emptyList(), PHYSICAL_SUPPORT_ID, STATUS_IDS, PARTICIPANTS_PARTICIPANT_TYPE_ID, PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertNull(dtoIn.getCardTypeId());
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutPhysicalSupportIdTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, null, STATUS_IDS, PARTICIPANTS_PARTICIPANT_TYPE_ID, PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertNull(dtoIn.getPhysicalSupportId());
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutStatusIdTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, Collections.emptyList(), PARTICIPANTS_PARTICIPANT_TYPE_ID, PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertNull(dtoIn.getStatusId());
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutParticipantsTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, STATUS_IDS, Collections.emptyList(), PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertNull(dtoIn.getParticipantsParticipantTypeId());
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutPaginationKeyTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, STATUS_IDS, PARTICIPANTS_PARTICIPANT_TYPE_ID, null, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertNull(dtoIn.getPaginationKey());
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithPageSizeTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, STATUS_IDS, PARTICIPANTS_PARTICIPANT_TYPE_ID, PAGINATION_KEY, null);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertNull(dtoIn.getPageSize());
    }

    @Test
    public void mapInWithFieldNullTest() {
        DTOIntListCards dtoIn = mapper.mapIn(null, Collections.emptyList(), null, Collections.emptyList(), PARTICIPANTS_PARTICIPANT_TYPE_ID, null, null);
        Assert.assertNotNull(dtoIn);
        Assert.assertNull(dtoIn.getCustomerId());
        Assert.assertNull(dtoIn.getCardTypeId());
        Assert.assertNull(dtoIn.getPhysicalSupportId());
        Assert.assertNull(dtoIn.getStatusId());
        Assert.assertNull(dtoIn.getPaginationKey());
        Assert.assertNull(dtoIn.getPageSize());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        DTOOutListCards dtoOut = mock.getDtoOutListCards();
        ServiceResponse<List<Card>> result = mapper.mapOut(dtoOut, new Pagination());
        Assert.assertNotNull(result.getData());
        Assert.assertNotNull(result.getPagination());
    }

    @Test
    public void mapOutDtoOutInitializedTest() {
        ServiceResponse<List<Card>> result = mapper.mapOut(new DTOOutListCards(), new Pagination());
        Assert.assertNull(result);
    }

    @Test
    public void mapOutArrayIsEmptyTest() throws IOException {
        DTOOutListCards dtoOut = mock.getDtoOutListCards();
        dtoOut.setData(new ArrayList<>());
        ServiceResponse<List<Card>> result = mapper.mapOut(dtoOut, null);
        Assert.assertNull(result);
    }

    @Test
    public void mapOutTest() throws IOException {
        Mockito.when(cypherTool.encrypt(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(CARD_RELATED_CONTRACT_ENCRYPT);

        DTOOutListCards dtoOut = mock.getDtoOutListCards();
        List<RelatedContract> relatedContracts = new ArrayList<>();
        relatedContracts.add(mock.getRelatedContract());
        dtoOut.getData().get(0).setRelatedContracts(relatedContracts);
        ServiceResponse<List<Card>> result = mapper.mapOut(dtoOut, new Pagination());

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData().get(0).getRelatedContracts());
        Assert.assertNotNull(result.getData().get(0).getRelatedContracts().get(0).getRelatedContractId());

        Assert.assertEquals(CARD_RELATED_CONTRACT_ENCRYPT, result.getData().get(0).getRelatedContracts().get(0).getRelatedContractId());

        Assert.assertNotNull(result.getData().get(0).getImages().get(0).getUrl());
        Assert.assertNotNull(result.getData().get(1).getImages().get(0).getUrl());
    }
}

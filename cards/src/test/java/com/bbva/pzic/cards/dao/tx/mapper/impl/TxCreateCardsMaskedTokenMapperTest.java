package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardsMaskedToken;
import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMEN2F;
import com.bbva.pzic.cards.dao.model.mp2f.FormatoMPMS12F;
import com.bbva.pzic.cards.dao.model.mp2f.mock.FormatsMp2fStubs;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardsMaskedTokenMapper;
import com.bbva.pzic.cards.facade.v0.dto.MaskedToken;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TxCreateCardsMaskedTokenMapperTest {

    private ITxCreateCardsMaskedTokenMapper mapper = new TxCreateCardsMaskedTokenMapper();
    private EntityMock entityMock = EntityMock.getInstance();
    private FormatsMp2fStubs formatsMp2fStubs = FormatsMp2fStubs.getInstance();

    @Test
    public void mapInTest() {
        InputCreateCardsMaskedToken input = entityMock.buildInputCreateCardsMaskedToken();
        FormatoMPMEN2F result = mapper.mapIn(input);

        assertNotNull(result.getIdetarj());
        assertNotNull(result.getFeccad());

        assertEquals(input.getCardId(), result.getIdetarj());
        assertEquals(input.getExpirationDate(), result.getFeccad());
    }

    @Test
    public void mapOutTest() {
        FormatoMPMS12F formatoMPMS12F = formatsMp2fStubs.getFormatoMPMS12F();
        MaskedToken result = mapper.mapOut(formatoMPMS12F);

        assertNotNull(result.getId());
        assertNotNull(result.getToken());
        assertEquals(formatoMPMS12F.getIdtoken(), result.getId());
        assertEquals(formatoMPMS12F.getNrotarj(), result.getToken());
    }
}
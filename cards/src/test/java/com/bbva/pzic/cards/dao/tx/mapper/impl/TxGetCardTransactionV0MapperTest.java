package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputGetCardTransaction;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.mpgm.*;
import com.bbva.pzic.cards.dao.model.mpgm.mock.FormatoMPGMMock;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created on 26/12/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxGetCardTransactionV0MapperTest {

    private static final String PERIODO_01 = "ANUAL";

    @Mock
    private EnumMapper enumMapper;

    @InjectMocks
    private TxGetCardTransactionV0Mapper mapper;

    private DummyMock dummyMock;

    @Before
    public void setUp() {
        dummyMock = new DummyMock();
    }

    @Test
    public void mapInTest() {
        InputGetCardTransaction dto = EntityMock.getInstance().buildInputGetCardTransaction();
        FormatoMPMENGM result = mapper.mapIn(dto);

        assertNotNull(result.getIdentif());
        assertNotNull(result.getNumtarj());

        assertEquals(dto.getCardId(), result.getNumtarj());
        assertEquals(dto.getTransactionId(), result.getIdentif());
    }

    private void enumMapOut() {
        Mockito.when(enumMapper.getEnumValue("cards.transaction.transactionType.id", "08")).thenReturn("PURCHASE");
        Mockito.when(enumMapper.getEnumValue("transactions.moneyFlow.id", "H")).thenReturn("EXPENSE");
        Mockito.when(enumMapper.getEnumValue("transactions.financingType.id", "1")).thenReturn("FINANCING_AVAILABLE");
        Mockito.when(enumMapper.getEnumValue("transactions.status.id", "C")).thenReturn("SETTLED");
    }

    @Test
    public void mapOutFullTest() throws IOException {
        enumMapOut();
        FormatoMPMS1GM formatoMPMS1GM = FormatoMPGMMock.getInstance().getFormatoMPMS1GM();
        TransactionData result = mapper.mapOut(formatoMPMS1GM, new TransactionData());

        assertNotNull(result);
        assertNotNull(result.getData());
        assertNotNull(result.getData().getId());
        assertNotNull(result.getData().getLocalAmount().getAmount());
        assertNotNull(result.getData().getLocalAmount().getCurrency());
        assertNotNull(result.getData().getTransactionType().getId());
        assertNotNull(result.getData().getTransactionType().getName());
        assertNotNull(result.getData().getTransactionType().getInternalCode().getId());
        assertNotNull(result.getData().getTransactionType().getInternalCode().getName());
        assertNotNull(result.getData().getConcept());
        assertNotNull(result.getData().getMoneyFlow().getId());
        assertNotNull(result.getData().getMoneyFlow().getName());
        assertNotNull(result.getData().getOperationDate());
        assertNotNull(result.getData().getAccountedDate());
        assertNotNull(result.getData().getValuationDate());
        assertNotNull(result.getData().getFinancingType().getId());
        assertNotNull(result.getData().getFinancingType().getName());
        assertNotNull(result.getData().getStatus().getId());
        assertNotNull(result.getData().getStatus().getName());
        assertNotNull(result.getData().getExchangeRate().getDate());
        assertNotNull(result.getData().getExchangeRate().getValues().getFactor().getRatio());
        assertNotNull(result.getData().getExchangeRate().getValues().getPriceType());
        assertNotNull(result.getData().getContract().getId());
        assertNotNull(result.getData().getContract().getNumber());
        assertNotNull(result.getData().getContract().getNumberType().getId());
        assertNotNull(result.getData().getContract().getNumberType().getName());
        assertNotNull(result.getData().getAdditionalInformation().getReference());

        assertEquals(result.getData().getId(), formatoMPMS1GM.getIdentif());
        assertEquals(result.getData().getLocalAmount().getAmount(), formatoMPMS1GM.getImporte());
        assertEquals(result.getData().getLocalAmount().getCurrency(), formatoMPMS1GM.getDivisa());
        assertEquals(result.getData().getTransactionType().getId(), "PURCHASE");
        assertEquals(result.getData().getTransactionType().getName(), formatoMPMS1GM.getNommov());
        assertEquals(result.getData().getTransactionType().getInternalCode().getId(), formatoMPMS1GM.getIntcod());
        assertEquals(result.getData().getTransactionType().getInternalCode().getName(), formatoMPMS1GM.getNomcod());

        assertEquals(result.getData().getConcept(), formatoMPMS1GM.getConcept());
        assertEquals(result.getData().getMoneyFlow().getId(), "EXPENSE");
        assertEquals(result.getData().getMoneyFlow().getName(), formatoMPMS1GM.getNomiope());
        assertEquals(result.getData().getFinancingType().getId(), "FINANCING_AVAILABLE");
        assertEquals(result.getData().getFinancingType().getName(), formatoMPMS1GM.getTipfina());
        assertEquals(result.getData().getStatus().getId(), "SETTLED");
        assertEquals(result.getData().getStatus().getName(), formatoMPMS1GM.getTipesta());

        assertEquals(result.getData().getExchangeRate().getValues().getFactor().getRatio(), formatoMPMS1GM.getTipcamb().toString());
        assertEquals(result.getData().getExchangeRate().getValues().getPriceType(), formatoMPMS1GM.getModcamb());
        assertEquals(result.getData().getContract().getId(), formatoMPMS1GM.getIdcont());
        assertEquals(result.getData().getContract().getNumber(), formatoMPMS1GM.getNumtarj());
        assertEquals(result.getData().getContract().getNumberType().getId(), formatoMPMS1GM.getTipnum());
        assertEquals(result.getData().getContract().getNumberType().getName(), formatoMPMS1GM.getDescnum());
        assertEquals(result.getData().getAdditionalInformation().getReference(), formatoMPMS1GM.getIdentif());

        formatoMPMS1GM = FormatoMPGMMock.getInstance().getFormatoMPMS1GM();
        formatoMPMS1GM.setFechcon(null);
        result = mapper.mapOut(formatoMPMS1GM, new TransactionData());

        assertNotNull(result);
        assertNotNull(result.getData());
        assertNull(result.getData().getAccountedDate());
        assertNull(result.getData().getValuationDate());
    }

    @Test
    public void mapOutFormatOutWithFechopeHoropeTest() {
        enumMapOut();
        FormatoMPMS1GM formatoMPMS1GM = new FormatoMPMS1GM();
        formatoMPMS1GM.setHorope("06:41:41");
        formatoMPMS1GM.setTipcamb(new BigDecimal("0000.000000000"));
        TransactionData result = mapper.mapOut(formatoMPMS1GM, new TransactionData());

        assertNotNull(result);
        assertEquals(result.getData().getExchangeRate().getValues().getFactor().getRatio(), "0");
    }

    @Test
    public void mapOut2WithTransactionDataInstanceTest() throws IOException {
        FormatoMPMS2GM formatoMPMS2GM = FormatoMPGMMock.getInstance().getFormatoMPMS2GM();
        TransactionData result = mapper.mapOut2(formatoMPMS2GM, new TransactionData());
        assertNotNull(result.getData());
        assertNull(result.getData().getTransactionType());
        assertNull(result.getData().getDetail());

    }

    @Test
    public void mapOut2WithTransactionTypeIdInvalidTest() throws IOException {
        FormatoMPMS2GM formatoMPMS2GM = FormatoMPGMMock.getInstance().getFormatoMPMS2GM();
        TransactionData transactionData = new TransactionData();
        transactionData.setData(new Transaction());
        transactionData.getData().setTransactionType(new TransactionType());
        transactionData.getData().getTransactionType().setId("MACGYVER");
        TransactionData result = mapper.mapOut2(formatoMPMS2GM, transactionData);
        assertNotNull(result.getData());
        assertNotNull(result.getData().getTransactionType());
        assertNull(result.getData().getDetail());
    }

    @Test
    public void mapOut2FullTest() throws IOException {
        FormatoMPMS2GM formatoMPMS2GM = FormatoMPGMMock.getInstance().getFormatoMPMS2GM();
        TransactionData transactionData = new TransactionData();
        transactionData.setData(new Transaction());
        transactionData.getData().setTransactionType(new TransactionType());
        transactionData.getData().getTransactionType().setId("PURCHASE");
        TransactionData result = mapper.mapOut2(formatoMPMS2GM, transactionData);

        Purchase purchase = (Purchase) result.getData().getDetail();
        assertNotNull(purchase.getStore().getId());
        assertNotNull(purchase.getStore().getName());
        assertNotNull(purchase.getStore().getCategory().getId());
        assertNotNull(purchase.getStore().getCategory().getName());
        assertNotNull(purchase.getPaymentChannel().getId());
        assertNotNull(purchase.getPaymentChannel().getName());

        assertEquals(purchase.getStore().getId(), formatoMPMS2GM.getIdtiend());
        assertEquals(purchase.getStore().getName(), formatoMPMS2GM.getNomtien());
        assertEquals(purchase.getStore().getCategory().getId(), formatoMPMS2GM.getIdcateg());
        assertEquals(purchase.getStore().getCategory().getName(), formatoMPMS2GM.getNomcat());
        assertEquals(purchase.getPaymentChannel().getId(), formatoMPMS2GM.getTippag());
        assertEquals(purchase.getPaymentChannel().getName(), formatoMPMS2GM.getDespag());
    }

    @Test
    public void mapOut2FormatEmptyTest() {
        TransactionData transactionData = new TransactionData();
        transactionData.setData(new Transaction());
        transactionData.getData().setTransactionType(new TransactionType());
        transactionData.getData().getTransactionType().setId("PURCHASE");
        TransactionData result = mapper.mapOut2(null, transactionData);

        Purchase purchase = (Purchase) result.getData().getDetail();
        assertNull(purchase);
    }

    @Test
    public void mapOut3WithTrasactionDataInstanceTest() throws IOException {
        FormatoMPMS3GM formatoMPMS3GM = FormatoMPGMMock.getInstance().getFormatoMPMS3GM();
        TransactionData result = mapper.mapOut3(formatoMPMS3GM, new TransactionData());
        assertNotNull(result);
        assertNotNull(result.getData());
        assertNull(result.getData().getTransactionType());
        assertNull(result.getData().getDetail());
    }

    @Test
    public void mapOut3WithTransactionTypeIdInvalidTest() throws IOException {
        FormatoMPMS3GM formatoMPMS3GM = FormatoMPGMMock.getInstance().getFormatoMPMS3GM();
        TransactionData transactionData = new TransactionData();
        transactionData.setData(new Transaction());
        transactionData.getData().setTransactionType(new TransactionType());
        transactionData.getData().getTransactionType().setId("MACGYVER");
        TransactionData result = mapper.mapOut3(formatoMPMS3GM, transactionData);
        assertNotNull(result.getData());
        assertNotNull(result.getData().getTransactionType());
        assertNull(result.getData().getDetail());
    }

    @Test
    public void mapOut3FullTest() throws IOException {
        FormatoMPMS3GM formatoMPMS3GM = FormatoMPGMMock.getInstance().getFormatoMPMS3GM();
        TransactionData transactionData = new TransactionData();
        transactionData.setData(new Transaction());
        transactionData.getData().setTransactionType(new TransactionType());
        transactionData.getData().getTransactionType().setId("CASH_WITHDRAWAL");
        Mockito.when(enumMapper.getEnumValue("cards.transaction.originType", formatoMPMS3GM.getOrigret())).thenReturn("BRANCH");
        TransactionData result = mapper.mapOut3(formatoMPMS3GM, transactionData);
        CashWithdrawal cashWithdrawal = (CashWithdrawal) result.getData().getDetail();
        assertNotNull(cashWithdrawal.getAtm().getId());
        assertNotNull(cashWithdrawal.getAtm().getNet().getId());
        assertNotNull(cashWithdrawal.getAtm().getNet().getName());
        assertNotNull(cashWithdrawal.getOriginType());

        assertEquals(cashWithdrawal.getAtm().getId(), formatoMPMS3GM.getIdatm());
        assertEquals(cashWithdrawal.getAtm().getNet().getId(), formatoMPMS3GM.getIdred());
        assertEquals(cashWithdrawal.getAtm().getNet().getName(), formatoMPMS3GM.getTipatm());
        assertEquals("BRANCH", cashWithdrawal.getOriginType());
        assertEquals(cashWithdrawal.getBranch().getId(), formatoMPMS3GM.getIdofic());
        assertEquals(cashWithdrawal.getBranch().getName(), formatoMPMS3GM.getDesofic());
    }

    @Test
    public void mapOut4FullTest() throws IOException {
        Mockito.when(enumMapper.getEnumValue(TxGetCardTransactionV0Mapper.CARDS_TRANSACTION_PERIOD, "01")).thenReturn(PERIODO_01);

        FormatoMPMS4GM input = FormatoMPGMMock.getInstance().getFormatoMPMS4GM();

        TransactionData transaction = new TransactionData();
        transaction.setData(new Transaction());
        transaction.getData().setTransactionType(new TransactionType());
        transaction.getData().getTransactionType().setId(TxGetCardTransactionV0Mapper.FEE_PAYMENT);

        transaction = mapper.mapOut4(input, transaction);
        FeePayment detail = (FeePayment) transaction.getData().getDetail();

        assertNotNull(transaction);
        assertNotNull(detail.getAccumulatedDisposedBalance());
        assertNotNull(detail.getAccumulatedDisposedBalance().getAmount());
        assertNotNull(detail.getAccumulatedDisposedBalance().getCurrency());
        assertNotNull(detail.getAccumulatedDisposedBalance().getPercentage());
        assertNotNull(detail.getAccumulatedDisposedBalance().getPeriod());
        assertNotNull(detail.getFeeExonerationAmount());
        assertNotNull(detail.getFeeExonerationAmount().getAmount());
        assertNotNull(detail.getFeeExonerationAmount().getCurrency());

        assertEquals(input.getImpcons(), detail.getAccumulatedDisposedBalance().getAmount());
        assertEquals(input.getDivcons(), detail.getAccumulatedDisposedBalance().getCurrency());
        assertEquals(input.getPorcons(), detail.getAccumulatedDisposedBalance().getPercentage());
        assertEquals(PERIODO_01, detail.getAccumulatedDisposedBalance().getPeriod());
        assertEquals(input.getImpmeta(), detail.getFeeExonerationAmount().getAmount());
        assertEquals(input.getDivmeta(), detail.getFeeExonerationAmount().getCurrency());
    }
}

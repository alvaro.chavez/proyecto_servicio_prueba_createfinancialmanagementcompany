package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatement;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatementDetail;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.filenet.Contenido;
import com.bbva.pzic.cards.dao.model.filenet.DocumentRequest;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.encrypt.impl.CypherTool;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

/**
 * Created on 27/07/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class GetCardFinancialStatementMapperTest {

    @InjectMocks
    private GetCardFinancialStatementMapper mapper;

    @Mock
    private CypherTool cypherTool;
    @Mock
    private ConfigurationManager configurationManager;
    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        Mockito.when(configurationManager.getProperty("servicing.cards.contractId")).thenReturn(EntityMock.ID_CONTRATO);
        Mockito.when(configurationManager.getProperty("servicing.cards.groupId")).thenReturn(EntityMock.ID_GRUPO);
        Mockito.when(serviceInvocationContext.getUser()).thenReturn(EntityMock.CLIENT_ID);
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_FINANCIAL_STATEMENT)).thenReturn(CARD_ID);
    }

    @Test
    public void mapInFullTest() {
        final InputGetCardFinancialStatement result = mapper.mapIn(CARD_ENCRYPT_ID, FINANCIAL_STATEMENT_ID, MEDIA_TYPE_PDF);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getFinancialStatementId());
        assertNotNull(result.getContentType());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(FINANCIAL_STATEMENT_ID, result.getFinancialStatementId());
        assertEquals(MEDIA_TYPE_PDF, result.getContentType());
    }

    @Test
    public void mapInWithEmptyContentTypesTest() {
        final InputGetCardFinancialStatement result = mapper.mapIn(CARD_ENCRYPT_ID, FINANCIAL_STATEMENT_ID, null);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getFinancialStatementId());
        assertNull(result.getContentType());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(FINANCIAL_STATEMENT_ID, result.getFinancialStatementId());
    }

    @Test
    public void testMapOutGetCardStatementNaturalLifeMiles() throws IOException {
        final DTOIntCardStatement dto = EntityMock.getInstance().getDTOIntCardStatement();
        final DocumentRequest documentResult = mapper.mapOut(dto);

        assertNotNull(documentResult.getContenido());
        assertFalse(documentResult.getContenido().isEmpty());

        final Contenido result = documentResult.getContenido().get(0);

        final DTOIntCardStatementDetail detail = dto.getDetail();

        assertNotNull(result.getProgramId());
        assertNotNull(result.getProgramTypeId());
        assertNotNull(result.getProgramTypeName());
        assertNotNull(result.getProgramCurrentLoyaltyUnitsBalance());
        assertNull(result.getProgramLifetimeAccumulatedLoyaltyUnits());
        assertNotNull(result.getProgramBonus());
        assertEquals(result.getProgramCurrentLoyaltyUnitsBalance(), detail.getProgram().getMonthPoints().toString());
        assertEquals(result.getProgramBonus(), detail.getProgram().getBonus().toString());

        assertEquals(result.getRowCardStatement(), dto.getRowCardStatement());

        genericAsserts(result, detail);

        assertNotNull(result.getMessage());
        assertTrue(result.getMessage().contains(dto.getMessages().get(0)));
        assertTrue(result.getMessage().contains(dto.getMessages().get(1)));

        assertNotNull(documentResult.getIdContrato());
        assertNotNull(documentResult.getIdGrupo());
        assertNotNull(documentResult.getListaClientes());

        assertEquals(EntityMock.ID_CONTRATO, documentResult.getIdContrato());
        assertEquals(EntityMock.ID_GRUPO, documentResult.getIdGrupo());
        assertEquals(EntityMock.CLIENT_ID, documentResult.getListaClientes().get(0).getCodigoCentral());
    }

    @Test
    public void testMapOutGetCardStatementNaturalPuntosVida() throws IOException {
        final DTOIntCardStatement dto = EntityMock.getInstance().getDTOIntCardStatement();
        dto.getDetail().getProgram().setLifeMilesNumber(null);

        final DocumentRequest documentResult = mapper.mapOut(dto);

        assertNotNull(documentResult.getContenido());
        assertFalse(documentResult.getContenido().isEmpty());

        final Contenido result = documentResult.getContenido().get(0);

        final DTOIntCardStatementDetail detail = dto.getDetail();

        assertNull(result.getProgramId());
        assertNotNull(result.getProgramTypeId());
        assertNotNull(result.getProgramTypeName());
        assertNotNull(result.getProgramCurrentLoyaltyUnitsBalance());
        assertNotNull(result.getProgramLifetimeAccumulatedLoyaltyUnits());
        assertNull(result.getProgramBonus());
        assertEquals(result.getProgramCurrentLoyaltyUnitsBalance(), detail.getProgram().getMonthPoints().toString());
        assertEquals(result.getProgramLifetimeAccumulatedLoyaltyUnits(), detail.getProgram().getTotalPoints().toString());

        assertEquals(result.getRowCardStatement(), dto.getRowCardStatement());

        genericAsserts(result, detail);

        assertNotNull(result.getMessage());
        assertTrue(result.getMessage().contains(dto.getMessages().get(0)));
        assertTrue(result.getMessage().contains(dto.getMessages().get(1)));

        assertNotNull(documentResult.getIdContrato());
        assertNotNull(documentResult.getIdGrupo());
        assertNotNull(documentResult.getListaClientes());

        assertEquals(EntityMock.ID_CONTRATO, documentResult.getIdContrato());
        assertEquals(EntityMock.ID_GRUPO, documentResult.getIdGrupo());
        assertEquals(EntityMock.CLIENT_ID, documentResult.getListaClientes().get(0).getCodigoCentral());
    }

    @Test
    public void testMapOutGetCardStatementWithoutUser() throws IOException {
        Mockito.when(serviceInvocationContext.getUser()).thenReturn(null);
        final DTOIntCardStatement dto = EntityMock.getInstance().getDTOIntCardStatement();
        final DocumentRequest documentResult = mapper.mapOut(dto);

        assertNotNull(documentResult.getContenido());
        assertFalse(documentResult.getContenido().isEmpty());

        final Contenido result = documentResult.getContenido().get(0);

        final DTOIntCardStatementDetail detail = dto.getDetail();

        assertNotNull(result.getProgramId());
        assertNotNull(result.getProgramTypeId());
        assertNotNull(result.getProgramTypeName());
        assertNotNull(result.getProgramCurrentLoyaltyUnitsBalance());
        assertNull(result.getProgramLifetimeAccumulatedLoyaltyUnits());
        assertNotNull(result.getProgramBonus());
        assertEquals(result.getProgramCurrentLoyaltyUnitsBalance(), detail.getProgram().getMonthPoints().toString());
        assertEquals(result.getProgramBonus(), detail.getProgram().getBonus().toString());

        assertEquals(result.getRowCardStatement(), dto.getRowCardStatement());

        genericAsserts(result, detail);

        assertNotNull(result.getMessage());
        assertTrue(result.getMessage().contains(dto.getMessages().get(0)));
        assertTrue(result.getMessage().contains(dto.getMessages().get(1)));

        assertNotNull(documentResult.getIdContrato());
        assertNotNull(documentResult.getIdGrupo());
        assertNull(documentResult.getListaClientes());

        assertEquals(EntityMock.ID_CONTRATO, documentResult.getIdContrato());
        assertEquals(EntityMock.ID_GRUPO, documentResult.getIdGrupo());
    }

    @Test
    public void testMapOutGetCardStatementEnterprise() throws IOException {
        final DTOIntCardStatement dto = EntityMock.getInstance().getDTOIntCardStatement();
        dto.getDetail().setParticipantType("E");
        final DocumentRequest documentResult = mapper.mapOut(dto);

        assertNotNull(documentResult.getContenido());
        assertFalse(documentResult.getContenido().isEmpty());

        final Contenido result = documentResult.getContenido().get(0);

        final DTOIntCardStatementDetail detail = dto.getDetail();

        assertNull(result.getProgramId());
        assertNull(result.getProgramTypeId());
        assertNull(result.getProgramTypeName());
        assertNull(result.getProgramCurrentLoyaltyUnitsBalance());
        assertNull(result.getProgramLifetimeAccumulatedLoyaltyUnits());
        assertNull(result.getProgramBonus());

        assertEquals(result.getRowCardStatement(), dto.getRowCardStatement());

        genericAsserts(result, detail);

        assertNotNull(result.getMessage());
        assertTrue(result.getMessage().contains(dto.getMessages().get(0)));
        assertTrue(result.getMessage().contains(dto.getMessages().get(1)));

        assertNotNull(documentResult.getNumeroContrato());
        assertNotNull(documentResult.getIdContrato());
        assertNotNull(documentResult.getIdGrupo());
        assertNotNull(documentResult.getListaClientes());

        assertEquals(dto.getDetail().getAccountNumberPEN(), documentResult.getNumeroContrato());
        assertEquals(EntityMock.ID_CONTRATO, documentResult.getIdContrato());
        assertEquals(EntityMock.ID_GRUPO, documentResult.getIdGrupo());
        assertEquals(EntityMock.CLIENT_ID, documentResult.getListaClientes().get(0).getCodigoCentral());
    }

    private void genericAsserts(Contenido result, DTOIntCardStatementDetail detail) {
        assertEquals(result.getCardId(), detail.getCardId());
        assertEquals(result.getCurrencyId(), detail.getCurrency());

        assertEquals(result.getChargedAccount1Id(), detail.getAccountNumberPEN());
        assertEquals(result.getChargedAccount1Currency(), DTOIntCardStatementDetail.PEN);

        assertEquals(result.getChargedAccount2Id(), detail.getAccountNumberUSD());
        assertEquals(result.getChargedAccount2Currency(), DTOIntCardStatementDetail.USD);

        assertEquals(result.getParticipantType(), detail.getParticipantType());

        assertEquals(result.getFormatsPan(), detail.getFormatsPan());
        assertEquals(result.getProductName(), detail.getProductName());
        assertEquals(result.getBranchName(), detail.getBranchName());

        assertNotNull(result.getParticipant1RelationshipTypeName());
        assertNotNull(result.getParticipant1RelationshipOrder());
        assertEquals(result.getParticipant1Name(), detail.getParticipantNameFirst());

        assertNotNull(result.getParticipant2RelationshipTypeName());
        assertNotNull(result.getParticipant2RelationshipOrder());
        assertEquals(result.getParticipant2Name(), detail.getParticipantNameSecond());

        assertNotNull(result.getEndDate());
        assertEquals(result.getPayTypeName(), detail.getPayType());

        assertNotNull(result.getParticipant1AddresessIsMainAddress());
        assertEquals("TRUE", result.getParticipant1AddresessIsMainAddress());
        assertEquals(result.getParticipant1AddresessName(), detail.getAddress().getName());
        assertEquals(result.getParticipant1AddresessStreetTypeName(), detail.getAddress().getStreetType());
        assertEquals(result.getParticipant1AddresessState(), detail.getAddress().getState());
        assertEquals(result.getParticipant1AddresessUbigeo(), detail.getAddress().getUbigeo());

        assertEquals(result.getCreditLimitValueAmount(), detail.getCreditLimit().toString());
        assertEquals(result.getCreditLimitValueCurrency(), detail.getCurrency());
        assertEquals(result.getLimitsDisposedBalanceAmount(), detail.getLimits().getDisposedBalance().toString());
        assertEquals(result.getLimitsDisposedBalanceCurrency(), detail.getCurrency());
        assertEquals(result.getLimitsAvailableBalanceAmount(), detail.getLimits().getAvailableBalance().toString());
        assertEquals(result.getLimitsAvailableBalanceCurrency(), detail.getCurrency());
        assertEquals(result.getLimitsFinancingDisposedBalanceAmount(), detail.getLimits().getFinancingDisposedBalance().toString());
        assertEquals(result.getLimitsFinancingDisposedBalanceCurrency(), detail.getCurrency());

        assertNotNull(result.getPaymentDate());

        assertNotNull(result.getInterestRates1TypeId());
        assertNotNull(result.getInterestRates1TypeName());
        assertEquals(result.getInterestRates1Percentage(), detail.getInterest().getPurchasePEN().toString());

        assertNotNull(result.getInterestRates2TypeId());
        assertNotNull(result.getInterestRates2TypeName());
        assertEquals(result.getInterestRates2Percentage(), detail.getInterest().getPurchaseUSD().toString());

        assertNotNull(result.getInterestRates3TypeId());
        assertNotNull(result.getInterestRates3TypeName());
        assertEquals(result.getInterestRates3Percentage(), detail.getInterest().getAdvancedPEN().toString());

        assertNotNull(result.getInterestRates4TypeId());
        assertNotNull(result.getInterestRates4TypeName());
        assertEquals(result.getInterestRates4Percentage(), detail.getInterest().getAdvancedUSD().toString());

        assertNotNull(result.getInterestRates5TypeId());
        assertNotNull(result.getInterestRates5TypeName());
        assertEquals(result.getInterestRates5Percentage(), detail.getInterest().getCountervailingPEN().toString());

        assertNotNull(result.getInterestRates6TypeId());
        assertNotNull(result.getInterestRates6TypeName());
        assertEquals(result.getInterestRates6Percentage(), detail.getInterest().getCountervailingUSD().toString());

        assertNotNull(result.getInterestRates7TypeId());
        assertNotNull(result.getInterestRates7TypeName());
        assertEquals(result.getInterestRates7Percentage(), detail.getInterest().getArrearsPEN().toString());

        assertNotNull(result.getInterestRates8TypeId());
        assertNotNull(result.getInterestRates8TypeName());
        assertEquals(result.getInterestRates8Percentage(), detail.getInterest().getArrearsUSD().toString());

        //assertNull(result.getPendingPayAmount());

        assertEquals(result.getDisposedBalanceLocalCurrencyCurrency(), DTOIntCardStatementDetail.PEN);
        assertEquals(result.getDisposedBalanceLocalCurrencyAmount(), detail.getDisposedBalanceLocalCurrency().getAmount().toString());
        assertEquals(result.getDisposedBalanceCurrency(), DTOIntCardStatementDetail.USD);
        assertEquals(result.getDisposedBalanceAmount(), detail.getDisposedBalance().getAmount().toString());

        assertEquals(result.getTotalTransactions(), detail.getTotalTransactions().toString());

        //assertNull(result.getCurrentBalance());

        assertEquals(result.getOutstandingBalanceLocalCurrencyCurrency(), DTOIntCardStatementDetail.PEN);
        assertEquals(result.getOutstandingBalanceLocalCurrencyAmount(), detail.getExtraPayments().getOutstandingBalanceLocalCurrency().getAmount().toString());
        assertEquals(result.getMinimunPaymentMinimumCapitalLocalCurrencyCurrency(), DTOIntCardStatementDetail.PEN);
        assertEquals(result.getMinimunPaymentMinimumCapitalLocalCurrencyAmount(), detail.getExtraPayments().getMinimumCapitalLocalCurrency().getAmount().toString());
        assertEquals(result.getInterestsBalanceLocalCurrencyCurrency(), DTOIntCardStatementDetail.PEN);
        assertEquals(result.getInterestsBalanceLocalCurrencyAmount(), detail.getExtraPayments().getInterestsBalanceLocalCurrency().getAmount().toString());
        assertEquals(result.getFeesBalanceLocalCurrencyCurrency(), DTOIntCardStatementDetail.PEN);
        assertEquals(result.getFeesBalanceLocalCurrencyAmount(), detail.getExtraPayments().getFeesBalanceLocalCurrency().getAmount().toString());
        assertEquals(result.getFinancingTransactionLocalBalanceCurrency(), DTOIntCardStatementDetail.PEN);
        assertEquals(result.getFinancingTransactionLocalBalanceAmount(), detail.getExtraPayments().getFinancingTransactionLocalBalance().getAmount().toString());
        assertEquals(result.getMinimumPaymentInvoiceMinimumAmountLocalCurrencyCurrency(), DTOIntCardStatementDetail.PEN);
        assertEquals(result.getMinimumPaymentInvoiceMinimumAmountLocalCurrencyAmount(), detail.getExtraPayments().getInvoiceMinimumAmountLocalCurrency().getAmount().toString());
        assertEquals(result.getInvoiceAmountLocalCurrencyCurrency(), DTOIntCardStatementDetail.PEN);
        assertEquals(result.getInvoiceAmountLocalCurrencyAmount(), detail.getExtraPayments().getInvoiceAmountLocalCurrency().getAmount().toString());

        assertEquals(result.getOutstandingBalanceCurrency(), DTOIntCardStatementDetail.USD);
        assertEquals(result.getOutstandingBalanceAmount(), detail.getExtraPayments().getOutstandingBalance().getAmount().toString());
        assertEquals(result.getMinimunPaymentMinimumCapitalCurrencyCurrency(), DTOIntCardStatementDetail.USD);
        assertEquals(result.getMinimunPaymentMinimumCapitalCurrencyAmount(), detail.getExtraPayments().getMinimumCapitalCurrency().getAmount().toString());
        assertEquals(result.getInterestsBalanceCurrency(), DTOIntCardStatementDetail.USD);
        assertEquals(result.getInterestsBalanceAmount(), detail.getExtraPayments().getInterestsBalance().getAmount().toString());
        assertEquals(result.getFeesBalanceCurrency(), DTOIntCardStatementDetail.USD);
        assertEquals(result.getFeesBalanceAmount(), detail.getExtraPayments().getFeesBalance().getAmount().toString());
        assertEquals(result.getFinancingTransactionBalanceCurrency(), DTOIntCardStatementDetail.USD);
        assertEquals(result.getFinancingTransactionBalanceAmount(), detail.getExtraPayments().getFinancingTransactionBalance().getAmount().toString());
        assertEquals(result.getMinimumPaymentInvoiceMinimumAmountCurrency(), DTOIntCardStatementDetail.USD);
        assertEquals(result.getMinimumPaymentInvoiceMinimumAmountAmount(), detail.getExtraPayments().getInvoiceMinimumAmountCurrency().getAmount().toString());
        assertEquals(result.getInvoiceAmountCurrency(), DTOIntCardStatementDetail.USD);
        assertEquals(result.getInvoiceAmountAmount(), detail.getExtraPayments().getInvoiceAmount().getAmount().toString());

        assertEquals(result.getTotalDebtLocalCurrencyCurrency(), DTOIntCardStatementDetail.PEN);
        assertEquals(result.getTotalDebtLocalCurrencyAmount(), detail.getTotalDebtLocalCurrency().getAmount().toString());
        assertEquals(result.getTotalDebtCurrency(), DTOIntCardStatementDetail.USD);
        assertEquals(result.getTotalDebtAmount(), detail.getTotalDebt().getAmount().toString());

        assertEquals(result.getTotalMonthsAmortizationMinimum(), detail.getTotalMonthsAmortizationMinimum().toString());
        assertEquals(result.getTotalMonthsAmortizationMinimumFull(), detail.getTotalMonthsAmortizationMinimumFull().toString());
    }

    @Test
    public void testMapOutEmpty() {
        final DocumentRequest result = mapper.mapOut(new DTOIntCardStatement());
        assertNull(result);
    }

}

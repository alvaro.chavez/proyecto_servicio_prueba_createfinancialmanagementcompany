package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.business.dto.DTOIntMembershipServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntSearchCriteria;
import com.bbva.pzic.cards.dao.model.mppf.FormatoMPM0PF;
import com.bbva.pzic.cards.dao.model.mppf.FormatoMPM0PS;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardMembershipMapper;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created on 17/10/2017.
 *
 * @author Entelgy
 */

@RunWith(MockitoJUnitRunner.class)
public class TxGetCardMembershipMapperTest {

    @InjectMocks
    private ITxGetCardMembershipMapper mapper = new TxGetCardMembershipMapper();

    @Mock
    private EnumMapper enumMapper;

    private DummyMock dummyMock;


    @Before
    public void setUp() {
        dummyMock = new DummyMock();
    }

    @Test
    public void mapInputFullTest() {
        final DTOIntSearchCriteria dtoIntSearchCriteria = new DTOIntSearchCriteria();
        dtoIntSearchCriteria.setCardId(CARD_ID);
        dtoIntSearchCriteria.setMembershipId("1");
        final FormatoMPM0PF formatoMPM0PF = mapper.mapInput(dtoIntSearchCriteria);
        assertNotNull(formatoMPM0PF);
        assertNotNull(formatoMPM0PF.getNtarjlf());
        assertNotNull(formatoMPM0PF.getIdpremi());


        assertEquals(CARD_ID, formatoMPM0PF.getNtarjlf());
        assertEquals("1", formatoMPM0PF.getIdpremi());
    }

    @Test
    public void mapOutputFullTest() {
        final DTOIntSearchCriteria dtoIntSearchCriteria = new DTOIntSearchCriteria();
        dtoIntSearchCriteria.setCardId(CARD_ID);
        dtoIntSearchCriteria.setMembershipId("1");
        final FormatoMPM0PS formatOutput = new FormatoMPM0PS();
        DTOIntMembershipServiceResponse dtoIntMembershipServiceResponse = new DTOIntMembershipServiceResponse();
        dtoIntMembershipServiceResponse = mapper.mapOutput(formatOutput, dtoIntSearchCriteria);

        assertNotNull(dtoIntMembershipServiceResponse);
        assertNotNull(dtoIntMembershipServiceResponse.getData());
    }

}
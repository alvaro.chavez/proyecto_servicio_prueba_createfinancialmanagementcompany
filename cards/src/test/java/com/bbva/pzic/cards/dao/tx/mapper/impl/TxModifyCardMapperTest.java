package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.dao.model.mpb5.FormatoMPM0B5E;
import com.bbva.pzic.cards.dao.tx.mapper.ITxModifyCardMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class TxModifyCardMapperTest {

    private ITxModifyCardMapper txModifyCard;

    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void init() {
        txModifyCard = new TxModifyCardMapper();
    }

    @Test
    public void testMapInput() throws IOException {
        final DTOIntCard dtoIn = entityMock.buildDTOIntCard();

        FormatoMPM0B5E formatoMPM0B5E = txModifyCard.mapInput(dtoIn);
        assertNotNull(formatoMPM0B5E.getIdetarj());
        assertNotNull(formatoMPM0B5E.getEsttarj());
        assertNotNull(formatoMPM0B5E.getCodraz());

        assertEquals(dtoIn.getCardId(), formatoMPM0B5E.getIdetarj());
        assertEquals(dtoIn.getStatusId(), formatoMPM0B5E.getEsttarj());
        assertEquals(dtoIn.getStatusReasonId(), formatoMPM0B5E.getCodraz());
    }

    @Test
    public void testMapInputWithoutStatusId() throws IOException {
        final DTOIntCard dtoIn = entityMock.buildDTOIntCard();
        dtoIn.setStatusId(null);

        FormatoMPM0B5E formatoMPM0B5E = txModifyCard.mapInput(dtoIn);
        assertNotNull(formatoMPM0B5E.getIdetarj());
        assertNull(formatoMPM0B5E.getEsttarj());
        assertNotNull(formatoMPM0B5E.getCodraz());

        assertEquals(dtoIn.getCardId(), formatoMPM0B5E.getIdetarj());
        assertEquals(dtoIn.getStatusReasonId(), formatoMPM0B5E.getCodraz());
    }

    @Test
    public void testMapInputWithoutStatusReasonId() throws IOException {
        final DTOIntCard dtoIn = entityMock.buildDTOIntCard();
        dtoIn.setStatusReasonId(null);

        FormatoMPM0B5E formatoMPM0B5E = txModifyCard.mapInput(dtoIn);
        assertNotNull(formatoMPM0B5E.getIdetarj());
        assertNotNull(formatoMPM0B5E.getEsttarj());
        assertNull(formatoMPM0B5E.getCodraz());

        assertEquals(dtoIn.getCardId(), formatoMPM0B5E.getIdetarj());
        assertEquals(dtoIn.getStatusId(), formatoMPM0B5E.getEsttarj());
    }

    @Test
    public void testMapInputEmpty() {
        FormatoMPM0B5E formatoMPM0B5E = txModifyCard.mapInput(new DTOIntCard());
        assertNull(formatoMPM0B5E.getIdetarj());
        assertNull(formatoMPM0B5E.getEsttarj());
        assertNull(formatoMPM0B5E.getCodraz());
    }
}
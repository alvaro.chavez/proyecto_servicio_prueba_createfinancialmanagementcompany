package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCard;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.encrypt.impl.CypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

/**
 * Created on 30/01/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateCardMapperTest {

    @InjectMocks
    private CreateCardMapper mapper;

    @Mock
    private Translator translator;

    @Mock
    private CypherTool cypherTool;

    private EntityMock entityMock = EntityMock.getInstance();

    private void mapInEnumMapper() {
        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.cardType.id", "DEBIT_CARD")).thenReturn("D");
        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.physicalSupport.id", "STICKER")).thenReturn("P");
        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.statementType", "PHYSICAL")).thenReturn("S");
        Mockito.when(translator.translateFrontendEnumValueStrictly("accounts.delivery.deliveryType", "PHYSICAL")).thenReturn("F");

        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.deliveriesManagement.id", "STATEMENT")).thenReturn("001");
        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.deliveriesManagement.id", "STICKER")).thenReturn("002");

        Mockito.when(cypherTool.decrypt(CARD_RELATED_CONTRACT_ENCRYPT, AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V0)).thenReturn(CARD_RELATED_CONTRACT_KEY_TESTED);
        Mockito.when(cypherTool.decrypt(CARD_RELATED_CONTRACT_ENCRYPT, AbstractCypherTool.NUCOREL, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARD_V02)).thenReturn(CARD_RELATED_CONTRACT_KEY_TESTED);
    }

    @Test
    public void mapInFullMPW1Test() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().getDestination().setId("HOME");
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getParticipantId());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());
        assertNotNull(result.getDtoIntCard().getSupportContractType());
        assertNotNull(result.getDtoIntCard().getDestinationIdHome());
        assertNull(result.getDtoIntCard().getDestinationIdBranch());
        assertNotNull(result.getDtoIntCard().getDeliveriesManagementServiceTypeIdStatement());
        assertNotNull(result.getDtoIntCard().getDeliveriesManagementContactDetailIdStatement());
        assertNotNull(result.getDtoIntCard().getDeliveriesManagementAddressIdStatement());
        assertNotNull(result.getDtoIntCard().getContractingBussinesAgentId());
        assertNotNull(result.getDtoIntCard().getContractingBussinesAgentOriginId());
        assertNotNull(result.getDtoIntCard().getMarketBusinessAgentId());
        assertNotNull(result.getDtoIntCard().getImageId());
        assertNotNull(result.getDtoIntCard().getOfferId());
        assertNotNull(result.getDtoIntCard().getManagementBranchId());
        assertNotNull(result.getDtoIntCard().getContractingBranchId());

        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());
        assertNull(result.getDtoIntCard().getNumber());

        assertEquals(input.getParticipants().get(1).getParticipantId(), result.getDtoIntCard().getParticipantId());
        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getDelivery().getAddress().getId(), result.getDtoIntCard().getDestinationIdHome());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
        assertEquals("F", result.getDtoIntCard().getSupportContractType());
        assertEquals("001", result.getDtoIntCard().getDeliveriesManagementServiceTypeIdStatement());
        assertEquals(input.getDeliveriesManagement().get(0).getContactDetail().getId(), result.getDtoIntCard().getDeliveriesManagementContactDetailIdStatement());
        assertEquals(input.getDeliveriesManagement().get(0).getAddress().getId(), result.getDtoIntCard().getDeliveriesManagementAddressIdStatement());
        assertEquals(input.getContractingBusinessAgent().getId(), result.getDtoIntCard().getContractingBussinesAgentId());
        assertEquals(input.getContractingBusinessAgent().getOriginId(), result.getDtoIntCard().getContractingBussinesAgentOriginId());
        assertEquals(input.getMarketBusinessAgent().getId(), result.getDtoIntCard().getMarketBusinessAgentId());
        assertEquals(input.getImage().getId(), result.getDtoIntCard().getImageId());
        assertEquals(input.getOfferId(), result.getDtoIntCard().getOfferId());
        assertEquals(input.getManagementBranch().getId(), result.getDtoIntCard().getManagementBranchId());
        assertEquals(input.getContractingBranch().getId(), result.getDtoIntCard().getContractingBranchId());
        assertEquals("002", result.getDtoIntCard().getDeliveryAdressConstant());
    }

    @Test
    public void mapInMPW1WithoutContactDetailTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().getDestination().setId("HOME");
        input.getDeliveriesManagement().get(0).setContactDetail(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard().getDeliveriesManagementServiceTypeIdStatement());
        assertNull(result.getDtoIntCard().getDeliveriesManagementContactDetailIdStatement());
        assertNotNull(result.getDtoIntCard().getDeliveriesManagementAddressIdStatement());

        assertEquals("001", result.getDtoIntCard().getDeliveriesManagementServiceTypeIdStatement());
        assertEquals(input.getDeliveriesManagement().get(0).getAddress().getId(), result.getDtoIntCard().getDeliveriesManagementAddressIdStatement());
    }

    @Test
    public void mapInMPW1WithoutAddressTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().getDestination().setId("HOME");
        input.getDeliveriesManagement().get(0).setAddress(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard().getDeliveriesManagementServiceTypeIdStatement());
        assertNotNull(result.getDtoIntCard().getDeliveriesManagementContactDetailIdStatement());
        assertNull(result.getDtoIntCard().getDeliveriesManagementAddressIdStatement());

        assertEquals("001", result.getDtoIntCard().getDeliveriesManagementServiceTypeIdStatement());
        assertEquals(input.getDeliveriesManagement().get(0).getContactDetail().getId(), result.getDtoIntCard().getDeliveriesManagementContactDetailIdStatement());
    }

    @Test
    public void mapInMPW1DeliveriesManagementStickerTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().getDestination().setId("HOME");
        input.getDeliveriesManagement().get(0).getServiceType().setId(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);

        assertEquals("002", result.getDtoIntCard().getDeliveriesManagementServiceTypeIdSticker());
        assertEquals(input.getDeliveriesManagement().get(1).getAddress().getId(), result.getDtoIntCard().getDeliveriesManagementAddressIdSticker());

    }

    @Test
    public void mapInMPW1DeliveriesManagementSMSCodeTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().getDestination().setId("HOME");
        input.getDeliveriesManagement().get(0).getServiceType().setId(null);
        input.getDeliveriesManagement().get(1).getServiceType().setId(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);

        assertEquals(input.getDeliveriesManagement().get(2).getContactDetail().getId(), result.getDtoIntCard().getDeliveriesManagementContactDetailIdSmsCode());
        assertEquals("S", result.getDtoIntCard().getDeliveryManagementConstant());

    }

    @Test
    public void mapInFullMPW1BranchTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNull(result.getDtoIntCard().getDestinationIdHome());
        assertNotNull(result.getDtoIntCard().getDestinationIdBranch());

        assertEquals(input.getDeliveriesManagement().get(0).getContactDetail().getId(), result.getDtoIntCard().getDeliveriesManagementContactDetailIdStatement());
        assertEquals(input.getDeliveriesManagement().get(0).getAddress().getId(), result.getDtoIntCard().getDestinationIdBranch());
    }

    @Test
    public void mapInMPRTFullTest() throws IOException {
        Card input = entityMock.getCardV0();
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getNumber());
        assertNotNull(result.getDtoIntCard().getParticipantId());
        assertNotNull(result.getDtoIntCard().getDeliveriesManagementContactDetailIdSmsCode());

        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getNumber(), result.getDtoIntCard().getNumber());
        assertEquals(input.getParticipants().get(0).getParticipantId(), result.getDtoIntCard().getParticipantId());
        assertEquals(input.getDeliveriesManagement().get(2).getContactDetail().getId(), result.getDtoIntCard().getDeliveriesManagementContactDetailIdSmsCode());
    }

    @Test
    public void mapInMPRTWithRenewalDueToDamageTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getRelatedContracts().get(0).getRelationType().setId("RENEWAL_DUE_TO_DAMAGE");
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdRenewalDueTo());
        assertNotNull(result.getDtoIntCard().getNumber());
        assertNotNull(result.getDtoIntCard().getParticipantId());
        assertNotNull(result.getDtoIntCard().getDeliveriesManagementContactDetailIdSmsCode());

        assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getDtoIntCard().getRelatedContractIdRenewalDueTo());
        assertEquals(input.getNumber(), result.getDtoIntCard().getNumber());
        assertEquals(input.getParticipants().get(0).getParticipantId(), result.getDtoIntCard().getParticipantId());
        assertEquals(input.getDeliveriesManagement().get(2).getContactDetail().getId(), result.getDtoIntCard().getDeliveriesManagementContactDetailIdSmsCode());
    }


    @Test
    public void mapInWithoutSupporContractTypeTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setSupportContractType(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNull(result.getDtoIntCard().getSupportContractType());
    }

    @Test
    public void mapIntWithoutParticipants() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setParticipants(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNull(result.getDtoIntCard().getParticipantId());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());

        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }

    @Test
    public void mapIntWithoutParticipantId() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getParticipants().get(0).setParticipantId(null);
        input.getParticipants().get(1).setParticipantId(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNull(result.getDtoIntCard().getParticipantId());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());

        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }


    @Test
    public void mapInWithoutCardTypeIdTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getCardType().setId(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());

        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }


    @Test
    public void mapInWithoutPhysicalSupportIdTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getPhysicalSupport().setId(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());

        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutRelatedContractNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setRelatedContracts(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());

        assertNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutRelatedContractEmptyTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setRelatedContracts(new ArrayList<RelatedContract>());
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());

        assertNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutRelatedContractRelationTypeNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getRelatedContracts().get(0).setRelationType(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());

        assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getDtoIntCard().getRelatedContractIdExpedition());
        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }


    @Test
    public void mapInWithoutRelatedContractRelationTypeIdExpeditionDueTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getRelatedContracts().get(0).getRelationType().setId("EXPEDITION_DUE_TO_ADDITIONAL");
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdExpedition());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());

        assertNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getDtoIntCard().getRelatedContractIdExpedition());
        assertEquals(STATEMENT_TYPE, result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutRelatedContractRelationTypeIdOtherTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getRelatedContracts().get(0).getRelationType().setId("OTHER");
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(CARD_RELATED_CONTRACT_KEY_TESTED, result.getDtoIntCard().getRelatedContractIdExpedition());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutDeliverDestinationIdBranchTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getDelivery().getDestination().setId("BRANCH");
        input.setNumber(null);

        InputCreateCard result = mapper.mapIn(input);
        assertNotNull(result);

        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());
        assertNotNull(result.getDtoIntCard().getDestinationIdBranch());

        assertNull(result.getDtoIntCard().getDestinationIdHome());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getDelivery().getAddress().getId(), result.getDtoIntCard().getDestinationIdBranch());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());

    }

    @Test
    public void mapInWithoutPaymentMethodEndDateTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getPaymentMethod().setEndDay(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());

        assertNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());

    }

    @Test
    public void mapInWithoutCutoffDayTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setCutOffDay(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());

        assertNull(result.getDtoIntCard().getCutOffDay());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
    }


    @Test
    public void mapInWithoutMembershipNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setMemberships(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());

        assertNull(result.getDtoIntCard().getMembershipsNumber());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }

    @Test
    public void mapInWithoutMembershipEmptyTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.setMemberships(new ArrayList<Membership>());
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());

        assertNull(result.getDtoIntCard().getMembershipsNumber());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }


    @Test
    public void mapInWithoutGrantedCreditTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setGrantedCredits(null);
        input.setNumber(null);
        InputCreateCard result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());

        assertNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getDelivery().getDestination().getId(), result.getDtoIntCard().getDeliveryDestinationId());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }


    @Test
    public void mapInWithoutDestinationTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setNumber(null);
        input.getDelivery().setDestination(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
        assertNotNull(result.getDtoIntCard().getCardTypeId());
        assertNotNull(result.getDtoIntCard().getPhysicalSupportId());
        assertNotNull(result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertNotNull(result.getDtoIntCard().getTitleId());
        assertNotNull(result.getDtoIntCard().getCutOffDay());
        assertNotNull(result.getDtoIntCard().getPaymentMethodEndDay());
        assertNotNull(result.getDtoIntCard().getStatementType());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsAmount());
        assertNotNull(result.getDtoIntCard().getGrantedCreditsCurrency());
        assertNotNull(result.getDtoIntCard().getMembershipsNumber());

        assertNull(result.getDtoIntCard().getDestinationIdHome());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpeditionConstant());
        assertNull(result.getDtoIntCard().getRelatedContractIdExpedition());
        assertNull(result.getDtoIntCard().getDestinationIdBranch());

        assertEquals("D", result.getDtoIntCard().getCardTypeId());
        assertEquals("P", result.getDtoIntCard().getPhysicalSupportId());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getDtoIntCard().getRelatedContractIdLinkedWith());
        assertEquals(input.getTitle().getId(), result.getDtoIntCard().getTitleId());
        assertEquals("S", result.getDtoIntCard().getStatementType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getDtoIntCard().getGrantedCreditsAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDtoIntCard().getGrantedCreditsCurrency());
        assertEquals(input.getMemberships().get(0).getNumber(), result.getDtoIntCard().getMembershipsNumber());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDtoIntCard().getPaymentMethodEndDay());
        assertEquals(input.getCutOffDay(), result.getDtoIntCard().getCutOffDay());
    }


    @Test
    public void mapInCardEmptyTest() {
        InputCreateCard result = mapper.mapIn(new Card());
        assertNotNull(result);
        assertNotNull(result.getDtoIntCard());
    }

    @Test
    public void mapOutEmptyTest() {
        CardData result = mapper.mapOut(new Card());
        assertNotNull(result);
        assertNotNull(result.getData());

    }

    @Test
    public void mapOutNullTest() {
        CardData result = mapper.mapOut(null);
        assertNull(result);
    }

    @Test
    public void mapInMPRTRelatedContractNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setRelatedContracts(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInMPRTRelatedContractEmptyTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setRelatedContracts(new ArrayList<RelatedContract>());
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInMPRTRelatedContractRelationTypeNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getRelatedContracts().get(0).setRelationType(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInMPRTDeliveriesManagementNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setDeliveriesManagement(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInMPRTDeliveriesManagementEmptyTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.setDeliveriesManagement(new ArrayList<DeliveriesManagement>());
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInMPRTDeliveriesManagementServiceTypeIdNullTest() throws IOException {
        mapInEnumMapper();

        Card input = entityMock.getCardV0();
        input.getDeliveriesManagement().get(0).setServiceType(null);
        InputCreateCard result = mapper.mapIn(input);

        assertNotNull(result);
    }

}

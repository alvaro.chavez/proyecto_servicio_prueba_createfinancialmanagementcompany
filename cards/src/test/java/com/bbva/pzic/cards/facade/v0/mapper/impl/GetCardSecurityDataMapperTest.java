package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.canonic.SecurityData;
import com.bbva.pzic.cards.canonic.SecurityDataArray;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA;

@RunWith(MockitoJUnitRunner.class)
public class GetCardSecurityDataMapperTest {

    @InjectMocks
    private GetCardSecurityDataMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock mock = EntityMock.getInstance();

    @Test
    public void mapInFullTest() {
        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_GET_CARD_SECURITY_DATA))
                .thenReturn("1234567890");
        InputGetCardSecurityData result = mapper.mapIn(EntityMock.CARD_ID, EntityMock.PUBLIC_KEY);

        assertNotNull(result.getCardId());
        assertEquals(EntityMock.CARD_ID, result.getCardId());
    }

    @Test
    public void mapOutFullTest() {
        SecurityData entity = mock.getSecurityData();
        SecurityDataArray result = mapper.mapOut(entity);

        assertNotNull(result.getData().get(0).getId());
        assertNotNull(result.getData().get(0).getName());
        assertNotNull(result.getData().get(0).getCode());

        assertEquals(entity.getId(), result.getData().get(0).getId());
        assertEquals(entity.getName(), result.getData().get(0).getName());
        assertEquals(entity.getCode(), result.getData().get(0).getCode());
    }

    @Test
    public void mapOutInitializedTest() {
        SecurityDataArray result = mapper.mapOut(new SecurityData());

        assertNull(result.getData().get(0).getId());
        assertNull(result.getData().get(0).getName());
        assertNull(result.getData().get(0).getCode());
    }

    @Test
    public void mapOutNullTest() {
        SecurityDataArray result = mapper.mapOut(null);

        assertNull(result);
    }
}
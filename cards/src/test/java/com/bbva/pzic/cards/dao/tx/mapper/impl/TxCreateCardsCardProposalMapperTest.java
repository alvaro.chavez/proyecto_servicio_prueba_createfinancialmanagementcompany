package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardsCardProposal;
import com.bbva.pzic.cards.canonic.CardProposal;
import com.bbva.pzic.cards.dao.model.kb93.FormatoKTECKB93;
import com.bbva.pzic.cards.dao.model.kb93.FormatoKTSKB931;
import com.bbva.pzic.cards.dao.model.kb93.mock.FormatsKb93Mock;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxCreateCardsCardProposalMapperTest {

    @InjectMocks
    private TxCreateCardsCardProposalMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    private DummyMock dummyMock;

    @Before
    public void setUp() {
        dummyMock = new DummyMock();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputCreateCardsCardProposal input = EntityMock.getInstance()
                .getInputCreateCardsCardProposal();
        FormatoKTECKB93 result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNotNull(result.getIdsprod());
        Assert.assertNotNull(result.getIdtipta());
        Assert.assertNotNull(result.getTipdoc());
        Assert.assertNotNull(result.getNumdoc());
        Assert.assertNotNull(result.getNombre1());
        Assert.assertNotNull(result.getNombre2());
        Assert.assertNotNull(result.getApelli1());
        Assert.assertNotNull(result.getApelli2());
        Assert.assertNotNull(result.getIdprofe());
        Assert.assertNotNull(result.getIdestci());
        Assert.assertNotNull(result.getIdvinc());
        Assert.assertNotNull(result.getValcon1());
        Assert.assertNotNull(result.getTipcon1());
        Assert.assertNotNull(result.getValcon2());
        Assert.assertNotNull(result.getTipcon2());
        Assert.assertNotNull(result.getMonline());
        Assert.assertNotNull(result.getDivisal());
        Assert.assertNotNull(result.getIddesti());
        Assert.assertNotNull(result.getIdbanco());
        Assert.assertNotNull(result.getIdofici());
        Assert.assertNotNull(result.getIdofert());
        Assert.assertEquals(input.getCardId(), result.getNumtarj());
        Assert.assertEquals(input.getCardProposal().getTitle().getId(),
                result.getIdsprod());
        Assert.assertEquals(input.getCardProposal().getCardType().getId(),
                result.getIdtipta());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                        .getIdentityDocument().getDocumentType().getId(),
                result.getTipdoc());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber(), result.getNumdoc());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                .getFirstName(), result.getNombre1());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                .getMiddleName(), result.getNombre2());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                .getLastName(), result.getApelli1());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                .getSecondLastName(), result.getApelli2());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                .getProfession().getId(), result.getIdprofe());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                .getMaritalStatus().getId(), result.getIdestci());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                .getRelationType().getId(), result.getIdvinc());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                        .getContactDetails().get(0).getContactValue(),
                result.getValcon1());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                        .getContactDetails().get(0).getContactType().getId(),
                result.getTipcon1());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                        .getContactDetails().get(1).getContactValue(),
                result.getValcon2());
        Assert.assertEquals(input.getCardProposal().getParticipant()
                        .getContactDetails().get(1).getContactType().getId(),
                result.getTipcon2());
        Assert.assertEquals(input.getCardProposal().getGrantedCredits().get(0)
                .getAmount(), result.getMonline());
        Assert.assertEquals(input.getCardProposal().getGrantedCredits().get(0)
                .getCurrency(), result.getDivisal());
        Assert.assertEquals(input.getCardProposal().getDelivery()
                .getDestination().getId(), result.getIddesti());
        Assert.assertEquals(input.getCardProposal().getBank().getId(),
                result.getIdbanco());
        Assert.assertEquals(input.getCardProposal().getBank().getBranch()
                .getId(), result.getIdofici());
        Assert.assertEquals(input.getCardProposal().getOfferId(),
                result.getIdofert());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoKTECKB93 result = mapper
                .mapIn(new InputCreateCardsCardProposal());
        Assert.assertNotNull(result);
        Assert.assertNull(result.getNumtarj());
        Assert.assertNull(result.getIdsprod());
        Assert.assertNull(result.getIdtipta());
        Assert.assertNull(result.getTipdoc());
        Assert.assertNull(result.getNumdoc());
        Assert.assertNull(result.getNombre1());
        Assert.assertNull(result.getNombre2());
        Assert.assertNull(result.getApelli1());
        Assert.assertNull(result.getApelli2());
        Assert.assertNull(result.getIdprofe());
        Assert.assertNull(result.getIdestci());
        Assert.assertNull(result.getIdvinc());
        Assert.assertNull(result.getValcon1());
        Assert.assertNull(result.getTipcon1());
        Assert.assertNull(result.getValcon2());
        Assert.assertNull(result.getTipcon2());
        Assert.assertNull(result.getMonline());
        Assert.assertNull(result.getDivisal());
        Assert.assertNull(result.getIddesti());
        Assert.assertNull(result.getIdbanco());
        Assert.assertNull(result.getIdofici());
        Assert.assertNull(result.getIdofert());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoKTSKB931 format = FormatsKb93Mock.getInstance()
                .getFormatoKTSKB931();

        Mockito.when(enumMapper.getEnumValue("cards.cardType.id", EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND)).thenReturn(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_ENUM);
        Mockito.when(enumMapper.getEnumValue("cards.delivery.destination.id", EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND)).thenReturn(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_ENUM);
        Mockito.when(enumMapper.getEnumValue("cards.identityDocument.documentType.id", EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND)).thenReturn(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_ENUM);

        CardProposal result = mapper.mapOut(format);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getId());
        Assert.assertNotNull(result.getTitle().getId());
        Assert.assertNotNull(result.getTitle().getName());
        Assert.assertNotNull(result.getCardType().getId());
        Assert.assertNotNull(result.getCardType().getName());
        Assert.assertNotNull(result.getBank().getId());
        Assert.assertNotNull(result.getBank().getName());
        Assert.assertNotNull(result.getBank().getBranch().getId());
        Assert.assertNotNull(result.getBank().getBranch().getName());
        Assert.assertNotNull(result.getDelivery().getDestination().getId());
        Assert.assertNotNull(result.getDelivery().getDestination().getName());
        Assert.assertNotNull(result.getParticipant().getIdentityDocument()
                .getDocumentType().getId());
        Assert.assertNotNull(result.getParticipant().getIdentityDocument()
                .getDocumentType().getDescription());
        Assert.assertNotNull(result.getParticipant().getIdentityDocument()
                .getNumber());
        Assert.assertNotNull(result.getParticipant().getFirstName());
        Assert.assertNotNull(result.getParticipant().getMiddleName());
        Assert.assertNotNull(result.getParticipant().getLastName());
        Assert.assertNotNull(result.getParticipant().getSecondLastName());
        Assert.assertNotNull(result.getParticipant().getProfession().getId());
        Assert.assertNotNull(result.getParticipant().getProfession().getName());
        Assert.assertNotNull(result.getParticipant().getMaritalStatus().getId());
        Assert.assertNotNull(result.getParticipant().getMaritalStatus()
                .getName());
        Assert.assertNotNull(result.getParticipant().getRelationType().getId());
        Assert.assertNotNull(result.getParticipant().getRelationType()
                .getName());

        Assert.assertNotNull(result.getGrantedCredits());
        Assert.assertFalse(result.getGrantedCredits().isEmpty());
        Assert.assertNotNull(result.getGrantedCredits().get(0).getAmount());
        Assert.assertNotNull(result.getGrantedCredits().get(0).getCurrency());

        Assert.assertNotNull(result.getOperationDate());

        Assert.assertEquals(format.getIdsolic(), result.getId());
        Assert.assertEquals(format.getIdprodu(), result.getTitle().getId());
        Assert.assertEquals(format.getDesprod(), result.getTitle().getName());
        Assert.assertEquals(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_ENUM, result.getCardType().getId());
        Assert.assertEquals(format.getDescta(), result.getCardType().getName());
        Assert.assertEquals(format.getIdbanco(), result.getBank().getId());
        Assert.assertEquals(format.getDesbanc(), result.getBank().getName());
        Assert.assertEquals(format.getIdofici(), result.getBank().getBranch()
                .getId());
        Assert.assertEquals(format.getDescofi(), result.getBank().getBranch()
                .getName());
        Assert.assertEquals(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_ENUM, result.getDelivery()
                .getDestination().getId());
        Assert.assertEquals(format.getNomdest(), result.getDelivery()
                .getDestination().getName());
        Assert.assertEquals(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_ENUM, result.getParticipant()
                .getIdentityDocument().getDocumentType().getId());
        Assert.assertEquals(format.getDestdo(), result.getParticipant()
                .getIdentityDocument().getDocumentType().getDescription());
        Assert.assertEquals(format.getNumdoc(), result.getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertEquals(format.getNombre1(), result.getParticipant()
                .getFirstName());
        Assert.assertEquals(format.getNombre2(), result.getParticipant()
                .getMiddleName());
        Assert.assertEquals(format.getApelli1(), result.getParticipant()
                .getLastName());
        Assert.assertEquals(format.getApelli2(), result.getParticipant()
                .getSecondLastName());
        Assert.assertEquals(format.getIdprofe(), result.getParticipant()
                .getProfession().getId());
        Assert.assertEquals(format.getDescpro(), result.getParticipant()
                .getProfession().getName());
        Assert.assertEquals(format.getIdestci(), result.getParticipant()
                .getMaritalStatus().getId());
        Assert.assertEquals(format.getDescec(), result.getParticipant()
                .getMaritalStatus().getName());
        Assert.assertEquals(format.getIdvinc(), result.getParticipant()
                .getRelationType().getId());
        Assert.assertEquals(format.getDesvinc(), result.getParticipant()
                .getRelationType().getName());
        Assert.assertEquals(format.getMonline(), result.getGrantedCredits()
                .get(0).getAmount());
        Assert.assertEquals(format.getDivisal(), result.getGrantedCredits()
                .get(0).getCurrency());
        Assert.assertEquals(dummyMock.buildDate(format.getFecope(), format.getHorope(), "yyyy-MM-ddHH:mm:ss"),
                result.getOperationDate().getTime());
    }

    @Test
    public void mapOutEmptyTest() throws IOException {
        FormatoKTSKB931 format = FormatsKb93Mock.getInstance()
                .getFormatoKTSKB931Empty();
        CardProposal result = mapper.mapOut(format);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getId());
        Assert.assertNull(result.getTitle());
        Assert.assertNull(result.getCardType());
        Assert.assertNull(result.getBank());
        Assert.assertNull(result.getDelivery());
        Assert.assertNull(result.getParticipant());
        Assert.assertNull(result.getGrantedCredits().get(0).getCurrency());
        Assert.assertNull(result.getContactDetails());
        Assert.assertNotNull(result.getGrantedCredits().get(0).getAmount());
        Assert.assertEquals(format.getMonline(), result.getGrantedCredits()
                .get(0).getAmount());
    }
}

package com.bbva.pzic.cards.dao.rest.mapper.impl;

import com.bbva.pzic.cards.dao.model.filenet.Document;
import com.bbva.pzic.cards.dao.model.filenet.Documents;
import com.bbva.pzic.cards.dao.rest.mapper.IRestDigitizeDocumentFileMapper;
import com.bbva.pzic.cards.dao.rest.mock.stubs.ResponseDocumentsMock;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 27/07/2018.
 *
 * @author Entelgy
 */
public class RestDigitizeDocumentFileMapperTest {

    private IRestDigitizeDocumentFileMapper mapper;

    @Before
    public void setUp() {
        mapper = new RestDigitizeDocumentFileMapper();
    }

    @Test
    public void mapOutFullTest() throws IOException {
        Documents input = ResponseDocumentsMock.INSTANCE.buildDocumentFile();
        Document result = mapper.mapOut(input);

        assertNotNull(result);
        assertEquals(input.getDocumentos().get(0), result);
    }

    @Test
    public void mapOutEmptyDocumentsTest() throws IOException {
        Documents input = ResponseDocumentsMock.INSTANCE.buildDocumentFile();
        input.getDocumentos().clear();
        Document result = mapper.mapOut(input);

        assertNull(result);
    }

    @Test
    public void mapOutNullDocumentsTest() {
        Documents input = new Documents();
        Document result = mapper.mapOut(input);

        assertNull(result);
    }
}
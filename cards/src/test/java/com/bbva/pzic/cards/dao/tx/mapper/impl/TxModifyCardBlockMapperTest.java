package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.canonic.BlockData;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2S;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxModifyCardBlockMapperTest {

    @InjectMocks
    private TxModifyCardBlockMapper txModifyCardBlockMapper;

    @Mock
    private Translator translator;

    private EntityMock entityMock = EntityMock.getInstance();

    private DummyMock dummyMock;

    @Before
    public void init() {
        dummyMock = new DummyMock();

        Mockito.when(translator.translateBackendEnumValueStrictly("cards.block.blockId", "LO")).thenReturn("LOST");
    }

    @Test
    public void testMapInput() {
        final DTOIntBlock block = entityMock.buildDTOIntBlock();
        FormatoMPM0B2 formatoMPM0B2 = txModifyCardBlockMapper.mapInput(block);

        assertNotNull(formatoMPM0B2.getIdetarj());
        assertNotNull(formatoMPM0B2.getIdebloq());
        assertNotNull(formatoMPM0B2.getIderazo());
        assertNotNull(formatoMPM0B2.getIdactbl());
        assertNotNull(formatoMPM0B2.getIdrepos());
        assertNotNull(formatoMPM0B2.getDescrip());

        assertEquals(block.getCard().getCardId(), formatoMPM0B2.getIdetarj());
        assertEquals(block.getBlockId(), formatoMPM0B2.getIdebloq());
        assertEquals(block.getReasonId(), formatoMPM0B2.getIderazo());
        assertEquals("S", formatoMPM0B2.getIdactbl());
        assertEquals("S", formatoMPM0B2.getIdrepos());
        assertEquals(block.getAdditionalInformation(), formatoMPM0B2.getDescrip());
    }

    @Test
    public void testMapInputWithAdditionalInformationNull() {
        final DTOIntBlock block = entityMock.buildDTOIntBlock();
        block.setAdditionalInformation(null);
        FormatoMPM0B2 formatoMPM0B2 = txModifyCardBlockMapper.mapInput(block);

        assertNotNull(formatoMPM0B2.getIdetarj());
        assertNotNull(formatoMPM0B2.getIdebloq());
        assertNotNull(formatoMPM0B2.getIderazo());
        assertNotNull(formatoMPM0B2.getIdactbl());
        assertNotNull(formatoMPM0B2.getIdrepos());
        assertNull(formatoMPM0B2.getDescrip());

        assertEquals(block.getCard().getCardId(), formatoMPM0B2.getIdetarj());
        assertEquals(block.getBlockId(), formatoMPM0B2.getIdebloq());
        assertEquals(block.getReasonId(), formatoMPM0B2.getIderazo());
        assertEquals("S", formatoMPM0B2.getIdactbl());
        assertEquals("S", formatoMPM0B2.getIdrepos());
    }

    @Test
    public void testMapInputWithIsReissuedNull() {
        final DTOIntBlock block = entityMock.buildDTOIntBlock();
        block.setIsReissued(null);
        FormatoMPM0B2 formatoMPM0B2 = txModifyCardBlockMapper.mapInput(block);

        assertNotNull(formatoMPM0B2.getIdetarj());
        assertNotNull(formatoMPM0B2.getIdebloq());
        assertNotNull(formatoMPM0B2.getIderazo());
        assertNotNull(formatoMPM0B2.getIdactbl());
        assertNull(formatoMPM0B2.getIdrepos());
        assertNotNull(formatoMPM0B2.getDescrip());

        assertEquals(block.getCard().getCardId(), formatoMPM0B2.getIdetarj());
        assertEquals(block.getBlockId(), formatoMPM0B2.getIdebloq());
        assertEquals(block.getReasonId(), formatoMPM0B2.getIderazo());
        assertEquals("S", formatoMPM0B2.getIdactbl());
        assertEquals(block.getAdditionalInformation(), formatoMPM0B2.getDescrip());
    }

    @Test
    public void testMapOutput() {
        final DTOIntBlock inputBlock = new DTOIntBlock();
        final FormatoMPM0B2S formatoMPM0B2S = entityMock.buildFormatoMPM0B2S();

        BlockData outputBlock = txModifyCardBlockMapper.mapOutput(formatoMPM0B2S, inputBlock);
        assertNotNull(outputBlock.getData().getBlockId());
        assertNotNull(outputBlock.getData().getName());
        assertNotNull(outputBlock.getData().getReason().getId());
        assertNotNull(outputBlock.getData().getReason().getName());
        assertNotNull(outputBlock.getData().getReference());
        assertNotNull(outputBlock.getData().getBlockDate());

        assertEquals("LOST", outputBlock.getData().getBlockId());
        assertEquals(formatoMPM0B2S.getDesbloq(), outputBlock.getData().getName());
        assertEquals(formatoMPM0B2S.getIderazo(), outputBlock.getData().getReason().getId());
        assertEquals(formatoMPM0B2S.getDesrazo(), outputBlock.getData().getReason().getName());
        assertEquals(formatoMPM0B2S.getMcnbloq().toString(), outputBlock.getData().getReference());
        assertEquals(dummyMock.buildDate(formatoMPM0B2S.getFecbloq(), formatoMPM0B2S.getHorablq(), "yyyy-MM-ddHH:mm:ss"), outputBlock.getData().getBlockDate().getTime());
    }

    @Test
    public void testMapOutWithoutBlockDate() {
        final DTOIntBlock inputBlock = new DTOIntBlock();
        final FormatoMPM0B2S formatoMPM0B2S = entityMock.buildFormatoMPM0B2S();
        formatoMPM0B2S.setHorablq(null);
        formatoMPM0B2S.setFecbloq(null);
        BlockData outputBlock = txModifyCardBlockMapper.mapOutput(formatoMPM0B2S, inputBlock);
        assertNotNull(outputBlock.getData().getBlockId());
        assertNotNull(outputBlock.getData().getName());
        assertNotNull(outputBlock.getData().getReason().getId());
        assertNotNull(outputBlock.getData().getReason().getName());
        assertNotNull(outputBlock.getData().getReference());
        assertNull(outputBlock.getData().getBlockDate());

        assertEquals("LOST", outputBlock.getData().getBlockId());
        assertEquals(formatoMPM0B2S.getDesbloq(), outputBlock.getData().getName());
        assertEquals(formatoMPM0B2S.getIderazo(), outputBlock.getData().getReason().getId());
        assertEquals(formatoMPM0B2S.getDesrazo(), outputBlock.getData().getReason().getName());
        assertEquals(formatoMPM0B2S.getMcnbloq().toString(), outputBlock.getData().getReference());
    }

    @Test
    public void testMapOutWithoutBlockDateWithHorablqNull() {
        final DTOIntBlock inputBlock = new DTOIntBlock();
        final FormatoMPM0B2S formatoMPM0B2S = entityMock.buildFormatoMPM0B2S();
        formatoMPM0B2S.setHorablq(null);
        BlockData outputBlock = txModifyCardBlockMapper.mapOutput(formatoMPM0B2S, inputBlock);
        assertNotNull(outputBlock.getData().getBlockId());
        assertNotNull(outputBlock.getData().getName());
        assertNotNull(outputBlock.getData().getReason().getId());
        assertNotNull(outputBlock.getData().getReason().getName());
        assertNotNull(outputBlock.getData().getReference());
        assertNull(outputBlock.getData().getBlockDate());

        assertEquals("LOST", outputBlock.getData().getBlockId());
        assertEquals(formatoMPM0B2S.getDesbloq(), outputBlock.getData().getName());
        assertEquals(formatoMPM0B2S.getIderazo(), outputBlock.getData().getReason().getId());
        assertEquals(formatoMPM0B2S.getDesrazo(), outputBlock.getData().getReason().getName());
        assertEquals(formatoMPM0B2S.getMcnbloq().toString(), outputBlock.getData().getReference());
    }

    @Test
    public void testMapOutWithoutBlockDateWithFecblqNull() {
        final DTOIntBlock inputBlock = new DTOIntBlock();
        final FormatoMPM0B2S formatoMPM0B2S = entityMock.buildFormatoMPM0B2S();
        formatoMPM0B2S.setFecbloq(null);
        BlockData outputBlock = txModifyCardBlockMapper.mapOutput(formatoMPM0B2S, inputBlock);
        assertNotNull(outputBlock.getData().getBlockId());
        assertNotNull(outputBlock.getData().getName());
        assertNotNull(outputBlock.getData().getReason().getId());
        assertNotNull(outputBlock.getData().getReason().getName());
        assertNotNull(outputBlock.getData().getReference());
        assertNull(outputBlock.getData().getBlockDate());

        assertEquals("LOST", outputBlock.getData().getBlockId());
        assertEquals(formatoMPM0B2S.getDesbloq(), outputBlock.getData().getName());
        assertEquals(formatoMPM0B2S.getIderazo(), outputBlock.getData().getReason().getId());
        assertEquals(formatoMPM0B2S.getDesrazo(), outputBlock.getData().getReason().getName());
        assertEquals(formatoMPM0B2S.getMcnbloq().toString(), outputBlock.getData().getReference());
    }

    @Test
    public void mapOutTestWithDtoNull() {
        final FormatoMPM0B2S formatoMPM0B2S = entityMock.buildFormatoMPM0B2S();
        BlockData outputBlock = txModifyCardBlockMapper.mapOutput(formatoMPM0B2S, null);

        assertNotNull(outputBlock);
        assertNotNull(outputBlock.getData());
        assertNotNull(outputBlock.getData().getBlockId());
        assertNotNull(outputBlock.getData().getName());
        assertNotNull(outputBlock.getData().getReason());
        assertNotNull(outputBlock.getData().getReference());
        assertNotNull(outputBlock.getData().getBlockDate());
    }

    @Test
    public void mapOutTestWithFormatInitialized() {
        BlockData outputBlock = txModifyCardBlockMapper.mapOutput(new FormatoMPM0B2S(), new DTOIntBlock());

        assertNotNull(outputBlock);
        assertNotNull(outputBlock.getData());
        assertNull(outputBlock.getData().getBlockId());
        assertNull(outputBlock.getData().getName());
        assertNull(outputBlock.getData().getReason());
        assertNull(outputBlock.getData().getReference());
        assertNull(outputBlock.getData().getBlockDate());
    }

}
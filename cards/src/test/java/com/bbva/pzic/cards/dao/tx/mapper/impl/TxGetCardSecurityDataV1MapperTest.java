package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputGetCardSecurityData;
import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMENDV;
import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMS1DV;
import com.bbva.pzic.cards.facade.v1.dto.SecurityData;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TxGetCardSecurityDataV1MapperTest {

    @InjectMocks
    private TxGetCardSecurityDataV1Mapper mapper;

    @Mock
    private Translator translator;

    private static final String SECURITY_DATA_ID = "CVV2";
    private static final String VALIDITY_TIME_UNIT_ID = "SECONDS";

    @Before
    public void init() {
        Mockito.when(translator.translateBackendEnumValueStrictly("securityData.id", "CVV2")).thenReturn(SECURITY_DATA_ID);
        Mockito.when(translator.translateBackendEnumValueStrictly("validity.timeUnit.id", "S")).thenReturn(VALIDITY_TIME_UNIT_ID);
    }

    @Test
    public void mapInFullTest() {
        InputGetCardSecurityData input = EntityMock.getInstance().getInputGetCardSecurityData();
        FormatoMPMENDV result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getNumtarj());
        assertNotNull(result.getKeypb01());
        assertNotNull(result.getKeypb02());
        assertNotNull(result.getKeypb03());
        assertNotNull(result.getKeypb04());
        assertNotNull(result.getKeypb05());
        assertNotNull(result.getKeypb06());
        assertNotNull(result.getKeypb07());
        assertNotNull(result.getKeypb08());
        assertNotNull(result.getKeypb09());
        assertNotNull(result.getKeypb10());
        assertNotNull(result.getKeypb11());
        assertNotNull(result.getKeypb12());
        assertNotNull(result.getKeypb13());
        assertNotNull(result.getKeypb14());

        assertEquals(input.getCardId(), result.getNumtarj());
        assertEquals(CIF_01, result.getKeypb01());
        assertEquals(CIF_02, result.getKeypb02());
        assertEquals(CIF_03, result.getKeypb03());
        assertEquals(CIF_04, result.getKeypb04());
        assertEquals(CIF_05, result.getKeypb05());
        assertEquals(CIF_06, result.getKeypb06());
        assertEquals(CIF_07, result.getKeypb07());
        assertEquals(CIF_08, result.getKeypb08());
        assertEquals(CIF_09, result.getKeypb09());
        assertEquals(CIF_10, result.getKeypb10());
        assertEquals(CIF_11, result.getKeypb11());
        assertEquals(CIF_12, result.getKeypb12());
        assertEquals(CIF_13, result.getKeypb13());
        assertEquals(CIF_14, result.getKeypb14());
    }

    @Test
    public void mapInPartialTest() {
        InputGetCardSecurityData input = EntityMock.getInstance().getInputGetCardSecurityData();
        final String partial = "12345678";
        input.setPublicKey(CIF_01 + CIF_02 + CIF_03 + CIF_04 + CIF_05 + CIF_06 + partial);
        FormatoMPMENDV result = mapper.mapIn(input);

        assertNotNull(result.getNumtarj());
        assertNotNull(result.getKeypb01());
        assertNotNull(result.getKeypb02());
        assertNotNull(result.getKeypb03());
        assertNotNull(result.getKeypb04());
        assertNotNull(result.getKeypb05());
        assertNotNull(result.getKeypb06());
        assertNotNull(result.getKeypb07());
        assertNull(result.getKeypb08());
        assertNull(result.getKeypb09());
        assertNull(result.getKeypb10());
        assertNull(result.getKeypb11());
        assertNull(result.getKeypb12());
        assertNull(result.getKeypb13());
        assertNull(result.getKeypb14());

        assertEquals(input.getCardId(), result.getNumtarj());
        assertEquals(CIF_01, result.getKeypb01());
        assertEquals(CIF_02, result.getKeypb02());
        assertEquals(CIF_03, result.getKeypb03());
        assertEquals(CIF_04, result.getKeypb04());
        assertEquals(CIF_05, result.getKeypb05());
        assertEquals(CIF_06, result.getKeypb06());
        assertEquals(partial, result.getKeypb07());
    }

    @Test
    public void mapInWithPublicKeyEmptyTest() {
        InputGetCardSecurityData dto = EntityMock.getInstance().getInputGetCardSecurityData();
        dto.setPublicKey("");
        FormatoMPMENDV result = mapper.mapIn(dto);
        assertNotNull(result);
        assertNull(result.getKeypb01());
        assertNull(result.getKeypb02());
        assertNull(result.getKeypb03());
        assertNull(result.getKeypb04());
        assertNull(result.getKeypb05());
        assertNull(result.getKeypb06());
        assertNull(result.getKeypb07());
        assertNull(result.getKeypb08());
        assertNull(result.getKeypb09());
        assertNull(result.getKeypb10());
        assertNull(result.getKeypb11());
        assertNull(result.getKeypb12());
        assertNull(result.getKeypb13());
        assertNull(result.getKeypb14());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoMPMS1DV formatoMPMS1DV = EntityMock.getInstance().getFormatoMPMS1DVMock();
        List<SecurityData> result = new ArrayList<>();
        result = mapper.mapOut(formatoMPMS1DV, result);

        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getDescription());
        assertNotNull(result.get(0).getCode());
        assertNotNull(result.get(0).getValidityPeriod().getTimeUnit());
        assertNotNull(result.get(0).getValidityPeriod().getValue());

        assertEquals(SECURITY_DATA_ID, result.get(0).getId());
        assertEquals(formatoMPMS1DV.getDscodsg(), result.get(0).getDescription());
        assertEquals(formatoMPMS1DV.getCodse01()
                        + formatoMPMS1DV.getCodse02()
                        + formatoMPMS1DV.getCodse03()
                        + formatoMPMS1DV.getCodse04()
                        + formatoMPMS1DV.getCodse05()
                        + formatoMPMS1DV.getCodse06()
                        + formatoMPMS1DV.getCodse07()
                        + formatoMPMS1DV.getCodse08()
                        + formatoMPMS1DV.getCodse09()
                        + formatoMPMS1DV.getCodse10()
                        + formatoMPMS1DV.getCodse11()
                        + formatoMPMS1DV.getCodse12()
                        + formatoMPMS1DV.getCodse13()
                        + formatoMPMS1DV.getCodse14()
                , result.get(0).getCode());
        assertEquals(VALIDITY_TIME_UNIT_ID, result.get(0).getValidityPeriod().getTimeUnit());
        assertEquals(formatoMPMS1DV.getTimerns(), result.get(0).getValidityPeriod().getValue());
    }

    @Test
    public void mapOutWithoutValidityPeriod() throws IOException {
        FormatoMPMS1DV formatoMPMS1DV = EntityMock.getInstance().getFormatoMPMS1DVMock();
        formatoMPMS1DV.setTimerun(null);
        formatoMPMS1DV.setTimerns(null);
        List<SecurityData> result = new ArrayList<>();
        result = mapper.mapOut(formatoMPMS1DV, result);

        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getDescription());
        assertNotNull(result.get(0).getCode());
        assertNull(result.get(0).getValidityPeriod());

        assertEquals(SECURITY_DATA_ID, result.get(0).getId());
        assertEquals(formatoMPMS1DV.getDscodsg(), result.get(0).getDescription());
        assertEquals(formatoMPMS1DV.getCodse01()
                        + formatoMPMS1DV.getCodse02()
                        + formatoMPMS1DV.getCodse03()
                        + formatoMPMS1DV.getCodse04()
                        + formatoMPMS1DV.getCodse05()
                        + formatoMPMS1DV.getCodse06()
                        + formatoMPMS1DV.getCodse07()
                        + formatoMPMS1DV.getCodse08()
                        + formatoMPMS1DV.getCodse09()
                        + formatoMPMS1DV.getCodse10()
                        + formatoMPMS1DV.getCodse11()
                        + formatoMPMS1DV.getCodse12()
                        + formatoMPMS1DV.getCodse13()
                        + formatoMPMS1DV.getCodse14()
                , result.get(0).getCode());
    }


    @Test
    public void mapOutEmptyTest() {
        List<SecurityData> result = new ArrayList<>();
        result = mapper.mapOut(new FormatoMPMS1DV(), result);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertNull(result.get(0).getId());
        assertNull(result.get(0).getDescription());
        assertNull(result.get(0).getCode());
        assertNull(result.get(0).getValidityPeriod());
    }

    @Test
    public void mapOutNullTest() {
        List<SecurityData> result = new ArrayList<>();
        result = mapper.mapOut(null, result);
        assertNotNull(result);
        assertEquals(0, result.size());
    }
}


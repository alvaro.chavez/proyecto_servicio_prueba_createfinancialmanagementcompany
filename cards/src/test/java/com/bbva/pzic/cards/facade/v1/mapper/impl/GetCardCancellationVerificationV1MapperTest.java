package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardCancellationVerification;
import com.bbva.pzic.cards.facade.v1.dto.CancellationVerification;
import org.junit.Test;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static org.junit.Assert.*;

public class GetCardCancellationVerificationV1MapperTest {

    private GetCardCancellationVerificationV1Mapper mapper = new GetCardCancellationVerificationV1Mapper();

    @Test
    public void mapInFullTest() {
        InputGetCardCancellationVerification result = mapper.mapIn(CARD_ID);

        assertNotNull(result);
        assertNotNull(result.getCardId());

        assertEquals(CARD_ID, result.getCardId());
    }


    @Test
    public void mapOutFullTest() {
        ServiceResponse<CancellationVerification> result = mapper.mapOut(new CancellationVerification());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<CancellationVerification> result = mapper.mapOut(null);
        assertNull(result);
    }
}

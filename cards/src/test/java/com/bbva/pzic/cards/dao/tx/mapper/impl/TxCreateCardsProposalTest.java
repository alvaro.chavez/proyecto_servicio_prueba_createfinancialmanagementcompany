package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntProposal;
import com.bbva.pzic.cards.canonic.Import;
import com.bbva.pzic.cards.canonic.Proposal;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTECKB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTS1KB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTS2KB89;
import com.bbva.pzic.cards.dao.model.kb89.FormatoKTSCKB89;
import com.bbva.pzic.cards.dao.model.kb89.mock.FormatsKb89Mock;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created on 12/12/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxCreateCardsProposalTest {

    private static final String CONTACT_DETAIL_ID_ENCRIPT = "GiygIYyubIHBijb";

    @InjectMocks
    private TxCreateCardsProposalMapper mapper;

    @Mock
    private Translator translator;

    @Mock
    private AbstractCypherTool cypherTool;

    private DummyMock dummyMock;

    @Before
    public void setUp() {
        dummyMock = new DummyMock();
    }

    @Test
    public void mapInTest() throws IOException {
        DTOIntProposal input = EntityMock.getInstance().getDtoIntProposal();
        FormatoKTECKB89 result = mapper.mapIn(input);

        assertNotNull(result.getIdtipta());
        assertNotNull(result.getIdprodu());
        assertNotNull(result.getIdsprod());
        assertNotNull(result.getIdtpago());
        assertNotNull(result.getIdperio());
        assertNotNull(result.getDiapago());
        assertNotNull(result.getMonline());
        assertNotNull(result.getDivisal());
        assertNotNull(result.getIddesti());
        assertNotNull(result.getIdbanco());
        assertNotNull(result.getIdofici());
        assertNotNull(result.getIdcoms1());
        assertNotNull(result.getNomcom1());
        assertNotNull(result.getTipcomi());
        assertNotNull(result.getImpcom1());
        assertNotNull(result.getIdcoms2());
        assertNotNull(result.getNomcom2());
        assertNotNull(result.getImpcom2());
        assertNotNull(result.getIdtstea());
        assertNotNull(result.getFectasa());
        assertNotNull(result.getTiptasa());
        assertNotNull(result.getTasatea());
        assertNotNull(result.getIdttcea());
        assertNotNull(result.getTastcea());
        assertNotNull(result.getProduct());
        assertNotNull(result.getImpprod());
        assertNotNull(result.getIdpmemb());
        assertNotNull(result.getNumpmem());
        assertNotNull(result.getIdcelul());
        assertNotNull(result.getEcenvio());
        assertNotNull(result.getIdofert());
        assertNotNull(result.getIndnotf());

        assertEquals(input.getCardType().getId(), result.getIdtipta());
        assertEquals(input.getTitle().getId(), result.getIdprodu());
        assertEquals(input.getTitle().getSubproduct().getId(), result.getIdsprod());
        assertEquals(input.getPaymentMethod().getId(), result.getIdtpago());
        assertEquals(input.getPaymentMethod().getPeriod().getId(), result.getIdperio());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getDiapago());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getMonline());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getDivisal());
        assertEquals(input.getDelivery().getDestination().getId(), result.getIddesti());
        assertEquals(input.getBank().getId(), result.getIdbanco());
        assertEquals(input.getBank().getBranch().getId(), result.getIdofici());
        assertEquals(input.getFees().getItemizeFees().get(0).getFeeType(), result.getIdcoms1());
        assertEquals(input.getFees().getItemizeFees().get(0).getName(), result.getNomcom1());
        assertEquals(input.getFees().getItemizeFees().get(0).getMode().getId(), result.getTipcomi());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount(), result.getImpcom1());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency(), result.getDivisal());
        assertEquals(input.getFees().getItemizeFees().get(1).getFeeType(), result.getIdcoms2());
        assertEquals(input.getFees().getItemizeFees().get(1).getName(), result.getNomcom2());
        assertEquals(input.getFees().getItemizeFees().get(1).getMode().getId(), result.getTipcomi());
        assertEquals(input.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount(), result.getImpcom2());
        assertEquals(input.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency(), result.getDivisal());
        assertEquals(input.getRates().get(0).getRateType().getId(), result.getIdtstea());
        assertEquals(input.getRates().get(0).getCalculationDate(), result.getFectasa());
        assertEquals(input.getRates().get(0).getModeid(), result.getTiptasa());
        assertEquals(input.getRates().get(0).getUnit().getPercentage(), result.getTasatea());
        assertEquals(input.getRates().get(1).getRateType().getId(), result.getIdttcea());
        assertEquals(input.getRates().get(1).getCalculationDate(), result.getFectasa());
        assertEquals(input.getRates().get(1).getModeid(), result.getTiptasa());
        assertEquals(input.getRates().get(1).getUnit().getPercentage(), result.getTastcea());
        assertEquals(input.getAdditionalProducts().get(0).getProductType(), result.getProduct());
        assertEquals(input.getAdditionalProducts().get(0).getAmount(), result.getImpprod());
        assertEquals(input.getAdditionalProducts().get(0).getCurrency(), result.getDivisal());
        assertEquals(input.getMembership().getId(), result.getIdpmemb());
        assertEquals(input.getMembership().getNumber(), result.getNumpmem());
        assertEquals(input.getContactDetailsId0(), result.getIdcelul());
        assertEquals(input.getContactDetailsId1(), result.getEcenvio());
        assertEquals(input.getOfferId(), result.getIdofert());
        assertEquals("S", result.getIndnotf());
    }


    @Test
    public void mapOutFullTest() throws IOException {
        FormatoKTSCKB89 input = FormatsKb89Mock.getInstance().getListFormatoKTSCKB89().get(0);

        Mockito.when(translator.translateBackendEnumValueStrictly("cards.paymentMethod.id", input.getIdtpago()))
                .thenReturn("A");
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.period.id", input.getIdperio()))
                .thenReturn("C");
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.offers.additionalProducts.productType", input.getProduct()))
                .thenReturn("I");
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.delivery.destionation.id", input.getIddesti()))
                .thenReturn("FRONT_VALUE_DESTINATION_ID");

        Mockito.when(cypherTool.encrypt(input.getIdcelul(), AbstractCypherTool.CONTACT_DETAIL_ID, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL)).thenReturn(CONTACT_DETAIL_ID_ENCRIPT);
        Mockito.when(cypherTool.encrypt(input.getEcenvio(), AbstractCypherTool.CONTACT_DETAIL_ID, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL)).thenReturn(CONTACT_DETAIL_ID_ENCRIPT);

        Proposal result = mapper.mapOut(input);
        assertNotNull(result.getId());
        assertNotNull(result.getDelivery().getDestination().getId());
        assertNotNull(result.getDelivery().getDestination().getName());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getName());
        assertNotNull(result.getPaymentMethod().getPeriod().getId());
        assertNotNull(result.getPaymentMethod().getPeriod().getName());
        assertNotNull(result.getPaymentMethod().getEndDay());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getAdditionalProducts().get(0).getProductType());
        assertNotNull(result.getAdditionalProducts().get(0).getAmount());
        assertNotNull(result.getAdditionalProducts().get(0).getCurrency());
        assertNotNull(result.getMembership().getId());
        assertNotNull(result.getMembership().getDescription());
        assertNotNull(result.getMembership().getNumber());
        assertNotNull(result.getContactDetails().get(0).getId());
        assertNotNull(result.getContactDetails().get(1).getId());
        assertNotNull(result.getOperationDate());
        assertNotNull(result.getOperationNumber());
        assertNotNull(result.getNotificationsByOperation());

        assertEquals(input.getIdsolic(), result.getId());
        assertEquals("FRONT_VALUE_DESTINATION_ID", result.getDelivery().getDestination().getId());
        assertEquals(input.getNomdest(), result.getDelivery().getDestination().getName());
        assertEquals("A", result.getPaymentMethod().getId());
        assertEquals(input.getNompago(), result.getPaymentMethod().getName());
        assertEquals("C", result.getPaymentMethod().getPeriod().getId());
        assertEquals(input.getNomperi(), result.getPaymentMethod().getPeriod().getName());
        assertEquals(input.getDiapago(), result.getPaymentMethod().getEndDay());
        assertEquals(input.getMonline(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(input.getDivisal(), result.getGrantedCredits().get(0).getCurrency());

        assertEquals("I", result.getAdditionalProducts().get(0).getProductType());
        assertEquals(input.getImpprod(), result.getAdditionalProducts().get(0).getAmount());
        assertEquals(input.getDivisal(), result.getAdditionalProducts().get(0).getCurrency());
        assertEquals(input.getIdpmemb(), result.getMembership().getId());
        assertEquals(input.getNompmeb(), result.getMembership().getDescription());
        assertEquals(input.getNumpmem(), result.getMembership().getNumber());
        assertEquals(CONTACT_DETAIL_ID_ENCRIPT, result.getContactDetails().get(0).getId());
        assertEquals(CONTACT_DETAIL_ID_ENCRIPT, result.getContactDetails().get(1).getId());
        assertEquals(dummyMock.buildDate(input.getFecope(), input.getHorope(), "yyyy-MM-ddHH:mm:ss"), result.getOperationDate().getTime());
        assertEquals(input.getIdsolic(), result.getOperationNumber());
        assertEquals("true", result.getNotificationsByOperation());
    }

    @Test
    public void mapOutEmptyTest() throws IOException {
        FormatoKTSCKB89 input = FormatsKb89Mock.getInstance().getListFormatoKTSCKB89().get(1);
        Proposal result = mapper.mapOut(input);

        assertNull(result.getId());
        assertNull(result.getDelivery());
        assertNotNull(result.getPaymentMethod());
        assertNotNull(result.getGrantedCredits());
        assertNull(result.getAdditionalProducts());
        assertNull(result.getMembership());
        assertNull(result.getContactDetails());
        assertNull(result.getOperationDate());
        assertNull(result.getOperationNumber());
        assertNull(result.getNotificationsByOperation());

        result = mapper.mapOut(new FormatoKTSCKB89());

        assertNull(result.getId());
        assertNull(result.getDelivery());
        assertNull(result.getPaymentMethod());
        assertNull(result.getGrantedCredits());
        assertNull(result.getAdditionalProducts());
        assertNull(result.getMembership());
        assertNull(result.getContactDetails());
        assertNull(result.getOperationDate());
        assertNull(result.getOperationNumber());
        assertNull(result.getNotificationsByOperation());
    }

    @Test
    public void mapOut2FullTest() throws IOException {
        FormatoKTS1KB89 formatOutput = FormatsKb89Mock.getInstance().getListFormatoKTS1KB89().get(0);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.rates.mode", formatOutput.getTiptasa()))
                .thenReturn("A");

        Proposal result = mapper.mapOut2(formatOutput, new Proposal());

        assertNotNull(result.getRates().get(0).getRateType().getId());
        assertNotNull(result.getRates().get(0).getRateType().getName());
        assertNotNull(result.getRates().get(0).getCalculationDate());
        assertNotNull(result.getRates().get(0).getMode().getId());
        assertNotNull(result.getRates().get(0).getMode().getName());
        assertNotNull(result.getRates().get(0).getUnit().getPercentage());

        assertEquals(formatOutput.getIdttasa(), result.getRates().get(0).getRateType().getId());
        assertEquals(formatOutput.getNomtasa(), result.getRates().get(0).getRateType().getName());
        assertEquals(formatOutput.getFectasa(), result.getRates().get(0).getCalculationDate());
        assertEquals("A", result.getRates().get(0).getMode().getId());
        assertEquals(formatOutput.getNomtipt(), result.getRates().get(0).getMode().getName());
        assertEquals(formatOutput.getPortasa(), result.getRates().get(0).getUnit().getPercentage());
        assertEquals(1, result.getRates().size());

        formatOutput = FormatsKb89Mock.getInstance().getListFormatoKTS1KB89().get(1);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.rates.mode", formatOutput.getTiptasa()))
                .thenReturn("B");

        result = mapper.mapOut2(formatOutput, result);

        assertNotNull(result.getRates().get(1).getRateType().getId());
        assertNotNull(result.getRates().get(1).getRateType().getName());
        assertNotNull(result.getRates().get(1).getCalculationDate());
        assertNotNull(result.getRates().get(1).getMode().getId());
        assertNotNull(result.getRates().get(1).getMode().getName());
        assertNotNull(result.getRates().get(1).getUnit().getPercentage());

        assertEquals(formatOutput.getIdttasa(), result.getRates().get(1).getRateType().getId());
        assertEquals(formatOutput.getNomtasa(), result.getRates().get(1).getRateType().getName());
        assertEquals(formatOutput.getFectasa(), result.getRates().get(1).getCalculationDate());
        assertEquals("B", result.getRates().get(1).getMode().getId());
        assertEquals(formatOutput.getNomtipt(), result.getRates().get(1).getMode().getName());
        assertEquals(formatOutput.getPortasa(), result.getRates().get(1).getUnit().getPercentage());
        assertEquals(2, result.getRates().size());
    }

    @Test
    public void mapOut2EmptyTest() throws IOException {
        FormatoKTS1KB89 formatOutput = FormatsKb89Mock.getInstance().getListFormatoKTS1KB89().get(2);
        Proposal result = mapper.mapOut2(formatOutput, new Proposal());

        assertNull(result.getRates().get(0).getRateType());
        assertNull(result.getRates().get(0).getCalculationDate());
        assertNull(result.getRates().get(0).getMode());
        assertNull(result.getRates().get(0).getUnit());
        assertEquals(1, result.getRates().size());
    }

    @Test
    public void mapOut3FullTest() throws IOException {
        FormatoKTS2KB89 formatoKTS2KB89 = FormatsKb89Mock.getInstance().getListFormatoKTS2KB89().get(0);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.itemizeFees.feeType", formatoKTS2KB89.getIdcomim()))
                .thenReturn("F");
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.itemizeFees.mode", formatoKTS2KB89.getTipcomi()))
                .thenReturn("M");

        Proposal dtoOut = new Proposal();
        dtoOut.setGrantedCredits(new ArrayList<Import>());
        Import importe = new Import();
        importe.setCurrency("PEM");
        dtoOut.getGrantedCredits().add(importe);

        Proposal result = mapper.mapOut3(formatoKTS2KB89, dtoOut);

        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getName());
        assertNotNull(result.getFees().getItemizeFees().get(0).getMode().getId());
        assertNotNull(result.getFees().getItemizeFees().get(0).getMode().getName());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());

        assertEquals("F", result.getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(formatoKTS2KB89.getNomcomi(), result.getFees().getItemizeFees().get(0).getName());
        assertEquals("M", result.getFees().getItemizeFees().get(0).getMode().getId());
        assertEquals(formatoKTS2KB89.getNomtipc(), result.getFees().getItemizeFees().get(0).getMode().getName());
        assertEquals(formatoKTS2KB89.getImpcomi(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertEquals(dtoOut.getGrantedCredits().get(0).getCurrency(),
                result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());

        formatoKTS2KB89 = FormatsKb89Mock.getInstance().getListFormatoKTS2KB89().get(1);
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.itemizeFees.feeType", formatoKTS2KB89.getIdcomim()))
                .thenReturn("D");
        Mockito.when(translator.translateBackendEnumValueStrictly("cards.itemizeFees.mode", formatoKTS2KB89.getTipcomi()))
                .thenReturn("W");

        result = mapper.mapOut3(formatoKTS2KB89, dtoOut);

        assertNotNull(result.getFees().getItemizeFees().get(1).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(1).getName());
        assertNotNull(result.getFees().getItemizeFees().get(1).getMode().getId());
        assertNotNull(result.getFees().getItemizeFees().get(1).getMode().getName());
        assertNotNull(result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());

        assertEquals("D", result.getFees().getItemizeFees().get(1).getFeeType());
        assertEquals(formatoKTS2KB89.getNomcomi(), result.getFees().getItemizeFees().get(1).getName());
        assertEquals("W", result.getFees().getItemizeFees().get(1).getMode().getId());
        assertEquals(formatoKTS2KB89.getNomtipc(), result.getFees().getItemizeFees().get(1).getMode().getName());
        assertEquals(formatoKTS2KB89.getImpcomi(), result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertEquals(dtoOut.getGrantedCredits().get(0).getCurrency(), result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
    }
}

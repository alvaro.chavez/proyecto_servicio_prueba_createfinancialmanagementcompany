package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.InputGetCardsCardOffer;
import com.bbva.pzic.cards.canonic.Offer;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static com.bbva.pzic.cards.EntityMock.CARD_ENCRYPT_ID;
import static com.bbva.pzic.cards.EntityMock.OFFER_ID;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_GET_CARDS_CARD_OFFER;

/**
 * Created on 09/11/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class GetCardsCardOfferMapperTest {

    @InjectMocks
    private GetCardsCardOfferMapper mapper;
    @Mock
    private AbstractCypherTool cypherTool;

    @Test
    public void mapInFullTest() {
        Mockito.when(cypherTool.decrypt(CARD_ID, AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_GET_CARDS_CARD_OFFER))
                .thenReturn(CARD_ENCRYPT_ID);
        InputGetCardsCardOffer result = mapper.mapIn(CARD_ID, OFFER_ID);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getOfferId());

        assertEquals(CARD_ENCRYPT_ID, result.getCardId());
        assertEquals(OFFER_ID, result.getOfferId());
    }

    @Test
    public void mapInNullTest() {
        InputGetCardsCardOffer result = mapper.mapIn(null, null);

        assertNull(result);
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<Offer> result = mapper.mapOut(new Offer());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<Offer> result = mapper.mapOut(null);
        Assert.assertNull(result);
    }

}
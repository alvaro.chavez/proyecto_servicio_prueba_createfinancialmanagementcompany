package com.bbva.pzic.cards.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 15/06/2016.
 *
 * @author Entelgy
 */
public class ErrorResponse {

    private int version;

    @JsonProperty("http-status")
    private int httpStatus;

    @JsonProperty("error-code")
    private String errorCode;

    @JsonProperty("error-message")
    private String errorMessage;

    private String severity;

    @JsonProperty("consumer-request-id")
    private String consumerRequestId;

    @JsonProperty("system-error-code")
    private String systemErrorCode;

    @JsonProperty("system-error-description")
    private String systemErrorDescription;

    @JsonProperty(value = "system-error-cause")
    private String systemErrorCause;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getConsumerRequestId() {
        return consumerRequestId;
    }

    public void setConsumerRequestId(String consumerRequestId) {
        this.consumerRequestId = consumerRequestId;
    }

    public String getSystemErrorCode() {
        return systemErrorCode;
    }

    public void setSystemErrorCode(String systemErrorCode) {
        this.systemErrorCode = systemErrorCode;
    }

    public String getSystemErrorDescription() {
        return systemErrorDescription;
    }

    public void setSystemErrorDescription(String systemErrorDescription) {
        this.systemErrorDescription = systemErrorDescription;
    }

    public String getSystemErrorCause() {
        return systemErrorCause;
    }

    public void setSystemErrorCause(String systemErrorCause) {
        this.systemErrorCause = systemErrorCause;
    }
}

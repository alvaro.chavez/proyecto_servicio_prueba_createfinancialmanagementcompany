package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanData;
import com.bbva.pzic.cards.dao.model.mpwt.FormatoMPM0DET;
import com.bbva.pzic.cards.dao.model.mpwt.FormatoMPM0TSC;
import com.bbva.pzic.cards.dao.model.mpwt.FormatoMPM0TSE;
import com.bbva.pzic.cards.dao.model.mpwt.mock.FormatMpwtMock;
import com.bbva.pzic.cards.dao.tx.mapper.ITxCreateCardInstallmentsPlanMapper;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.List;

public class TxCreateCardInstallmentsPlanMapperTest {

    @InjectMocks
    private ITxCreateCardInstallmentsPlanMapper createInstallmentPlanMapper;

    @Mock
    private EnumMapper enumMapper;

    private EntityMock entityMock = EntityMock.getInstance();

    private FormatMpwtMock formatMpwtMock;

    @Before
    public void setUp() {
        formatMpwtMock = new FormatMpwtMock();
        createInstallmentPlanMapper = new TxCreateCardInstallmentsPlanMapper();
        MockitoAnnotations.initMocks(this);
        mapOutEnumMapper();
    }

    public void mapOutEnumMapper() {
        Mockito.when(enumMapper.getEnumValue("installmentPlan.rates.rateType.id", "TEA")).thenReturn("TEA");
        Mockito.when(enumMapper.getEnumValue("installmentPlan.rates.rateType.id", "TCEA")).thenReturn("TCEA");
    }

    @Test
    public void testMapInFullCreateInstallmentPlan() {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputCreateInstallmentPlan();
        FormatoMPM0TSE result = createInstallmentPlanMapper.mapIn(dtoIn);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNcuotas());
        Assert.assertNotNull(result.getNumoper());
        Assert.assertNotNull(result.getNumtarj());

        Assert.assertEquals(dtoIn.getCardId(), result.getNumtarj());
        Assert.assertEquals(dtoIn.getTermsNumber(), result.getNcuotas());
        Assert.assertEquals(dtoIn.getTransactionId(), result.getNumoper());
    }

    @Test
    public void testMapInFullCreateInstallmentPlanWithoutTermsTotalNumber() {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputCreateInstallmentPlan();
        dtoIn.setTermsNumber(null);
        FormatoMPM0TSE result = createInstallmentPlanMapper.mapIn(dtoIn);

        Assert.assertNotNull(result);
        Assert.assertNull(result.getNcuotas());
        Assert.assertNotNull(result.getNumoper());
        Assert.assertNotNull(result.getNumtarj());

        Assert.assertEquals(dtoIn.getCardId(), result.getNumtarj());
        Assert.assertEquals(dtoIn.getTransactionId(), result.getNumoper());
    }

    @Test
    public void testMapOutCreateInstallmentPlanFull() throws IOException {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputCreateInstallmentPlan();
        FormatoMPM0TSC formatoMPM0TSC = formatMpwtMock.getFormatoMPM0TSC();
        List<FormatoMPM0DET> formatoMPM0DETList = formatMpwtMock.getFormatoMPM0DET();

        InstallmentsPlanData data = createInstallmentPlanMapper.mapOut1(formatoMPM0TSC, dtoIn);

        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getData());
        Assert.assertNotNull(data.getData().getId());
        Assert.assertNotNull(data.getData().getTerms().getNumber());
        Assert.assertNotNull(data.getData().getFirstInstallmentDate());
        Assert.assertNotNull(data.getData().getCapital().getAmount());
        Assert.assertNotNull(data.getData().getCapital().getCurrency());
        Assert.assertNotNull(data.getData().getRates().get(0).getRateType().getId());
        Assert.assertNotNull(data.getData().getRates().get(0).getRateType().getName());
        Assert.assertNotNull(data.getData().getRates().get(0).getMode().getName());
        Assert.assertNotNull(data.getData().getRates().get(0).getUnit().getPercentage());
        Assert.assertNotNull(data.getData().getRates().get(1).getRateType().getId());
        Assert.assertNotNull(data.getData().getRates().get(1).getRateType().getName());
        Assert.assertNotNull(data.getData().getRates().get(1).getMode().getName());
        Assert.assertNotNull(data.getData().getRates().get(1).getUnit().getPercentage());
        Assert.assertNotNull(data.getData().getInterest().getAmount());
        Assert.assertNotNull(data.getData().getInterest().getCurrency());
        Assert.assertNotNull(data.getData().getTotal().getAmount());
        Assert.assertNotNull(data.getData().getTotal().getCurrency());
        Assert.assertNotNull(data.getData().getFirstInstallment().getAmount());
        Assert.assertNotNull(data.getData().getFirstInstallment().getCurrency());

        Assert.assertEquals(formatoMPM0TSC.getNumopcu(), data.getData().getId());
        Assert.assertEquals(formatoMPM0TSC.getNcuotas(), data.getData().getTerms().getNumber());
        Assert.assertEquals(formatoMPM0TSC.getTotcapi(), data.getData().getCapital().getAmount());
        Assert.assertEquals(formatoMPM0TSC.getDivtcap(), data.getData().getCapital().getCurrency());
        Assert.assertEquals("TEA", data.getData().getRates().get(0).getRateType().getId());
        Assert.assertEquals("TASA EFECTIVA ANUAL", data.getData().getRates().get(0).getRateType().getName());
        Assert.assertEquals("PERCENTAGE", data.getData().getRates().get(0).getMode().getName());
        Assert.assertEquals(formatoMPM0TSC.getTasaefa(), data.getData().getRates().get(0).getUnit().getPercentage());
        Assert.assertEquals("TCEA", data.getData().getRates().get(1).getRateType().getId());
        Assert.assertEquals("TASA DE COSTO EFECTIVA ANUAL", data.getData().getRates().get(1).getRateType().getName());
        Assert.assertEquals("PERCENTAGE", data.getData().getRates().get(1).getMode().getName());
        Assert.assertEquals(formatoMPM0TSC.getPortcea(), data.getData().getRates().get(1).getUnit().getPercentage());
        Assert.assertEquals(formatoMPM0TSC.getTotinte(), data.getData().getInterest().getAmount());
        Assert.assertEquals(formatoMPM0TSC.getDivtint(), data.getData().getInterest().getCurrency());
        Assert.assertEquals(formatoMPM0TSC.getTotcuot(), data.getData().getTotal().getAmount());
        Assert.assertEquals(formatoMPM0TSC.getDivtcuo(), data.getData().getTotal().getCurrency());
        Assert.assertEquals(formatoMPM0TSC.getImpcuo1(), data.getData().getFirstInstallment().getAmount());
        Assert.assertEquals(formatoMPM0TSC.getDivtcuo(), data.getData().getFirstInstallment().getCurrency());

        FormatoMPM0DET firstScheduleInstallments = formatoMPM0DETList.get(0);
        data = createInstallmentPlanMapper.mapOut2(firstScheduleInstallments, data);

        Assert.assertEquals(1, data.getData().getScheduledPayments().size());
        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getData());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getMaturityDate());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getTotal().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getTotal().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getCapital().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getCapital().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getInterest().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getInterest().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getFee().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getFee().getCurrency());

        Assert.assertEquals(firstScheduleInstallments.getFecpago(), data.getData().getScheduledPayments().get(0).getMaturityDate());
        Assert.assertEquals(firstScheduleInstallments.getImpcuot(), data.getData().getScheduledPayments().get(0).getTotal().getAmount());
        Assert.assertEquals(firstScheduleInstallments.getDivcuot(), data.getData().getScheduledPayments().get(0).getTotal().getCurrency());
        Assert.assertEquals(firstScheduleInstallments.getCapital(), data.getData().getScheduledPayments().get(0).getCapital().getAmount());
        Assert.assertEquals(firstScheduleInstallments.getDivcapc(), data.getData().getScheduledPayments().get(0).getCapital().getCurrency());
        Assert.assertEquals(firstScheduleInstallments.getInteres(), data.getData().getScheduledPayments().get(0).getInterest().getAmount());
        Assert.assertEquals(firstScheduleInstallments.getDivinte(), data.getData().getScheduledPayments().get(0).getInterest().getCurrency());
        Assert.assertEquals(firstScheduleInstallments.getComisio(), data.getData().getScheduledPayments().get(0).getFee().getAmount());
        Assert.assertEquals(firstScheduleInstallments.getDivcomi(), data.getData().getScheduledPayments().get(0).getFee().getCurrency());

        FormatoMPM0DET secondScheduleInstallments = formatoMPM0DETList.get(1);
        data = createInstallmentPlanMapper.mapOut2(secondScheduleInstallments, data);

        Assert.assertEquals(2, data.getData().getScheduledPayments().size());
        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getData());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getMaturityDate());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getTotal().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getTotal().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getCapital().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getCapital().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getInterest().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getInterest().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getFee().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getFee().getCurrency());

        Assert.assertEquals(secondScheduleInstallments.getFecpago(), data.getData().getScheduledPayments().get(1).getMaturityDate());
        Assert.assertEquals(secondScheduleInstallments.getImpcuot(), data.getData().getScheduledPayments().get(1).getTotal().getAmount());
        Assert.assertEquals(secondScheduleInstallments.getDivcuot(), data.getData().getScheduledPayments().get(1).getTotal().getCurrency());
        Assert.assertEquals(secondScheduleInstallments.getCapital(), data.getData().getScheduledPayments().get(1).getCapital().getAmount());
        Assert.assertEquals(secondScheduleInstallments.getDivcapc(), data.getData().getScheduledPayments().get(1).getCapital().getCurrency());
        Assert.assertEquals(secondScheduleInstallments.getInteres(), data.getData().getScheduledPayments().get(1).getInterest().getAmount());
        Assert.assertEquals(secondScheduleInstallments.getDivinte(), data.getData().getScheduledPayments().get(1).getInterest().getCurrency());
        Assert.assertEquals(secondScheduleInstallments.getComisio(), data.getData().getScheduledPayments().get(1).getFee().getAmount());
        Assert.assertEquals(secondScheduleInstallments.getDivcomi(), data.getData().getScheduledPayments().get(1).getFee().getCurrency());
    }

    @Test
    public void testMapOutCreateInstallmentPlanInitialized() {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputCreateInstallmentPlan();
        InstallmentsPlanData data = createInstallmentPlanMapper.mapOut1(new FormatoMPM0TSC(), dtoIn);

        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getData());
        Assert.assertNull(data.getData().getTerms());
        Assert.assertNull(data.getData().getId());
        Assert.assertNull(data.getData().getFirstInstallmentDate());
        Assert.assertNull(data.getData().getCapital());
        Assert.assertNotNull(data.getData().getRates().get(0).getRateType().getId());
        Assert.assertNotNull(data.getData().getRates().get(0).getRateType().getName());
        Assert.assertNotNull(data.getData().getRates().get(0).getMode().getName());
        Assert.assertNull(data.getData().getRates().get(0).getUnit());
        Assert.assertNotNull(data.getData().getRates().get(1).getRateType().getId());
        Assert.assertNotNull(data.getData().getRates().get(1).getRateType().getName());
        Assert.assertNotNull(data.getData().getRates().get(1).getMode().getName());
        Assert.assertNull(data.getData().getRates().get(1).getUnit());
        Assert.assertNull(data.getData().getInterest());
        Assert.assertNull(data.getData().getTotal());
        Assert.assertNull(data.getData().getFirstInstallment());

        data = createInstallmentPlanMapper.mapOut2(new FormatoMPM0DET(), data);

        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getData());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0));
        Assert.assertNull(data.getData().getScheduledPayments().get(0).getMaturityDate());
        Assert.assertNull(data.getData().getScheduledPayments().get(0).getTotal());
        Assert.assertNull(data.getData().getScheduledPayments().get(0).getCapital());
        Assert.assertNull(data.getData().getScheduledPayments().get(0).getInterest());
        Assert.assertNull(data.getData().getScheduledPayments().get(0).getFee());
    }
}
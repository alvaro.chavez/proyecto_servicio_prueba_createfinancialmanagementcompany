package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.cards.facade.v1.dto.ReimburseCardTransactionTransactionRefund;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.Enums;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static com.bbva.pzic.cards.EntityMock.TRANSACTION_ID;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReimburseCardTransactionTransactionRefundMapperTest {

    @InjectMocks
    private ReimburseCardTransactionTransactionRefundMapper mapper;

    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() throws IOException {
        ReimburseCardTransactionTransactionRefund input = EntityMock.getInstance().getReimburseCardTransactionTransactionRefundMock();
        when(translator.translateFrontendEnumValueStrictly(Enums.TRANSACTIONREFUND_REFUNDTYPE_ID, input.getRefundType().getId())).thenReturn(Constants.REFUND);
        InputReimburseCardTransactionTransactionRefund result = mapper.mapIn(CARD_ID, TRANSACTION_ID, input);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getTransactionId());
        assertNotNull(result.getRefundType());
        assertNotNull(result.getRefundType().getId());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(TRANSACTION_ID, result.getTransactionId());
        assertEquals(Constants.REFUND, result.getRefundType().getId());
    }

    @Test
    public void mapInWithoutRefundType() {
        InputReimburseCardTransactionTransactionRefund result = mapper.mapIn(CARD_ID, TRANSACTION_ID, new ReimburseCardTransactionTransactionRefund());

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getTransactionId());
        assertNull(result.getRefundType());

        assertEquals(CARD_ID, result.getCardId());
        assertEquals(TRANSACTION_ID, result.getTransactionId());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<ReimburseCardTransactionTransactionRefund> result = mapper.mapOut(new ReimburseCardTransactionTransactionRefund());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<ReimburseCardTransactionTransactionRefund> result = mapper.mapOut(null);

        assertNull(result);
    }
}

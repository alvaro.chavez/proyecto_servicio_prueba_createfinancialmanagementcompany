package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateOffersGenerateCards;
import com.bbva.pzic.cards.facade.v0.dto.OfferGenerate;
import com.bbva.pzic.cards.facade.v0.mapper.ICreateOffersGenerateCardsMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 04/11/2020.
 *
 * @author Entelgy.
 */
public class CreateOffersGenerateCardsMapperTest {

    private ICreateOffersGenerateCardsMapper mapper;

    @Before
    public void setUp() {
        mapper = new CreateOffersGenerateCardsMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        OfferGenerate input = EntityMock.getInstance().buildOfferGenerate();
        InputCreateOffersGenerateCards result = mapper.mapIn(input, EntityMock.BCS_DEVICE_SCREEN_SIZE_HEADER);

        assertNotNull(result);
        assertNotNull(result.getBcsDeviceScreenSize());
        assertNotNull(result.getQuestions());
        assertEquals(2, result.getQuestions().size());
        assertNotNull(result.getQuestions().get(0).getId());
        assertNotNull(result.getQuestions().get(0).getAnswerValue());
        assertNotNull(result.getQuestions().get(1).getId());
        assertNotNull(result.getQuestions().get(1).getAnswerValue());

        assertEquals(EntityMock.BCS_DEVICE_SCREEN_SIZE_HEADER, result.getBcsDeviceScreenSize());
        assertEquals(input.getQuestions().get(0).getId(), result.getQuestions().get(0).getId());
        assertEquals(input.getQuestions().get(0).getAnswer().getValue(), result.getQuestions().get(0).getAnswerValue());
        assertEquals(input.getQuestions().get(1).getId(), result.getQuestions().get(1).getId());
        assertEquals(input.getQuestions().get(1).getAnswer().getValue(), result.getQuestions().get(1).getAnswerValue());
    }

    @Test
    public void mapInEmptyTest() {
        InputCreateOffersGenerateCards result = mapper.mapIn(new OfferGenerate(), null);

        assertNotNull(result);
        assertNull(result.getBcsDeviceScreenSize());
        assertNull(result.getQuestions());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponseOK<OfferGenerate> result = mapper.mapOut(new OfferGenerate());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponseOK<OfferGenerate> result = mapper.mapOut(null);

        assertNull(result);
    }
}
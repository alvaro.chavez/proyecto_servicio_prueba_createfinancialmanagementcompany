package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOInputListCardTransactions;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ListCardTransactionsV0MapperTest {

    @InjectMocks
    private ListCardTransactionsV0Mapper transactionsV0Mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Test
    public void testMapInput() {
        final Long pageSize = 123L;

        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS)).thenReturn(CARD_ID);
        final DTOInputListCardTransactions dtoInputListCardTransactions =
                transactionsV0Mapper.mapInput(true, CARD_ENCRYPT_ID, FROM_OPERATION_DATE, TO_OPERATION_DATE, PAGINATION_KEY, pageSize, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS);
        assertNotNull(dtoInputListCardTransactions);

        assertNotNull(dtoInputListCardTransactions.getCardId());
        assertEquals(CARD_ID, dtoInputListCardTransactions.getCardId());

        assertNotNull(dtoInputListCardTransactions.getFromOperationDate());
        assertEquals(FROM_OPERATION_DATE, dtoInputListCardTransactions.getFromOperationDate());

        assertNotNull(dtoInputListCardTransactions.getToOperationDate());
        assertEquals(TO_OPERATION_DATE, dtoInputListCardTransactions.getToOperationDate());


        assertNotNull(dtoInputListCardTransactions.getPaginationKey());
        assertEquals(PAGINATION_KEY, dtoInputListCardTransactions.getPaginationKey());

        assertNotNull(dtoInputListCardTransactions.getPageSize());
        assertEquals(pageSize, dtoInputListCardTransactions.getPageSize());

        assertTrue(dtoInputListCardTransactions.isHaveFinancingType());
    }

    @Test
    public void testMapInputPartial() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS)).thenReturn(CARD_ID);
        final DTOInputListCardTransactions dtoInputListCardTransactions =
                transactionsV0Mapper.mapInput(true, CARD_ENCRYPT_ID, null, null, null, null, RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS);
        assertNotNull(dtoInputListCardTransactions);

        assertNotNull(dtoInputListCardTransactions.getCardId());
        assertEquals(CARD_ID, dtoInputListCardTransactions.getCardId());

        assertNull(dtoInputListCardTransactions.getFromOperationDate());

        assertNull(dtoInputListCardTransactions.getToOperationDate());

        assertNull(dtoInputListCardTransactions.getPaginationKey());

        assertNull(dtoInputListCardTransactions.getPageSize());

        assertTrue(dtoInputListCardTransactions.isHaveFinancingType());
    }
}

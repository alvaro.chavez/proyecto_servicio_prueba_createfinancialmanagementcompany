package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputModifyCardLimit;
import com.bbva.pzic.cards.canonic.AmountLimit;
import com.bbva.pzic.cards.canonic.Limit;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Collections;


/**
 * Created on 20/11/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ModifyCardLimitMapperTest {

    @InjectMocks
    private ModifyCardLimitMapper limitMapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock mock = EntityMock.getInstance();

    @Before
    public void init() {
        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_LIMIT)).thenReturn(EntityMock.CARD_ID);
    }

    @Test
    public void testMapInFull() {
        Limit limit = mock.getLimit();
        InputModifyCardLimit input = limitMapper.mapIn(EntityMock.CARD_ENCRYPT_ID, EntityMock.LIMIT_ID, limit);
        Assert.assertNotNull(input);
        Assert.assertNotNull(input.getCardId());
        Assert.assertNotNull(input.getLimitId());
        Assert.assertNotNull(input.getAmountLimitCurrency());
        Assert.assertNotNull(input.getAmountLimitAmount());
        Assert.assertNotNull(input.getOfferId());

        Assert.assertEquals(input.getCardId(), EntityMock.CARD_ID);
        Assert.assertEquals(input.getLimitId(), input.getLimitId());
        Assert.assertEquals(input.getAmountLimitCurrency(), input.getAmountLimitCurrency());
        Assert.assertEquals(input.getAmountLimitAmount(), input.getAmountLimitAmount());
        Assert.assertEquals(input.getOfferId(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutCardId() {
        Limit limit = mock.getLimit();
        InputModifyCardLimit input = limitMapper.mapIn(null, EntityMock.LIMIT_ID, limit);
        Assert.assertNotNull(input);
        Assert.assertNull(input.getCardId());
        Assert.assertNotNull(input.getLimitId());
        Assert.assertNotNull(input.getAmountLimitCurrency());
        Assert.assertNotNull(input.getAmountLimitAmount());
        Assert.assertNotNull(input.getOfferId());

        Assert.assertEquals(input.getLimitId(), input.getLimitId());
        Assert.assertEquals(input.getAmountLimitCurrency(), input.getAmountLimitCurrency());
        Assert.assertEquals(input.getAmountLimitAmount(), input.getAmountLimitAmount());
        Assert.assertEquals(input.getOfferId(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutLimitId() {
        Limit limit = mock.getLimit();
        InputModifyCardLimit input = limitMapper.mapIn(EntityMock.CARD_ENCRYPT_ID, null, limit);
        Assert.assertNotNull(input);
        Assert.assertNotNull(input.getCardId());
        Assert.assertNull(input.getLimitId());
        Assert.assertNotNull(input.getAmountLimitCurrency());
        Assert.assertNotNull(input.getAmountLimitAmount());
        Assert.assertNotNull(input.getOfferId());

        Assert.assertEquals(input.getCardId(), EntityMock.CARD_ID);
        Assert.assertEquals(input.getAmountLimitCurrency(), input.getAmountLimitCurrency());
        Assert.assertEquals(input.getAmountLimitAmount(), input.getAmountLimitAmount());
        Assert.assertEquals(input.getOfferId(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutLimitAmountLimitsCurrency() {
        Limit limit = mock.getLimit();
        limit.getAmountLimits().get(0).setCurrency(null);
        InputModifyCardLimit input = limitMapper.mapIn(EntityMock.CARD_ENCRYPT_ID, EntityMock.LIMIT_ID, limit);
        Assert.assertNotNull(input);
        Assert.assertNotNull(input.getCardId());
        Assert.assertNotNull(input.getLimitId());
        Assert.assertNull(input.getAmountLimitCurrency());
        Assert.assertNotNull(input.getAmountLimitAmount());
        Assert.assertNotNull(input.getOfferId());

        Assert.assertEquals(input.getCardId(), EntityMock.CARD_ID);
        Assert.assertEquals(input.getLimitId(), input.getLimitId());
        Assert.assertEquals(input.getAmountLimitAmount(), input.getAmountLimitAmount());
        Assert.assertEquals(input.getOfferId(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutLimitAmountLimitsAmount() {
        Limit limit = mock.getLimit();
        limit.getAmountLimits().get(0).setAmount(null);
        InputModifyCardLimit input = limitMapper.mapIn(EntityMock.CARD_ENCRYPT_ID, EntityMock.LIMIT_ID, limit);
        Assert.assertNotNull(input);
        Assert.assertNotNull(input.getCardId());
        Assert.assertNotNull(input.getLimitId());
        Assert.assertNotNull(input.getAmountLimitCurrency());
        Assert.assertNull(input.getAmountLimitAmount());
        Assert.assertNotNull(input.getOfferId());

        Assert.assertEquals(input.getCardId(), EntityMock.CARD_ID);
        Assert.assertEquals(input.getLimitId(), input.getLimitId());
        Assert.assertEquals(input.getAmountLimitCurrency(), input.getAmountLimitCurrency());
        Assert.assertEquals(input.getOfferId(), input.getOfferId());
    }

    @Test
    public void testMapInWithLimitAmountLimitsAmountLimitInitialized() {
        Limit limit = mock.getLimit();
        limit.setAmountLimits(Collections.singletonList(new AmountLimit()));
        InputModifyCardLimit input = limitMapper.mapIn(EntityMock.CARD_ENCRYPT_ID, EntityMock.LIMIT_ID, limit);
        Assert.assertNotNull(input);
        Assert.assertNotNull(input.getCardId());
        Assert.assertNotNull(input.getLimitId());
        Assert.assertNull(input.getAmountLimitCurrency());
        Assert.assertNull(input.getAmountLimitAmount());
        Assert.assertNotNull(input.getOfferId());

        Assert.assertEquals(input.getCardId(), EntityMock.CARD_ID);
        Assert.assertEquals(input.getLimitId(), input.getLimitId());
        Assert.assertEquals(input.getOfferId(), input.getOfferId());
    }

    @Test
    public void testMapInWithLimitAmountLimitsInicialized() {
        Limit limit = mock.getLimit();
        limit.setAmountLimits(Collections.emptyList());
        InputModifyCardLimit input = limitMapper.mapIn(EntityMock.CARD_ENCRYPT_ID, EntityMock.LIMIT_ID, limit);
        Assert.assertNotNull(input);
        Assert.assertNotNull(input.getCardId());
        Assert.assertNotNull(input.getLimitId());
        Assert.assertNull(input.getAmountLimitCurrency());
        Assert.assertNull(input.getAmountLimitAmount());
        Assert.assertNotNull(input.getOfferId());

        Assert.assertEquals(input.getCardId(), EntityMock.CARD_ID);
        Assert.assertEquals(input.getLimitId(), input.getLimitId());
        Assert.assertEquals(input.getOfferId(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutLimitAmountLimits() {
        Limit limit = mock.getLimit();
        limit.setAmountLimits(null);
        InputModifyCardLimit input = limitMapper.mapIn(EntityMock.CARD_ENCRYPT_ID, EntityMock.LIMIT_ID, limit);
        Assert.assertNotNull(input);
        Assert.assertNotNull(input.getCardId());
        Assert.assertNotNull(input.getLimitId());
        Assert.assertNull(input.getAmountLimitCurrency());
        Assert.assertNull(input.getAmountLimitAmount());
        Assert.assertNotNull(input.getOfferId());

        Assert.assertEquals(input.getCardId(), EntityMock.CARD_ID);
        Assert.assertEquals(input.getLimitId(), input.getLimitId());
        Assert.assertEquals(input.getOfferId(), input.getOfferId());
    }

    @Test
    public void testMapInWithoutOfferId() {
        Limit limit = mock.getLimit();
        limit.setOfferId(null);
        InputModifyCardLimit input = limitMapper.mapIn(EntityMock.CARD_ENCRYPT_ID, EntityMock.LIMIT_ID, limit);
        Assert.assertNotNull(input);
        Assert.assertNotNull(input.getCardId());
        Assert.assertNotNull(input.getLimitId());
        Assert.assertNotNull(input.getAmountLimitCurrency());
        Assert.assertNotNull(input.getAmountLimitAmount());
        Assert.assertNull(input.getOfferId());

        Assert.assertEquals(input.getCardId(), EntityMock.CARD_ID);
        Assert.assertEquals(input.getLimitId(), input.getLimitId());
        Assert.assertEquals(input.getAmountLimitCurrency(), input.getAmountLimitCurrency());
        Assert.assertEquals(input.getAmountLimitAmount(), input.getAmountLimitAmount());
    }

    ///mapOut
    @Test
    public void testMapOutFull() {
        Limit limit = new Limit();
        limit.setOperationDate(Calendar.getInstance());
        ServiceResponse<Limit> result = limitMapper.mapOut(limit);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void testMapOutWithoutOperationDate() {
        Limit limit = new Limit();
        ServiceResponse<Limit> result = limitMapper.mapOut(limit);
        Assert.assertNull(result);
    }

    @Test
    public void testMapOutWithLimitNull() {
        ServiceResponse<Limit> result = limitMapper.mapOut(null);
        Assert.assertNull(result);
    }
}

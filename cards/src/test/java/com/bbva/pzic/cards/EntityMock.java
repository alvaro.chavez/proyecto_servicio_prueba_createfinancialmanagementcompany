package com.bbva.pzic.cards;

import com.bbva.jee.arq.spring.core.servicing.utils.Money;
import com.bbva.pzic.cards.business.dto.*;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.Import;
import com.bbva.pzic.cards.canonic.PhysicalSupport;
import com.bbva.pzic.cards.canonic.Proposal;
import com.bbva.pzic.cards.canonic.Reason;
import com.bbva.pzic.cards.canonic.SecurityData;
import com.bbva.pzic.cards.canonic.Status;
import com.bbva.pzic.cards.canonic.Terms;
import com.bbva.pzic.cards.canonic.*;
import com.bbva.pzic.cards.dao.model.mcdv.FormatoMPMS1DV;
import com.bbva.pzic.cards.dao.model.mp6j.FormatoMPMS16J;
import com.bbva.pzic.cards.dao.model.mpb2.FormatoMPM0B2S;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCRMDC0;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCRMDC1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS2L1;
import com.bbva.pzic.cards.dao.model.mpl5.FormatoMPMS1L5;
import com.bbva.pzic.cards.dao.model.mpv1.FormatoMPM0V1S;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.facade.v0.dto.*;
import com.bbva.pzic.cards.facade.v1.dto.*;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.lang.time.DateUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

import static com.bbva.pzic.cards.util.Constants.*;

/**
 * Created on 29/12/2016.
 *
 * @author Entelgy
 */
public class EntityMock {

    public static final String URL = "https://openapi.bbva.com/ccds/covers";
    public static final String HEADER_USER_AGENT_STUB = "C183F266503F3D91037CA3ED75D1E1B035B5D832;Android;Samung;Galaxy Ace 2;512x324;Android;4.4;BMES;4.4;hdpi";
    public static final String CONTEXT_PROVIDER_HEADER_CONTACT_ID = "XP916262";
    public static final String URL_INITIAL_WITHOUT_DIMENSIONS = "pg=0001&bin=491910&default_image=false&v=4&width=&height=&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=true";
    public static final String URL_FINAL_WITH_DIMENSIONS = "pg=0001&bin=491910&default_image=false&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=true";
    public static final String HEADER_SCREEN_SIZE_STUB = "512X324";
    public static final String CONFIGURATION_MANAGER_USER = "Q0FSUkVURVJB";
    public static final String CONFIGURATION_MANAGER_PASSWORD = "Q0FSUkVURVJBX1dT";

    public static final String CIF_01 = "123456789123456789123456789123456789123456789123456789123456789123456789121";
    public static final String CIF_02 = "234567891234567891234567891234567891234567891234567891234567891234567891232";
    public static final String CIF_03 = "912345678912345678912345678912345678912345678912345678912345678912345678913";
    public static final String CIF_04 = "456789123456789123456789123456789456789123456789123456782345678912345678914";
    public static final String CIF_05 = "123456783123123123123123123124554389123456789123456789123456789123456789125";
    public static final String CIF_06 = "912345678912345678912345678912345678912345678912345678912345678912345678916";
    public static final String CIF_07 = "891234567891234567891234567891234567891234567891234567891234567891234567897";
    public static final String CIF_08 = "789123456789123456789123456789123456789123456789123456789123456789123456788";
    public static final String CIF_09 = "678912345678912345678912345678912345678912345678912345678912345678912345679";
    public static final String CIF_10 = "567891234567891234567891234567891234567891234567891234567891234567891234560";
    public static final String CIF_11 = "123456923982397041397123493299567891234567891234567891234567891234567891231";
    public static final String CIF_12 = "567891234567891234567891234567891234567891234567891234234567890991234567892";
    public static final String CIF_13 = "897012347098413809413208947092173408912740917208347102837501230984701328743";
    public static final String CIF_14 = "891423708134208143087142084108123408402138402187341093470127340271340872414";
    public static final String CARD_RELATED_CONTRACT_ENCRYPT = "asdjnjkneq123s";
    public static final String CARD_ID = "1234567890";
    public static final String CARD_ENCRYPT_ID = "##4232$%6778";
    public static final String BLOCK_ID = "INTERNAL";
    public static final String ACTIVATION_ID = "CASHWITHDRAWAL_ACTIVATION";
    public static final String CARD_TYPE_ID_KEY_TESTED = "PAN";
    public static final String CARD_CARDTYPE_ID_KEY_TESTED = "DEBIT_CARD";
    public static final String CARD_BRAND_ASSOCIATION_ID_KEY_TESTED = "VISA";
    public static final String PHYSICAL_SUPPORT_ID = "STICKER";
    public static final String CARD_STATUS_KEY_TESTED = "INOPERATIVE";
    public static final String CARD_RELATED_CONTRACT_KEY_TESTED = "PAN";
    public static final String CARD_DELIVERIES_MANAGEMENT_STATEMENT = "STATEMENT";
    public static final String CARD_DELIVERIES_MANAGEMENT_CARD = "CARD";
    public static final String CARD_DELIVERIES_MANAGEMENT_STICKER = "STICKER";
    public static final String CARD_ENCRYPT_NUMTARJ_KEY_TESTED = "1hhn22-wepaq44g";
    public static final String CARD_ENCRYPT_NUCOREL_KEY_TESTED = "1aban312327";
    public static final String TRANSACTION_ID = "123456";
    public static final String CARD_ID_ENCRYPTED = "##4232$%6778";
    public static final String PUBLIC_KEY = CIF_01 + CIF_02 + CIF_03 + CIF_04 + CIF_05 + CIF_06 + CIF_07 + CIF_08 + CIF_09 + CIF_10 + CIF_11 + CIF_12 + CIF_13 + CIF_14;
    public static final String FINANCIAL_STATEMENT_ID = "1234567890";
    public static final String PROPOSAL_STATUS_CREATED = "CREATED";
    public static final String PROPOSAL_STATUS_FRONTEND_VALUE_3 = "3";

    public static final String CUSTOMER_ID = "12345678";

    public static final String PAGINATION_KEY = "abc123";
    public static final Integer PAGINATION_SIZE = 123;

    public static final List<String> CARD_TYPE_IDS = Arrays.asList("CREDIT_CARD", "DEBIT_CARD", "PREPAID_CARD");

    public static final List<String> STATUS_IDS = Arrays.asList("OPERATIVE", "BLOCKED");
    public static final String CARD_ACTIVATIONS_ID_KEY_TESTED = "CARD_ACTIVATIONS";
    public static final String ECOMMERCE_ACTIVATION = "ECOMMERCE_ACTIVATION";
    public static final String ON_OFF = "ON_OFF";
    public static final String FOREIGN_PURCHASES_ACTIVATION = "FOREIGN_PURCHASES_ACTIVATION";
    public static final String FOREIGN_CASHWITHDRAWAL_ACTIVATION = "FOREIGN_CASHWITHDRAWAL_ACTIVATION";
    public static final String LIMIT_ID = "12345678901234567890";
    public static final List<String> PARTICIPANTS_PARTICIPANT_TYPE_ID = Arrays.asList("HOLDER", "AUTHORIZED");

    public static final String STATEMENT_TYPE = "S";

    public static final String PROPERTY_CONDITIONS_OUTCOMES_OUTCOME_TYPE_ID_VALUE_TESTED = "01";
    public static final String PROPERTY_CONDITIONS_OUTCOMES_OUTCOME_TYPE_ID_KEY_TESTED = "FEE_AMOUNT";
    public static final Boolean IS_ACTIVE = Boolean.TRUE;

    public static final String MEDIA_TYPE_PDF = "application/pdf";

    public static final String FROM_OPERATION_DATE = "2015-12-01";
    public static final String TO_OPERATION_DATE = "2015-12-31";
    public static final String ID_CONTRATO = "JP0001";
    public static final String ID_GRUPO = "5";
    public static final String CLIENT_ID = "90016482";
    public static final String OFFER_ID = "1002548986";

    public static final String ENUM_CARDS_CARD_TYPE_ID = "ADD";
    public static final String ENUM_CARDS_ITEMIZE_FEES_FEE_TYPE0 = "WER";
    public static final String ENUM_CARDS_ITEMIZE_FEES_FEE_TYPE1 = "AER";
    public static final String ENUM_CARDS_ITEMIZE_FEES_MODE0 = "QWR";
    public static final String ENUM_CARDS_ITEMIZE_FEES_MODE1 = "RWR";
    public static final String ENUM_CARDS_ADDITIONAL_PRODUCTS_PRODUCT_TYPE = "TTR";
    public static final String ENUM_CARDS_PERIOD_ID = "123";
    public static final String ENUM_CARDS_PAYMENT_METHOD_ID = "EDF";
    public static final String ENUM_CANCELLATION_VERIFICATION_RATE_UNIT_RATE_TYPE_AMOUNT = "AMOUNT";
    public static final String ENUM_CANCELLATION_VERIFICATION_CONDITION_FORMAT_TYPE_AMOUNT = "AMOUNT";

    public static final String FRECCUO_VAL = "MONTHLY";
    public static final String FRECCUO_M = "M";
    public static final String PERCENTAGE_VAL = "PERCENTAGE";
    public static final String PERCENTAGE_P = "P";
    public static final String AMOUNT_VAL = "AMOUNT";
    public static final String AMOUNT_M = "M";

    public static final String RELATED_CONTRACT_ID = "0011019923456789";
    public static final String RELATED_CONTRACT_ENCRYPT_ID = "1aban312327";
    public static final String NUMBER = "4140680123456789";
    public static final String MASKED_NUMBER = "*********3456789";

    public static final String NUMBER_TYPE_ID_1 = "1";
    public static final String NUMBER_TYPE_ID_PAN = "PAN";
    public static final String PRODUCT_ID_50 = "50";
    public static final String PRODUCT_ID_CARDS = "CARDS";

    public static final String DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_ENUM = "DNI";
    public static final String DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND = "L";
    public static final String HOME_CARDS_DELIVERY_DESTINATION_ID_ENUM = "HOME";
    public static final String HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND = "H";
    public static final String CREDIT_CARD_CARDS_CAR_TYPE_ID_ENUM = "CREDIT_CARD";
    public static final String CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND = "C";

    public static final String EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM = "EMAIL";
    public static final String EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND = "MA";
    public static final String PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM = "PHONE_NUMBER";
    public static final String PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND = "TF";

    public static final String CCC_TIPREEM = "01";
    public static final String CCC_REFUNDTYPE = "COLLECT_IN_ACCOUNT";
    public static final String CCC_TIPPREC = "V";
    public static final String CCC_PRICETYPE = "SALE";
    public static final String CCC_TIPOIMP = "01";
    public static final String CCC_TAXTYPE = "FINANCIAL_TRANSACTIONS";

    public static final String CONTACT_TYPE_SPECIFIC_ENUM = "N";
    public static final String CONTACT_TYPE_STORED_ENUM = "E";
    public static final String CONTACT_DETAIL_TYPE_EMAIL_ENUM = "E";
    public static final String CONTACT_DETAIL_TYPE_LANDLINE_ENUM = "L";
    public static final String CONTACT_DETAIL_TYPE_MOBILE_ENUM = "M";
    public static final String CONTACT_DETAIL_TYPE_SOCIAL_MEDIA_ENUM = "S";

    public static final String PHONE_TYPE_LANDLINE_ENUM = "0";
    public static final String SOCIAL_NETWORK_SOCIAL_MEDIA_ENUM_FRONTEND = "FACEBOOK";
    public static final String SOCIAL_NETWORK_SOCIAL_MEDIA_ENUM = "0";

    public static final String DESTINATION_ID_ENUM = "C";

    public static final String PROPOSAL_ID = "10";
    public static final String SHIPMENT_ID = "123";
    public static final String CARD_AGREEMENT = "231";
    public static final String CARD_REFERENCE_NUMBER = "432";
    public static final String CARD_EXTERNAL_CODE = "4234";
    public static final String CARD_SHIPPING_COMPANY_ID = "536654";
    public static final String ADDRESSES_EXPAND = "addresses";
    public static final String STUB_ACTIVATION_ID = "DCVV";
    public static final String CUSTOMER_CODE = "87654321";
    public static final String REFUND_TYPE_ID_KEY = "A";
    public static final String REFUND_TYPE_ID_VALUE = "REFUND";
    public static final String CARD_CANCELLATIONS_REASONID_FRONTEND = "UNEXPECTED_EXPENSES";
    public static final String CARD_CANCELLATIONS_REASONID_BACKEND = "CP";
    public static final String CARD_CANCELLATIONS_STATUSID_FRONTEND = "COMPLETED";
    public static final String CARD_CANCELLATIONS_STATUSID_BACKEND = "01";
    public static final String CARDS_NUMBERTYPE_ID_FRONTEND = "PAN";
    public static final String CARDS_NUMBERTYPE_ID_BACKEND = "1";
    public static final String BCS_DEVICE_SCREEN_SIZE_HEADER = "123";
    private static final EntityMock INSTANCE = new EntityMock();

    private final ObjectMapperHelper objectMapper;

    private EntityMock() {
        objectMapper = ObjectMapperHelper.getInstance();
    }

    public static EntityMock getInstance() {
        return INSTANCE;
    }

    public static DTOInputCreateCashRefund getDTOInputCreateCashRefund() {
        DTOInputCreateCashRefund dtoInputCreateCashRefund = new DTOInputCreateCashRefund();
        dtoInputCreateCashRefund.setCardId(CARD_ENCRYPT_ID);
        DTOIntCashRefund dtoIntCashRefund = new DTOIntCashRefund();
        dtoIntCashRefund.setRefundType(CCC_TIPREEM);
        DTOIntRefundAmount dtoIntRefundAmount = new DTOIntRefundAmount();
        dtoIntRefundAmount.setCurrency("PEN");
        dtoIntCashRefund.setRefundAmount(dtoIntRefundAmount);
        DTOIntReceivingAccount dtoIntReceivingAccount = new DTOIntReceivingAccount();
        dtoIntReceivingAccount.setId("001101300229073025");
        dtoIntCashRefund.setReceivingAccount(dtoIntReceivingAccount);
        dtoInputCreateCashRefund.setCashRefund(dtoIntCashRefund);
        return dtoInputCreateCashRefund;
    }

    public static FormatoMCRMDC0 getFormatoMCRMDC0() {
        FormatoMCRMDC0 formatoMCRMDC0 = new FormatoMCRMDC0();
        formatoMCRMDC0.setIdoper("0012019");
        formatoMCRMDC0.setDesoper("RETIRO SALDO ACREEDOR");
        formatoMCRMDC0.setTipreem(CCC_TIPREEM);
        formatoMCRMDC0.setIsalacr(new BigDecimal("14000.00"));
        formatoMCRMDC0.setDivsacr("PEN");
        formatoMCRMDC0.setNcuenta("001101300229073025");
        formatoMCRMDC0.setFecoper(new GregorianCalendar(2016, Calendar.NOVEMBER, 30).getTime());
        formatoMCRMDC0.setHoraope("17:28:34");
        formatoMCRMDC0.setFecfact(new GregorianCalendar(2016, Calendar.NOVEMBER, 30).getTime());
        formatoMCRMDC0.setHorafac("17:30:54");
        formatoMCRMDC0.setIsalcta(new BigDecimal("14000.00"));
        formatoMCRMDC0.setDivsact("PEN");
        formatoMCRMDC0.setTipcam(new BigDecimal("3.38"));
        formatoMCRMDC0.setTipprec(CCC_TIPPREC);
        formatoMCRMDC0.setTotimp(new BigDecimal("17.50"));
        formatoMCRMDC0.setDivtoti("PEN");
        return formatoMCRMDC0;
    }

    public static FormatoMCRMDC1 getFormatoMCRMDC1() {
        FormatoMCRMDC1 formatoMCRMDC1 = new FormatoMCRMDC1();
        formatoMCRMDC1.setTipoimp(CCC_TIPOIMP);
        formatoMCRMDC1.setMonimp(new BigDecimal("0.10"));
        formatoMCRMDC1.setDivimp("PEN");
        return formatoMCRMDC1;
    }

    public static CashRefund getCashRefund() {
        CashRefund cashRefund = new CashRefund();
        cashRefund.setRefundType(CCC_REFUNDTYPE);
        RefundAmount refundAmount = new RefundAmount();
        refundAmount.setCurrency("PEN");
        cashRefund.setRefundAmount(refundAmount);
        ReceivingAccount receivingAccount = new ReceivingAccount();
        receivingAccount.setId("001101300229073025");
        cashRefund.setReceivingAccount(receivingAccount);
        return cashRefund;
    }

    public static DTOIntCashRefund getDTOIntCashRefund() {
        DTOIntCashRefund dtoIntCashRefund = new DTOIntCashRefund();
        dtoIntCashRefund.setId("0012019");
        dtoIntCashRefund.setDescription("RETIRO SALDO ACREEDOR");
        dtoIntCashRefund.setRefundType(CCC_TIPREEM);
        DTOIntRefundAmount dtoIntRefundAmount = new DTOIntRefundAmount();
        dtoIntRefundAmount.setAmount(new BigDecimal("14001.00"));
        dtoIntRefundAmount.setCurrency("PEN");
        dtoIntCashRefund.setRefundAmount(dtoIntRefundAmount);
        DTOIntReceivingAccount dtoIntReceivingAccount = new DTOIntReceivingAccount();
        dtoIntReceivingAccount.setId("001101300229073025");
        dtoIntCashRefund.setReceivingAccount(dtoIntReceivingAccount);
        Date operationDate = new GregorianCalendar(2016, Calendar.NOVEMBER, 30, 17, 28, 34).getTime();
        dtoIntCashRefund.setOperationDate(getCalendar(operationDate));
        Date accountingDate = new GregorianCalendar(2016, Calendar.NOVEMBER, 30, 17, 30, 54).getTime();
        dtoIntCashRefund.setAccoutingDate(getCalendar(accountingDate));
        DTOIntReceivedAmount dtoIntReceivedAmount = new DTOIntReceivedAmount();
        dtoIntReceivedAmount.setAmount(new BigDecimal("14000.00"));
        dtoIntReceivedAmount.setCurrency("PEN");
        dtoIntCashRefund.setReceivedAmount(dtoIntReceivedAmount);
        DTOIntExchangeRate dtoIntExchangeRate = new DTOIntExchangeRate();
        Date exchangeRateDate = new GregorianCalendar(2016, Calendar.NOVEMBER, 30, 17, 28, 34).getTime();
        dtoIntExchangeRate.setDate(getCalendar(exchangeRateDate));
        DTOIntValues values = new DTOIntValues();
        DTOIntFactor factor = new DTOIntFactor();
        factor.setValue(new BigDecimal("3.38"));
        values.setFactor(factor);
        values.setPriceType(CCC_TIPPREC);
        dtoIntExchangeRate.setValues(values);
        dtoIntCashRefund.setExchangeRate(dtoIntExchangeRate);
        DTOIntTaxes dtoIntTaxes = new DTOIntTaxes();
        DTOIntTotalTaxes dtoIntTotalTaxes = new DTOIntTotalTaxes();
        dtoIntTotalTaxes.setAmount(new BigDecimal("17.50"));
        dtoIntTotalTaxes.setCurrency("PEN");
        dtoIntTaxes.setTotalTaxes(dtoIntTotalTaxes);

        DTOIntItemizeTaxes dtoIntItemizeTaxes = new DTOIntItemizeTaxes();
        dtoIntItemizeTaxes.setTaxType(CCC_TIPOIMP);
        DTOIntMonetaryAmount dtoIntMonetaryAmount = new DTOIntMonetaryAmount();
        dtoIntMonetaryAmount.setAmount(new BigDecimal("0.10"));
        dtoIntMonetaryAmount.setCurrency("PEN");
        dtoIntItemizeTaxes.setMonetaryAmount(dtoIntMonetaryAmount);
        List<DTOIntItemizeTaxes> dtoIntItemizeTaxesList = new ArrayList<>();
        dtoIntItemizeTaxesList.add(dtoIntItemizeTaxes);
        dtoIntTaxes.setItemizeTaxes(dtoIntItemizeTaxesList);

        dtoIntCashRefund.setTaxes(dtoIntTaxes);

        return dtoIntCashRefund;
    }

    private static Calendar getCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public DTOIntListCards getDtoIntListCards() {
        DTOIntListCards dtoIntListCards = new DTOIntListCards();
        dtoIntListCards.setCustomerId("CPD0001");
        dtoIntListCards.setCardTypeId(CARD_TYPE_IDS);
        dtoIntListCards.setPhysicalSupportId("NORMAL_PLASTIC");
        dtoIntListCards.setStatusId(STATUS_IDS);
        dtoIntListCards.setParticipantsParticipantTypeId(PARTICIPANTS_PARTICIPANT_TYPE_ID);
        dtoIntListCards.setPageSize(123);
        dtoIntListCards.setPaginationKey("abc123");
        return dtoIntListCards;
    }

    public FormatoMPMS2L1 getFormatoMPMS2L1() {
        FormatoMPMS2L1 mpms2L1 = new FormatoMPMS2L1();
        mpms2L1.setIdpagin("123");
        mpms2L1.setTampagi(123);
        return mpms2L1;
    }

    public DTOOutListCards getDtoOutListCards() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/DTOOutListCards.json"), DTOOutListCards.class);
    }

    public DTOInputCreateCardRelatedContract getDtoInputCreateRelatedContract() {
        DTOInputCreateCardRelatedContract dtoInputCreateCardRelatedContract = new DTOInputCreateCardRelatedContract();
        dtoInputCreateCardRelatedContract.setCardId("CAR-1234");
        dtoInputCreateCardRelatedContract.setContractId("45671234");
        return dtoInputCreateCardRelatedContract;
    }

    public FormatoMPM0V1S geFormatoMPM0V1S() {
        FormatoMPM0V1S formatoOut = new FormatoMPM0V1S();
        formatoOut.setIdcorel("1ab23nsan3-12327");
        formatoOut.setNucorel("4567");
        formatoOut.setIdtcore("1");
        formatoOut.setDetcore("International Bank Account Number");
        return formatoOut;
    }

    public com.bbva.pzic.cards.canonic.RelatedContract getRelatedContract() {
        com.bbva.pzic.cards.canonic.RelatedContract relatedContract = new com.bbva.pzic.cards.canonic.RelatedContract();
        relatedContract.setContractId("#$5&&/%%1234");
        relatedContract.setRelatedContractId("asoeFienDcr");
        return relatedContract;
    }

    public DTOOutCreateCardRelatedContract geDtoOutCreateRelatedContract() {
        DTOOutCreateCardRelatedContract dtoOut = new DTOOutCreateCardRelatedContract();
        dtoOut.setRelatedContractId("9087 6542 3456 1234 0987");
        dtoOut.setContractId("CON_4567");
        dtoOut.setNumberTypeId("PAN");
        dtoOut.setNumberTypeName("International Bank Account Number");
        return dtoOut;
    }

    public Block buildBlock() {
        final Block block = new Block();
        Reason reason = new Reason();
        reason.setId("re");
        block.setBlockId("CANCELACION X FRAUDE");
        block.setReason(reason);
        block.setIsActive(Boolean.TRUE);
        block.setIsReissued(Boolean.TRUE);
        block.setAdditionalInformation("Cualquier cosa");
        return block;
    }

    public DTOIntBlock buildDTOIntBlock() {
        final DTOIntCard card = new DTOIntCard();
        card.setCardId(CARD_ID);
        final DTOIntBlock block = new DTOIntBlock();
        block.setCard(card);
        block.setReasonId("LO");
        block.setBlockId(BLOCK_ID);
        block.setIsActive("S");
        block.setIsReissued("S");
        block.setAdditionalInformation("Cualquier cosa");
        return block;
    }

    public Activation buildActivation() {
        final Activation activation = new Activation();
        activation.setIsActive(true);
        return activation;
    }

    public DTOIntActivation buildDTOIntActivation() {
        final DTOIntCard card = new DTOIntCard();
        card.setCardId(CARD_ID);
        final DTOIntActivation dtoIntActivation = new DTOIntActivation();
        dtoIntActivation.setCard(card);
        dtoIntActivation.setActivationId(ACTIVATION_ID);
        dtoIntActivation.setIsActive(true);
        return dtoIntActivation;
    }

    public DTOInputListCardTransactions buildDTOInputListTransactions() {
        final DTOInputListCardTransactions dtoInputListCardTransactions = new DTOInputListCardTransactions();
        dtoInputListCardTransactions.setCardId(CARD_ID);
        dtoInputListCardTransactions.setFromOperationDate(FROM_OPERATION_DATE);
        dtoInputListCardTransactions.setToOperationDate(TO_OPERATION_DATE);
        dtoInputListCardTransactions.setPaginationKey(PAGINATION_KEY);
        dtoInputListCardTransactions.setPageSize(123L);
        dtoInputListCardTransactions.setRegistryId(RegistryIds.SMC_REGISTRY_ID_OF_LIST_CARD_TRANSACTIONS);
        dtoInputListCardTransactions.setHaveFinancingType(Boolean.TRUE);
        return dtoInputListCardTransactions;
    }

    public SecurityData getSecurityData() {
        SecurityData securityData = new SecurityData();
        securityData.setId("1234");
        securityData.setName("Codigo de seguridad");
        securityData.setCode("3333");
        return securityData;
    }

    public InputGetCardSecurityData getInputGetCardSecurityData() {
        InputGetCardSecurityData input = new InputGetCardSecurityData();
        input.setCardId(CARD_ID);
        input.setPublicKey(CIF_01 + CIF_02 + CIF_03 + CIF_04 + CIF_05 + CIF_06 + CIF_07 + CIF_08 + CIF_09
                + CIF_10 + CIF_11 + CIF_12 + CIF_13 + CIF_14);
        return input;
    }

    public Card getCard() {
        Card card = new Card();
        com.bbva.pzic.cards.canonic.CardType cardType = new com.bbva.pzic.cards.canonic.CardType();
        cardType.setId("DEBIT_CARD");
        PhysicalSupport physicalSupport = new PhysicalSupport();
        physicalSupport.setId("STICKER");
        Status status = new Status();
        status.setId("OPERATIVE");
        StatusReason reason = new StatusReason();
        reason.setId("01");
        status.setReason(reason);
        List<com.bbva.pzic.cards.canonic.RelatedContract> relatedContracts = new ArrayList<>();
        com.bbva.pzic.cards.canonic.RelatedContract relatedContract = new com.bbva.pzic.cards.canonic.RelatedContract();
        relatedContract.setContractId(CARD_RELATED_CONTRACT_ENCRYPT);
        relatedContracts.add(relatedContract);

        card.setRelatedContracts(relatedContracts);
        card.setCardType(cardType);
        card.setPhysicalSupport(physicalSupport);
        card.setStatus(status);
        return card;
    }

    public DTOIntCard buildDTOIntCard() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/dtoIntCard.json"), DTOIntCard.class);
    }

    public DTOIntCard getInputCreateCard() {
        DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardTypeId("D");
        dtoIntCard.setPhysicalSupportId("S");
        dtoIntCard.setRelatedContractId("1234567891235467");

        return dtoIntCard;
    }

    public InstallmentsPlan getInputInstallmentsPlan() {
        InstallmentsPlan dtoInt = new InstallmentsPlan();
        dtoInt.setTransactionId(TRANSACTION_ID);
        Terms terms = new Terms();
        terms.setNumber(3);
        dtoInt.setTerms(terms);

        return dtoInt;
    }

    public DTOIntCardInstallmentsPlan buildDTOInputCreateInstallmentPlan() {
        DTOIntCardInstallmentsPlan dtoInput = new DTOIntCardInstallmentsPlan();
        dtoInput.setCardId(CARD_ID);
        dtoInput.setTransactionId(TRANSACTION_ID);
        dtoInput.setTermsNumber(3);

        return dtoInput;
    }

    public InstallmentsPlanSimulation getInstallmentsPlanSimulation() {
        InstallmentsPlanSimulation dtoInt = new InstallmentsPlanSimulation();
        dtoInt.setTransactionId("12342");
        Money capital = new Money();
        capital.setAmount(new BigDecimal(5000));
        capital.setCurrency("PEN");
        dtoInt.setCapital(capital);

        Terms terms = new Terms();
        terms.setNumber(3);
        dtoInt.setTerms(terms);

        return dtoInt;
    }

    public DTOIntCardInstallmentsPlan buildDTOInputSimulateInstallmentPlan() {
        DTOIntCardInstallmentsPlan dtoInput = new DTOIntCardInstallmentsPlan();
        dtoInput.setCardId(CARD_ID);
        dtoInput.setTransactionId(TRANSACTION_ID);
        dtoInput.setTermsNumber(3);
        dtoInput.setAmount(new BigDecimal(5000));
        dtoInput.setCurrency("PEN");
        dtoInput.setTransactionId("43212");

        return dtoInput;
    }

    public FormatoMPM0B2S buildFormatoMPM0B2S() {
        FormatoMPM0B2S formatoMPM0B2S = new FormatoMPM0B2S();
        formatoMPM0B2S.setIdebloq("LO");
        formatoMPM0B2S.setDesbloq("Additional Information");
        formatoMPM0B2S.setIderazo("reason Id");
        formatoMPM0B2S.setDesrazo("reason Descrip");
        formatoMPM0B2S.setMcnbloq(123456);
        formatoMPM0B2S.setFecbloq(new Date());
        formatoMPM0B2S.setHorablq("06:34:55");
        return formatoMPM0B2S;
    }

    public DTOIntPin buildDTOIntPin() {
        DTOIntPin dtoIntPin = new DTOIntPin();
        dtoIntPin.setCardId(CARD_ID);
        dtoIntPin.setPin(CIF_01 + CIF_02 + CIF_03 + CIF_04 + CIF_05 + CIF_06 + CIF_07 + CIF_08 + CIF_09
                + CIF_10 + CIF_11 + CIF_12 + CIF_13 + CIF_14);

        return dtoIntPin;
    }

    public List<Activation> buildListActivation() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/activations.json"), new TypeReference<List<Activation>>() {
        });
    }

    public InputModifyCardActivations buildDTOInputItemsActivations() {
        InputModifyCardActivations dtoInput = new InputModifyCardActivations();
        dtoInput.setCardId(CARD_ID);
        DTOIntActivation dtoIntActivationPosition1 = new DTOIntActivation();
        dtoIntActivationPosition1.setIsActive(true);
        dtoIntActivationPosition1.setActivationId("01");

        DTOIntActivation dtoIntActivationPosition2 = new DTOIntActivation();
        dtoIntActivationPosition2.setIsActive(true);
        dtoIntActivationPosition2.setActivationId("02");

        DTOIntActivation dtoIntActivationPosition3 = new DTOIntActivation();
        dtoIntActivationPosition3.setIsActive(false);
        dtoIntActivationPosition3.setActivationId("03");

        DTOIntActivation dtoIntActivationPosition4 = new DTOIntActivation();
        dtoIntActivationPosition4.setIsActive(false);
        dtoIntActivationPosition4.setActivationId("04");

        DTOIntActivation dtoIntActivationPosition5 = new DTOIntActivation();
        dtoIntActivationPosition5.setIsActive(true);
        dtoIntActivationPosition5.setActivationId("05");

        dtoInput.setDtoIntActivationPosition1(dtoIntActivationPosition1);
        dtoInput.setDtoIntActivationPosition2(dtoIntActivationPosition2);
        dtoInput.setDtoIntActivationPosition3(dtoIntActivationPosition3);
        dtoInput.setDtoIntActivationPosition4(dtoIntActivationPosition4);
        dtoInput.setDtoIntActivationPosition5(dtoIntActivationPosition5);
        return dtoInput;
    }

    public Limit getLimit() {
        Limit limit = new Limit();
        limit.setAmountLimits(new ArrayList<>());
        limit.getAmountLimits().add(getAmountLimit());
        limit.setOfferId("1234567890");
        return limit;
    }

    private AmountLimit getAmountLimit() {
        AmountLimit amountLimit = new AmountLimit();
        amountLimit.setAmount(new BigDecimal("1234567890123.12"));
        amountLimit.setCurrency("PER");
        return amountLimit;
    }

    public InputModifyCardLimit getInputModifyCardLimit() {
        InputModifyCardLimit input = new InputModifyCardLimit();
        input.setCardId("1234567890123456789");
        input.setLimitId("12345678901234567890");
        input.setAmountLimitCurrency("PER");
        input.setOfferId("1234567890");
        input.setAmountLimitAmount(new BigDecimal("1234567890123.12"));
        return input;
    }

    public InputGetCardTransaction buildInputGetCardTransaction() {
        InputGetCardTransaction input = new InputGetCardTransaction();
        input.setCardId(CARD_ID);
        input.setTransactionId(TRANSACTION_ID);
        return input;
    }

    public Card getCardV0() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/dto_card_mapper_in_facade.json"), Card.class);
    }

    public InputCreateCard getInputCreateCardFull() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/input_create_card_full.json"), InputCreateCard.class);
    }

    public InputCreateCard getInputCreateCardAdvanceFull() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/input_create_card_advance_full.json"), InputCreateCard.class);
    }

    public InputListInstallmentPlans getInputListInstallmentPlans() {
        InputListInstallmentPlans installmentPlans = new InputListInstallmentPlans();
        installmentPlans.setCardId(CARD_ID);
        installmentPlans.setPageSize(123L);
        installmentPlans.setPaginationKey("abc");
        return installmentPlans;
    }

    public InputGetCardConditions getInputGetConditions() {
        InputGetCardConditions input = new InputGetCardConditions();
        input.setCardId(CARD_ENCRYPT_ID);
        return input;
    }

    public InputListCardFinancialStatements getInputListCardFinancialStatements() {
        InputListCardFinancialStatements input = new InputListCardFinancialStatements();
        input.setIsActive(Boolean.TRUE);
        input.setCardId("1234567890123456");
        input.setPaginationKey("abc12");
        input.setPageSize(123);
        return input;
    }

    public DTOIntStatementList getDtoOutStatementList() {
        DTOIntStatementList dtoIntStatementList = new DTOIntStatementList();
        dtoIntStatementList.setData(new ArrayList<>());
        dtoIntStatementList.getData().add(getStatement());
        return dtoIntStatementList;
    }

    private Statement getStatement() {
        Statement statement = new Statement();
        statement.setId("123");
        statement.setCutOffDate(new Date());
        statement.setIsActive(Boolean.TRUE);
        return statement;
    }

    public InputGetCardFinancialStatement getCardFinancialStatement() {
        InputGetCardFinancialStatement inputGetCardFinancialStatement = new InputGetCardFinancialStatement();
        inputGetCardFinancialStatement.setCardId(CARD_ID);
        inputGetCardFinancialStatement.setFinancialStatementId(FINANCIAL_STATEMENT_ID);
        return inputGetCardFinancialStatement;
    }

    public DTOIntCardStatement getDTOIntCardStatement() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/dtoIntCardStatement.json"), DTOIntCardStatement.class);
    }

    public InputGetCardsCardOffer getInputGetCardsCardOffer() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputGetCardsCardOffer.json"), InputGetCardsCardOffer.class);
    }

    public DTOIntDetailSimulation getDTOIntCardOfferSimulate() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("mock/dtoIntCardOfferSimulate.json"),
                DTOIntDetailSimulation.class);
    }

    public DTOIntCashAdvancesSearchCriteria getDTOIntCashAdvancesSearchCriteria() {
        DTOIntCashAdvancesSearchCriteria dtoIntCashAdvancesSearchCriteria = new DTOIntCashAdvancesSearchCriteria();
        dtoIntCashAdvancesSearchCriteria.setCardId(CARD_ID);
        return dtoIntCashAdvancesSearchCriteria;
    }

    public DTOIntListCashAdvances getDTOIntListCashAdvances() {
        DTOIntListCashAdvances dtoIntListCashAdvances = new DTOIntListCashAdvances();
        dtoIntListCashAdvances.setData(Collections.singletonList(new CashAdvances()));
        return dtoIntListCashAdvances;
    }

    public Proposal getProposal() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/proposal.json"), Proposal.class);
    }

    public DTOIntProposal getDtoIntProposal() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/dtoIntProposal.json"), DTOIntProposal.class);
    }

    public FormatoMPMS1L5 buildFormatoMPMS1L5() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/FormatoMPMS1L5.json"), FormatoMPMS1L5.class);
    }

    public List<RelatedContracts> buildRelatedContracts() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/RelatedContracts.json"), new TypeReference<List<DTOIntRelatedContracts>>() {
        });
    }

    public InputCreateCardsCardProposal getInputCreateCardsCardProposal() throws IOException {
        InputCreateCardsCardProposal inputCreateCardsCardProposal = new InputCreateCardsCardProposal();
        inputCreateCardsCardProposal.setCardId(CARD_ID);
        inputCreateCardsCardProposal.setCardProposal(objectMapper.readValue(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("mock/dtoIntCardProposal.json"),
                DTOIntCardProposal.class));
        return inputCreateCardsCardProposal;
    }

    public CardProposal getCardProposal() throws IOException {
        return objectMapper.readValue(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("mock/cardProposal.json"),
                CardProposal.class);
    }

    public HolderSimulation getHolderSimulation() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/holderSimulation.json"), HolderSimulation.class);
    }

    public MaskedToken buildMaskedToken() throws ParseException {
        MaskedToken maskedToken = new MaskedToken();
        maskedToken.setExpirationDate(DateUtils.parseDate("2018-12-15", new String[]{"yyyy-MM-dd"}));
        return maskedToken;
    }

    public InputCreateCardsMaskedToken buildInputCreateCardsMaskedToken() {
        InputCreateCardsMaskedToken input = new InputCreateCardsMaskedToken();
        input.setCardId(CARD_ID);
        input.setExpirationDate("122020");
        return input;
    }

    public ProposalCard buildProposalCard() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/proposalCard.json"), ProposalCard.class);
    }

    public InputListCardProposals buildInputListCardProposals() {
        InputListCardProposals input = new InputListCardProposals();
        input.setStatus(PROPOSAL_STATUS_CREATED);
        return input;
    }

    public InputCreateCardProposal buildInputCreateCardProposal() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputCreateCardProposal.json"), InputCreateCardProposal.class);
    }

    public List<Card> fakeCards() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/cards.json"), new TypeReference<List<Card>>() {
        });
    }

    public List<HolderSimulation> fakeCardsHolderSimulation() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/cardHolderSimulation.json"), new TypeReference<List<HolderSimulation>>() {
        });
    }

    public InputModifyCardProposal buildInputModifyCardProposal() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputModifyCardProposal.json"), InputModifyCardProposal.class);
    }

    public CardPost buildCardPost() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/cardPostV1.json"), CardPost.class);
    }

    public com.bbva.pzic.cards.facade.v1.dto.Delivery buildDeliverySpecificEmail() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/input_create_card_delivery_specific_email.json"), com.bbva.pzic.cards.facade.v1.dto.Delivery.class);
    }

    public com.bbva.pzic.cards.facade.v1.dto.Delivery buildDeliverySpecificLandline() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/input_create_card_delivery_specific_landline.json"), com.bbva.pzic.cards.facade.v1.dto.Delivery.class);
    }

    public com.bbva.pzic.cards.facade.v1.dto.Delivery buildDeliverySpecificMobile() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/input_create_card_delivery_specific_mobile.json"), com.bbva.pzic.cards.facade.v1.dto.Delivery.class);
    }

    public com.bbva.pzic.cards.facade.v1.dto.Delivery buildDeliverySpecificSocialMedia() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/input_create_card_delivery_specific_social_media.json"), com.bbva.pzic.cards.facade.v1.dto.Delivery.class);
    }

    public com.bbva.pzic.cards.facade.v1.dto.Delivery buildDeliveryStored() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/input_create_card_delivery_stored.json"), com.bbva.pzic.cards.facade.v1.dto.Delivery.class);
    }

    public DTOIntDelivery buildDTOIntDeliverySpecificEmail() throws IOException {
        DTOIntDelivery dtoIntDelivery = objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/input_create_card_delivery_specific_email.json"), DTOIntDelivery.class);
        dtoIntDelivery.setCardId(CARD_ID);
        dtoIntDelivery.setServiceTypeId(STATEMENT_TYPE);
        dtoIntDelivery.getContact().setContactTypeOriginal(SPECIFIC);
        dtoIntDelivery.getContact().getContact().setContactDetailTypeOriginal(EMAIL);
        return dtoIntDelivery;
    }

    public DTOIntDelivery buildDTOIntDeliverySpecificLandline() throws IOException {
        DTOIntDelivery dtoIntDelivery = objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/input_create_card_delivery_specific_landline.json"), DTOIntDelivery.class);
        dtoIntDelivery.setCardId(CARD_ID);
        dtoIntDelivery.setServiceTypeId(STATEMENT_TYPE);
        dtoIntDelivery.getContact().setContactTypeOriginal(SPECIFIC);
        dtoIntDelivery.getContact().getContact().setCountryId("CA");
        dtoIntDelivery.getContact().getContact().setContactDetailTypeOriginal(LANDLINE);
        return dtoIntDelivery;
    }

    public DTOIntDelivery buildDTOIntDeliverySpecificMobile() throws IOException {
        DTOIntDelivery dtoIntDelivery = objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/input_create_card_delivery_specific_mobile.json"), DTOIntDelivery.class);
        dtoIntDelivery.setCardId(CARD_ID);
        dtoIntDelivery.setServiceTypeId(STATEMENT_TYPE);
        dtoIntDelivery.getContact().setContactTypeOriginal(SPECIFIC);
        dtoIntDelivery.getContact().getContact().setContactDetailTypeOriginal(MOBILE);
        return dtoIntDelivery;
    }

    public DTOIntDelivery buildDTOIntDeliverySpecificSocialMedia() throws IOException {
        DTOIntDelivery dtoIntDelivery = objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/input_create_card_delivery_specific_social_media.json"), DTOIntDelivery.class);
        dtoIntDelivery.setCardId(CARD_ID);
        dtoIntDelivery.setServiceTypeId(STATEMENT_TYPE);
        dtoIntDelivery.getContact().setContactTypeOriginal(SPECIFIC);
        dtoIntDelivery.getContact().getContact().setContactDetailTypeOriginal(SOCIAL_MEDIA);
        return dtoIntDelivery;
    }

    public DTOIntDelivery buildDTOIntDeliveryStored() throws IOException {
        DTOIntDelivery dtoIntDelivery = objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/input_create_card_delivery_stored.json"), DTOIntDelivery.class);
        dtoIntDelivery.setCardId(CARD_ID);
        dtoIntDelivery.setServiceTypeId(STATEMENT_TYPE);
        dtoIntDelivery.getContact().setContactTypeOriginal(STORED);
        return dtoIntDelivery;
    }

    public DTOIntCard getDTOIntCard() {
        DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardId(CARD_ID);
        return dtoIntCard;
    }

    public List<Limits> buildListLimits() {
        List<Limits> limits = new ArrayList<>();
        Limits limit1 = new Limits();
        limit1.setId("1111111");
        List<AmountLimit> amountLimits = new ArrayList<>();
        AmountLimit amountLimit = new AmountLimit();
        amountLimit.setAmount(new BigDecimal("4000.5"));
        amountLimit.setCurrency("PEN");
        amountLimits.add(amountLimit);
        limit1.setAmountLimits(amountLimits);

        AllowedInterval allowedInterval = new AllowedInterval();
        Import minAmount = new Import();
        minAmount.setAmount(new BigDecimal("3000.5"));
        minAmount.setCurrency("PEN");
        allowedInterval.setMinimumAmount(minAmount);
        Import maxAmount = new Import();
        maxAmount.setAmount(new BigDecimal("5000.5"));
        maxAmount.setCurrency("PEN");
        allowedInterval.setMaximumAmount(maxAmount);
        limit1.setAllowedInterval(allowedInterval);
        limits.add(limit1);


        Limits limit2 = new Limits();
        limit2.setId("2222222");

        amountLimits = new ArrayList<>();
        amountLimit = new AmountLimit();
        amountLimit.setAmount(new BigDecimal("8000.5"));
        amountLimit.setCurrency("PEN");
        amountLimits.add(amountLimit);
        limit2.setAmountLimits(amountLimits);

        allowedInterval = new AllowedInterval();
        minAmount = new Import();
        minAmount.setAmount(new BigDecimal("7000.5"));
        minAmount.setCurrency("PEN");
        allowedInterval.setMinimumAmount(minAmount);
        maxAmount = new Import();
        maxAmount.setAmount(new BigDecimal("9000.5"));
        maxAmount.setCurrency("DOL");
        allowedInterval.setMaximumAmount(maxAmount);
        limit2.setAllowedInterval(allowedInterval);
        limits.add(limit2);

        return limits;
    }

    public InputGetCardCancellationVerification getInputGetCardCancellationVerificationMock() {
        InputGetCardCancellationVerification mock = new InputGetCardCancellationVerification();
        mock.setCardId(CARD_ID);
        return mock;
    }

    public FormatoMPMS1DV getFormatoMPMS1DVMock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/getCardSecurityDataV1/formatoMPMS1DV.json"), FormatoMPMS1DV.class);
    }


    public com.bbva.pzic.cards.facade.v1.dto.Proposal buildModifyCardProposal() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/modifyProposalCard.json"), com.bbva.pzic.cards.facade.v1.dto.Proposal.class);
    }

    public ReportCardCreation getReportCardCreationMock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/createCardReportsV0/reportCardCreation.json"), ReportCardCreation.class);
    }

    public InputCreateCardReports getInputCreateCardReportsMock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/createCardReportsV0/inputCreateCardReports.json"), InputCreateCardReports.class);
    }

    public SimulateTransactionRefund buildSimulateTransactionRefund() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/simulateTransactionRefundV1/simulateTransactionRefund.json"), SimulateTransactionRefund.class);
    }

    public InputSimulateCardTransactionTransactionRefund buildInputSimulateCardTransactionTransactionRefund() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/simulateTransactionRefundV1/inputSimulateCardTransactionTransactionRefund.json"), InputSimulateCardTransactionTransactionRefund.class);
    }

    public ReimburseCardTransactionTransactionRefund getReimburseCardTransactionTransactionRefundMock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputReimburseCardTransactionTransactionRefund.json"), ReimburseCardTransactionTransactionRefund.class);
    }

    public InputReimburseCardTransactionTransactionRefund getInputReimburseCardTransactionTransactionRefundMock() {
        InputReimburseCardTransactionTransactionRefund input = new InputReimburseCardTransactionTransactionRefund();
        input.setCardId(CARD_ID);
        input.setTransactionId(TRANSACTION_ID);
        InputRefundType refundType = new InputRefundType();
        refundType.setId("REFUND");
        input.setRefundType(refundType);
        return input;
    }

    public InputReimburseCardTransactionTransactionRefund getInputReimburseCardWithoutRefundType() {
        InputReimburseCardTransactionTransactionRefund input = new InputReimburseCardTransactionTransactionRefund();
        input.setCardId(CARD_ID);
        input.setTransactionId(TRANSACTION_ID);
        return input;
    }

    public FormatoMPMS16J getFormatoMPMS16JMock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/reimburseCardTransactionTransactionRefundResponse.json"), FormatoMPMS16J.class);
    }

    public InputGetCardFinancialStatement getInputGetCardFinancialStatementV1() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputGetCardFinancialStatementV1.json"), InputGetCardFinancialStatement.class);
    }

    public RequestCancel getRequestCancelV1() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/requestCancelV1.json"), RequestCancel.class);
    }

    public InputCreateCardCancelRequest getInputCreateCardCancelRequestV1() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputCreateCardCancelRequestV1.json"), InputCreateCardCancelRequest.class);
    }

    public Cancellation getCancellationV1Mock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/cancellationV1.json"), Cancellation.class);
    }

    public InputCreateCardCancellations getInputCreateCardCancellationsV1Mock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputCreateCardCancellationsV1.json"), InputCreateCardCancellations.class);
    }

    public OfferGenerate buildOfferGenerate() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/offerGenerate.json"), OfferGenerate.class);
    }

    public InputCreateOffersGenerateCards buildInputCreateOffersGenerateCards() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputCreateOffersGenerateCards.json"), InputCreateOffersGenerateCards.class);
    }
}

package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputListCardProposals;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;
import com.bbva.pzic.cards.facade.v1.mapper.IListCardProposalsMapper;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created on 29/11/2019.
 *
 * @author Entelgy
 */
public class ListCardProposalsMapperTest {

    private IListCardProposalsMapper mapper = new ListCardProposalsMapper();

    @Test
    public void mapInFullTest() {
        InputListCardProposals result = mapper.mapIn(EntityMock.PROPOSAL_STATUS_CREATED);

        assertNotNull(result);
        assertNotNull(result.getStatus());
        assertEquals(EntityMock.PROPOSAL_STATUS_CREATED, result.getStatus());
    }

    @Test
    public void mapInEmptyTest() {
        InputListCardProposals result = mapper.mapIn(null);

        assertNotNull(result);
        assertNull(result.getStatus());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<List<Proposal>> result = mapper.mapOut(Collections.singletonList(new Proposal()));
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<List<Proposal>> result = mapper.mapOut(Collections.emptyList());
        assertNull(result);
    }
}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.business.dto.DTOInputCreateCashRefund;
import com.bbva.pzic.cards.business.dto.DTOIntCashRefund;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCEMDC0;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCRMDC0;
import com.bbva.pzic.cards.dao.model.mpdc.FormatoMCRMDC1;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TxCreateCardCashRefundMapperTest {

    @InjectMocks
    private TxCreateCardCashRefundMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    private DummyMock dummyMock;

    @Before
    public void setUp() {
        dummyMock = new DummyMock();
        Mockito.when(enumMapper.getEnumValue("cards.refundType", CCC_TIPREEM)).thenReturn(CCC_REFUNDTYPE);
        Mockito.when(enumMapper.getEnumValue("exchangeRate.values.priceType", CCC_TIPPREC)).thenReturn(CCC_PRICETYPE);
        Mockito.when(enumMapper.getEnumValue("transfers.taxes.taxType", CCC_TIPOIMP)).thenReturn(CCC_TAXTYPE);
    }

    @Test
    public void testMapInFullCreateCardCashRefund(){
        DTOInputCreateCashRefund dtoInputCreateCashRefund = getDTOInputCreateCashRefund();
        FormatoMCEMDC0 formatoMCEMDC0 = mapper.mapInput(dtoInputCreateCashRefund);
        assertNotNull(formatoMCEMDC0);
        assertNotNull(formatoMCEMDC0.getNrotarj());
        assertEquals(dtoInputCreateCashRefund.getCardId(),formatoMCEMDC0.getNrotarj());
        assertNotNull(formatoMCEMDC0.getTipreem());
        assertEquals(dtoInputCreateCashRefund.getCashRefund().getRefundType(),formatoMCEMDC0.getTipreem());
        assertNotNull(formatoMCEMDC0.getDivisa());
        assertEquals(dtoInputCreateCashRefund.getCashRefund().getRefundAmount().getCurrency(),formatoMCEMDC0.getDivisa());
        assertNotNull(formatoMCEMDC0.getNcuenta());
        assertEquals(dtoInputCreateCashRefund.getCashRefund().getReceivingAccount().getId(),formatoMCEMDC0.getNcuenta());
    }

    @Test
    public void testMapOutputFormatoMCRMDC0(){
        FormatoMCRMDC0 formatoMCRMDC0 = getFormatoMCRMDC0();
        DTOIntCashRefund dtoIntCashRefund = mapper.mapOutput(formatoMCRMDC0,new DTOIntCashRefund());
        assertNotNull(dtoIntCashRefund);
        assertNotNull(dtoIntCashRefund.getId());
        assertEquals(formatoMCRMDC0.getIdoper(),dtoIntCashRefund.getId());
        assertNotNull(dtoIntCashRefund.getDescription());
        assertEquals(formatoMCRMDC0.getDesoper(),dtoIntCashRefund.getDescription());
        assertNotNull(dtoIntCashRefund.getRefundType());
        assertEquals(CCC_REFUNDTYPE,dtoIntCashRefund.getRefundType());
        assertNotNull(dtoIntCashRefund.getRefundAmount());
        assertNotNull(dtoIntCashRefund.getRefundAmount().getAmount());
        assertEquals(formatoMCRMDC0.getIsalacr(),dtoIntCashRefund.getRefundAmount().getAmount());
        assertNotNull(dtoIntCashRefund.getRefundAmount().getCurrency());
        assertEquals(formatoMCRMDC0.getDivsacr(),dtoIntCashRefund.getRefundAmount().getCurrency());
        assertNotNull(dtoIntCashRefund.getReceivingAccount());
        assertNotNull(dtoIntCashRefund.getReceivingAccount().getId());
        assertEquals(formatoMCRMDC0.getNcuenta(),dtoIntCashRefund.getReceivingAccount().getId());
        assertNotNull(dtoIntCashRefund.getOperationDate());
        assertEquals(dummyMock.buildDate(formatoMCRMDC0.getFecoper(), formatoMCRMDC0.getHoraope(), "yyyy-MM-ddHH:mm:ss"), dtoIntCashRefund.getOperationDate().getTime());
        assertNotNull(dtoIntCashRefund.getAccoutingDate());
        assertEquals(dummyMock.buildDate(formatoMCRMDC0.getFecfact(), formatoMCRMDC0.getHorafac(), "yyyy-MM-ddHH:mm:ss"), dtoIntCashRefund.getAccoutingDate().getTime());
        assertNotNull(dtoIntCashRefund.getReceivedAmount());
        assertNotNull(dtoIntCashRefund.getReceivedAmount().getAmount());
        assertEquals(formatoMCRMDC0.getIsalcta(),dtoIntCashRefund.getReceivedAmount().getAmount());
        assertNotNull(dtoIntCashRefund.getReceivedAmount().getCurrency());
        assertEquals(formatoMCRMDC0.getDivsact(),dtoIntCashRefund.getReceivedAmount().getCurrency());
        assertNotNull(dtoIntCashRefund.getExchangeRate());
        assertNotNull(dtoIntCashRefund.getExchangeRate().getDate());
        assertEquals(dummyMock.buildDate(formatoMCRMDC0.getFecoper(), formatoMCRMDC0.getHoraope(), "yyyy-MM-ddHH:mm:ss"), dtoIntCashRefund.getExchangeRate().getDate().getTime());
        assertNotNull(dtoIntCashRefund.getExchangeRate().getValues());
        assertNotNull(dtoIntCashRefund.getExchangeRate().getValues().getFactor());
        assertNotNull(dtoIntCashRefund.getExchangeRate().getValues().getFactor().getValue());
        assertEquals(formatoMCRMDC0.getTipcam(),dtoIntCashRefund.getExchangeRate().getValues().getFactor().getValue());
        assertNotNull(dtoIntCashRefund.getExchangeRate().getValues().getPriceType());
        assertEquals(CCC_PRICETYPE,dtoIntCashRefund.getExchangeRate().getValues().getPriceType());
        assertNotNull(dtoIntCashRefund.getTaxes());
        assertNotNull(dtoIntCashRefund.getTaxes().getTotalTaxes());
        assertNotNull(dtoIntCashRefund.getTaxes().getTotalTaxes().getAmount());
        assertEquals(formatoMCRMDC0.getTotimp(),dtoIntCashRefund.getTaxes().getTotalTaxes().getAmount());
        assertNotNull(dtoIntCashRefund.getTaxes().getTotalTaxes().getCurrency());
        assertEquals(formatoMCRMDC0.getDivtoti(),dtoIntCashRefund.getTaxes().getTotalTaxes().getCurrency());
    }

    @Test
    public void testMapOutputFormatoMCRMDC1(){
        FormatoMCRMDC1 formatoMCRMDC1 = getFormatoMCRMDC1();
        DTOIntCashRefund dtoIntCashRefund = mapper.mapOutput(formatoMCRMDC1, new DTOIntCashRefund());
        assertNotNull(dtoIntCashRefund.getTaxes());
        assertNotNull(dtoIntCashRefund.getTaxes().getItemizeTaxes());
        assertFalse(dtoIntCashRefund.getTaxes().getItemizeTaxes().isEmpty());
        assertNotNull(dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0));
        assertNotNull(dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0).getTaxType());
        assertEquals(CCC_TAXTYPE,dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0).getTaxType());
        assertNotNull(dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount());
        assertNotNull(dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount().getAmount());
        assertEquals(formatoMCRMDC1.getMonimp(),dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount().getAmount());
        assertNotNull(dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount().getCurrency());
        assertEquals(formatoMCRMDC1.getDivimp(),dtoIntCashRefund.getTaxes().getItemizeTaxes().get(0).getMonetaryAmount().getCurrency());
    }

}

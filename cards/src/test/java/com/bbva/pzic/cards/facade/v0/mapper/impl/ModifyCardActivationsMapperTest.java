package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputModifyCardActivations;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import com.bbva.pzic.cards.facade.RegistryIds;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.CARD_ACTIVATIONS_ID_KEY_TESTED;
import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 10/10/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ModifyCardActivationsMapperTest {

    @InjectMocks
    private ModifyCardActivationsMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Mock
    private Translator translator;

    private final EntityMock mock = EntityMock.getInstance();

    @Before
    public void init() {
        when(cypherTool.decrypt(CARD_ID, AbstractCypherTool.IDETARJ, RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATIONS))
                .thenReturn(CARD_ACTIVATIONS_ID_KEY_TESTED);
    }

    @Test
    public void mapInFull() throws IOException {
        final List<Activation> activations = mock.buildListActivation();

        when(translator.translateFrontendEnumValueStrictly("cards.activation.activationId", activations.get(0).getActivationId()))
                .thenReturn("02");
        when(translator.translateFrontendEnumValueStrictly("cards.activation.activationId", activations.get(1).getActivationId()))
                .thenReturn("03");

        final InputModifyCardActivations entity = mapper.mapIn(CARD_ID, activations);

        assertNotNull(entity);
        assertNotNull(entity.getCardId());
        assertNotNull(entity.getDtoIntActivationPosition1());
        assertNotNull(entity.getDtoIntActivationPosition2());
        assertNotNull(entity.getDtoIntActivationPosition3());
        assertNotNull(entity.getDtoIntActivationPosition4());
        assertNotNull(entity.getDtoIntActivationPosition5());

        assertEquals("02", entity.getDtoIntActivationPosition1().getActivationId());
        assertTrue(entity.getDtoIntActivationPosition1().getIsActive());
        assertEquals("03", entity.getDtoIntActivationPosition2().getActivationId());
        assertFalse(entity.getDtoIntActivationPosition2().getIsActive());
    }

    @Test
    public void mapInFullWithOutActivaonsEmpty() {
        final InputModifyCardActivations entity = mapper.mapIn(CARD_ID, Collections.emptyList());

        assertNotNull(entity);
        assertNotNull(entity.getCardId());
        assertNull(entity.getDtoIntActivationPosition1());
        assertNull(entity.getDtoIntActivationPosition2());
        assertNull(entity.getDtoIntActivationPosition3());
        assertNull(entity.getDtoIntActivationPosition4());
        assertNull(entity.getDtoIntActivationPosition5());
    }

    @Test
    public void mapOutFull() {
        ServiceResponse<List<Activation>> result = mapper.mapOut(Collections.singletonList(new Activation()));

        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmpty() {
        ServiceResponse<List<Activation>> result = mapper.mapOut(Collections.emptyList());

        assertNull(result);
    }
}

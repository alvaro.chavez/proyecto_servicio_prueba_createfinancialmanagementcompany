package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCardInstallmentsPlan;
import com.bbva.pzic.cards.canonic.InstallmentsPlanSimulationData;
import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0DET;
import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0WSC;
import com.bbva.pzic.cards.dao.model.mpws.FormatoMPM0WSE;
import com.bbva.pzic.cards.dao.model.mpws.mock.FormatMpwsMock;
import com.bbva.pzic.cards.dao.tx.mapper.ITxSimulateCardInstallmentsPlanMapper;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class TxSimulateCardInstallmentsPlanMapperTest {

    @InjectMocks
    private ITxSimulateCardInstallmentsPlanMapper simulateCardInstallmentsPlanMapper;

    @Mock
    private EnumMapper enumMapper;

    private EntityMock entityMock = EntityMock.getInstance();

    private FormatMpwsMock formatMpwsMock;

    @Before
    public void setUp() {
        formatMpwsMock = new FormatMpwsMock();
        simulateCardInstallmentsPlanMapper = new TxSimulateCardInstallmentsPlanMapper();
        MockitoAnnotations.initMocks(this);
        mapOutEnumMapper();
    }

    public void mapOutEnumMapper() {
        Mockito.when(enumMapper.getEnumValue("installmentPlan.rates.rateType.id", "TEA")).thenReturn("TEA");
        Mockito.when(enumMapper.getEnumValue("installmentPlan.rates.rateType.id", "TCEA")).thenReturn("TCEA");
    }

    @Test
    public void testMapInFullSimulateCardInstallmentPlan() {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputSimulateInstallmentPlan();
        FormatoMPM0WSE result = simulateCardInstallmentsPlanMapper.mapIn(dtoIn);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNotNull(result.getImpoper());
        Assert.assertNotNull(result.getDivoper());
        Assert.assertNotNull(result.getNcuotas());
        Assert.assertNotNull(result.getNumoper());

        Assert.assertEquals(dtoIn.getCardId(), result.getNumtarj());
        Assert.assertEquals(dtoIn.getAmount(), result.getImpoper());
        Assert.assertEquals(dtoIn.getCurrency(), result.getDivoper());
        Assert.assertEquals(dtoIn.getTermsNumber(), result.getNcuotas());
        Assert.assertEquals(dtoIn.getTransactionId(), result.getNumoper());
    }

    @Test
    public void testMapInFullSimulateCardInstallmentPlanWithoutAmount() {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputSimulateInstallmentPlan();
        dtoIn.setAmount(null);
        FormatoMPM0WSE result = simulateCardInstallmentsPlanMapper.mapIn(dtoIn);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNull(result.getImpoper());
        Assert.assertNotNull(result.getDivoper());
        Assert.assertNotNull(result.getNcuotas());
        Assert.assertNotNull(result.getNumoper());

        Assert.assertEquals(dtoIn.getCardId(), result.getNumtarj());
        Assert.assertEquals(dtoIn.getCurrency(), result.getDivoper());
        Assert.assertEquals(dtoIn.getTermsNumber(), result.getNcuotas());
        Assert.assertEquals(dtoIn.getTransactionId(), result.getNumoper());
    }

    @Test
    public void testMapInFullSimulateCardInstallmentPlanWithoutCurrency() {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputSimulateInstallmentPlan();
        dtoIn.setCurrency(null);
        FormatoMPM0WSE result = simulateCardInstallmentsPlanMapper.mapIn(dtoIn);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNotNull(result.getImpoper());
        Assert.assertNull(result.getDivoper());
        Assert.assertNotNull(result.getNcuotas());
        Assert.assertNotNull(result.getNumoper());

        Assert.assertEquals(dtoIn.getCardId(), result.getNumtarj());
        Assert.assertEquals(dtoIn.getAmount(), result.getImpoper());
        Assert.assertEquals(dtoIn.getTermsNumber(), result.getNcuotas());
        Assert.assertEquals(dtoIn.getTransactionId(), result.getNumoper());
    }

    @Test
    public void testMapInFullSimulateCardInstallmentPlanWithoutTermsTotalNumber() {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputSimulateInstallmentPlan();
        dtoIn.setTermsNumber(null);
        FormatoMPM0WSE result = simulateCardInstallmentsPlanMapper.mapIn(dtoIn);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNotNull(result.getImpoper());
        Assert.assertNotNull(result.getDivoper());
        Assert.assertNull(result.getNcuotas());
        Assert.assertNotNull(result.getNumoper());

        Assert.assertEquals(dtoIn.getCardId(), result.getNumtarj());
        Assert.assertEquals(dtoIn.getAmount(), result.getImpoper());
        Assert.assertEquals(dtoIn.getCurrency(), result.getDivoper());
        Assert.assertEquals(dtoIn.getTransactionId(), result.getNumoper());
    }

    @Test
    public void testMapInFullSimulateCardInstallmentPlanWithoutTransactionId() {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputSimulateInstallmentPlan();
        dtoIn.setTransactionId(null);
        FormatoMPM0WSE result = simulateCardInstallmentsPlanMapper.mapIn(dtoIn);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getNumtarj());
        Assert.assertNotNull(result.getImpoper());
        Assert.assertNotNull(result.getDivoper());
        Assert.assertNotNull(result.getNcuotas());
        Assert.assertNull(result.getNumoper());

        Assert.assertEquals(dtoIn.getCardId(), result.getNumtarj());
        Assert.assertEquals(dtoIn.getAmount(), result.getImpoper());
        Assert.assertEquals(dtoIn.getCurrency(), result.getDivoper());
        Assert.assertEquals(dtoIn.getTermsNumber(), result.getNcuotas());
    }

    @Test
    public void testMapOutSimulateCardInstallmentPlanFull() throws IOException {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputSimulateInstallmentPlan();
        FormatoMPM0WSC formatoMPM0WSC = formatMpwsMock.getFormatoMPM0WSC();
        List<FormatoMPM0DET> formatoMPM0DETList = formatMpwsMock.getFormatoMPM0DET();

        InstallmentsPlanSimulationData data = simulateCardInstallmentsPlanMapper.mapOut1(formatoMPM0WSC, dtoIn);

        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getData());
        Assert.assertNotNull(data.getData().getCapital().getAmount());
        Assert.assertNotNull(data.getData().getCapital().getCurrency());
        Assert.assertNotNull(data.getData().getRates().get(0).getRateType().getId());
        Assert.assertNotNull(data.getData().getRates().get(0).getRateType().getName());
        Assert.assertNotNull(data.getData().getRates().get(0).getPercentage());
        Assert.assertNotNull(data.getData().getRates().get(1).getRateType().getId());
        Assert.assertNotNull(data.getData().getRates().get(1).getRateType().getName());
        Assert.assertNotNull(data.getData().getRates().get(1).getPercentage());
        Assert.assertNotNull(data.getData().getInterest().getAmount());
        Assert.assertNotNull(data.getData().getInterest().getCurrency());
        Assert.assertNotNull(data.getData().getTotal().getAmount());
        Assert.assertNotNull(data.getData().getTotal().getCurrency());

        Assert.assertEquals(formatoMPM0WSC.getTotcapi(), data.getData().getCapital().getAmount());
        Assert.assertEquals(formatoMPM0WSC.getDivtcap(), data.getData().getCapital().getCurrency());
        Assert.assertEquals("TEA", data.getData().getRates().get(0).getRateType().getId());
        Assert.assertEquals("TASA EFECTIVA ANUAL", data.getData().getRates().get(0).getRateType().getName());
        Assert.assertEquals(formatoMPM0WSC.getTasaefa(), new BigDecimal(data.getData().getRates().get(0).getPercentage()));
        Assert.assertEquals("TCEA", data.getData().getRates().get(1).getRateType().getId());
        Assert.assertEquals("TASA DE COSTO EFECTIVA ANUAL", data.getData().getRates().get(1).getRateType().getName());
        Assert.assertEquals(formatoMPM0WSC.getPortcea(), new BigDecimal(data.getData().getRates().get(1).getPercentage()));
        Assert.assertEquals(formatoMPM0WSC.getTotinte(), data.getData().getInterest().getAmount());
        Assert.assertEquals(formatoMPM0WSC.getDivtint(), data.getData().getInterest().getCurrency());
        Assert.assertEquals(formatoMPM0WSC.getTotcuot(), data.getData().getTotal().getAmount());
        Assert.assertEquals(formatoMPM0WSC.getDivtcuo(), data.getData().getTotal().getCurrency());

        FormatoMPM0DET firstScheduleInstallments = formatoMPM0DETList.get(0);
        data = simulateCardInstallmentsPlanMapper.mapOut2(firstScheduleInstallments, data);

        Assert.assertEquals(1, data.getData().getScheduledPayments().size());
        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getData());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getMaturityDate());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getTotal().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getTotal().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getCapital().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getCapital().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getInterest().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getInterest().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getFee().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0).getFee().getCurrency());

        Assert.assertEquals(firstScheduleInstallments.getFecpago(), data.getData().getScheduledPayments().get(0).getMaturityDate());
        Assert.assertEquals(firstScheduleInstallments.getImpcuot(), data.getData().getScheduledPayments().get(0).getTotal().getAmount());
        Assert.assertEquals(firstScheduleInstallments.getDivcuot(), data.getData().getScheduledPayments().get(0).getTotal().getCurrency());
        Assert.assertEquals(firstScheduleInstallments.getCapital(), data.getData().getScheduledPayments().get(0).getCapital().getAmount());
        Assert.assertEquals(firstScheduleInstallments.getDivcapc(), data.getData().getScheduledPayments().get(0).getCapital().getCurrency());
        Assert.assertEquals(firstScheduleInstallments.getInteres(), data.getData().getScheduledPayments().get(0).getInterest().getAmount());
        Assert.assertEquals(firstScheduleInstallments.getDivinte(), data.getData().getScheduledPayments().get(0).getInterest().getCurrency());
        Assert.assertEquals(firstScheduleInstallments.getComisio(), data.getData().getScheduledPayments().get(0).getFee().getAmount());
        Assert.assertEquals(firstScheduleInstallments.getDivcomi(), data.getData().getScheduledPayments().get(0).getFee().getCurrency());

        FormatoMPM0DET secondScheduleInstallments = formatoMPM0DETList.get(1);
        data = simulateCardInstallmentsPlanMapper.mapOut2(secondScheduleInstallments, data);

        Assert.assertEquals(2, data.getData().getScheduledPayments().size());
        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getData());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getMaturityDate());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getTotal().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getTotal().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getCapital().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getCapital().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getInterest().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getInterest().getCurrency());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getFee().getAmount());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(1).getFee().getCurrency());

        Assert.assertEquals(secondScheduleInstallments.getFecpago(), data.getData().getScheduledPayments().get(1).getMaturityDate());
        Assert.assertEquals(secondScheduleInstallments.getImpcuot(), data.getData().getScheduledPayments().get(1).getTotal().getAmount());
        Assert.assertEquals(secondScheduleInstallments.getDivcuot(), data.getData().getScheduledPayments().get(1).getTotal().getCurrency());
        Assert.assertEquals(secondScheduleInstallments.getCapital(), data.getData().getScheduledPayments().get(1).getCapital().getAmount());
        Assert.assertEquals(secondScheduleInstallments.getDivcapc(), data.getData().getScheduledPayments().get(1).getCapital().getCurrency());
        Assert.assertEquals(secondScheduleInstallments.getInteres(), data.getData().getScheduledPayments().get(1).getInterest().getAmount());
        Assert.assertEquals(secondScheduleInstallments.getDivinte(), data.getData().getScheduledPayments().get(1).getInterest().getCurrency());
        Assert.assertEquals(secondScheduleInstallments.getComisio(), data.getData().getScheduledPayments().get(1).getFee().getAmount());
        Assert.assertEquals(secondScheduleInstallments.getDivcomi(), data.getData().getScheduledPayments().get(1).getFee().getCurrency());
    }

    @Test
    public void testMapOutCreateInstallmentPlanInitialized() {
        DTOIntCardInstallmentsPlan dtoIn = entityMock.buildDTOInputCreateInstallmentPlan();
        InstallmentsPlanSimulationData data = simulateCardInstallmentsPlanMapper.mapOut1(new FormatoMPM0WSC(), dtoIn);

        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getData());
        Assert.assertNull(data.getData().getCapital());
        Assert.assertNotNull(data.getData().getRates().get(0).getRateType().getId());
        Assert.assertNotNull(data.getData().getRates().get(0).getRateType().getName());
        Assert.assertNull(data.getData().getRates().get(0).getPercentage());
        Assert.assertNotNull(data.getData().getRates().get(1).getRateType().getId());
        Assert.assertNotNull(data.getData().getRates().get(1).getRateType().getName());
        Assert.assertNull(data.getData().getRates().get(1).getPercentage());
        Assert.assertNull(data.getData().getInterest());
        Assert.assertNull(data.getData().getTotal());

        data = simulateCardInstallmentsPlanMapper.mapOut2(new FormatoMPM0DET(), data);

        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getData());
        Assert.assertNotNull(data.getData().getScheduledPayments().get(0));
        Assert.assertNull(data.getData().getScheduledPayments().get(0).getMaturityDate());
        Assert.assertNull(data.getData().getScheduledPayments().get(0).getTotal());
        Assert.assertNull(data.getData().getScheduledPayments().get(0).getCapital());
        Assert.assertNull(data.getData().getScheduledPayments().get(0).getInterest());
        Assert.assertNull(data.getData().getScheduledPayments().get(0).getFee());
    }
}
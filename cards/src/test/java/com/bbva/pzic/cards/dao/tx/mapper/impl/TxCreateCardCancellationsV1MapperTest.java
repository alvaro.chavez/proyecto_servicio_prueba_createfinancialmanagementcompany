package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancellations;
import com.bbva.pzic.cards.dao.model.mpcn.FormatoMPME1NC;
import com.bbva.pzic.cards.dao.model.mpcn.FormatoMPMS1NC;
import com.bbva.pzic.cards.dao.model.mpcn.mock.FormatsMpcnMock;
import com.bbva.pzic.cards.facade.v1.dto.Cancellation;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.*;
import static com.bbva.pzic.cards.util.Converter.dateToDateTime;
import static com.bbva.pzic.cards.util.Converter.stringToBoolean;
import static com.bbva.pzic.cards.util.Enums.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TxCreateCardCancellationsV1MapperTest {

    @InjectMocks
    private TxCreateCardCancellationsV1Mapper mapper;

    @Mock
    private Translator translator;

    private FormatsMpcnMock formatsMpcnMock = FormatsMpcnMock.getInstance();

    @Before
    public void init() {
        when(translator.translateBackendEnumValueStrictly(
                CARD_CANCELLATIONS_REASONID, CARD_CANCELLATIONS_REASONID_BACKEND)).thenReturn(CARD_CANCELLATIONS_REASONID_FRONTEND);
        when(translator.translateBackendEnumValueStrictly(
                CARD_CANCELLATIONS_STATUSID, CARD_CANCELLATIONS_STATUSID_BACKEND)).thenReturn(CARD_CANCELLATIONS_STATUSID_FRONTEND);
        when(translator.translateBackendEnumValueStrictly(
                CARDS_NUMBERTYPE_ID, CARDS_NUMBERTYPE_ID_BACKEND)).thenReturn(CARDS_NUMBERTYPE_ID_FRONTEND);
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputCreateCardCancellations input = EntityMock.getInstance().getInputCreateCardCancellationsV1Mock();

        FormatoMPME1NC result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCodtar());
        assertNotNull(result.getCodraz());

        assertEquals(input.getCardId(), result.getCodtar());
        assertEquals(input.getReason().getId(), result.getCodraz());
    }

    @Test
    public void mapInWithoutReasonTest() throws IOException {
        InputCreateCardCancellations input = EntityMock.getInstance().getInputCreateCardCancellationsV1Mock();
        input.setReason(null);

        FormatoMPME1NC result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCodtar());
        assertNull(result.getCodraz());

        assertEquals(input.getCardId(), result.getCodtar());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoMPMS1NC input = formatsMpcnMock.getFormatoMPMS1NC();

        Cancellation result = mapper.mapOutFormatoMPMS1NC(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getOperationData());
        assertNotNull(result.getOperationData().getOperationNumber());
        assertNotNull(result.getOperationData().getOperationDate());
        assertNotNull(result.getOperationData().getCancellationDate());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getId());
        assertNotNull(result.getCard().getNumber());
        assertNotNull(result.getCard().getNumberType());
        assertNotNull(result.getCard().getNumberType().getId());
        assertNotNull(result.getCard().getNumberType().getDescription());
        assertNotNull(result.getCard().getProduct());
        assertNotNull(result.getCard().getProduct().getId());
        assertNotNull(result.getCard().getProduct().getDescription());
        assertNotNull(result.getCard().getCurrencies());
        assertNotNull(result.getCard().getCurrencies().getCurrency());
        assertNotNull(result.getCard().getCurrencies().getIsMajor());

        assertEquals(input.getNumope(), result.getId());
        assertEquals(input.getNumope(), result.getOperationData().getOperationNumber());
        assertEquals(dateToDateTime(input.getFechope()), result.getOperationData().getOperationDate());
        assertEquals(dateToDateTime(input.getFechcan()), result.getOperationData().getCancellationDate());
        assertEquals(CARD_CANCELLATIONS_STATUSID_FRONTEND, result.getStatus().getId());
        assertEquals(CARD_CANCELLATIONS_REASONID_FRONTEND, result.getReason().getId());
        assertEquals(input.getNumtar(), result.getCard().getId());
        assertEquals(input.getNumtar(), result.getCard().getNumber());
        assertEquals(CARDS_NUMBERTYPE_ID_FRONTEND, result.getCard().getNumberType().getId());
        assertEquals(input.getDetnuta(), result.getCard().getNumberType().getDescription());
        assertEquals(input.getTipprod(), result.getCard().getProduct().getId());
        assertEquals(input.getDescpro(), result.getCard().getProduct().getDescription());
        assertEquals(input.getDivtar(), result.getCard().getCurrencies().getCurrency());
        assertEquals(stringToBoolean(input.getInmopr(), "1", "0"), result.getCard().getCurrencies().getIsMajor());
    }

    @Test
    public void mapOutWithoutInnerParametersTest() throws IOException {
        FormatoMPMS1NC input = formatsMpcnMock.getFormatoMPMS1NC();
        input.setNumope(null);
        input.setFechope(null);
        input.setFechcan(null);
        input.setEstcan(null);
        input.setCodraz(null);
        input.setIdtnuta(null);
        input.setDetnuta(null);
        input.setTipprod(null);
        input.setDescpro(null);
        input.setDivtar(null);
        input.setInmopr(null);

        Cancellation result = mapper.mapOutFormatoMPMS1NC(input);

        assertNotNull(result);
        assertNull(result.getId());
        assertNull(result.getOperationData());
        assertNull(result.getStatus());
        assertNull(result.getReason());
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getId());
        assertNotNull(result.getCard().getNumber());
        assertNull(result.getCard().getNumberType());
        assertNull(result.getCard().getProduct());
        assertNull(result.getCard().getCurrencies());

        assertEquals(input.getNumtar(), result.getCard().getId());
        assertEquals(input.getNumtar(), result.getCard().getNumber());
    }

    @Test
    public void mapOutEmptyTest() {
        Cancellation result = mapper.mapOutFormatoMPMS1NC(new FormatoMPMS1NC());

        assertNull(result);
    }
}

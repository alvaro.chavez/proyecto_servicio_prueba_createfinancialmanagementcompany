package com.bbva.pzic.cards.facade.v01.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntActivation;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.ACTIVATION_ID;
import static com.bbva.pzic.cards.EntityMock.CARD_ENCRYPT_ID;
import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ModifyCardActivationMapperTest {

    @InjectMocks
    private ModifyCardActivationMapper modifyCardActivationMapper;

    @Mock
    private AbstractCypherTool cypherTool;

    private EntityMock mock = EntityMock.getInstance();

    @Before
    public void init() {
        Mockito.when(cypherTool.decrypt("##4232$%6778", AbstractCypherTool.NUMTARJ, SMC_REGISTRY_ID_OF_MODIFY_CARD_ACTIVATION))
                .thenReturn("12345678");
    }

    @Test
    public void testMapInput() {
        final Activation activation = mock.buildActivation();
        final DTOIntActivation dtoIntActivation =
                modifyCardActivationMapper.mapInput(CARD_ENCRYPT_ID, ACTIVATION_ID, activation);

        assertNotNull(dtoIntActivation);
        assertNotNull(dtoIntActivation.getCard());
        assertNotNull(dtoIntActivation.getIsActive());

        assertEquals("12345678", dtoIntActivation.getCard().getCardId());
        assertEquals(ACTIVATION_ID, dtoIntActivation.getActivationId());
        assertEquals(activation.getIsActive(), dtoIntActivation.getIsActive());
    }

}
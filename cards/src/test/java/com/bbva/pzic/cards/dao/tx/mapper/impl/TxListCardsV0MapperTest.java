package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.managers.InputHeaderManager;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMENL1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS1L1;
import com.bbva.pzic.cards.dao.model.mpl1.FormatoMPMS2L1;
import com.bbva.pzic.cards.dao.model.mpl1.mock.FormatsMpl1Mock;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Collections;

import static com.bbva.pzic.cards.EntityMock.HEADER_USER_AGENT_STUB;
import static com.bbva.pzic.cards.util.Constants.HEADER_USER_AGENT;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 06/02/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxListCardsV0MapperTest {

    private static final String ONE = "PAN";
    private static final String PREPAID_CARD = "PREPAID_CARD";
    private static final String STICKER = "STICKER";
    private static final String INOPERATIVE = "INOPERATIVE";
    private static final String VISA = "VISA";
    private static final String LOST = "LOST";

    @InjectMocks
    private TxListCardsV0Mapper mapper;

    @Mock
    private Translator translator;
    @Mock
    private InputHeaderManager inputHeaderManager;

    private EntityMock entityMock = EntityMock.getInstance();
    private FormatsMpl1Mock formatsMpl1Mock = new FormatsMpl1Mock();

    private void mapInEnum() {
        when(translator.translateFrontendEnumValueStrictly("cards.cardType.id", "CREDIT_CARD")).thenReturn("C");
        when(translator.translateFrontendEnumValueStrictly("cards.cardType.id", "DEBIT_CARD")).thenReturn("D");
        when(translator.translateFrontendEnumValueStrictly("cards.cardType.id", PREPAID_CARD)).thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.physicalSupport.id", "NORMAL_PLASTIC")).thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.status.id", "OPERATIVE")).thenReturn("A");
        when(translator.translateFrontendEnumValueStrictly("cards.status.id", "BLOCKED")).thenReturn("B");
        when(translator.translateFrontendEnumValueStrictly("cards.participantType.id", "HOLDER")).thenReturn("T");
        when(translator.translateFrontendEnumValueStrictly("cards.participantType.id", "AUTHORIZED")).thenReturn("A");
    }

    private void mapOutEnum() {
        when(translator.translateBackendEnumValueStrictly("cards.numberType.id", "1")).thenReturn(ONE);
        when(translator.translateBackendEnumValueStrictly("relatedContracts.numberType.id", "1")).thenReturn(ONE);
        when(translator.translateBackendEnumValueStrictly("cards.cardType.id", "R")).thenReturn(PREPAID_CARD);
        when(translator.translateBackendEnumValueStrictly("cards.physicalSupport.id", "S")).thenReturn("STICKER");
        when(translator.translateBackendEnumValueStrictly("cards.status.id", "I")).thenReturn(INOPERATIVE);
        when(translator.translateBackendEnumValueStrictly("cards.brandAssociation.id", "V")).thenReturn(VISA);
        when(translator.translateBackendEnumValueStrictly("cards.block.blockId", "LO")).thenReturn(LOST);
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "01")).thenReturn("ON");
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "02")).thenReturn("OF");
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "03")).thenReturn("XD");
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "04")).thenReturn("DD");
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "05")).thenReturn("OD");
        when(translator.translateBackendEnumValueStrictly("participants.participantType.id", "T")).thenReturn("HOLDER");
        when(translator.translateBackendEnumValueStrictly("participants.participantType.id", "A")).thenReturn("AUTHORIZED");

        when(inputHeaderManager.getHeader(HEADER_USER_AGENT)).thenReturn(HEADER_USER_AGENT_STUB);
    }

    @Test
    public void mapInFullTest() {
        DTOIntListCards dtoIn = entityMock.getDtoIntListCards();
        mapInEnum();
        FormatoMPMENL1 result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertEquals(result.getNumclie(), dtoIn.getCustomerId());
        assertEquals("C|D|P", result.getTiptarc());
        assertEquals("P", result.getSopfisl());
        assertEquals("A|B", result.getEstarjo());
        assertEquals("T|A", result.getTippart());
        assertEquals(result.getIdpagin(), dtoIn.getPaginationKey());
        assertEquals(result.getTampagi().toString(), dtoIn.getPageSize().toString());
    }

    @Test
    public void mapInWithOutTippartTest() {
        DTOIntListCards dtoIn = entityMock.getDtoIntListCards();
        dtoIn.setParticipantsParticipantTypeId(null);
        mapInEnum();
        FormatoMPMENL1 result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertEquals(result.getNumclie(), dtoIn.getCustomerId());
        assertEquals("C|D|P", result.getTiptarc());
        assertEquals("P", result.getSopfisl());
        assertEquals("A|B", result.getEstarjo());
        assertNull(result.getTippart());
        assertEquals(result.getIdpagin(), dtoIn.getPaginationKey());
        assertEquals(result.getTampagi().toString(), dtoIn.getPageSize().toString());
    }

    @Test
    public void mapInWithTippartTest() {
        DTOIntListCards dtoIn = entityMock.getDtoIntListCards();
        dtoIn.setParticipantsParticipantTypeId(Collections.singletonList("HOLDER"));
        mapInEnum();
        FormatoMPMENL1 result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertEquals(result.getNumclie(), dtoIn.getCustomerId());
        assertEquals("C|D|P", result.getTiptarc());
        assertEquals("P", result.getSopfisl());
        assertEquals("A|B", result.getEstarjo());
        assertEquals("T", result.getTippart());
        assertEquals(result.getIdpagin(), dtoIn.getPaginationKey());
        assertEquals(result.getTampagi().toString(), dtoIn.getPageSize().toString());
    }

    @Test
    public void mapInWithoutCustomerIdTest() {
        DTOIntListCards dtoIn = entityMock.getDtoIntListCards();
        dtoIn.setCustomerId(null);
        dtoIn.setCardTypeId(Collections.singletonList("CREDIT_CARD"));
        dtoIn.setStatusId(Collections.singletonList("OPERATIVE"));
        mapInEnum();
        FormatoMPMENL1 result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertNull(result.getNumclie());
        assertEquals("C", result.getTiptarc());
        assertEquals("P", result.getSopfisl());
        assertEquals("A", result.getEstarjo());
        assertEquals(result.getIdpagin(), dtoIn.getPaginationKey());
        assertEquals(result.getTampagi().toString(), dtoIn.getPageSize().toString());
    }

    @Test
    public void mapInWithoutCardTypeIdTest() {
        DTOIntListCards dtoIn = entityMock.getDtoIntListCards();
        dtoIn.setCardTypeId(null);
        mapInEnum();
        FormatoMPMENL1 result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertEquals(result.getNumclie(), dtoIn.getCustomerId());
        assertNull(result.getTiptarc());
        assertEquals("P", result.getSopfisl());
        assertEquals("A|B", result.getEstarjo());
        assertEquals(result.getIdpagin(), dtoIn.getPaginationKey());
        assertEquals(result.getTampagi().toString(), dtoIn.getPageSize().toString());
    }

    @Test
    public void mapInWithoutPhysicalSupportIdTest() {
        DTOIntListCards dtoIn = entityMock.getDtoIntListCards();
        dtoIn.setPhysicalSupportId(null);
        mapInEnum();
        FormatoMPMENL1 result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertEquals(result.getNumclie(), dtoIn.getCustomerId());
        assertEquals("C|D|P", result.getTiptarc());
        assertNull(result.getSopfisl());
        assertEquals("A|B", result.getEstarjo());
        assertEquals(result.getIdpagin(), dtoIn.getPaginationKey());
        assertEquals(result.getTampagi().toString(), dtoIn.getPageSize().toString());
    }

    @Test
    public void mapInWithoutStatusIdTest() {
        DTOIntListCards dtoIn = entityMock.getDtoIntListCards();
        dtoIn.setStatusId(null);
        mapInEnum();
        FormatoMPMENL1 result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertEquals(dtoIn.getCustomerId(), result.getNumclie());
        assertEquals("C|D|P", result.getTiptarc());
        assertEquals("P", result.getSopfisl());
        assertNull(result.getEstarjo());
        assertEquals(result.getIdpagin(), dtoIn.getPaginationKey());
        assertEquals(result.getTampagi().toString(), dtoIn.getPageSize().toString());
    }

    @Test
    public void mapInWithoutPaginationKeyTest() {
        DTOIntListCards dtoIn = entityMock.getDtoIntListCards();
        dtoIn.setPaginationKey(null);
        mapInEnum();
        FormatoMPMENL1 result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertEquals(result.getNumclie(), dtoIn.getCustomerId());
        assertEquals("C|D|P", result.getTiptarc());
        assertEquals("P", result.getSopfisl());
        assertEquals("A|B", result.getEstarjo());
        assertNull(result.getIdpagin());
        assertEquals(result.getTampagi().toString(), dtoIn.getPageSize().toString());
    }

    @Test
    public void mapInWithoutPageSizeTest() {
        DTOIntListCards dtoIn = entityMock.getDtoIntListCards();
        dtoIn.setPageSize(null);
        mapInEnum();
        FormatoMPMENL1 result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertEquals(result.getNumclie(), dtoIn.getCustomerId());
        assertEquals("C|D|P", result.getTiptarc());
        assertEquals("P", result.getSopfisl());
        assertEquals("A|B", result.getEstarjo());
        assertEquals(result.getIdpagin(), dtoIn.getPaginationKey());
        assertNull(result.getTampagi());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, null);
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        assertEquals(result.getStatus().getLastUpdatedDate(), mpms1L1.getFecsitt());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

        assertNotNull(result.getImages());
        //0
        assertEquals(mpms1L1.getIdimgt1(), result.getImages().get(0).getId());
        assertEquals(mpms1L1.getNimgta1(), result.getImages().get(0).getName());
        assertEquals("pg=0001&bin=491910&default_image=false&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=false", result.getImages().get(0).getUrl());
        //1
        assertEquals(mpms1L1.getIdimgt2(), result.getImages().get(1).getId());
        assertEquals(mpms1L1.getNimgta2(), result.getImages().get(1).getName());
        assertEquals("pg=0001&bin=491910&default_image=false&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=true", result.getImages().get(1).getUrl());

        //Blocks
        //0
        assertEquals("LOST", result.getBlocks().get(0).getBlockId());
        assertEquals(mpms1L1.getDesbloq(), result.getBlocks().get(0).getName());
        assertEquals(mpms1L1.getIderazo(), result.getBlocks().get(0).getReason().getId());
        assertEquals(mpms1L1.getDesrazo(), result.getBlocks().get(0).getReason().getName());
        assertEquals(mpms1L1.getFecbloq(), result.getBlocks().get(0).getBlockDate().getTime());
        assertTrue(result.getBlocks().get(0).getIsActive());

        //Activations
        //0
        assertEquals("ON", result.getActivations().get(0).getActivationId());
        assertTrue(result.getActivations().get(0).getIsActive());
        assertEquals("OF", result.getActivations().get(1).getActivationId());
        assertFalse(result.getActivations().get(1).getIsActive());
        assertEquals("XD", result.getActivations().get(2).getActivationId());
        assertTrue(result.getActivations().get(2).getIsActive());
        assertEquals("DD", result.getActivations().get(3).getActivationId());
        assertFalse(result.getActivations().get(3).getIsActive());
        assertEquals("OD", result.getActivations().get(4).getActivationId());
        assertTrue(result.getActivations().get(4).getIsActive());

        assertNotNull(result.getParticipants().get(0).getParticipantId());
        assertEquals(mpms1L1.getIdpart(), result.getParticipants().get(0).getParticipantId());
        assertNotNull(result.getParticipants().get(0).getFirstName());
        assertEquals(mpms1L1.getNompart(), result.getParticipants().get(0).getFirstName());
        assertNotNull(result.getParticipants().get(0).getLastName());
        assertEquals(mpms1L1.getApepart(), result.getParticipants().get(0).getLastName());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertEquals("HOLDER", result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getName());
        assertEquals(mpms1L1.getDespart(), result.getParticipants().get(0).getParticipantType().getName());
    }

    @Test
    public void mapOutWithoutParticipantsTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdpart(null);
        mpms1L1.setNompart(null);
        mpms1L1.setApepart(null);
        mpms1L1.setTippart(null);
        mpms1L1.setDespart(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, null);
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        assertEquals(result.getStatus().getLastUpdatedDate(), mpms1L1.getFecsitt());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

        assertNotNull(result.getImages());
        //0
        assertEquals(mpms1L1.getIdimgt1(), result.getImages().get(0).getId());
        assertEquals(mpms1L1.getNimgta1(), result.getImages().get(0).getName());
        assertEquals("pg=0001&bin=491910&default_image=false&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=false", result.getImages().get(0).getUrl());
        //1
        assertEquals(mpms1L1.getIdimgt2(), result.getImages().get(1).getId());
        assertEquals(mpms1L1.getNimgta2(), result.getImages().get(1).getName());
        assertEquals("pg=0001&bin=491910&default_image=false&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=true", result.getImages().get(1).getUrl());

        //Blocks
        //0
        assertEquals("LOST", result.getBlocks().get(0).getBlockId());
        assertEquals(mpms1L1.getDesbloq(), result.getBlocks().get(0).getName());
        assertEquals(mpms1L1.getIderazo(), result.getBlocks().get(0).getReason().getId());
        assertEquals(mpms1L1.getDesrazo(), result.getBlocks().get(0).getReason().getName());
        assertEquals(mpms1L1.getFecbloq(), result.getBlocks().get(0).getBlockDate().getTime());
        assertTrue(result.getBlocks().get(0).getIsActive());

        //Activations
        //0
        assertEquals("ON", result.getActivations().get(0).getActivationId());
        assertTrue(result.getActivations().get(0).getIsActive());
        assertEquals("OF", result.getActivations().get(1).getActivationId());
        assertFalse(result.getActivations().get(1).getIsActive());

        assertNull(result.getParticipants());
    }

    @Test
    public void mapOutWithoutParticipantsParticipantTypeTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);

        mpms1L1.setTippart(null);
        mpms1L1.setDespart(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, null);
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

        assertNotNull(result.getImages());
        //0
        assertEquals(mpms1L1.getIdimgt1(), result.getImages().get(0).getId());
        assertEquals(mpms1L1.getNimgta1(), result.getImages().get(0).getName());
        assertEquals("pg=0001&bin=491910&default_image=false&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=false", result.getImages().get(0).getUrl());
        //1
        assertEquals(mpms1L1.getIdimgt2(), result.getImages().get(1).getId());
        assertEquals(mpms1L1.getNimgta2(), result.getImages().get(1).getName());
        assertEquals("pg=0001&bin=491910&default_image=false&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=true", result.getImages().get(1).getUrl());

        //Blocks
        //0
        assertEquals("LOST", result.getBlocks().get(0).getBlockId());
        assertEquals(mpms1L1.getDesbloq(), result.getBlocks().get(0).getName());
        assertEquals(mpms1L1.getIderazo(), result.getBlocks().get(0).getReason().getId());
        assertEquals(mpms1L1.getDesrazo(), result.getBlocks().get(0).getReason().getName());
        assertEquals(mpms1L1.getFecbloq(), result.getBlocks().get(0).getBlockDate().getTime());
        assertTrue(result.getBlocks().get(0).getIsActive());

        //Activations
        //0
        assertEquals("ON", result.getActivations().get(0).getActivationId());
        assertTrue(result.getActivations().get(0).getIsActive());

        assertNotNull(result.getParticipants().get(0).getParticipantId());
        assertEquals(mpms1L1.getIdpart(), result.getParticipants().get(0).getParticipantId());
        assertNotNull(result.getParticipants().get(0).getFirstName());
        assertEquals(mpms1L1.getNompart(), result.getParticipants().get(0).getFirstName());
        assertNotNull(result.getParticipants().get(0).getLastName());
        assertEquals(mpms1L1.getApepart(), result.getParticipants().get(0).getLastName());
        assertNull(result.getParticipants().get(0).getParticipantType());

    }

    @Test
    public void mapOutDTOOutInitializedTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(1);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());
        //Blocks
        //0
        assertEquals("LOST", result.getBlocks().get(0).getBlockId());
        assertEquals(mpms1L1.getIderazo(), result.getBlocks().get(0).getReason().getId());
        assertEquals(mpms1L1.getDesrazo(), result.getBlocks().get(0).getReason().getName());
        assertEquals(mpms1L1.getFecbloq(), result.getBlocks().get(0).getBlockDate().getTime());
        assertFalse(result.getBlocks().get(0).getIsActive());
    }

    @Test
    public void mapOutWithoutFieldsSimpleTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setNumtarj(null);
        mpms1L1.setFecvenc(null);
        mpms1L1.setNombcli(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertNull(result.getNumber());
        assertNull(result.getCardId());
        assertNull(result.getExpirationDate());
        assertNull(result.getHolderName());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());
    }

    @Test
    public void mapOutWithoutNumberTypeTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdtnuco(null);
        mpms1L1.setDetnuco(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertNull(result.getNumberType());
        //assertEquals(result.getNumberType().getId(), ONE);
        //assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutIdtnucoTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdtnuco(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertNull(result.getNumberType().getId());
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());
    }

    @Test
    public void mapOutWithoutDetnucoTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setDetnuco(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertNull(result.getNumberType().getName());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutCurrenciesTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setMdispue(null);
        mpms1L1.setMdispub(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        assertNull(result.getCurrencies());
        /*//0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertEquals(result.getCurrencies().get(0).getIsMajor(), Boolean.TRUE);
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertEquals(result.getCurrencies().get(1).getIsMajor());
        */
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutMdispueTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setMdispue(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(0).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }


    @Test
    public void mapOutWithoutMdispubTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setMdispub(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutCardTypeTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setTiptarj(null);
        mpms1L1.setDtitarj(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertNull(result.getCardType());
        //assertEquals(result.getCardType().getId(), PREPAID_CARD);
        //assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutTiptarjeTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setTiptarj(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertNull(result.getCardType().getId());
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutDtitarjTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setDtitarj(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertNull(result.getCardType().getName());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutPhysicalSupportTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setSopfiss(null);
        mpms1L1.setDsopfis(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertNull(result.getPhysicalSupport());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutSopfissTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setSopfiss(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertNull(result.getPhysicalSupport().getId());
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutDsopfisTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setDsopfis(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertNull(result.getPhysicalSupport().getName());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutStatusTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setEstarjs(null);
        mpms1L1.setDestarj(null);
        mpms1L1.setFecsitt(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertNull(result.getStatus());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutEstarjsTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setEstarjs(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertNull(result.getStatus().getId());
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutDestarjTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setDestarj(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertNull(result.getStatus().getName());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutAvailableBalanceTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setSdispon(null);
        mpms1L1.setMdispon(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        assertNull(result.getAvailableBalance());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutSdisponTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setSdispon(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertNull(result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutMdisponTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setMdispon(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertNull(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutDisposedBalanceTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setSdispue(null);
        mpms1L1.setMdispue(null);
        mpms1L1.setSdispub(null);
        mpms1L1.setMdispub(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        assertNull(result.getCurrencies());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        assertNull(result.getDisposedBalance());
        //currentBalances
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutSdispueTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setSdispue(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertNull(result.getDisposedBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutCurrentBalancesWithoutMdispueTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setMdispue(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(0).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertNull(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutDisposedBalanceWithSizeOneTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setSdispue(null);
        mpms1L1.setMdispue(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(0).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().size(), 1);
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutSdispubTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setSdispub(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertNull(result.getDisposedBalance().getCurrentBalances().get(1).getAmount());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutAvailableBalanceWithoutMdispubTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setMdispub(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        //assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        //assertEquals(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertNull(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutcurrentBalancesWithSizeOneTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);

        mpms1L1.setSdispub(null);
        mpms1L1.setMdispub(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        assertEquals(result.getDisposedBalance().getCurrentBalances().size(), 1);
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutTitleTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIprotar(null);
        mpms1L1.setDprotar(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertNull(result.getTitle());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutIprotarTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIprotar(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertNull(result.getTitle().getId());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutDprotarTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setDprotar(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertNull(result.getTitle().getName());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutBrandAssociationTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdmatar(null);
        mpms1L1.setNomatar(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertNull(result.getBrandAssociation());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutIdmatarTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdmatar(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertNull(result.getBrandAssociation().getId());
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutNomatarTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setNomatar(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertNull(result.getBrandAssociation().getName());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutGrantedCreditsTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIlimcre(null);
        mpms1L1.setDlimcre(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        assertNull(result.getGrantedCredits());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutIlimcreTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIlimcre(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertNull(result.getGrantedCredits().get(0).getAmount());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutDlimcreTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setDlimcre(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertNull(result.getGrantedCredits().get(0).getCurrency());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

    }

    @Test
    public void mapOutWithoutRelatedContractTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdcorel(null);
        mpms1L1.setNucorel(null);
        mpms1L1.setIdtcore(null);
        mpms1L1.setDetcore(null);
        mpms1L1.setIdcortv(null);
        mpms1L1.setNucortv(null);
        mpms1L1.setIdtcotv(null);
        mpms1L1.setDetcotv(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertNull(result.getRelatedContracts());
    }

    @Test
    public void mapOutWithoutBlocksTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdebloq(null);
        mpms1L1.setIderazo(null);
        mpms1L1.setDesrazo(null);
        mpms1L1.setFecbloq(null);
        mpms1L1.setIdactbl(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertNull(result.getBlocks());
    }

    @Test
    public void mapOutWithoutblockId() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdebloq(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertNull(result.getBlocks().get(0).getBlockId());
        assertNotNull(result.getBlocks().get(0).getReason().getId());
        assertNotNull(result.getBlocks().get(0).getReason().getName());
        assertNotNull(result.getBlocks().get(0).getBlockDate());
        assertNotNull(result.getBlocks().get(0).getIsActive());
    }

    @Test
    public void mapOutWithoutblockName() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setDesbloq(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertNotNull(result.getBlocks().get(0).getBlockId());
        assertNull(result.getBlocks().get(0).getName());
        assertNotNull(result.getBlocks().get(0).getReason().getId());
        assertNotNull(result.getBlocks().get(0).getReason().getName());
        assertNotNull(result.getBlocks().get(0).getBlockDate());
        assertNotNull(result.getBlocks().get(0).getIsActive());
    }

    @Test
    public void mapOutWithoutReasonId() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIderazo(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertNotNull(result.getBlocks().get(0).getBlockId());
        assertNull(result.getBlocks().get(0).getReason().getId());
        assertNotNull(result.getBlocks().get(0).getReason().getName());
        assertNotNull(result.getBlocks().get(0).getBlockDate());
        assertNotNull(result.getBlocks().get(0).getIsActive());
    }

    @Test
    public void mapOutWithoutReasonName() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setDesrazo(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertNotNull(result.getBlocks().get(0).getBlockId());
        assertNotNull(result.getBlocks().get(0).getReason().getId());
        assertNull(result.getBlocks().get(0).getReason().getName());
        assertNotNull(result.getBlocks().get(0).getBlockDate());
        assertNotNull(result.getBlocks().get(0).getIsActive());
    }

    @Test
    public void mapOutWithoutBlockDate() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setFecbloq(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertNotNull(result.getBlocks().get(0).getBlockId());
        assertNotNull(result.getBlocks().get(0).getReason().getId());
        assertNotNull(result.getBlocks().get(0).getReason().getName());
        assertNull(result.getBlocks().get(0).getBlockDate());
        assertNotNull(result.getBlocks().get(0).getIsActive());
    }

    @Test
    public void mapOutWithoutIsActive() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdactbl(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertNotNull(result.getBlocks().get(0).getBlockId());
        assertNotNull(result.getBlocks().get(0).getReason().getId());
        assertNotNull(result.getBlocks().get(0).getReason().getName());
        assertNotNull(result.getBlocks().get(0).getBlockDate());
        assertNull(result.getBlocks().get(0).getIsActive());
    }

    @Test
    public void mapOutWithoutIdcorelTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdcorel(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        //0
        assertNull(result.getRelatedContracts().get(0).getRelatedContractId());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
    }

    @Test
    public void mapOutWithoutNucorelTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setNucorel(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
    }

    @Test
    public void mapOutRelatedContractWithoutFieldsSimplesTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdcorel(null);
        mpms1L1.setNucorel(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        //0
        assertNull(result.getRelatedContracts().get(0).getRelatedContractId());
        assertNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
    }

    @Test
    public void mapOutRelatedContractWithoutNumberTypeTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdtcore(null);
        mpms1L1.setDetcore(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertNull(result.getRelatedContracts().get(0).getNumberType());

    }

    @Test
    public void mapOutWithoutIdtcoreTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdtcore(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertNull(result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
    }

    @Test
    public void mapOutWithoutDetcoreTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setDetcore(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertNull(result.getRelatedContracts().get(0).getNumberType().getName());
    }

    @Test
    public void mapOutWithoutListImagenPosition0Test() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdimgt1(null);
        mpms1L1.setNimgta1(null);
        mpms1L1.setImgtar1(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, null);
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        assertEquals(result.getStatus().getLastUpdatedDate(), mpms1L1.getFecsitt());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

        assertNotNull(result.getImages());

        //1
        assertEquals(mpms1L1.getIdimgt2(), result.getImages().get(0).getId());
        assertEquals(mpms1L1.getNimgta2(), result.getImages().get(0).getName());
        assertEquals("pg=0001&bin=491910&default_image=false&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=true", result.getImages().get(0).getUrl());
    }

    @Test
    public void mapOutWithoutListImagenPosition1Test() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdimgt2(null);
        mpms1L1.setNimgta2(null);
        mpms1L1.setImgtar2(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, null);
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        assertEquals(result.getStatus().getLastUpdatedDate(), mpms1L1.getFecsitt());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

        assertNotNull(result.getImages());
        assertEquals(1, result.getImages().size());
        //0
        assertEquals(mpms1L1.getIdimgt1(), result.getImages().get(0).getId());
        assertEquals(mpms1L1.getNimgta1(), result.getImages().get(0).getName());
        assertEquals("pg=0001&bin=491910&default_image=false&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=false", result.getImages().get(0).getUrl());
    }

    @Test
    public void mapOutWithListImagenPosition1EmptyTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setImgtar2("");
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, null);
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        assertEquals(result.getStatus().getLastUpdatedDate(), mpms1L1.getFecsitt());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

        assertNotNull(result.getImages());
        assertEquals(1, result.getImages().size());
        //0
        assertEquals(mpms1L1.getIdimgt1(), result.getImages().get(0).getId());
        assertEquals(mpms1L1.getNimgta1(), result.getImages().get(0).getName());
        assertEquals("pg=0001&bin=491910&default_image=false&v=4&width=512&height=324&country=pe&app_id=com.bbva.wallet&issue_date=19700101&back=false", result.getImages().get(0).getUrl());
    }

    @Test
    public void mapOutWithoutListImageTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIdimgt1(null);
        mpms1L1.setNimgta1(null);
        mpms1L1.setImgtar1(null);
        mpms1L1.setIdimgt2(null);
        mpms1L1.setNimgta2(null);
        mpms1L1.setImgtar2(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, null);
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertEquals(result.getNumber(), mpms1L1.getNumtarj());
        //assertEquals(result.getCardId(), mpms1L1.getNumtarj());
        assertNotNull(result.getExpirationDate());
        assertEquals(result.getHolderName(), mpms1L1.getNombcli());
        //NumberType
        assertEquals(result.getNumberType().getId(), ONE);
        assertEquals(result.getNumberType().getName(), mpms1L1.getDetnuco());
        //currencies
        //0
        assertEquals(result.getCurrencies().get(0).getCurrency(), mpms1L1.getMdispue());
        assertTrue(result.getCurrencies().get(0).getIsMajor());
        //1
        assertEquals(result.getCurrencies().get(1).getCurrency(), mpms1L1.getMdispub());
        assertFalse(result.getCurrencies().get(1).getIsMajor());
        //cardType
        assertEquals(result.getCardType().getId(), PREPAID_CARD);
        assertEquals(result.getCardType().getName(), mpms1L1.getDtitarj());
        //physicalSupport
        assertEquals(result.getPhysicalSupport().getId(), STICKER);
        assertEquals(result.getPhysicalSupport().getName(), mpms1L1.getDsopfis());
        //status
        assertEquals(result.getStatus().getId(), INOPERATIVE);
        assertEquals(result.getStatus().getName(), mpms1L1.getDestarj());
        assertEquals(result.getStatus().getLastUpdatedDate(), mpms1L1.getFecsitt());
        //availableBalance
        //currentBalances
        //0
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispon());
        assertEquals(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispon());
        //disposedBalance
        //currentBalances
        //0
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getAmount(), mpms1L1.getSdispue());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency(), mpms1L1.getMdispue());
        //1
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getAmount(), mpms1L1.getSdispub());
        assertEquals(result.getDisposedBalance().getCurrentBalances().get(1).getCurrency(), mpms1L1.getMdispub());
        //title
        assertEquals(result.getTitle().getId(), mpms1L1.getIprotar());
        assertEquals(result.getTitle().getName(), mpms1L1.getDprotar());
        //brandAssociation
        assertEquals(result.getBrandAssociation().getId(), VISA);
        assertEquals(result.getBrandAssociation().getName(), mpms1L1.getNomatar());
        //grantedCredits
        //0
        assertEquals(result.getGrantedCredits().get(0).getAmount(), mpms1L1.getIlimcre());
        assertEquals(result.getGrantedCredits().get(0).getCurrency(), mpms1L1.getDlimcre());
        //relatedContracts
        assertEquals(2, result.getRelatedContracts().size());
        //0
        assertEquals(result.getRelatedContracts().get(0).getRelatedContractId(), mpms1L1.getIdcorel());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), mpms1L1.getNucorel());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getName(), mpms1L1.getDetcore());
        //1
        assertEquals(result.getRelatedContracts().get(1).getRelatedContractId(), mpms1L1.getIdcortv());
        assertEquals(result.getRelatedContracts().get(1).getNumber(), mpms1L1.getNucortv());
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getId(), ONE);
        assertEquals(result.getRelatedContracts().get(1).getNumberType().getName(), mpms1L1.getDetcotv());

        assertNull(result.getImages());

    }

    @Test
    public void mapOutWithuoutActivationIdTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setCodactv(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertNotNull(result.getActivations());
        assertNull(result.getActivations().get(0).getActivationId());
        assertNotNull(result.getActivations().get(0).getIsActive());
    }

    @Test
    public void mapOutWithuoutIsActiveTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setIndactv(null);
        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertNotNull(result.getActivations());
        assertNotNull(result.getActivations().get(0).getActivationId());
        assertNull(result.getActivations().get(0).getIsActive());
    }

    @Test
    public void mapOutWithuoutActivationListTest() throws IOException {
        mapOutEnum();
        FormatoMPMS1L1 mpms1L1 = formatsMpl1Mock.getFormatoMPMS1L1().get(0);
        mpms1L1.setCodactv(null);
        mpms1L1.setIndactv(null);
        mpms1L1.setCodreti(null);
        mpms1L1.setIndreti(null);
        mpms1L1.setCodinte(null);
        mpms1L1.setIndinte(null);
        mpms1L1.setCodcoex(null);
        mpms1L1.setIndcoex(null);
        mpms1L1.setCodsobr(null);
        mpms1L1.setIndsobr(null);

        DTOOutListCards dtoOut = mapper.mapOut(mpms1L1, null, new DTOOutListCards());
        assertNotNull(dtoOut);
        Card result = dtoOut.getData().get(0);

        assertNull(result.getActivations());
    }

    @Test
    public void mapOut2FullTest() {
        FormatoMPMS2L1 formatoMPMS2L1 = entityMock.getFormatoMPMS2L1();

        DTOOutListCards dtoOut = new DTOOutListCards();
        dtoOut = mapper.mapOut2(formatoMPMS2L1, null, dtoOut);

        assertNotNull(dtoOut);
        assertEquals(dtoOut.getPagination().getPaginationKey(), formatoMPMS2L1.getIdpagin());
        assertEquals(dtoOut.getPagination().getPageSize().toString(), formatoMPMS2L1.getTampagi().toString());
    }

    @Test
    public void mapOut2WithDTOoutIncializedTest() {
        FormatoMPMS2L1 formatoMPMS2L1 = entityMock.getFormatoMPMS2L1();

        DTOOutListCards dtoOut = mapper.mapOut2(formatoMPMS2L1, null, new DTOOutListCards());

        assertNotNull(dtoOut);
        assertEquals(dtoOut.getPagination().getPaginationKey(), formatoMPMS2L1.getIdpagin());
        assertEquals(dtoOut.getPagination().getPageSize().toString(), formatoMPMS2L1.getTampagi().toString());
    }

    @Test
    public void mapOut2WithformatoMPMS2L1IncializedTest() {
        FormatoMPMS2L1 formatoMPMS2L1 = new FormatoMPMS2L1();

        DTOOutListCards dtoOut = mapper.mapOut2(formatoMPMS2L1, null, new DTOOutListCards());

        assertNotNull(dtoOut);
        assertNull(dtoOut.getPagination());
    }

    @Test
    public void mapOut2WithOutIdpaginTest() {
        FormatoMPMS2L1 formatoMPMS2L1 = entityMock.getFormatoMPMS2L1();
        formatoMPMS2L1.setIdpagin(null);
        DTOOutListCards dtoOut = new DTOOutListCards();
        dtoOut = mapper.mapOut2(formatoMPMS2L1, null, dtoOut);

        assertNotNull(dtoOut);
        assertNull(dtoOut.getPagination().getPaginationKey());
        assertEquals(dtoOut.getPagination().getPageSize().toString(), formatoMPMS2L1.getTampagi().toString());
    }

    @Test
    public void mapOut2WithOutTampagiTest() {
        FormatoMPMS2L1 formatoMPMS2L1 = entityMock.getFormatoMPMS2L1();
        formatoMPMS2L1.setTampagi(null);
        DTOOutListCards dtoOut = new DTOOutListCards();
        dtoOut = mapper.mapOut2(formatoMPMS2L1, null, dtoOut);

        assertNotNull(dtoOut);
        assertEquals(dtoOut.getPagination().getPaginationKey(), formatoMPMS2L1.getIdpagin());
        assertNull(dtoOut.getPagination().getPageSize());
    }

}

package com.bbva.pzic.cards.facade.v0.security.validation;

import com.bbva.jee.arq.spring.core.servicing.security.validations.ApplicationSecurityValidator;
import com.bbva.pzic.cards.canonic.Block;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK;


/**
 * Created on 26/02/2018.
 *
 * @author Entelgy
 */
public class ModifyCardBlockSecurityValidatorV0Test {

    private final static String CARD_ID = "1234";
    private final static String BLOCK_ID = "12345";

    private ApplicationSecurityValidator securityValidator;

    @Before
    public void init() {
        securityValidator = new ModifyCardBlockSecurityValidatorV0();
    }

    @Test
    public void getOperationBlockisReissuedTrueTest() {
        List<Object> declaredParameters = new ArrayList<>();
        declaredParameters.add(CARD_ID);
        declaredParameters.add(BLOCK_ID);
        Block block = new Block();
        block.setIsReissued(Boolean.TRUE);
        declaredParameters.add(block);

        String result = securityValidator.getOperation(declaredParameters);
        Assert.assertNotNull(result);
        Assert.assertEquals(SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK.concat("OP002"), result);
    }

    @Test
    public void getOperationBlockisReissuedFalseTest() {
        List<Object> declaredParameters = new ArrayList<>();
        declaredParameters.add(CARD_ID);
        declaredParameters.add(BLOCK_ID);
        Block block = new Block();
        block.setIsReissued(Boolean.FALSE);
        declaredParameters.add(block);
        String result = securityValidator.getOperation(declaredParameters);
        Assert.assertNotNull(result);
        Assert.assertEquals(SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK.concat("OP001"), result);
    }

    @Test
    public void getOperationBlockisReissuedNullTest() {
        List<Object> declaredParameters = new ArrayList<>();
        declaredParameters.add(CARD_ID);
        declaredParameters.add(BLOCK_ID);
        Block block = new Block();
        block.setIsReissued(null);
        declaredParameters.add(block);
        String result = securityValidator.getOperation(declaredParameters);
        Assert.assertNotNull(result);
        Assert.assertEquals(SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK.concat("OP001"), result);
    }

    @Test
    public void getOperationBlockNullTest() {
        List<Object> declaredParameters = new ArrayList<>();
        declaredParameters.add(CARD_ID);
        declaredParameters.add(BLOCK_ID);
        declaredParameters.add(null);
        String result = securityValidator.getOperation(declaredParameters);
        Assert.assertNotNull(result);
        Assert.assertEquals(SMC_REGISTRY_ID_OF_MODIFY_CARD_BLOCK.concat("OP001"), result);
    }

}

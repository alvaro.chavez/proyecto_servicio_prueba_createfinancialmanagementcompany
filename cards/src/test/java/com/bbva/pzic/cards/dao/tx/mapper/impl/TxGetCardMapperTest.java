package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.jee.arq.spring.core.managers.InputHeaderManager;
import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.Condition;
import com.bbva.pzic.cards.canonic.PaymentAmount;
import com.bbva.pzic.cards.canonic.PaymentMethod;
import com.bbva.pzic.cards.dao.model.mpg1.*;
import com.bbva.pzic.cards.dao.model.mpg1.mock.FormatoMPG1Mock;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static com.bbva.pzic.cards.EntityMock.HEADER_USER_AGENT_STUB;
import static com.bbva.pzic.cards.util.Constants.DEFAULT_TIME;
import static com.bbva.pzic.cards.util.Constants.HEADER_USER_AGENT;
import static com.bbva.pzic.cards.util.Converter.convertFrom;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 17/10/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxGetCardMapperTest {

    @InjectMocks
    private TxGetCardMapper mapper;
    @Mock
    private Translator translator;
    @Mock
    private InputHeaderManager inputHeaderManager;

    private final DummyMock dummyMock = new DummyMock();
    private final FormatoMPG1Mock formatoMPG1Mock = FormatoMPG1Mock.getInstance();

    @Test
    public void mapInFullTest() {
        final DTOIntCard entity = new DTOIntCard();
        entity.setCardId(CARD_ID);
        final FormatoMPMENG1 result = mapper.mapIn(entity);
        assertNotNull(result);
        assertNotNull(result.getIdetarj());

        assertEquals(CARD_ID, result.getIdetarj());
    }

    private void enumMapOut() {
        when(translator.translateBackendEnumValueStrictly("cards.numberType.id", "1")).thenReturn("PAN");
        when(translator.translateBackendEnumValueStrictly("cards.cardType.id", "C")).thenReturn("CREDIT_CARD");
        when(translator.translateBackendEnumValueStrictly("participants.participantType.id", "T")).thenReturn("HOLDER");
        when(translator.translateBackendEnumValueStrictly("conditions.conditionId", "01")).thenReturn("CARD_MAINTENANCE_FEE");
        when(translator.translateBackendEnumValueStrictly("conditions.outcomes.outcomeType.id", "01")).thenReturn("FEE_AMOUNT");
        when(translator.translateBackendEnumValueStrictly("conditions.conditionId", "02")).thenReturn("REISSUE");
        when(translator.translateBackendEnumValueStrictly("cards.status.id", "A")).thenReturn("OPERATIVE");
        when(translator.translateBackendEnumValueStrictly("paymentMethod.paymentType.id", "T")).thenReturn("TOTAL_AMOUNT_PAYMENT");
        when(translator.translateBackendEnumValueStrictly("paymentMethod.paymentAmountsType.id", "1")).thenReturn("MINIMUM_AMOUNT");
        when(translator.translateBackendEnumValueStrictly("cards.rates.mode", "P")).thenReturn("PERCENTAGE");
        when(translator.translateBackendEnumValueStrictly("cards.rates.mode", "A")).thenReturn("AMOUNT");
        when(translator.translateBackendEnumValueStrictly("conditions.outcomes.factType.id", "01")).thenReturn("VALUE_LESSER_THAN");
        when(translator.translateBackendEnumValueStrictly("conditions.outcomes.factType.id", "02")).thenReturn("VALUE_GREATER_THAN");
        when(translator.translateBackendEnumValueStrictly("conditions.outcomes.apply.id", "01")).thenReturn("CURRENT_BALANCE");
        when(translator.translateBackendEnumValueStrictly("conditions.outcomes.apply.id", "02")).thenReturn("MONTHLY_AVERAGE_BALANCE");
        when(translator.translateBackendEnumValueStrictly("cards.brandAssociation.id", "V")).thenReturn("VISA");

        when(inputHeaderManager.getHeader(HEADER_USER_AGENT)).thenReturn(HEADER_USER_AGENT_STUB);
    }

    @Test
    public void mapOut1FullTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        final Card result = mapper.mapOut1(formatoMPMS1G1, new Card());

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getName());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getName());
        assertNotNull(result.getTitle());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getName());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNotNull(result.getCurrencies().get(1));
        assertNotNull(result.getCurrencies().get(1).getCurrency());
        assertNotNull(result.getCurrencies().get(1).getIsMajor());
        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getGrantedCredits().get(0));
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getAvailableBalance());
        assertNotNull(result.getAvailableBalance().getCurrentBalances());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0));
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());
        assertNotNull(result.getDisposedBalance().getCurrentBalances());
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0));
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0).getAmount());
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getStatus().getLastUpdatedDate());
        assertNotNull(result.getImages());
        assertNotNull(result.getImages().get(0));
        assertNotNull(result.getImages().get(0).getName());
        assertNotNull(result.getImages().get(0).getId());
        assertNotNull(result.getImages().get(0).getUrl());
        assertNotNull(result.getImages().get(1));
        assertNotNull(result.getImages().get(1).getName());
        assertNotNull(result.getImages().get(1).getId());
        assertNotNull(result.getImages().get(1).getUrl());
        assertNotNull(result.getIssueDate());
        assertNotNull(result.getCutOffDate());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getName());
        assertNotNull(result.getBank().getBranch().getId());
        assertNotNull(result.getBank().getBranch().getName());
        assertNotNull(result.getMembership().getId());
        assertNotNull(result.getMembership().getDescription());
        assertTrue(result.getIsBusiness());
        assertNotNull(result.getBrandAssociation());
        assertNotNull(result.getBrandAssociation().getId());
        assertNotNull(result.getBrandAssociation().getName());

        assertEquals(formatoMPMS1G1.getIdetarj(), result.getCardId());
        assertEquals(formatoMPMS1G1.getNumtarj(), result.getNumber());

        assertEquals("PAN", result.getNumberType().getId());
        assertEquals(formatoMPMS1G1.getDetnuco(), result.getNumberType().getName());

        assertEquals("CREDIT_CARD", result.getCardType().getId());
        assertEquals(formatoMPMS1G1.getDtitarj(), result.getCardType().getName());

        assertEquals(formatoMPMS1G1.getTipprod(), result.getTitle().getId());
        assertEquals(formatoMPMS1G1.getDescpro(), result.getTitle().getName());
        assertEquals(formatoMPMS1G1.getFecvenc(), result.getExpirationDate());
        assertEquals(formatoMPMS1G1.getNomtit(), result.getHolderName());

        assertEquals(formatoMPMS1G1.getDdispue(), result.getCurrencies().get(0).getCurrency());
        assertTrue(result.getCurrencies().get(0).getIsMajor());

        assertEquals(formatoMPMS1G1.getDdispub(), result.getCurrencies().get(1).getCurrency());
        assertFalse(result.getCurrencies().get(1).getIsMajor());

        assertEquals(formatoMPMS1G1.getLimcre(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getGrantedCredits().get(0).getCurrency());

        assertEquals(formatoMPMS1G1.getSaldisp(), result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals(1, result.getDisposedBalance().getCurrentBalances().size());

        assertEquals(formatoMPMS1G1.getSalusad(), result.getDisposedBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMonusad(), result.getDisposedBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals("OPERATIVE", result.getStatus().getId());
        assertEquals(formatoMPMS1G1.getDestarj(), result.getStatus().getName());
        assertEquals(formatoMPMS1G1.getFecsitt(), result.getStatus().getLastUpdatedDate());

        assertEquals(formatoMPMS1G1.getIdimgt(), result.getImages().get(0).getId());
        assertEquals(formatoMPMS1G1.getNimgta(), result.getImages().get(0).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(0).getUrl());

        assertEquals(formatoMPMS1G1.getIdimgt2(), result.getImages().get(1).getId());
        assertEquals(formatoMPMS1G1.getNimgta2(), result.getImages().get(1).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(1).getUrl());

        assertEquals(FunctionUtils.buildDatetime(formatoMPMS1G1.getFecent(), DEFAULT_TIME), result.getIssueDate());
        assertEquals(formatoMPMS1G1.getFecorte(), result.getCutOffDate());
        assertEquals(formatoMPMS1G1.getCodofba(), result.getBank().getId());
        assertEquals(formatoMPMS1G1.getDeofban(), result.getBank().getName());
        assertEquals(formatoMPMS1G1.getNuofges(), result.getBank().getBranch().getId());
        assertEquals(formatoMPMS1G1.getDeofges(), result.getBank().getBranch().getName());

        assertEquals(formatoMPMS1G1.getIdbenef(), result.getMembership().getId());
        assertEquals(formatoMPMS1G1.getDesbenf(), result.getMembership().getDescription());

        assertEquals(convertFrom(formatoMPMS1G1.getIndempr()), result.getIsBusiness());
        assertEquals("VISA", result.getBrandAssociation().getId());
        assertEquals(formatoMPMS1G1.getNomatar(), result.getBrandAssociation().getName());
    }

    @Test
    public void mapOut1IsBusinessFalseTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setIndempr("N");
        final Card result = mapper.mapOut1(formatoMPMS1G1, new Card());

        assertNotNull(result);
        assertFalse(result.getIsBusiness());
        assertEquals(convertFrom(formatoMPMS1G1.getIndempr()), result.getIsBusiness());
    }

    @Test
    public void mapOut1IsBusinessNullTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setIndempr(null);
        final Card result = mapper.mapOut1(formatoMPMS1G1, new Card());

        assertNotNull(result);
        assertNull(result.getIsBusiness());
    }

    @Test
    public void mapOut1WithoutBrandAssociationIdTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setIdmatar(null);
        final Card result = mapper.mapOut1(formatoMPMS1G1, new Card());

        assertNotNull(result);
        assertNotNull(result.getBrandAssociation());
        assertNull(result.getBrandAssociation().getId());
        assertNotNull(result.getBrandAssociation().getName());

        assertEquals(formatoMPMS1G1.getNomatar(), result.getBrandAssociation().getName());
    }

    @Test
    public void mapOut1WithoutBrandAssociationNameTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setNomatar(null);
        final Card result = mapper.mapOut1(formatoMPMS1G1, new Card());

        assertNotNull(result);
        assertNotNull(result.getBrandAssociation());
        assertNotNull(result.getBrandAssociation().getId());
        assertNull(result.getBrandAssociation().getName());

        assertEquals("VISA", result.getBrandAssociation().getId());
    }

    @Test
    public void mapOut1WithoutBrandAssociationIdNameTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setIdmatar(null);
        formatoMPMS1G1.setNomatar(null);
        final Card result = mapper.mapOut1(formatoMPMS1G1, new Card());

        assertNotNull(result);
        assertNull(result.getBrandAssociation());
    }

    @Test
    public void mapOut1WithCutOffDateNullTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setFecorte(null);
        final Card result = mapper.mapOut1(formatoMPMS1G1, new Card());

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getName());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getName());
        assertNotNull(result.getTitle());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getName());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNotNull(result.getCurrencies().get(1));
        assertNotNull(result.getCurrencies().get(1).getCurrency());
        assertNotNull(result.getCurrencies().get(1).getIsMajor());
        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getGrantedCredits().get(0));
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getAvailableBalance());
        assertNotNull(result.getAvailableBalance().getCurrentBalances());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0));
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());
        assertNotNull(result.getDisposedBalance().getCurrentBalances());
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0));
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0).getAmount());
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getImages());
        assertNotNull(result.getImages().get(0));
        assertNotNull(result.getImages().get(0).getName());
        assertNotNull(result.getImages().get(0).getId());
        assertNotNull(result.getImages().get(0).getUrl());
        assertNotNull(result.getImages().get(1));
        assertNotNull(result.getImages().get(1).getName());
        assertNotNull(result.getImages().get(1).getId());
        assertNotNull(result.getImages().get(1).getUrl());
        assertNotNull(result.getIssueDate());
        assertNull(result.getCutOffDay());

        assertEquals(formatoMPMS1G1.getIdetarj(), result.getCardId());
        assertEquals(formatoMPMS1G1.getNumtarj(), result.getNumber());

        assertEquals("PAN", result.getNumberType().getId());
        assertEquals(formatoMPMS1G1.getDetnuco(), result.getNumberType().getName());

        assertEquals("CREDIT_CARD", result.getCardType().getId());
        assertEquals(formatoMPMS1G1.getDtitarj(), result.getCardType().getName());

        assertEquals(formatoMPMS1G1.getTipprod(), result.getTitle().getId());
        assertEquals(formatoMPMS1G1.getDescpro(), result.getTitle().getName());
        assertEquals(formatoMPMS1G1.getFecvenc(), result.getExpirationDate());
        assertEquals(formatoMPMS1G1.getNomtit(), result.getHolderName());

        assertEquals(formatoMPMS1G1.getDdispue(), result.getCurrencies().get(0).getCurrency());
        assertTrue(result.getCurrencies().get(0).getIsMajor());

        assertEquals(formatoMPMS1G1.getDdispub(), result.getCurrencies().get(1).getCurrency());
        assertFalse(result.getCurrencies().get(1).getIsMajor());

        assertEquals(formatoMPMS1G1.getLimcre(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getGrantedCredits().get(0).getCurrency());

        assertEquals(formatoMPMS1G1.getSaldisp(), result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals(1, result.getDisposedBalance().getCurrentBalances().size());

        assertEquals(formatoMPMS1G1.getSalusad(), result.getDisposedBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMonusad(), result.getDisposedBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals("OPERATIVE", result.getStatus().getId());
        assertEquals(formatoMPMS1G1.getDestarj(), result.getStatus().getName());

        assertEquals(formatoMPMS1G1.getIdimgt(), result.getImages().get(0).getId());
        assertEquals(formatoMPMS1G1.getNimgta(), result.getImages().get(0).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(0).getUrl());

        assertEquals(formatoMPMS1G1.getIdimgt2(), result.getImages().get(1).getId());
        assertEquals(formatoMPMS1G1.getNimgta2(), result.getImages().get(1).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(1).getUrl());

        assertEquals(FunctionUtils.buildDatetime(formatoMPMS1G1.getFecent(), DEFAULT_TIME), result.getIssueDate());

    }

    @Test
    public void mapOut1WithoutGrantedCreditsTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setLimcre(null);
        formatoMPMS1G1.setMontar(null);
        final Card result = mapper.mapOut1(formatoMPMS1G1, new Card());

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getName());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getName());
        assertNotNull(result.getTitle());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getName());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNotNull(result.getCurrencies().get(1));
        assertNotNull(result.getCurrencies().get(1).getCurrency());
        assertNotNull(result.getCurrencies().get(1).getIsMajor());
        assertNull(result.getGrantedCredits());
        assertNotNull(result.getAvailableBalance());
        assertNotNull(result.getAvailableBalance().getCurrentBalances());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0));
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertNull(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());
        assertNotNull(result.getDisposedBalance().getCurrentBalances());
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0));
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0).getAmount());
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getImages());
        assertNotNull(result.getImages().get(0));
        assertNotNull(result.getImages().get(0).getName());
        assertNotNull(result.getImages().get(0).getId());
        assertNotNull(result.getImages().get(0).getUrl());
        assertNotNull(result.getImages().get(1));
        assertNotNull(result.getImages().get(1).getName());
        assertNotNull(result.getImages().get(1).getId());
        assertNotNull(result.getImages().get(1).getUrl());
        assertNotNull(result.getIssueDate());

        assertEquals(formatoMPMS1G1.getIdetarj(), result.getCardId());
        assertEquals(formatoMPMS1G1.getNumtarj(), result.getNumber());

        assertEquals("PAN", result.getNumberType().getId());
        assertEquals(formatoMPMS1G1.getDetnuco(), result.getNumberType().getName());

        assertEquals("CREDIT_CARD", result.getCardType().getId());
        assertEquals(formatoMPMS1G1.getDtitarj(), result.getCardType().getName());

        assertEquals(formatoMPMS1G1.getTipprod(), result.getTitle().getId());
        assertEquals(formatoMPMS1G1.getDescpro(), result.getTitle().getName());
        assertEquals(formatoMPMS1G1.getFecvenc(), result.getExpirationDate());
        assertEquals(formatoMPMS1G1.getNomtit(), result.getHolderName());

        assertEquals(formatoMPMS1G1.getDdispue(), result.getCurrencies().get(0).getCurrency());
        assertTrue(result.getCurrencies().get(0).getIsMajor());

        assertEquals(formatoMPMS1G1.getDdispub(), result.getCurrencies().get(1).getCurrency());
        assertFalse(result.getCurrencies().get(1).getIsMajor());

        assertEquals(formatoMPMS1G1.getSaldisp(), result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals(1, result.getDisposedBalance().getCurrentBalances().size());

        assertEquals(formatoMPMS1G1.getSalusad(), result.getDisposedBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMonusad(), result.getDisposedBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals("OPERATIVE", result.getStatus().getId());
        assertEquals(formatoMPMS1G1.getDestarj(), result.getStatus().getName());

        assertEquals(formatoMPMS1G1.getIdimgt(), result.getImages().get(0).getId());
        assertEquals(formatoMPMS1G1.getNimgta(), result.getImages().get(0).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(0).getUrl());

        assertEquals(formatoMPMS1G1.getIdimgt2(), result.getImages().get(1).getId());
        assertEquals(formatoMPMS1G1.getNimgta2(), result.getImages().get(1).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(1).getUrl());

        assertEquals(FunctionUtils.buildDatetime(formatoMPMS1G1.getFecent(), DEFAULT_TIME), result.getIssueDate());

    }

    @Test
    public void mapOut1WithoutDisposedBalanceTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setSalusad(BigDecimal.ZERO);
        formatoMPMS1G1.setMonusad(null);
        Card result = new Card();
        result = mapper.mapOut1(formatoMPMS1G1, result);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getName());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getName());
        assertNotNull(result.getTitle());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getName());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNotNull(result.getCurrencies().get(1));
        assertNotNull(result.getCurrencies().get(1).getCurrency());
        assertNotNull(result.getCurrencies().get(1).getIsMajor());
        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getGrantedCredits().get(0));
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getAvailableBalance());
        assertNotNull(result.getAvailableBalance().getCurrentBalances());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0));
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());
        assertNull(result.getDisposedBalance());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getImages());
        assertNotNull(result.getImages().get(0));
        assertNotNull(result.getImages().get(0).getName());
        assertNotNull(result.getImages().get(0).getId());
        assertNotNull(result.getImages().get(0).getUrl());
        assertNotNull(result.getImages().get(1));
        assertNotNull(result.getImages().get(1).getName());
        assertNotNull(result.getImages().get(1).getId());
        assertNotNull(result.getImages().get(1).getUrl());
        assertNotNull(result.getIssueDate());

        assertEquals(formatoMPMS1G1.getIdetarj(), result.getCardId());
        assertEquals(formatoMPMS1G1.getNumtarj(), result.getNumber());

        assertEquals("PAN", result.getNumberType().getId());
        assertEquals(formatoMPMS1G1.getDetnuco(), result.getNumberType().getName());

        assertEquals("CREDIT_CARD", result.getCardType().getId());
        assertEquals(formatoMPMS1G1.getDtitarj(), result.getCardType().getName());

        assertEquals(formatoMPMS1G1.getTipprod(), result.getTitle().getId());
        assertEquals(formatoMPMS1G1.getDescpro(), result.getTitle().getName());
        assertEquals(formatoMPMS1G1.getFecvenc(), result.getExpirationDate());
        assertEquals(formatoMPMS1G1.getNomtit(), result.getHolderName());

        assertEquals(formatoMPMS1G1.getDdispue(), result.getCurrencies().get(0).getCurrency());
        assertTrue(result.getCurrencies().get(0).getIsMajor());

        assertEquals(formatoMPMS1G1.getDdispub(), result.getCurrencies().get(1).getCurrency());
        assertFalse(result.getCurrencies().get(1).getIsMajor());

        assertEquals(formatoMPMS1G1.getLimcre(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getGrantedCredits().get(0).getCurrency());

        assertEquals(formatoMPMS1G1.getSaldisp(), result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals("OPERATIVE", result.getStatus().getId());
        assertEquals(formatoMPMS1G1.getDestarj(), result.getStatus().getName());

        assertEquals(formatoMPMS1G1.getIdimgt(), result.getImages().get(0).getId());
        assertEquals(formatoMPMS1G1.getNimgta(), result.getImages().get(0).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(0).getUrl());

        assertEquals(formatoMPMS1G1.getIdimgt2(), result.getImages().get(1).getId());
        assertEquals(formatoMPMS1G1.getNimgta2(), result.getImages().get(1).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(1).getUrl());

        assertEquals(FunctionUtils.buildDatetime(formatoMPMS1G1.getFecent(), DEFAULT_TIME), result.getIssueDate());

    }

    @Test
    public void mapOut1WithoutImages1Test() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setSalusad(BigDecimal.ZERO);
        formatoMPMS1G1.setMonusad(null);
        formatoMPMS1G1.setImgtar2(null);
        Card result = new Card();
        result = mapper.mapOut1(formatoMPMS1G1, result);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getName());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getName());
        assertNotNull(result.getTitle());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getName());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNotNull(result.getCurrencies().get(1));
        assertNotNull(result.getCurrencies().get(1).getCurrency());
        assertNotNull(result.getCurrencies().get(1).getIsMajor());
        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getGrantedCredits().get(0));
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getAvailableBalance());
        assertNotNull(result.getAvailableBalance().getCurrentBalances());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0));
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());
        assertNull(result.getDisposedBalance());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getImages());
        assertNotNull(result.getImages().get(0));
        assertNotNull(result.getImages().get(0).getName());
        assertNotNull(result.getImages().get(0).getId());
        assertNotNull(result.getImages().get(0).getUrl());
        assertNotNull(result.getIssueDate());

        assertEquals(formatoMPMS1G1.getIdetarj(), result.getCardId());
        assertEquals(formatoMPMS1G1.getNumtarj(), result.getNumber());

        assertEquals("PAN", result.getNumberType().getId());
        assertEquals(formatoMPMS1G1.getDetnuco(), result.getNumberType().getName());

        assertEquals("CREDIT_CARD", result.getCardType().getId());
        assertEquals(formatoMPMS1G1.getDtitarj(), result.getCardType().getName());

        assertEquals(formatoMPMS1G1.getTipprod(), result.getTitle().getId());
        assertEquals(formatoMPMS1G1.getDescpro(), result.getTitle().getName());
        assertEquals(formatoMPMS1G1.getFecvenc(), result.getExpirationDate());
        assertEquals(formatoMPMS1G1.getNomtit(), result.getHolderName());

        assertEquals(formatoMPMS1G1.getDdispue(), result.getCurrencies().get(0).getCurrency());
        assertTrue(result.getCurrencies().get(0).getIsMajor());

        assertEquals(formatoMPMS1G1.getDdispub(), result.getCurrencies().get(1).getCurrency());
        assertFalse(result.getCurrencies().get(1).getIsMajor());

        assertEquals(formatoMPMS1G1.getLimcre(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getGrantedCredits().get(0).getCurrency());

        assertEquals(formatoMPMS1G1.getSaldisp(), result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals("OPERATIVE", result.getStatus().getId());
        assertEquals(formatoMPMS1G1.getDestarj(), result.getStatus().getName());

        assertEquals(1, result.getImages().size());
        assertEquals(formatoMPMS1G1.getIdimgt(), result.getImages().get(0).getId());
        assertEquals(formatoMPMS1G1.getNimgta(), result.getImages().get(0).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(0).getUrl());

        assertEquals(FunctionUtils.buildDatetime(formatoMPMS1G1.getFecent(), DEFAULT_TIME), result.getIssueDate());

    }

    @Test
    public void mapOut1WithImages1EmptyTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setSalusad(BigDecimal.ZERO);
        formatoMPMS1G1.setMonusad(null);
        formatoMPMS1G1.setImgtar2("");
        Card result = new Card();
        result = mapper.mapOut1(formatoMPMS1G1, result);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getName());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getName());
        assertNotNull(result.getTitle());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getName());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNotNull(result.getCurrencies().get(1));
        assertNotNull(result.getCurrencies().get(1).getCurrency());
        assertNotNull(result.getCurrencies().get(1).getIsMajor());
        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getGrantedCredits().get(0));
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getAvailableBalance());
        assertNotNull(result.getAvailableBalance().getCurrentBalances());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0));
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());
        assertNull(result.getDisposedBalance());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getImages());
        assertNotNull(result.getImages().get(0));
        assertNotNull(result.getImages().get(0).getName());
        assertNotNull(result.getImages().get(0).getId());
        assertNotNull(result.getImages().get(0).getUrl());
        assertNotNull(result.getIssueDate());

        assertEquals(formatoMPMS1G1.getIdetarj(), result.getCardId());
        assertEquals(formatoMPMS1G1.getNumtarj(), result.getNumber());

        assertEquals("PAN", result.getNumberType().getId());
        assertEquals(formatoMPMS1G1.getDetnuco(), result.getNumberType().getName());

        assertEquals("CREDIT_CARD", result.getCardType().getId());
        assertEquals(formatoMPMS1G1.getDtitarj(), result.getCardType().getName());

        assertEquals(formatoMPMS1G1.getTipprod(), result.getTitle().getId());
        assertEquals(formatoMPMS1G1.getDescpro(), result.getTitle().getName());
        assertEquals(formatoMPMS1G1.getFecvenc(), result.getExpirationDate());
        assertEquals(formatoMPMS1G1.getNomtit(), result.getHolderName());

        assertEquals(formatoMPMS1G1.getDdispue(), result.getCurrencies().get(0).getCurrency());
        assertTrue(result.getCurrencies().get(0).getIsMajor());

        assertEquals(formatoMPMS1G1.getDdispub(), result.getCurrencies().get(1).getCurrency());
        assertFalse(result.getCurrencies().get(1).getIsMajor());

        assertEquals(formatoMPMS1G1.getLimcre(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getGrantedCredits().get(0).getCurrency());

        assertEquals(formatoMPMS1G1.getSaldisp(), result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals("OPERATIVE", result.getStatus().getId());
        assertEquals(formatoMPMS1G1.getDestarj(), result.getStatus().getName());

        assertEquals(1, result.getImages().size());
        assertEquals(formatoMPMS1G1.getIdimgt(), result.getImages().get(0).getId());
        assertEquals(formatoMPMS1G1.getNimgta(), result.getImages().get(0).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(0).getUrl());

        assertEquals(FunctionUtils.buildDatetime(formatoMPMS1G1.getFecent(), DEFAULT_TIME), result.getIssueDate());

    }

    @Test
    public void mapOutWithEmptyDdispubTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setDdispub("");
        final Card entity = dummyMock.getCardMock();
        final Card result = mapper.mapOut1(formatoMPMS1G1, entity);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getName());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getName());
        assertNotNull(result.getTitle());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getName());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());

        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getAvailableBalance());
        assertNotNull(result.getDisposedBalance());
        assertNotNull(result.getStatus());
        assertNotNull(result.getImages());
        assertNotNull(result.getIssueDate());

        assertEquals(formatoMPMS1G1.getIdetarj(), result.getCardId());
        assertEquals(formatoMPMS1G1.getNumtarj(), result.getNumber());

        assertEquals("PAN", result.getNumberType().getId());
        assertEquals(formatoMPMS1G1.getDetnuco(), result.getNumberType().getName());

        assertEquals("CREDIT_CARD", result.getCardType().getId());
        assertEquals(formatoMPMS1G1.getDtitarj(), result.getCardType().getName());

        assertEquals(formatoMPMS1G1.getTipprod(), result.getTitle().getId());
        assertEquals(formatoMPMS1G1.getDescpro(), result.getTitle().getName());
        assertEquals(formatoMPMS1G1.getFecvenc(), result.getExpirationDate());
        assertEquals(formatoMPMS1G1.getNomtit(), result.getHolderName());

        assertEquals(formatoMPMS1G1.getDdispue(), result.getCurrencies().get(0).getCurrency());
        assertTrue(result.getCurrencies().get(0).getIsMajor());

    }

    @Test
    public void mapOutWithNullDdispubTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        formatoMPMS1G1.setDdispub(null);
        final Card result = mapper.mapOut1(formatoMPMS1G1, new Card());

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getName());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getName());
        assertNotNull(result.getTitle());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getName());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());

        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getAvailableBalance());
        assertNotNull(result.getDisposedBalance());
        assertNotNull(result.getStatus());
        assertNotNull(result.getImages());
        assertNotNull(result.getIssueDate());

        assertEquals(1, result.getCurrencies().size());
        assertEquals(formatoMPMS1G1.getIdetarj(), result.getCardId());
        assertEquals(formatoMPMS1G1.getNumtarj(), result.getNumber());

        assertEquals("PAN", result.getNumberType().getId());
        assertEquals(formatoMPMS1G1.getDetnuco(), result.getNumberType().getName());

        assertEquals("CREDIT_CARD", result.getCardType().getId());
        assertEquals(formatoMPMS1G1.getDtitarj(), result.getCardType().getName());

        assertEquals(formatoMPMS1G1.getTipprod(), result.getTitle().getId());
        assertEquals(formatoMPMS1G1.getDescpro(), result.getTitle().getName());
        assertEquals(formatoMPMS1G1.getFecvenc(), result.getExpirationDate());
        assertEquals(formatoMPMS1G1.getNomtit(), result.getHolderName());

        assertEquals(formatoMPMS1G1.getDdispue(), result.getCurrencies().get(0).getCurrency());
        assertTrue(result.getCurrencies().get(0).getIsMajor());

    }

    @Test
    public void mapOut1EmptyTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1Emtpy();
        Card result = new Card();
        result = mapper.mapOut1(formatoMPMS1G1, result);

        assertNotNull(result);
        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getGrantedCredits().get(0));
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNull(result.getAvailableBalance());
        assertNull(result.getDisposedBalance());

        assertEquals(1, result.getGrantedCredits().size());
        assertNull(result.getCurrencies());
        assertNull(result.getGrantedCredits().get(0).getCurrency());
        assertNull(result.getStatus());
        assertNull(result.getImages());
    }

    @Test
    public void mapOut2FullTest() throws IOException {
        enumMapOut();
        FormatoMPMS2G1 formatoMPMS2G1 = formatoMPG1Mock.getFormatoMPMS2G1().get(0);
        Card result = mapper.mapOut2(formatoMPMS2G1, new Card());

        assertNotNull(result.getRelatedContracts());
        assertNotNull(result.getRelatedContracts().get(0));
        assertNotNull(result.getRelatedContracts().get(0).getRelatedContractId());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getName());

        assertEquals(formatoMPMS2G1.getIdcorel(), result.getRelatedContracts().get(0).getRelatedContractId());
        assertEquals(formatoMPMS2G1.getNumber(), result.getRelatedContracts().get(0).getNumber());
        assertEquals(formatoMPMS2G1.getNucorel(), result.getRelatedContracts().get(0).getContractId());
        assertEquals("PAN", result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(formatoMPMS2G1.getDetcore(), result.getRelatedContracts().get(0).getNumberType().getName());

        formatoMPMS2G1 = formatoMPG1Mock.getFormatoMPMS2G1().get(1);
        result = mapper.mapOut2(formatoMPMS2G1, result);

        assertNotNull(result.getRelatedContracts().get(1));
        assertNotNull(result.getRelatedContracts().get(1).getRelatedContractId());
        assertNotNull(result.getRelatedContracts().get(1).getNumber());
        assertNotNull(result.getRelatedContracts().get(1).getNumberType());
        assertNotNull(result.getRelatedContracts().get(1).getNumberType().getId());
        assertNotNull(result.getRelatedContracts().get(1).getNumberType().getName());

        assertEquals(formatoMPMS2G1.getIdcorel(), result.getRelatedContracts().get(1).getRelatedContractId());
        assertEquals(formatoMPMS2G1.getNumber(), result.getRelatedContracts().get(1).getNumber());
        assertEquals(formatoMPMS2G1.getNucorel(), result.getRelatedContracts().get(1).getContractId());
        assertEquals("PAN", result.getRelatedContracts().get(1).getNumberType().getId());
        assertEquals(formatoMPMS2G1.getDetcore(), result.getRelatedContracts().get(1).getNumberType().getName());

        formatoMPMS2G1 = formatoMPG1Mock.getFormatoMPMS2G1().get(2);
        result = mapper.mapOut2(formatoMPMS2G1, result);

        assertEquals(3, result.getRelatedContracts().size());
    }

    @Test
    public void mapOut1WithMapOut2FullTest() throws IOException {
        enumMapOut();
        final FormatoMPMS1G1 formatoMPMS1G1 = formatoMPG1Mock.getFormatoMPMS1G1();
        Card result = mapper.mapOut1(formatoMPMS1G1, new Card());

        FormatoMPMS2G1 formatoMPMS2G1 = formatoMPG1Mock.getFormatoMPMS2G1().get(0);
        result = mapper.mapOut2(formatoMPMS2G1, result);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getName());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getName());
        assertNotNull(result.getTitle());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getName());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0));
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNotNull(result.getCurrencies().get(1));
        assertNotNull(result.getCurrencies().get(1).getCurrency());
        assertNotNull(result.getCurrencies().get(1).getIsMajor());
        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getGrantedCredits().get(0));
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getAvailableBalance());
        assertNotNull(result.getAvailableBalance().getCurrentBalances());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0));
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertNotNull(result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());
        assertNotNull(result.getDisposedBalance().getCurrentBalances());
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0));
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0).getAmount());
        assertNotNull(result.getDisposedBalance().getCurrentBalances().get(0).getCurrency());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getStatus().getLastUpdatedDate());
        assertNotNull(result.getImages());
        assertNotNull(result.getImages().get(0));
        assertNotNull(result.getImages().get(0).getName());
        assertNotNull(result.getImages().get(0).getId());
        assertNotNull(result.getImages().get(0).getUrl());
        assertNotNull(result.getImages().get(1));
        assertNotNull(result.getImages().get(1).getName());
        assertNotNull(result.getImages().get(1).getId());
        assertNotNull(result.getImages().get(1).getUrl());
        assertNotNull(result.getIssueDate());
        assertNotNull(result.getCutOffDate());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getName());
        assertNotNull(result.getBank().getBranch().getId());
        assertNotNull(result.getBank().getBranch().getName());
        assertNotNull(result.getMembership().getId());
        assertNotNull(result.getMembership().getDescription());

        assertEquals(formatoMPMS1G1.getIdetarj(), result.getCardId());
        assertEquals(formatoMPMS1G1.getNumtarj(), result.getNumber());

        assertEquals("PAN", result.getNumberType().getId());
        assertEquals(formatoMPMS1G1.getDetnuco(), result.getNumberType().getName());

        assertEquals("CREDIT_CARD", result.getCardType().getId());
        assertEquals(formatoMPMS1G1.getDtitarj(), result.getCardType().getName());

        assertEquals(formatoMPMS1G1.getTipprod(), result.getTitle().getId());
        assertEquals(formatoMPMS1G1.getDescpro(), result.getTitle().getName());
        assertEquals(formatoMPMS1G1.getFecvenc(), result.getExpirationDate());
        assertEquals(formatoMPMS1G1.getNomtit(), result.getHolderName());

        assertEquals(formatoMPMS1G1.getDdispue(), result.getCurrencies().get(0).getCurrency());
        assertTrue(result.getCurrencies().get(0).getIsMajor());

        assertEquals(formatoMPMS1G1.getDdispub(), result.getCurrencies().get(1).getCurrency());
        assertFalse(result.getCurrencies().get(1).getIsMajor());

        assertEquals(formatoMPMS1G1.getLimcre(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getGrantedCredits().get(0).getCurrency());

        assertEquals(formatoMPMS1G1.getSaldisp(), result.getAvailableBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMontar(), result.getAvailableBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals(1, result.getDisposedBalance().getCurrentBalances().size());

        assertEquals(formatoMPMS1G1.getSalusad(), result.getDisposedBalance().getCurrentBalances().get(0).getAmount());
        assertEquals(formatoMPMS1G1.getMonusad(), result.getDisposedBalance().getCurrentBalances().get(0).getCurrency());

        assertEquals("OPERATIVE", result.getStatus().getId());
        assertEquals(formatoMPMS1G1.getDestarj(), result.getStatus().getName());
        assertEquals(formatoMPMS1G1.getFecsitt(), result.getStatus().getLastUpdatedDate());

        assertEquals(formatoMPMS1G1.getIdimgt(), result.getImages().get(0).getId());
        assertEquals(formatoMPMS1G1.getNimgta(), result.getImages().get(0).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(0).getUrl());

        assertEquals(formatoMPMS1G1.getIdimgt2(), result.getImages().get(1).getId());
        assertEquals(formatoMPMS1G1.getNimgta2(), result.getImages().get(1).getName());
        assertEquals("width=512&height=324&country=pe", result.getImages().get(1).getUrl());

        assertEquals(FunctionUtils.buildDatetime(formatoMPMS1G1.getFecent(), DEFAULT_TIME), result.getIssueDate());
        assertEquals(formatoMPMS1G1.getFecorte(), result.getCutOffDate());
        assertEquals(formatoMPMS1G1.getCodofba(), result.getBank().getId());
        assertEquals(formatoMPMS1G1.getDeofban(), result.getBank().getName());
        assertEquals(formatoMPMS1G1.getNuofges(), result.getBank().getBranch().getId());
        assertEquals(formatoMPMS1G1.getDeofges(), result.getBank().getBranch().getName());

        assertEquals(formatoMPMS1G1.getIdbenef(), result.getMembership().getId());
        assertEquals(formatoMPMS1G1.getDesbenf(), result.getMembership().getDescription());

        //*¨***************************


        assertNotNull(result.getRelatedContracts());
        assertNotNull(result.getRelatedContracts().get(0));
        assertNotNull(result.getRelatedContracts().get(0).getRelatedContractId());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getName());

        assertEquals(formatoMPMS2G1.getIdcorel(), result.getRelatedContracts().get(0).getRelatedContractId());
        assertEquals(formatoMPMS2G1.getNumber(), result.getRelatedContracts().get(0).getNumber());
        assertEquals(formatoMPMS2G1.getNucorel(), result.getRelatedContracts().get(0).getContractId());
        assertEquals("PAN", result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(formatoMPMS2G1.getDetcore(), result.getRelatedContracts().get(0).getNumberType().getName());

        formatoMPMS2G1 = formatoMPG1Mock.getFormatoMPMS2G1().get(1);
        result = mapper.mapOut2(formatoMPMS2G1, result);

        assertNotNull(result.getRelatedContracts().get(1));
        assertNotNull(result.getRelatedContracts().get(1).getRelatedContractId());
        assertNotNull(result.getRelatedContracts().get(1).getNumber());
        assertNotNull(result.getRelatedContracts().get(1).getNumberType());
        assertNotNull(result.getRelatedContracts().get(1).getNumberType().getId());
        assertNotNull(result.getRelatedContracts().get(1).getNumberType().getName());

        assertEquals(formatoMPMS2G1.getIdcorel(), result.getRelatedContracts().get(1).getRelatedContractId());
        assertEquals(formatoMPMS2G1.getNumber(), result.getRelatedContracts().get(1).getNumber());
        assertEquals(formatoMPMS2G1.getNucorel(), result.getRelatedContracts().get(1).getContractId());
        assertEquals("PAN", result.getRelatedContracts().get(1).getNumberType().getId());
        assertEquals(formatoMPMS2G1.getDetcore(), result.getRelatedContracts().get(1).getNumberType().getName());

        formatoMPMS2G1 = formatoMPG1Mock.getFormatoMPMS2G1().get(2);
        result = mapper.mapOut2(formatoMPMS2G1, result);

        assertEquals(3, result.getRelatedContracts().size());
    }

    @Test
    public void mapOut2WithCardNullTest() throws IOException {
        enumMapOut();
        FormatoMPMS2G1 formatoMPMS2G1 = formatoMPG1Mock.getFormatoMPMS2G1().get(0);
        Card result = new Card();
        result = mapper.mapOut2(formatoMPMS2G1, result);

        assertNotNull(result.getRelatedContracts());
        assertNotNull(result.getRelatedContracts().get(0));
        assertNotNull(result.getRelatedContracts().get(0).getRelatedContractId());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getName());

        assertEquals(formatoMPMS2G1.getIdcorel(), result.getRelatedContracts().get(0).getRelatedContractId());
        assertEquals(formatoMPMS2G1.getNumber(), result.getRelatedContracts().get(0).getNumber());
        assertEquals(formatoMPMS2G1.getNucorel(), result.getRelatedContracts().get(0).getContractId());
        assertEquals("PAN", result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(formatoMPMS2G1.getDetcore(), result.getRelatedContracts().get(0).getNumberType().getName());


        formatoMPMS2G1 = formatoMPG1Mock.getFormatoMPMS2G1().get(1);
        result = mapper.mapOut2(formatoMPMS2G1, result);

        assertNotNull(result.getRelatedContracts().get(1));
        assertNotNull(result.getRelatedContracts().get(1).getRelatedContractId());
        assertNotNull(result.getRelatedContracts().get(1).getNumber());
        assertNotNull(result.getRelatedContracts().get(1).getNumberType());
        assertNotNull(result.getRelatedContracts().get(1).getNumberType().getId());
        assertNotNull(result.getRelatedContracts().get(1).getNumberType().getName());

        assertEquals(formatoMPMS2G1.getIdcorel(), result.getRelatedContracts().get(1).getRelatedContractId());
        assertEquals(formatoMPMS2G1.getNumber(), result.getRelatedContracts().get(1).getNumber());
        assertEquals(formatoMPMS2G1.getNucorel(), result.getRelatedContracts().get(1).getContractId());
        assertEquals("PAN", result.getRelatedContracts().get(1).getNumberType().getId());
        assertEquals(formatoMPMS2G1.getDetcore(), result.getRelatedContracts().get(1).getNumberType().getName());

        formatoMPMS2G1 = formatoMPG1Mock.getFormatoMPMS2G1().get(2);
        result = mapper.mapOut2(formatoMPMS2G1, result);

        assertEquals(3, result.getRelatedContracts().size());


    }

    @Test
    public void mapOut3FullTest() throws IOException {
        enumMapOut();
        FormatoMPMS3G1 formatoMPMS3G1 = formatoMPG1Mock.getFormatoMPMS3G1().get(0);
        Card result = mapper.mapOut3(formatoMPMS3G1, new Card());

        assertNotNull(result);
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0));
        assertNotNull(result.getParticipants().get(0).getParticipantId());
        assertNotNull(result.getParticipants().get(0).getFirstName());
        assertNotNull(result.getParticipants().get(0).getLastName());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getName());

        assertEquals(formatoMPMS3G1.getIdpart(), result.getParticipants().get(0).getParticipantId());
        assertEquals(formatoMPMS3G1.getNompart(), result.getParticipants().get(0).getFirstName());
        assertEquals(formatoMPMS3G1.getApepart(), result.getParticipants().get(0).getLastName());
        assertEquals("HOLDER", result.getParticipants().get(0).getParticipantType().getId());
        assertEquals(formatoMPMS3G1.getDespart(), result.getParticipants().get(0).getParticipantType().getName());

        formatoMPMS3G1 = formatoMPG1Mock.getFormatoMPMS3G1().get(1);
        result = mapper.mapOut3(formatoMPMS3G1, result);

        assertNotNull(result.getParticipants().get(1));
        assertNotNull(result.getParticipants().get(1).getParticipantId());
        assertNotNull(result.getParticipants().get(1).getFirstName());
        assertNotNull(result.getParticipants().get(1).getLastName());
        assertNotNull(result.getParticipants().get(1).getParticipantType());
        assertNotNull(result.getParticipants().get(1).getParticipantType().getId());
        assertNotNull(result.getParticipants().get(1).getParticipantType().getName());

        assertEquals(formatoMPMS3G1.getIdpart(), result.getParticipants().get(1).getParticipantId());
        assertEquals(formatoMPMS3G1.getNompart(), result.getParticipants().get(1).getFirstName());
        assertEquals(formatoMPMS3G1.getApepart(), result.getParticipants().get(1).getLastName());
        assertEquals("HOLDER", result.getParticipants().get(1).getParticipantType().getId());
        assertEquals(formatoMPMS3G1.getDespart(), result.getParticipants().get(1).getParticipantType().getName());

        formatoMPMS3G1 = formatoMPG1Mock.getFormatoMPMS3G1().get(2);
        result = mapper.mapOut3(formatoMPMS3G1, result);

        assertEquals(3, result.getParticipants().size());
    }

    @Test
    public void mapOut3WithCardNullTest() throws IOException {
        enumMapOut();
        FormatoMPMS3G1 formatoMPMS3G1 = formatoMPG1Mock.getFormatoMPMS3G1().get(0);
        Card result = new Card();
        result = mapper.mapOut3(formatoMPMS3G1, result);

        assertNotNull(result);
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0));
        assertNotNull(result.getParticipants().get(0).getParticipantId());
        assertNotNull(result.getParticipants().get(0).getFirstName());
        assertNotNull(result.getParticipants().get(0).getLastName());
        assertNotNull(result.getParticipants().get(0).getParticipantType());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getId());
        assertNotNull(result.getParticipants().get(0).getParticipantType().getName());

        assertEquals(formatoMPMS3G1.getIdpart(), result.getParticipants().get(0).getParticipantId());
        assertEquals(formatoMPMS3G1.getNompart(), result.getParticipants().get(0).getFirstName());
        assertEquals(formatoMPMS3G1.getApepart(), result.getParticipants().get(0).getLastName());
        assertEquals("HOLDER", result.getParticipants().get(0).getParticipantType().getId());
        assertEquals(formatoMPMS3G1.getDespart(), result.getParticipants().get(0).getParticipantType().getName());

        formatoMPMS3G1 = formatoMPG1Mock.getFormatoMPMS3G1().get(1);
        result = mapper.mapOut3(formatoMPMS3G1, result);

        assertNotNull(result.getParticipants().get(1));
        assertNotNull(result.getParticipants().get(1).getParticipantId());
        assertNotNull(result.getParticipants().get(1).getFirstName());
        assertNotNull(result.getParticipants().get(1).getLastName());
        assertNotNull(result.getParticipants().get(1).getParticipantType());
        assertNotNull(result.getParticipants().get(1).getParticipantType().getId());
        assertNotNull(result.getParticipants().get(1).getParticipantType().getName());

        assertEquals(formatoMPMS3G1.getIdpart(), result.getParticipants().get(1).getParticipantId());
        assertEquals(formatoMPMS3G1.getNompart(), result.getParticipants().get(1).getFirstName());
        assertEquals(formatoMPMS3G1.getApepart(), result.getParticipants().get(1).getLastName());
        assertEquals("HOLDER", result.getParticipants().get(1).getParticipantType().getId());
        assertEquals(formatoMPMS3G1.getDespart(), result.getParticipants().get(1).getParticipantType().getName());

        formatoMPMS3G1 = formatoMPG1Mock.getFormatoMPMS3G1().get(2);
        result = mapper.mapOut3(formatoMPMS3G1, result);

        assertEquals(3, result.getParticipants().size());
    }

    @Test
    public void mapOut4FullWithConditionIdCardMaintenanceFeeTest() throws IOException {
        enumMapOut();
        FormatoMPMS4G1 formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1().get(0);
        Card card = mapper.mapOut4(formatoMPMS4G1, new Card());

        Condition result = card.getConditions().get(0);

        assertNotNull(result.getConditionId());
        assertNotNull(result.getName());
        assertNotNull(result.getPeriod().getId());
        assertNotNull(result.getPeriod().getName());
        assertNotNull(result.getPeriod().getCheckDate());
        assertNotNull(result.getPeriod().getStartDate());
        assertNotNull(result.getPeriod().getRemainingTime().getUnit());
        assertNotNull(result.getPeriod().getRemainingTime().getNumber());
        assertNotNull(result.getAccumulatedAmount().getAmount());
        assertNotNull(result.getAccumulatedAmount().getCurrency());
        assertNotNull(result.getFacts().get(0).getConditionAmount().getAmount());
        assertNotNull(result.getFacts().get(0).getConditionAmount().getCurrency());
        assertNotNull(result.getFacts().get(0).getFactType().getId());
        assertNotNull(result.getFacts().get(0).getFactType().getName());
        assertNotNull(result.getFacts().get(0).getApply().getId());
        assertNotNull(result.getFacts().get(0).getApply().getName());
        assertNotNull(result.getOutcomes());
        assertNotNull(result.getOutcomes().get(0));
        assertNotNull(result.getOutcomes().get(0).getOutcomeType());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getId());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getName());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getAmount());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getCurrency());
        assertNotNull(result.getOutcomes().get(0).getDueDate());

        assertEquals("CARD_MAINTENANCE_FEE", result.getConditionId());
        assertEquals(formatoMPMS4G1.getDsconme(), result.getName());
        assertEquals("FEE_AMOUNT", result.getOutcomes().get(0).getOutcomeType().getId());
        assertEquals(formatoMPMS4G1.getDscomme(), result.getOutcomes().get(0).getOutcomeType().getName());
        assertEquals(formatoMPMS4G1.getImporte(), result.getOutcomes().get(0).getFeeAmount().getAmount());
        assertEquals(formatoMPMS4G1.getMonimp(), result.getOutcomes().get(0).getFeeAmount().getCurrency());
        assertEquals(FunctionUtils.buildDatetime(formatoMPMS4G1.getFecmem(), DEFAULT_TIME), result.getOutcomes().get(0).getDueDate());
        assertEquals(formatoMPMS4G1.getIdperio(), result.getPeriod().getId());
        assertEquals(formatoMPMS4G1.getDsperio(), result.getPeriod().getName());
        assertEquals(FunctionUtils.buildDatetime(formatoMPMS4G1.getFeceval(), DEFAULT_TIME), result.getPeriod().getCheckDate());
        assertEquals(FunctionUtils.buildDatetime(formatoMPMS4G1.getFecinme(), DEFAULT_TIME), result.getPeriod().getStartDate());
        assertEquals("MONTHS", result.getPeriod().getRemainingTime().getUnit());
        assertEquals(formatoMPMS4G1.getCanmesm(), result.getPeriod().getRemainingTime().getNumber());
        assertEquals(formatoMPMS4G1.getImpacum(), result.getAccumulatedAmount().getAmount());
        assertEquals(formatoMPMS4G1.getMonimp(), result.getAccumulatedAmount().getCurrency());
        assertEquals(formatoMPMS4G1.getImpmeta(), result.getFacts().get(0).getConditionAmount().getAmount());
        assertEquals(formatoMPMS4G1.getMonimp(), result.getFacts().get(0).getConditionAmount().getCurrency());
        assertEquals("VALUE_LESSER_THAN", result.getFacts().get(0).getFactType().getId());
        assertEquals(formatoMPMS4G1.getDslsgol(), result.getFacts().get(0).getFactType().getName());
        assertEquals("CURRENT_BALANCE", result.getFacts().get(0).getApply().getId());
        assertEquals(formatoMPMS4G1.getDscubal(), result.getFacts().get(0).getApply().getName());

        formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1().get(1);
        card = mapper.mapOut4(formatoMPMS4G1, card);

        result = card.getConditions().get(1);

        assertNotNull(result);
        assertNotNull(result.getConditionId());
        assertNotNull(result.getName());
        assertNotNull(result.getPeriod());
        assertNotNull(result.getOutcomes());
        assertNotNull(result.getOutcomes().get(0));
        assertNotNull(result.getOutcomes().get(0).getOutcomeType());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getId());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getName());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getAmount());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getCurrency());
        assertNotNull(result.getOutcomes().get(0).getDueDate());

        formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1().get(2);
        card = mapper.mapOut4(formatoMPMS4G1, card);

        assertEquals(3, card.getConditions().size());
        assertNull(card.getConditions().get(2).getPeriod());
    }

    @Test
    public void mapOut4FullWithCardNullTest() throws IOException {
        enumMapOut();
        FormatoMPMS4G1 formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1().get(0);
        Card card = new Card();
        card = mapper.mapOut4(formatoMPMS4G1, card);

        Condition result = card.getConditions().get(0);

        assertNotNull(result.getConditionId());
        assertNotNull(result.getName());
        assertNotNull(result.getPeriod());
        assertNotNull(result.getOutcomes());
        assertNotNull(result.getOutcomes().get(0));
        assertNotNull(result.getOutcomes().get(0).getOutcomeType());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getId());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getName());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getAmount());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getCurrency());
        assertNotNull(result.getOutcomes().get(0).getDueDate());

        assertEquals("CARD_MAINTENANCE_FEE", result.getConditionId());
        assertEquals(formatoMPMS4G1.getDsconme(), result.getName());
        assertEquals("FEE_AMOUNT", result.getOutcomes().get(0).getOutcomeType().getId());
        assertEquals(formatoMPMS4G1.getDscomme(), result.getOutcomes().get(0).getOutcomeType().getName());
        assertEquals(formatoMPMS4G1.getImporte(), result.getOutcomes().get(0).getFeeAmount().getAmount());
        assertEquals(formatoMPMS4G1.getMonimp(), result.getOutcomes().get(0).getFeeAmount().getCurrency());

        // Index 1
        formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1().get(1);
        card = mapper.mapOut4(formatoMPMS4G1, card);

        result = card.getConditions().get(1);

        assertNotNull(result);
        assertNotNull(result.getConditionId());
        assertNotNull(result.getName());
        assertNotNull(result.getOutcomes());
        assertNotNull(result.getOutcomes().get(0));
        assertNotNull(result.getOutcomes().get(0).getOutcomeType());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getId());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getName());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getAmount());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getCurrency());
        assertNotNull(result.getOutcomes().get(0).getDueDate());
        assertNull(result.getPeriod().getRemainingTime());

        formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1().get(2);
        card = mapper.mapOut4(formatoMPMS4G1, card);

        assertEquals(3, card.getConditions().size());
        assertNull(card.getConditions().get(2).getPeriod());
    }

    @Test
    public void mapOut4FullWithoutFeeAmountTest() throws IOException {
        enumMapOut();
        FormatoMPMS4G1 formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1().get(0);
        formatoMPMS4G1.setImporte(null);
        formatoMPMS4G1.setMonimp(null);
        Card card = mapper.mapOut4(formatoMPMS4G1, new Card());

        Condition result = card.getConditions().get(0);

        assertNotNull(result.getConditionId());
        assertNotNull(result.getName());
        assertNotNull(result.getPeriod());
        assertNotNull(result.getOutcomes());
        assertNotNull(result.getOutcomes().get(0));
        assertNotNull(result.getOutcomes().get(0).getOutcomeType());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getId());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getName());
        assertNull(result.getOutcomes().get(0).getFeeAmount());
        assertNotNull(result.getOutcomes().get(0).getDueDate());

        assertEquals("CARD_MAINTENANCE_FEE", result.getConditionId());
        assertEquals(formatoMPMS4G1.getDsconme(), result.getName());
        assertEquals("FEE_AMOUNT", result.getOutcomes().get(0).getOutcomeType().getId());
        assertEquals(formatoMPMS4G1.getDscomme(), result.getOutcomes().get(0).getOutcomeType().getName());

        // Index 1
        formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1().get(1);
        card = mapper.mapOut4(formatoMPMS4G1, card);

        result = card.getConditions().get(1);

        assertNotNull(result.getConditionId());
        assertNotNull(result.getName());
        assertNotNull(result.getPeriod());
        assertNotNull(result.getOutcomes());
        assertNotNull(result.getOutcomes().get(0));
        assertNotNull(result.getOutcomes().get(0).getOutcomeType());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getId());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getName());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getAmount());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getCurrency());
        assertNotNull(result.getOutcomes().get(0).getDueDate());

        formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1().get(2);
        card = mapper.mapOut4(formatoMPMS4G1, card);

        assertEquals(3, card.getConditions().size());
        assertNull(card.getConditions().get(2).getPeriod());
    }

    @Test
    public void mapOut4FullWithConditionsIdReissueTest() throws IOException {
        enumMapOut();

        final FormatoMPMS4G1 formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1().get(0);
        formatoMPMS4G1.setIdconme("02");
        final Card card = mapper.mapOut4(formatoMPMS4G1, new Card());

        Condition result = card.getConditions().get(0);

        assertNotNull(result);
        assertNotNull(result.getConditionId());
        assertNotNull(result.getName());
        assertNotNull(result.getPeriod());
        assertNotNull(result.getOutcomes());
        assertNotNull(result.getOutcomes().get(0));
        assertNotNull(result.getOutcomes().get(0).getOutcomeType());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getId());
        assertNotNull(result.getOutcomes().get(0).getOutcomeType().getName());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getAmount());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getCurrency());
        assertNotNull(result.getOutcomes().get(0).getDueDate());

        assertEquals(formatoMPMS4G1.getImporte(), result.getOutcomes().get(0).getFeeAmount().getAmount());
        assertEquals(formatoMPMS4G1.getMonimp(), result.getOutcomes().get(0).getFeeAmount().getCurrency());
    }

    @Test
    public void mapOut4WithOutTest() throws IOException {
        enumMapOut();
        final FormatoMPMS4G1 formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS4G1Empty().get(0);
        formatoMPMS4G1.setIdconme("02");
        final Card card = mapper.mapOut4(formatoMPMS4G1, new Card());

        Condition result = card.getConditions().get(0);

        assertNotNull(result);
        assertNotNull(result.getConditionId());
        assertNull(result.getName());
        assertNull(result.getPeriod());
        assertNotNull(result.getOutcomes());
        assertNotNull(result.getOutcomes().get(0));
        assertNull(result.getOutcomes().get(0).getOutcomeType());
        assertNotNull(result.getOutcomes().get(0).getFeeAmount().getAmount());
        assertNull(result.getOutcomes().get(0).getFeeAmount().getCurrency());
        assertNull(result.getOutcomes().get(0).getDueDate());
        assertNull(result.getOutcomes().get(0).getFeeAmount().getCurrency());
    }

    @Test
    public void mapOut2InitializedTest() {
        final Card result = mapper.mapOut2(new FormatoMPMS2G1(), new Card());
        assertNotNull(result);
        assertNotNull(result.getRelatedContracts());
        assertEquals(1, result.getRelatedContracts().size());
    }

    @Test
    public void mapOut3InitializedTest() {
        final Card result = mapper.mapOut3(new FormatoMPMS3G1(), new Card());
        assertNotNull(result);
        assertNotNull(result.getParticipants());
        assertEquals(1, result.getParticipants().size());
    }

    @Test
    public void mapOut4InitializedTest() {
        final Card result = mapper.mapOut3(new FormatoMPMS3G1(), new Card());
        assertNotNull(result);
        assertNull(result.getConditions());
    }

    @Test
    public void mapOut5FullTest() throws IOException {
        enumMapOut();
        FormatoMPMS5G1 formatoMPMS4G1 = formatoMPG1Mock.getFormatoMPMS5G1();
        Card card = new Card();
        card = mapper.mapOut5(formatoMPMS4G1, card);
        assertNotNull(card);
        PaymentMethod result = card.getPaymentMethods().get(0);
        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getName());
        assertNotNull(result.getEndDate());
        assertNull(result.getPaymentAmounts());
        assertEquals(result.getId(), "TOTAL_AMOUNT_PAYMENT");
        assertEquals(result.getName(), formatoMPMS4G1.getDsforpa());
    }

    @Test
    public void mapOut6FullTest() throws IOException {
        enumMapOut();
        FormatoMPMS6G1 formatoMPMS6G1 = formatoMPG1Mock.getFormatoMPMS6G1List().get(0);
        Card result = new Card();
        result = mapper.mapOut6(formatoMPMS6G1, result);
        assertNotNull(result);
        assertNotNull(result.getPaymentMethods().get(0).getPaymentAmounts());
        PaymentAmount paymentAmount = result.getPaymentMethods().get(0).getPaymentAmounts().get(0);
        assertNotNull(paymentAmount.getId());
        assertNotNull(paymentAmount.getName());
        assertNotNull(paymentAmount.getValues().get(0).getAmount());
        assertNotNull(paymentAmount.getValues().get(0).getCurrency());

        assertEquals(paymentAmount.getId(), "MINIMUM_AMOUNT");
        assertEquals(paymentAmount.getName(), formatoMPMS6G1.getDsforpa());
        assertEquals(paymentAmount.getValues().get(0).getAmount(), formatoMPMS6G1.getImpamis());
        assertEquals(paymentAmount.getValues().get(0).getCurrency(), formatoMPMS6G1.getMopamis());
    }

    @Test
    public void mapOut6WithoutAmountTest() throws IOException {
        enumMapOut();
        FormatoMPMS6G1 formatoMPMS6G1 = formatoMPG1Mock.getFormatoMPMS6G1List().get(0);
        formatoMPMS6G1.setImpamis(null);
        Card result = new Card();
        result = mapper.mapOut6(formatoMPMS6G1, result);
        assertNotNull(result);
        assertNotNull(result.getPaymentMethods().get(0).getPaymentAmounts());
        PaymentAmount paymentAmount = result.getPaymentMethods().get(0).getPaymentAmounts().get(0);
        assertNotNull(paymentAmount.getId());
        assertNotNull(paymentAmount.getName());
        assertNull(paymentAmount.getValues().get(0).getAmount());
        assertNotNull(paymentAmount.getValues().get(0).getCurrency());

        assertEquals(paymentAmount.getId(), "MINIMUM_AMOUNT");
        assertEquals(paymentAmount.getName(), formatoMPMS6G1.getDsforpa());
        assertEquals(paymentAmount.getValues().get(0).getCurrency(), formatoMPMS6G1.getMopamis());
    }

    @Test
    public void mapOut6WithoutCurrencyTest() throws IOException {
        enumMapOut();
        FormatoMPMS6G1 formatoMPMS6G1 = formatoMPG1Mock.getFormatoMPMS6G1List().get(0);
        formatoMPMS6G1.setMopamis(null);
        Card result = new Card();
        result = mapper.mapOut6(formatoMPMS6G1, result);

        assertNotNull(result);
        assertNotNull(result.getPaymentMethods().get(0).getPaymentAmounts());
        PaymentAmount paymentAmount = result.getPaymentMethods().get(0).getPaymentAmounts().get(0);
        assertNotNull(paymentAmount.getId());
        assertNotNull(paymentAmount.getName());
        assertNotNull(paymentAmount.getValues().get(0).getAmount());
        assertNull(paymentAmount.getValues().get(0).getCurrency());

        assertEquals(paymentAmount.getId(), "MINIMUM_AMOUNT");
        assertEquals(paymentAmount.getName(), formatoMPMS6G1.getDsforpa());
        assertEquals(paymentAmount.getValues().get(0).getAmount(), formatoMPMS6G1.getImpamis());
    }

    @Test
    public void mapOut6WithoutValuesTest() throws IOException {
        enumMapOut();
        FormatoMPMS6G1 formatoMPMS6G1 = formatoMPG1Mock.getFormatoMPMS6G1List().get(0);
        formatoMPMS6G1.setMopamis(null);
        formatoMPMS6G1.setImpamis(null);
        Card result = new Card();
        result = mapper.mapOut6(formatoMPMS6G1, result);
        assertNotNull(result);
        assertNotNull(result.getPaymentMethods().get(0).getPaymentAmounts());
        PaymentAmount paymentAmount = result.getPaymentMethods().get(0).getPaymentAmounts().get(0);
        assertNotNull(paymentAmount.getId());
        assertNotNull(paymentAmount.getName());
        assertNull(paymentAmount.getValues());

        assertEquals(paymentAmount.getId(), "MINIMUM_AMOUNT");
        assertEquals(paymentAmount.getName(), formatoMPMS6G1.getDsforpa());
    }

    @Test
    public void mapOut7FullTest() throws IOException {
        enumMapOut();
        FormatoMPMS7G1 formatoMPMS7G1 = formatoMPG1Mock.getFormatoMPMS7G1List().get(0);
        Card result = new Card();
        result = mapper.mapOut7(formatoMPMS7G1, result);
        assertNotNull(result);
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().get(0).getRateType());
        assertNotNull(result.getRates().get(0).getRateType().getId());
        assertNotNull(result.getRates().get(0).getRateType().getName());
        assertNotNull(result.getRates().get(0).getUnit());
        assertNotNull(result.getRates().get(0).getUnit().getId());
        assertNotNull(result.getRates().get(0).getUnit().getName());
        assertNotNull(result.getRates().get(0).getUnit().getPercentage());

        assertEquals(formatoMPMS7G1.getIdttasa(), result.getRates().get(0).getRateType().getId());
        assertEquals(formatoMPMS7G1.getNomtasa(), result.getRates().get(0).getRateType().getName());
        assertEquals("PERCENTAGE", result.getRates().get(0).getUnit().getId());
        assertEquals(formatoMPMS7G1.getNomtipt(), result.getRates().get(0).getUnit().getName());
        assertEquals(formatoMPMS7G1.getValtasa(), result.getRates().get(0).getUnit().getPercentage());

        formatoMPMS7G1 = formatoMPG1Mock.getFormatoMPMS7G1List().get(1);
        result = mapper.mapOut7(formatoMPMS7G1, result);
        assertNotNull(result);
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().get(1).getRateType());
        assertNotNull(result.getRates().get(1).getRateType().getId());
        assertNotNull(result.getRates().get(1).getRateType().getName());
        assertNotNull(result.getRates().get(1).getUnit());
        assertNotNull(result.getRates().get(1).getUnit().getId());
        assertNotNull(result.getRates().get(1).getUnit().getName());
        assertNotNull(result.getRates().get(1).getUnit().getPercentage());

        assertEquals(formatoMPMS7G1.getIdttasa(), result.getRates().get(1).getRateType().getId());
        assertEquals(formatoMPMS7G1.getNomtasa(), result.getRates().get(1).getRateType().getName());
        assertEquals("AMOUNT", result.getRates().get(1).getUnit().getId());
        assertEquals(formatoMPMS7G1.getNomtipt(), result.getRates().get(1).getUnit().getName());
        assertEquals(formatoMPMS7G1.getValtasa(), result.getRates().get(1).getUnit().getPercentage());

        formatoMPMS7G1 = formatoMPG1Mock.getFormatoMPMS7G1List().get(2);
        result = mapper.mapOut7(formatoMPMS7G1, result);
        assertEquals(3, result.getRates().size());
        assertNull(result.getRates().get(2).getRateType());
        assertNull(result.getRates().get(2).getUnit().getName());
        assertNotNull(result.getRates().get(2).getUnit().getPercentage());
    }

    @Test
    public void mapOut8FullTest() throws IOException {
        FormatoMPMS8G1 formatoMPMS8G1 = formatoMPG1Mock.getFormatoMPMS8G1();
        Card result = new Card();

        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", formatoMPMS8G1.getCodactv())).thenReturn("ON_OFF");
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", formatoMPMS8G1.getCodreti())).thenReturn("CASHWITHDRAWAL_ACTIVATION");
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", formatoMPMS8G1.getCodinte())).thenReturn("ECOMMERCE_ACTIVATION");
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", formatoMPMS8G1.getCodcoex())).thenReturn("FOREIGN_PURCHASES_ACTIVATION");
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", formatoMPMS8G1.getCodsobr())).thenReturn("OVERDRAFT_ACTIVATION");
        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", formatoMPMS8G1.getCoddcvv())).thenReturn("DCVV");

        result = mapper.mapOut8(formatoMPMS8G1, result);

        assertNotNull(result.getActivations().get(0).getActivationId());
        assertNotNull(result.getActivations().get(0).getIsActive());
        assertNotNull(result.getActivations().get(1).getActivationId());
        assertNotNull(result.getActivations().get(1).getIsActive());
        assertNotNull(result.getActivations().get(2).getActivationId());
        assertNotNull(result.getActivations().get(2).getIsActive());
        assertNotNull(result.getActivations().get(3).getActivationId());
        assertNotNull(result.getActivations().get(3).getIsActive());
        assertNotNull(result.getActivations().get(4).getActivationId());
        assertNotNull(result.getActivations().get(4).getIsActive());
        assertNotNull(result.getActivations().get(5).getActivationId());
        assertNotNull(result.getActivations().get(5).getIsActive());

        assertEquals("ON_OFF", result.getActivations().get(0).getActivationId());
        assertTrue(result.getActivations().get(0).getIsActive());
        assertEquals("CASHWITHDRAWAL_ACTIVATION", result.getActivations().get(1).getActivationId());
        assertFalse(result.getActivations().get(1).getIsActive());
        assertEquals("ECOMMERCE_ACTIVATION", result.getActivations().get(2).getActivationId());
        assertTrue(result.getActivations().get(2).getIsActive());
        assertEquals("FOREIGN_PURCHASES_ACTIVATION", result.getActivations().get(3).getActivationId());
        assertFalse(result.getActivations().get(3).getIsActive());
        assertEquals("OVERDRAFT_ACTIVATION", result.getActivations().get(4).getActivationId());
        assertTrue(result.getActivations().get(4).getIsActive());
        assertEquals("DCVV", result.getActivations().get(5).getActivationId());
        assertFalse(result.getActivations().get(5).getIsActive());
    }

    @Test
    public void mapOut8EmptyTest() {
        Card result = new Card();
        result = mapper.mapOut8(new FormatoMPMS8G1(), result);

        assertNull(result.getActivations());
    }
}

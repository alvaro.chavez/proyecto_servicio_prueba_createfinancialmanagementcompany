package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMENG2;
import com.bbva.pzic.cards.dao.model.mpg2.FormatoMPMS1G2;
import com.bbva.pzic.cards.dao.model.mpg2.mock.FormatoMPMS1G2Mock;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static com.bbva.pzic.cards.EntityMock.ECOMMERCE_ACTIVATION;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 6/10/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxListCardActivationsMapperV0Test {

    private final FormatoMPMS1G2Mock formatoMPMS1G2Mock = new FormatoMPMS1G2Mock();
    @InjectMocks
    private TxListCardActivationsMapperV0 mapper;
    @Mock
    private Translator translator;

    @Test
    public void mapInFull() {
        final DTOIntCard dtoIntCard = new DTOIntCard();
        dtoIntCard.setCardId(CARD_ID);

        final FormatoMPMENG2 result = mapper.mapInt(dtoIntCard);

        assertNotNull(result);

        assertEquals(dtoIntCard.getCardId(), result.getIdetarj());
    }

    @Test
    public void mapIntEmpty() {
        final FormatoMPMENG2 result = mapper.mapInt(new DTOIntCard());

        assertNotNull(result);
        assertNull(result.getIdetarj());
    }

    @Test
    public void mapOutFull() throws IOException {
        final List<FormatoMPMS1G2> formatoMPMS1G2s = formatoMPMS1G2Mock.getFormatoMPMS1G2s();

        when(translator.translateBackendEnumValueStrictly("cards.activation.activationId", "03"))
                .thenReturn(ECOMMERCE_ACTIVATION);

        final FormatoMPMS1G2 formatoMPMS1G2 = formatoMPMS1G2s.get(2);
        final List<Activation> result = mapper.mapOut(formatoMPMS1G2, new ArrayList<>());

        assertNotNull(result.get(0).getActivationId());
        assertNotNull(result.get(0).getName());
        assertNotNull(result.get(0).getIsActive());

        assertEquals(ECOMMERCE_ACTIVATION, result.get(0).getActivationId());
        assertEquals(formatoMPMS1G2.getDesactv(), result.get(0).getName());
        assertTrue(result.get(0).getIsActive());
    }

    @Test
    public void mapOutWithFormatNullTest() {
        List<Activation> result = mapper.mapOut(new FormatoMPMS1G2(), new ArrayList<>());

        assertEquals(1, result.size());
    }
}

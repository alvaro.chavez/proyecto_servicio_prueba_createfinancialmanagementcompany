package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntMembership;
import com.bbva.pzic.cards.business.dto.DTOIntMembershipServiceResponse;
import com.bbva.pzic.cards.business.dto.DTOIntSearchCriteria;
import com.bbva.pzic.cards.business.dto.DTOIntStatus;
import com.bbva.pzic.cards.facade.v0.dto.Membership;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardMembershipMapper;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

;


@RunWith(MockitoJUnitRunner.class)
public class GetCardMembershipMapperTest {

    private static final String CARD_ID = "1234567890123456";

    @InjectMocks
    private IGetCardMembershipMapper getCardMembershipMapper = new GetCardMembershipMapper();

    @Mock
    private AbstractCypherTool cypherTool;

    @Before
    public void setUp() {

        Mockito.when(cypherTool.decrypt(Mockito.any(String.class), Mockito.eq(AbstractCypherTool.CARDID))).thenReturn(CARD_ID);
    }


    @Test
    public void testMapInput() {
        String membershipId = "1";

        DTOIntSearchCriteria dtoIntSearchCriteria = getCardMembershipMapper.mapInput("1234567890123456", membershipId);
        assertNotNull(dtoIntSearchCriteria);
        assertEquals(CARD_ID, dtoIntSearchCriteria.getCardId());
        assertEquals(membershipId, dtoIntSearchCriteria.getMembershipId());
    }


    @Test
    public void testMapOutput() throws IOException, ParseException {

        DTOIntMembershipServiceResponse dtoIntMembershipServiceResponse = new DTOIntMembershipServiceResponse();

        DTOIntMembership dtoIntMembership = new DTOIntMembership();

        dtoIntMembership.setId("01");
        dtoIntMembership.setDescription("LIFEMILE");
        dtoIntMembership.setNumber("00902638052");

        dtoIntMembership.setEnrollmentDate(new Date(
                new GregorianCalendar(2018, 3, 20).getTimeInMillis()));

        dtoIntMembership.setCancellationDate(new Date(
                new GregorianCalendar(2018, 7, 20).getTimeInMillis()));

        DTOIntStatus dtoIntStatus = new DTOIntStatus();
        dtoIntStatus.setId("S");

        dtoIntMembership.setStatus(dtoIntStatus);


        Boolean IssuedDueToMigration = false;
        dtoIntMembership.setIssuedDueToMigration(IssuedDueToMigration);

        dtoIntMembership.setExpirationDate(new Date(new GregorianCalendar(2018, 9, 29).getTimeInMillis()));

        dtoIntMembershipServiceResponse.setData(dtoIntMembership);

        ServiceResponse<Membership> response = getCardMembershipMapper.mapOutput(dtoIntMembershipServiceResponse);

        assertNotNull(response);
        assertNotNull(response.getData());
        assertNotNull(response.getData().getStatus());
        assertEquals(dtoIntMembershipServiceResponse.getData().getId(), response.getData().getId());
        assertEquals(dtoIntMembershipServiceResponse.getData().getDescription(), response.getData().getDescription());
        assertEquals(dtoIntMembershipServiceResponse.getData().getNumber(), response.getData().getNumber());
        assertEquals(dtoIntMembershipServiceResponse.getData().getEnrollmentDate(), response.getData().getEnrollmentDate());
        assertEquals(dtoIntMembershipServiceResponse.getData().getCancellationDate(), response.getData().getCancellationDate());
        assertEquals(dtoIntMembershipServiceResponse.getData().getStatus().getId(), response.getData().getStatus().getId());
        assertEquals(dtoIntMembershipServiceResponse.getData().getIssuedDueToMigration(), response.getData().getIssuedDueToMigration());


    }


}




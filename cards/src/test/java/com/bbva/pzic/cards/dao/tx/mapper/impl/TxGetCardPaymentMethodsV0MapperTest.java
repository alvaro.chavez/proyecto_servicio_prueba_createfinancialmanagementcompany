package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.canonic.PaymentMethod;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMENG5;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMS1G5;
import com.bbva.pzic.cards.dao.model.mpg5.FormatoMPMS2G5;
import com.bbva.pzic.cards.dao.model.mpg5.mock.FormatoMPMSNG5Mock;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static org.junit.Assert.*;

/**
 * Created on 25/10/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxGetCardPaymentMethodsV0MapperTest {

    private final FormatoMPMSNG5Mock formatoMPMSNG5Mock = new FormatoMPMSNG5Mock();

    @InjectMocks
    private TxGetCardPaymentMethodsMapperV0 mapper;

    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() {
        final DTOIntCard entity = new DTOIntCard();
        entity.setCardId(CARD_ID);
        final FormatoMPMENG5 result = mapper.mapIn(entity);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertEquals(CARD_ID, result.getIdetarj());
    }

    public void enumMapOut() {
        Mockito.when(translator.translateBackendEnumValueStrictly("paymentMethod.paymentType.id", "T")).thenReturn("TOTAL_AMOUNT_PAYMENT");
        Mockito.when(translator.translateBackendEnumValueStrictly("paymentMethod.paymentAmountsType.id", "1")).thenReturn("MINIMUM_AMOUNT");
        Mockito.when(translator.translateBackendEnumValueStrictly("paymentMethod.paymentAmountsType.id", "2")).thenReturn("MINIMUM_AMOUNT_TO_AVOID_INTEREST");
        Mockito.when(translator.translateBackendEnumValueStrictly("paymentMethod.paymentAmountsType.id", "3")).thenReturn("PENDING_MINIMUM_TO_AVOID_INTEREST_AMOUNT");
    }

    @Test
    public void mapOut1FullTest() {
        enumMapOut();
        final FormatoMPMS1G5 formatoMPMS1G5 = formatoMPMSNG5Mock.getFormatoMPMS1G5();
        final PaymentMethod result = mapper.mapOut1(formatoMPMS1G5, new PaymentMethod());

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getName());
        assertNotNull(result.getEndDate());

        assertEquals("TOTAL_AMOUNT_PAYMENT", result.getId());
        assertEquals(formatoMPMS1G5.getDsforpa(), result.getName());
        assertEquals(formatoMPMS1G5.getFecpago(), result.getEndDate());
    }

    @Test
    public void mapOut1WithEmptyTest() {
        final PaymentMethod result = mapper.mapOut1(new FormatoMPMS1G5(), new PaymentMethod());

        assertNotNull(result);
        assertNull(result.getId());
        assertNull(result.getName());
        assertNull(result.getEndDate());
    }

    @Test
    public void mapOut2FullTest() {
        enumMapOut();
        final FormatoMPMS2G5 formatoMPMS2G5 = formatoMPMSNG5Mock.getFormatoMPMS2G5().get(0);
        final PaymentMethod result = mapper.mapOut2(formatoMPMS2G5, new PaymentMethod());

        assertNotNull(result);
        assertNotNull(result.getPaymentAmounts());
        assertNotNull(result.getPaymentAmounts().get(0));
        assertNotNull(result.getPaymentAmounts().get(0).getId());
        assertNotNull(result.getPaymentAmounts().get(0).getName());
        assertNotNull(result.getPaymentAmounts().get(0).getValues());
        assertNotNull(result.getPaymentAmounts().get(0).getValues().get(0));
        assertNotNull(result.getPaymentAmounts().get(0).getValues().get(0).getAmount());
        assertNotNull(result.getPaymentAmounts().get(0).getValues().get(0).getCurrency());

        assertEquals("MINIMUM_AMOUNT", result.getPaymentAmounts().get(0).getId());
        assertEquals(formatoMPMS2G5.getDsforpa(), result.getPaymentAmounts().get(0).getName());
        assertEquals(formatoMPMS2G5.getImpamis(), result.getPaymentAmounts().get(0).getValues().get(0).getAmount());
        assertEquals(formatoMPMS2G5.getMopamis(), result.getPaymentAmounts().get(0).getValues().get(0).getCurrency());
    }

    @Test
    public void mapOut2WithoutValuesTest() {
        enumMapOut();
        final FormatoMPMS2G5 formatoMPMS2G5 = formatoMPMSNG5Mock.getFormatoMPMS2G5().get(1);
        formatoMPMS2G5.setImpamis(null);
        formatoMPMS2G5.setMopamis(null);
        final PaymentMethod result = mapper.mapOut2(formatoMPMS2G5, new PaymentMethod());

        assertNotNull(result);
        assertNotNull(result.getPaymentAmounts());
        assertNotNull(result.getPaymentAmounts().get(0));
        assertNotNull(result.getPaymentAmounts().get(0).getId());
        assertNotNull(result.getPaymentAmounts().get(0).getName());
        assertNull(result.getPaymentAmounts().get(0).getValues());

        assertEquals("MINIMUM_AMOUNT_TO_AVOID_INTEREST", result.getPaymentAmounts().get(0).getId());
        assertEquals(formatoMPMS2G5.getDsforpa(), result.getPaymentAmounts().get(0).getName());
    }

    @Test
    public void mapOut2EmptyTest() {
        enumMapOut();
        final FormatoMPMS2G5 formatoMPMS2G5 = formatoMPMSNG5Mock.getFormatoMPMS2G5Emtpy();
        final PaymentMethod result = mapper.mapOut2(formatoMPMS2G5, new PaymentMethod());
        assertNotNull(result);
        assertNotNull(result.getPaymentAmounts());
        assertNotNull(result.getPaymentAmounts().get(0));
        assertNotNull(result.getPaymentAmounts().get(0).getValues().get(0).getAmount());

        assertNull(result.getPaymentAmounts().get(0).getId());
        assertNull(result.getPaymentAmounts().get(0).getName());
        assertNull(result.getPaymentAmounts().get(0).getValues().get(0).getCurrency());
    }

    @Test
    public void mapOut2WithTwoFormatTypePENDING_MINIMUM_TO_AVOID_INTEREST_AMOUNTTest() {
        enumMapOut();
        final FormatoMPMS2G5 formatoMPMS2G5 = formatoMPMSNG5Mock.getFormatoMPMS2G5().get(2);
        final PaymentMethod result = mapper.mapOut2(formatoMPMS2G5, new PaymentMethod());

        assertNotNull(result);
        assertNotNull(result.getPaymentAmounts());
        assertNotNull(result.getPaymentAmounts().get(0));
        assertNotNull(result.getPaymentAmounts().get(0).getId());
        assertNotNull(result.getPaymentAmounts().get(0).getName());
        assertNotNull(result.getPaymentAmounts().get(0).getValues());
        assertNotNull(result.getPaymentAmounts().get(0).getValues().get(0));
        assertNotNull(result.getPaymentAmounts().get(0).getValues().get(0).getAmount());
        assertNotNull(result.getPaymentAmounts().get(0).getValues().get(0).getCurrency());

        assertEquals("PENDING_MINIMUM_TO_AVOID_INTEREST_AMOUNT", result.getPaymentAmounts().get(0).getId());
        assertEquals(formatoMPMS2G5.getDsforpa(), result.getPaymentAmounts().get(0).getName());
        assertEquals(formatoMPMS2G5.getImpamis(), result.getPaymentAmounts().get(0).getValues().get(0).getAmount());
        assertEquals(formatoMPMS2G5.getMopamis(), result.getPaymentAmounts().get(0).getValues().get(0).getCurrency());

        final FormatoMPMS2G5 format2 = formatoMPMSNG5Mock.getFormatoMPMS2G5().get(6);
        final PaymentMethod response = mapper.mapOut2(format2, result);
        assertNotNull(response);
        assertEquals(2, result.getPaymentAmounts().size());
    }
}

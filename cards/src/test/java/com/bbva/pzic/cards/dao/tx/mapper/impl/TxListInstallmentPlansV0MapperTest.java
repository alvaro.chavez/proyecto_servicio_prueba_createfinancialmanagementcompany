package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.DummyMock;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOInstallmentsPlanList;
import com.bbva.pzic.cards.business.dto.InputListInstallmentPlans;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMENGG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS1GG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS2GG;
import com.bbva.pzic.cards.dao.model.mpgg.FormatoMPMS3GG;
import com.bbva.pzic.cards.dao.model.mpgg.mock.FormatoMPMEMock;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 9/02/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxListInstallmentPlansV0MapperTest {

    @InjectMocks
    private TxListInstallmentPlansV0Mapper mapper;
    @Mock
    private EnumMapper enumMapper;

    private EntityMock entityMock = EntityMock.getInstance();
    private DummyMock dummyMock = new DummyMock();
    private FormatoMPMEMock formatoMPMEMock = new FormatoMPMEMock();

    @Before
    public void init() {
        Mockito.when(enumMapper.getEnumValue("installmentsPlans.financingType.id", "01")).thenReturn("MONTHS_WITHOUT_INTERESTS");
        Mockito.when(enumMapper.getEnumValue("installmentsPlans.terms.frequency", "D")).thenReturn("DAILY");
    }

    @Test
    public void mapperInFullTest() {
        InputListInstallmentPlans input = entityMock.getInputListInstallmentPlans();
        FormatoMPMENGG result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getIdpagin());
        assertNotNull(result.getNumtarj());
        assertNotNull(result.getTampagi());

        assertEquals(input.getCardId(), result.getNumtarj());
        assertEquals(input.getPaginationKey(), result.getIdpagin());
        assertEquals(input.getPageSize().toString(), result.getTampagi().toString());
    }

    @Test
    public void mapperInEmptyTest() {
        FormatoMPMENGG result = mapper.mapIn(new InputListInstallmentPlans());
        assertNotNull(result);
        assertNull(result.getIdpagin());
        assertNull(result.getNumtarj());
        assertNull(result.getTampagi());
    }

    @Test
    public void mapOutFullTest() throws IOException {

        FormatoMPMS1GG input = formatoMPMEMock.getFormatoMPMS1GG();
        DTOInstallmentsPlanList result = mapper.mapOut(input, null);


        assertNotNull(result);
        assertNotNull(result.getData());

        InstallmentsPlan data = result.getData().get(0);
        assertNotNull(data.getId());
        assertNotNull(data.getDescription());
        assertNotNull(data.getFinancingType().getId());
        assertNotNull(data.getFinancingType().getName());
        assertNotNull(data.getTransactionId());
        assertNotNull(data.getStartDate());
        assertNotNull(data.getFirstInstallmentDate());
        assertNotNull(data.getOperationDate());
        assertNotNull(data.getTerms().getNumber());
        assertNotNull(data.getTerms().getPending());
        assertNotNull(data.getTerms().getFrequency());
        assertNotNull(data.getTotal().getAmount());
        assertNotNull(data.getTotal().getCurrency());
        assertNotNull(data.getCapital().getAmount());
        assertNotNull(data.getCapital().getCurrency());
        assertNotNull(data.getOutstandingBalance().getAmount());
        assertNotNull(data.getOutstandingBalance().getCurrency());
        assertNotNull(data.getAmortizedAmount().getAmount());
        assertNotNull(data.getAmortizedAmount().getCurrency());
        assertNotNull(data.getAmortizedPercentage());
        assertNotNull(data.getFirstInstallment().getAmount());
        assertNotNull(data.getFirstInstallment().getCurrency());


        assertEquals(input.getIdentif(), data.getId());
        assertEquals(input.getDsentif(), data.getDescription());
        assertEquals("MONTHS_WITHOUT_INTERESTS", data.getFinancingType().getId());
        assertEquals(input.getDsfinan(), data.getFinancingType().getName());
        assertEquals(input.getIdentif(), data.getTransactionId());
        assertEquals(input.getTotcuo(), data.getTerms().getNumber());
        assertEquals(input.getPencuo(), data.getTerms().getPending());
        assertEquals("DAILY", data.getTerms().getFrequency());
        assertEquals(input.getImptot(), data.getTotal().getAmount());
        assertEquals(input.getMoneda(), data.getTotal().getCurrency());
        assertEquals(input.getImpori(), data.getCapital().getAmount());
        assertEquals(input.getMoneda(), data.getCapital().getCurrency());
        assertEquals(input.getMontres(), data.getOutstandingBalance().getAmount());
        assertEquals(input.getMoneda(), data.getOutstandingBalance().getCurrency());
        assertEquals(input.getMonpag(), data.getAmortizedAmount().getAmount());
        assertEquals(input.getMoneda(), data.getAmortizedAmount().getCurrency());
        assertEquals(input.getPorcpag(), data.getAmortizedPercentage());
        assertEquals(input.getImpcuo1(), data.getFirstInstallment().getAmount());
        assertEquals(input.getMoneda(), data.getFirstInstallment().getCurrency());
        assertNull(data.getScheduledPayments());
    }

    @Test
    public void mapOut2FullTest() throws IOException {
        FormatoMPMS2GG formatoMPMS2GG = formatoMPMEMock.getFormatoMPMS2GG();
        DTOInstallmentsPlanList installmentsPlanList = dummyMock.getDtoInstallmentsPlanList();
        installmentsPlanList = mapper.mapOut2(formatoMPMS2GG, installmentsPlanList);
        assertNotNull(installmentsPlanList);
        InstallmentsPlan result = installmentsPlanList.getData().get(0);
        assertNotNull(result);
        assertNotNull(result.getScheduledPayments());
        assertNotNull(result.getScheduledPayments().get(0).getMaturityDate());
        assertNotNull(result.getScheduledPayments().get(0).getTotal().getAmount());
        assertNotNull(result.getScheduledPayments().get(0).getTotal().getCurrency());

        assertEquals(formatoMPMS2GG.getMontpag(), result.getScheduledPayments().get(0).getTotal().getAmount());
        assertEquals(result.getCapital().getCurrency(), result.getScheduledPayments().get(0).getTotal().getCurrency());
    }

    @Test
    public void mapOut3FullTest() throws IOException {
        FormatoMPMS3GG formatoMPMS3GG = formatoMPMEMock.getFormatoMPMS3GG();
        DTOInstallmentsPlanList result = mapper.mapOut3(formatoMPMS3GG, null);
        assertNotNull(result);
        assertNotNull(result.getPagination());
        assertNotNull(result.getPagination().getPageSize());
        assertNotNull(result.getPagination().getPaginationKey());

        assertEquals(formatoMPMS3GG.getIdpagin(), result.getPagination().getPaginationKey());
        assertEquals(formatoMPMS3GG.getTampagi().intValue(), result.getPagination().getPageSize().intValue());

    }
}

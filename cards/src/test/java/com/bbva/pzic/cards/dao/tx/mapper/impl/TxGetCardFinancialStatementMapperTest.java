package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatement;
import com.bbva.pzic.cards.business.dto.DTOIntCardStatementDetail;
import com.bbva.pzic.cards.business.dto.InputGetCardFinancialStatement;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPME1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS1GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS2GH;
import com.bbva.pzic.cards.dao.model.mpgh.FormatoMPMS3GH;
import com.bbva.pzic.cards.dao.model.mpgh.mock.FormatsMPGHMock;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardFinancialStatementMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created on 28/07/2018.
 *
 * @author Entelgy
 */
public class TxGetCardFinancialStatementMapperTest {
    private ITxGetCardFinancialStatementMapper mapper;

    @Before
    public void setUp() {
        mapper = new TxGetCardFinancialStatementMapper();
    }

    @Test
    public void mapInFullTest() {
        InputGetCardFinancialStatement dtoIn = EntityMock.getInstance().getCardFinancialStatement();
        final FormatoMPME1GH result = mapper.mapIn(dtoIn);

        assertNotNull(result);
        assertNotNull(result.getIdetarj());
        assertNotNull(result.getIddocta());
        assertEquals(dtoIn.getCardId(), result.getIdetarj());
        assertEquals(dtoIn.getFinancialStatementId(), result.getIddocta());
    }

    @Test
    public void testMapResponseFormatToDtoOut() throws IOException {
        final FormatoMPMS1GH formato = FormatsMPGHMock.INSTANCE.getFormatoMPMS1GHNaturalLifeMilles();
        final DTOIntCardStatementDetail result = mapper.mapOut(formato).getDetail();

        Assert.assertEquals(result.getFormatsPan(), formato.getNumtarj());
        Assert.assertEquals(result.getProductName(), formato.getTiptarj());
        Assert.assertEquals(result.getBranchName(), formato.getOficina());

        Assert.assertEquals(result.getParticipantNameFirst(), formato.getTitptar());
        Assert.assertEquals(result.getParticipantNameSecond(), formato.getTitstar());
        Assert.assertEquals(result.getParticipantType(), formato.getIndtper());
        Assert.assertEquals(result.getAccountNumberPEN(), formato.getNumcuep());
        Assert.assertEquals(result.getAccountNumberUSD(), formato.getNumcueu());

        Assert.assertEquals(result.getProgram().getLifeMilesNumber(), formato.getNumlife());
        Assert.assertEquals(result.getProgram().getMonthPoints(), formato.getPuntmes());
        Assert.assertEquals(result.getProgram().getTotalPoints(), formato.getPunacum());
        Assert.assertEquals(result.getProgram().getBonus(), formato.getBonomes());

        Assert.assertEquals(result.getCardId(), formato.getNumcont());
        Assert.assertEquals(result.getCurrency(), formato.getDivcont());
        Assert.assertEquals(result.getEndDate(), formato.getFeccier());
        Assert.assertEquals(result.getPayType(), formato.getPagcarg());

        Assert.assertEquals(result.getAddress().getName(), formato.getDirptit());
        Assert.assertEquals(result.getAddress().getStreetType(), formato.getDirctit());
        Assert.assertEquals(result.getAddress().getState(), formato.getDirppro());
        Assert.assertEquals(result.getAddress().getUbigeo(), formato.getDirpubi());

        Assert.assertEquals(result.getCreditLimit(), formato.getLincred());
        Assert.assertEquals(result.getLimits().getDisposedBalance(), formato.getCreutil());
        Assert.assertEquals(result.getLimits().getAvailableBalance(), formato.getCredisp());

        Assert.assertEquals(result.getPaymentDate(), formato.getFeculpa());

        Assert.assertEquals(result.getInterest().getPurchasePEN(), formato.getTeacomp());
        Assert.assertEquals(result.getInterest().getPurchaseUSD(), formato.getTeacomu());
        Assert.assertEquals(result.getInterest().getAdvancedPEN(), formato.getTeaavap());
        Assert.assertEquals(result.getInterest().getAdvancedUSD(), formato.getTeaavau());
        Assert.assertEquals(result.getInterest().getCountervailingPEN(), formato.getTeacmpp());
        Assert.assertEquals(result.getInterest().getCountervailingUSD(), formato.getTeacmpu());
        Assert.assertEquals(result.getInterest().getArrearsPEN(), formato.getTeamorp());
        Assert.assertEquals(result.getInterest().getArrearsUSD(), formato.getTeamoru());

        Assert.assertEquals(result.getDisposedBalanceLocalCurrency().getAmount(), formato.getCreutip());
        Assert.assertEquals(result.getDisposedBalanceLocalCurrency().getCurrency(), DTOIntCardStatementDetail.PEN);
        Assert.assertEquals(result.getDisposedBalance().getAmount(), formato.getCreutiu());
        Assert.assertEquals(result.getDisposedBalance().getCurrency(), DTOIntCardStatementDetail.USD);

        Assert.assertEquals(result.getTotalTransactions(), formato.getNummovi());

        Assert.assertEquals(result.getLimits().getFinancingDisposedBalance(), formato.getImpcuot());

        Assert.assertEquals(formato.getImpatpe(), result.getExtraPayments().getOutstandingBalanceLocalCurrency().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getOutstandingBalanceLocalCurrency().getCurrency());
        Assert.assertEquals(formato.getMonmipe(), result.getExtraPayments().getMinimumCapitalLocalCurrency().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getMinimumCapitalLocalCurrency().getCurrency());
        Assert.assertEquals(formato.getImpinpe(), result.getExtraPayments().getInterestsBalanceLocalCurrency().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getInterestsBalanceLocalCurrency().getCurrency());
        Assert.assertEquals(formato.getImpcope(), result.getExtraPayments().getFeesBalanceLocalCurrency().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getFeesBalanceLocalCurrency().getCurrency());
        Assert.assertEquals(formato.getCuopape(), result.getExtraPayments().getFinancingTransactionLocalBalance().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getFinancingTransactionLocalBalance().getCurrency());
        Assert.assertEquals(formato.getPagmipe(), result.getExtraPayments().getInvoiceMinimumAmountLocalCurrency().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getInvoiceMinimumAmountLocalCurrency().getCurrency());
        Assert.assertEquals(formato.getPagtope(), result.getExtraPayments().getInvoiceAmountLocalCurrency().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.PEN, result.getExtraPayments().getInvoiceAmountLocalCurrency().getCurrency());

        Assert.assertEquals(formato.getImpatus(), result.getExtraPayments().getOutstandingBalance().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getOutstandingBalance().getCurrency());
        Assert.assertEquals(formato.getMonmius(), result.getExtraPayments().getMinimumCapitalCurrency().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getMinimumCapitalCurrency().getCurrency());
        Assert.assertEquals(formato.getImpinus(), result.getExtraPayments().getInterestsBalance().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getInterestsBalance().getCurrency());
        Assert.assertEquals(formato.getImpcous(), result.getExtraPayments().getFeesBalance().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getFeesBalance().getCurrency());
        Assert.assertEquals(formato.getCuopaus(), result.getExtraPayments().getFinancingTransactionBalance().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getFinancingTransactionBalance().getCurrency());
        Assert.assertEquals(formato.getPagmius(), result.getExtraPayments().getInvoiceMinimumAmountCurrency().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getInvoiceMinimumAmountCurrency().getCurrency());
        Assert.assertEquals(formato.getPagtous(), result.getExtraPayments().getInvoiceAmount().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.USD, result.getExtraPayments().getInvoiceAmount().getCurrency());

        Assert.assertEquals(formato.getImpdetp(), result.getTotalDebtLocalCurrency().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.PEN, result.getTotalDebtLocalCurrency().getCurrency());
        Assert.assertEquals(formato.getImpdetu(), result.getTotalDebt().getAmount());
        Assert.assertEquals(DTOIntCardStatementDetail.USD, result.getTotalDebt().getCurrency());

        Assert.assertEquals(formato.getMapmini(), result.getTotalMonthsAmortizationMinimum());
        Assert.assertEquals(formato.getMapminf(), result.getTotalMonthsAmortizationMinimumFull());
    }

    @Test
    public void testMapResponseFormatToDtoOut2() throws IOException {
        final List<FormatoMPMS2GH> formatos = FormatsMPGHMock.INSTANCE.getFormatoMPMS2GH();

        DTOIntCardStatement result = mapper.mapOut2(formatos.get(0), null);
        Assert.assertEquals(result.getRowCardStatement().size(), 1);
        Assert.assertEquals(result.getRowCardStatement().get(0), formatos.get(0).getLinescu());

        result = mapper.mapOut2(formatos.get(1), result);
        Assert.assertEquals(result.getRowCardStatement().size(), 2);
        Assert.assertEquals(result.getRowCardStatement().get(1), formatos.get(1).getLinescu());

        result = mapper.mapOut2(formatos.get(2), result);
        Assert.assertEquals(result.getRowCardStatement().size(), 3);
        Assert.assertEquals(result.getRowCardStatement().get(1), formatos.get(1).getLinescu());
    }

    @Test
    public void testMapResponseFormatToDtoOut3() throws IOException {
        final List<FormatoMPMS3GH> formatos = FormatsMPGHMock.INSTANCE.getFormatoMPMS3GH();

        DTOIntCardStatement result = mapper.mapOut3(formatos.get(0), null);
        Assert.assertEquals(result.getMessages().size(), 1);

        result = mapper.mapOut3(formatos.get(1), result);
        Assert.assertEquals(result.getMessages().size(), 2);

        result = mapper.mapOut3(formatos.get(2), result);
        Assert.assertEquals(result.getMessages().size(), 3);
    }
}

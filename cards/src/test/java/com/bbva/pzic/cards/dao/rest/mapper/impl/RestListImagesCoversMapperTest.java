package com.bbva.pzic.cards.dao.rest.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.Image;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesRequest;
import com.bbva.pzic.cards.dao.model.awsimages.AWSImagesResponse;
import com.bbva.pzic.cards.dao.rest.mock.stubs.AWSImagesStubs;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created on 04/11/2019.
 *
 * @author Entelgy
 */
public class RestListImagesCoversMapperTest {

    private final RestListImagesCoversMapper mapper = new RestListImagesCoversMapper();

    @Test
    public void mapInFull() throws IOException {
        List<Card> input = EntityMock.getInstance().fakeCards();

        AWSImagesRequest result = mapper.mapIn(input);

        assertEquals(7, result.getCards().size());

        assertNotNull(result.getCards().get(0).getBin());
        assertNotNull(result.getCards().get(0).getType().getId());
        assertNotNull(result.getCards().get(0).getIssueDate());
        assertNull(result.getCards().get(0).getIsBackImage());
        assertNotNull(result.getCards().get(1).getBin());
        assertNotNull(result.getCards().get(1).getType().getId());
        assertNotNull(result.getCards().get(1).getIssueDate());
        assertNotNull(result.getCards().get(1).getIsBackImage());
        assertNotNull(result.getCountry().getId());
        assertNotNull(result.getWidth());
        assertNotNull(result.getHeight());
        assertTrue(result.getIsDefaultImage());

        // pg=0021&bin=414791&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171123&type=V&width=512&height=324
        assertEquals("0021", result.getCards().get(0).getCodProd());
        assertEquals("414791", result.getCards().get(0).getBin());
        assertEquals("V", result.getCards().get(0).getType().getId());
        assertEquals("2017-11-23", result.getCards().get(0).getIssueDate());
        assertNull(result.getCards().get(0).getIsBackImage());

        // pg=0021&bin=414791&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171123&back=true&type=V&width=512&height=324
        assertEquals("0021", result.getCards().get(1).getCodProd());
        assertEquals("414791", result.getCards().get(1).getBin());
        assertEquals("V", result.getCards().get(1).getType().getId());
        assertEquals("2017-11-23", result.getCards().get(1).getIssueDate());
        assertTrue(result.getCards().get(1).getIsBackImage());

        assertEquals("0022", result.getCards().get(2).getCodProd());
        assertEquals("2017-11-24", result.getCards().get(2).getIssueDate());
        assertEquals("N", result.getCards().get(2).getType().getId());
        assertNull(result.getCards().get(2).getIsBackImage());

        assertEquals("0022", result.getCards().get(3).getCodProd());
        assertEquals("2017-11-24", result.getCards().get(3).getIssueDate());
        assertEquals("N", result.getCards().get(3).getType().getId());
        assertTrue(result.getCards().get(3).getIsBackImage());

        assertEquals("0023", result.getCards().get(4).getCodProd());
        assertNull(result.getCards().get(4).getIssueDate());
        assertTrue(result.getCards().get(4).getIsBackImage());

        assertEquals("0024", result.getCards().get(5).getCodProd());
        assertEquals("2017-11-26", result.getCards().get(5).getIssueDate());
        assertNull(result.getCards().get(5).getIsBackImage());

        assertEquals("0025", result.getCards().get(6).getCodProd());
        assertNull(result.getCards().get(6).getIssueDate());
        assertTrue(result.getCards().get(6).getIsBackImage());

        // General
        assertEquals("pe", result.getCountry().getId());
        assertEquals("512", result.getWidth());
        assertEquals("324", result.getHeight());
        assertTrue(result.getIsDefaultImage());
    }

    private String replaceSubstring(String url) {
        String segment[] = url.split("&");
        String values[] = segment[0].split("=");
        return url.replaceFirst(values[1], "");
    }

    @Test
    public void mapInWithoutCodProdPgFull() throws IOException {
        List<Card> input = EntityMock.getInstance().fakeCards();

        String url = input.get(1).getImages().get(0).getUrl();
        input.get(1).getImages().get(0).setUrl(replaceSubstring(url));

        url = input.get(1).getImages().get(1).getUrl();
        input.get(1).getImages().get(1).setUrl(replaceSubstring(url));

        AWSImagesRequest result = mapper.mapIn(input);

        assertEquals(7, result.getCards().size());

        assertNull(result.getCards().get(0).getCodProd());
        assertNotNull(result.getCards().get(0).getBin());
        assertNotNull(result.getCards().get(0).getType().getId());
        assertNotNull(result.getCards().get(0).getIssueDate());
        assertNull(result.getCards().get(0).getIsBackImage());

        assertNull(result.getCards().get(1).getCodProd());
        assertNotNull(result.getCards().get(1).getBin());
        assertNotNull(result.getCards().get(1).getType().getId());
        assertNotNull(result.getCards().get(1).getIssueDate());
        assertNotNull(result.getCards().get(1).getIsBackImage());
        assertNotNull(result.getCountry().getId());
        assertNotNull(result.getWidth());
        assertNotNull(result.getHeight());
        assertTrue(result.getIsDefaultImage());

        // pg=&bin=414791&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171123&type=V&width=512&height=324
        assertEquals("414791", result.getCards().get(0).getBin());
        assertEquals("V", result.getCards().get(0).getType().getId());
        assertEquals("2017-11-23", result.getCards().get(0).getIssueDate());
        assertNull(result.getCards().get(0).getIsBackImage());

        // pg=&bin=414791&default_image=false&v=4&country=pe&app_id=com.bbva.wallet&issue_date=20171123&back=true&type=V&width=512&height=324
        assertEquals("414791", result.getCards().get(1).getBin());
        assertEquals("V", result.getCards().get(1).getType().getId());
        assertEquals("2017-11-23", result.getCards().get(1).getIssueDate());
        assertTrue(result.getCards().get(1).getIsBackImage());
    }

    @Test
    public void mapOutFull() throws IOException {
        List<Card> input = EntityMock.getInstance().fakeCards();
        AWSImagesResponse response = AWSImagesStubs.INSTANCE.buildAWSImagesResponse();

        mapper.mapOut(response, input);

        assertEquals(9, input.size()); // 0, 2, x, 2, 0, 1, 1, y
        assertEquals(8, response.getData().size());

        // Card Index 0 empty
        // Card Index 1
        List<Image> images = input.get(1).getImages();
        assertEquals(2, images.size());
        assertEquals(response.getData().get(0).get(5).getValue(), images.get(0).getUrl()); // AWS Card 0
        assertNull(images.get(1).getUrl()); // AWS Card 1

        // Card Index 2 empty
        // Card Index 3
        images = input.get(3).getImages();
        assertEquals(2, images.size());
        assertEquals(response.getData().get(2).get(2).getValue(), images.get(0).getUrl()); // AWS Card 2
        assertEquals(response.getData().get(3).get(3).getValue(), images.get(1).getUrl()); // AWS Card 3

        // Card Index 4
        images = input.get(4).getImages();
        assertTrue(images.isEmpty());

        // Card Index 5
        images = input.get(5).getImages();
        assertEquals(2, images.size());
        assertEquals(response.getData().get(4).get(2).getValue(), images.get(0).getUrl()); // AWS Card 4
        assertNull(images.get(1).getUrl());

        // Card Index 6
        images = input.get(6).getImages();
        assertEquals(1, images.size());
        assertEquals(response.getData().get(5).get(2).getValue(), images.get(0).getUrl()); // AWS Card 5

        // Card Index 7
        images = input.get(7).getImages();
        assertEquals(1, images.size());
        assertNull(images.get(0).getUrl()); // AWS Card 6

        // Card Index 8
        images = input.get(8).getImages();
        assertEquals(1, images.size());
        assertNull(images.get(0).getUrl()); // AWS Card 7
    }

    @Test
    public void mapOutResponseNullFull() throws IOException {
        List<Card> input = EntityMock.getInstance().fakeCards();
        mapper.mapOut(null, input);
        assertEquals(9, input.size());
    }

    @Test
    public void mapOutNullFull() {
        List<Card> input = Collections.emptyList();
        mapper.mapOut(null, input);
        assertNotNull(input);
    }
}

package com.bbva.pzic.cards.dao.tx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputGetCardsCardOffer;
import com.bbva.pzic.cards.canonic.Offer;
import com.bbva.pzic.cards.dao.model.mpg9.FormatoMPMENG9;
import com.bbva.pzic.cards.dao.model.mpg9.FormatoMPMS1G9;
import com.bbva.pzic.cards.dao.model.mpg9.mock.FormatoMpg9Mock;
import com.bbva.pzic.cards.dao.tx.mapper.ITxGetCardsCardOfferMapper;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static com.bbva.pzic.cards.dao.tx.mapper.impl.TxGetCardsCardOfferMapper.OUTPUT_CARDS_OFFERTYPE_ID;
import static org.junit.Assert.*;

/**
 * Created on 09/11/2018.
 *
 * @author Entelgy
 */
public class TxGetCardsCardOfferMapperTest {

    private static final String IDTOFER_01 = "CREDIT_LIMIT_INCREASE";

    @InjectMocks
    private ITxGetCardsCardOfferMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        mapper = new TxGetCardsCardOfferMapper();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputGetCardsCardOffer input = EntityMock.getInstance().getInputGetCardsCardOffer();
        FormatoMPMENG9 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getNumtarj());
        assertNotNull(result.getNumofer());

        assertEquals(input.getCardId(), result.getNumtarj());
        assertEquals(input.getOfferId(), result.getNumofer());
    }

    @Test
    public void mapInNullTest() {
        FormatoMPMENG9 result = mapper.mapIn(null);

        assertNull(result);
    }

    @Test
    public void mapOutFullTest() {
        Mockito.when(enumMapper.getEnumValue(OUTPUT_CARDS_OFFERTYPE_ID, "01")).thenReturn(IDTOFER_01);

        FormatoMPMS1G9 input = FormatoMpg9Mock.getInstance().getFormatoMPMS1G9();
        Offer result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getOfferType().getId());
        assertNotNull(result.getOfferType().getName());
        assertNotNull(result.getOrigin().getId());
        assertNotNull(result.getOrigin().getDescription());
        assertNotNull(result.getIsEditable());
        assertNotNull(result.getDetails().getMinimumAmount().get(0).getAmount());
        assertNotNull(result.getDetails().getMinimumAmount().get(0).getCurrency());
        assertNotNull(result.getDetails().getMaximumAmount().get(0).getAmount());
        assertNotNull(result.getDetails().getMaximumAmount().get(0).getCurrency());
        assertNotNull(result.getDetails().getSuggestedAmount().get(0).getAmount());
        assertNotNull(result.getDetails().getSuggestedAmount().get(0).getCurrency());

        assertEquals(input.getNumofer(), result.getId());
        assertEquals(IDTOFER_01, result.getOfferType().getId());
        assertEquals(input.getDesofer(), result.getOfferType().getName());
        assertEquals(input.getIdorige(), result.getOrigin().getId());
        assertEquals(input.getDescori(), result.getOrigin().getDescription());
        assertEquals(true, result.getIsEditable());
        assertEquals(input.getLimmini(), result.getDetails().getMinimumAmount().get(0).getAmount());
        assertEquals(input.getDivisa(), result.getDetails().getMinimumAmount().get(0).getCurrency());

        assertEquals(input.getLimmaxi(), result.getDetails().getMaximumAmount().get(0).getAmount());
        assertEquals(input.getDivisa(), result.getDetails().getMaximumAmount().get(0).getCurrency());

        assertEquals(input.getLimofer(), result.getDetails().getSuggestedAmount().get(0).getAmount());
        assertEquals(input.getDivisa(), result.getDetails().getSuggestedAmount().get(0).getCurrency());
    }

    @Test
    public void mapOutFullEmptyTest() {
        FormatoMPMS1G9 input = FormatoMpg9Mock.getInstance().getFormatoMPMS1G9Empty();
        Offer result = mapper.mapOut(input);

        assertNotNull(result);
        assertNull(result.getId());
        assertNull(result.getOfferType());
        assertNull(result.getOrigin());
        assertNull(result.getIsEditable());
        assertNull(result.getDetails());
    }

    @Test
    public void mapOutFullNullTest() {
        Offer result = mapper.mapOut(null);

        assertNull(result);
    }
}


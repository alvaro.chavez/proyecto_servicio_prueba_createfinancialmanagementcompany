package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateOffersGenerateCards;
import com.bbva.pzic.cards.dao.model.phiat021_1.Cardproduct;
import com.bbva.pzic.cards.dao.model.phiat021_1.Cardproducts;
import com.bbva.pzic.cards.dao.model.phiat021_1.PeticionTransaccionPhiat021_1;
import com.bbva.pzic.cards.dao.model.phiat021_1.RespuestaTransaccionPhiat021_1;
import com.bbva.pzic.cards.dao.model.phiat021_1.mock.Phiat021_1Stubs;
import com.bbva.pzic.cards.facade.v0.dto.OfferGenerate;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created on 05/11/2020.
 *
 * @author Entelgy.
 */
public class ApxCreateOffersGenerateCardsMapperTest {

    private ApxCreateOffersGenerateCardsMapper mapper;

    @Before
    public void setup() {
        mapper = new ApxCreateOffersGenerateCardsMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputCreateOffersGenerateCards input = EntityMock.getInstance().buildInputCreateOffersGenerateCards();
        PeticionTransaccionPhiat021_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getBcsDeviceScreenSize());
        assertNotNull(result.getQuestions());
        assertEquals(2, result.getQuestions().size());
        assertNotNull(result.getQuestions().get(0).getQuestion().getId());
        assertNotNull(result.getQuestions().get(0).getQuestion().getAnswer());
        assertNotNull(result.getQuestions().get(0).getQuestion().getAnswer().getValue());
        assertNotNull(result.getQuestions().get(1).getQuestion().getId());
        assertNotNull(result.getQuestions().get(1).getQuestion().getAnswer());
        assertNotNull(result.getQuestions().get(1).getQuestion().getAnswer().getValue());
    }

    @Test
    public void mapInEmptyTest() {
        PeticionTransaccionPhiat021_1 result = mapper.mapIn(new InputCreateOffersGenerateCards());

        assertNotNull(result);
        assertNull(result.getBcsDeviceScreenSize());
        assertNull(result.getQuestions());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionPhiat021_1 input = Phiat021_1Stubs.getInstance().buildRespuestaTransaccionPhiat021_1();
        OfferGenerate result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getDictum());
        assertNotNull(result.getDictum().getId());
        assertNotNull(result.getDictum().getReasons());
        assertEquals(1, result.getDictum().getReasons().size());
        assertNotNull(result.getDictum().getReasons().get(0).getId());
        assertNotNull(result.getDictum().getReasons().get(0).getDescription());
        assertNotNull(result.getCardProducts());
        assertEquals(2, result.getCardProducts().size());
        assertNotNull(result.getCardProducts().get(0).getId());
        assertNotNull(result.getCardProducts().get(0).getName());
        assertNotNull(result.getCardProducts().get(0).getSubproduct());
        assertNotNull(result.getCardProducts().get(0).getSubproduct().getId());
        assertNotNull(result.getCardProducts().get(0).getCardType());
        assertNotNull(result.getCardProducts().get(0).getCardType().getId());
        assertNotNull(result.getCardProducts().get(0).getCardType().getName());
        assertNotNull(result.getCardProducts().get(0).getImages());
        assertEquals(1, result.getCardProducts().get(0).getImages().size());
        assertNotNull(result.getCardProducts().get(0).getImages().get(0).getId());
        assertNotNull(result.getCardProducts().get(0).getImages().get(0).getName());
        assertNotNull(result.getCardProducts().get(0).getImages().get(0).getUrl());
        assertNotNull(result.getCardProducts().get(0).getGrantedCredits());
        assertEquals(2, result.getCardProducts().get(0).getGrantedCredits().size());
        assertNotNull(result.getCardProducts().get(0).getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getCardProducts().get(0).getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getCardProducts().get(0).getGrantedCredits().get(1).getAmount());
        assertNotNull(result.getCardProducts().get(0).getGrantedCredits().get(1).getCurrency());
        assertEquals(1, result.getCardProducts().get(0).getRates().size());
        assertNotNull(result.getCardProducts().get(0).getRates().get(0).getRateType());
        assertNotNull(result.getCardProducts().get(0).getRates().get(0).getRateType().getId());
        assertNotNull(result.getCardProducts().get(0).getRates().get(0).getUnit());
        assertNotNull(result.getCardProducts().get(0).getRates().get(0).getUnit().getId());
        assertNotNull(result.getCardProducts().get(0).getRates().get(0).getUnit().getName());
        assertNotNull(result.getCardProducts().get(0).getRates().get(0).getUnit().getPercentage());
        assertNotNull(result.getCardProducts().get(0).getPriorityLevel());
        assertNotNull(result.getCardProducts().get(0).getBankIdentificationNumber());
        assertNotNull(result.getCardProducts().get(0).getFees());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees());
        assertEquals(2, result.getCardProducts().get(0).getFees().getItemizeFees().size());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees().get(1).getFeeType());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees().get(1).getItemizeFeeUnit());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getCardProducts().get(0).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getCardProducts().get(0).getBenefits());
        assertEquals(1, result.getCardProducts().get(0).getBenefits().size());
        assertNotNull(result.getCardProducts().get(0).getBenefits().get(0).getId());
        assertNotNull(result.getCardProducts().get(0).getBenefits().get(0).getName());
        assertNotNull(result.getCardProducts().get(0).getBenefits().get(0).getDescription());
        assertNotNull(result.getCardProducts().get(0).getGrantedMinimumCredits());
        assertEquals(1, result.getCardProducts().get(0).getGrantedMinimumCredits().size());
        assertNotNull(result.getCardProducts().get(0).getGrantedMinimumCredits().get(0).getAmount());
        assertNotNull(result.getCardProducts().get(0).getGrantedMinimumCredits().get(0).getCurrency());
        assertNotNull(result.getCardProducts().get(0).getLoyaltyProgram());
        assertNotNull(result.getCardProducts().get(0).getLoyaltyProgram().getId());
        assertNotNull(result.getCardProducts().get(0).getBrandAssociation());
        assertNotNull(result.getCardProducts().get(0).getBrandAssociation().getId());
        assertNotNull(result.getCardProducts().get(0).getOfferId());
        assertNotNull(result.getCardProducts().get(1).getId());
        assertNotNull(result.getCardProducts().get(1).getName());
        assertNotNull(result.getCardProducts().get(1).getCardType());
        assertNotNull(result.getCardProducts().get(1).getCardType().getId());
        assertNotNull(result.getCardProducts().get(1).getCardType().getName());
        assertNotNull(result.getCardProducts().get(1).getImages());
        assertEquals(1, result.getCardProducts().get(1).getImages().size());
        assertNotNull(result.getCardProducts().get(1).getImages().get(0).getId());
        assertNotNull(result.getCardProducts().get(1).getImages().get(0).getName());
        assertNotNull(result.getCardProducts().get(1).getImages().get(0).getUrl());
        assertNotNull(result.getCardProducts().get(1).getGrantedCredits());
        assertEquals(2, result.getCardProducts().get(1).getGrantedCredits().size());
        assertNotNull(result.getCardProducts().get(1).getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getCardProducts().get(1).getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getCardProducts().get(1).getGrantedCredits().get(1).getAmount());
        assertNotNull(result.getCardProducts().get(1).getGrantedCredits().get(1).getCurrency());
        assertEquals(1, result.getCardProducts().get(1).getRates().size());
        assertNotNull(result.getCardProducts().get(1).getRates().get(0).getRateType());
        assertNotNull(result.getCardProducts().get(1).getRates().get(0).getRateType().getId());
        assertNotNull(result.getCardProducts().get(1).getRates().get(0).getUnit());
        assertNotNull(result.getCardProducts().get(1).getRates().get(0).getUnit().getId());
        assertNotNull(result.getCardProducts().get(1).getRates().get(0).getUnit().getName());
        assertNotNull(result.getCardProducts().get(1).getRates().get(0).getUnit().getPercentage());
        assertNotNull(result.getCardProducts().get(1).getPriorityLevel());
        assertNotNull(result.getCardProducts().get(1).getBankIdentificationNumber());
        assertNotNull(result.getCardProducts().get(1).getFees());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees());
        assertEquals(2, result.getCardProducts().get(1).getFees().getItemizeFees().size());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees().get(1).getFeeType());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees().get(1).getItemizeFeeUnit());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getCardProducts().get(1).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getCardProducts().get(1).getBenefits());
        assertEquals(1, result.getCardProducts().get(1).getBenefits().size());
        assertNotNull(result.getCardProducts().get(1).getBenefits().get(0).getId());
        assertNotNull(result.getCardProducts().get(1).getBenefits().get(0).getName());
        assertNotNull(result.getCardProducts().get(1).getBenefits().get(0).getDescription());
        assertNotNull(result.getCardProducts().get(1).getGrantedMinimumCredits());
        assertEquals(1, result.getCardProducts().get(1).getGrantedMinimumCredits().size());
        assertNotNull(result.getCardProducts().get(1).getGrantedMinimumCredits().get(0).getAmount());
        assertNotNull(result.getCardProducts().get(1).getGrantedMinimumCredits().get(0).getCurrency());
        assertNotNull(result.getCardProducts().get(1).getLoyaltyProgram());
        assertNotNull(result.getCardProducts().get(1).getLoyaltyProgram().getId());
        assertNotNull(result.getCardProducts().get(1).getBrandAssociation());
        assertNotNull(result.getCardProducts().get(1).getBrandAssociation().getId());
        assertNotNull(result.getCardProducts().get(1).getOfferId());

        assertEquals(input.getDictum().getId(), result.getDictum().getId());
        assertEquals(input.getDictum().getReasons().get(0).getReason().getId(), result.getDictum().getReasons().get(0).getId());
        assertEquals(input.getDictum().getReasons().get(0).getReason().getDescription(), result.getDictum().getReasons().get(0).getDescription());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getId(), result.getCardProducts().get(0).getId());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getName(), result.getCardProducts().get(0).getName());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getSubproduct().getId(), result.getCardProducts().get(0).getSubproduct().getId());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getCardtype().getId(), result.getCardProducts().get(0).getCardType().getId());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getCardtype().getName(), result.getCardProducts().get(0).getCardType().getName());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getImages().get(0).getImage().getId(), result.getCardProducts().get(0).getImages().get(0).getId());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getImages().get(0).getImage().getName(), result.getCardProducts().get(0).getImages().get(0).getName());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getImages().get(0).getImage().getUrl(), result.getCardProducts().get(0).getImages().get(0).getUrl());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getGrantedcredits().get(0).getGrantedcredit().getAmount(), result.getCardProducts().get(0).getGrantedCredits().get(0).getAmount().toString());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getGrantedcredits().get(0).getGrantedcredit().getCurrency(), result.getCardProducts().get(0).getGrantedCredits().get(0).getCurrency());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getGrantedcredits().get(1).getGrantedcredit().getAmount(), result.getCardProducts().get(0).getGrantedCredits().get(1).getAmount().toString());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getGrantedcredits().get(1).getGrantedcredit().getCurrency(), result.getCardProducts().get(0).getGrantedCredits().get(1).getCurrency());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getRates().get(0).getRate().getRatetype().getId(), result.getCardProducts().get(0).getRates().get(0).getRateType().getId());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getRates().get(0).getRate().getUnit().getId(), result.getCardProducts().get(0).getRates().get(0).getUnit().getId());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getRates().get(0).getRate().getUnit().getName(), result.getCardProducts().get(0).getRates().get(0).getUnit().getName());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getRates().get(0).getRate().getUnit().getPercentage(), result.getCardProducts().get(0).getRates().get(0).getUnit().getPercentage().toString());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getPrioritylevel(), new Integer(result.getCardProducts().get(0).getPriorityLevel()));
        assertEquals(input.getCardproducts().get(0).getCardproduct().getBankidentificationnumber(), result.getCardProducts().get(0).getBankIdentificationNumber());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getFees().getItemizefees().get(0).getItemizefee().getFeetype(), result.getCardProducts().get(0).getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getUnittype(), result.getCardProducts().get(0).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getAmount(), result.getCardProducts().get(0).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount().toString());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getCurrency(), result.getCardProducts().get(0).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getFees().getItemizefees().get(1).getItemizefee().getFeetype(), result.getCardProducts().get(0).getFees().getItemizeFees().get(1).getFeeType());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getFees().getItemizefees().get(1).getItemizefee().getItemizefeeunit().getUnittype(), result.getCardProducts().get(0).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getUnitType());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getFees().getItemizefees().get(1).getItemizefee().getItemizefeeunit().getAmount(), result.getCardProducts().get(0).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount().toString());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getFees().getItemizefees().get(1).getItemizefee().getItemizefeeunit().getCurrency(), result.getCardProducts().get(0).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getBenefits().get(0).getBenefit().getId(), result.getCardProducts().get(0).getBenefits().get(0).getId());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getBenefits().get(0).getBenefit().getName(), result.getCardProducts().get(0).getBenefits().get(0).getName());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getBenefits().get(0).getBenefit().getDescription(), result.getCardProducts().get(0).getBenefits().get(0).getDescription());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getGrantedminimumcredits().get(0).getGrantedminimumcredit().getAmount(), result.getCardProducts().get(0).getGrantedMinimumCredits().get(0).getAmount().toString());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getGrantedminimumcredits().get(0).getGrantedminimumcredit().getCurrency(), result.getCardProducts().get(0).getGrantedMinimumCredits().get(0).getCurrency());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getLoyaltyprogram().getId(), result.getCardProducts().get(0).getLoyaltyProgram().getId());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getBrandassociation().getId(), result.getCardProducts().get(0).getBrandAssociation().getId());
        assertEquals(input.getCardproducts().get(0).getCardproduct().getOfferid(), result.getCardProducts().get(0).getOfferId());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getId(), result.getCardProducts().get(1).getId());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getName(), result.getCardProducts().get(1).getName());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getCardtype().getId(), result.getCardProducts().get(1).getCardType().getId());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getCardtype().getName(), result.getCardProducts().get(1).getCardType().getName());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getImages().get(0).getImage().getId(), result.getCardProducts().get(1).getImages().get(0).getId());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getImages().get(0).getImage().getName(), result.getCardProducts().get(1).getImages().get(0).getName());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getImages().get(0).getImage().getUrl(), result.getCardProducts().get(1).getImages().get(0).getUrl());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getGrantedcredits().get(0).getGrantedcredit().getAmount(), result.getCardProducts().get(1).getGrantedCredits().get(0).getAmount().toString());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getGrantedcredits().get(0).getGrantedcredit().getCurrency(), result.getCardProducts().get(1).getGrantedCredits().get(0).getCurrency());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getGrantedcredits().get(1).getGrantedcredit().getAmount(), result.getCardProducts().get(1).getGrantedCredits().get(1).getAmount().toString());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getGrantedcredits().get(1).getGrantedcredit().getCurrency(), result.getCardProducts().get(1).getGrantedCredits().get(1).getCurrency());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getRates().get(0).getRate().getRatetype().getId(), result.getCardProducts().get(1).getRates().get(0).getRateType().getId());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getRates().get(0).getRate().getUnit().getId(), result.getCardProducts().get(1).getRates().get(0).getUnit().getId());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getRates().get(0).getRate().getUnit().getName(), result.getCardProducts().get(1).getRates().get(0).getUnit().getName());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getRates().get(0).getRate().getUnit().getPercentage(), result.getCardProducts().get(1).getRates().get(0).getUnit().getPercentage().toString());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getPrioritylevel(), new Integer(result.getCardProducts().get(1).getPriorityLevel()));
        assertEquals(input.getCardproducts().get(1).getCardproduct().getBankidentificationnumber(), result.getCardProducts().get(1).getBankIdentificationNumber());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getFees().getItemizefees().get(0).getItemizefee().getFeetype(), result.getCardProducts().get(1).getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getUnittype(), result.getCardProducts().get(1).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getAmount(), result.getCardProducts().get(1).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount().toString());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getCurrency(), result.getCardProducts().get(1).getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getFees().getItemizefees().get(1).getItemizefee().getFeetype(), result.getCardProducts().get(1).getFees().getItemizeFees().get(1).getFeeType());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getFees().getItemizefees().get(1).getItemizefee().getItemizefeeunit().getUnittype(), result.getCardProducts().get(1).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getUnitType());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getFees().getItemizefees().get(1).getItemizefee().getItemizefeeunit().getAmount(), result.getCardProducts().get(1).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount().toString());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getFees().getItemizefees().get(1).getItemizefee().getItemizefeeunit().getCurrency(), result.getCardProducts().get(1).getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getBenefits().get(0).getBenefit().getId(), result.getCardProducts().get(1).getBenefits().get(0).getId());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getBenefits().get(0).getBenefit().getName(), result.getCardProducts().get(1).getBenefits().get(0).getName());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getBenefits().get(0).getBenefit().getDescription(), result.getCardProducts().get(1).getBenefits().get(0).getDescription());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getGrantedminimumcredits().get(0).getGrantedminimumcredit().getAmount(), result.getCardProducts().get(1).getGrantedMinimumCredits().get(0).getAmount().toString());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getGrantedminimumcredits().get(0).getGrantedminimumcredit().getCurrency(), result.getCardProducts().get(1).getGrantedMinimumCredits().get(0).getCurrency());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getLoyaltyprogram().getId(), result.getCardProducts().get(1).getLoyaltyProgram().getId());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getBrandassociation().getId(), result.getCardProducts().get(1).getBrandAssociation().getId());
        assertEquals(input.getCardproducts().get(1).getCardproduct().getOfferid(), result.getCardProducts().get(1).getOfferId());
    }

    @Test
    public void mapOutEmptyTest() {
        OfferGenerate result = mapper.mapOut(new RespuestaTransaccionPhiat021_1());

        assertNotNull(result);
        assertNull(result.getDictum());
        assertNull(result.getCardProducts());
    }

    @Test
    public void mapOutEmptyDictumReasonsTest() throws IOException {
        RespuestaTransaccionPhiat021_1 input = Phiat021_1Stubs.getInstance().buildRespuestaTransaccionPhiat021_1();
        input.getDictum().setReasons(null);
        OfferGenerate result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getDictum());
        assertNull(result.getDictum().getReasons());
    }

    @Test
    public void mapOutEmptyCardProductsItemsTest() throws IOException {
        RespuestaTransaccionPhiat021_1 input = Phiat021_1Stubs.getInstance().buildRespuestaTransaccionPhiat021_1();
        input.getCardproducts().get(0).setCardproduct(new Cardproduct());
        input.getCardproducts().get(1).setCardproduct(new Cardproduct());
        OfferGenerate result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getDictum());
        assertNotNull(result.getDictum().getId());
        assertNotNull(result.getDictum().getReasons());
        assertEquals(1, result.getDictum().getReasons().size());
        assertNotNull(result.getDictum().getReasons().get(0).getId());
        assertNotNull(result.getDictum().getReasons().get(0).getDescription());
        assertNotNull(result.getCardProducts());
        assertEquals(2, result.getCardProducts().size());
        assertNull(result.getCardProducts().get(0).getId());
        assertNull(result.getCardProducts().get(0).getName());
        assertNull(result.getCardProducts().get(0).getSubproduct());
        assertNull(result.getCardProducts().get(0).getCardType());
        assertNull(result.getCardProducts().get(0).getImages());
        assertNull(result.getCardProducts().get(0).getGrantedCredits());
        assertNull(result.getCardProducts().get(0).getPriorityLevel());
        assertNull(result.getCardProducts().get(0).getBankIdentificationNumber());
        assertNull(result.getCardProducts().get(0).getFees());
        assertNull(result.getCardProducts().get(0).getBenefits());
        assertNull(result.getCardProducts().get(0).getGrantedMinimumCredits());
        assertNull(result.getCardProducts().get(0).getLoyaltyProgram());
        assertNull(result.getCardProducts().get(0).getBrandAssociation());
        assertNull(result.getCardProducts().get(0).getOfferId());
        assertNull(result.getCardProducts().get(1).getId());
        assertNull(result.getCardProducts().get(1).getName());
        assertNull(result.getCardProducts().get(1).getCardType());
        assertNull(result.getCardProducts().get(1).getImages());
        assertNull(result.getCardProducts().get(1).getGrantedCredits());
        assertNull(result.getCardProducts().get(1).getPriorityLevel());
        assertNull(result.getCardProducts().get(1).getBankIdentificationNumber());
        assertNull(result.getCardProducts().get(1).getFees());
        assertNull(result.getCardProducts().get(1).getBenefits());
        assertNull(result.getCardProducts().get(1).getGrantedMinimumCredits());
        assertNull(result.getCardProducts().get(1).getLoyaltyProgram());
        assertNull(result.getCardProducts().get(1).getBrandAssociation());
        assertNull(result.getCardProducts().get(1).getOfferId());
    }

    @Test
    public void mapOutEmptyFeesItemizeFeesEmptyTest() throws IOException {
        RespuestaTransaccionPhiat021_1 input = Phiat021_1Stubs.getInstance().buildRespuestaTransaccionPhiat021_1();
        input.getCardproducts().get(0).getCardproduct().getFees().setItemizefees(null);
        OfferGenerate result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getCardProducts().get(0).getFees());
        assertNull(result.getCardProducts().get(0).getFees().getItemizeFees());
    }
}
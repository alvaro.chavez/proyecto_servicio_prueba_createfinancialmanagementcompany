package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.business.dto.DTOIntPin;
import com.bbva.pzic.cards.canonic.Pin;
import org.junit.Before;
import org.junit.Test;

import static com.bbva.pzic.cards.EntityMock.CARD_CARDTYPE_ID_KEY_TESTED;
import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static org.junit.Assert.*;

public class ModifyCardPinMapperTest {

    private ModifyCardPinMapper modifyCardPinMapper;

    @Before
    public void setUp() {
        modifyCardPinMapper = new ModifyCardPinMapper();
    }

    @Test
    public void mapInTest() {
        final Pin pin = new Pin();
        pin.setPin("123456789012345678901234567890");
        final DTOIntPin dtoIntPin = modifyCardPinMapper.mapIn(CARD_ID, pin);

        assertNotNull(dtoIntPin);
        assertNotNull(dtoIntPin.getCardId());
        assertNotNull(dtoIntPin.getPin());

        assertEquals(CARD_ID, dtoIntPin.getCardId());
        assertEquals(pin.getPin(), dtoIntPin.getPin());
    }

    @Test
    public void mapInWithoutPinTest() {
        final DTOIntPin dtoIntPin = modifyCardPinMapper.mapIn(CARD_ID, new Pin());

        assertNotNull(dtoIntPin);
        assertNotNull(dtoIntPin.getCardId());
        assertNull(dtoIntPin.getPin());

        assertEquals(CARD_ID, dtoIntPin.getCardId());
    }
}

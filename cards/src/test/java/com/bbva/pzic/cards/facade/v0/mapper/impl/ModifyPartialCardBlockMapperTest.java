package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntBlock;
import com.bbva.pzic.cards.canonic.Block;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import com.bbva.pzic.cards.util.mappers.MapperUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.bbva.pzic.cards.EntityMock.BLOCK_ID;
import static com.bbva.pzic.cards.EntityMock.CARD_ENCRYPT_ID;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ModifyPartialCardBlockMapperTest {

    @InjectMocks
    private ModifyPartialCardBlockMapper mapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Mock
    private Translator translator;

    @Mock
    private MapperUtils mapperUtils;

    private EntityMock mock = EntityMock.getInstance();

    @Before
    public void setUp() {
        Mockito.when(cypherTool.decrypt(CARD_ENCRYPT_ID, AbstractCypherTool.NUMTARJ, RegistryIds.SMC_REGISTRY_ID_OF_MODIFY_PARTIAL_CARD_BLOCK)).thenReturn("1223413412412331");
        Mockito.when(translator.translateFrontendEnumValueStrictly("cards.block.blockId", BLOCK_ID)).thenReturn("CT");
        Mockito.when(mapperUtils.convertBooleanToString(Boolean.TRUE)).thenReturn("S");
    }

    @Test
    public void mapInFullTest() {
        Block block = mock.buildBlock();
        DTOIntBlock result = mapper.mapIn(CARD_ENCRYPT_ID, BLOCK_ID, block);

        assertNotNull(result.getCard().getCardId());
        assertNotNull(result.getBlockId());
        assertNotNull(result.getIsActive());
        assertNotNull(result.getReasonId());

        assertEquals("1223413412412331", result.getCard().getCardId());
        assertEquals("CT", result.getBlockId());
        assertEquals("S", result.getIsActive());
        assertEquals(block.getReason().getId(), result.getReasonId());
    }

    @Test
    public void mapInWithoutisActiveTest() {
        Block block = mock.buildBlock();
        block.setIsActive(null);
        DTOIntBlock result = mapper.mapIn(CARD_ENCRYPT_ID, BLOCK_ID, block);

        assertNotNull(result.getCard().getCardId());
        assertNotNull(result.getBlockId());
        assertNotNull(result.getIsActive());
        assertNotNull(result.getReasonId());

        assertEquals("1223413412412331", result.getCard().getCardId());
        assertEquals("CT", result.getBlockId());
        assertEquals(block.getReason().getId(), result.getReasonId());
        assertEquals("S", result.getIsActive());
    }

    @Test
    public void mapInWithoutReasonIdTest() {
        Block block = mock.buildBlock();
        block.getReason().setId(null);
        DTOIntBlock result = mapper.mapIn(CARD_ENCRYPT_ID, BLOCK_ID, block);

        assertNotNull(result.getCard().getCardId());
        assertNotNull(result.getBlockId());
        assertNotNull(result.getIsActive());
        assertNull(result.getReasonId());

        assertEquals("1223413412412331", result.getCard().getCardId());
        assertEquals("CT", result.getBlockId());
        assertEquals("S", result.getIsActive());
    }
}
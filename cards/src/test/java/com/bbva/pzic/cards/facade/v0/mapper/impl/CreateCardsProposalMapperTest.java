package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntProposal;
import com.bbva.pzic.cards.canonic.Proposal;
import com.bbva.pzic.cards.canonic.ProposalData;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 10/12/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateCardsProposalMapperTest {

    private static final String CONTACT_DETAILS_ID_IN_ENCRYPT = "abcHGvugiGvuyGI";
    private static final String CONTACT_DETAILS_ID_IN_DECRYPT = "CONTACT_DETAIL";

    @InjectMocks
    private CreateCardsProposalMapper mapper;

    @Mock
    private Translator translator;

    @Mock
    private AbstractCypherTool cypherTool;

    @Before
    public void setUp() {
        when(cypherTool.decrypt(CONTACT_DETAILS_ID_IN_ENCRYPT, AbstractCypherTool.CONTACT_DETAIL_ID, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL)).thenReturn(CONTACT_DETAILS_ID_IN_DECRYPT);
    }

    @Test
    public void mapInFullTest() throws IOException {
        Proposal input = EntityMock.getInstance().getProposal();

        when(cypherTool.decrypt(CONTACT_DETAILS_ID_IN_ENCRYPT, AbstractCypherTool.CONTACT_DETAIL_ID, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL)).thenReturn(CONTACT_DETAILS_ID_IN_DECRYPT);

        when(translator.translateFrontendEnumValueStrictly("cards.cardType.id", input.getCardType().getId()))
                .thenReturn("C");
        when(translator.translateFrontendEnumValueStrictly("cards.paymentMethod.id", input.getPaymentMethod().getId()))
                .thenReturn("TAP");
        when(translator.translateFrontendEnumValueStrictly("cards.period.id", input.getPaymentMethod().getPeriod().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.destionation.id", input.getDelivery().getDestination().getId()))
                .thenReturn("H");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.feeType", input.getFees().getItemizeFees().get(0).getFeeType()))
                .thenReturn("PE");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.feeType", input.getFees().getItemizeFees().get(1).getFeeType()))
                .thenReturn("PI");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.mode", input.getFees().getItemizeFees().get(0).getMode().getId()))
                .thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.mode", input.getFees().getItemizeFees().get(1).getMode().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.rates.mode", input.getRates().get(0).getMode().getId()))
                .thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.rates.mode", input.getRates().get(1).getMode().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.offers.additionalProducts.productType", input.getAdditionalProducts().get(0).getProductType()))
                .thenReturn("PR");

        DTOIntProposal result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getSubproduct().getId());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getPeriod().getId());
        assertNotNull(result.getPaymentMethod().getEndDay());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getDelivery().getDestination().getId());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getBranch().getId());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getName());
        assertNotNull(result.getFees().getItemizeFees().get(0).getMode().getId());
        assertNull(result.getFees().getItemizeFees().get(0).getMode().getName());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getFees().getItemizeFees().get(1).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(1).getName());
        assertNotNull(result.getFees().getItemizeFees().get(1).getMode().getId());
        assertNull(result.getFees().getItemizeFees().get(1).getMode().getName());
        assertNotNull(result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getRates().get(0).getRateType().getId());
        assertNotNull(result.getRates().get(0).getCalculationDate());
        assertNotNull(result.getRates().get(0).getModeid());
        assertNotNull(result.getRates().get(0).getUnit().getPercentage());
        assertNotNull(result.getRates().get(1).getRateType().getId());
        assertNotNull(result.getRates().get(1).getCalculationDate());
        assertNotNull(result.getRates().get(1).getModeid());
        assertNotNull(result.getRates().get(1).getUnit().getPercentage());
        assertNotNull(result.getAdditionalProducts().get(0).getProductType());
        assertNotNull(result.getAdditionalProducts().get(0).getAmount());
        assertNotNull(result.getAdditionalProducts().get(0).getCurrency());
        assertNotNull(result.getMembership().getId());
        assertNotNull(result.getMembership().getNumber());
        assertNotNull(result.getContactDetailsId0());
        assertNotNull(result.getContactDetailsId1());
        assertNotNull(result.getOfferId());
        assertNotNull(result.getNotificationsByOperation());

        assertEquals("C", result.getCardType().getId());
        assertEquals(input.getTitle().getId(), result.getTitle().getId());
        assertEquals(input.getTitle().getSubproduct().getId(), result.getTitle().getSubproduct().getId());
        assertEquals("TAP", result.getPaymentMethod().getId());
        assertEquals("M", result.getPaymentMethod().getPeriod().getId());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getPaymentMethod().getEndDay());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getGrantedCredits().get(0).getCurrency());
        assertEquals("H", result.getDelivery().getDestination().getId());
        assertEquals(input.getBank().getId(), result.getBank().getId());
        assertEquals(input.getBank().getBranch().getId(), result.getBank().getBranch().getId());
        assertEquals("PE", result.getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(input.getFees().getItemizeFees().get(0).getName(), result.getFees().getItemizeFees().get(0).getName());
        assertEquals("P", result.getFees().getItemizeFees().get(0).getMode().getId());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertEquals("PI", result.getFees().getItemizeFees().get(1).getFeeType());
        assertEquals(input.getFees().getItemizeFees().get(1).getName(), result.getFees().getItemizeFees().get(1).getName());
        assertEquals("M", result.getFees().getItemizeFees().get(1).getMode().getId());
        assertEquals(input.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount(), result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertEquals(input.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency(), result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getRates().get(0).getRateType().getId(), result.getRates().get(0).getRateType().getId());
        assertEquals(input.getRates().get(1).getRateType().getId(), result.getRates().get(1).getRateType().getId());
        assertEquals(input.getRates().get(0).getCalculationDate(), result.getRates().get(0).getCalculationDate());
        assertEquals("P", result.getRates().get(0).getModeid());
        assertEquals(input.getRates().get(0).getUnit().getPercentage(), result.getRates().get(0).getUnit().getPercentage());
        assertEquals("PR", result.getAdditionalProducts().get(0).getProductType());
        assertEquals(input.getAdditionalProducts().get(0).getAmount(), result.getAdditionalProducts().get(0).getAmount());
        assertEquals(input.getAdditionalProducts().get(0).getCurrency(), result.getAdditionalProducts().get(0).getCurrency());
        assertEquals(input.getMembership().getId(), result.getMembership().getId());
        assertEquals(input.getMembership().getNumber(), result.getMembership().getNumber());
        assertEquals(CONTACT_DETAILS_ID_IN_DECRYPT, result.getContactDetailsId0());
        assertEquals(CONTACT_DETAILS_ID_IN_DECRYPT, result.getContactDetailsId1());
        assertEquals(input.getOfferId(), result.getOfferId());
        assertEquals(input.getNotificationsByOperation(), result.getNotificationsByOperation());
    }


    @Test
    public void mapInWithOutCardTypeTest() throws IOException {
        Proposal input = EntityMock.getInstance().getProposal();
        input.setCardType(null);

        when(translator.translateFrontendEnumValueStrictly("cards.paymentMethod.id", input.getPaymentMethod().getId()))
                .thenReturn("TAP");
        when(translator.translateFrontendEnumValueStrictly("cards.period.id", input.getPaymentMethod().getPeriod().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.destionation.id", input.getDelivery().getDestination().getId()))
                .thenReturn("H");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.feeType", input.getFees().getItemizeFees().get(0).getFeeType()))
                .thenReturn("PE");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.feeType", input.getFees().getItemizeFees().get(1).getFeeType()))
                .thenReturn("PI");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.mode", input.getFees().getItemizeFees().get(0).getMode().getId()))
                .thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.mode", input.getFees().getItemizeFees().get(1).getMode().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.rates.mode", input.getRates().get(0).getMode().getId()))
                .thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.offers.additionalProducts.productType", input.getAdditionalProducts().get(0).getProductType()))
                .thenReturn("PR");

        DTOIntProposal result = mapper.mapIn(input);
        assertNotNull(result);
        assertNull(result.getCardType());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getSubproduct().getId());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getPeriod().getId());
        assertNotNull(result.getPaymentMethod().getEndDay());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getDelivery().getDestination().getId());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getBranch().getId());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getName());
        assertNotNull(result.getFees().getItemizeFees().get(0).getMode().getId());
        assertNull(result.getFees().getItemizeFees().get(0).getMode().getName());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getFees().getItemizeFees().get(1).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(1).getName());
        assertNotNull(result.getFees().getItemizeFees().get(1).getMode().getId());
        assertNull(result.getFees().getItemizeFees().get(1).getMode().getName());
        assertNotNull(result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getRates().get(0).getRateType().getId());
        assertNotNull(result.getRates().get(1).getRateType().getId());
        assertNotNull(result.getRates().get(0).getCalculationDate());
        assertNotNull(result.getRates().get(0).getModeid());
        assertNotNull(result.getRates().get(0).getUnit().getPercentage());
        assertNotNull(result.getAdditionalProducts().get(0).getProductType());
        assertNotNull(result.getAdditionalProducts().get(0).getAmount());
        assertNotNull(result.getAdditionalProducts().get(0).getCurrency());
        assertNotNull(result.getMembership().getId());
        assertNotNull(result.getMembership().getNumber());
        assertNotNull(result.getContactDetailsId0());
        assertNotNull(result.getContactDetailsId1());
        assertNotNull(result.getOfferId());
        assertNotNull(result.getNotificationsByOperation());

        assertEquals(input.getTitle().getId(), result.getTitle().getId());
        assertEquals(input.getTitle().getSubproduct().getId(), result.getTitle().getSubproduct().getId());
        assertEquals("TAP", result.getPaymentMethod().getId());
        assertEquals("M", result.getPaymentMethod().getPeriod().getId());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getPaymentMethod().getEndDay());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getGrantedCredits().get(0).getCurrency());
        assertEquals("H", result.getDelivery().getDestination().getId());
        assertEquals(input.getBank().getId(), result.getBank().getId());
        assertEquals(input.getBank().getBranch().getId(), result.getBank().getBranch().getId());
        assertEquals("PE", result.getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(input.getFees().getItemizeFees().get(0).getName(), result.getFees().getItemizeFees().get(0).getName());
        assertEquals("P", result.getFees().getItemizeFees().get(0).getMode().getId());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertEquals("PI", result.getFees().getItemizeFees().get(1).getFeeType());
        assertEquals(input.getFees().getItemizeFees().get(1).getName(), result.getFees().getItemizeFees().get(1).getName());
        assertEquals("M", result.getFees().getItemizeFees().get(1).getMode().getId());
        assertEquals(input.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount(), result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertEquals(input.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency(), result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getRates().get(0).getRateType().getId(), result.getRates().get(0).getRateType().getId());
        assertEquals(input.getRates().get(1).getRateType().getId(), result.getRates().get(1).getRateType().getId());
        assertEquals(input.getRates().get(0).getCalculationDate(), result.getRates().get(0).getCalculationDate());
        assertEquals("P", result.getRates().get(0).getModeid());
        assertEquals(input.getRates().get(0).getUnit().getPercentage(), result.getRates().get(0).getUnit().getPercentage());
        assertEquals("PR", result.getAdditionalProducts().get(0).getProductType());
        assertEquals(input.getAdditionalProducts().get(0).getAmount(), result.getAdditionalProducts().get(0).getAmount());
        assertEquals(input.getAdditionalProducts().get(0).getCurrency(), result.getAdditionalProducts().get(0).getCurrency());
        assertEquals(input.getMembership().getId(), result.getMembership().getId());
        assertEquals(input.getMembership().getNumber(), result.getMembership().getNumber());
        assertEquals(CONTACT_DETAILS_ID_IN_DECRYPT, result.getContactDetailsId0());
        assertEquals(CONTACT_DETAILS_ID_IN_DECRYPT, result.getContactDetailsId1());
        assertEquals(input.getOfferId(), result.getOfferId());
        assertEquals(input.getNotificationsByOperation(), result.getNotificationsByOperation());
    }


    @Test
    public void mapInWithOutFeesTest() throws IOException {
        Proposal input = EntityMock.getInstance().getProposal();
        input.setFees(null);

        when(translator.translateFrontendEnumValueStrictly("cards.cardType.id", input.getCardType().getId()))
                .thenReturn("C");
        when(translator.translateFrontendEnumValueStrictly("cards.paymentMethod.id", input.getPaymentMethod().getId()))
                .thenReturn("TAP");
        when(translator.translateFrontendEnumValueStrictly("cards.period.id", input.getPaymentMethod().getPeriod().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.destionation.id", input.getDelivery().getDestination().getId()))
                .thenReturn("H");
        when(translator.translateFrontendEnumValueStrictly("cards.rates.mode", input.getRates().get(0).getMode().getId()))
                .thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.offers.additionalProducts.productType", input.getAdditionalProducts().get(0).getProductType()))
                .thenReturn("PR");

        DTOIntProposal result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getSubproduct().getId());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getPeriod().getId());
        assertNotNull(result.getPaymentMethod().getEndDay());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getDelivery().getDestination().getId());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getBranch().getId());
        assertNull(result.getFees());
        assertNotNull(result.getRates().get(0).getRateType().getId());
        assertNotNull(result.getRates().get(1).getRateType().getId());
        assertNotNull(result.getRates().get(0).getCalculationDate());
        assertNotNull(result.getRates().get(0).getModeid());
        assertNotNull(result.getRates().get(0).getUnit().getPercentage());
        assertNotNull(result.getAdditionalProducts().get(0).getProductType());
        assertNotNull(result.getAdditionalProducts().get(0).getAmount());
        assertNotNull(result.getAdditionalProducts().get(0).getCurrency());
        assertNotNull(result.getMembership().getId());
        assertNotNull(result.getMembership().getNumber());
        assertNotNull(result.getContactDetailsId0());
        assertNotNull(result.getContactDetailsId1());
        assertNotNull(result.getOfferId());
        assertNotNull(result.getNotificationsByOperation());

        assertEquals("C", result.getCardType().getId());
        assertEquals(input.getTitle().getId(), result.getTitle().getId());
        assertEquals(input.getTitle().getSubproduct().getId(), result.getTitle().getSubproduct().getId());
        assertEquals("TAP", result.getPaymentMethod().getId());
        assertEquals("M", result.getPaymentMethod().getPeriod().getId());
        assertEquals(input.getPaymentMethod().getEndDay(), result.getPaymentMethod().getEndDay());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getGrantedCredits().get(0).getCurrency());
        assertEquals("H", result.getDelivery().getDestination().getId());
        assertEquals(input.getBank().getId(), result.getBank().getId());
        assertEquals(input.getBank().getBranch().getId(), result.getBank().getBranch().getId());
        assertEquals(input.getRates().get(0).getRateType().getId(), result.getRates().get(0).getRateType().getId());
        assertEquals(input.getRates().get(1).getRateType().getId(), result.getRates().get(1).getRateType().getId());
        assertEquals(input.getRates().get(0).getCalculationDate(), result.getRates().get(0).getCalculationDate());
        assertEquals("P", result.getRates().get(0).getModeid());
        assertEquals(input.getRates().get(0).getUnit().getPercentage(), result.getRates().get(0).getUnit().getPercentage());
        assertEquals("PR", result.getAdditionalProducts().get(0).getProductType());
        assertEquals(input.getAdditionalProducts().get(0).getAmount(), result.getAdditionalProducts().get(0).getAmount());
        assertEquals(input.getAdditionalProducts().get(0).getCurrency(), result.getAdditionalProducts().get(0).getCurrency());
        assertEquals(input.getMembership().getId(), result.getMembership().getId());
        assertEquals(input.getMembership().getNumber(), result.getMembership().getNumber());
        assertEquals(CONTACT_DETAILS_ID_IN_DECRYPT, result.getContactDetailsId0());
        assertEquals(CONTACT_DETAILS_ID_IN_DECRYPT, result.getContactDetailsId1());
        assertEquals(input.getOfferId(), result.getOfferId());
        assertEquals(input.getNotificationsByOperation(), result.getNotificationsByOperation());
    }

    @Test
    public void mapInWithoutContactDetailsTest() throws IOException {
        Proposal input = EntityMock.getInstance().getProposal();
        input.setContactDetails(null);

        when(translator.translateFrontendEnumValueStrictly("cards.cardType.id", input.getCardType().getId()))
                .thenReturn("C");
        when(translator.translateFrontendEnumValueStrictly("cards.paymentMethod.id", input.getPaymentMethod().getId()))
                .thenReturn("TAP");
        when(translator.translateFrontendEnumValueStrictly("cards.period.id", input.getPaymentMethod().getPeriod().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.destionation.id", input.getDelivery().getDestination().getId()))
                .thenReturn("H");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.feeType", input.getFees().getItemizeFees().get(0).getFeeType()))
                .thenReturn("PE");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.feeType", input.getFees().getItemizeFees().get(1).getFeeType()))
                .thenReturn("PI");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.mode", input.getFees().getItemizeFees().get(0).getMode().getId()))
                .thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.mode", input.getFees().getItemizeFees().get(1).getMode().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.rates.mode", input.getRates().get(0).getMode().getId()))
                .thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.rates.mode", input.getRates().get(1).getMode().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.offers.additionalProducts.productType", input.getAdditionalProducts().get(0).getProductType()))
                .thenReturn("PR");

        DTOIntProposal result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getSubproduct().getId());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getPeriod().getId());
        assertNotNull(result.getPaymentMethod().getEndDay());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getDelivery().getDestination().getId());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getBranch().getId());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getName());
        assertNotNull(result.getFees().getItemizeFees().get(0).getMode().getId());
        assertNull(result.getFees().getItemizeFees().get(0).getMode().getName());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getFees().getItemizeFees().get(1).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(1).getName());
        assertNotNull(result.getFees().getItemizeFees().get(1).getMode().getId());
        assertNull(result.getFees().getItemizeFees().get(1).getMode().getName());
        assertNotNull(result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getRates().get(0).getRateType().getId());
        assertNotNull(result.getRates().get(0).getCalculationDate());
        assertNotNull(result.getRates().get(0).getModeid());
        assertNotNull(result.getRates().get(0).getUnit().getPercentage());
        assertNotNull(result.getRates().get(1).getRateType().getId());
        assertNotNull(result.getRates().get(1).getCalculationDate());
        assertNotNull(result.getRates().get(1).getModeid());
        assertNotNull(result.getRates().get(1).getUnit().getPercentage());
        assertNotNull(result.getAdditionalProducts().get(0).getProductType());
        assertNotNull(result.getAdditionalProducts().get(0).getAmount());
        assertNotNull(result.getAdditionalProducts().get(0).getCurrency());
        assertNotNull(result.getMembership().getId());
        assertNotNull(result.getMembership().getNumber());
        assertNull(result.getContactDetailsId0());
        assertNull(result.getContactDetailsId1());
        assertNotNull(result.getOfferId());
        assertNotNull(result.getNotificationsByOperation());
    }

    @Test
    public void mapInContactDetailsWithOneItemTest() throws IOException {
        Proposal input = EntityMock.getInstance().getProposal();
        input.getContactDetails().remove(1);

        when(cypherTool.decrypt(CONTACT_DETAILS_ID_IN_ENCRYPT, AbstractCypherTool.CONTACT_DETAIL_ID, RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_PROPOSAL)).thenReturn(CONTACT_DETAILS_ID_IN_DECRYPT);

        when(translator.translateFrontendEnumValueStrictly("cards.cardType.id", input.getCardType().getId()))
                .thenReturn("C");
        when(translator.translateFrontendEnumValueStrictly("cards.paymentMethod.id", input.getPaymentMethod().getId()))
                .thenReturn("TAP");
        when(translator.translateFrontendEnumValueStrictly("cards.period.id", input.getPaymentMethod().getPeriod().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.delivery.destionation.id", input.getDelivery().getDestination().getId()))
                .thenReturn("H");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.feeType", input.getFees().getItemizeFees().get(0).getFeeType()))
                .thenReturn("PE");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.feeType", input.getFees().getItemizeFees().get(1).getFeeType()))
                .thenReturn("PI");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.mode", input.getFees().getItemizeFees().get(0).getMode().getId()))
                .thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.itemizeFees.mode", input.getFees().getItemizeFees().get(1).getMode().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.rates.mode", input.getRates().get(0).getMode().getId()))
                .thenReturn("P");
        when(translator.translateFrontendEnumValueStrictly("cards.rates.mode", input.getRates().get(1).getMode().getId()))
                .thenReturn("M");
        when(translator.translateFrontendEnumValueStrictly("cards.offers.additionalProducts.productType", input.getAdditionalProducts().get(0).getProductType()))
                .thenReturn("PR");

        DTOIntProposal result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getTitle().getId());
        assertNotNull(result.getTitle().getSubproduct().getId());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getPeriod().getId());
        assertNotNull(result.getPaymentMethod().getEndDay());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getDelivery().getDestination().getId());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getBranch().getId());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getName());
        assertNotNull(result.getFees().getItemizeFees().get(0).getMode().getId());
        assertNull(result.getFees().getItemizeFees().get(0).getMode().getName());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getFees().getItemizeFees().get(1).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(1).getName());
        assertNotNull(result.getFees().getItemizeFees().get(1).getMode().getId());
        assertNull(result.getFees().getItemizeFees().get(1).getMode().getName());
        assertNotNull(result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(1).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getRates().get(0).getRateType().getId());
        assertNotNull(result.getRates().get(0).getCalculationDate());
        assertNotNull(result.getRates().get(0).getModeid());
        assertNotNull(result.getRates().get(0).getUnit().getPercentage());
        assertNotNull(result.getRates().get(1).getRateType().getId());
        assertNotNull(result.getRates().get(1).getCalculationDate());
        assertNotNull(result.getRates().get(1).getModeid());
        assertNotNull(result.getRates().get(1).getUnit().getPercentage());
        assertNotNull(result.getAdditionalProducts().get(0).getProductType());
        assertNotNull(result.getAdditionalProducts().get(0).getAmount());
        assertNotNull(result.getAdditionalProducts().get(0).getCurrency());
        assertNotNull(result.getMembership().getId());
        assertNotNull(result.getMembership().getNumber());
        assertNotNull(result.getContactDetailsId0());
        assertNull(result.getContactDetailsId1());
        assertNotNull(result.getOfferId());
        assertNotNull(result.getNotificationsByOperation());
    }

    @Test
    public void mapInEmptyTest() {
        DTOIntProposal result = mapper.mapIn(new Proposal());
        assertNotNull(result);
        assertNull(result.getCardType());
        assertNull(result.getTitle());
        assertNull(result.getPaymentMethod());
        assertNull(result.getGrantedCredits());
        assertNull(result.getDelivery());
        assertNull(result.getBank());
        assertNull(result.getFees());
        assertNull(result.getRates());
        assertNull(result.getAdditionalProducts());
        assertNull(result.getMembership());
        assertNull(result.getContactDetailsId0());
        assertNull(result.getContactDetailsId1());
        assertNull(result.getOfferId());
        assertNull(result.getNotificationsByOperation());
    }

    @Test
    public void mapOutFullTest() {
        ProposalData result = mapper.mapOut(new Proposal());
        assertNotNull(result);
    }

    @Test
    public void mapOutEmptyTest() {
        ProposalData result = mapper.mapOut(null);
        assertNull(result);
    }
}

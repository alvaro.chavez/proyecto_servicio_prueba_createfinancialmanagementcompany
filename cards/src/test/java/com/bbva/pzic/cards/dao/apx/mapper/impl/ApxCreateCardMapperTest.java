package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntCard;
import com.bbva.pzic.cards.dao.apx.mapper.IApxCreateCardMapper;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Entityin;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.Entityout;
import com.bbva.pzic.cards.dao.model.ppcutc01_1.mock.FormatsPpcutc01_1Stubs;
import com.bbva.pzic.cards.facade.v1.dto.CardPost;
import com.bbva.pzic.cards.util.FunctionUtils;
import com.bbva.pzic.utilTest.DateUtils;
import org.junit.Test;

import java.io.IOException;

import static com.bbva.pzic.cards.util.Constants.DEFAULT_TIME;
import static org.junit.Assert.*;

/**
 * Created on 07/03/2020.
 *
 * @author Entelgy
 */
public class ApxCreateCardMapperTest {

    private final IApxCreateCardMapper mapper = new ApxCreateCardMapper();

    @Test
    public void mapInFullServiceTypeId004Test() throws IOException {
        EntityMock stubs = EntityMock.getInstance();
        DTOIntCard input = stubs.buildDTOIntCard();
        input.getDeliveries().get(0).setDestination(null);
        input.getDeliveries().get(0).setAddress(null);
        input.getDeliveries().get(1).setDestination(null);
        input.getDeliveries().get(1).setAddress(null);
        input.getDeliveries().get(2).setDestination(null);
        input.getDeliveries().get(2).setAddress(null);

        Entityin result = mapper.mapIn(input);

        commonsInputAsserts(input, result);

        assertNotNull(result.getDeliveries().get(0).getDelivery().getContact().getContacttype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getContact().getId());
        assertNull(result.getDeliveries().get(0).getDelivery().getDestination());
        assertNull(result.getDeliveries().get(0).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(1).getDelivery().getContact().getContacttype());
        assertNotNull(result.getDeliveries().get(1).getDelivery().getContact().getId());
        assertNull(result.getDeliveries().get(1).getDelivery().getDestination());
        assertNull(result.getDeliveries().get(1).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(2).getDelivery().getContact().getContacttype());
        assertNotNull(result.getDeliveries().get(2).getDelivery().getContact().getId());
        assertNull(result.getDeliveries().get(2).getDelivery().getDestination());
        assertNull(result.getDeliveries().get(2).getDelivery().getAddress());

        assertEquals(input.getDeliveries().get(0).getContact().getContactType(), result.getDeliveries().get(0).getDelivery().getContact().getContacttype());
        assertEquals(input.getDeliveries().get(0).getContact().getId(), result.getDeliveries().get(0).getDelivery().getContact().getId());
        assertEquals(input.getDeliveries().get(1).getContact().getContactType(), result.getDeliveries().get(1).getDelivery().getContact().getContacttype());
        assertEquals(input.getDeliveries().get(1).getContact().getId(), result.getDeliveries().get(1).getDelivery().getContact().getId());
        assertEquals(input.getDeliveries().get(2).getContact().getContactType(), result.getDeliveries().get(2).getDelivery().getContact().getContacttype());
        assertEquals(input.getDeliveries().get(2).getContact().getId(), result.getDeliveries().get(2).getDelivery().getContact().getId());
    }

    @Test
    public void mapInFullServiceTypeIdOtherTest() throws IOException {
        EntityMock stubs = EntityMock.getInstance();
        DTOIntCard input = stubs.buildDTOIntCard();
        input.getDeliveries().get(0).setServiceTypeId("001");
        input.getDeliveries().get(0).setContact(null);
        input.getDeliveries().get(1).setServiceTypeId("002");
        input.getDeliveries().get(1).setContact(null);
        input.getDeliveries().get(2).setServiceTypeId("003");
        input.getDeliveries().get(2).setContact(null);
        input.getDeliveries().get(2).setAddress(null);

        Entityin result = mapper.mapIn(input);

        commonsInputAsserts(input, result);

        // 0 - HOME
        assertNull(result.getDeliveries().get(0).getDelivery().getContact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch());

        assertEquals(input.getDeliveries().get(0).getDestination().getId(), result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertEquals(input.getDeliveries().get(0).getAddress().getAddressType(), result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertEquals(input.getDeliveries().get(0).getAddress().getId(), result.getDeliveries().get(0).getDelivery().getAddress().getId());

        // 1 - CUSTOM
        assertNull(result.getDeliveries().get(1).getDelivery().getContact());
        assertNotNull(result.getDeliveries().get(1).getDelivery().getDestination().getId());
        assertNotNull(result.getDeliveries().get(1).getDelivery().getAddress().getAddresstype());
        assertNotNull(result.getDeliveries().get(1).getDelivery().getAddress().getId());
        assertNull(result.getDeliveries().get(1).getDelivery().getDestination().getBranch());

        assertEquals(input.getDeliveries().get(1).getDestination().getId(), result.getDeliveries().get(1).getDelivery().getDestination().getId());
        assertEquals(input.getDeliveries().get(1).getAddress().getAddressType(), result.getDeliveries().get(1).getDelivery().getAddress().getAddresstype());
        assertEquals(input.getDeliveries().get(1).getAddress().getId(), result.getDeliveries().get(1).getDelivery().getAddress().getId());

        // 2 - BRANCH
        assertNull(result.getDeliveries().get(2).getDelivery().getContact());
        assertNotNull(result.getDeliveries().get(2).getDelivery().getDestination().getId());
        assertNull(result.getDeliveries().get(2).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(2).getDelivery().getDestination().getBranch().getId());

        assertEquals(input.getDeliveries().get(2).getDestination().getId(), result.getDeliveries().get(2).getDelivery().getDestination().getId());
        assertEquals(input.getDeliveries().get(2).getDestination().getBranch().getId(), result.getDeliveries().get(2).getDelivery().getDestination().getBranch().getId());
    }

    private void commonsInputAsserts(final DTOIntCard input, final Entityin result) {
        assertNotNull(result.getCardtype().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getPhysicalsupport().getId());
        assertNotNull(result.getHoldername());
        assertNotNull(result.getCurrencies().get(0).getCurrency().getCurrency());
        assertNotNull(result.getCurrencies().get(0).getCurrency().getIsmajor());
        assertNotNull(result.getGrantedcredits().get(0).getGrantedcredit().getAmount());
        assertNotNull(result.getGrantedcredits().get(0).getGrantedcredit().getCurrency());
        assertNotNull(result.getCutoffday());
        assertNotNull(result.getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedcontracts().get(0).getRelatedcontract().getProduct().getId());
        assertNotNull(result.getRelatedcontracts().get(0).getRelatedcontract().getProduct().getProducttype().getId());
        assertNotNull(result.getImages().get(0).getImage().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertNotNull(result.getDeliveries().get(1).getDelivery().getServicetype().getId());
        assertNotNull(result.getDeliveries().get(2).getDelivery().getServicetype().getId());
        assertNotNull(result.getParticipants().get(0).getParticipant().getPersontype());
        assertNotNull(result.getParticipants().get(0).getParticipant().getId());
        assertNotNull(result.getParticipants().get(0).getParticipant().getParticipanttype().getId());
        assertNotNull(result.getParticipants().get(0).getParticipant().getLegalpersontype().getId());
        assertNotNull(result.getParticipants().get(1).getParticipant().getPersontype());
        assertNotNull(result.getParticipants().get(1).getParticipant().getId());
        assertNotNull(result.getParticipants().get(1).getParticipant().getParticipanttype().getId());
        assertNotNull(result.getParticipants().get(1).getParticipant().getLegalpersontype().getId());
        assertNotNull(result.getPaymentmethod().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getDay());
        assertNotNull(result.getSupportcontracttype());
        assertNotNull(result.getMemberships().get(0).getMembership().getNumber());
        assertNotNull(result.getCardagreement());
        assertNotNull(result.getContractingbranch().getId());
        assertNotNull(result.getManagementbranch().getId());
        assertNotNull(result.getContractingbusinessagent().getId());
        assertNotNull(result.getMarketbusinessagent().getId());
        assertNotNull(result.getBankidentificationnumber());

        assertEquals(input.getCardTypeId(), result.getCardtype().getId());
        assertEquals(input.getProductId(), result.getProduct().getId());
        assertEquals(input.getPhysicalSupportId(), result.getPhysicalsupport().getId());
        assertEquals(input.getHolderName(), result.getHoldername());
        assertEquals(input.getCurrenciesCurrency(), result.getCurrencies().get(0).getCurrency().getCurrency());
        assertEquals(input.getCurrenciesIsMajor(), result.getCurrencies().get(0).getCurrency().getIsmajor());
        assertEquals(input.getGrantedCreditsAmount(), result.getGrantedcredits().get(0).getGrantedcredit().getAmount());
        assertEquals(input.getGrantedCreditsCurrency(), result.getGrantedcredits().get(0).getGrantedcredit().getCurrency());
        assertEquals(input.getCutOffDay().toString(), result.getCutoffday());
        assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertEquals(input.getRelatedContracts().get(0).getRelationType().getId(), result.getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct().getId(), result.getRelatedcontracts().get(0).getRelatedcontract().getProduct().getId());
        assertEquals(input.getRelatedContracts().get(0).getProduct().getProductType().getId(), result.getRelatedcontracts().get(0).getRelatedcontract().getProduct().getProducttype().getId());
        assertEquals(input.getImages().get(0).getId(), result.getImages().get(0).getImage().getId());
        assertEquals(input.getDeliveries().get(0).getServiceTypeId(), result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertEquals(input.getDeliveries().get(1).getServiceTypeId(), result.getDeliveries().get(1).getDelivery().getServicetype().getId());
        assertEquals(input.getDeliveries().get(2).getServiceTypeId(), result.getDeliveries().get(2).getDelivery().getServicetype().getId());
        assertEquals(input.getParticipants().get(0).getPersonType(), result.getParticipants().get(0).getParticipant().getPersontype());
        assertEquals(input.getParticipants().get(0).getId(), result.getParticipants().get(0).getParticipant().getId());
        assertEquals(input.getParticipants().get(0).getParticipantTypeId(), result.getParticipants().get(0).getParticipant().getParticipanttype().getId());
        assertEquals(input.getParticipants().get(0).getLegalPersonTypeId(), result.getParticipants().get(0).getParticipant().getLegalpersontype().getId());
        assertEquals(input.getParticipants().get(1).getPersonType(), result.getParticipants().get(1).getParticipant().getPersontype());
        assertEquals(input.getParticipants().get(1).getId(), result.getParticipants().get(1).getParticipant().getId());
        assertEquals(input.getParticipants().get(1).getParticipantTypeId(), result.getParticipants().get(1).getParticipant().getParticipanttype().getId());
        assertEquals(input.getParticipants().get(1).getLegalPersonTypeId(), result.getParticipants().get(1).getParticipant().getLegalpersontype().getId());
        assertEquals(input.getPaymentMethod().getId(), result.getPaymentmethod().getId());
        assertEquals(input.getPaymentMethod().getFrequency().getId(), result.getPaymentmethod().getFrecuency().getId());
        assertEquals(input.getPaymentMethod().getFrequency().getDaysOfMonth().getDay(), result.getPaymentmethod().getFrecuency().getDaysofmonth().getDay());
        assertEquals(input.getSupportContractType(), result.getSupportcontracttype());
        assertEquals(input.getMembershipsNumber(), result.getMemberships().get(0).getMembership().getNumber());
        assertEquals(input.getCardAgreement(), result.getCardagreement());
        assertEquals(input.getContractingBranchId(), result.getContractingbranch().getId());
        assertEquals(input.getManagementBranchId(), result.getManagementbranch().getId());
        assertEquals(input.getContractingBusinessAgentId(), result.getContractingbusinessagent().getId());
        assertEquals(input.getMarketBusinessAgentId(), result.getMarketbusinessagent().getId());
        assertEquals(input.getBankIdentificationNumber(), result.getBankidentificationnumber());
    }

    @Test
    public void mapInEmptyTest() {
        Entityin result = mapper.mapIn(new DTOIntCard());

        assertNull(result.getCardtype());
        assertNull(result.getProduct());
        assertNull(result.getPhysicalsupport());
        assertNull(result.getHoldername());
        assertNull(result.getCurrencies());
        assertNull(result.getGrantedcredits());
        assertNull(result.getCutoffday());
        assertNull(result.getRelatedcontracts());
        assertNull(result.getImages());
        assertNull(result.getDeliveries());
        assertNull(result.getParticipants());
        assertNull(result.getPaymentmethod());
        assertNull(result.getSupportcontracttype());
        assertNull(result.getMemberships());
        assertNull(result.getCardagreement());
        assertNull(result.getContractingbranch());
        assertNull(result.getManagementbranch());
        assertNull(result.getContractingbusinessagent());
        assertNull(result.getMarketbusinessagent());
        assertNull(result.getBankidentificationnumber());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        Entityout entityOut = FormatsPpcutc01_1Stubs.getInstance().getEntityOut();

        CardPost result = mapper.mapOut(entityOut);

        commonsOutputAsserts(entityOut, result);

        // 0 - HOME :: STORED
        assertNotNull(result.getDeliveries().get(0).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(0).getContact().getId());
        assertNotNull(result.getDeliveries().get(0).getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getId());

        assertEquals(entityOut.getDeliveries().get(0).getDelivery().getDestination().getId(), result.getDeliveries().get(0).getDestination().getId());
        assertEquals(entityOut.getDeliveries().get(0).getDelivery().getAddress().getAddresstype(), result.getDeliveries().get(0).getAddress().getAddressType());
        assertEquals(entityOut.getDeliveries().get(0).getDelivery().getAddress().getId(), result.getDeliveries().get(0).getAddress().getId());
        assertEquals(entityOut.getCardagreement() + entityOut.getDeliveries().get(0).getDelivery().getAddress().getId(),
                result.getDeliveries().get(0).getId());

        // 1 - CUSTOM :: SPECIFIC
        assertNotNull(result.getDeliveries().get(1).getContact().getContactType());
        assertNull(result.getDeliveries().get(1).getContact().getId());
        assertNotNull(result.getDeliveries().get(1).getDestination().getId());
        assertNotNull(result.getDeliveries().get(1).getAddress().getAddressType());
        assertNull(result.getDeliveries().get(1).getAddress().getId());
        assertNull(result.getDeliveries().get(1).getId());

        assertEquals(entityOut.getDeliveries().get(1).getDelivery().getDestination().getId(), result.getDeliveries().get(1).getDestination().getId());
        assertEquals(entityOut.getDeliveries().get(1).getDelivery().getAddress().getAddresstype(), result.getDeliveries().get(1).getAddress().getAddressType());

        // 2 - BRANCH
        assertNotNull(result.getDeliveries().get(2).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(2).getContact().getId());
        assertNotNull(result.getDeliveries().get(2).getDestination().getId());
        assertNull(result.getDeliveries().get(2).getAddress());
        assertNotNull(result.getDeliveries().get(2).getDestination().getBranch().getId());
        assertNotNull(result.getDeliveries().get(2).getId());

        assertEquals(entityOut.getDeliveries().get(2).getDelivery().getDestination().getId(), result.getDeliveries().get(2).getDestination().getId());
        assertEquals(entityOut.getDeliveries().get(2).getDelivery().getDestination().getBranch().getId(), result.getDeliveries().get(2).getDestination().getBranch().getId());
        assertEquals(entityOut.getCardagreement() + entityOut.getDeliveries().get(2).getDelivery().getDestination().getBranch().getId(),
                result.getDeliveries().get(2).getId());
    }

    private void commonsOutputAsserts(final Entityout entityOut, final CardPost result) {
        assertNotNull(result.getId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getNumberType());
        assertNotNull(result.getNumberType().getId());
        assertNotNull(result.getNumberType().getDescription());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getCardType().getDescription());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getBrandAssociation());
        assertNotNull(result.getBrandAssociation().getId());
        assertNotNull(result.getBrandAssociation().getDescription());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getPhysicalSupport().getDescription());
        assertNotNull(result.getExpirationDate());
        assertNotNull(result.getHolderName());
        assertNotNull(result.getCurrencies());
        assertNotNull(result.getCurrencies().get(0).getCurrency());
        assertNotNull(result.getCurrencies().get(0).getIsMajor());
        assertNotNull(result.getGrantedCredits());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertNull(result.getImages());
        assertNotNull(result.getCutOffDay());
        assertNotNull(result.getOpeningDate());
        assertNotNull(result.getRelatedContracts().get(0));
        assertNotNull(result.getRelatedContracts().get(0).getId());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType());
        assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertNotNull(result.getDeliveries());
        assertNotNull(result.getDeliveries().get(0).getServiceType());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNotNull(result.getSupportContractType());
        assertNotNull(result.getMemberships().get(0).getId());
        assertNotNull(result.getMemberships().get(0).getNumber());
        assertNotNull(result.getCardAgreement());
        assertNotNull(result.getContractingBranch().getId());
        assertNotNull(result.getManagementBranch().getId());
        assertNotNull(result.getContractingBusinessAgent().getId());
        assertNotNull(result.getMarketBusinessAgent().getId());
        assertNotNull(result.getBankIdentificationNumber());

        assertEquals(entityOut.getId(), result.getId());
        assertEquals(entityOut.getNumber(), result.getNumber());
        assertEquals(entityOut.getNumbertype().getId(), result.getNumberType().getId());
        assertEquals(entityOut.getNumbertype().getDescription(), result.getNumberType().getDescription());
        assertEquals(entityOut.getCardtype().getId(), result.getCardType().getId());
        assertEquals(entityOut.getCardtype().getDescription(), result.getCardType().getDescription());
        assertEquals(entityOut.getProduct().getId(), result.getProduct().getId());
        assertEquals(entityOut.getProduct().getName(), result.getProduct().getName());
        assertEquals(entityOut.getBrandassociation().getId(), result.getBrandAssociation().getId());
        assertEquals(entityOut.getBrandassociation().getDescription(), result.getBrandAssociation().getDescription());
        assertEquals(entityOut.getPhysicalsupport().getId(), result.getPhysicalSupport().getId());
        assertEquals(entityOut.getPhysicalsupport().getDescription(), result.getPhysicalSupport().getDescription());
        assertEquals(entityOut.getExpirationdate(), DateUtils.getFormatDefaultHost(result.getExpirationDate()));
        assertEquals(entityOut.getHoldername(), result.getHolderName());
        assertEquals(entityOut.getCurrencies().get(0).getCurrency().getCurrency(), result.getCurrencies().get(0).getCurrency());
        assertEquals(entityOut.getCurrencies().get(0).getCurrency().getIsmajor(), result.getCurrencies().get(0).getIsMajor());
        assertEquals(entityOut.getGrantedcredits().get(0).getGrantedcredit().getAmount(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(entityOut.getGrantedcredits().get(0).getGrantedcredit().getCurrency(), result.getGrantedCredits().get(0).getCurrency());
        assertEquals(entityOut.getStatus().getId(), result.getStatus().getId());
        assertEquals(entityOut.getStatus().getDescription(), result.getStatus().getDescription());
        assertEquals(new Integer(entityOut.getCutoffday()), result.getCutOffDay());
        assertEquals(entityOut.getRelatedcontracts().get(0).getRelatedcontract().getId(), result.getRelatedContracts().get(0).getId());
        assertEquals(entityOut.getRelatedcontracts().get(0).getRelatedcontract().getContractid(), result.getRelatedContracts().get(0).getContractId());
        assertEquals(entityOut.getRelatedcontracts().get(0).getRelatedcontract().getProduct().getId(), result.getRelatedContracts().get(0).getProduct().getId());
        assertEquals(entityOut.getRelatedcontracts().get(0).getRelatedcontract().getProduct().getProducttype().getId(), result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        assertEquals(entityOut.getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId(), result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(entityOut.getDeliveries().get(0).getDelivery().getServicetype().getId(), result.getDeliveries().get(0).getServiceType().getId());
        assertEquals(entityOut.getSupportcontracttype(), result.getSupportContractType());
        assertEquals(entityOut.getMemberships().get(0).getMembership().getId(), result.getMemberships().get(0).getId());
        assertEquals(entityOut.getMemberships().get(0).getMembership().getNumber(), result.getMemberships().get(0).getNumber());
        assertEquals(entityOut.getCardagreement(), result.getCardAgreement());
        assertEquals(entityOut.getContractingbranch().getId(), result.getContractingBranch().getId());
        assertEquals(entityOut.getManagementbranch().getId(), result.getManagementBranch().getId());
        assertEquals(entityOut.getContractingbusinessagent().getId(), result.getContractingBusinessAgent().getId());
        assertEquals(entityOut.getMarketbusinessagent().getId(), result.getMarketBusinessAgent().getId());
        assertEquals(entityOut.getBankidentificationnumber(), result.getBankIdentificationNumber());
    }

    @Test
    public void mapOutEmptyTest() {
        CardPost result = mapper.mapOut(new Entityout());

        assertNull(result.getId());
        assertNull(result.getNumber());
        assertNull(result.getNumberType());
        assertNull(result.getCardType());
        assertNull(result.getProduct());
        assertNull(result.getBrandAssociation());
        assertNull(result.getPhysicalSupport());
        assertNull(result.getExpirationDate());
        assertNull(result.getHolderName());
        assertNull(result.getCurrencies());
        assertNull(result.getGrantedCredits());
        assertNull(result.getStatus());
        assertNull(result.getImages());
        assertNull(result.getCutOffDay());
        assertNull(result.getOpeningDate());
        assertNull(result.getRelatedContracts());
        assertNull(result.getDeliveries());
        assertNull(result.getSupportContractType());
        assertNull(result.getMemberships());
        assertNull(result.getCardAgreement());
        assertNull(result.getContractingBranch());
        assertNull(result.getManagementBranch());
        assertNull(result.getContractingBusinessAgent());
        assertNull(result.getMarketBusinessAgent());
        assertNull(result.getBankIdentificationNumber());
    }
}

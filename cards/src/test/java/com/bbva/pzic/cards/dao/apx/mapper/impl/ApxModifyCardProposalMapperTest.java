package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntProposal;
import com.bbva.pzic.cards.business.dto.InputModifyCardProposal;
import com.bbva.pzic.cards.dao.apx.mapper.IApxModifyCardProposalMapper;
import com.bbva.pzic.cards.dao.model.ppcut003_1.PeticionTransaccionPpcut003_1;
import com.bbva.pzic.cards.dao.model.ppcut003_1.RespuestaTransaccionPpcut003_1;
import com.bbva.pzic.cards.dao.model.ppcut003_1.mock.Ppcut003_1Stubs;
import com.bbva.pzic.cards.facade.v1.dto.Proposal;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.*;

public class ApxModifyCardProposalMapperTest {

    private IApxModifyCardProposalMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxModifyCardProposalMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputModifyCardProposal input = EntityMock.getInstance().buildInputModifyCardProposal();
        PeticionTransaccionPpcut003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContacttype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getContactdetailtype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch().getId());
        assertNotNull(result.getPaymentmethod());
        assertNotNull(result.getPaymentmethod().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency());
        assertNotNull(result.getPaymentmethod().getFrecuency().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getDay());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getCutoffday());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduletimes());
        assertNotNull(result.getContactability().getScheduletimes().getEndtime());
        assertNotNull(result.getContactability().getScheduletimes().getStarttime());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizerates());
        assertEquals(1, result.getRates().getItemizerates().size());
        assertNotNull(result.getRates().getItemizerates().get(0).getItemizerate().getRatetype());
        assertNotNull(result.getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit());
        assertNotNull(result.getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getUnitratetype());
        assertNotNull(result.getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizefees());
        assertEquals(1, result.getFees().getItemizefees().size());
        assertNotNull(result.getFees().getItemizefees().get(0).getItemizefee().getFeetype());
        assertNotNull(result.getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit());
        assertNotNull(result.getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getUnittype());
        assertNotNull(result.getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getAmount());
        assertNotNull(result.getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getCurrency());
        assertNotNull(result.getGrantedcredits());
        assertEquals(1, result.getGrantedcredits().size());
        assertNotNull(result.getGrantedcredits().get(0).getGrantedcredit().getAmount());
        assertNotNull(result.getGrantedcredits().get(0).getGrantedcredit().getCurrency());
        assertNotNull(result.getOfferid());
        assertNotNull(result.getNotificationsbyoperation());

        assertEquals(input.getProposalId(), result.getProposalId());
        assertEquals(input.getProposal().getDeliveries().get(0).getServiceTypeId(), result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertEquals(input.getProposal().getDeliveries().get(0).getContact().getContactType(), result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContacttype());
        assertEquals(input.getProposal().getDeliveries().get(0).getContact().getContactDetailType(), result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getContactdetailtype());
        assertEquals(input.getProposal().getDeliveries().get(0).getContact().getContactAddress(), result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getAddress());
        assertEquals(input.getProposal().getDeliveries().get(0).getAddress().getAddressType(), result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertEquals(input.getProposal().getDeliveries().get(0).getAddress().getId(), result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertEquals(input.getProposal().getDeliveries().get(0).getDestination().getId(), result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertEquals(input.getProposal().getDeliveries().get(0).getDestination().getBranch().getId(), result.getDeliveries().get(0).getDelivery().getDestination().getBranch().getId());
        assertEquals(input.getProposal().getPaymentMethod().getId(), result.getPaymentmethod().getId());
        assertEquals(input.getProposal().getPaymentMethod().getFrequency().getId(), result.getPaymentmethod().getFrecuency().getId());
        assertEquals(input.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth().getDay(), result.getPaymentmethod().getFrecuency().getDaysofmonth().getDay());
        assertEquals(input.getProposal().getPaymentMethod().getFrequency().getDaysOfMonth().getCutOffDay(), result.getPaymentmethod().getFrecuency().getDaysofmonth().getCutoffday());
        assertEquals(input.getProposal().getContactability().getReason(), result.getContactability().getReason());
        assertEquals(input.getProposal().getContactability().getScheduletimes().getEndtime(), result.getContactability().getScheduletimes().getEndtime());
        assertEquals(input.getProposal().getContactability().getScheduletimes().getStarttime(), result.getContactability().getScheduletimes().getStarttime());
        assertEquals(input.getProposal().getProduct().getId(), result.getProduct().getId());
        assertEquals(input.getProposal().getRate().getItemizeRates().get(0).getRateType(), result.getRates().getItemizerates().get(0).getItemizerate().getRatetype());
        assertEquals(input.getProposal().getRate().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType(), result.getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getUnitratetype());
        assertEquals(input.getProposal().getRate().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage(), new BigDecimal(result.getRates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getPercentage()));
        assertEquals(input.getProposal().getFees().getItemizeFees().get(0).getFeeType(), result.getFees().getItemizefees().get(0).getItemizefee().getFeetype());
        assertEquals(input.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType(), result.getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getUnittype());
        assertEquals(input.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount(), new BigDecimal(result.getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getAmount()));
        assertEquals(input.getProposal().getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency(), result.getFees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getCurrency());
        assertEquals(input.getProposal().getGrantedCredits().get(0).getAmount(), new BigDecimal(result.getGrantedcredits().get(0).getGrantedcredit().getAmount()));
        assertEquals(input.getProposal().getGrantedCredits().get(0).getCurrency(), result.getGrantedcredits().get(0).getGrantedcredit().getCurrency());
        assertEquals(input.getProposal().getOfferId(), result.getOfferid());
        assertEquals(input.getProposal().getNotificationsByOperation(), result.getNotificationsbyoperation().toString());
    }

    @Test
    public void mapInWithoutDeliveryServiceTypeIdTest() throws IOException {
        InputModifyCardProposal input = EntityMock.getInstance().buildInputModifyCardProposal();
        input.getProposal().getDeliveries().get(0).setServiceTypeId(null);
        PeticionTransaccionPpcut003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNull(result.getDeliveries().get(0).getDelivery().getServicetype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContacttype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getContactdetailtype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch().getId());
        assertNotNull(result.getPaymentmethod());
        assertNotNull(result.getPaymentmethod().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency());
        assertNotNull(result.getPaymentmethod().getFrecuency().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getDay());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getCutoffday());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduletimes());
        assertNotNull(result.getContactability().getScheduletimes().getEndtime());
        assertNotNull(result.getContactability().getScheduletimes().getStarttime());
        assertNotNull(result.getNotificationsbyoperation());
    }

    @Test
    public void mapInWithoutDeliveryContactTest() throws IOException {
        InputModifyCardProposal input = EntityMock.getInstance().buildInputModifyCardProposal();
        input.getProposal().getDeliveries().get(0).setContact(null);
        PeticionTransaccionPpcut003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch().getId());
        assertNotNull(result.getPaymentmethod());
        assertNotNull(result.getPaymentmethod().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency());
        assertNotNull(result.getPaymentmethod().getFrecuency().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getDay());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getCutoffday());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduletimes());
        assertNotNull(result.getContactability().getScheduletimes().getEndtime());
        assertNotNull(result.getContactability().getScheduletimes().getStarttime());
        assertNotNull(result.getNotificationsbyoperation());
    }

    @Test
    public void mapInWithoutDeliveryContactContactTest() throws IOException {
        InputModifyCardProposal input = EntityMock.getInstance().buildInputModifyCardProposal();
        input.getProposal().getDeliveries().get(0).getContact().setContactDetailType(null);
        input.getProposal().getDeliveries().get(0).getContact().setContactAddress(null);
        PeticionTransaccionPpcut003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContacttype());
        assertNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch().getId());
        assertNotNull(result.getPaymentmethod());
        assertNotNull(result.getPaymentmethod().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency());
        assertNotNull(result.getPaymentmethod().getFrecuency().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getDay());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getCutoffday());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduletimes());
        assertNotNull(result.getContactability().getScheduletimes().getEndtime());
        assertNotNull(result.getContactability().getScheduletimes().getStarttime());
        assertNotNull(result.getNotificationsbyoperation());
    }

    @Test
    public void mapInWithoutDeliveryAddressTest() throws IOException {
        InputModifyCardProposal input = EntityMock.getInstance().buildInputModifyCardProposal();
        input.getProposal().getDeliveries().get(0).setAddress(null);
        PeticionTransaccionPpcut003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContacttype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getContactdetailtype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getAddress());
        assertNull(result.getDeliveries().get(0).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch().getId());
        assertNotNull(result.getPaymentmethod());
        assertNotNull(result.getPaymentmethod().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency());
        assertNotNull(result.getPaymentmethod().getFrecuency().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getDay());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getCutoffday());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduletimes());
        assertNotNull(result.getContactability().getScheduletimes().getEndtime());
        assertNotNull(result.getContactability().getScheduletimes().getStarttime());
        assertNotNull(result.getNotificationsbyoperation());
    }

    @Test
    public void mapInWithoutDeliveryDestinationBranchTest() throws IOException {
        InputModifyCardProposal input = EntityMock.getInstance().buildInputModifyCardProposal();
        input.getProposal().getDeliveries().get(0).getDestination().setBranch(null);
        PeticionTransaccionPpcut003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContacttype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getContactdetailtype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch());
        assertNotNull(result.getPaymentmethod());
        assertNotNull(result.getPaymentmethod().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency());
        assertNotNull(result.getPaymentmethod().getFrecuency().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getDay());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getCutoffday());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduletimes());
        assertNotNull(result.getContactability().getScheduletimes().getEndtime());
        assertNotNull(result.getContactability().getScheduletimes().getStarttime());
        assertNotNull(result.getNotificationsbyoperation());
    }

    @Test
    public void mapInWithoutPaymentMethodFrecuencyTest() throws IOException {
        InputModifyCardProposal input = EntityMock.getInstance().buildInputModifyCardProposal();
        input.getProposal().getPaymentMethod().setFrequency(null);
        PeticionTransaccionPpcut003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContacttype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getContactdetailtype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch().getId());
        assertNotNull(result.getPaymentmethod());
        assertNotNull(result.getPaymentmethod().getId());
        assertNull(result.getPaymentmethod().getFrecuency());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduletimes());
        assertNotNull(result.getContactability().getScheduletimes().getEndtime());
        assertNotNull(result.getContactability().getScheduletimes().getStarttime());
        assertNotNull(result.getNotificationsbyoperation());
    }

    @Test
    public void mapInWithoutPaymentMethodFrecuencyDaysOfMonthTest() throws IOException {
        InputModifyCardProposal input = EntityMock.getInstance().buildInputModifyCardProposal();
        input.getProposal().getPaymentMethod().getFrequency().setDaysOfMonth(null);
        PeticionTransaccionPpcut003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContacttype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getContactdetailtype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch().getId());
        assertNotNull(result.getPaymentmethod());
        assertNotNull(result.getPaymentmethod().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency());
        assertNotNull(result.getPaymentmethod().getFrecuency().getId());
        assertNull(result.getPaymentmethod().getFrecuency().getDaysofmonth());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduletimes());
        assertNotNull(result.getContactability().getScheduletimes().getEndtime());
        assertNotNull(result.getContactability().getScheduletimes().getStarttime());
        assertNotNull(result.getNotificationsbyoperation());
    }

    @Test
    public void mapInWithoutContactabilityScheduleTimesTest() throws IOException {
        InputModifyCardProposal input = EntityMock.getInstance().buildInputModifyCardProposal();
        input.getProposal().getContactability().setScheduletimes(null);
        PeticionTransaccionPpcut003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getServicetype().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContacttype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getContactdetailtype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getSpecificcontact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getAddresstype());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDelivery().getDestination().getBranch().getId());
        assertNotNull(result.getPaymentmethod());
        assertNotNull(result.getPaymentmethod().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency());
        assertNotNull(result.getPaymentmethod().getFrecuency().getId());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getDay());
        assertNotNull(result.getPaymentmethod().getFrecuency().getDaysofmonth().getCutoffday());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNull(result.getContactability().getScheduletimes());
        assertNotNull(result.getNotificationsbyoperation());
    }

    @Test
    public void mapInProposalEmptyTest() throws IOException {
        InputModifyCardProposal input = EntityMock.getInstance().buildInputModifyCardProposal();
        input.setProposal(new DTOIntProposal());
        PeticionTransaccionPpcut003_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getProposalId());
        assertNull(result.getDeliveries());
        assertNull(result.getPaymentmethod());
        assertNull(result.getContactability());
        assertNull(result.getProduct());
        assertNull(result.getFees());
        assertNull(result.getRates());
        assertNull(result.getGrantedcredits());
        assertNull(result.getOfferid());
        assertNull(result.getNotificationsbyoperation());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionPpcut003_1 input = Ppcut003_1Stubs.getInstance().getProposal();
        Proposal result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getDescription());
        assertNotNull(result.getDeliveries().get(0).getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getId());
        assertNotNull(result.getDeliveries().get(0).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getContactDetailType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getName());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getName());
        assertNotNull(result.getPaymentMethod());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency());
        assertNotNull(result.getPaymentMethod().getFrecuency().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDescription());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getCutOffDay());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduleTimes());
        assertNotNull(result.getContactability().getScheduleTimes().getStartTime());
        assertNotNull(result.getContactability().getScheduleTimes().getEndTime());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getDescription());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getDescription());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getOfferId());
        assertNotNull(result.getNotificationsByOperation());

        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getId(), result.getDeliveries().get(0).getId());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getServicetype().getId(), result.getDeliveries().get(0).getServiceType().getId());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getServicetype().getDescription(), result.getDeliveries().get(0).getServiceType().getDescription());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getSpecificcontact().getId(), result.getDeliveries().get(0).getContact().getId());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getSpecificcontact().getContacttype(), result.getDeliveries().get(0).getContact().getContactType());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getSpecificcontact().getContact().getContactdetailtype(), result.getDeliveries().get(0).getContact().getContact().getContactDetailType());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getSpecificcontact().getContact().getAddress(), result.getDeliveries().get(0).getContact().getContact().getAddress());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getAddress().getId(), result.getDeliveries().get(0).getAddress().getId());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getAddress().getAddresstype(), result.getDeliveries().get(0).getAddress().getAddressType());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getDestination().getId(), result.getDeliveries().get(0).getDestination().getId());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getDestination().getName(), result.getDeliveries().get(0).getDestination().getName());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getDestination().getBranch().getId(), result.getDeliveries().get(0).getDestination().getBranch().getId());
        assertEquals(input.getCampo_1_deliveries().get(0).getDelivery().getDestination().getBranch().getName(), result.getDeliveries().get(0).getDestination().getBranch().getName());
        assertEquals(input.getCampo_2_paymentmethod().getId(), result.getPaymentMethod().getId());
        assertEquals(input.getCampo_2_paymentmethod().getFrecuency().getId(), result.getPaymentMethod().getFrecuency().getId());
        assertEquals(input.getCampo_2_paymentmethod().getFrecuency().getDescription(), result.getPaymentMethod().getFrecuency().getDescription());
        assertEquals(input.getCampo_2_paymentmethod().getFrecuency().getDaysofmonth().getCutoffday(), result.getPaymentMethod().getFrecuency().getDaysOfMonth().getCutOffDay());
        assertEquals(input.getCampo_2_paymentmethod().getFrecuency().getDaysofmonth().getDay(), result.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay());
        assertEquals(input.getCampo_3_contactability().getReason(), result.getContactability().getReason());
        assertEquals(input.getCampo_8_product().getId(), result.getProduct().getId());
        assertEquals(input.getCampo_8_product().getName(), result.getProduct().getName());
        assertEquals(input.getCampo_4_grantedcredits().get(0).getGrantedcredit().getCurrency(), result.getGrantedCredits().get(0).getCurrency());
        assertEquals(input.getCampo_4_grantedcredits().get(0).getGrantedcredit().getAmount(), result.getGrantedCredits().get(0).getAmount().toString());
        assertEquals(input.getCampo_5_rates().getItemizerates().get(0).getItemizerate().getRatetype(), result.getRates().getItemizeRates().get(0).getRateType());
        assertEquals(input.getCampo_5_rates().getItemizerates().get(0).getItemizerate().getDescription(), result.getRates().getItemizeRates().get(0).getDescription());
        assertEquals(input.getCampo_5_rates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getUnitratetype(), result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertEquals(input.getCampo_5_rates().getItemizerates().get(0).getItemizerate().getItemizeratesunit().getPercentage(), result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage().toBigInteger().toString());
        assertEquals(input.getCampo_6_fees().getItemizefees().get(0).getItemizefee().getFeetype(), result.getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(input.getCampo_6_fees().getItemizefees().get(0).getItemizefee().getDescription(), result.getFees().getItemizeFees().get(0).getDescription());
        assertEquals(input.getCampo_6_fees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getUnittype(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertEquals(input.getCampo_6_fees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getAmount(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount().toBigInteger().toString());
        assertEquals(input.getCampo_6_fees().getItemizefees().get(0).getItemizefee().getItemizefeeunit().getCurrency(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getCampo_7_offerid(), result.getOfferId());
        assertEquals(input.getCampo_9_notificationsbyoperation().toString(), result.getNotificationsByOperation());
    }

    @Test
    public void mapOutWithoutDeliveryContactTest() throws IOException {
        RespuestaTransaccionPpcut003_1 input = Ppcut003_1Stubs.getInstance().getProposal();
        input.getCampo_1_deliveries().get(0).getDelivery().setSpecificcontact(null);
        Proposal result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getDescription());
        assertNull(result.getDeliveries().get(0).getContact());
        assertNotNull(result.getDeliveries().get(0).getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(0).getDestination());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getName());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getName());
        assertNotNull(result.getPaymentMethod());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency());
        assertNotNull(result.getPaymentMethod().getFrecuency().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDescription());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getCutOffDay());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduleTimes());
        assertNotNull(result.getContactability().getScheduleTimes().getStartTime());
        assertNotNull(result.getContactability().getScheduleTimes().getEndTime());
        assertNotNull(result.getNotificationsByOperation());
    }

    @Test
    public void mapOutWithoutDeliveryAddressTest() throws IOException {
        RespuestaTransaccionPpcut003_1 input = Ppcut003_1Stubs.getInstance().getProposal();
        input.getCampo_1_deliveries().get(0).getDelivery().setAddress(null);
        Proposal result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getDescription());
        assertNotNull(result.getDeliveries().get(0).getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getId());
        assertNotNull(result.getDeliveries().get(0).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getContactDetailType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getAddress());
        assertNull(result.getDeliveries().get(0).getAddress());
        assertNotNull(result.getDeliveries().get(0).getDestination());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getName());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getName());
        assertNotNull(result.getPaymentMethod());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency());
        assertNotNull(result.getPaymentMethod().getFrecuency().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDescription());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getCutOffDay());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduleTimes());
        assertNotNull(result.getContactability().getScheduleTimes().getStartTime());
        assertNotNull(result.getContactability().getScheduleTimes().getEndTime());
        assertNotNull(result.getNotificationsByOperation());
    }

    @Test
    public void mapOutWithoutDeliveryDestinationBranchTest() throws IOException {
        RespuestaTransaccionPpcut003_1 input = Ppcut003_1Stubs.getInstance().getProposal();
        input.getCampo_1_deliveries().get(0).getDelivery().getDestination().setBranch(null);
        Proposal result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getDescription());
        assertNotNull(result.getDeliveries().get(0).getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getId());
        assertNotNull(result.getDeliveries().get(0).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getContactDetailType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getName());
        assertNull(result.getDeliveries().get(0).getDestination().getBranch());
        assertNotNull(result.getPaymentMethod());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency());
        assertNotNull(result.getPaymentMethod().getFrecuency().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDescription());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getCutOffDay());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduleTimes());
        assertNotNull(result.getContactability().getScheduleTimes().getStartTime());
        assertNotNull(result.getContactability().getScheduleTimes().getEndTime());
        assertNotNull(result.getNotificationsByOperation());
    }

    @Test
    public void mapOutWithoutPaymentMethodFrecuencyTest() throws IOException {
        RespuestaTransaccionPpcut003_1 input = Ppcut003_1Stubs.getInstance().getProposal();
        input.getCampo_2_paymentmethod().setFrecuency(null);
        Proposal result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getDescription());
        assertNotNull(result.getDeliveries().get(0).getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getId());
        assertNotNull(result.getDeliveries().get(0).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getContactDetailType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getName());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getName());
        assertNotNull(result.getPaymentMethod());
        assertNotNull(result.getPaymentMethod().getId());
        assertNull(result.getPaymentMethod().getFrecuency());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduleTimes());
        assertNotNull(result.getContactability().getScheduleTimes().getStartTime());
        assertNotNull(result.getContactability().getScheduleTimes().getEndTime());
        assertNotNull(result.getNotificationsByOperation());
    }

    @Test
    public void mapOutWithoutPaymentMethodFrecuencyDaysOfMonthTest() throws IOException {
        RespuestaTransaccionPpcut003_1 input = Ppcut003_1Stubs.getInstance().getProposal();
        input.getCampo_2_paymentmethod().getFrecuency().setDaysofmonth(null);
        Proposal result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getDescription());
        assertNotNull(result.getDeliveries().get(0).getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getId());
        assertNotNull(result.getDeliveries().get(0).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getContactDetailType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getName());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getName());
        assertNotNull(result.getPaymentMethod());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency());
        assertNotNull(result.getPaymentMethod().getFrecuency().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDescription());
        assertNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNotNull(result.getContactability().getScheduleTimes());
        assertNotNull(result.getContactability().getScheduleTimes().getStartTime());
        assertNotNull(result.getContactability().getScheduleTimes().getEndTime());
        assertNotNull(result.getNotificationsByOperation());
    }

    @Test
    public void mapOutWithoutContactabilityScheduleTimesTest() throws IOException {
        RespuestaTransaccionPpcut003_1 input = Ppcut003_1Stubs.getInstance().getProposal();
        input.getCampo_3_contactability().setScheduletimes(null);
        Proposal result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getDeliveries());
        assertEquals(1, result.getDeliveries().size());
        assertNotNull(result.getDeliveries().get(0).getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getId());
        assertNotNull(result.getDeliveries().get(0).getServiceType().getDescription());
        assertNotNull(result.getDeliveries().get(0).getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getId());
        assertNotNull(result.getDeliveries().get(0).getContact().getContactType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getContactDetailType());
        assertNotNull(result.getDeliveries().get(0).getContact().getContact().getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getAddress().getAddressType());
        assertNotNull(result.getDeliveries().get(0).getAddress().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination());
        assertNotNull(result.getDeliveries().get(0).getDestination().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getName());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getId());
        assertNotNull(result.getDeliveries().get(0).getDestination().getBranch().getName());
        assertNotNull(result.getPaymentMethod());
        assertNotNull(result.getPaymentMethod().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency());
        assertNotNull(result.getPaymentMethod().getFrecuency().getId());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDescription());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getCutOffDay());
        assertNotNull(result.getPaymentMethod().getFrecuency().getDaysOfMonth().getDay());
        assertNotNull(result.getContactability());
        assertNotNull(result.getContactability().getReason());
        assertNull(result.getContactability().getScheduleTimes());
        assertNotNull(result.getNotificationsByOperation());
    }

    @Test
    public void mapOutEmptyTest() {
        Proposal result = mapper.mapOut(new RespuestaTransaccionPpcut003_1());

        assertNotNull(result);
        assertNull(result.getContact());
        assertNull(result.getDeliveries());
        assertNull(result.getPaymentMethod());
        assertNull(result.getContactability());
        assertNull(result.getProduct());
        assertNull(result.getFees());
        assertNull(result.getRates());
        assertNull(result.getGrantedCredits());
        assertNull(result.getOfferId());
        assertNull(result.getNotificationsByOperation());
    }
}

package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardsCardProposal;
import com.bbva.pzic.cards.canonic.CardProposal;
import com.bbva.pzic.cards.canonic.CardProposalData;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.io.IOException;

import static com.bbva.pzic.cards.facade.RegistryIds.SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL;

/**
 * Created on 24/12/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateCardsCardProposalMapperTest {

    @InjectMocks
    private CreateCardsCardProposalMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Mock
    private AbstractCypherTool cypherTool;

    @Test
    public void mapInFullTest() throws IOException {
        CardProposal input = EntityMock.getInstance().getCardProposal();

        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.CARDID, SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL))
                .thenReturn(EntityMock.CARD_ID);
        Mockito.when(enumMapper.getBackendValue("cards.cardType.id", EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_ENUM))
                .thenReturn(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.identityDocument.documentType.id", EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.delivery.destination.id", EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_ENUM))
                .thenReturn(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND);

        InputCreateCardsCardProposal result = mapper.mapIn(EntityMock.CARD_ID_ENCRYPTED, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getCardProposal().getTitle().getId());
        Assert.assertNotNull(result.getCardProposal().getCardType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getDocumentType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getFirstName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMiddleName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getSecondLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getProfession().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMaritalStatus().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getAmount());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getCurrency());
        Assert.assertNotNull(result.getCardProposal().getDelivery()
                .getDestination().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getBranch()
                .getId());
        Assert.assertNotNull(result.getCardProposal().getOfferId());

        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());
        Assert.assertEquals(input.getTitle().getId(), result.getCardProposal()
                .getTitle().getId());
        Assert.assertEquals(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND, result
                .getCardProposal().getCardType().getId());
        Assert.assertEquals(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getIdentityDocument().getDocumentType()
                .getId());
        Assert.assertEquals(input.getParticipant().getIdentityDocument()
                .getNumber(), result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertEquals(input.getParticipant().getFirstName(), result
                .getCardProposal().getParticipant().getFirstName());
        Assert.assertEquals(input.getParticipant().getMiddleName(), result
                .getCardProposal().getParticipant().getMiddleName());
        Assert.assertEquals(input.getParticipant().getLastName(), result
                .getCardProposal().getParticipant().getLastName());
        Assert.assertEquals(input.getParticipant().getSecondLastName(), result
                .getCardProposal().getParticipant().getSecondLastName());
        Assert.assertEquals(input.getParticipant().getProfession().getId(),
                result.getCardProposal().getParticipant().getProfession()
                        .getId());
        Assert.assertEquals(input.getParticipant().getMaritalStatus().getId(),
                result.getCardProposal().getParticipant().getMaritalStatus()
                        .getId());
        Assert.assertEquals(input.getParticipant().getRelationType().getId(), result
                .getCardProposal().getParticipant().getRelationType().getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(0)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertEquals(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(0).getContactType()
                .getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(1)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertEquals(EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(1).getContactType()
                .getId());
        Assert.assertEquals(input.getGrantedCredits().get(0).getAmount(),
                result.getCardProposal().getGrantedCredits().get(0).getAmount());
        Assert.assertEquals(input.getGrantedCredits().get(0).getCurrency(),
                result.getCardProposal().getGrantedCredits().get(0)
                        .getCurrency());
        Assert.assertEquals(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND,
                result.getCardProposal().getDelivery().getDestination().getId());
        Assert.assertEquals(input.getBank().getId(), result.getCardProposal()
                .getBank().getId());
        Assert.assertEquals(input.getBank().getBranch().getId(), result
                .getCardProposal().getBank().getBranch().getId());
        Assert.assertEquals(input.getOfferId(), result.getCardProposal()
                .getOfferId());
    }

    @Test
    public void mapInWithoutCardTypeTest() throws IOException {
        CardProposal input = EntityMock.getInstance().getCardProposal();
        input.setCardType(null);
        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.CARDID, SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL))
                .thenReturn(EntityMock.CARD_ID);
        Mockito.when(enumMapper.getBackendValue("cards.identityDocument.documentType.id", EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.delivery.destination.id", EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_ENUM))
                .thenReturn(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND);

        InputCreateCardsCardProposal result = mapper.mapIn(EntityMock.CARD_ID_ENCRYPTED,
                input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getCardProposal().getTitle().getId());
        Assert.assertNull(result.getCardProposal().getCardType());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getDocumentType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getFirstName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMiddleName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getSecondLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getProfession().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMaritalStatus().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getAmount());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getCurrency());
        Assert.assertNotNull(result.getCardProposal().getDelivery()
                .getDestination().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getBranch()
                .getId());
        Assert.assertNotNull(result.getCardProposal().getOfferId());

        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());
        Assert.assertEquals(input.getTitle().getId(), result.getCardProposal()
                .getTitle().getId());

        Assert.assertEquals(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getIdentityDocument().getDocumentType()
                .getId());
        Assert.assertEquals(input.getParticipant().getIdentityDocument()
                .getNumber(), result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertEquals(input.getParticipant().getFirstName(), result
                .getCardProposal().getParticipant().getFirstName());
        Assert.assertEquals(input.getParticipant().getMiddleName(), result
                .getCardProposal().getParticipant().getMiddleName());
        Assert.assertEquals(input.getParticipant().getLastName(), result
                .getCardProposal().getParticipant().getLastName());
        Assert.assertEquals(input.getParticipant().getSecondLastName(), result
                .getCardProposal().getParticipant().getSecondLastName());
        Assert.assertEquals(input.getParticipant().getProfession().getId(),
                result.getCardProposal().getParticipant().getProfession()
                        .getId());
        Assert.assertEquals(input.getParticipant().getMaritalStatus().getId(),
                result.getCardProposal().getParticipant().getMaritalStatus()
                        .getId());
        Assert.assertEquals(input.getParticipant().getRelationType().getId(), result
                .getCardProposal().getParticipant().getRelationType().getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(0)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertEquals(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(0).getContactType()
                .getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(1)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertEquals(EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(1).getContactType()
                .getId());
        Assert.assertEquals(input.getGrantedCredits().get(0).getAmount(),
                result.getCardProposal().getGrantedCredits().get(0).getAmount());
        Assert.assertEquals(input.getGrantedCredits().get(0).getCurrency(),
                result.getCardProposal().getGrantedCredits().get(0)
                        .getCurrency());
        Assert.assertEquals(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND,
                result.getCardProposal().getDelivery().getDestination().getId());
        Assert.assertEquals(input.getBank().getId(), result.getCardProposal()
                .getBank().getId());
        Assert.assertEquals(input.getBank().getBranch().getId(), result
                .getCardProposal().getBank().getBranch().getId());
        Assert.assertEquals(input.getOfferId(), result.getCardProposal()
                .getOfferId());
    }

    @Test
    public void mapInWithoutParticipantTest() throws IOException {
        CardProposal input = EntityMock.getInstance().getCardProposal();
        input.setParticipant(null);
        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.CARDID, SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL))
                .thenReturn(EntityMock.CARD_ID);
        Mockito.when(enumMapper.getBackendValue("cards.cardType.id", EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_ENUM))
                .thenReturn(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.delivery.destination.id", EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_ENUM))
                .thenReturn(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND);

        InputCreateCardsCardProposal result = mapper.mapIn(EntityMock.CARD_ID_ENCRYPTED,
                input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getCardProposal().getTitle().getId());
        Assert.assertNotNull(result.getCardProposal().getCardType().getId());
        Assert.assertNull(result.getCardProposal().getParticipant());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getAmount());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getCurrency());
        Assert.assertNotNull(result.getCardProposal().getDelivery()
                .getDestination().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getBranch()
                .getId());
        Assert.assertNotNull(result.getCardProposal().getOfferId());

        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());
        Assert.assertEquals(input.getTitle().getId(), result.getCardProposal()
                .getTitle().getId());
        Assert.assertEquals(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND, result
                .getCardProposal().getCardType().getId());
        Assert.assertEquals(input.getGrantedCredits().get(0).getAmount(),
                result.getCardProposal().getGrantedCredits().get(0).getAmount());
        Assert.assertEquals(input.getGrantedCredits().get(0).getCurrency(),
                result.getCardProposal().getGrantedCredits().get(0)
                        .getCurrency());
        Assert.assertEquals(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND,
                result.getCardProposal().getDelivery().getDestination().getId());
        Assert.assertEquals(input.getBank().getId(), result.getCardProposal()
                .getBank().getId());
        Assert.assertEquals(input.getBank().getBranch().getId(), result
                .getCardProposal().getBank().getBranch().getId());
        Assert.assertEquals(input.getOfferId(), result.getCardProposal()
                .getOfferId());
    }

    @Test
    public void mapInWithoutIdentityDocumentTest() throws IOException {
        CardProposal input = EntityMock.getInstance().getCardProposal();
        input.getParticipant().setIdentityDocument(null);
        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.CARDID, SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL))
                .thenReturn(EntityMock.CARD_ID);
        Mockito.when(enumMapper.getBackendValue("cards.cardType.id", EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_ENUM))
                .thenReturn(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.delivery.destination.id", EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_ENUM))
                .thenReturn(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND);

        InputCreateCardsCardProposal result = mapper.mapIn(EntityMock.CARD_ID_ENCRYPTED,
                input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getCardProposal().getTitle().getId());
        Assert.assertNotNull(result.getCardProposal().getCardType().getId());
        Assert.assertNull(result.getCardProposal().getParticipant()
                .getIdentityDocument());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getFirstName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMiddleName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getSecondLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getProfession().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMaritalStatus().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getAmount());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getCurrency());
        Assert.assertNotNull(result.getCardProposal().getDelivery()
                .getDestination().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getBranch()
                .getId());
        Assert.assertNotNull(result.getCardProposal().getOfferId());

        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());
        Assert.assertEquals(input.getTitle().getId(), result.getCardProposal()
                .getTitle().getId());
        Assert.assertEquals(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND, result
                .getCardProposal().getCardType().getId());
        Assert.assertEquals(input.getParticipant().getFirstName(), result
                .getCardProposal().getParticipant().getFirstName());
        Assert.assertEquals(input.getParticipant().getMiddleName(), result
                .getCardProposal().getParticipant().getMiddleName());
        Assert.assertEquals(input.getParticipant().getLastName(), result
                .getCardProposal().getParticipant().getLastName());
        Assert.assertEquals(input.getParticipant().getSecondLastName(), result
                .getCardProposal().getParticipant().getSecondLastName());
        Assert.assertEquals(input.getParticipant().getProfession().getId(),
                result.getCardProposal().getParticipant().getProfession()
                        .getId());
        Assert.assertEquals(input.getParticipant().getMaritalStatus().getId(),
                result.getCardProposal().getParticipant().getMaritalStatus()
                        .getId());
        Assert.assertEquals(input.getParticipant().getRelationType().getId(), result
                .getCardProposal().getParticipant().getRelationType().getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(0)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertEquals(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(0).getContactType()
                .getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(1)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertEquals(EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(1).getContactType()
                .getId());
        Assert.assertEquals(input.getGrantedCredits().get(0).getAmount(),
                result.getCardProposal().getGrantedCredits().get(0).getAmount());
        Assert.assertEquals(input.getGrantedCredits().get(0).getCurrency(),
                result.getCardProposal().getGrantedCredits().get(0)
                        .getCurrency());
        Assert.assertEquals(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND,
                result.getCardProposal().getDelivery().getDestination().getId());
        Assert.assertEquals(input.getBank().getId(), result.getCardProposal()
                .getBank().getId());
        Assert.assertEquals(input.getBank().getBranch().getId(), result
                .getCardProposal().getBank().getBranch().getId());
        Assert.assertEquals(input.getOfferId(), result.getCardProposal()
                .getOfferId());
    }

    @Test
    public void mapInWithoutIdentityDocumentDocumentTypeTest() throws IOException {
        CardProposal input = EntityMock.getInstance().getCardProposal();
        input.getParticipant().getIdentityDocument().setDocumentType(null);
        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.CARDID, SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL))
                .thenReturn(EntityMock.CARD_ID);
        Mockito.when(enumMapper.getBackendValue("cards.cardType.id", EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_ENUM))
                .thenReturn(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.delivery.destination.id", EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_ENUM))
                .thenReturn(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND);

        InputCreateCardsCardProposal result = mapper.mapIn(EntityMock.CARD_ID_ENCRYPTED,
                input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getCardProposal().getTitle().getId());
        Assert.assertNotNull(result.getCardProposal().getCardType().getId());
        Assert.assertNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getDocumentType());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getFirstName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMiddleName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getSecondLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getProfession().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMaritalStatus().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getAmount());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getCurrency());
        Assert.assertNotNull(result.getCardProposal().getDelivery()
                .getDestination().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getBranch()
                .getId());
        Assert.assertNotNull(result.getCardProposal().getOfferId());

        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());
        Assert.assertEquals(input.getTitle().getId(), result.getCardProposal()
                .getTitle().getId());
        Assert.assertEquals(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND, result
                .getCardProposal().getCardType().getId());
        Assert.assertEquals(input.getParticipant().getIdentityDocument()
                .getNumber(), result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertEquals(input.getParticipant().getFirstName(), result
                .getCardProposal().getParticipant().getFirstName());
        Assert.assertEquals(input.getParticipant().getMiddleName(), result
                .getCardProposal().getParticipant().getMiddleName());
        Assert.assertEquals(input.getParticipant().getLastName(), result
                .getCardProposal().getParticipant().getLastName());
        Assert.assertEquals(input.getParticipant().getSecondLastName(), result
                .getCardProposal().getParticipant().getSecondLastName());
        Assert.assertEquals(input.getParticipant().getProfession().getId(),
                result.getCardProposal().getParticipant().getProfession()
                        .getId());
        Assert.assertEquals(input.getParticipant().getMaritalStatus().getId(),
                result.getCardProposal().getParticipant().getMaritalStatus()
                        .getId());
        Assert.assertEquals(input.getParticipant().getRelationType().getId(), result
                .getCardProposal().getParticipant().getRelationType().getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(0)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertEquals(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(0).getContactType()
                .getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(1)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertEquals(EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(1).getContactType()
                .getId());
        Assert.assertEquals(input.getGrantedCredits().get(0).getAmount(),
                result.getCardProposal().getGrantedCredits().get(0).getAmount());
        Assert.assertEquals(input.getGrantedCredits().get(0).getCurrency(),
                result.getCardProposal().getGrantedCredits().get(0)
                        .getCurrency());
        Assert.assertEquals(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND,
                result.getCardProposal().getDelivery().getDestination().getId());
        Assert.assertEquals(input.getBank().getId(), result.getCardProposal()
                .getBank().getId());
        Assert.assertEquals(input.getBank().getBranch().getId(), result
                .getCardProposal().getBank().getBranch().getId());
        Assert.assertEquals(input.getOfferId(), result.getCardProposal()
                .getOfferId());
    }

    @Test
    public void mapInWithoutContactDetailsTest() throws IOException {
        CardProposal input = EntityMock.getInstance().getCardProposal();
        input.getParticipant().setContactDetails(null);
        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.CARDID, SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL))
                .thenReturn(EntityMock.CARD_ID);
        Mockito.when(enumMapper.getBackendValue("cards.cardType.id", EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_ENUM))
                .thenReturn(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.identityDocument.documentType.id", EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.delivery.destination.id", EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_ENUM))
                .thenReturn(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND);

        InputCreateCardsCardProposal result = mapper.mapIn(EntityMock.CARD_ID_ENCRYPTED,
                input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getCardProposal().getTitle().getId());
        Assert.assertNotNull(result.getCardProposal().getCardType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getDocumentType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getFirstName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMiddleName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getSecondLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getProfession().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMaritalStatus().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType().getId());
        Assert.assertNull(result.getCardProposal().getParticipant()
                .getContactDetails());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getAmount());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getCurrency());
        Assert.assertNotNull(result.getCardProposal().getDelivery()
                .getDestination().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getBranch()
                .getId());
        Assert.assertNotNull(result.getCardProposal().getOfferId());

        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());
        Assert.assertEquals(input.getTitle().getId(), result.getCardProposal()
                .getTitle().getId());
        Assert.assertEquals(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND, result
                .getCardProposal().getCardType().getId());
        Assert.assertEquals(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getIdentityDocument().getDocumentType()
                .getId());
        Assert.assertEquals(input.getParticipant().getIdentityDocument()
                .getNumber(), result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertEquals(input.getParticipant().getFirstName(), result
                .getCardProposal().getParticipant().getFirstName());
        Assert.assertEquals(input.getParticipant().getMiddleName(), result
                .getCardProposal().getParticipant().getMiddleName());
        Assert.assertEquals(input.getParticipant().getLastName(), result
                .getCardProposal().getParticipant().getLastName());
        Assert.assertEquals(input.getParticipant().getSecondLastName(), result
                .getCardProposal().getParticipant().getSecondLastName());
        Assert.assertEquals(input.getParticipant().getProfession().getId(),
                result.getCardProposal().getParticipant().getProfession()
                        .getId());
        Assert.assertEquals(input.getParticipant().getMaritalStatus().getId(),
                result.getCardProposal().getParticipant().getMaritalStatus()
                        .getId());
        Assert.assertEquals(input.getParticipant().getRelationType().getId(), result
                .getCardProposal().getParticipant().getRelationType().getId());
        Assert.assertEquals(input.getGrantedCredits().get(0).getAmount(),
                result.getCardProposal().getGrantedCredits().get(0).getAmount());
        Assert.assertEquals(input.getGrantedCredits().get(0).getCurrency(),
                result.getCardProposal().getGrantedCredits().get(0)
                        .getCurrency());
        Assert.assertEquals(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND,
                result.getCardProposal().getDelivery().getDestination().getId());
        Assert.assertEquals(input.getBank().getId(), result.getCardProposal()
                .getBank().getId());
        Assert.assertEquals(input.getBank().getBranch().getId(), result
                .getCardProposal().getBank().getBranch().getId());
        Assert.assertEquals(input.getOfferId(), result.getCardProposal()
                .getOfferId());
    }

    @Test
    public void mapInWithoutContactDetails1ContactTypeTest() throws IOException {
        CardProposal input = EntityMock.getInstance().getCardProposal();
        input.getParticipant().getContactDetails().get(1).setContactType(null);
        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.CARDID, SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL))
                .thenReturn(EntityMock.CARD_ID);
        Mockito.when(enumMapper.getBackendValue("cards.cardType.id", EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_ENUM))
                .thenReturn(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.identityDocument.documentType.id", EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.delivery.destination.id", EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_ENUM))
                .thenReturn(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND);

        InputCreateCardsCardProposal result = mapper.mapIn(EntityMock.CARD_ID_ENCRYPTED,
                input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getCardProposal().getTitle().getId());
        Assert.assertNotNull(result.getCardProposal().getCardType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getDocumentType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getFirstName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMiddleName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getSecondLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getProfession().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMaritalStatus().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactType());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getAmount());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getCurrency());
        Assert.assertNotNull(result.getCardProposal().getDelivery()
                .getDestination().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getBranch()
                .getId());
        Assert.assertNotNull(result.getCardProposal().getOfferId());

        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());
        Assert.assertEquals(input.getTitle().getId(), result.getCardProposal()
                .getTitle().getId());
        Assert.assertEquals(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND, result
                .getCardProposal().getCardType().getId());
        Assert.assertEquals(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getIdentityDocument().getDocumentType()
                .getId());
        Assert.assertEquals(input.getParticipant().getIdentityDocument()
                .getNumber(), result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertEquals(input.getParticipant().getFirstName(), result
                .getCardProposal().getParticipant().getFirstName());
        Assert.assertEquals(input.getParticipant().getMiddleName(), result
                .getCardProposal().getParticipant().getMiddleName());
        Assert.assertEquals(input.getParticipant().getLastName(), result
                .getCardProposal().getParticipant().getLastName());
        Assert.assertEquals(input.getParticipant().getSecondLastName(), result
                .getCardProposal().getParticipant().getSecondLastName());
        Assert.assertEquals(input.getParticipant().getProfession().getId(),
                result.getCardProposal().getParticipant().getProfession()
                        .getId());
        Assert.assertEquals(input.getParticipant().getMaritalStatus().getId(),
                result.getCardProposal().getParticipant().getMaritalStatus()
                        .getId());
        Assert.assertEquals(input.getParticipant().getRelationType().getId(), result
                .getCardProposal().getParticipant().getRelationType().getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(0)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertEquals(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(0).getContactType()
                .getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(1)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertEquals(input.getGrantedCredits().get(0).getAmount(),
                result.getCardProposal().getGrantedCredits().get(0).getAmount());
        Assert.assertEquals(input.getGrantedCredits().get(0).getCurrency(),
                result.getCardProposal().getGrantedCredits().get(0)
                        .getCurrency());
        Assert.assertEquals(EntityMock.HOME_CARDS_DELIVERY_DESTINATION_ID_BACKEND,
                result.getCardProposal().getDelivery().getDestination().getId());
        Assert.assertEquals(input.getBank().getId(), result.getCardProposal()
                .getBank().getId());
        Assert.assertEquals(input.getBank().getBranch().getId(), result
                .getCardProposal().getBank().getBranch().getId());
        Assert.assertEquals(input.getOfferId(), result.getCardProposal()
                .getOfferId());
    }

    @Test
    public void mapInWithoutDeliveryTest() throws IOException {
        CardProposal input = EntityMock.getInstance().getCardProposal();
        input.setDelivery(null);
        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.CARDID, SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL))
                .thenReturn(EntityMock.CARD_ID);
        Mockito.when(enumMapper.getBackendValue("cards.cardType.id", EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_ENUM))
                .thenReturn(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.identityDocument.documentType.id", EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("cards.contactDetails.contactType.id", EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_ENUM))
                .thenReturn(EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND);

        InputCreateCardsCardProposal result = mapper.mapIn(EntityMock.CARD_ID_ENCRYPTED,
                input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCardId());
        Assert.assertNotNull(result.getCardProposal().getTitle().getId());
        Assert.assertNotNull(result.getCardProposal().getCardType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getDocumentType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getFirstName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMiddleName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getSecondLastName());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getProfession().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getMaritalStatus().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getRelationType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertNotNull(result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactType().getId());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getAmount());
        Assert.assertNotNull(result.getCardProposal().getGrantedCredits()
                .get(0).getCurrency());
        Assert.assertNull(result.getCardProposal().getDelivery());
        Assert.assertNotNull(result.getCardProposal().getBank().getId());
        Assert.assertNotNull(result.getCardProposal().getBank().getBranch()
                .getId());
        Assert.assertNotNull(result.getCardProposal().getOfferId());

        Assert.assertEquals(EntityMock.CARD_ID, result.getCardId());
        Assert.assertEquals(input.getTitle().getId(), result.getCardProposal()
                .getTitle().getId());
        Assert.assertEquals(EntityMock.CREDIT_CARD_CARDS_CAR_TYPE_ID_BACKEND, result
                .getCardProposal().getCardType().getId());
        Assert.assertEquals(EntityMock.DNI_CARDS_IDENTITY_DOCUMENT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getIdentityDocument().getDocumentType()
                .getId());
        Assert.assertEquals(input.getParticipant().getIdentityDocument()
                .getNumber(), result.getCardProposal().getParticipant()
                .getIdentityDocument().getNumber());
        Assert.assertEquals(input.getParticipant().getFirstName(), result
                .getCardProposal().getParticipant().getFirstName());
        Assert.assertEquals(input.getParticipant().getMiddleName(), result
                .getCardProposal().getParticipant().getMiddleName());
        Assert.assertEquals(input.getParticipant().getLastName(), result
                .getCardProposal().getParticipant().getLastName());
        Assert.assertEquals(input.getParticipant().getSecondLastName(), result
                .getCardProposal().getParticipant().getSecondLastName());
        Assert.assertEquals(input.getParticipant().getProfession().getId(),
                result.getCardProposal().getParticipant().getProfession()
                        .getId());
        Assert.assertEquals(input.getParticipant().getMaritalStatus().getId(),
                result.getCardProposal().getParticipant().getMaritalStatus()
                        .getId());
        Assert.assertEquals(input.getParticipant().getRelationType().getId(), result
                .getCardProposal().getParticipant().getRelationType().getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(0)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(0).getContactValue());
        Assert.assertEquals(EntityMock.EMAIL_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(0).getContactType()
                .getId());
        Assert.assertEquals(input.getParticipant().getContactDetails().get(1)
                .getContactValue(), result.getCardProposal().getParticipant()
                .getContactDetails().get(1).getContactValue());
        Assert.assertEquals(EntityMock.PHONE_NUMBER_CARDS_CONTACT_DETAILS_CONTACT_TYPE_ID_BACKEND, result.getCardProposal()
                .getParticipant().getContactDetails().get(1).getContactType()
                .getId());
        Assert.assertEquals(input.getGrantedCredits().get(0).getAmount(),
                result.getCardProposal().getGrantedCredits().get(0).getAmount());
        Assert.assertEquals(input.getGrantedCredits().get(0).getCurrency(),
                result.getCardProposal().getGrantedCredits().get(0)
                        .getCurrency());
        Assert.assertEquals(input.getBank().getId(), result.getCardProposal()
                .getBank().getId());
        Assert.assertEquals(input.getBank().getBranch().getId(), result
                .getCardProposal().getBank().getBranch().getId());
        Assert.assertEquals(input.getOfferId(), result.getCardProposal()
                .getOfferId());
    }

    @Test
    public void mapInEmptyTest() {
        Mockito.when(cypherTool.decrypt(EntityMock.CARD_ID_ENCRYPTED, AbstractCypherTool.CARDID, SMC_REGISTRY_ID_OF_CREATE_CARDS_CARD_PROPOSAL))
                .thenReturn(EntityMock.CARD_ID);
        InputCreateCardsCardProposal result = mapper.mapIn(EntityMock.CARD_ID_ENCRYPTED,
                new CardProposal());
        Assert.assertNotNull(result);
        Assert.assertNull(result.getCardProposal());
        Assert.assertNotNull(result.getCardId());
    }

    @Test
    public void mapOutFullTest() {
        Response result = mapper.mapOut(new CardProposalData());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getEntity());
    }
}

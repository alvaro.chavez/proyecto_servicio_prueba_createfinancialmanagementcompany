package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.pzic.cards.dao.rest.mock.stubs.ResponseDocumentsMock;
import com.bbva.pzic.cards.facade.v0.mapper.IGetCardFinancialStatementDocumentMapper;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.MEDIA_TYPE_PDF;
import static org.junit.Assert.*;

/**
 * Created on 29/07/2018.
 *
 * @author Entelgy
 */
public class GetCardFinancialStatementDocumentMapperTest {

    private IGetCardFinancialStatementDocumentMapper mapper;

    @Before
    public void setUp() {
        mapper = new GetCardFinancialStatementDocumentMapper();
    }

    @Test
    public void mapOut() throws IOException {
        MultipartBody result = mapper.mapOut(
                ResponseDocumentsMock.INSTANCE.buildDocumentFile().getDocumentos().get(0),
                MEDIA_TYPE_PDF);

        assertNotNull(result);
        assertNotNull(result.getAllAttachments());
        assertNotNull(result.getAllAttachments().get(0));
        assertNotNull(result.getAllAttachments().get(0).getContentId());
        assertNotNull(result.getAllAttachments().get(0).getObject());

        assertEquals("rootPart", result.getAllAttachments().get(0).getContentId());
        assertEquals("attachmentPart", result.getAllAttachments().get(1).getContentId());
        assertEquals(MEDIA_TYPE_PDF, result.getAllAttachments().get(1).getContentType().toString());
    }

    @Test
    public void mapOutEmptyTest() {
        MultipartBody result = mapper.mapOut(null, MEDIA_TYPE_PDF);

        assertNull(result);
    }
}

package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancelRequest;
import com.bbva.pzic.cards.facade.v1.dto.RequestCancel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.CARD_ID;
import static com.bbva.pzic.cards.EntityMock.CUSTOMER_CODE;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CreateCardCancelRequestV1MapperTest {

    @InjectMocks
    private CreateCardCancelRequestV1Mapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    private EntityMock entityMock = EntityMock.getInstance();

    @Before
    public void setUp() {
        when(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID)).thenReturn(CUSTOMER_CODE);
    }

    @Test
    public void mapInFullTest() throws IOException {
        RequestCancel input = entityMock.getRequestCancelV1();

        InputCreateCardCancelRequest result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result.getCustomerId());
        assertNotNull(result.getCardId());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getReason().getComments());
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getProduct());
        assertNotNull(result.getCard().getProduct().getId());
        assertNotNull(result.getCard().getProduct().getSubproduct());
        assertNotNull(result.getCard().getProduct().getSubproduct().getId());

        assertEquals(CUSTOMER_CODE, result.getCustomerId());
        assertEquals(CARD_ID, result.getCardId());
        assertEquals(input.getReason().getId(), result.getReason().getId());
        assertEquals(input.getReason().getComments(), result.getReason().getComments());
        assertEquals(input.getCard().getProduct().getId(), result.getCard().getProduct().getId());
        assertEquals(input.getCard().getProduct().getSubProduct().getId(), result.getCard().getProduct().getSubproduct().getId());
    }

    @Test
    public void mapInWithNullValuesTest() throws IOException {
        RequestCancel input = entityMock.getRequestCancelV1();
        input.setCard(null);
        input.setReason(null);

        InputCreateCardCancelRequest result = mapper.mapIn(CARD_ID, input);

        assertNotNull(result.getCustomerId());
        assertNotNull(result.getCardId());
        assertNull(result.getReason());
        assertNull(result.getCard());

        assertEquals(CUSTOMER_CODE, result.getCustomerId());
        assertEquals(CARD_ID, result.getCardId());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<RequestCancel> result = mapper.mapOut(new RequestCancel());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<RequestCancel> result = mapper.mapOut(null);

        assertNull(result);
    }
}

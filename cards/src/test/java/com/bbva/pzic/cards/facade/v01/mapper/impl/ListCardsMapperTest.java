package com.bbva.pzic.cards.facade.v01.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntListCards;
import com.bbva.pzic.cards.business.dto.DTOOutListCards;
import com.bbva.pzic.cards.canonic.CardsList;
import com.bbva.pzic.cards.facade.RegistryIds;
import com.bbva.pzic.cards.util.Constants;
import com.bbva.pzic.cards.util.encrypt.AbstractCypherTool;
import com.bbva.pzic.cards.util.mappers.MapperUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import static com.bbva.pzic.cards.EntityMock.*;

/**
 * Created on 07/02/2017.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ListCardsMapperTest {

    @InjectMocks
    private ListCardsMapper mapper;

    @Mock
    private MapperUtils mapperUtils;

    @Mock
    private AbstractCypherTool cypherTool;

    @Mock
    private ConfigurationManager configurationManager;

    private EntityMock mock = EntityMock.getInstance();

    @Test
    public void mapInFullTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, STATUS_IDS, PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutCustomerIdTest() {
        DTOIntListCards dtoIn = mapper.mapIn(null, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, STATUS_IDS, PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertNull(dtoIn.getCustomerId());
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutCardTypeIdTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, Collections.emptyList(), PHYSICAL_SUPPORT_ID, STATUS_IDS, PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertNull(dtoIn.getCardTypeId());
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutPhysicalSupportIdTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, null, STATUS_IDS, PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertNull(dtoIn.getPhysicalSupportId());
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutStatusIdTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, Collections.emptyList(), PAGINATION_KEY, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertNull(dtoIn.getStatusId());
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithoutPaginationKeyTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, STATUS_IDS, null, PAGINATION_SIZE);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertNull(dtoIn.getPaginationKey());
        Assert.assertEquals(dtoIn.getPageSize().toString(), PAGINATION_SIZE.toString());
    }

    @Test
    public void mapInWithPageSizeTest() {
        DTOIntListCards dtoIn = mapper.mapIn(CUSTOMER_ID, CARD_TYPE_IDS, PHYSICAL_SUPPORT_ID, STATUS_IDS, PAGINATION_KEY, null);
        Assert.assertNotNull(dtoIn);
        Assert.assertEquals(dtoIn.getCustomerId(), CUSTOMER_ID);
        Assert.assertEquals(dtoIn.getCardTypeId(), CARD_TYPE_IDS);
        Assert.assertEquals(dtoIn.getPhysicalSupportId(), PHYSICAL_SUPPORT_ID);
        Assert.assertEquals(dtoIn.getStatusId(), STATUS_IDS);
        Assert.assertEquals(dtoIn.getPaginationKey(), PAGINATION_KEY);
        Assert.assertNull(dtoIn.getPageSize());
    }

    @Test
    public void mapInWithFieldNullTest() {
        DTOIntListCards dtoIn = mapper.mapIn(null, Collections.emptyList(), null, Collections.emptyList(), null, null);
        Assert.assertNotNull(dtoIn);
        Assert.assertNull(dtoIn.getCustomerId());
        Assert.assertNull(dtoIn.getCardTypeId());
        Assert.assertNull(dtoIn.getPhysicalSupportId());
        Assert.assertNull(dtoIn.getStatusId());
        Assert.assertNull(dtoIn.getPaginationKey());
        Assert.assertNull(dtoIn.getPageSize());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        Mockito.when(cypherTool.encrypt(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("C");
        Mockito.when(configurationManager.getProperty(Constants.PROPERTY_IMAGE_WALLET_HOSTNAME)).thenReturn(URL);
        Mockito.when(mapperUtils.buildUrl(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("D");

        DTOOutListCards dtoOut = mock.getDtoOutListCards();

        CardsList result = mapper.mapOut(dtoOut, URL);
        Assert.assertNotNull(result.getData().get(0).getImages().get(0).getUrl());
        Assert.assertNotNull(result.getData().get(1).getImages().get(0).getUrl());
    }

    @Test
    public void mapOutDtoOutNullTest() {
        CardsList result = mapper.mapOut(null, URL);
        Assert.assertNull(result);
    }

    @Test
    public void mapOutDtoOutInitializedTest() {
        CardsList result = mapper.mapOut(new DTOOutListCards(), URL);
        Assert.assertNull(result);
    }

    @Test
    public void mapOutArrayIsEmptyTest() throws IOException {
        DTOOutListCards dtoOut = mock.getDtoOutListCards();
        dtoOut.setData(new ArrayList<>());
        CardsList result = mapper.mapOut(dtoOut, URL);
        Assert.assertNull(result);
    }
}

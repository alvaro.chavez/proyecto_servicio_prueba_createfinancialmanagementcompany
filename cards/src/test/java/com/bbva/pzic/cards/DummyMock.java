package com.bbva.pzic.cards;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.cards.business.dto.DTOInstallmentsPlanList;
import com.bbva.pzic.cards.canonic.Activation;
import com.bbva.pzic.cards.canonic.Card;
import com.bbva.pzic.cards.canonic.InstallmentsPlan;
import com.bbva.pzic.cards.model.Customer;
import com.bbva.pzic.cards.model.Option;
import com.bbva.pzic.cards.model.Person;
import com.bbva.pzic.cards.model.User;
import com.bbva.pzic.cards.util.Errors;
import com.bbva.pzic.cards.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created on 24/06/2016.
 *
 * @author Entelgy
 */
@Component
public class DummyMock {

    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    public Person getPerson() {
        Person person = new Person();
        person.setId("001");
        person.setAge(20);
        person.setDocumentNumber("0001-234");
        person.setFullName("Luis La Mera");
        person.setDocumentType(getOption());
        person.setCustomerList(getCustomerList());

        return person;
    }

    public Option getOption() {
        Option option = new Option("123");
        option.setId("123");
        option.setValue("TELE");
        return option;
    }

    public List<Customer> getCustomerList() {
        List<Customer> customerList = new ArrayList<>();
        customerList.add(getCustomer());
        customerList.add(getCustomer());
        customerList.add(getCustomer());
        customerList.add(getCustomer());
        return customerList;
    }

    public Customer getCustomer() {
        Customer customer = new Customer();
        customer.setAge(100);
        customer.setId("123456789");
        customer.setFullName("todos unidos entelgy");
        customer.setDocumentType(getOption());
        customer.setUsers(getUserList());
        return customer;
    }

    public List<User> getUserList() {
        List<User> userList = new ArrayList<>();
        userList.add(getUser());
        userList.add(getUser());
        userList.add(getUser());
        userList.add(getUser());
        return userList;
    }

    public User getUser() {
        User user = new User();
        user.setId("1224");
        user.setLogin("lmera");
        user.setPassword("irma");
        return user;
    }

    public List<Activation> getActivationList() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/patch_request_modify_card_activations.json"), new TypeReference<List<Activation>>() {
        });
    }

    public Activation getActivation() {
        Activation activation = new Activation();
        activation.setActivationId("ECOMMERCE_ACTIVATION");
        activation.setIsActive(true);
        return activation;
    }

    public Card getCardMock() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/get_card.json"), Card.class);
    }

    public InstallmentsPlan getInstallmentsPlan() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/installmentPlan.json"), InstallmentsPlan.class);
    }

    public DTOInstallmentsPlanList getDtoInstallmentsPlanList() throws IOException {
        DTOInstallmentsPlanList planList = new DTOInstallmentsPlanList();
        planList.setData(new ArrayList<>());
        planList.getData().add(getInstallmentsPlan());
        return planList;
    }

    public Date buildDate(Date date, String time, String format) {
        String formatDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        try {
            return new SimpleDateFormat(format).parse(String.format("%s%s", formatDate, time));
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

    public String formatTime(final String time) {
        return time.substring(0, 2) + ":" + time.substring(2, 4) + ":" + time.substring(4);
    }
}

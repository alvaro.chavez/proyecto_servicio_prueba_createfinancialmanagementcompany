package com.bbva.pzic.cards.facade.v1.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputSimulateCardTransactionTransactionRefund;
import com.bbva.pzic.cards.facade.v1.dto.SimulateTransactionRefund;
import com.bbva.pzic.cards.util.Enums;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 17/09/2020.
 *
 * @author Entelgy.
 */
@RunWith(MockitoJUnitRunner.class)
public class SimulateCardTransactionTransactionRefundMapperTest {

    @InjectMocks
    private SimulateCardTransactionTransactionRefundMapper mapper;

    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() throws IOException {
        SimulateTransactionRefund input = EntityMock.getInstance().buildSimulateTransactionRefund();
        when(translator.translateFrontendEnumValueStrictly(Enums.TRANSACTION_REFUND_REFUND_TYPE_ID_KEY, input.getRefundType().getId())).thenReturn(EntityMock.REFUND_TYPE_ID_KEY);
        InputSimulateCardTransactionTransactionRefund result = mapper.mapIn(EntityMock.CARD_ID, EntityMock.TRANSACTION_ID, input);

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getTransactionId());
        assertNotNull(result.getRefundTypeId());

        assertEquals(EntityMock.CARD_ID, result.getCardId());
        assertEquals(EntityMock.TRANSACTION_ID, result.getTransactionId());
        assertEquals(EntityMock.REFUND_TYPE_ID_KEY, result.getRefundTypeId());
    }

    @Test
    public void mapInEmptyTest() {
        InputSimulateCardTransactionTransactionRefund result = mapper.mapIn(EntityMock.CARD_ID, EntityMock.TRANSACTION_ID, new SimulateTransactionRefund());

        assertNotNull(result);
        assertNotNull(result.getCardId());
        assertNotNull(result.getTransactionId());
        assertNull(result.getRefundTypeId());

        assertEquals(EntityMock.CARD_ID, result.getCardId());
        assertEquals(EntityMock.TRANSACTION_ID, result.getTransactionId());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ServiceResponse<SimulateTransactionRefund> result = mapper.mapOut(EntityMock.getInstance().buildSimulateTransactionRefund());

        assertNotNull(result);
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<SimulateTransactionRefund> result = mapper.mapOut(null);

        assertNull(result);
    }
}
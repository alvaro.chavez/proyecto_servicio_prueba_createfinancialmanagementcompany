package com.bbva.pzic.cards.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {

    @NotNull(groups = ValidationGroup.ModifyCustomer.class)
    @Size(min = 10, max = 10, groups = ValidationGroup.ModifyCustomer.class)
    private String id;
    @Size(max = 20)
    private String login;
    @Size(min = 6, max = 20)
    private String password;
    @NotNull
    @Valid
    private Option type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Option getType() {
        return type;
    }

    public void setType(Option type) {
        this.type = type;
    }

}

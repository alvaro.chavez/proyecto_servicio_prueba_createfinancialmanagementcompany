package com.bbva.pzic.cards.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.DTOIntDetailSimulation;
import com.bbva.pzic.cards.canonic.HolderSimulation;
import com.bbva.pzic.cards.dao.rest.mock.stubs.CardsOfferSimulateMock;
import com.bbva.pzic.cards.util.BusinessServiceUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.cards.EntityMock.CLIENT_ID;
import static com.bbva.pzic.cards.EntityMock.HEADER_USER_AGENT_STUB;
import static com.bbva.pzic.cards.EntityMock.URL_INITIAL_WITHOUT_DIMENSIONS;
import static com.bbva.pzic.cards.EntityMock.URL_FINAL_WITH_DIMENSIONS;


/**
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateCardsOfferSimulateMapperTest {

    @Mock
    private ServiceInvocationContext serviceInvocationContext;
    @InjectMocks
    private CreateCardsOfferSimulateMapper mapper;

    private void enumMapIn() {
        Mockito.when(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID)).thenReturn(CLIENT_ID);
    }

    @Test
    public void mapInFullTest() throws IOException {
        enumMapIn();

        HolderSimulation input = CardsOfferSimulateMock.getInstance().buildCarHolderSimulationRequest();
        DTOIntDetailSimulation result = mapper.mapIn(EntityMock.OFFER_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getOfferId());
        Assert.assertNotNull(result.getClientId());
        Assert.assertNotNull(result.getProduct().getId());
        Assert.assertNotNull(result.getProduct().getSubproduct().getId());

        Assert.assertEquals(EntityMock.OFFER_ID, result.getOfferId());
        Assert.assertEquals(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID), result.getClientId());
        Assert.assertEquals(input.getDetails().getProduct().getId(), result.getProduct().getId());
        Assert.assertEquals(input.getDetails().getProduct().getSubproduct().getId(),
                result.getProduct().getSubproduct().getId());
    }

    @Test
    public void mapInEmptyTest() {
        DTOIntDetailSimulation result = mapper.mapIn(EntityMock.OFFER_ID, new HolderSimulation());
        Assert.assertNull(result);
    }

    @Test
    public void mapOutNullTest() {
        ServiceResponse<HolderSimulation> result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}

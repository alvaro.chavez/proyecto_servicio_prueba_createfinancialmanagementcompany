package com.bbva.pzic.cards.dao.apx.mapper.impl;

import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardCancelRequest;
import com.bbva.pzic.cards.dao.model.pcpst007_1.PeticionTransaccionPcpst007_1;
import com.bbva.pzic.cards.dao.model.pcpst007_1.RespuestaTransaccionPcpst007_1;
import com.bbva.pzic.cards.dao.model.pcpst007_1.mock.FormatsPcpst007_1Mock;
import com.bbva.pzic.cards.facade.v1.dto.RequestCancel;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;

import static org.junit.Assert.*;

public class ApxCreateCardCancelRequestV1MapperTest {

    private ApxCreateCardCancelRequestV1Mapper mapper = new ApxCreateCardCancelRequestV1Mapper();

    private EntityMock entityMock = EntityMock.getInstance();

    @Test
    public void mapInFullTest() throws IOException {
        InputCreateCardCancelRequest input = entityMock.getInputCreateCardCancelRequestV1();

        PeticionTransaccionPcpst007_1 result = mapper.mapIn(input);

        assertNotNull(result.getCustomerid());
        assertNotNull(result.getCardid());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getReason().getComments());
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getProduct());
        assertNotNull(result.getCard().getProduct().getId());
        assertNotNull(result.getCard().getProduct().getSubproduct());
        assertNotNull(result.getCard().getProduct().getSubproduct().getId());

        assertEquals(input.getCustomerId(), result.getCustomerid());
        assertEquals(input.getCardId(), result.getCardid());
        assertEquals(input.getReason().getId(), result.getReason().getId());
        assertEquals(input.getReason().getComments(), result.getReason().getComments());
        assertEquals(input.getCard().getProduct().getId(), result.getCard().getProduct().getId());
        assertEquals(input.getCard().getProduct().getSubproduct().getId(), result.getCard().getProduct().getSubproduct().getId());
    }

    @Test
    public void mapInWithoutNonMandatoryParametersTest() throws IOException {
        InputCreateCardCancelRequest input = entityMock.getInputCreateCardCancelRequestV1();
        input.setCard(null);

        PeticionTransaccionPcpst007_1 result = mapper.mapIn(input);

        assertNotNull(result.getCustomerid());
        assertNotNull(result.getCardid());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getReason().getComments());
        assertNull(result.getCard());

        assertEquals(input.getCustomerId(), result.getCustomerid());
        assertEquals(input.getCardId(), result.getCardid());
        assertEquals(input.getReason().getId(), result.getReason().getId());
        assertEquals(input.getReason().getComments(), result.getReason().getComments());
    }

    @Test
    public void mapOutFullTest() throws IOException, ParseException {
        RespuestaTransaccionPcpst007_1 input = FormatsPcpst007_1Mock.getInstance().getRespuestaTransaccionPcpst007_1();

        RequestCancel result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getOperationTracking());
        assertNotNull(result.getOperationTracking().getOperationNumber());
        assertNotNull(result.getOperationTracking().getOperationDate());
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getId());
        assertNotNull(result.getCard().getNumber());
        assertNotNull(result.getCard().getNumberType());
        assertNotNull(result.getCard().getNumberType().getId());
        assertNotNull(result.getCard().getNumberType().getDescription());
        assertNotNull(result.getCard().getProduct());
        assertNotNull(result.getCard().getProduct().getId());
        assertNotNull(result.getCard().getProduct().getDescription());
        assertNotNull(result.getCard().getProduct().getSubproduct());
        assertNotNull(result.getCard().getProduct().getSubproduct().getId());

        assertEquals(input.getOperationtracking().getOperationnumber(), result.getOperationTracking().getOperationNumber());
        assertEquals(DateUtils.toDateTime(input.getOperationtracking().getOperationdate(), "00:00:00"), result.getOperationTracking().getOperationDate());
        assertEquals(input.getCampo_2_card().getId(), result.getCard().getId());
        assertEquals(input.getCampo_2_card().getNumber(), result.getCard().getNumber());
        assertEquals(input.getCampo_2_card().getNumbertype().getId(), result.getCard().getNumberType().getId());
        assertEquals(input.getCampo_2_card().getNumbertype().getDescription(), result.getCard().getNumberType().getDescription());
        assertEquals(input.getCampo_2_card().getProduct().getId(), result.getCard().getProduct().getId());
        assertEquals(input.getCampo_2_card().getProduct().getDescription(), result.getCard().getProduct().getDescription());
        assertEquals(input.getCampo_2_card().getProduct().getSubproduct().getId(), result.getCard().getProduct().getSubproduct().getId());
    }

    @Test
    public void mapOutWithoutInnerParametersTest() throws IOException {
        RespuestaTransaccionPcpst007_1 input = FormatsPcpst007_1Mock.getInstance().getRespuestaTransaccionPcpst007_1();
        input.setOperationtracking(null);
        input.getCampo_2_card().setNumbertype(null);
        input.getCampo_2_card().getProduct().setSubproduct(null);

        RequestCancel result = mapper.mapOut(input);

        assertNotNull(result);
        assertNull(result.getOperationTracking());
        assertNotNull(result.getCard());
        assertNotNull(result.getCard().getId());
        assertNotNull(result.getCard().getNumber());
        assertNull(result.getCard().getNumberType());
        assertNotNull(result.getCard().getProduct());
        assertNotNull(result.getCard().getProduct().getId());
        assertNotNull(result.getCard().getProduct().getDescription());
        assertNull(result.getCard().getProduct().getSubproduct());

        assertEquals(input.getCampo_2_card().getId(), result.getCard().getId());
        assertEquals(input.getCampo_2_card().getNumber(), result.getCard().getNumber());
        assertEquals(input.getCampo_2_card().getProduct().getId(), result.getCard().getProduct().getId());
        assertEquals(input.getCampo_2_card().getProduct().getDescription(), result.getCard().getProduct().getDescription());
    }
}

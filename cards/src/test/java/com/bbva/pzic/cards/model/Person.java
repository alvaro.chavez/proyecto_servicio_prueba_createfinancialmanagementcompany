package com.bbva.pzic.cards.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created on 24/06/2016.
 *
 * @author Entelgy
 */
public class Person {

    @NotNull(groups = ValidationGroup.ModifyCustomer.class)
    @Size(min = 10, max = 10, groups = ValidationGroup.ModifyCustomer.class)
    private String id;
    @NotNull(groups = ValidationGroup.CreateCustomer.class)
    private String fullName;
    private Integer age;
    @NotNull(groups = ValidationGroup.CreateCustomer.class)
    @Valid
    private Option documentType;
    @NotNull
    @Pattern(regexp = "^[0-9]{8,11}$")
    private String documentNumber;

    private List<Customer> customerList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Option getDocumentType() {
        return documentType;
    }

    public void setDocumentType(Option documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }
}
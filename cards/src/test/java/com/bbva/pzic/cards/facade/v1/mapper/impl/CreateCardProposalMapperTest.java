package com.bbva.pzic.cards.facade.v1.mapper.impl;


import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.cards.EntityMock;
import com.bbva.pzic.cards.business.dto.InputCreateCardProposal;
import com.bbva.pzic.cards.facade.v1.dto.Address;
import com.bbva.pzic.cards.facade.v1.dto.ProposalCard;
import com.bbva.pzic.cards.facade.v1.mapper.common.AbstractCardProposalMapper;
import com.bbva.pzic.cards.facade.v1.mapper.common.CardProposalTimeMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 11/20/2019.
 *
 * @author Entelgy
 */
public class CreateCardProposalMapperTest {

    private CreateCardProposalMapper mapper;

    private AbstractCardProposalMapper abstractCardProposalMapper = new CardProposalTimeMapper();

    @Before
    public void setUp() {
        mapper = new CreateCardProposalMapper();
        mapper.setAddressMapper(abstractCardProposalMapper);
    }

    @Test
    public void mapInFullTest() throws IOException {
        ProposalCard input = EntityMock.getInstance().buildProposalCard();
        InputCreateCardProposal result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getContactType());
        assertNotNull(result.getContact().getContact());
        assertNotNull(result.getContact().getContact().getContactDetailType());
        assertNotNull(result.getContact().getContact().getNumber());
        assertNotNull(result.getContact().getContact().getPhoneCompany());
        assertNotNull(result.getContact().getContact().getPhoneCompany().getId());
        assertNotNull(result.getFees());
        assertNotNull(result.getFees().getItemizeFees());
        assertEquals(1, result.getFees().getItemizeFees().size());
        assertNotNull(result.getFees().getItemizeFees().get(0).getFeeType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertNotNull(result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertNotNull(result.getRates());
        assertNotNull(result.getRates().getItemizeRates());
        assertEquals(1, result.getRates().getItemizeRates().size());
        assertNotNull(result.getRates().getItemizeRates().get(0).getRateType());
        assertNotNull(result.getRates().getItemizeRates().get(0).getCalculationDate());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertNotNull(result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertNotNull(result.getGrantedCredits());
        assertEquals(1, result.getGrantedCredits().size());
        assertNotNull(result.getGrantedCredits().get(0).getAmount());
        assertNotNull(result.getGrantedCredits().get(0).getCurrency());
        assertNotNull(result.getPhysicalSupport());
        assertNotNull(result.getPhysicalSupport().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getCardType());
        assertNotNull(result.getCardType().getId());
        assertNotNull(result.getParticipants());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getId());
        assertNotNull(result.getParticipants().get(0).getIsCustomer());
        assertNotNull(result.getParticipants().get(0).getPersonType());
        assertNotNull(result.getParticipants().get(0).getParticipantTypeId());
        assertNotNull(result.getParticipants().get(1).getPersonType());
        assertNotNull(result.getParticipants().get(1).getId());
        assertNotNull(result.getParticipants().get(1).getIsCustomer());
        assertNotNull(result.getParticipants().get(1).getParticipantTypeId());
        assertNotNull(result.getOfferId());

        assertEquals(input.getContact().getContactType(), result.getContact().getContactType());
        assertEquals(input.getContact().getContact().getContactDetailType(), result.getContact().getContact().getContactDetailType());
        assertEquals(input.getContact().getContact().getNumber(), result.getContact().getContact().getNumber());
        assertEquals(input.getContact().getContact().getPhoneCompany().getId(), result.getContact().getContact().getPhoneCompany().getId());
        assertEquals(input.getFees().getItemizeFees().get(0).getFeeType(), result.getFees().getItemizeFees().get(0).getFeeType());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getUnitType());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getAmount());
        assertEquals(input.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency(), result.getFees().getItemizeFees().get(0).getItemizeFeeUnit().getCurrency());
        assertEquals(input.getRates().getItemizeRates().get(0).getRateType(), result.getRates().getItemizeRates().get(0).getRateType());
        assertEquals(input.getRates().getItemizeRates().get(0).getCalculationDate(), result.getRates().getItemizeRates().get(0).getCalculationDate());
        assertEquals(input.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage(), result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getPercentage());
        assertEquals(input.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType(), result.getRates().getItemizeRates().get(0).getItemizeRatesUnit().getUnitRateType());
        assertEquals(input.getGrantedCredits().get(0).getAmount(), result.getGrantedCredits().get(0).getAmount());
        assertEquals(input.getGrantedCredits().get(0).getCurrency(), result.getGrantedCredits().get(0).getCurrency());
        assertEquals(input.getPhysicalSupport().getId(), result.getPhysicalSupport().getId());
        assertEquals(input.getCardType().getId(), result.getCardType().getId());
        assertEquals(input.getProduct().getId(), result.getProduct().getId());
        assertEquals(2, input.getParticipants().size(), result.getParticipants().size());
        assertEquals(input.getParticipants().get(0).getPersonType(), result.getParticipants().get(0).getPersonType());
        assertEquals(input.getParticipants().get(0).getId(), result.getParticipants().get(0).getId());
        assertEquals(input.getParticipants().get(0).getIsCustomer(), result.getParticipants().get(0).getIsCustomer());
        assertEquals(input.getParticipants().get(0).getParticipantType().getId(), result.getParticipants().get(0).getParticipantTypeId());
        assertEquals(input.getParticipants().get(1).getPersonType(), result.getParticipants().get(1).getPersonType());
        assertEquals(input.getParticipants().get(1).getId(), result.getParticipants().get(1).getId());
        assertEquals(input.getParticipants().get(1).getIsCustomer(), result.getParticipants().get(1).getIsCustomer());
        assertEquals(input.getParticipants().get(1).getParticipantType().getId(), result.getParticipants().get(1).getParticipantTypeId());
        assertEquals(input.getOfferId(), result.getOfferId());
    }

    @Test
    public void mapInEmptyTest() {
        InputCreateCardProposal result = mapper.mapIn(new ProposalCard());

        assertNotNull(result);
        assertNull(result.getFees());
        assertNull(result.getRates());
        assertNull(result.getContact());
        assertNull(result.getGrantedCredits());
        assertNull(result.getPhysicalSupport());
        assertNull(result.getProduct());
        assertNull(result.getCardType());
        assertNull(result.getParticipants());
        assertNull(result.getOfferId());
    }

    @Test
    public void testMapOutFull() throws IOException {
        ServiceResponse<ProposalCard> proposalCard = mapper.mapOut(EntityMock.getInstance().buildProposalCard());

        assertNotNull(proposalCard);
        assertNotNull(proposalCard.getData());
        assertNull(proposalCard.getPagination());
    }

    @Test
    public void testMapOutNull() {
        ServiceResponse<ProposalCard> proposalCard = mapper.mapOut(null);

        assertNull(proposalCard);
    }
}
